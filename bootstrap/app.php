<?php

use App\Http\Middleware\LogRequestResponse;
use App\Http\Middleware\AuditTrailMiddleware;
use App\Http\Middleware\Tracking;
use Fideloper\Proxy\TrustedProxyServiceProvider;
use Fideloper\Proxy\TrustProxies;
use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use BaoPham\DynamoDb\DynamoDbServiceProvider;

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

// Configure Swagger wrapper
$app->configure('swagger-lume');

// Configure CORS
$app->configure('cors');

// Configure AWS
$app->configure('aws');

// Configure Amqp
$app->configure('amqp');

// Configure Queues
$app->configure('queues');

// Configure Consumers
$app->configure('consumers');

// Configure filesystems
$app->configure('filesystems');

// Configure LumenEventStreamMQ
$app->configure('eventstreammq');

// Configure Intercom Messenger
$app->configure('intercom');

$app->configure('tracker');

$app->configure('trustedproxy');

$app->configure('modules_map');

$app->configure('account_essential');

$app->configure('audit_trail');
$app->configure('audit_trail_endpoints');

$app->configure('dynamodb');
/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    TrustProxies::class,
    Tracking::class,
]);

$app->routeMiddleware([
    'authn' => App\Http\Middleware\AuthnMiddleware::class,
    'auth0' => App\Http\Middleware\Auth0Middleware::class,
    'user' => \App\Http\Middleware\UserMiddleware::class,
    'ess' => \App\Http\Middleware\EmployeeMiddleware::class,
    'cors' => Barryvdh\Cors\HandleCors::class,
    'subscription' => \App\Http\Middleware\SubscriptionMiddleware::class,
    'access' => \App\Http\Middleware\AccessMiddleware::class,
    'authz' => \App\Http\Middleware\AuthzMiddleware::class,
    'audit' => \App\Http\Middleware\AuditTrailMiddleware::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// Trusted Proxies
$app->register(TrustedProxyServiceProvider::class);
$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\Auth0ManagementServiceProvider::class);
$app->register(App\Providers\PermissionServiceProvider::class);
$app->register(App\Providers\TaskServiceProvider::class);
$app->register(App\Providers\RequestServiceProvider::class);
$app->register(App\Providers\AuditServiceProvider::class);
$app->register(App\Providers\UploadTaskServiceProvider::class);
$app->register(App\Providers\UploadServiceProvider::class);
$app->register(App\Providers\ValidatorProvider::class);
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);

// CORS
$app->register(Barryvdh\Cors\LumenServiceProvider::class);

// Swagger Wrapper
$app->register(\SwaggerLume\ServiceProvider::class);

// Dingo API package
$app->register(Dingo\Api\Provider\LumenServiceProvider::class);

// AWS
$app->register(Aws\Laravel\AwsServiceProvider::class);

// RabbitMQ
if (!class_exists('App')) {
    class_alias(\Illuminate\Support\Facades\App::class, 'App');
}
$app->register(Bschmitt\Amqp\LumenServiceProvider::class);

// Redis
$app->register(Illuminate\Redis\RedisServiceProvider::class);

// Consumer Artisan
$app->register(App\Providers\ConsumerCommandServiceProvider::class);

// SafeAMQP
$app->register(Salarium\SafeAmqp\Providers\SafeAmqpLumenServiceProvider::class);

//EventStreamMQ
$app->register(Salarium\LumenEventStreamMQ\Providers\EventStreamServiceProvider::class);

$app->register(DynamoDbServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
    require __DIR__.'/../app/Http/channels.php';
});

$app->configureMonologUsing(function ($monolog) use ($app) {
    if (!$app->runningUnitTests()) {
        $monolog->pushHandler(new StreamHandler(storage_path('logs/lumen.log'), Logger::INFO));
        $monolog->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

        $monolog->pushHandler(new StreamHandler(storage_path('logs/lumen-err.log'), Logger::WARNING));
        $monolog->pushHandler(new StreamHandler('php://stderr', Logger::WARNING));
    } else {
        $monolog->pushHandler(new NullHandler());
    }

    return $monolog;
});

return $app;
