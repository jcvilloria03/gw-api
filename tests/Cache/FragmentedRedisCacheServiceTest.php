<?php

namespace Tests\Cache;

use lastguest\Murmur;
use Mockery as M;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Salarium\Cache\FragmentedRedisCache;

class FragmentedRedisCacheTest extends \Tests\TestCase
{
    public function testGetReturnsNullOnEmptyData()
    {
        Redis::shouldReceive('hGet')
            ->andReturn(null);

        $service = App::make(FragmentedRedisCache::class);

        $data = $service->get('test');

        $this->assertEquals(null, $data);
    }

    public function testGet()
    {
        $service = App::make(FragmentedRedisCache::class);

        Redis::shouldReceive('hGet')
            ->with(
                $service->generateBucketKey('test'),
                $service->generateFieldKey('test')
            )
            ->andReturn(json_encode(['test' => 'data']));

        $data = $service->get('test');

        $this->assertEquals(['test' => 'data'], $data);
    }

    public function testSet()
    {
        $service = App::make(FragmentedRedisCache::class);

        Redis::shouldReceive('hSet')
            ->with(
                $service->generateBucketKey('test'),
                $service->generateFieldKey('test'),
                json_encode(['test' => 'data'])
            )
            ->shouldReceive('hGet')
            ->with(
                $service->generateBucketKey('test'),
                $service->generateFieldKey('test')
            )
            ->andReturn(json_encode(['test' => 'data']));

        $service->set('test', ['test' => 'data']);
        $this->assertEquals(['test' => 'data'], $service->get('test'));
    }

    public function testDelete()
    {
        $service = App::make(FragmentedRedisCache::class);

        Redis::shouldReceive('hDel')
            ->with(
                $service->generateBucketKey('test'),
                $service->generateFieldKey('test')
            )
            ->shouldReceive('hGet')
            ->with(':bucket:7', ':test')
            ->andReturn(null);

        $service->delete('test');
        $this->assertEmpty($service->get('test'));
    }

    public function testExists()
    {
        $service = App::make(FragmentedRedisCache::class);

        Redis::shouldReceive('hExists')
            ->with(
                $service->generateBucketKey('test'),
                $service->generateFieldKey('test')
            )
            ->andReturn(1)
            ->shouldReceive('hExists')
            ->with(
                $service->generateBucketKey('missing'),
                $service->generateFieldKey('missing')
            )
            ->andReturn(0);

        $this->assertTrue($service->exists('test'));
        $this->assertFalse($service->exists('missing'));
    }

    public function testSetPrefixes()
    {
        $service = App::make(FragmentedRedisCache::class);
        $baseBucketKey =  $service->generateBucketKey('test');
        $baseFieldKey  = $service->generateFieldKey('test');

        $service->setPrefix('test');
        $service->setHashFieldPrefix('fieldTest');

        Redis::shouldReceive('hGet')
            ->with(
                'test'.$baseBucketKey,
                'fieldTest'.$baseFieldKey
            );

        $service->get('test');
    }
}
