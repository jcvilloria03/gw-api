<?php

namespace Tests\Account;

use App\Account\AccountAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class AccountAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testGetInvalidScope()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAccountId, 1));
    }

    public function testUpdatePass()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetAccountId, 1));
    }

    public function testUpdateNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate(1, 1));
    }

    public function testGetAccountCompaniesPass()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetAccountCompanies($targetAccountId, 1));
    }

    public function testGetAccountCompaniesNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGetAccountCompanies(1, 1));
    }

    public function testGetAccountCompaniesInvalidScope()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetAccountCompanies($targetAccountId, 1));
    }

    public function testProgressPass()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->progress($targetAccountId, 1));
    }

    public function testProgressNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->progress(1, 1));
    }

    public function testProgressInvalidScope()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->progress($targetAccountId, 1));
    }

    public function testAuthorizeGetAccountLogsPass()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::VIEW_LOGS_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetAccountLogs($targetAccountId, 1));
    }

    public function testAuthorizeGetAccountLogsNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGetAccountLogs(1, 1));
    }

    public function testAuthorizeGetAccountLogsInvalidScope()
    {
        $taskScope = new TaskScopes(AccountAuthorizationService::VIEW_LOGS_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Account\AccountAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetAccountLogs($targetAccountId, 1));
    }
}
