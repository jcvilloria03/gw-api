<?php

namespace Tests\Authz;

use App\Authz\AuthzDataScope;
use PHPUnit\Framework\TestCase;

class AuthzDataScopeTest extends TestCase
{
    const MOCK_CLEARANCE_DATA = [
        'module1' => [
            'data_scope' => [
                'COMPANY' => [
                    1, 2, 3
                ],

                'DEPARTMENT' => [
                    4, 5, 6
                ]
            ]
        ],

        'module2' => [
            'data_scope' => [
                'COMPANY' => [
                    1, 2, 3
                ],

                'DEPARTMENT' => [
                    4, 5, 6
                ]
            ]
        ],
    ];

    public function testMakeFromClearance()
    {
        $authzDataScope = AuthzDataScope::makeFromClearance(self::MOCK_CLEARANCE_DATA);

        $this->assertEquals($authzDataScope->getDataScope(), [
            'COMPANY' => [
                1, 2, 3
            ],

            'DEPARTMENT' => [
                4, 5, 6
            ]
        ]);
    }

    public function testAuthorization()
    {
        $authzDataScope = AuthzDataScope::makeFromClearance(self::MOCK_CLEARANCE_DATA);

        $this->assertTrue($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 1));
        $this->assertTrue($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 2));
        $this->assertTrue($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 3));

        $this->assertFalse($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 4));
        $this->assertFalse($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 5));
        $this->assertFalse($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, 6));

        $this->assertFalse($authzDataScope->isAuthorized('_INVALID_SCOPE_', 9999));
    }
}
