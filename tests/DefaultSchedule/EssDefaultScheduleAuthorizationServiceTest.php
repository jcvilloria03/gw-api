<?php

namespace Tests\DefaultSchedule;

use App\DefaultSchedule\EssDefaultScheduleAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssApprovalAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeView()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssDefaultScheduleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\DefaultSchedule\EssDefaultScheduleAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($user['user_id']));
    }

    public function testFailToAuthorizeView()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\DefaultSchedule\EssDefaultScheduleAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($user['user_id']));
    }
}
