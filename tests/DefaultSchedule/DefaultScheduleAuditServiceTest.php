<?php

namespace Tests\DefaultSchedule;

use App\Audit\AuditService;
use App\DefaultSchedule\DefaultScheduleAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class DefaultScheduleAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
        ]);
        $item = [
            'action' => DefaultScheduleAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new DefaultScheduleAuditService($mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => DefaultScheduleAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $locationAuditService = new DefaultScheduleAuditService($mockAuditService);
        $locationAuditService->logUpdate($item);
    }
}
