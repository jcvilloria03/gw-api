<?php

namespace Tests\DefaultSchedule;

use App\DefaultSchedule\DefaultScheduleAuthorizationService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

class DefaultScheduleAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::VIEW_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::CREATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DefaultScheduleAuthorizationService::UPDATE_TASK);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            DefaultScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }
}
