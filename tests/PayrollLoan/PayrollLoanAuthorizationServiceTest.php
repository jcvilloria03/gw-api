<?php

namespace Tests\PayrollLoan;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\PayrollLoan\PayrollLoanAuthorizationService;
use Tests\Authorization\AuthorizationServiceTestTrait;
use Tests\TestCase;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
* @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
*/
class PayrollLoanAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollLoanDetails, $user));
    }

    public function testGetCompanyPayrollLoansPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testGetCompanyPayrollLoansInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetPayrollLoanDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollLoanDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollLoanDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollLoanDetails, $user));
    }

    public function testRequestPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestNoScope()
    {
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testRequestInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::REQUEST_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeRequest($targetPayrollLoanDetails, $user));
    }

    public function testApprovePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApprovePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApprovePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApproveNoScope()
    {
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApproveInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApproveInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApproveInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }

    public function testApproveInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollLoanAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollLoanDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            PayrollLoanAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeApprove($targetPayrollLoanDetails, $user));
    }
}
