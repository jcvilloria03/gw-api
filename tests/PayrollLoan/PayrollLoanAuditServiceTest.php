<?php

namespace Tests\PayrollLoan;

use App\Audit\AuditService;
use App\PayrollLoan\PayrollLoanAuditService;
use App\PayrollLoan\PayrollLoanRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;

class PayrollLoanAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => PayrollLoanAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockRequestService = m::mock(PayrollLoanRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanAuditService = new PayrollLoanAuditService($mockRequestService, $mockAuditService);
        $payrollLoanAuditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => PayrollLoanAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockRequestService = m::mock(PayrollLoanRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanAuditService = new PayrollLoanAuditService($mockRequestService, $mockAuditService);
        $payrollLoanAuditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => PayrollLoanAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock(PayrollLoanRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanAuditService = new PayrollLoanAuditService($mockRequestService, $mockAuditService);
        $payrollLoanAuditService->log($item);
    }

    public function testLogBatchCreate()
    {
        $user = [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];

        $items = [
            'action' => PayrollLoanAuditService::ACTION_BATCH_CREATE,
            'company_id' => 1,
            'user_id' => 1,
            'account_id' => 1,
            'loans_ids' => [1,2,3]
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log');

        $mockRequestService = m::mock(PayrollLoanRequestService::class);
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'full_name' => 'Test name'
            ]
        ]));
        $mockRequestService
            ->shouldReceive('get')
            ->andReturn($response);

        $loanRequestAuditService = new PayrollLoanAuditService(
            $mockRequestService,
            $mockAuditService
        );

        $loanRequestAuditService->log($items, $user);
    }
}
