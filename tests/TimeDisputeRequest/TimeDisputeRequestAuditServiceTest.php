<?php

namespace Tests\TimeDisputeRequest;

use App\Audit\AuditService;
use App\TimeDisputeRequest\TimeDisputeRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TimeDisputeRequestAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => TimeDisputeRequestAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $timeDisputeRequestAuditService = new TimeDisputeRequestAuditService($mockAuditService);
        $timeDisputeRequestAuditService->logCreate($item);
    }
}
