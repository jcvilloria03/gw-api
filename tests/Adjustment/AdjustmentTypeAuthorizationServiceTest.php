<?php

namespace Tests\Adjustment;

use Tests\Common\CommonAuthorizationService;

class AdjustmentAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Adjustment\AdjustmentAuthorizationService::class;
}
