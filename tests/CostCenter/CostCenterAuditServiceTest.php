<?php

namespace Tests\CostCenter;

use App\Audit\AuditService;
use App\CostCenter\CostCenterAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CostCenterAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
        ]);
        $item = [
            'action' => CostCenterAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new CostCenterAuditService($mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => CostCenterAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $costCenterAuditService = new CostCenterAuditService($mockAuditService);
        $costCenterAuditService->logDelete($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description'
        ]);
        $item = [
            'action' => CostCenterAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $costCenterAuditService = new CostCenterAuditService($mockAuditService);
        $costCenterAuditService->logUpdate($item);
    }
}
