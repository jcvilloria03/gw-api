<?php

namespace Tests\CostCenter;

use App\CostCenter\CostCenterAuthorizationService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

//TODO: Review this.  WHY IS THE Class under test being mocked here?

class CostCenterAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersPassAccountLevel()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testGetCompanyCostCentersInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyCostCenters($targetCostCenterDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailableNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCostCenterDetails, $user));
    }

    public function testDeleteCostCenterCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::DELETE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCostCenterDetails, $user));
    }

    public function testDeleteCostCenterAccountLevelSpecificFail()
    {
        $taskScope = new TaskScopes(CostCenterAuthorizationService::DELETE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetCostCenterDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2,
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CostCenterAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCostCenterDetails, $user));
    }
}
