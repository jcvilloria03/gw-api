<?php

namespace Tests\CostCenter;

use App\CostCenter\CostCenterEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CostCenterESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $costCenterIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')->once();

        $costCentersESIndexQueueService = new CostCenterEsIndexQueueService($mockIndexQueueService);
        $costCentersESIndexQueueService->queue($costCenterIds);
    }
}
