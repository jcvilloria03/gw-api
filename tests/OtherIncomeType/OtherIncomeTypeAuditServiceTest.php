<?php

namespace Tests\OtherIncomeType;

use App\Audit\AuditService;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class OtherIncomeTypeAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => OtherIncomeTypeAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
            'type' => 'App\Model\OtherIncomeType'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $otherIncomeTypeAuditService = new OtherIncomeTypeAuditService(
            $mockAuditService
        );
        $otherIncomeTypeAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'name' => 'Old name',
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'New name',
        ]);
        $item = [
            'action' => OtherIncomeTypeAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
            'type' => 'App\Model\OtherIncomeType'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new OtherIncomeTypeAuditService($mockAuditService);
        $employeeAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1
        ]);
        $item = [
            'action' => OtherIncomeTypeAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData,
            'type' => 'App\Model\OtherIncomeType'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $otherIncomeTypeAuditService = new OtherIncomeTypeAuditService($mockAuditService);
        $otherIncomeTypeAuditService->logDelete($item);
    }
}
