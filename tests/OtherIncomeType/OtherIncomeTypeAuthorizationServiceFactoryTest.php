<?php

namespace Tests\OtherIncomeType;

use App\AllowanceType\AllowanceTypeAuthorizationService;
use App\BonusType\BonusTypeAuthorizationService;
use App\CommissionType\CommissionTypeAuthorizationService;
use App\DeductionType\DeductionTypeAuthorizationService;
use App\OtherIncomeType\OtherIncomeTypeAuthorizationServiceFactory;
use PHPUnit\Framework\TestCase;

class OtherIncomeTypeAuthorizationServiceFactoryTest extends TestCase
{

    public function testGet()
    {
        $result = OtherIncomeTypeAuthorizationServiceFactory::get(
            OtherIncomeTypeAuthorizationServiceFactory::ALLOWANCE_TYPE
        );
        $this->assertInstanceOf(AllowanceTypeAuthorizationService::class, $result);

        $result = OtherIncomeTypeAuthorizationServiceFactory::get(
            OtherIncomeTypeAuthorizationServiceFactory::BONUS_TYPE
        );
        $this->assertInstanceOf(BonusTypeAuthorizationService::class, $result);

        $result = OtherIncomeTypeAuthorizationServiceFactory::get(
            OtherIncomeTypeAuthorizationServiceFactory::COMMISSION_TYPE
        );
        $this->assertInstanceOf(CommissionTypeAuthorizationService::class, $result);

        $result = OtherIncomeTypeAuthorizationServiceFactory::get(
            OtherIncomeTypeAuthorizationServiceFactory::DEDUCTION_TYPE
        );
        $this->assertInstanceOf(DeductionTypeAuthorizationService::class, $result);

        $this->expectException(\Exception::class);
        OtherIncomeTypeAuthorizationServiceFactory::get(
            'something wrong'
        );
    }
}
