<?php

namespace Tests\OtherIncomeType;

use App\OtherIncomeType\OtherIncomeTypeRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class OtherIncomeTypeRequestServiceTest extends TestCase
{
    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->get(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1);
    }

    public function testGetAllCompanyIncomeTypes()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getAllCompanyIncomeTypes(1, 'bonus_type');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getAllCompanyIncomeTypes(1, 'bonus_type');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getAllCompanyIncomeTypes(1, 'bonus_type');
    }

    public function testIsDeleteAvailable()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->IsDeleteAvailable(1, [ 1 ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);
    }

    public function testDelete()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_NO_CONTENT, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeTypeRequestService($client);

        // test Response::HTTP_NO_CONTENT
        $response = $requestService->deleteMultiple(1, [ 1 ]);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $response = $requestService->deleteMultiple(1, [ 1 ]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->deleteMultiple(1, [ 1 ]);
    }

    public function testIsEditAvailable()
    {
        $handler = HandlerStack::create(new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]));

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->isEditAvailable([
            'other_income_type_id' => 1
        ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->isEditAvailable([
            'other_income_type_id' => 1
        ]);
    }
}
