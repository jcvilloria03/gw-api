<?php

namespace Tests\Attachment;

use App\Audit\AuditService;
use App\Attachment\AttachmentAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class AttachmentAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => '1_12433adsa',
            'type' => 'png',
            'path' => 'test/path',
            'request_id' => 1,
        ]);
        $item = [
            'action' => AttachmentAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $attachmentAuditService = new AttachmentAuditService($mockAuditService);
        $attachmentAuditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1
        ]);
        $item = [
            'action' => AttachmentAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $attachmentAuditService = new AttachmentAuditService($mockAuditService);
        $attachmentAuditService->log($item);
    }
}
