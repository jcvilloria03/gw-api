<?php

namespace Tests\Company;

use App\Company\CompanyAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class CompanyAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => $accountId
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetNoScope()
    {
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetInvalidAccountScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompanyDetails, $user));
    }

    public function testCreatePass()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetAccountId, 1));
    }

    public function testCreateNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate(1, 1));
    }

    public function testCreateInvalidScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAccountId, 1));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => $accountId
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdateNoScope()
    {
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testUpdateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Company\CompanyAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompanyDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }


    public function testDeleteNoScope()
    {
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(CompanyAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            CompanyAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCompanyDetails, $user));
    }
}
