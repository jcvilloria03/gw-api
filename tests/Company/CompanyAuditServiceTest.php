<?php

namespace Tests\Company;

use App\Audit\AuditService;
use App\Company\CompanyAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CompanyAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => CompanyAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new CompanyAuditService($mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'name' => 'Old name',
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'New name',
        ]);
        $item = [
            'action' => CompanyAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new CompanyAuditService($mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1
        ]);
        $item = [
            'action' => CompanyAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $companyAuditService = new CompanyAuditService($mockAuditService);
        $companyAuditService->logDelete($item);
    }
}
