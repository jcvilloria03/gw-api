<?php

namespace Tests\Company;

use App\Company\CompanyRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyRequestServiceTest extends \Tests\TestCase
{
    public function testGet()
    {
        $data = json_encode(['account_id' => 1]);
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [], $data),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new CompanyRequestService($client);

        Redis::shouldReceive('hGet')
            ->times(2)
            ->shouldReceive('hSet')
            ->once();

        // test Response::HTTP_OK
        $response = $requestService->getAccountId(1);
        $this->assertNotEmpty($response);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getAccountId(1);
    }

    public function testGetCacheExists()
    {
        $data = json_encode(['account_id' => 1]);
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [], $data)
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new CompanyRequestService($client);


        Redis::shouldReceive('hGet')
            ->once()
            ->andReturn(1)
            ->shouldReceive('hSet')
            ->never();
        $requestService->getAccountId(1);
    }
}
