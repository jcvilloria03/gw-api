<?php

namespace Tests\Workflow;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Workflow\WorkflowAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class WorkflowAuthorizationServiceTest extends \Tests\TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetWorkflowDetails, $user));
    }

    public function testGetCompanyWorkflowsPassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testGetCompanyWorkflowsInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyWorkflows($targetWorkflowDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetWorkflowDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetWorkflowDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailableNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(WorkflowAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetWorkflowDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            WorkflowAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetWorkflowDetails, $user));
    }
}
