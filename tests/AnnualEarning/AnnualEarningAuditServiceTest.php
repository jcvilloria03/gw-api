<?php

namespace Tests\AnnualEarning;

use App\AnnualEarning\AnnualEarningRequestService;
use App\Audit\AuditService;
use App\AnnualEarning\AnnualEarningAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class AnnualEarningAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => AnnualEarningAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
            'type' => 'App\Model\AnnualEarning'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockRequestService = m::mock(AnnualEarningRequestService::class);

        $annualEarningAuditService = new AnnualEarningAuditService(
            $mockAuditService,
            $mockRequestService
        );
        $annualEarningAuditService->logCreate($item);
    }
}
