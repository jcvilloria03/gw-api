<?php

namespace Tests\AnnualEarning;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use App\AnnualEarning\AnnualEarningAuthorizationService;
use Tests\Authorization\AuthorizationServiceTestTrait;

class AnnualEarningAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }



    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetAnnualEarningsDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $companyId = 1;
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AnnualEarningAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAnnualEarningsDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AnnualEarningAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetAnnualEarningsDetails, $user));
    }
}
