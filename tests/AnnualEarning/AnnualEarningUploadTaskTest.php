<?php

namespace Tests\AnnualEarning;

use App\AnnualEarning\AnnualEarningUploadTask;
use App\AnnualEarning\AnnualEarningUploadTaskException;
use Illuminate\Support\Facades\Redis;
use Mockery as m;

class AnnualEarningUploadTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        $this->assertInstanceOf(AnnualEarningUploadTask::class, $task);
    }

    public function testCreateNoJobId()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($companyId);

        $idPrefix = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateWithJobIdExistingJob()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($companyId, $jobId);

        $this->assertEquals($task->getId(), $jobId);
    }

    public function testCreateWithJobIdNonExistingJob()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $this->setExpectedException(AnnualEarningUploadTaskException::class);
        $task->create($companyId, $jobId);
    }

    public function testIsCompanyIdSameValidNewJob()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($companyId);
        $this->assertTrue($task->isCompanyIdSame());
    }

    public function testIsCompanyIdSameValidExistingJob()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($companyId, $jobId);
        $this->assertTrue($task->isCompanyIdSame());
    }

    public function testIsCompanyIdSameInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . 1 . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->never();

        $this->setExpectedException(AnnualEarningUploadTaskException::class);
        $task->create($companyId, $jobId);
        $this->assertFalse($task->isCompanyIdSame());
    }

    public function testGetS3Bucket()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        $this->assertNotEmpty($task->getS3Bucket());
    }

    public function testSaveFile()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId);
        $s3Key = $task->saveFile('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  AnnualEarningUploadTask::ID_PREFIX . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testUpdateValidationStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);
        $this->setExpectedException(AnnualEarningUploadTaskException::class);
        $task->updateValidationStatus('invalid status');
    }

    public function testUpdateValidationStatusPersonal()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusPayrollLoan()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToQueued()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => AnnualEarningUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToValidating()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATING);
    }

    public function testUpdateValidationStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_VALIDATING,
            AnnualEarningUploadTask::STATUS_VALIDATED,
            AnnualEarningUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATING);
    }

    public function testUpdateValidationStatusToValidated()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => AnnualEarningUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATED);
    }

    public function testUpdateValidationStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED,
            AnnualEarningUploadTask::STATUS_VALIDATED,
            AnnualEarningUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATED);
    }

    public function testUpdateValidationStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => AnnualEarningUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_FAILED);
    }

    public function testUpdateValidationStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED,
            AnnualEarningUploadTask::STATUS_VALIDATED,
            AnnualEarningUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(AnnualEarningUploadTask::STATUS_VALIDATION_FAILED);
    }

    public function testUpdateSaveStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);
        $this->setExpectedException(AnnualEarningUploadTaskException::class);
        $task->updateSaveStatus('invalid status');
    }

    public function testUpdateSaveStatusValid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_VALIDATING,
            AnnualEarningUploadTask::STATUS_VALIDATION_QUEUED,
            AnnualEarningUploadTask::STATUS_VALIDATION_FAILED,
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(AnnualEarningUploadTaskException::class);
        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueued()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => AnnualEarningUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToValidating()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => AnnualEarningUploadTask::STATUS_SAVE_QUEUED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_SAVING,
            AnnualEarningUploadTask::STATUS_SAVED,
            AnnualEarningUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidated()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => AnnualEarningUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_SAVE_QUEUED,
            AnnualEarningUploadTask::STATUS_SAVED,
            AnnualEarningUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => AnnualEarningUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateSaveStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            AnnualEarningUploadTask::STATUS_SAVE_QUEUED,
            AnnualEarningUploadTask::STATUS_SAVED,
            AnnualEarningUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => AnnualEarningUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateErrorFileLocation()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $validProcesses = [
            AnnualEarningUploadTask::PROCESS_VALIDATION,
            AnnualEarningUploadTask::PROCESS_SAVE,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['blah']);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateErrorFileLocation($validProcesses[array_rand($validProcesses)], 'key');
    }

    public function testFetch()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->times(2)
            ->andReturn(['a' => 'b'], []);

        $task->create($companyId, $jobId);
        $task->fetch();
    }

    public function testFetchSelectedFields()
    {
        $companyId = 999;
        $jobId = AnnualEarningUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hMGet')
            ->once();

        $task->create($companyId, $jobId);
        $task->fetch(['field1', 'field2']);
    }

    public function testFetchErrorFileFromS3Invalid()
    {
        // empty result and no result body should return null
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->times(2)
            ->andReturn([], ['Header' => 'haha']);
        $task = new AnnualEarningUploadTask($mockS3Client);

        $this->assertNull($task->fetchErrorFileFromS3('key'));
        $this->assertNull($task->fetchErrorFileFromS3('key'));
    }

    public function testFetchErrorFileFromS3Valid()
    {
        // empty result and no result body should return null
        $mockStream = new class() {
            public function getContents()
            {
                return '{"a" : 1}';
            }
        };
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->once()
            ->andReturn(['Body' => $mockStream]);
        $task = new AnnualEarningUploadTask($mockS3Client);

        $this->assertEquals(["a" => 1], $task->fetchErrorFileFromS3('key'));
    }

    public function testSetUserId()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->setUserId(1);
    }

    public function testGetUserId()
    {
        $userId = 2;
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AnnualEarningUploadTask($mockS3Client);

        Redis::shouldReceive('hGet')
            ->once()
            ->andReturn($userId);

        $this->assertEquals($userId, $task->getUserId());
    }
}
