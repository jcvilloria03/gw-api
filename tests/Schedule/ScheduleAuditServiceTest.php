<?php

namespace Tests\Schedule;

use App\Audit\AuditService;
use App\Schedule\ScheduleAuditService;
use App\Schedule\ScheduleRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ScheduleAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => ScheduleAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockRequestService = m::mock(ScheduleRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new ScheduleAuditService($mockRequestService, $mockAuditService);
        $scheduleAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => ScheduleAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockRequestService = m::mock(ScheduleRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new ScheduleAuditService($mockRequestService, $mockAuditService);
        $scheduleAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => ScheduleAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock(ScheduleRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new ScheduleAuditService($mockRequestService, $mockAuditService);
        $scheduleAuditService->logDelete($item);
    }
}
