<?php

namespace Tests\Schedule;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Schedule\ScheduleAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class ScheduleAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetScheduleDetails, $user));
    }

    public function testGetCompanySchedulesPassAccountLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testGetCompanySchedulesInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::VIEW_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanySchedules($targetScheduleDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::CREATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetScheduleDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $accountId = 1;
        $companyId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::UPDATE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetScheduleDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $accountId = 1;
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(ScheduleAuthorizationService::DELETE_TASK);
        $targetScheduleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            ScheduleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetScheduleDetails, $user));
    }
}
