<?php

namespace Tests\UndertimeRequest;

use App\Audit\AuditService;
use App\UndertimeRequest\UndertimeRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class UndertimeRequestAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => UndertimeRequestAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $undertimeRequestAuditService = new UndertimeRequestAuditService($mockAuditService);
        $undertimeRequestAuditService->logCreate($item);
    }
}
