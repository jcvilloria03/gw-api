<?php

namespace Tests\Role;

use \Mockery as m;
use App\Model\Role;
use App\Model\Task;
use App\Model\UserRole;
use App\Model\Permission;
use App\Permission\Scope;
use App\Role\UserRoleService;
use App\Permission\TargetType;
use App\Role\DefaultRoleService;
use Illuminate\Support\Collection;
use Salarium\Cache\FragmentedRedisCache;
use App\Authorization\AuthorizationService;
use App\Permission\AuthorizationPermissions;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthorizationServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    protected $task1;
    protected $task2;

    private function createTask($taskName)
    {
        return Task::create([
            'name' => $taskName,
            'display_name' => 'display_name',
            'description' => 'description',
            'module' => 'module',
            'submodule' => 'submodule',
            'allowed_scopes' => '["Account", "Company"]',
        ]);
    }

    private function createRole(string $roleName)
    {
        return Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => $roleName,
            'module_access' => ['HRIS']
        ]);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function createRoleAndPermissions()
    {
        $task1 = $this->createTask('create.something');
        $task2 = $this->createTask('update.something');
        $task3 = Task::create([
            'name' => 'test.module_access',
            'display_name' => 'display_name',
            'description' => 'description',
            'module' => 'Payroll',
            'submodule' => 'submodule',
            'allowed_scopes' => '["Account", "Company"]',
        ]);
        // create roles with overlapping permissions
        $role1 = $this->createRole('role1');
        $role2 = $this->createRole('role2');
        $role3 = $this->createRole('role3');
        $role4 = $this->createRole('role4');

        // insert permissions for role1
        Permission::insert([
            [
                'scope' => json_encode([
                    TargetType::ACCOUNT => SCOPE::TARGET_ALL
                ]),
                'task_id' => $task1->id,
                'role_id' => $role1->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::COMPANY => [1,2]
                ]),
                'task_id' => $task1->id,
                'role_id' => $role1->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::PAYROLL_GROUP => [1,2]
                ]),
                'task_id' => $task1->id,
                'role_id' => $role1->id,
            ],
        ]);
        // insert permissions for role2
        Permission::insert([
            [
                'scope' => json_encode([
                    TargetType::COMPANY => SCOPE::TARGET_ALL
                ]),
                'task_id' => $task1->id,
                'role_id' => $role2->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::PAYROLL_GROUP => [2,3]
                ]),
                'task_id' => $task1->id,
                'role_id' => $role2->id,
            ],
        ]);
        // insert permissions for role3
        Permission::insert([
            [
                'scope' => json_encode([
                    TargetType::ACCOUNT => SCOPE::TARGET_ALL
                ]),
                'task_id' => $task2->id,
                'role_id' => $role3->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::COMPANY => [1,2]
                ]),
                'task_id' => $task2->id,
                'role_id' => $role3->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::PAYROLL_GROUP => [1,2]
                ]),
                'task_id' => $task2->id,
                'role_id' => $role3->id,
            ],
            [
                'scope' => json_encode((object) null),
                'task_id' => $task2->id,
                'role_id' => $role3->id,
            ],
        ]);
        // insert permissions for role4
        Permission::insert([
            [
                'scope' => json_encode([
                    TargetType::COMPANY => [3, 1]
                ]),
                'task_id' => $task2->id,
                'role_id' => $role4->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::PAYROLL_GROUP => [2,3,4]
                ]),
                'task_id' => $task1->id,
                'role_id' => $role4->id,
            ],
        ]);

        // insert permissions for role4
        Permission::insert([
            [
                'scope' => json_encode([
                    TargetType::COMPANY => [3, 1]
                ]),
                'task_id' => $task3->id,
                'role_id' => $role4->id,
            ],
            [
                'scope' => json_encode([
                    TargetType::PAYROLL_GROUP => [2,3,4]
                ]),
                'task_id' => $task3->id,
                'role_id' => $role4->id,
            ],
        ]);

        $userRole1 = new UserRole(['role_id' => $role1->id]);
        $userRole1->setRelation('role', $role1);

        $userRole2 = new UserRole(['role_id' => $role2->id]);
        $userRole2->setRelation('role', $role2);

        $userRole3 = new UserRole(['role_id' => $role3->id]);
        $userRole3->setRelation('role', $role3);

        $userRole4 = new UserRole(['role_id' => $role4->id]);
        $userRole4->setRelation('role', $role4);

        return collect([$userRole1, $userRole2, $userRole3, $userRole4]);
    }

    private function getCachedPermissions()
    {
        return [
            "create.something"=> [
              "scopes"=> [
                [
                  "type"=> "Account",
                  "target"=> "all"
                ],
                [
                  "type"=> "Company",
                  "target"=> "all"
                ],
                [
                  "type"=> "Payroll Group",
                  "target"=> [
                    1,
                    2,
                    3,
                    4
                  ]
                ]
              ],
              "modules"=> [
                "HRIS"
              ]
            ],
            "update.something"=> [
              "scopes"=> [
                [
                  "type"=> "Account",
                  "target"=> "all"
                ],
                [
                  "type"=> "Company",
                  "target"=> [
                    1,
                    2,
                    3
                  ]
                ],
                [
                  "type"=> "Payroll Group",
                  "target"=> [
                    1,
                    2
                  ]
                ]
              ],
              "modules"=> [
                "HRIS"
              ]
              ],
              "test.module_access"=> [
                "scopes"=> [
                  [
                    "type"=> "Account",
                    "target"=> "all"
                  ],
                  [
                    "type"=> "Company",
                    "target"=> [
                      1,
                      2,
                      3
                    ]
                  ],
                  [
                    "type"=> "Payroll Group",
                    "target"=> [
                      1,
                      2
                    ]
                  ]
                ],
                "modules"=> []
              ]
        ];
    }

    public function testInit()
    {
        $mockUserRoleService = m::mock('App\Role\UserRoleService')
            ->shouldReceive('getUserRoles')
            ->once()
            ->andReturn($this->createRoleAndPermissions())
            ->getMock();

        $mockCacheService = m::mock(FragmentedRedisCache::class)
            ->shouldReceive('setPrefix')
            ->once()
            ->shouldReceive('setHashFieldPrefix')
            ->once()
            ->shouldReceive('exists')
            ->once()
            ->andReturn(false)
            ->shouldReceive('set')
            ->once()
            ->getMock();


        $authorizationPermissions = new AuthorizationPermissions();

        $authorizationService = new AuthorizationService(
            $mockUserRoleService,
            $authorizationPermissions,
            $mockCacheService
        );
        $authorizationService->buildUserPermissions(1);

        // check 'create.something' task scopes
        $taskScopes = $authorizationService->getTaskScopes('create.something');
        $this->assertNotNull($taskScopes);
        $this->assertEquals(3, count($taskScopes->getScopes()));
        $scope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::ACCOUNT, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::COMPANY, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::PAYROLL_GROUP, $scope->getType());
        $this->assertEquals([1, 2, 3, 4], $scope->getTarget());

        // check 'update.something' task scopes
        $taskScopes = $authorizationService->getTaskScopes('update.something');
        $this->assertNotNull($taskScopes);
        $this->assertEquals(3, count($taskScopes->getScopes()));
        $scope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::ACCOUNT, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::COMPANY, $scope->getType());
        $this->assertEquals([1, 2, 3], $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::PAYROLL_GROUP, $scope->getType());
        $this->assertEquals([1, 2], $scope->getTarget());

        // check getMultipleTaskScopes function

        $taskScopes = $authorizationService->getMultipleTaskScopes([
            'create.something',
            'update.something'
        ]);
        $this->assertEquals(2, count($taskScopes));
    }

    public function testBuildPermisionsFromCache()
    {
        $mockUserRoleService = m::mock('App\Role\UserRoleService')
            ->shouldReceive('getUserRoles')
            ->never()
            ->getMock();

        $mockCacheService = m::mock(FragmentedRedisCache::class)
            ->shouldReceive('setPrefix')
            ->once()
            ->shouldReceive('setHashFieldPrefix')
            ->once()
            ->shouldReceive('exists')
            ->once()
            ->andReturn(true)
            ->shouldReceive('get')
            ->andReturn($this->getCachedPermissions())
            ->getMock();


        $authorizationPermissions = new AuthorizationPermissions();

        $authorizationService = new AuthorizationService(
            $mockUserRoleService,
            $authorizationPermissions,
            $mockCacheService
        );
        $authorizationService->buildUserPermissions(1);

        // check 'create.something' task scopes
        $taskScopes = $authorizationService->getTaskScopes('create.something');
        $this->assertNotNull($taskScopes);
        $this->assertEquals(3, count($taskScopes->getScopes()));
        $scope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::ACCOUNT, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::COMPANY, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::PAYROLL_GROUP, $scope->getType());
        $this->assertEquals([1, 2, 3, 4], $scope->getTarget());

        // check 'update.something' task scopes
        $taskScopes = $authorizationService->getTaskScopes('update.something');
        $this->assertNotNull($taskScopes);
        $this->assertEquals(3, count($taskScopes->getScopes()));
        $scope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::ACCOUNT, $scope->getType());
        $this->assertEquals(SCOPE::TARGET_ALL, $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::COMPANY, $scope->getType());
        $this->assertEquals([1, 2, 3], $scope->getTarget());
        $scope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        $this->assertNotNull($scope);
        $this->assertEquals(TargetType::PAYROLL_GROUP, $scope->getType());
        $this->assertEquals([1, 2], $scope->getTarget());

        // check getMultipleTaskScopes function

        $taskScopes = $authorizationService->getMultipleTaskScopes([
            'create.something',
            'update.something'
        ]);
        $this->assertEquals(2, count($taskScopes));
    }

    public function testCheckTaskModuleAccessReturnsFalseOnEmptyTask()
    {
        $mockUserRoleService = m::mock('App\Role\UserRoleService')
            ->shouldReceive('getUserRoles')
            ->never()
            ->getMock();

        $mockCacheService = m::mock(FragmentedRedisCache::class)
            ->shouldReceive('setPrefix')
            ->once()
            ->shouldReceive('setHashFieldPrefix')
            ->once()
            ->shouldReceive('exists')
            ->once()
            ->andReturn(true)
            ->shouldReceive('get')
            ->andReturn($this->getCachedPermissions())
            ->getMock();


        $authorizationPermissions = new AuthorizationPermissions();

        $authorizationService = new AuthorizationService(
            $mockUserRoleService,
            $authorizationPermissions,
            $mockCacheService
        );
        $authorizationService->buildUserPermissions(1);

        $this->assertFalse($authorizationService->checkTaskModuleAccess('missing.task'));
    }

    public function testCheckTaskModuleAccessReturnsFalseOnEmptyTaskModuleScope()
    {
        $mockUserRoleService = m::mock('App\Role\UserRoleService')
            ->shouldReceive('getUserRoles')
            ->never()
            ->getMock();

        $mockCacheService = m::mock(FragmentedRedisCache::class)
            ->shouldReceive('setPrefix')
            ->once()
            ->shouldReceive('setHashFieldPrefix')
            ->once()
            ->shouldReceive('exists')
            ->once()
            ->andReturn(true)
            ->shouldReceive('get')
            ->andReturn($this->getCachedPermissions())
            ->getMock();


        $authorizationPermissions = new AuthorizationPermissions();

        $authorizationService = new AuthorizationService(
            $mockUserRoleService,
            $authorizationPermissions,
            $mockCacheService
        );
        $authorizationService->buildUserPermissions(1);

        $this->assertFalse($authorizationService->checkTaskModuleAccess('test.module_access'));
    }

    public function testCheckTaskModuleAccessReturnsTrueOnNotEmptyTaskModuleScope()
    {
        $mockUserRoleService = m::mock('App\Role\UserRoleService')
            ->shouldReceive('getUserRoles')
            ->never()
            ->getMock();

        $mockCacheService = m::mock(FragmentedRedisCache::class)
            ->shouldReceive('setPrefix')
            ->once()
            ->shouldReceive('setHashFieldPrefix')
            ->once()
            ->shouldReceive('exists')
            ->once()
            ->andReturn(true)
            ->shouldReceive('get')
            ->andReturn($this->getCachedPermissions())
            ->getMock();


        $authorizationPermissions = new AuthorizationPermissions();

        $authorizationService = new AuthorizationService(
            $mockUserRoleService,
            $authorizationPermissions,
            $mockCacheService
        );
        $authorizationService->buildUserPermissions(1);

        $this->assertTrue($authorizationService->checkTaskModuleAccess('create.something'));
        $this->assertTrue($authorizationService->checkTaskModuleAccess('update.something'));
    }
}
