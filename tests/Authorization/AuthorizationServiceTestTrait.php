<?php

namespace Tests\Authorization;

use Mockery as m;
use App\Permission\TaskScopes;
use Salarium\Cache\FragmentedRedisCache;
use App\Permission\AuthorizationPermissions;

trait AuthorizationServiceTestTrait
{


    /**
     * Create a mock authorization service,
     * and specify the task scopes to inject.
     * This assumes that the task scopes
     * are for the expected Task of the service.
     *
     * @param string $serviceName Class name of AuthorizationService subclass to make
     * @param App\Permission\TaskScopes $mockTaskScopes TaskScope objects to inject
     */
    protected function createMockAuthorizationService(
        string $serviceName,
        TaskScopes $mockTaskScopes = null,
        $mockModuleAccess = true
    ) {
        $mockThis                 = $serviceName . '[buildUserPermissions, getTaskScopes, checkTaskModuleAccess]';
        $mockUserRoleService      = m::mock('App\Role\UserRoleService');
        $authorizationPermissions = new AuthorizationPermissions();
        $cacheService             = new FragmentedRedisCache();

        if (!empty($mockTaskScopes)) {
            $authorizationPermissions->addTaskScope($mockTaskScopes);
        }

        $mockAuthorizationService = m::mock(
            $mockThis,
            [
                $mockUserRoleService,
                $authorizationPermissions,
                $cacheService
            ]
        );

        $mockAuthorizationService
            ->shouldReceive('buildUserPermissions')
            ->once();

        $mockAuthorizationService
            ->shouldReceive('getTaskScopes')
            ->andReturn($mockTaskScopes);

        $mockAuthorizationService
            ->shouldReceive('checkTaskModuleAccess')
            ->andReturn($mockModuleAccess);

        return $mockAuthorizationService;
    }
}
