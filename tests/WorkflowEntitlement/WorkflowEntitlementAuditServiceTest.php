<?php

namespace Tests\WorkflowEntitlement;

use App\Audit\AuditService;
use App\WorkflowEntitlement\WorkflowEntitlementAuditService;
use App\WorkflowEntitlement\WorkflowEntitlementRequestService;
use Illuminate\Http\JsonResponse;
use Mockery as m;

class WorkflowEntitlementAuditServiceTest extends \Tests\TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => WorkflowEntitlementAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $workflowEntitlementAuditService = new WorkflowEntitlementAuditService($mockAuditService);
        $workflowEntitlementAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => WorkflowEntitlementAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $workflowEntitlementAuditService = new WorkflowEntitlementAuditService($mockAuditService);
        $workflowEntitlementAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => WorkflowEntitlementAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $workflowEntitlementAuditService = new WorkflowEntitlementAuditService($mockAuditService);
        $workflowEntitlementAuditService->logDelete($item);
    }

    public function testLogUploadedWorkflowEntitlements()
    {
        $user = [
            'id' => 1,
            'account_id' => 1
        ];

        $workflowEntitlements = [
            'created' => [
                [
                    'id' => 1,
                    'workflow_id' => 1,
                    'employee_id' => 1,
                    'request_type_id' => 1
                ]
            ],
            'updated' => [
                [
                    'id' => 2,
                    'workflow_id' => 2,
                    'employee_id' => 2,
                    'request_type_id' => 2
                ]
            ],
            'deleted' => [
                [
                    'id' => 3,
                    'workflow_id' => 3,
                    'employee_id' => 3,
                    'request_type_id' => 3
                ]
            ]
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log');

        $mockWorkflowEntitlementRequestService = m::mock(WorkflowEntitlementRequestService::class);
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'full_name' => 'Test name'
            ]
        ]));
        $mockWorkflowEntitlementRequestService
            ->shouldReceive('get')
            ->andReturn($response);

        $workflowEntitlementAuditService = new WorkflowEntitlementAuditService(
            $mockAuditService,
            $mockWorkflowEntitlementRequestService
        );

        $workflowEntitlementAuditService->logUploadedWorkflowEntitlements($workflowEntitlements, $user);
    }
}
