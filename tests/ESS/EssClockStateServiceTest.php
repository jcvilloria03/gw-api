<?php

namespace Tests\ESS;

use App\ESS\EssClockStateService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssClockStateServiceTest extends \Tests\TestCase
{
    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        $response = $essClockStateService->get(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $essClockStateService->get(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $essClockStateService->get(1);
    }

    public function testGetTimesheet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        $response = $essClockStateService->getTimesheet(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $essClockStateService->getTimesheet(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $essClockStateService->getTimesheet(1);
    }

    public function testGetTimesheetWithTimestampParams()
    {
        $query = [
            'start_timestamp' => 2313123123,
            'end_timestamp' => 2313123123,
        ];

        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        $response = $essClockStateService->getTimesheet(1, $query);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $essClockStateService->getTimesheet(1, $query);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $essClockStateService->getTimesheet(1, $query);
    }

    public function testLog()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        $response = $essClockStateService->log(1, 1, ['state' => 1]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $essClockStateService->log(1, 1, ['state' => 1]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $essClockStateService->log(1, 1, ['state' => 1]);
    }

    public function testGetTeamAttendance()
    {
        $clockOut = [
            [
                'employee_uid' => 2,
                'company_uid' => 1,
                'status' => false,
                'tags' => [],
                'timestamp' => 123456789,
            ]
        ];
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, ['Content-Type' => 'application/json'], json_encode($clockOut)),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        // Current employee is excluded
        $this->assertEquals($essClockStateService->getTeamAttendance([1, 2], 1), $clockOut);
    }

    public function testGetTeamAttendanceInvalid()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $essClockStateService = new EssClockStateService($client);

        $this->assertEquals($essClockStateService->getTeamAttendance([3], 1), []);
    }
}
