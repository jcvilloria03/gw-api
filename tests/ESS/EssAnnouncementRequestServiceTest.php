<?php

namespace Tests\ESS;

use App\ESS\EssAnnouncementRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssAnnouncementRequestServiceTest extends TestCase
{
    public function testCreate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->create([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->create([]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->create([]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->create([]);
    }

    public function testGetUserRoleForAnnouncement()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getUserRoleForAnnouncement(1, 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getUserRoleForAnnouncement(1, 1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getUserRoleForAnnouncement(1, 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUserRoleForAnnouncement(1, 1);
    }

    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1, 'sender', 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->get(1, 'sender', 1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1, 'sender', 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->get(1, 'sender', 1);
    }

    public function testGetReply()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getReply(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getReply(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getReply(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getReply(1);
    }

    public function testGetUserAnnouncements()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getUserAnnouncements(1, [], 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getUserAnnouncements(1, [], 1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getUserAnnouncements(1, [], 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUserAnnouncements(1, [], 1);
    }

    public function testReply()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        $response = $requestService->reply(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->reply(1, []);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->reply(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->reply(1, []);
    }

    public function testRecipientSawAnnouncement()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->recipientSawAnnouncement(1, 1, '10.0.0.1');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->recipientSawAnnouncement(1, 1, '10.0.0.1');

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->recipientSawAnnouncement(1, 1, '10.0.0.1');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->recipientSawAnnouncement(1, 1, '10.0.0.1');
    }

    public function testMarkReplyAsSeen()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->markReplyAsSeen(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->markReplyAsSeen(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->markReplyAsSeen(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->markReplyAsSeen(1);
    }

    public function testGetUserUnreadAnnouncements()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new EssAnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getUserUnreadAnnouncements(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getUserUnreadAnnouncements(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getUserUnreadAnnouncements(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUserUnreadAnnouncements(1);
    }
}
