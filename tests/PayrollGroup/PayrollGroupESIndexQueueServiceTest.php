<?php

namespace Tests\PayrollGroup;

use App\PayrollGroup\PayrollGroupEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollGroupEsIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $payrollGroupIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')->once();

        $payrollGroupsESIndexQueueService = new PayrollGroupEsIndexQueueService($mockIndexQueueService);
        $payrollGroupsESIndexQueueService->queue($payrollGroupIds);
    }
}
