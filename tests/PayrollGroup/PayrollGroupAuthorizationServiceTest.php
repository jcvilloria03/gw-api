<?php

namespace Tests\PayrollGroup;

use App\PayrollGroup\PayrollGroupAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class PayrollGroupAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetInvalidCompanyLevelValidPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 3,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }


    public function testGetNoScope()
    {
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollGroupDetails, $user));
    }

    public function testAuthorizeGetCompanyPayrollGroupsPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsNoScope()
    {
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testAuthorizeGetCompanyPayrollGroupsInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $targetCompanyId = 1;
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyPayrollGroups(
            $targetAccountId,
            $targetCompanyId,
            $user
        ));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreateNoScope()
    {
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollGroupDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }


    public function testDeleteNoScope()
    {
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailableNoScope()
    {
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetPayrollGroupDetails, $user));
    }


    public function testAuthorizeGetAccountPayrollGroupsPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetAccountPayrollGroups($user));
    }

    public function testAuthorizeGetAccountPayrollGroupsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetAccountPayrollGroups($user));
    }

    public function testAuthorizeGetAccountPayrollGroupsCompanyLevelSpecificShouldFail()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetAccountPayrollGroups($user));
    }

    public function testAuthorizeGetAccountPayrollGroupsNoScope()
    {
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGetAccountPayrollGroups($user));
    }

    public function testAuthorizeGetAccountPayrollGroupsInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetAccountPayrollGroups($user));
    }

    public function testAuthorizeGetAccountPayrollGroupsInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetAccountPayrollGroups($user));
    }
    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdatePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }


    public function testUpdateNoScope()
    {
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }

    public function testUpdateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollGroupDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\PayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetPayrollGroupDetails, $user));
    }
}
