<?php

namespace Tests\PayrollGroup;

use App\PayrollGroup\EssPayrollGroupAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssPayrollGroupAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeView()
    {
        $user = ['user_id' => 1];
        $taskScope = new TaskScopes(EssPayrollGroupAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\EssPayrollGroupAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($user['user_id']));
    }

    public function testFailToAuthorizeView()
    {
        $user = ['user_id' => 1];
        $authorizationService = $this->createMockAuthorizationService(
            'App\PayrollGroup\EssPayrollGroupAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($user['user_id']));
    }

}
