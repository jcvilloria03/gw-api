<?php

namespace Tests\PayrollGroup;

use Mockery as m;
use App\Audit\AuditService;
use PHPUnit\Framework\TestCase;
use App\PayrollGroup\PayrollGroupAuditService;
use App\PayrollGroup\PayrollGroupRequestService;
use Illuminate\Http\JsonResponse;

class PayrollGroupAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => PayrollGroupAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $rankAuditService = new PayrollGroupAuditService($mockAuditService);
        $rankAuditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => PayrollGroupAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $rankAuditService = new PayrollGroupAuditService($mockAuditService);
        $rankAuditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'name' => 'Old name',
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'New name',
        ]);
        $item = [
            'action' => PayrollGroupAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new PayrollGroupAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogBatchCreate()
    {
        $user = [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];

        $items = [
            'action' => PayrollGroupAuditService::ACTION_BATCH_CREATE,
            'company_id' => 1,
            'user_id' => 1,
            'account_id' => 1,
            'payroll_groups_ids' => [1,2,3]
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log');

        $mockRequestService = m::mock(PayrollGroupRequestService::class);
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'full_name' => 'Test name'
            ]
        ]));
        $mockRequestService
            ->shouldReceive('get')
            ->andReturn($response);

        $loanRequestAuditService = new PayrollGroupAuditService(
            $mockAuditService,
            $mockRequestService
        );

        $loanRequestAuditService->log($items, $user);
    }
}
