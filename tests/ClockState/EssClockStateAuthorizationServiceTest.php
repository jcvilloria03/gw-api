<?php

namespace Tests\ClockState;

use App\ClockState\EssClockStateAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Mockery as m;

class EssClockStateAuthorizationServiceTest extends TestCase
{
    use Traits\EssClockStateAuthorizationServiceTestTrait;

    public function testAuthorizeViewTimeRecords()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssClockStateAuthorizationService::VIEW_TIME_RECORDS_TASK);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService'
        );

        $authorizationService
            ->shouldReceive('buildUserPermissions')
            ->once();
        $authorizationService
            ->shouldReceive('getTaskScopes')
            ->twice()
            ->andReturn($taskScope);

        $this->assertTrue($authorizationService->authorizeViewTimeRecords($user['user_id']));
    }

    public function testFailToAuthorizeApprove()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService'
        );

        $authorizationService
            ->shouldReceive('buildUserPermissions')
            ->once();
        $authorizationService
            ->shouldReceive('getTaskScopes')
            ->once();

        $this->assertFalse($authorizationService->authorizeViewTimeRecords($user['user_id']));
    }

    public function testAuthorizeLog()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssClockStateAuthorizationService::LOG_TASK);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService'
        );

        $authorizationService
            ->shouldReceive('buildUserPermissions')
            ->once();
        $authorizationService
            ->shouldReceive('getTaskScopes')
            ->twice()
            ->andReturn($taskScope);

        $this->assertTrue($authorizationService->authorizeLog($user['user_id']));
    }

    public function testFailToAuthorizeLog()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService'
        );

        $authorizationService
            ->shouldReceive('buildUserPermissions')
            ->once();
        $authorizationService
            ->shouldReceive('getTaskScopes')
            ->once();

        $this->assertFalse($authorizationService->authorizeLog($user['user_id']));
    }


    public function testValidateIPSuccessWithRestriction()
    {
        $user = [ 'employee_id' => 1 ];
        $fakeIpAddress = '192.168.0.1';
        $mockEmployeeInfo = [
            "time_attendance" => [
                "primary_location_id" => 9,
                "secondary_location_id" => null
            ],
            "location_id" => null
        ];

        $mockLocationInfo = [
            "ip_addresses" => [
                [
                    "ip_address" => "192.168.0.1"
                ],
                [
                    "ip_address" => "192.168.*.1"
                ]
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            json_encode($mockEmployeeInfo),
            json_encode($mockLocationInfo)
        );

        $this->assertTrue($authorizationService->isIPAllowed($user['employee_id'], $fakeIpAddress));
    }


    public function testValidateIPSuccessNoRestriction()
    {
        $user = [ 'employee_id' => 1 ];
        $fakeIpAddress = '192.168.0.1';
        $mockEmployeeInfo = [
            "time_attendance" => [
                "primary_location_id" => 9,
                "secondary_location_id" => null
            ],
            "location_id" => null
        ];

        $mockLocationInfo = [
            "ip_addresses" => []
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            json_encode($mockEmployeeInfo),
            json_encode($mockLocationInfo)
        );

        $this->assertTrue($authorizationService->isIPAllowed($user['employee_id'], $fakeIpAddress));
    }


    public function testValidateIPForbidden()
    {
        $user = [ 'employee_id' => 1 ];
        $fakeIpAddress = '192.168.0.1';
        $mockEmployeeInfo = [
            "time_attendance" => [
                "primary_location_id" => 9,
                "secondary_location_id" => null
            ],
            "location_id" => null
        ];

        $mockLocationInfo = [
            "ip_addresses" => [
                [
                    "ip_address" => "17.223.24.51"
                ],
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            json_encode($mockEmployeeInfo),
            json_encode($mockLocationInfo)
        );


        $this->assertFalse($authorizationService->isIPAllowed($user['employee_id'], $fakeIpAddress));
    }


    public function testValidateIPCorrectWildcardMatching()
    {
        $user = [ 'employee_id' => 1 ];
        $fakeIpAddress = '192.168.0.1';
        $mockEmployeeInfo = [
            "time_attendance" => [
                "primary_location_id" => 9,
                "secondary_location_id" => null
            ],
            "location_id" => null
        ];

        $mockLocationInfo = [
            "ip_addresses" => [
                [
                    "ip_address" => "192.*.0.1"
                ],
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            json_encode($mockEmployeeInfo),
            json_encode($mockLocationInfo)
        );

        $this->assertTrue($authorizationService->isIPAllowed($user['employee_id'], $fakeIpAddress));
    }


    public function testValidateIPIncorrectWildcardMatching()
    {
        $user = [ 'employee_id' => 1 ];
        $fakeIpAddress = '192.168.0.23';
        $mockEmployeeInfo = [
            "time_attendance" => [
                "primary_location_id" => 9,
                "secondary_location_id" => null
            ],
            "location_id" => null
        ];

        $mockLocationInfo = [
            "ip_addresses" => [
                [
                    "ip_address" => "192.*.168.0.1"
                ],
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            json_encode($mockEmployeeInfo),
            json_encode($mockLocationInfo)
        );

        $this->assertFalse($authorizationService->isIPAllowed($user['employee_id'], $fakeIpAddress));
    }

    public function testValidateTimeMethodsNoShift()
    {
        $user = [
            'employee_id' => 1,
            'employee_company_id' => 123
        ];

        $attributes = [
            'origin' => 'Web Bundy'
        ];

        $mockEmployeeShifts = [];

        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            null,
            null,
            json_encode($mockEmployeeShifts)
        );

        $this->assertTrue($authorizationService->validateTimeMethods($user, $attributes));
    }

    public function testValidateTimeMethodsWithShiftNotValid()
    {
        $user = [
            'employee_id' => 1,
            'employee_company_id' => 123
        ];

        $attributes = [
            'origin' => 'Web BundyXXXXXX'
        ];

        $mockEmployeeShifts = [
            'data' => [
                [
                    'schedule' => [
                        'allowed_time_methods' => ['Web Bundy']
                    ]
                ]
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            null,
            null,
            json_encode($mockEmployeeShifts)
        );

        $this->assertFalse($authorizationService->validateTimeMethods($user, $attributes));
    }


    public function testValidateTimeMethodsWithShiftValid()
    {
        $user = [
            'employee_id' => 1,
            'employee_company_id' => 123
        ];

        $attributes = [
            'origin' => 'Web Bundy'
        ];

        $mockEmployeeShifts = [
            'data' => [
                [
                    'schedule' => [
                        'allowed_time_methods' => ['Web Bundy']
                    ]
                ]
            ]
        ];
        $authorizationService = $this->createMockEssClockStateAuthorizationService(
            'App\ClockState\EssClockStateAuthorizationService',
            null,
            null,
            json_encode($mockEmployeeShifts)
        );

        $this->assertTrue($authorizationService->validateTimeMethods($user, $attributes));
    }

}
