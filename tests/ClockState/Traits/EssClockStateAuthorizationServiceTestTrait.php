<?php

namespace Tests\ClockState\Traits;

use Mockery as m;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use App\Employee\EmployeeRequestService;
use Salarium\Cache\FragmentedRedisCache;
use App\Permission\AuthorizationPermissions;

use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use App\Location\TimeAttendanceLocationRequestService as LocationRequestService;
use App\Shift\ShiftRequestService;

/**
 * @SuppressWarnings(PHPMD.UnusedPrivateField)
 */
trait EssClockStateAuthorizationServiceTestTrait
{


    /**
     * Create a mock ess clock state authorization service,
     * and specify the task scopes to inject.
     * This assumes that the task scopes
     * are for the expected Task of the service.
     *
     * @param string $serviceName Class name of AuthorizationService subclass to make
     * @param App\Permission\TaskScopes $mockTaskScopes TaskScope objects to inject
     */
    protected function createMockEssClockStateAuthorizationService(
        string $serviceName,
        string $mockEmployeeInfo = null,
        string $mockLocationInfo = null,
        string $mockShiftInfo = null
    ) {
        $mockThis = $serviceName . '[buildUserPermissions, getTaskScopes]';
        $mockUserRoleService = m::mock('App\Role\UserRoleService');
        $authorizationPermissions = new AuthorizationPermissions();
        $cacheService             = new FragmentedRedisCache();

        # Employee request service mock
        $mockEmployee = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [], $mockEmployeeInfo),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);
        $handlerEmployee = HandlerStack::create($mockEmployee);
        $clientEmployee = new Client(['handler' => $handlerEmployee]);
        $employeeRequestService = new EmployeeRequestService($clientEmployee);

        # Location service mock
        $mockLocation = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [], $mockLocationInfo),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handlerLocation = HandlerStack::create($mockLocation);

        $clientLocation = new Client(['handler' => $handlerLocation]);
        $locationService = new LocationRequestService($clientLocation);

        # Shift service mock
        $mockShift = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [], $mockShiftInfo),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);
        $handlerShift = HandlerStack::create($mockShift);
        $clientShift = new Client(['handler' => $handlerShift]);
        $shiftService = new ShiftRequestService($clientShift);

                
        $mockAuthorizationService = m::mock(
            $mockThis,
            [
                $mockUserRoleService,
                $authorizationPermissions,
                $cacheService,
                $employeeRequestService,
                $locationService,
                $shiftService
            ]
        );

        return $mockAuthorizationService;
    }
}
