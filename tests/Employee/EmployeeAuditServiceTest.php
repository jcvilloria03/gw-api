<?php

namespace Tests\Employee;

use App\Employee\EmployeeAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Mockery as m;

class EmployeeAuditServiceTest extends \Tests\TestCase
{
    public function testLogForEditAction()
    {
        $this->markTestSkipped('Deprecated.');
        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[logEdit]', [
            $mockRequestService,
            $mockAuditService,
        ]);

        $employeeAuditService->shouldReceive('logEdit')->once();

        $employeeAuditService->log(['action' => EmployeeAuditService::ACTION_EDIT]);
    }

    public function testLogForNonExistingAction()
    {
        $this->markTestSkipped('Deprecated.');
        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = new EmployeeAuditService($mockRequestService, $mockAuditService);

        $employeeAuditService->log(['action' => 'InvalidAction']);
    }

    public function testLogEdit()
    {
        $this->markTestSkipped('Deprecated.');
        $item = [
            'new' => json_encode([
                'id' => 1,
                'company_id' => 1,
                'first_name' => 'new first name',
                'last_name' => 'new last name',
                'middle_name' => 'middle name',
                'birth_date' => null,
                'address' => [
                    'address_line_1' => 'address',
                    'city' => 'new city',
                ]
            ]),
            'old' => json_encode([
                'id' => 1,
                'company_id' => 1,
                'first_name' => 'old first name',
                'last_name' => 'old last name',
                'middle_name' => 'middle name',
                'birth_date' => null,
                'address' => [
                    'address_line_1' => 'address',
                    'city' => 'old city',
                ]
            ]),
            'user' => json_encode([
                'id' => 1,
                'account_id' => 1,
            ]),
        ];

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')->once();

        $employeeAuditService = new EmployeeAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logEdit($item);
    }

    public function testLogCreatedEmployees()
    {
        $this->markTestSkipped('Deprecated.');
        $employeeIds = [1];
        $userId = 1;
        $employeeData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'account_id' => 2
        ]);
        $mockResponse = new JsonResponse($employeeData, JsonResponse::HTTP_OK);

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockRequestService->shouldReceive('getEmployee')
            ->once()
            ->andReturn($mockResponse);
        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[logCreate]', [
            $mockRequestService, $mockAuditService
        ]);

        $employeeAuditService->shouldReceive('logCreate')->once();

        $employeeAuditService->logCreatedEmployees($employeeIds, $userId);
    }

    public function testLogCreatedEmployeesInvalidEmployee()
    {
        $this->markTestSkipped('Deprecated.');
        $employeeIds = [1];
        $userId = 1;

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockRequestService->shouldReceive('getEmployee')
            ->once()
            ->andThrow(new \Exception('404'));
        $mockAuditService = m::mock('App\Audit\AuditService');

        Log::shouldReceive('error')
            ->once();

        $employeeAuditService = new EmployeeAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logCreatedEmployees($employeeIds, $userId);
    }

    public function testLogCreatedTaEmployees()
    {
        $this->markTestSkipped('Deprecated.');
        $userId = 1;
        $employees = [
            [
                'id' => 1,
                'company_id' => 1,
                'account_id' => 2
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'account_id' => 3
            ],
        ];

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[logCreate]', [
            $mockRequestService, $mockAuditService
        ]);

        $employeeAuditService->shouldReceive('logCreate')->twice();

        $employeeAuditService->logCreatedTaEmployees($employees, $userId);
    }

    public function testLogForCreateAction()
    {
        $this->markTestSkipped('Deprecated.');
        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[logCreate]', [
            $mockRequestService,
            $mockAuditService,
        ]);

        $employeeAuditService->shouldReceive('logCreate')->once();

        $employeeAuditService->log(['action' => EmployeeAuditService::ACTION_CREATE]);
    }

    public function testLogCreate()
    {
        $this->markTestSkipped('Deprecated.');
        $item = [
            'new' => json_encode([
                'company_id' => 1,
                'first_name' => 'new first name',
                'last_name' => 'new last name',
                'middle_name' => 'new middle name',
            ]),
            'user' => json_encode([
                'id' => 1,
                'account_id' => 1,
            ]),
        ];

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')->once();

        $employeeAuditService = new EmployeeAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogUpdatedEmployees()
    {
        $this->markTestSkipped('Deprecated.');
        $employeeIds = [1, 2];
        $jobId = 1;
        $userId = 1;
        $employeeData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'account_id' => 2
        ]);
        $mockResponse1 = new JsonResponse($employeeData, JsonResponse::HTTP_OK);
        $mockResponse2 = new JsonResponse($employeeData, JsonResponse::HTTP_OK);

        Redis::shouldReceive('sMembers')
            ->once()
            ->andReturn(
                [
                    'audit:PERSONAL:MTpCYXV0aXN0YTpQYXVsOg==',
                    'audit:PAYROLL:MTpCYXV0aXN0YTpQYXVsOg=='
                ]
            )
            ->shouldReceive('hGetAll')
            ->times(2)
            ->andReturn(
                [
                    'old' => '{"id": 1, "company_id": 1, "account_id": 1}'
                ],
                [
                    'old' => '{"id": 2, "company_id": 2, "account_id": 2}'
                ]
            );

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockRequestService->shouldReceive('getEmployee')
            ->times(2)
            ->andReturn($mockResponse1, $mockResponse2);

        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('queue')
            ->times(2);

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[queue]', [
            $mockRequestService, $mockAuditService
        ]);

        $employeeAuditService->logUpdatedEmployees($jobId, $employeeIds, $userId);
    }

    public function testLogUpdatedEmployeesInvalidEmployee()
    {
        $this->markTestSkipped('Deprecated.');
        $employeeIds = [1];
        $jobId = 1;
        $userId = 1;

        Redis::shouldReceive('sMembers')
            ->once()
            ->andReturn(
                [
                    'audit:PERSONAL:MTpCYXV0aXN0YTpQYXVsOg=='
                ]
            )
            ->shouldReceive('hGetAll')
            ->once()
            ->andReturn(
                [
                    'old' => '{"id": 1, "company_id": 1, "account_id": 1}'
                ]
            );

        Log::shouldReceive('error')
            ->once();

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockRequestService->shouldReceive('getEmployee')
            ->once()
            ->andThrow(new \Exception('404'));

        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('queue')
            ->never();

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[queue]', [
            $mockRequestService, $mockAuditService
        ]);

        $employeeAuditService->logUpdatedEmployees($jobId, $employeeIds, $userId);
    }

    public function testCacheEmployeesBeforeUpdate()
    {
        $this->markTestSkipped('Deprecated.');
        $employeeIds = [1];
        $jobId = 1;
        $userId = 1;
        $companyId = 1;
        $employeeData = json_encode([
            'data' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'account_id' => 2
                ]
            ]
        ]);
        $mockResponse = new JsonResponse($employeeData, JsonResponse::HTTP_OK);

        Redis::shouldReceive('hMSet')
            ->once()
            ->shouldReceive('expire')
            ->once()
            ->shouldReceive('sAdd')
            ->once();

        $mockRequestService = m::mock('App\Employee\EmployeeRequestService');
        $mockRequestService->shouldReceive('getCompanyEmployeesByIds')
            ->once()
            ->andReturn($mockResponse);

        $mockAuditService = m::mock('App\Audit\AuditService');

        $employeeAuditService = m::mock(EmployeeAuditService::class . '[queue]', [
            $mockRequestService, $mockAuditService
        ]);

        $employeeAuditService->cacheEmployeesBeforeUpdate($jobId, $employeeIds, $userId, $companyId);
    }
}
