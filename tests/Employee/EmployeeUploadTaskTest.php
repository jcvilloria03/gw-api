<?php

namespace Tests\Employee;

use Mockery as m;
use App\Model\CompanyUser;
use App\Auth0\Auth0UserService;
use App\Employee\EmployeeUploadTask;
use Illuminate\Support\Facades\Redis;
use App\CompanyUser\CompanyUserService;
use App\Employee\EmployeeUploadTaskException;
use App\Authn\AuthnService;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class EmployeeUploadTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        $this->assertInstanceOf(EmployeeUploadTask::class, $task);
    }

    public function testCreateNoJobId()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        Redis::shouldReceive('expire')
            ->once();

        $task->create($companyId);

        $idPrefix =  EmployeeUploadTask::ID_PREFIX . $companyId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateWithJobIdExistingJob()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($companyId, $jobId);

        $this->assertEquals($task->getId(), $jobId);
    }

    public function testCreateWithJobIdNonExistingJob()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->create($companyId, $jobId);
    }

    public function testIsCompanyIdSameValidNewJob()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        Redis::shouldReceive('expire')
            ->once();

        $task->create($companyId);
        $this->assertTrue($task->isCompanyIdSame());
    }

    public function testIsCompanyIdSameValidExistingJob()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($companyId, $jobId);
        $this->assertTrue($task->isCompanyIdSame());
    }

    public function testIsCompanyIdSameInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . 1 . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->create($companyId, $jobId);
        $this->assertFalse($task->isCompanyIdSame());
    }

    public function testGetS3Bucket()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        $this->assertNotEmpty($task->getS3Bucket());
    }

    public function testSavePersonalInfo()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId);
        $s3Key = $task->savePersonalInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'employee_upload_personal:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testSavePayrollInfoValid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATED]);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId);
        $s3Key = $task->savePayrollInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'employee_upload_payroll:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testUpdateValidationStatusInvalidProcess()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateValidationStatus('invalid process', 'new status');
    }

    public function testUpdateValidationStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateValidationStatus(EmployeeUploadTask::PROCESS_PERSONAL, 'invalid status');
    }

    public function testUpdateValidationStatusPersonal()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusPayroll()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToQueued()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToValidating()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATION_QUEUED]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATING
        );
    }

    public function testUpdateValidationStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_VALIDATING,
            EmployeeUploadTask::STATUS_VALIDATED,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATING
        );
    }

    public function testUpdateValidationStatusToValidated()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATED
        );
    }

    public function testUpdateValidationStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUploadTask::STATUS_VALIDATED,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATED
        );
    }

    public function testUpdateValidationStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATING]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED
        );
    }

    public function testUpdateValidationStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUploadTask::STATUS_VALIDATED,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED
        );
    }

    public function testUpdateSaveStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateSaveStatus('invalid status');
    }

    public function testUpdateSaveStatusValid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_VALIDATING,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED,
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueued()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => EmployeeUploadTask::STATUS_SAVING,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToValidating()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => EmployeeUploadTask::STATUS_SAVE_QUEUED,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_SAVING,
            EmployeeUploadTask::STATUS_SAVED,
            EmployeeUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidated()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => EmployeeUploadTask::STATUS_SAVING,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_SAVE_QUEUED,
            EmployeeUploadTask::STATUS_SAVED,
            EmployeeUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => EmployeeUploadTask::STATUS_SAVING,
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateSaveStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_SAVE_QUEUED,
            EmployeeUploadTask::STATUS_SAVED,
            EmployeeUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'payroll_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'people_status' => EmployeeUploadTask::STATUS_VALIDATED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateErrorFileLocation()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $validProcesses = [
            EmployeeUploadTask::PROCESS_PERSONAL,
            EmployeeUploadTask::PROCESS_PEOPLE,
            EmployeeUploadTask::PROCESS_SAVE,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['blah']);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateErrorFileLocation($validProcesses[array_rand($validProcesses)], 'key');
    }

    public function testUpdateErrorFileLocationInvalidProcess()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['blah']);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateErrorFileLocation('invalid process', 'key');
    }

    public function testFetch()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->times(2)
            ->andReturn(['a' => 'b'], []);

        $task->create($companyId, $jobId);
        $task->fetch();
    }

    public function testFetchSelectedFields()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hMGet')
            ->once();

        $task->create($companyId, $jobId);
        $task->fetch(['field1', 'field2']);
    }

    public function testFetchErrorFileFromS3Invalid()
    {
        // empty result and no result body should return null
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->times(2)
            ->andReturn([], ['Header' => 'haha']);
        $task = new EmployeeUploadTask($mockS3Client);

        $this->assertNull($task->fetchErrorFileFromS3('key'));
        $this->assertNull($task->fetchErrorFileFromS3('key'));
    }

    public function testFetchErrorFileFromS3Valid()
    {
        // empty result and no result body should return null
        $mockStream = new class() {
            public function getContents()
            {
                return '{"a" : 1}';
            }
        };
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->once()
            ->andReturn(['Body' => $mockStream]);
        $task = new EmployeeUploadTask($mockS3Client);

        $this->assertEquals(["a" => 1], $task->fetchErrorFileFromS3('key'));
    }

    public function testSetUserId()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->setUserId(1);
    }

    public function testGetUserId()
    {
        $userId = 2;
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGet')
            ->once()
            ->andReturn($userId);

        $this->assertEquals($userId, $task->getUserId());
    }

    public function testSaveTimeAttendanceInfoValid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['personal_status' => EmployeeUploadTask::STATUS_VALIDATED]);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId);
        $s3Key = $task->saveTimeAttendanceInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'employee_upload_time_attendance:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testUpdateTaSaveStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateTaSaveStatus('invalid status');
    }

    public function testUpdateTaSaveStatusValid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'time_attendance_status' => EmployeeUploadTask::STATUS_VALIDATED,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateTaSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateTaSaveStatusInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUploadTask::STATUS_VALIDATING,
            EmployeeUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUploadTask::STATUS_VALIDATION_FAILED,
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUploadTask::STATUS_VALIDATED,
                'time_attendance_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateTaSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testCreateUserRecordsExistingRecords()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $mockCompanyUserService->shouldReceive('getByUserId')
            ->andReturn(1);
        $mockCompanyUserService->shouldReceive('create')
            ->never();
        app()->instance(CompanyUserService::class, $mockCompanyUserService);

        $mockAuth0UserService = m::mock(Auth0UserService::class);
        $mockAuth0UserService->shouldReceive('getUser')
            ->andReturn(1);
        $mockAuth0UserService->shouldReceive('create')
            ->never();
        app()->instance(Auth0UserService::class, $mockAuth0UserService);

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        $task->createUserRecords([
            [
                'id' => 1,
                'user_id' => 1
            ]
        ]);
    }

    public function testCreateUserRecordsExistingCompanyUser()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $mockCompanyUserService->shouldReceive('getByUserId')
            ->andReturn(new CompanyUser([
                'user_id'     => 1,
                'company_id'  => 1,
                'employee_id' => 1
            ]));
        $mockCompanyUserService->shouldReceive('create')
            ->never();
        app()->instance(CompanyUserService::class, $mockCompanyUserService);

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUploadTask($mockS3Client);

        $task->createUserRecords([
            [
                'id'         => 1,
                'user_id'    => 1,
                'account_id' => 1,
                'company_id' => 1
            ]
        ]);
    }
}
