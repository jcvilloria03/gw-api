<?php

namespace Tests\Employee;

use App\Employee\EmployeeAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class EmployeeAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testCreatePass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreatePassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreatePassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testViewCompanyEmployeesPass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesPassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesPassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testViewCompanyEmployeesInvalidOtherScope()
    {
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyEmployees($targetCompany, $user));
    }

    public function testGetPass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetPassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetPassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(EmployeeAuthorizationService::VIEW_TASK);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCompany, $user));
    }

    public function testUpdatePass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdatePassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdatePassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdateNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdateInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdateInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompany, $user));
    }

    public function testUpdateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(EmployeeAuthorizationService::UPDATE_TASK);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EmployeeAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCompany, $user));
    }
}
