<?php

namespace Tests\Employee;

use App\Employee\EssEmployeeAuthorizationService;
use App\Employee\EmployeeAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssEmployeeAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testViewPass()
    {
        $userId = 1;
        $taskScope = new TaskScopes(EssEmployeeAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EssEmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($userId));
    }

    public function testViewFail()
    {
        $userId = 1;
        $taskScope = new TaskScopes(EmployeeAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Employee\EssEmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($userId));
    }
}
