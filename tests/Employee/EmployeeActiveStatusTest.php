<?php

namespace Tests\Employee;

use Laravel\Lumen\Testing\DatabaseTransactions;
use Mockery as m;
use App\Http\Controllers\Concerns\ProcessesEmployeeActiveStatusUpdatesTrait;
use App\Auth0\Auth0UserService;
use App\Role\DefaultRoleService;
use App\Role\UserRoleService;
use App\Auth0\Auth0ManagementService;
use App\Model\Role;
use App\Model\Task;
use App\Model\Permission;
use App\Model\UserRole;
use App\Model\Auth0User;
use App\Model\CompanyUser;

class EmployeeActiveStatusTest extends \Tests\TestCase
{
    use DatabaseTransactions;
    use ProcessesEmployeeActiveStatusUpdatesTrait;
    
    const COMPANY_ID = 1;
    const ACCOUNT_ID = 1;

    /**
     * @var App\Model\Role
     */
    private $role;

    /**
     * @var App\Role\DefaultRoleService
     */
    private $defaultRoleService;

    /**
     * @var App\Role\UserRoleService
     */
    private $userRoleService;
    
    private $employeeData;

    public function setUp()
    {
        parent::setUp();

        $this->defaultRoleService = app()->make(DefaultRoleService::class);
        $this->userRoleService = app()->make(UserRoleService::class);

        $this->role = $this->createEmployeeRole();
        $this->createPermissions($this->role);

        $this->employeeData = [
            "email" => "gw-api-test@salarium.com",
            "first_name" => "GW",
            "last_name" => "API",
            "middle_name" => "",
            "account_id" => self::ACCOUNT_ID,
            "company_id" => self::COMPANY_ID,
            "active" => true,

        ];
    }

    public function testPreActiveAuth()
    {
        $userId = 1;
        $employeeId = 1;

        $this->auth0User = $this->createAuth0User($userId);
        $this->createCompanyUser($userId, $employeeId);

        $key = self::ACCOUNT_ID.'-'.$userId;
        $this->auth0User->auth0_user_id = "preactive|{$key}";
        $this->auth0User->status = Auth0User::STATUS_INACTIVE;

        $mockAuth0UserService = m::mock(Auth0UserService::class);

        $mockAuth0UserService->shouldReceive('createEmployeeUserInManagement')
            ->once()
            ->andReturn($this->auth0User);

        $this->app->bind(Auth0UserService::class, function () use ($mockAuth0UserService) {
            return $mockAuth0UserService;
        });

        $this->employeeData["id"] = $employeeId;
        $this->employeeData["employee_id"] = $employeeId;

        $this->processEmployeeActiveStatus($this->employeeData, true, $this->auth0User);
        $this->assertEquals(1, $this->getEmployeeRoleCount($userId));
    }

    public function testAlreadyActive()
    {
        $userId = 2;
        $employeeId = 2;

        $this->auth0User = $this->createAuth0User($userId);
        $this->createCompanyUser($userId, $employeeId);

        $this->auth0User->auth0_user_id = "auth0|5e1ff519744fa80e8ac0f19f";
        $this->auth0User->status = Auth0User::STATUS_INACTIVE;
        
        $mockAuth0UserService = m::mock(Auth0UserService::class);

        $mockAuth0UserService->shouldReceive('setStatus')
            ->times(2)
            ->andReturn(true, true);

        $this->app->bind(Auth0UserService::class, function () use ($mockAuth0UserService) {
            return $mockAuth0UserService;
        });

        $mockAuth0ManagementService = m::mock(Auth0ManagementService::class);

        $mockAuth0ManagementService->shouldReceive('getAuth0UserProfile')
            ->once()
            ->andReturn([
                "email_verified" => true
            ]);

        $this->app->bind(Auth0ManagementService::class, function () use ($mockAuth0ManagementService) {
            return $mockAuth0ManagementService;
        });

        $this->employeeData["id"] = $employeeId;
        $this->employeeData["employee_id"] = $employeeId;

        $this->processEmployeeActiveStatus($this->employeeData, true, $this->auth0User);
        $this->assertEquals(1, $this->getEmployeeRoleCount($userId));
    }

    private function createEmployeeRole()
    {
        $role = factory(Role::class)->make();
        $role->company_id = self::COMPANY_ID;
        $role->account_id = self::ACCOUNT_ID;
        $role->name = Role::EMPLOYEE;
        $role->type = Role::EMPLOYEE;
        $moduleAccess = implode(',', Role::MODULE_ACCESSES);
        $role->module_access = "[{$moduleAccess}]";
        $role->save();

        return $role;
    }

    private function createPermissions($role)
    {
        $essTasks = Task::where([
            ['module', '=', 'ESS']
        ])->get();

        foreach ($essTasks as $task) {
            $permission = new Permission();
            $permission->task_id = $task->id;
            $permission->role_id = $role->id;
            $permission->scope = "{}";
            $permission->save();
        }
    }

    private function createAuth0User($userId)
    {
        $auth0User = new Auth0User();
        $auth0User->user_id = $userId;
        $auth0User->account_id = self::ACCOUNT_ID;

        return $auth0User;
    }

    private function createCompanyUser($userId, $employeeId)
    {
        $companyUser = new CompanyUser();
        $companyUser->user_id = $userId;
        $companyUser->company_id = self::COMPANY_ID;
        $companyUser->employee_id = $employeeId;
        $companyUser->save();
    }

    private function getEmployeeRoleCount($userId)
    {
        $userRole = UserRole::where([
            ['role_id', '=', $this->role->id],
            ['user_id', '=', $userId]
        ])->get();

        return $userRole->count();
    }
}

