<?php

namespace Tests\Employee;

use Mockery as m;
use App\Model\Role;
use App\Model\Auth0User;
use App\Model\CompanyUser;
use App\Role\UserRoleService;
use App\Auth0\Auth0UserService;
use App\Role\DefaultRoleService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use App\CompanyUser\CompanyUserService;
use App\Employee\EmployeeUpdateUploadTask;
use App\Employee\EmployeeUploadTaskException;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class EmployeeUpdateUploadTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        $this->assertInstanceOf(EmployeeUpdateUploadTask::class, $task);
    }

    public function testGetProcess()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        Redis::shouldReceive('expire')
            ->once();

        Redis::shouldReceive('hSet')
            ->times(1);

        $task->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PERSONAL);

        $this->assertEquals(EmployeeUpdateUploadTask::PROCESS_PERSONAL, $task->getProcess());
    }

    public function testCreateNoJobIdPersonal()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        Redis::shouldReceive('expire')
            ->once();

        Redis::shouldReceive('hSet')
            ->times(1);

        $task->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PERSONAL);

        $idPrefix =  EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateNoJobIdPayroll()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        Redis::shouldReceive('expire')
            ->once();

        Redis::shouldReceive('hSet')
            ->times(1);

        $task->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PAYROLL);

        $idPrefix =  EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateNoJobIdInvalidProcess()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->create($companyId, null, 'invalid');
    }

    public function testSavePersonalInfo()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL]);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId, EmployeeUpdateUploadTask::PROCESS_PERSONAL);
        $s3Key = $task->savePersonalInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'employee_update_upload_personal:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testSavePayrollInfo()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['process' => EmployeeUpdateUploadTask::PROCESS_PAYROLL]);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($companyId, $jobId, EmployeeUpdateUploadTask::PROCESS_PAYROLL);
        $s3Key = $task->savePayrollInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'employee_update_upload_payroll:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testUpdateValidationStatusInvalidProcess()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateValidationStatus('invalid process', 'new status');
    }

    public function testUpdateValidationStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateValidationStatus(EmployeeUpdateUploadTask::PROCESS_PERSONAL, 'invalid status');
    }

    public function testUpdateValidationStatusPersonal()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusPayroll()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToQueued()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATING,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED
        );
    }

    public function testUpdateValidationStatusToValidating()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATING
        );
    }

    public function testUpdateValidationStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_VALIDATING,
            EmployeeUpdateUploadTask::STATUS_VALIDATED,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATING
        );
    }

    public function testUpdateValidationStatusToValidated()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATING,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATED
        );
    }

    public function testUpdateValidationStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUpdateUploadTask::STATUS_VALIDATED,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATED
        );
    }

    public function testUpdateValidationStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATING,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED
        );
    }

    public function testUpdateValidationStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUpdateUploadTask::STATUS_VALIDATED,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED
        );
    }

    public function testUpdateSaveStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateSaveStatus('invalid status');
    }

    public function testUpdateSaveStatusValid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_VALIDATING,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_QUEUED,
            EmployeeUpdateUploadTask::STATUS_VALIDATION_FAILED,
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'payroll_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
                'process' => EmployeeUpdateUploadTask::PROCESS_PAYROLL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(EmployeeUploadTaskException::class);
        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueued()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueuedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => EmployeeUpdateUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToValidating()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidatingInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_SAVING,
            EmployeeUpdateUploadTask::STATUS_SAVED,
            EmployeeUpdateUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidated()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => EmployeeUpdateUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidatedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED,
            EmployeeUpdateUploadTask::STATUS_SAVED,
            EmployeeUpdateUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidationFailed()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => EmployeeUpdateUploadTask::STATUS_SAVING
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateSaveStatusToValidationFailedInvalid()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $invalidCurrentStatus = [
            EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED,
            EmployeeUpdateUploadTask::STATUS_SAVED,
            EmployeeUpdateUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'personal_status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
                'process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateErrorFileLocation()
    {
        $companyId = 999;
        $jobId = EmployeeUpdateUploadTask::ID_PREFIX . $companyId . ':' . uniqid();
        $validProcesses = [
            EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            EmployeeUpdateUploadTask::PROCESS_PAYROLL
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['process' => EmployeeUpdateUploadTask::PROCESS_PERSONAL]);

        $task->create($companyId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateErrorFileLocation($validProcesses[array_rand($validProcesses)], 'key');
    }

    public function testActivateEmployeesCreatesNewRecords()
    {

        $companyUser = new CompanyUser;
        $companyUser->fill([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $auth0User = new Auth0User;
        $auth0User->fill([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'auth0_user_id' => 'preactive|1-1',
            'status'        => 'inactive'
        ]);

        $employeeRole = new Role;
        $employeeRole->fill([
            'id'            => 1,
            'account_id'    => 1,
            'company_id'    => 1,
            'name'          => 'Default Employee Role',
            'type'          => 'Employee',
            'custom_role'   => 0,
            'module_access' => ["HRIS","T&A","Payroll"]
        ]);

        $auth0UserService = m::mock(Auth0UserService::class);
        $auth0UserService->shouldReceive('getUserByEmployeeId')
            ->with(1)
            ->andReturn(null)
            ->shouldReceive('getUser')
            ->with(1)
            ->andReturn(null)
            ->shouldReceive('generatePreactiveId')
            ->andReturn('preactive|1-1')
            ->shouldReceive('create')
            ->with([
                'user_id'       => 1,
                'account_id'    => 1,
                'auth0_user_id' => 'preactive|1-1',
                'status'        => 'inactive'
            ])
            ->andReturn($auth0User)
            ->shouldReceive('batchCreateEmployeeUserInManagement')
            ->andReturn(true);

        $companyUserService = m::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getByEmployeeId')
            ->with(1)
            ->andReturn(null)
            ->shouldReceive('create')
            ->with([
                'user_id'     => 1,
                'company_id'  => 1,
                'employee_id' => 1
            ])
            ->andReturn($companyUser);

        $defaultRoleService = m::mock(DefaultRoleService::class);
        $defaultRoleService->shouldReceive('getEmployeeSystemRole')
                ->with(1, 1)
                ->andReturn($employeeRole);

        $userRoleService = m::mock(UserRoleService::class);
        $userRoleService->shouldReceive('getWithConditions')
            ->with([
                ['user_id', 1],
                ['role_id', $employeeRole->id]
            ])
            ->andReturn(collect([]))
            ->shouldReceive('create')
            ->with([
                'role_id' => $employeeRole->id,
                'user_id' => $auth0User->user_id,
            ])
            ->andReturn(true);

        $this->app->instance(Auth0UserService::class, $auth0UserService);
        $this->app->instance(CompanyUserService::class, $companyUserService);
        $this->app->instance(DefaultRoleService::class, $defaultRoleService);
        $this->app->instance(UserRoleService::class, $userRoleService);

        $employees = [
            [
                'id'          => 1,
                'user_id'     => 1,
                'account_id'  => 1,
                'company_id'  => 1,
                'employee_id' => 1,
                'email'       => 'test@test.com',
                'name'        => 'test test',
                'first_name'  => 'test',
                'last_name'   => 'test',
                'middle_name' => 'test',
            ]
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $task->activateEmployees(1, $employees);
    }

    public function testActivateEmployeesExistingCompanyUser()
    {

        $companyUser = new CompanyUser;
        $companyUser->fill([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $auth0User = new Auth0User;
        $auth0User->fill([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'auth0_user_id' => 'preactive|1-1',
            'status'        => 'inactive'
        ]);

        $employeeRole = new Role;
        $employeeRole->fill([
            'id'            => 1,
            'account_id'    => 1,
            'company_id'    => 1,
            'name'          => 'Default Employee Role',
            'type'          => 'Employee',
            'custom_role'   => 0,
            'module_access' => ["HRIS","T&A","Payroll"]
        ]);

        $auth0UserService = m::mock(Auth0UserService::class);
        $auth0UserService->shouldReceive('getUserByEmployeeId')
            ->with(1)
            ->andReturn(null)
            ->shouldReceive('getUser')
            ->with(1)
            ->andReturn(null)
            ->shouldReceive('generatePreactiveId')
            ->andReturn('preactive|1-1')
            ->shouldReceive('create')
            ->with([
                'user_id'       => 1,
                'account_id'    => 1,
                'auth0_user_id' => 'preactive|1-1',
                'status'        => 'inactive'
            ])
            ->andReturn($auth0User)
            ->shouldReceive('batchCreateEmployeeUserInManagement')
            ->andReturn(true);

        $companyUserService = m::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getByEmployeeId')
            ->with(1)
            ->andReturn($companyUser)
            ->shouldReceive('create')
            ->never();

        $defaultRoleService = m::mock(DefaultRoleService::class);
        $defaultRoleService->shouldReceive('getEmployeeSystemRole')
                ->with(1, 1)
                ->andReturn($employeeRole);

        $userRoleService = m::mock(UserRoleService::class);
        $userRoleService->shouldReceive('getWithConditions')
            ->with([
                ['user_id', 1],
                ['role_id', $employeeRole->id]
            ])
            ->andReturn(collect([]))
            ->shouldReceive('create')
            ->with([
                'role_id' => $employeeRole->id,
                'user_id' => $auth0User->user_id,
            ])
            ->andReturn(true);

        $this->app->instance(Auth0UserService::class, $auth0UserService);
        $this->app->instance(CompanyUserService::class, $companyUserService);
        $this->app->instance(DefaultRoleService::class, $defaultRoleService);
        $this->app->instance(UserRoleService::class, $userRoleService);

        $employees = [
            [
                'id'          => 1,
                'user_id'     => 1,
                'account_id'  => 1,
                'company_id'  => 1,
                'employee_id' => 1,
                'email'       => 'test@test.com',
                'name'        => 'test test',
                'first_name'  => 'test',
                'last_name'   => 'test',
                'middle_name' => 'test',
            ]
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $task->activateEmployees(1, $employees);
    }

    public function testActivateEmployeesExistingAuth0User()
    {

        $companyUser = new CompanyUser;
        $companyUser->fill([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $auth0User = new Auth0User;
        $auth0User->fill([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'auth0_user_id' => 'preactive|1-1',
            'status'        => 'inactive'
        ]);

        $employeeRole = new Role;
        $employeeRole->fill([
            'id'            => 1,
            'account_id'    => 1,
            'company_id'    => 1,
            'name'          => 'Default Employee Role',
            'type'          => 'Employee',
            'custom_role'   => 0,
            'module_access' => ["HRIS","T&A","Payroll"]
        ]);

        $auth0UserService = m::mock(Auth0UserService::class);
        $auth0UserService->shouldReceive('getUserByEmployeeId')
            ->with(1)
            ->andReturn($auth0User)
            ->shouldReceive('getUser')
            ->with(1)
            ->andReturn($auth0User)
            ->shouldReceive('generatePreactiveId')
            ->never()
            ->shouldReceive('create')
            ->never()
            ->shouldReceive('batchCreateEmployeeUserInManagement')
            ->andReturn(true);

        $companyUserService = m::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getByEmployeeId')
            ->with(1)
            ->andReturn($companyUser)
            ->shouldReceive('create')
            ->never();

        $defaultRoleService = m::mock(DefaultRoleService::class);
        $defaultRoleService->shouldReceive('getEmployeeSystemRole')
                ->with(1, 1)
                ->andReturn($employeeRole);

        $userRoleService = m::mock(UserRoleService::class);
        $userRoleService->shouldReceive('getWithConditions')
            ->with([
                ['user_id', 1],
                ['role_id', $employeeRole->id]
            ])
            ->andReturn(collect([]))
            ->shouldReceive('create')
            ->with([
                'role_id' => $employeeRole->id,
                'user_id' => $auth0User->user_id,
            ])
            ->andReturn(true);

        $this->app->instance(Auth0UserService::class, $auth0UserService);
        $this->app->instance(CompanyUserService::class, $companyUserService);
        $this->app->instance(DefaultRoleService::class, $defaultRoleService);
        $this->app->instance(UserRoleService::class, $userRoleService);

        $employees = [
            [
                'id'          => 1,
                'user_id'     => 1,
                'account_id'  => 1,
                'company_id'  => 1,
                'employee_id' => 1,
                'email'       => 'test@test.com',
                'name'        => 'test test',
                'first_name'  => 'test',
                'last_name'   => 'test',
                'middle_name' => 'test',
            ]
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $task->activateEmployees(1, $employees);
    }

    public function testActivateEmployeesExistingActiveAuth0User()
    {

        $companyUser = new CompanyUser;
        $companyUser->fill([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $auth0User = new Auth0User;
        $auth0User->fill([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'auth0_user_id' => 'auth0|1-1',
            'status'        => 'inactive'
        ]);

        $employeeRole = new Role;
        $employeeRole->fill([
            'id'            => 1,
            'account_id'    => 1,
            'company_id'    => 1,
            'name'          => 'Default Employee Role',
            'type'          => 'Employee',
            'custom_role'   => 0,
            'module_access' => ["HRIS","T&A","Payroll"]
        ]);

        $auth0UserService = m::mock(Auth0UserService::class);
        $auth0UserService->shouldReceive('getUserByEmployeeId')
            ->with(1)
            ->andReturn($auth0User)
            ->shouldReceive('getUser')
            ->with(1)
            ->andReturn($auth0User)
            ->shouldReceive('generatePreactiveId')
            ->never()
            ->shouldReceive('create')
            ->never()
            ->shouldReceive('createEmployeeUserInManagement')
            ->never();

        $companyUserService = m::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getByEmployeeId')
            ->with(1)
            ->andReturn($companyUser)
            ->shouldReceive('create')
            ->never();

        $defaultRoleService = m::mock(DefaultRoleService::class);
        $defaultRoleService->shouldReceive('getEmployeeSystemRole')
                ->with(1, 1)
                ->andReturn($employeeRole);

        $userRoleService = m::mock(UserRoleService::class);
        $userRoleService->shouldReceive('getWithConditions')
            ->with([
                ['user_id', 1],
                ['role_id', $employeeRole->id]
            ])
            ->andReturn(collect([]))
            ->shouldReceive('create')
            ->with([
                'role_id' => $employeeRole->id,
                'user_id' => $auth0User->user_id,
            ])
            ->andReturn(true);

        $this->app->instance(Auth0UserService::class, $auth0UserService);
        $this->app->instance(CompanyUserService::class, $companyUserService);
        $this->app->instance(DefaultRoleService::class, $defaultRoleService);
        $this->app->instance(UserRoleService::class, $userRoleService);

        $employees = [
            [
                'id'          => 1,
                'user_id'     => 1,
                'account_id'  => 1,
                'company_id'  => 1,
                'employee_id' => 1,
                'email'       => 'test@test.com',
                'name'        => 'test test',
                'first_name'  => 'test',
                'last_name'   => 'test',
                'middle_name' => 'test',
            ]
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new EmployeeUpdateUploadTask($mockS3Client);
        $task->activateEmployees(1, $employees);
    }
}
