<?php

namespace Tests\Employee;

use App\Employee\EmployeeESIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class EmployeeESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $employeeIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->with([
                'id' => $employeeIds,
                'type' => 'employee',
            ])
            ->once();

        $employeeESIndexQueueService = new EmployeeESIndexQueueService($mockIndexQueueService);
        $employeeESIndexQueueService->queue($employeeIds);
    }
}
