<?php

namespace Tests\AttendanceView;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\AttendanceView\AttendanceViewAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class AttendanceViewAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    /**
     * Gets attendanceViewDetails.
     * @param int $accountId
     * @param int $companyId
     * @return object
     */
    private function getViewDetails($accountId = 1, $companyId = 1)
    {
        return (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
    }

    /**
     * Gets user data.
     * @param int $userId
     * @param int $accountId
     * @return array
     */
    private function getUserData($userId = 1, $accountId = 1)
    {
        return [
            'user_id' => $userId,
            'account_id' => $accountId
        ];
    }

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );

        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($this->getViewDetails(), $this->getUserData()));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($this->getViewDetails(), $this->getUserData()));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData()));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData()));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [1]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData()));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData()));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData(1, 2)));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(AttendanceViewAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);

        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            AttendanceViewAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($this->getViewDetails(), $this->getUserData(1, 2)));
    }
}
