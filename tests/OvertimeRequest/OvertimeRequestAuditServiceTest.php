<?php

namespace Tests\OvertimeRequest;

use App\Audit\AuditService;
use App\OvertimeRequest\OvertimeRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class OvertimeRequestAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => OvertimeRequestAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $overtimeRequestAuditService = new OvertimeRequestAuditService($mockAuditService);
        $overtimeRequestAuditService->logCreate($item);
    }
}
