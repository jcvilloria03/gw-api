<?php

namespace Tests\Validator;

use App\Task\TaskService;
use App\Validator\UserValidator;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use Tests\TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserValidatorTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
    }

    public function testValidAndInvalidUserTypes()
    {
        $validator = new UserValidator();

        $validCases = [
            [
                'user_type' => UserValidator::TYPE_OWNER,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => true,
                    UserValidator::TYPE_SUPER_ADMIN => true,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_SUPER_ADMIN,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_ADMIN,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_EMPLOYEE,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => false,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
        ];

        foreach ($validCases as $validate) {
            foreach ($validate['allowed_types'] as $type => $checked) {
                $attributes = ['user_type' => $type];
                $errors = $validator->validateUserType($attributes, $validate['user_type']);
                // valid cases
                if ($checked) {
                    $this->assertEmpty($errors);
                // invalid cases
                } else {
                    $this->assertCount(1, $errors);
                }
            }
        }
    }

    public function testValidAndInvalidCompanyUserTypes()
    {
        $validator = new UserValidator();

        $validCases = [
            [
                'user_type' => UserValidator::TYPE_OWNER,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_SUPER_ADMIN,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_ADMIN,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => true,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
            [
                'user_type' => UserValidator::TYPE_EMPLOYEE,
                'allowed_types' => [
                    UserValidator::TYPE_OWNER => false,
                    UserValidator::TYPE_SUPER_ADMIN => false,
                    UserValidator::TYPE_ADMIN => false,
                    UserValidator::TYPE_EMPLOYEE => true
                ]
            ],
        ];

        foreach ($validCases as $validate) {
            foreach ($validate['allowed_types'] as $type => $checked) {
                $attributes = ['user_type' => $type];
                $errors = $validator->validateUserType($attributes, $validate['user_type'], true);
                // valid cases
                if ($checked) {
                    $this->assertEmpty($errors);
                // invalid cases
                } else {
                    $this->assertCount(1, $errors);
                }
            }
        }
    }
}
