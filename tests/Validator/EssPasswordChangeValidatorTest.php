<?php

namespace Tests\Validator;

use App\Validator\EssPasswordChangeValidator;

class EssPasswordChangeValidatorTest extends \Tests\TestCase
{
    public function testValidateValid()
    {
        $attributes = [
            'new_password' => 'test-password',
            'new_password_confirmation' => 'test-password',
        ];

        $validator = new EssPasswordChangeValidator();

        $errors = $validator->validate($attributes);

        $this->assertEmpty($errors);
    }

    /**
     * @dataProvider invalidValidationData
     */
    public function testValidateInvalid($data)
    {
        $attributes = $data;
        $validator = new EssPasswordChangeValidator();

        $errors = $validator->validate($attributes);

        $this->assertNotEmpty($errors);
    }

    public function invalidValidationData()
    {
        return [
            [
                [
                    'new_password' => '',
                    'new_password_confirmation' => 'test-password',
                ]
            ],
            [
                [
                    'new_password' => 'test',
                    'new_password_confirmation' => 'test-password',
                ]
            ],
            [
                [
                    'new_password' => 'test-password',
                    'new_password_confirmation' => '',
                ]
            ],
            [
                [
                    'new_password' => 'test-password',
                    'new_password_confirmation' => 'test',
                ]
            ]
        ];
    }
}
