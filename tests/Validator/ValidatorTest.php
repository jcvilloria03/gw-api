<?php

namespace Tests\Validator;

use App\Validator\Validator;
use App\Rule\Rule;
use Illuminate\Validation\Validator as IlluminateValidator;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class ValidatorTest extends \Tests\TestCase
{
    use MockeryPHPUnitIntegration;

    public function testValidatorCanBeInitialized()
    {
        $subject = $this->getMockForAbstractClass(Validator::class, []);
        $this->assertInstanceOf(Validator::class, $subject);
    }

    public function testGetValidator()
    {
        $mockRule = new class extends Rule {
            protected function setRules()
            {
                $this->rules = [
                    'employee_id' => 'required|alphanum_hyphen_underscore',
                    'first_name' => 'required|name',
                    'last_name' => 'required|name',
                    'middle_name' => 'name',
                    'email' => 'required|email',
                    'mobile_number' => 'mobile_number',
                    'telephone_number' => 'absolute_number',
                    'country' => 'name',
                    'city' => 'name',
                    'zip_code' => 'absolute_number',
                    'birth_date' => 'date_multi_format:m/d/Y',
                ];
            }
        };

        $employeeValidator = $this->getMockForAbstractClass(Validator::class, []);
        $validator = $employeeValidator->getValidator([], $mockRule);
        $this->assertInstanceOf(IlluminateValidator::class, $validator);
    }
}
