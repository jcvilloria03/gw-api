<?php

namespace Tests\Validator;

use App\Employee\EmployeeRequestService;
use Mockery;
use App\Validator\TimeRecordBulkCreateOrDeleteValidator;
use Illuminate\Http\JsonResponse;

class TimeRecordBulkCreateOrDeleteValidatorTest extends \Tests\TestCase
{
    public function testValidateValid()
    {
        $data = [
            'company_id' => 1,
            'create' => [
                [
                    'employee_id' => 1,
                    'type' => 'clock_in',
                    'tags' => ['test tag'],
                    'timestamp' => 123456789
                ]
            ],
            'delete' => [
                [
                    'employee_id' => 1,
                    'timestamp' => 123456789
                ]
            ]
        ];

        $mockEmployeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $mockEmployeeRequestService->shouldReceive('getCompanyTaEmployees')->with(1, [])
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'first_name' => 'first',
                        'last_name' => 'last'
                    ]
                ]
            ])));

        $validator = new TimeRecordBulkCreateOrDeleteValidator($mockEmployeeRequestService);
        $errors = $validator->validate($data);

        $this->assertEmpty($errors);
    }

    public function testValidateFailed()
    {
        $data = [
            'company_id' => 1,
            'create' => [
                [
                    'employee_id' => 'invalid',
                    'type' => 'clock_in',
                    'tags' => ['test tag'],
                    'timestamp' => 123456789
                ]
            ],
            'delete' => [
                [
                    'employee_id' => 1,
                    'timestamp' => 123456789
                ]
            ]
        ];

        $mockEmployeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $validator = new TimeRecordBulkCreateOrDeleteValidator($mockEmployeeRequestService);
        $errors = $validator->validate($data);

        $this->assertNotEmpty($errors);
    }

    public function testValidateOnInvalidEmployees()
    {
        $employeeIdFirst = 1;
        $employeeIdSecond = 2;

        $data = [
            'company_id' => 1,
            'create' => [
                [
                    'employee_id' => $employeeIdFirst,
                    'type' => 'clock_in',
                    'tags' => ['test tag'],
                    'timestamp' => 123456789
                ]
            ],
            'delete' => [
                [
                    'employee_id' => $employeeIdSecond,
                    'timestamp' => 123456789
                ]
            ]
        ];

        $mockEmployeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $mockEmployeeRequestService->shouldReceive('getCompanyTaEmployees')->with(1, [])
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    [
                        'id' => 3,
                        'first_name' => 'first',
                        'last_name' => 'last'
                    ]
                ]
            ])));

        $validator = new TimeRecordBulkCreateOrDeleteValidator($mockEmployeeRequestService);
        $errors = $validator->validate($data);

        $message = "Employees with IDs [{$employeeIdFirst}, {$employeeIdSecond}] don't exist";

        $this->assertNotEmpty($errors);
        $this->assertEquals($errors[0], $message);
    }
}
