<?php

namespace Tests\Validator;

use App\Account\AccountRequestService;
use App\Model\Role;
use App\PayrollGroup\PayrollGroupRequestService;
use App\Role\RoleService;
use App\Task\TaskService;
use App\Team\TeamRequestService;
use App\Validator\RoleValidator;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use Tests\TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RoleValidatorTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->andReturn($mockS3Client);
        app()->instance('aws', $dependency);
    }

    private function getInstance()
    {
        $mockValidator = m::mock(RoleValidator::class);

        $mockValidator->makePartial();

        return $mockValidator;
    }

    public function testValidRole()
    {
        $mockValidator = $this->getInstance();

        $attributes = [
            'account_id' => 1,
            'name' => 'test custom role a',
            'permissions' => [
                [
                    'task_id' => 20,
                    'scopes' => [
                        [
                            'type' => 'Company',
                            'targets' => [1],
                        ]
                    ],
                ]
            ],
        ];

        $validator = app('validator')->make(
            $attributes,
            [
                'name' => 'required',
                'permissions' => 'required',
            ]
        );

        $mockValidator
            ->shouldReceive('getValidator')
            ->andReturn($validator);

        $actual = $mockValidator->validate($attributes);

        $this->assertEmpty($actual);
    }
    public function testValidateUserTypeOwnerNeedsToHaveAdminRoleForEveryAssignedCompany()
    {
        $accountId = 222;
        $company1Id = 333;
        $company2Id = 444;

        $service = $this->app->make(RoleService::class);
        $role2 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $role3 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);

        $attributes = [
            'user_type' => 'owner',
            'assigned_companies' => [
                [
                    'company_id' => $company1Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id
                    ]
                ],
                [
                    'company_id' => $company2Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id,
                        $role3->id
                    ]
                ]
            ]
        ];

        $validator = $this->getInstance();
        $result = $validator->validateAssignedRoles($attributes, $accountId);

        $this->arrayHasKey('assigned_companies', $result);
        $this->assertEquals(
            'Users of type Owner and Super Admin need to have admin role for every company.',
            $result['assigned_companies'][0]
        );
    }

    public function testValidateUserTypeSuperAdminNeedsToHaveAdminRoleForEveryAssignedCompany()
    {
        $accountId = 222;
        $company1Id = 333;
        $company2Id = 444;

        $service = $this->app->make(RoleService::class);
        $role2 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $role3 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);

        $attributes = [
            'user_type' => 'super admin',
            'assigned_companies' => [
                [
                    'company_id' => $company1Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id
                    ]
                ],
                [
                    'company_id' => $company2Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id,
                        $role3->id
                    ]
                ]
            ]
        ];

        $validator = $this->getInstance();
        $result = $validator->validateAssignedRoles($attributes, $accountId);

        $this->arrayHasKey('assigned_companies', $result);
        $this->assertEquals(
            'Users of type Owner and Super Admin need to have admin role for every company.',
            $result['assigned_companies'][0]
        );
    }

    public function testValidateUserTypeAdminNeedsToHaveAtLeastOneAdminRole()
    {
        $accountId = 222;
        $company1Id = 333;
        $company2Id = 444;

        $service = $this->app->make(RoleService::class);
        $role2 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $role3 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);

        $attributes = [
            'user_type' => 'admin',
            'assigned_companies' => [
                [
                    'company_id' => $company1Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id
                    ]
                ],
                [
                    'company_id' => $company2Id,
                    'employee_id' => 'not empty',
                    'roles_ids' => [
                        $role2->id,
                        $role3->id
                    ]
                ]
            ]
        ];

        $validator = $this->getInstance();
        $result = $validator->validateAssignedRoles($attributes, $accountId);

        $this->arrayHasKey('assigned_companies', $result);
        $this->assertEquals(
            'Users of type Owner, Super Admin and Admin need to have at least one admin role.',
            $result['assigned_companies'][0]
        );
    }

    public function testValidateRoleTypeUserAdminTypeNeedToHaveAtLeastOnEmployeeRoleValid()
    {
        $accountId = 222;
        $company1Id = 333;
        $company2Id = 444;

        $service = $this->app->make(RoleService::class);
        $role2 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);
        $role3 = $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);

        $attributes = [
            'user_type' => 'employee',
            'assigned_companies' => [
                [
                    'company_id' => $company1Id,
                    'employee_id' => 'not empty',
                    'roles_ids' =>  [
                        $role2->id
                    ]
                ],
                [
                    'company_id' => $company2Id,
                    'employee_id' => 'not empty',
                    'roles_ids' =>  [
                        $role2->id,
                        $role3->id
                    ]
                ]
            ]
        ];

        $validator = $this->getInstance();
        $result = $validator->validateAssignedRoles($attributes, $accountId);

        $this->assertEmpty($result);
    }
}
