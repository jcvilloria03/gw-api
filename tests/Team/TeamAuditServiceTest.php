<?php

namespace Tests\Team;

use App\Audit\AuditService;
use App\Team\TeamAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TeamAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => TeamAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $teamAuditService = new TeamAuditService($mockAuditService);
        $teamAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => TeamAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $teamAuditService = new TeamAuditService($mockAuditService);
        $teamAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => TeamAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $teamAuditService = new TeamAuditService($mockAuditService);
        $teamAuditService->logDelete($item);
    }
}
