<?php

namespace Tests\Team;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Team\TeamAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class TeamAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetTeamDetails, $user));
    }

    public function testGetCompanyTeamsPassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testGetCompanyTeamsInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyTeams($targetTeamDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetTeamDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailableNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetTeamDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetTeamDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(TeamAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetTeamDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TeamAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetTeamDetails, $user));
    }
}
