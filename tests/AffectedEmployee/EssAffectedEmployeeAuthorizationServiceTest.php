<?php

namespace Tests\AffectedEmployee;

use App\AffectedEmployee\EssAffectedEmployeeAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssAffectedEmployeeAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeView()
    {
        $user = ['user_id' => 1];
        $taskScope = new TaskScopes(EssAffectedEmployeeAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\AffectedEmployee\EssAffectedEmployeeAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSearch($user['user_id']));
    }

    public function testFailToAuthorizeView()
    {
        $user = ['user_id' => 1];
        $authorizationService = $this->createMockAuthorizationService(
            'App\AffectedEmployee\EssAffectedEmployeeAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeSearch($user['user_id']));
    }
}
