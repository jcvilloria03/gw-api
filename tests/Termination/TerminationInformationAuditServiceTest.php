<?php

namespace Tests\Termination;

use App\Audit\AuditService;
use App\Termination\TerminationInformationAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TerminationInformationAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => TerminationInformationAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new TerminationInformationAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => TerminationInformationAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new TerminationInformationAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $data = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => TerminationInformationAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $data
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new TerminationInformationAuditService($mockAuditService);
        $auditService->log($item);
    }
}
