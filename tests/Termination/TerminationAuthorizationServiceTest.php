<?php

namespace Tests\Termination;

use Tests\Common\CommonAuthorizationService;

class TerminationAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Termination\TerminationAuthorizationService::class;
}
