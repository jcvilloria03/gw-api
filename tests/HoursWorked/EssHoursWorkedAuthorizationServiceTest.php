<?php

namespace Tests\HoursWorked;

use App\Permission\TaskScopes;
use App\HoursWorked\EssHoursWorkedAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class EssEssHoursWorkedAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testAuthorizeGetHoursWorked()
    {
        $user = ['user_id' => 1];
        $taskScope = new TaskScopes(EssHoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            EssHoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetHoursWorked($user['user_id']));
    }

    public function testFailToAuthorizeGetHoursWorked()
    {
        $user = ['user_id' => 1];
        $authorizationService = $this->createMockAuthorizationService(
            EssHoursWorkedAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($user['user_id']));
    }
}
