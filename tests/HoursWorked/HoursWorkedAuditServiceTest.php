<?php

namespace Tests\HoursWorked;

use App\Audit\AuditService;
use App\HoursWorked\HoursWorkedAuditService;
use App\HoursWorked\HoursWorkedRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class HoursWorkedAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'shift_id' => 1,
            'type_id' => 1,
            'value' => 8,
            'company_id' => 1,
            'hour_type' => [
                'id' => 1,
                'name' => 'Regular'
            ]
        ]);
        $item = [
            'action' => HoursWorkedAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();
        $mockHoursWorkedRequestService = m::mock(HoursWorkedRequestService::class);

        $hoursWorkedAuditService = new HoursWorkedAuditService(
            $mockAuditService,
            $mockHoursWorkedRequestService
        );
        $hoursWorkedAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'shift_id' => 1,
            'type_id' => 1,
            'value' => 3,
            'company_id' => 1,
            'hour_type' => [
                'id' => 1,
                'name' => 'Regular'
            ]
        ]);
        $newData = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'shift_id' => 1,
            'type_id' => 1,
            'value' => 8,
            'company_id' => 1,
            'hour_type' => [
                'id' => 1,
                'name' => 'Regular'
            ]
        ]);
        $item = [
            'action' => HoursWorkedAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockHoursWorkedRequestService = m::mock(HoursWorkedRequestService::class);
        $hoursWorkedAuditService = new HoursWorkedAuditService(
            $mockAuditService,
            $mockHoursWorkedRequestService
        );

        $hoursWorkedAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'shift_id' => 1,
            'type_id' => 1,
            'value' => 3,
            'company_id' => 1,
            'hour_type' => [
                'id' => 1,
                'name' => 'Regular'
            ]
        ]);
        $item = [
            'action' => HoursWorkedAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockHoursWorkedRequestService = m::mock(HoursWorkedRequestService::class);

        $hoursWorkedAuditService = new HoursWorkedAuditService(
            $mockAuditService,
            $mockHoursWorkedRequestService
        );

        $hoursWorkedAuditService->logDelete($item);
    }
}
