<?php

namespace Tests\HoursWorked;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\HoursWorked\HoursWorkedAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class HoursWorkedAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetHoursWorked($targetHoursWorkedDetails, $user));
    }

    public function testBulkCreateOrUpdateOrDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user));
    }

    public function testBulkCreateOrUpdateOrDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user));
    }

    public function testBulkCreateOrUpdateOrDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user));
    }

    public function testBulkCreateOrUpdateOrDeleteNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user)
        );
    }

    public function testBulkCreateOrUpdateOrDeleteInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user)
        );
    }

    public function testBulkCreateOrUpdateOrDeleteInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user)
        );
    }

    public function testBulkCreateOrUpdateOrDeleteInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user)
        );
    }

    public function testBulkCreateOrUpdateOrDeleteInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(HoursWorkedAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetHoursWorkedDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            HoursWorkedAuthorizationService::class,
            $taskScope
        );

        $this->assertFalse(
            $authorizationService->authorizeBulkCreateOrUpdateOrDelete($targetHoursWorkedDetails, $user)
        );
    }
}
