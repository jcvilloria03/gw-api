<?php

namespace Tests\CompanyUser;

use Mockery as m;
use App\Model\CompanyUser;
use App\User\UserRequestService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\CompanyUser\CompanyUserService;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyUserServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testCreateCompanyUser()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockUserRequestService);

        $attributes = [
            'user_id' => 1,
            'company_id' => 2,
            'employee_id' => 3
        ];

        $actual = $companyUserService->create($attributes);

        $this->assertEquals($attributes['user_id'], $actual->user_id);
        $this->assertEquals($attributes['company_id'], $actual->company_id);
        $this->assertEquals($attributes['employee_id'], $actual->employee_id);
    }

    public function testGetCompanyUserByEmployeeIdExists()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockUserRequestService);

        $expected = CompanyUser::create([
            'user_id' => 1,
            'company_id' => 2,
            'employee_id' => 3
        ]);

        $actual = $companyUserService->getByEmployeeId($expected->employee_id);

        $this->assertEquals($expected->user_id, $actual->user_id);
        $this->assertEquals($expected->company_id, $actual->company_id);
        $this->assertEquals($expected->employee_id, $actual->employee_id);
    }

    public function testGetCompanyUserByEmployeeIdDoesntExist()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockUserRequestService);

        $actual = $companyUserService->getByEmployeeId(1);

        $this->assertEmpty($actual);
    }

    public function testGetCompanyUserByUserIdExists()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockUserRequestService);

        $expected = CompanyUser::create([
            'user_id' => 1,
            'company_id' => 2,
            'employee_id' => 3
        ]);

        $actual = $companyUserService->getByUserId($expected->user_id);

        $this->assertEquals($expected->user_id, $actual->user_id);
        $this->assertEquals($expected->company_id, $actual->company_id);
        $this->assertEquals($expected->employee_id, $actual->employee_id);
    }

    public function testGetCompanyUserByUserIdDoesntExist()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockUserRequestService);

        $actual = $companyUserService->getByUserId(1);

        $this->assertEmpty($actual);
    }

    public function testSync()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('del')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $mockRequestService = m::mock(UserRequestService::class);
        $mockRequestService->shouldReceive('getUserCompaniesRelations')
            ->once()
            ->andReturn(new JsonResponse(json_encode([
                [
                    'user_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ])));

        $service = new CompanyUserService($mockRequestService);
        $service->sync(1);
        $actual = $service->getByUserId(1);
        $this->assertNotEmpty($actual);
    }

    public function testGetUserCompaniesRelationsWithError()
    {
        Log::shouldReceive('error')->once();
        $mockRequestService = m::mock(UserRequestService::class);
        $mockRequestService->shouldReceive('getUserCompaniesRelations')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));
        $service = new CompanyUserService($mockRequestService);
        $actual = $service->getUserCompaniesRelations(1);
        $this->assertEmpty($actual);
    }

    public function testGetUserCompaniesRelations()
    {
        $mockRequestService = m::mock(UserRequestService::class);
        $mockRequestService->shouldReceive('getUserCompaniesRelations')
            ->once()
            ->andReturn(new JsonResponse(json_encode([
                [
                    'user_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ])));

        $service = new CompanyUserService($mockRequestService);
        $actual = $service->getUserCompaniesRelations(1);
        $this->assertNotEmpty($actual);
    }

    public function testGetByUserAndCompanyId()
    {
        $mockRequestService = m::mock(UserRequestService::class);
        $companyUserService = new CompanyUserService($mockRequestService);

        $expected = CompanyUser::create([
            'user_id' => 1,
            'company_id' => 2,
            'employee_id' => 3
        ]);

        $actual = $companyUserService->getByUserAndCompanyId($expected['user_id'], $expected['company_id']);

        $this->assertEquals($expected->user_id, $actual->user_id);
        $this->assertEquals($expected->company_id, $actual->company_id);
        $this->assertEquals($expected->id, $actual->id);
    }
}
