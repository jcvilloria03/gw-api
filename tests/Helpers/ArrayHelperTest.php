<?php

namespace Tests\Helpers;

use App\Helpers\ArrayHelper;
use PHPUnit\Framework\TestCase;

class ArrayHelperTest extends TestCase
{
    public function testArrayDiffAssocRecursive()
    {
        $helper = new ArrayHelper();
        $firstArray = [
            'a' => 'a',
            'b' => [
                'c' => 'c',
                'd' => [
                    'e' => 'e',
                    'f' => 'g',
                    'a' => [
                        'b' => [
                            'c' => 'c',
                            'd' => 'd',
                        ]
                    ],
                ]
            ],
            'h' => 'h',
            'i' => 'i',
            'k' => [
                'l' => 'l'
            ],
            'm' => [],
        ];
        $secondArray = [
            'a' => 'b',
            'b' => [
                'c' => 'd',
                'd' => [
                    'e' => 'f',
                    'f' => 'g',
                    'a' => [
                        'b' => [
                            'c' => 'd',
                            'd' => 'd',
                        ]
                    ],
                ]
            ],
            'h' => 'h',
            'j' => 'j',
            'm' => 'm',
        ];

        $expected = [
            'a' => 'a',
            'b' => [
                'c' => 'c',
                'd' => [
                    'e' => 'e',
                    'a' => [
                        'b' => [
                            'c' => 'c',
                        ]
                    ],
                ]
            ],
            'i' => 'i',
            'k' => [
                'l' => 'l'
            ],
            'm' => [],
        ];

        $actual = $helper->arrayDiffAssocRecursive($firstArray, $secondArray);

        $this->assertEquals($expected, $actual);
    }
}
