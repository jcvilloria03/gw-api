<?php

namespace Tests\Storage;

use App\Storage\PayrollUploadService;
use Aws\S3\S3Client;
use Illuminate\Http\UploadedFile;
use Mockery;

class PayrollUploadServiceTest extends \Tests\TestCase
{
    const VALID_JOB_NAMES = [
        'validate-attendance-file',
        'validate-allowance-file',
        'validate-bonus-file',
        'validate-commission-file',
        'validate-deduction-file',
    ];

    public function getInstance()
    {
        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject')
            ->once();

        return new PayrollUploadService($mockS3Client);
    }

    public function testPayrollUploadJobNames()
    {
        $this->assertTrue(in_array(PayrollUploadService::ATTENDANCE_UPLOAD, self::VALID_JOB_NAMES));
        $this->assertTrue(in_array(PayrollUploadService::ALLOWANCE_UPLOAD, self::VALID_JOB_NAMES));
        $this->assertTrue(in_array(PayrollUploadService::BONUS_UPLOAD, self::VALID_JOB_NAMES));
        $this->assertTrue(in_array(PayrollUploadService::COMMISSION_UPLOAD, self::VALID_JOB_NAMES));
        $this->assertTrue(in_array(PayrollUploadService::DEDUCTION_UPLOAD, self::VALID_JOB_NAMES));
    }

    public function testSaveFile()
    {
        $mockCreatedBy = [
            'account_id' => 1,
            'user_id' => 1,
        ];

        $mockPayrollUploadService = $this->getInstance();
        $mockUploadedFile = new class extends UploadedFile
        {
            public function __construct()
            {
                // Do nothing, just override parent::__construct()
            }

            public function getClientOriginalName()
            {
                return 'testclientoriginalname.csv';
            }

            public function getPathName()
            {
                return '/sample/path/to/file.csv';
            }
        };

        $actual = $mockPayrollUploadService->saveFile($mockUploadedFile, $mockCreatedBy);

        $this->assertTrue(strpos($actual, 'testclientoriginalname') !== false);
        $this->assertEquals(env('UPLOADS_BUCKET'), $mockPayrollUploadService->getS3Bucket());
    }
}
