<?php

namespace Tests\FinalPay;

use App\Audit\AuditService;
use App\FinalPay\FinalPayAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class FinalPayAuditLogServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => FinalPayAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new FinalPayAuditService($mockAuditService);
        $auditService->log($item);
    }
}
