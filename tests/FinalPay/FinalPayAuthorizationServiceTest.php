<?php

namespace Tests\FinalPay;

use Tests\Common\CommonAuthorizationService;

class FinalPayAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\FinalPay\FinalPayAuthorizationService::class;
}
