<?php

namespace Tests\Contribution;

use App\Contribution\ContributionRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ContributionRequestServiceTest extends TestCase
{
    public function testGetBasis()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new ContributionRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBasis();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getBasis();

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getBasis();

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getBasis();
    }
}
