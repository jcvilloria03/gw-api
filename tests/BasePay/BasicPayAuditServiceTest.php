<?php

namespace Tests\BasicPay;

use App\Audit\AuditService;
use App\BasePay\BasicPayAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class BasicPayAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => BasicPayAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new BasicPayAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => BasicPayAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new BasicPayAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1
        ]);
        $item = [
            'action' => BasicPayAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new BasicPayAuditService($mockAuditService);
        $auditService->log($item);
    }
}
