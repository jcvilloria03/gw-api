<?php

namespace Tests\Earnings;

use Tests\Common\CommonAuthorizationService;

class EarningsAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Earning\EarningAuthorizationService::class;
}
