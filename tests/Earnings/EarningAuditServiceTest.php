<?php

namespace Tests\Earning;

use App\Audit\AuditService;
use App\Earning\EarningAuditService;
use App\Earning\EarningRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class EarningAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => EarningAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
            'type' => 'App\Model\Earning'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockRequestService = m::mock(EarningRequestService::class);

        $annualEarningAuditService = new EarningAuditService(
            $mockAuditService,
            $mockRequestService
        );
        $annualEarningAuditService->logCreate($item);
    }
}
