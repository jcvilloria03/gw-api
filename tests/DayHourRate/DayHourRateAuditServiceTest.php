<?php

namespace Tests\DayHourRate;

use App\Audit\AuditService;
use App\DayHourRate\DayHourRateAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class DayHourRateAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            [
                'id' => 1,
                'company_id' => 1,
                'abreviation' => 'regular',
            ]
        ]);
        $item = [
            'action' => DayHourRateAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $projectAuditService = new DayHourRateAuditService($mockAuditService);
        $projectAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => DayHourRateAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $projectAuditService = new DayHourRateAuditService($mockAuditService);
        $projectAuditService->logUpdate($item);
    }
}
