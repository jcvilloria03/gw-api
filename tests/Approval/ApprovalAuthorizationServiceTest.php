<?php

namespace Tests\Approval;

use App\Approval\ApprovalAuthorizationService;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use App\Permission\TargetType;
use PHPUnit\Framework\TestCase;

class ApprovalAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeApproveValid()
    {
        $accountId = 1;
        $companyId = 1;

        $request = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope = new TaskScopes(ApprovalAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            'App\Approval\ApprovalAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeApprove($request, $user));
    }

    public function testAuthorizeApproveInvalid()
    {
        $accountId = 1;
        $companyId = 1;

        $request = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $scope = new Scope(TargetType::ACCOUNT, [4]);
        $taskScope = new TaskScopes(ApprovalAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $taskScope->addScope($scope);

        $authorizationService = $this->createMockAuthorizationService(
            'App\Approval\ApprovalAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeApprove($request, $user));
    }

}
