<?php

namespace Tests\Approval;

use App\Approval\EssApprovalAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssApprovalAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeApprove()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssApprovalAuthorizationService::APPROVE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Approval\EssApprovalAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeApprove($user['user_id']));
    }

    public function testFailToAuthorizeApprove()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Approval\EssApprovalAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeApprove($user['user_id']));
    }
}
