<?php

namespace Tests\Approval;

use App\Approval\ApprovalRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApprovalRequestServiceTest extends TestCase
{
    public function testGetUserApprovals()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new ApprovalRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getUserApprovals(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getUserApprovals(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUserApprovals(1, []);
    }
}
