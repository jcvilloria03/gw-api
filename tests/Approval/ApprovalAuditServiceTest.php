<?php

namespace Tests\Approval;

use App\Audit\AuditService;
use App\Approval\ApprovalAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ApprovalAuditServiceTest extends TestCase
{
    public function testLogApprove()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $request = json_encode([
            'id' => 1,
            'employee_id' => 11,
            'workflow_id' => 5,
            'position' => 1,
            'request_id' => 1,
            'request_type' => 1
        ]);
        $item = [
            'action' => ApprovalAuditService::ACTION_APPROVE_REQUEST,
            'user' => $user,
            'requests' => $request
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $approvalAuditService = new ApprovalAuditService($mockAuditService);
        $approvalAuditService->logApprove($item);
    }

    public function testLogDeny()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $request = json_encode([
            'id' => 1,
            'employee_id' => 11,
            'workflow_id' => 5,
            'position' => 1,
            'request_id' => 1,
            'request_type' => 1
        ]);
        $item = [
            'action' => ApprovalAuditService::ACTION_DENY_REQUEST,
            'user' => $user,
            'requests' => $request
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $approvalAuditService = new ApprovalAuditService($mockAuditService);
        $approvalAuditService->log($item);
    }
}
