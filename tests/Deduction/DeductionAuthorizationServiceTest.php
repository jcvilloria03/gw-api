<?php

namespace Tests\Deduction;

use Tests\Common\CommonAuthorizationService;

class DeductionAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Deduction\DeductionAuthorizationService::class;
}
