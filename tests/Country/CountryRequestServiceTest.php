<?php

namespace Tests\Country;

use App\Country\CountryRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class CountryRequestServiceTest extends TestCase
{
    public function testGetAll()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new CountryRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getAll();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
