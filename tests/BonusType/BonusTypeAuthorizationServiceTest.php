<?php

namespace Tests\BonusType;

use Tests\Common\CommonAuthorizationService;

class BonusTypeAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\BonusType\BonusTypeAuthorizationService::class;
}
