<?php

namespace Tests\Department;

use App\Department\DepartmentAuthorizationService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class DepartmentAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsPassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testGetCompanyDepartmentsInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyDepartments($targetDepartmentDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailableNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetDepartmentDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetDepartmentDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }


    public function testDeleteNoScope()
    {
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(DepartmentAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetDepartmentDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            DepartmentAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetDepartmentDetails, $user));
    }
}
