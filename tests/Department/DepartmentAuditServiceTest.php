<?php

namespace Tests\Department;

use App\Audit\AuditService;
use App\Department\DepartmentAuditService;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class DepartmentAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
        ]);
        $item = [
            'action' => DepartmentAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $departmentAuditService = new DepartmentAuditService($mockAuditService);
        $departmentAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
            'parent_id' => null
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
            'parent_id' => null
        ]);
        $item = [
            'action' => DepartmentAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $departmentAuditService = new DepartmentAuditService($mockAuditService);
        $departmentAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => DepartmentAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $departmentAuditService = new DepartmentAuditService($mockAuditService);
        $departmentAuditService->logDelete($item);
    }
}
