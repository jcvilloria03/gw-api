<?php

namespace Tests\Department;

use App\Department\DepartmentEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class DepartmentESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $departmentIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')->once();

        $departmentsESIndexQueueService = new DepartmentEsIndexQueueService($mockIndexQueueService);
        $departmentsESIndexQueueService->queue($departmentIds);
    }
}
