<?php

namespace Tests\Position;

use App\Position\PositionEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PositionESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $positionIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')->once();

        $positionsESIndexQueueService = new PositionEsIndexQueueService($mockIndexQueueService);
        $positionsESIndexQueueService->queue($positionIds);
    }
}
