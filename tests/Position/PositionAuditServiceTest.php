<?php

namespace Tests\Position;

use App\Audit\AuditService;
use App\Position\PositionAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PositionAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => PositionAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $positionAuditService = new PositionAuditService($mockAuditService);
        $positionAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
            'parent_id' => null,
            'department_id' => null
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'updatedName',
            'description' => 'description',
            'parent_id' => null,
            'department_id' => null
        ]);
        $item = [
            'action' => PositionAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $positionAuditService = new PositionAuditService($mockAuditService);
        $positionAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => PositionAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $positionAuditService = new PositionAuditService($mockAuditService);
        $positionAuditService->logDelete($item);
    }
}
