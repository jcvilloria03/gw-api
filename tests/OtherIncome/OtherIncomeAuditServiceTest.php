<?php

namespace Tests\OtherIncome;

use App\Audit\AuditService;
use App\OtherIncome\OtherIncomeAuditService;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class OtherIncomeAuditServiceTest extends TestCase
{
    const OTHER_INCOMES_JSON_EXAMPLE = '
    [
        {
          "id": 1,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineCommission",
          "recipients": {
            "employees": [
              {
                "id": 1,
                "other_income_id": 1,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:45:45",
                "updated_at": "2018-07-04 07:45:45",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 1,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false
            },
            {
              "id": 2,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineCommissionType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 2,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineCommission",
          "recipients": {
            "employees": [
              {
                "id": 2,
                "other_income_id": 2,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:51:15",
                "updated_at": "2018-07-04 07:51:15",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 3,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineCommissionType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 2,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "reason": "Something new",
          "type_name": "App\\\Model\\\PhilippineAdjustment",
          "recipients": {
            "employees": [
              {
                "id": 2,
                "other_income_id": 2,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:51:15",
                "updated_at": "2018-07-04 07:51:15",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 3,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineAdjustmentType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 1,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "auto_assign": false,
          "inactive": false,
          "recurring": 1,
          "credit_frequency": "DAILY",
          "prorated": 1,
          "prorated_by": "PRORATED_BY_DAY",
          "entitled_when": "ON_PAID_LEAVE",
          "valid_from": "2018-06-01",
          "valid_to": "2019-06-01",
          "type_name": "App\\\Model\\\PhilippineAllowance",
          "recipients": {
            "employees": [
              {
                "id": 1,
                "other_income_id": 1,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:45:45",
                "updated_at": "2018-07-04 07:45:45",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 1,
              "disburse_through_special_pay_run": true,
              "status": false
            },
            {
              "id": 2,
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineAllowanceType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 2,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "auto_assign": false,
          "inactive": false,
          "recurring": 1,
          "credit_frequency": "DAILY",
          "prorated": 1,
          "prorated_by": "PRORATED_BY_DAY",
          "entitled_when": "ON_PAID_LEAVE",
          "valid_from": "2018-06-01",
          "valid_to": "2019-06-01",
          "type_name": "App\\\Model\\\PhilippineAllowance",
          "recipients": {
            "employees": [
              {
                "id": 2,
                "other_income_id": 2,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:51:15",
                "updated_at": "2018-07-04 07:51:15",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 3,
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "AllowanceType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineAllowanceType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 1,
          "type_id": 7,
          "type": {
            "id": 7,
            "name": "BonusType",
            "company_id": 1,
            "fully_taxable": true,
            "max_non_taxable": null,
            "basis": "FIXED",
            "frequency": null
          },
          "company_id": 1,
          "amount": 1000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineBonus",
          "basis": "CURRENT_BASIC_SALARY",
          "recipients": {
            "employees": [
              {
                "id": 1,
                "other_income_id": 1,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:45:45",
                "updated_at": "2018-07-04 07:45:45",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 1,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false,
              "prorate_based_on_tenure": null,
              "coverage_from": null,
              "coverage_to": null,
              "amount": null
            },
            {
              "id": 2,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false,
              "prorate_based_on_tenure": null,
              "coverage_from": null,
              "coverage_to": null,
              "amount": null
            }
          ]
        },
        {
          "id": 2,
          "type_id": 7,
          "type": {
            "id": 7,
            "name": "BonusType",
            "company_id": 1,
            "fully_taxable": true,
            "max_non_taxable": null,
            "basis": "FIXED",
            "frequency": null
          },
          "company_id": 1,
          "amount": 1000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineBonus",
          "basis": "CURRENT_BASIC_SALARY",
          "recipients": {
            "employees": [
              {
                "id": 2,
                "other_income_id": 2,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:51:15",
                "updated_at": "2018-07-04 07:51:15",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 3,
              "date": "2017-01-01",
              "disburse_through_special_pay_run": true,
              "status": false,
              "prorate_based_on_tenure": null,
              "coverage_from": null,
              "coverage_to": null,
              "amount": null
            }
          ]
        },
        {
          "id": 53,
          "type_id": 7,
          "company_id": 1,
          "amount": 10000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineCommission",
          "recipients": {
            "employees": [
              {
                "id": 194,
                "other_income_id": 53,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-08-13 12:50:20",
                "updated_at": "2018-08-13 12:50:20",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 133,
              "date": "2018-06-01",
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineCommissionType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 54,
          "type_id": 7,
          "company_id": 1,
          "amount": 10000,
          "percentage": null,
          "auto_assign": false,
          "inactive": false,
          "type_name": "App\\\Model\\\PhilippineCommission",
          "recipients": {
            "employees": [
              {
                "id": 195,
                "other_income_id": 54,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-08-13 12:50:40",
                "updated_at": "2018-08-13 12:50:40",
                "deleted_at": null
              }
            ]
          },
          "release_details": [
            {
              "id": 134,
              "date": "2018-06-01",
              "disburse_through_special_pay_run": true,
              "status": false
            }
          ],
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineCommissionType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          }
        },
        {
          "id": 1,
          "type_id": 7,
          "company_id": 1,
          "amount": 1000,
          "inactive": false,
          "recurring": true,
          "frequency": "EVERY_PAY_OF_THE_MONTH",
          "valid_from": "2018-06-01",
          "valid_to": "2019-06-01",
          "type_name": "App\\\Model\\\PhilippineDeduction",
          "recipients": {
            "employees": [
              {
                "id": 1,
                "other_income_id": 1,
                "recipient_id": 1,
                "type": "employees",
                "created_at": "2018-07-04 07:45:45",
                "updated_at": "2018-07-04 07:45:45",
                "deleted_at": null
              }
            ]
          },
          "type": {
            "id": 7,
            "company_id": 1,
            "name": "CommissionType",
            "fully_taxable": true,
            "max_non_taxable": null,
            "other_income_type_subtype_id": 1,
            "other_income_type_subtype_type": "App\\\Model\\\PhilippineDeductionType",
            "created_at": "2018-07-04 07:45:32",
            "updated_at": "2018-07-04 07:45:32",
            "deleted_at": null,
            "other_income_type_subtype": {
              "id": 1,
              "basis": "FIXED",
              "created_at": "2018-07-04 07:45:32",
              "updated_at": "2018-07-04 07:45:32"
            }
          },
          "created_at": "2018-08-13 13:05:00"
        }
      ]
    ';

    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'name',
        ]);
        $item = [
            'action' => OtherIncomeAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
            'type' => 'App\Model\OtherIncome'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $otherIncomeAuditService = new OtherIncomeAuditService(
            $mockAuditService
        );
        $otherIncomeAuditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'name' => 'Old name',
        ]);
        $newData = json_encode([
            'id' => 1,
            'name' => 'New name',
        ]);
        $item = [
            'action' => OtherIncomeAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
            'type' => 'App\Model\OtherIncome'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new OtherIncomeAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1
        ]);
        $item = [
            'action' => OtherIncomeAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData,
            'type' => 'App\Model\OtherIncome'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $auditService = new OtherIncomeAuditService($mockAuditService);
        $auditService->log($item);
    }

    public function testMapAndQueueAuditItemsDeleteLogs()
    {
        $otherIncomes = json_decode(self::OTHER_INCOMES_JSON_EXAMPLE, true);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $auditService = new OtherIncomeAuditService($mockAuditService);

        $auditService->mapAndQueueAuditItems(
            $otherIncomes,
            OtherIncomeAuditService::ACTION_DELETE,
            [ 'user_id' => 1, 'account_id' => 1 ]
        );
    }

    public function testLogBatchCreate()
    {
        $user = [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];

        $items = [
            'action' => OtherIncomeAuditService::ACTION_BATCH_CREATE,
            'company_id' => 1,
            'user_id' => 1,
            'account_id' => 1,
            'type' => 'bonus',
            'other_income_ids' => [1,2,3]
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log');

        $mockRequestService = m::mock(OtherIncomeRequestService::class);
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'full_name' => 'Test name'
            ]
        ]));
        $mockRequestService
            ->shouldReceive('get')
            ->andReturn($response);

        $loanRequestAuditService = new OtherIncomeAuditService(
            $mockAuditService,
            $mockRequestService
        );

        $loanRequestAuditService->log($items, $user);
    }
}
