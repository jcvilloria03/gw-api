<?php

namespace Tests\OtherIncome;

use App\OtherIncome\OtherIncomeRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class OtherIncomeRequestServiceTest extends TestCase
{
    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1);
    }

    public function testGetAllCompanyIncomes()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getAllCompanyIncomes(1, 'bonus_type');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getAllCompanyIncomes(1, 'bonus_type');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getAllCompanyIncomes(1, 'bonus_type');
    }

    public function testGetEmployeeOtherIncomes()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getEmployeeOtherIncomes(1, 1, 'bonus_type');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getEmployeeOtherIncomes(1, 1, 'bonus_type');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getEmployeeOtherIncomes(1, 1, 'bonus_type');
    }

    public function testIsDeleteAvailable()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->IsDeleteAvailable(1, [ 1 ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->IsDeleteAvailable(1, [ 1 ]);
    }

    public function testDelete()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_NO_CONTENT, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new OtherIncomeRequestService($client);

        // test Response::HTTP_NO_CONTENT
        $response = $requestService->deleteMultiple(1, [ 1 ]);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $response = $requestService->deleteMultiple(1, [ 1 ]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->deleteMultiple(1, [ 1 ]);
    }

    public function testUploadPreview()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $requestService = new OtherIncomeRequestService($client);
        // test Response::HTTP_OK
        $response = $requestService->getUploadPreview('test');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getUploadPreview('test');
        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUploadPreview('test');
    }

    public function testDownloadOtherIncomesCsv()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $requestService = new OtherIncomeRequestService($client);
        // test Response::HTTP_OK
        $response = $requestService->downloadOtherIncomesCsv(1, 'bonus_type', [ 1 ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $response = $requestService->downloadOtherIncomesCsv(1, 'bonus_type', [ 1 ]);
        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->downloadOtherIncomesCsv(1, 'bonus_type', [ 1 ]);
        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->downloadOtherIncomesCsv(1, 'bonus_type', [ 1 ]);
    }
}
