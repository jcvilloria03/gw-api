<?php

namespace Tests\OtherIncome;

use App\Allowance\AllowanceAuthorizationService;
use App\Bonus\BonusAuthorizationService;
use App\Commission\CommissionAuthorizationService;
use App\Deduction\DeductionAuthorizationService;
use App\OtherIncome\OtherIncomeAuthorizationServiceFactory;
use PHPUnit\Framework\TestCase;

class OtherIncomeAuthorizationServiceFactoryTest extends TestCase
{

    public function testGet()
    {
        $result = OtherIncomeAuthorizationServiceFactory::get(
            OtherIncomeAuthorizationServiceFactory::PHILIPPINE_ALLOWANCE
        );
        $this->assertInstanceOf(AllowanceAuthorizationService::class, $result);

        $result = OtherIncomeAuthorizationServiceFactory::get(
            OtherIncomeAuthorizationServiceFactory::PHILIPPINE_BONUS
        );
        $this->assertInstanceOf(BonusAuthorizationService::class, $result);

        $result = OtherIncomeAuthorizationServiceFactory::get(
            OtherIncomeAuthorizationServiceFactory::PHILIPPINE_COMMISSION
        );
        $this->assertInstanceOf(CommissionAuthorizationService::class, $result);

        $result = OtherIncomeAuthorizationServiceFactory::get(
            OtherIncomeAuthorizationServiceFactory::PHILIPPINE_DEDUCTION
        );
        $this->assertInstanceOf(DeductionAuthorizationService::class, $result);

        $this->expectException(\Exception::class);
        OtherIncomeAuthorizationServiceFactory::get(
            'something wrong'
        );
    }
}
