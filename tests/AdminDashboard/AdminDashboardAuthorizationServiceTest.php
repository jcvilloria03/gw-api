<?php

namespace Tests\AdminDashboard;

use App\AdminDashboard\AdminDashboardAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

/**
 * Class AdminDashboardAuthorizationServiceTest
 *
 * @package Tests\AdminDashboard
 */
class AdminDashboardAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    protected $authorizationServiceClass = \App\AdminDashboard\AdminDashboardAuthorizationService::class;
    protected $authorizationService;

    public function setUp()
    {
        parent::setUp();

        if (!$this->authorizationServiceClass) {
            throw new \Exception('Please set $authorizationServiceClass property!');
        }
        $this->authorizationService = app()->make($this->authorizationServiceClass);
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetExportPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }

    public function testGetExportInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->exportTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeExportData($targetCommonDetails, $user));
    }


    public function testGetAllPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }
}
