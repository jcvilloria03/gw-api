<?php

namespace Tests\AdminDashboard;

use App\AdminDashboard\AdminDashboardRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class AdminDashboardRequestServiceTest
 *
 * @package Tests\AdminDashboard
 */
class AdminDashboardRequestServiceTest extends TestCase
{
    public function testGetAllCalendarData()
    {
        $mock = new MockHandler(
            [
                new GuzzleResponse(Response::HTTP_OK, []),
                new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
                new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
                new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
            ]
        );

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AdminDashboardRequestService($client);
        $params = [
            'company_id' => 1,
            'start_date' => '2016-03-03',
            'end_date' => '2016-03-03',
            'group' => 'payroll'
        ];

        // test Response::HTTP_OK
        $response = $requestService->getAllCalendarData($params);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarData($params);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarData($params);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarData($params);
    }

    public function testGetAllCalendarDataCounts()
    {
        $mock = new MockHandler(
            [
                new GuzzleResponse(Response::HTTP_OK, []),
                new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
                new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
                new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
            ]
        );

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AdminDashboardRequestService($client);
        $params = [
            'company_id' => 1,
            'start_date' => '2016-03-03',
            'end_date' => '2016-03-03',
            'group' => 'payroll'
        ];

        // test Response::HTTP_OK
        $response = $requestService->getAllCalendarDataCounts($params);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarDataCounts($params);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarDataCounts($params);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getAllCalendarDataCounts($params);
    }

    public function testExportScorecard()
    {
        $mock = new MockHandler(
            [
                new GuzzleResponse(Response::HTTP_OK, []),
                new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
                new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
                new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
            ]
        );

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AdminDashboardRequestService($client);
        $id = 1; // company_id
        $params = [
            'export_type' => 'present',
            'employees_ids' => []
        ];

        // test Response::HTTP_OK
        $response = $requestService->downloadCsv($id, $params);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->downloadCsv($id, $params);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->downloadCsv($id, $params);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->downloadCsv($id, $params);
    }

    public function testGetDashboardStatistics()
    {
        $mock = new MockHandler(
            [
                new GuzzleResponse(Response::HTTP_OK, []),
                new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
                new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
                new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
            ]
        );

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AdminDashboardRequestService($client);
        $params = [
            'company_id' => 1
        ];

        // test Response::HTTP_OK
        $response = $requestService->getDashboardStatistics($params);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getDashboardStatistics($params);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getDashboardStatistics($params);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getDashboardStatistics($params);
    }
}
