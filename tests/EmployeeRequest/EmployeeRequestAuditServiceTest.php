<?php

namespace Tests\EmployeeRequest;

use App\Audit\AuditService;
use App\EmployeeRequest\EmployeeRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class EmployeeRequestAuditServiceTest extends TestCase
{
    public function testLogSendMessage()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'content' => 'test content',
            'request_id' => 1,
        ]);
        $item = [
            'action' => EmployeeRequestAuditService::ACTION_SEND_MESSAGE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeRequestAuditService = new EmployeeRequestAuditService($mockAuditService);
        $employeeRequestAuditService->logSendMessage($item);
    }

    public function testLogUpdateStatus()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'content' => 'test content',
            'request_id' => 1,
        ]);
        $item = [
            'action' => EmployeeRequestAuditService::ACTION_UPDATE_STATUS,
            'user' => $user,
            'new' => $newData,
            'new_status' => 'approve'
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeRequestAuditService = new EmployeeRequestAuditService($mockAuditService);
        $employeeRequestAuditService->logUpdateStatus($item);
    }

    public function testLogCancelRequest()
    {
        $user = json_encode([
            'id' => 1,
            'employee_id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $request = json_encode([
            'id' => 1,
            'request_id' => 1,
            'request_type' => 'leave_request'
        ]);
        $item = [
            'action' => EmployeeRequestAuditService::ACTION_CANCEL_REQUEST,
            'user' => $user,
            'request' => $request
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeRequestAuditService = new EmployeeRequestAuditService($mockAuditService);
        $employeeRequestAuditService->logCancelRequest($item);
    }
}
