<?php

namespace Tests\EmployeeRequest;

use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssEmployeeRequestAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeViewEmployeeRequests()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($user));
    }

    public function testAuthorizeCreateEmployeeRequests()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($user));
    }

    public function testFailToAuthorizeViewEmployeeRequests()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($user));
    }

    public function testAuthorizeUpdateEmployeeRequests()
    {
        $user = [ 'user_id' => 1 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($user));
    }

    public function testFailToAuthorizeUpdateEmployeeRequests()
    {
        $user = [ 'user_id' => 1 ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($user));
    }

    public function testAuthorizeAttachmentUpload()
    {
        $user = [
            'user_id' => 1,
            'employee_id' => 1
        ];
        $employeeId = 1;
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($user, $employeeId));
    }

    public function testFailToAuthorizeAttachmentUpload()
    {
        $user = [
            'user_id' => 1,
            'employee_id' => 1
        ];
        $employeeId = 1;
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($user, $employeeId));
    }

    public function testAuthorizeViewSingleRequestValid()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 2 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $workflows = [
            [
                'workflow_id' => 1,
                'position' => 3,
                'workflow_level_id' => 12
            ]
        ];

        $leaveRequest= new \stdClass();
        $leaveRequest->employee_id = 2;
        $leaveRequest->workflow_id = 1;
        $this->assertTrue($authorizationService->authorizeViewSingleRequest($user, $workflows, $leaveRequest));
    }

    public function testAuthorizeViewSingleRequestFails()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 2 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $workflows = [
            [
                'workflow_id' => 4,
                'position' => 3,
                'workflow_level_id' => 12
            ]
        ];

        $leaveRequest= new \stdClass();
        $leaveRequest->employee_id = 4;
        $leaveRequest->workflow_id = 1;
        $this->assertFalse($authorizationService->authorizeViewSingleRequest($user, $workflows, $leaveRequest));
    }

    public function testAuthorizeSendMessageValid()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 2 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $workflows = [
            [
                'workflow_id' => 1,
                'position' => 3,
                'workflow_level_id' => 12
            ]
        ];

        $leaveRequest= new \stdClass();
        $leaveRequest->employee_id = 2;
        $leaveRequest->workflow_id = 1;
        $this->assertTrue($authorizationService->authorizeSendMessage($user, $workflows, $leaveRequest));
    }

    public function testAuthorizeSendMessageFails()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 2 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $workflows = [
            [
                'workflow_id' => 4,
                'position' => 3,
                'workflow_level_id' => 12
            ]
        ];

        $leaveRequest= new \stdClass();
        $leaveRequest->employee_id = 4;
        $leaveRequest->workflow_id = 1;
        $this->assertFalse($authorizationService->authorizeSendMessage($user, $workflows, $leaveRequest));
    }

    public function testAuthorizeCancelEmployeeRequests()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 4 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::CANCEL_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $employeeRequest = new \stdClass();
        $employeeRequest->employee_id = 4;
        $employeeRequest->workflow_id = 1;
        $this->assertTrue($authorizationService->authorizeCancel($user, $employeeRequest));
    }

    public function testFailToAuthorizeCancelEmployeeRequests()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 4 ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            null
        );
        $employeeRequest = new \stdClass();
        $employeeRequest->employee_id = 4;
        $employeeRequest->workflow_id = 1;
        $this->assertFalse($authorizationService->authorizeCancel($user, $employeeRequest));
    }

    public function testAuthorizeCancelOtherEmployeeEmployeeRequests()
    {
        $user = [ 'user_id' => 1, 'employee_id' => 4 ];
        $taskScope = new TaskScopes(EssEmployeeRequestAuthorizationService::CANCEL_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\EmployeeRequest\EssEmployeeRequestAuthorizationService',
            $taskScope
        );
        $employeeRequest = new \stdClass();
        $employeeRequest->employee_id = 2;
        $employeeRequest->workflow_id = 1;
        $this->assertFalse($authorizationService->authorizeCancel($user, $employeeRequest));
    }
}
