<?php

namespace Tests\TimeRecord;

use App\Audit\AuditService;
use App\TimeRecord\TimeRecordAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class TimeRecordAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_id' => 1,
            'company_id' => 1,
            'timestamp' => 123456789
        ]);
        $item = [
            'action' => TimeRecordAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $timeRecordAuditService = new TimeRecordAuditService($mockAuditService);
        $timeRecordAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'employee_id' => 1,
            'company_id' => 1,
            'timestamp' => 123456789
        ]);
        $item = [
            'action' => TimeRecordAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $timeRecordAuditService = new TimeRecordAuditService($mockAuditService);
        $timeRecordAuditService->logDelete($item);
    }
}
