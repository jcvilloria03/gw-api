<?php

namespace Tests\TimeRecord;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\TimeRecord\TimeRecordAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class TimeRecordAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testAuthorizeViewTimeRecordsPassAccountLevel()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeViewTimeRecordsInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewTimeRecords($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeleteNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeleteInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $accountId = 1;
        $companyId = 1;
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeleteInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeleteInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }

    public function testAuthorizeBulkCreateOrDeleteInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(TimeRecordAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['T%A']);
        $targetTimeRecordDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            TimeRecordAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeBulkCreateOrDelete($targetTimeRecordDetails, $user));
    }
}
