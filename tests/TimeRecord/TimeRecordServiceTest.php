<?php

namespace Tests\TimeRecord;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Mockery;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;
use App\TimeRecord\TimeRecordService;
use App\TimeRecord\TimeRecordRequestService;

class TimeRecordServiceTest extends TestCase
{
    public function testCreateValid()
    {
        $expected = [
            'item' => [
                'employee_uid' => [
                    'N' => '703'
                ],
                'timestamp' => [
                    'N' => '1526593252'
                ]
            ]
        ];
        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('create')
            ->once()
            ->andReturn(new JsonResponse(json_encode($expected)));
        $service = new TimeRecordService($mockRequestService);
        $actual = $service->create([]);
        $this->assertEquals($expected['item'], $actual);
    }

    public function testCreateInvalid()
    {
        Log::shouldReceive('error')->once();
        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('create')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));
        $service = new TimeRecordService($mockRequestService);
        $actual = $service->create([]);
        $this->assertEmpty($actual);
    }

    public function testDeleteValid()
    {
        $expected = [
            'RequestId' => ''
        ];
        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('delete')
            ->once()
            ->andReturn(new JsonResponse(json_encode($expected)));
        $service = new TimeRecordService($mockRequestService);
        $actual = $service->delete([]);
        $this->assertEquals($expected, $actual);
    }

    public function testDeleteInvalid()
    {
        Log::shouldReceive('error')->once();
        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('delete')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));
        $service = new TimeRecordService($mockRequestService);
        $actual = $service->delete([]);
        $this->assertEmpty($actual);
    }

    public function testBulkCreateOrDelete()
    {
        $this->markTestSkipped('Follow testsNew instead.');
        $companyId = 1;

        $deleteExpected = [
            'metadata' => []
        ];

        $createExpected = [
            'item' => [
                'employee_id' => 1,
                'company_id' => 1,
                'timestamp' => 123456789
            ]
        ];

        $data = [
            'create' => [
                [
                    'employee_id' => 1,
                    'company_id' => 1,
                    'timestamp' => 123456789,
                    'type' => 'clock_in',
                ]
            ],
            'delete' => [
                [
                    'employee_id' => 1,
                    'timestamp' => 123456789
                ]
            ]
        ];

        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('delete')
            ->once()
            ->andReturn(new JsonResponse(json_encode($deleteExpected)))
            ->shouldReceive('create')
            ->once()
            ->andReturn(new JsonResponse(json_encode($createExpected)));

        $service = new TimeRecordService($mockRequestService);
        $responses = $service->bulkCreateOrDelete($data, $companyId);

        $this->assertCount(1, $responses['created']);
        $this->assertEquals($responses['created'][0], $createExpected['item']);
        $this->assertCount(1, $responses['deleted']);
        $this->assertEquals($responses['deleted'][0], $deleteExpected);
    }

    public function testGetTimesheetForEmployee()
    {
        $expected = [];

        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('getTimesheet')
            ->andReturn(new JsonResponse(json_encode($expected)));

        $this->app->instance(TimeRecordRequestService::class, $mockRequestService);

        $service = new TimeRecordService($mockRequestService);

        $actual = $service->getTimesheetForEmployee(1);

        $this->assertEquals($expected, $actual);
    }

    public function testGetTimesheetForEmployeeInvalid()
    {
        Log::shouldReceive('error')->once();
        $mockRequestService = Mockery::mock(TimeRecordRequestService::class);
        $mockRequestService
            ->shouldReceive('getTimesheet')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));
        $service = new TimeRecordService($mockRequestService);
        $actual = $service->getTimesheetForEmployee(1);
        $this->assertEmpty($actual);
    }
}
