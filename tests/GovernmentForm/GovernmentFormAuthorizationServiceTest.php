<?php

namespace Tests\GovernmentForm;

use App\GovernmentForm\GovernmentFormAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class GovernmentFormAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testCreatePass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreatePassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreatePassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCompany, $user));
    }

    public function testViewPass()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewPassCompanyScopeAll()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewPassCompanyScopeSpecific()
    {
        $targetCompanyId = 2;
        $targetCompany = (object) [
            'id' => $targetCompanyId,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, [$targetCompanyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewNoScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewInvalidAccountScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewInvalidCompanyScope()
    {
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $scope = new Scope(TargetType::COMPANY, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($targetCompany, $user));
    }

    public function testViewInvalidOtherScope()
    {
        $taskScope = new TaskScopes(GovernmentFormAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompany = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\GovernmentForm\GovernmentFormAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($targetCompany, $user));
    }
}
