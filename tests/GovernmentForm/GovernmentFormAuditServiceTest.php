<?php

namespace Tests\GovernmentForm;

use App\GovernmentForm\GovernmentFormAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class GovernmentFormAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $data = json_encode([
            'company_id' => 1,
        ]);
        $item = [
            'action' => GovernmentFormAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $data
        ];
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $governmentFormAuditService = new GovernmentFormAuditService($mockAuditService);
        $governmentFormAuditService->logCreate($item);
    }
}
