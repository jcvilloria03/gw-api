<?php

namespace Tests\GovernmentForm;

use App\GovernmentForm\GovernmentFormRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GovernmentFormRequestServiceTest extends TestCase
{
    public function testGetGovernmentFormPeriods()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getGovernmentFormPeriods(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getGovernmentFormPeriods(1);
    }

    public function testGetSssR5Methods()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSssR5Methods();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetSssR5FormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSssR5FormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetSssR5FormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSssR5FormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetSSSLoanCollectionValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSssLoanCollectionValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateSssR5()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateSssR5([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateSssCollection()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateSssCollection([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetSssCollectionFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSssLoanCollectionFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetHdmfMsrfChannels()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getHdmfMsrfChannels();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetHmdfMsrfFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getHdmfMsrfFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetHdmfMsrfFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getHdmfMsrfFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetStlrfFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getStlrfFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetStlrfFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getStlrfFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateHdmfMsrf()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateHdmfMsrf([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateStlrf()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateStlrf([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetPhilhealthFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getPhilhealthFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601cFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601cFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateBir1601c()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateBir1601C([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601CFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601CFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601EFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601EFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601EFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601EFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateBir1601E()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateBir1601E([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601fFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601fFormOptions();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetBir1601FFormValues()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getBir1601FFormValues([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGenerateBir1601f()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new GovernmentFormRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->generateBir1601F([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
