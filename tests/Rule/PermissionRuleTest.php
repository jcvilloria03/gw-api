<?php

namespace Tests\Rule;

use App\Model\Task;
use App\Rule\PermissionRule;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PermissionRuleTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    private $mockTask;

    private function getInstance()
    {
        $this->mockTask = Task::create([
            'name' => 'test.task.a',
            'display_name' => 'TEST TASK A',
            'description' => 'TEST TASK A',
            'module' => 'TEST MODULE',
            'submodule' => 'TEST SUBMODULE',
            'allowed_scopes' => json_encode(['Account' => [1]]),
            'ess' => true,
        ]);

        return new PermissionRule();
    }

    public function testValidRule()
    {
        $rule = $this->getInstance();

        $attributes = [
            'task_id' => $this->mockTask->id,
            'scopes' => [
                [
                    'type' => 'Company',
                    'targets' => [1],
                ],
            ],
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertEmpty($validator->errors()->all());
    }

    public function testValidateRequiredTaskId()
    {
        $rule = $this->getInstance();

        $attributes = [
            'scopes' => [
                [
                    'type' => 'Company',
                    'targets' => [1],
                ],
            ],
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }

    public function testValidateInvalidTask()
    {
        $rule = $this->getInstance();

        $attributes = [
            'task_id' => 9999999,
            'scopes' => [
                [
                    'type' => 'Company',
                    'targets' => [1],
                ],
            ],
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }
}
