<?php

namespace Tests\Rule;

use App\Rule\ScopeRule;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ScopeRuleTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    private function getInstance()
    {
        return new ScopeRule();
    }

    public function testValidRule()
    {
        $rule = $this->getInstance();

        $attributes = [
            'type' => 'Account',
            'targets' => [1],
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertEmpty($validator->errors()->all());
    }

    public function testValidateRequired()
    {
        $rule = $this->getInstance();

        $attributes = [
            'type' => '',
            'targets' => 'all',
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }

    public function testValidateInvalidScopeType()
    {
        $rule = $this->getInstance();

        $attributes = [
            'type' => 'InvalidScopeTypeA',
            'targets' => [1],
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }

    public function testValidateTargetAll()
    {
        $rule = $this->getInstance();

        $attributes = [
            'type' => 'Company',
            'targets' => 'all',
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertEmpty($validator->errors()->all());
    }
}
