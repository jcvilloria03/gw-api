<?php

namespace Tests\Rule;

use App\Rule\TimeRecordBulkCreateOrDeleteRule;

class TimeRecordBulkCreateOrDeleteRuleTest extends \Tests\TestCase
{
    public function testValidRule()
    {
        $rule = new TimeRecordBulkCreateOrDeleteRule();

        $attributes = [
            'company_id' => 1,
            'create' => [
                [
                    'employee_id' => 1,
                    'type' => 'clock_in',
                    'tags' => ['test tag'],
                    'timestamp' => 123456789
                ]
            ],
            'delete' => [
                [
                    'employee_id' => 1,
                    'timestamp' => 123456789
                ]
            ]
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertEmpty($validator->errors()->all());
    }

    public function testInvalidRule()
    {
        $rule = new TimeRecordBulkCreateOrDeleteRule();

        $attributes = [
            'company_id' => 'invalid',
            'create' => [
                [
                    'employee_id' => 'invalid',
                    'type' => 'invalid',
                    'tags' => 'invalid',
                    'timestamp' => 'invalid'
                ]
            ],
            'delete' => [
                [
                    'employee_id' => 'invalid',
                    'timestamp' => 'invalid'
                ]
            ]
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertEquals(7, count($validator->errors()->all()));
    }
}
