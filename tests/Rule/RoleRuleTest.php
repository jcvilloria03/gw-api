<?php

namespace Tests\Rule;

use App\Model\Role;
use App\Rule\RoleRule;
use App\Task\TaskService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RoleRuleTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    private function getInstance()
    {
        $attributes = [
            'account_id' => 2,
            'company_id' => 1,
            'name' => 'test existing custom role',
            'custom_role' => 1,
        ];

        return new RoleRule($attributes);
    }

    public function testValidRule()
    {
        $rule = $this->getInstance();

        $service = new TaskService();
        $tasks = $service->getOwnerTasks();

        $attributes = [
            'name' => 'test custom role a',
            'tasks' => $tasks->pluck('id')->toArray(),
            'selected_modules' => ['HRIS'],
            'company_id' => 10,
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );
        $this->assertEmpty($validator->errors()->all());
    }

    public function testValidRequiredFields()
    {
        $rule = $this->getInstance();

        $attributes = [];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $expected = 3;
        $actual = count($validator->errors()->all());

        $this->assertEquals($expected, $actual);
    }

    public function testValidateInvalidEmployeeCompany()
    {
        $rule = $this->getInstance();

        $attributes = [
            'name' => 'test custom role a',
            'permissions' => [
                [
                    'task_id' => 10,
                    'scopes' => [
                        [
                            'type' => 'Company',
                            'targets' => [1],
                        ],
                    ],
                ],
            ],
            'employee_company_id' => 99999,
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }

    public function testValidateEmptyPermissions()
    {
        $rule = $this->getInstance();

        $attributes = [
            'name' => 'test custom role a',
            'permissions' => [],
            'employee_company_id' => 10,
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }

    public function testValidateUniqueRoleNamePerAccount()
    {
        $rule = $this->getInstance();

        Role::create([
            'account_id' => 1,
            'company_id' => 2,
            'name' => 'TEST CUSTOM ROLE A',
            'type' => 'Admin',
            'custom_role' => true,
        ]);

        Role::create([
            'account_id' => 2,
            'company_id' => 2,
            'name' => 'TEST CUSTOM ROLE B',
            'type' => 'Admin',
            'custom_role' => true,
        ]);

        $attributes = [
            'name' => 'TEST CUSTOM ROLE B',
            'permissions' => [],
            'employee_company_id' => 10,
        ];

        $validator = app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages()
        );

        $this->assertNotEmpty($validator->errors()->all());
    }
}
