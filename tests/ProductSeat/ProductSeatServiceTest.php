<?php

namespace Tests\ProductSeat;

use Mockery;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\ProductSeat\ProductSeatService;
use App\ProductSeat\ProductSeatRequestService;
use Aws\Handler\GuzzleV5\GuzzleHandler;
use Aws\MockHandler;
use GuzzleHttp\HandlerStack;

class ProductSeatServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testGetAllAvailable()
    {
        $mockProductService = Mockery::mock(ProductSeatService::class);
        $mockProductService->shouldReceive('getAllAvailable')
            ->andReturn(
                [
                    'data' => [
                        [
                            'name' => 'time and attendance',
                            'id' => 2
                        ],
                        [
                            'name' => 'payroll',
                            'id' => 3
                        ],
                        [
                            'name' => 'time and attendance plus payroll',
                            'id' => 4
                        ]
                    ]
                ]
            );
        $productSeats = $mockProductService->getAllAvailable();
        $this->assertCount(3, $productSeats['data']);
    }

    public function testGetTimeAndAttendanceId()
    {
        $mockProductService = Mockery::mock(ProductSeatService::class)->makePartial();
        $mockProductService->shouldReceive('getAllAvailable')
            ->andReturn(
                [
                    'data' => [
                        [
                            'name' => 'time and attendance',
                            'id' => 2
                        ],
                        [
                            'name' => 'payroll',
                            'id' => 3
                        ],
                        [
                            'name' => 'time and attendance plus payroll',
                            'id' => 4
                        ]
                    ]
                ]
            );

        $this->assertEquals(2, $mockProductService->getTimeAndAttendanceId());
    }
}
