<?php

namespace Tests\ProductSeat;

use App\ProductSeat\ProductSeatRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;
use PHPUnit\Framework\TestCase;

class ProductSeatRequestServiceTest extends TestCase
{
    public function testGetAllAvailable()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new ProductSeatRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getAllAvailable();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetProductSeatsDetails()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new ProductSeatRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getProductSeatsDetails(1, [1], null);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
