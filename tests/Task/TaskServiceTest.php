<?php

namespace Tests\Task;

use App\Model\Task;
use App\Task\TaskService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TaskServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testGetOwnerTasks()
    {
        $service = new TaskService();
        $tasks = $service->getOwnerTasks();
        $this->assertNotEmpty($tasks);
        $this->assertGreaterThan(0, $tasks->count());
        $this->assertFalse($tasks->contains('ess', true));
        $this->assertTrue($tasks->contains('allowed_scopes', json_decode(Task::ACCOUNT_ONLY_SCOPE)));
    }

    public function testGetAdminTasks()
    {
        $service = new TaskService();
        $tasks = $service->getAdminTasks();
        $this->assertNotEmpty($tasks);
        $this->assertGreaterThan(0, $tasks->count());
        $this->assertFalse($tasks->contains('ess', true));
        $this->assertFalse($tasks->contains('allowed_scopes', json_decode(Task::ACCOUNT_ONLY_SCOPE)));
    }

    public function testGetEmployeeTasks()
    {
        $service = new TaskService();
        $tasks = $service->getEmployeeTasks();
        $this->assertNotEmpty($tasks);
        $this->assertGreaterThan(0, $tasks->count());
        $this->assertFalse($tasks->contains('ess', false));
    }

    public function testGetTasksArrangedByModules()
    {
        $service = new TaskService();
        $tasks = $service->getTasksArrangedByModules();
        $this->assertNotEmpty($tasks);
    }

    public function testGetSuperAdminTasks()
    {
        $service = new TaskService();
        $tasks = $service->getSuperAdminTasks();
        $this->assertNotEmpty($tasks);
        $this->assertGreaterThan(0, $tasks->count());
        $this->assertTrue($tasks->contains('name', 'view.subscription_invoices'));
    }
}
