<?php

namespace Tests\SpecialPayRun;

use Tests\Common\CommonAuthorizationService;

class SpecialPayRunAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\SpecialPayRun\SpecialPayRunAuthorizationService::class;
}
