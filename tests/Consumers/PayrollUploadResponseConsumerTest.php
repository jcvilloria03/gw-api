<?php

namespace Tests\Consumers;

use App\Payroll\PayrollUploadTask;
use App\Consumers\PayrollUploadResponseConsumer;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Mockery as m;

class PayrollUploadResponseConsumerTest extends \Tests\TestCase
{
    public function testCallbackAttendanceValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ATTENDANCE
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayrollAttendanceUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateAttendanceUploadTask]');
        $mockConsumer
            ->shouldReceive('generateAttendanceUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackBonusValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::BONUS
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayrollBonusUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateBonusUploadTask]');
        $mockConsumer
            ->shouldReceive('generateBonusUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackCommissionValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::COMMISSION
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayrollCommissionUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateCommissionUploadTask]');
        $mockConsumer
            ->shouldReceive('generateCommissionUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackDeductionValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::DEDUCTION
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayslipDeductionUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateDeductionUploadTask]');
        $mockConsumer
            ->shouldReceive('generateDeductionUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackAllowanceValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayrollAllowanceUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateAllowanceUploadTask]');
        $mockConsumer
            ->shouldReceive('generateAllowanceUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackSave()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => PayrollUploadTask::PROCESS_SAVE,
            'status' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask =  new class() {
            public function create($a, $b)
            {
                return $a + $b;
            }
            public function updateSaveStatus($c)
            {
                return $c;
            }
        };
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateAllowanceUploadTask]');
        $mockConsumer
            ->shouldReceive('generateAllowanceUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackErrorFileS3Key()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => PayrollUploadTask::PROCESS_SAVE,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE,
            'status' => 1,
            'error_file_s3_key' => 'key',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask =  new class() {
            public function create($a, $b)
            {
                return $a + $b;
            }
            public function updateSaveStatus($c)
            {
                return $c;
            }
            public function updateErrorFileLocation($d)
            {
                return $d;
            }
        };
        $mockConsumer = m::mock('App\Consumers\PayrollUploadResponseConsumer[generateAllowanceUploadTask]');
        $mockConsumer
            ->shouldReceive('generateAllowanceUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'payroll_id' => 1,
            'task' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyCompanyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyTask()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'payroll_type' => PayrollUploadResponseConsumer::ALLOWANCE,
            'task' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyPayrollType()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackInvalidPayrollType()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'task' => 1,
            'status' => 1,
            'payroll_type' => 'invalid',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Log::shouldReceive('error')
            ->once();

        $consumer = new PayrollUploadResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateAttendanceUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);
        $consumer = new PayrollUploadResponseConsumer();
        $task = $consumer->generateAttendanceUploadTask();
        $this->assertEquals('App\Payroll\PayrollAttendanceUploadTask', get_class($task));
    }

    public function testGenerateAllowanceUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);
        $consumer = new PayrollUploadResponseConsumer();
        $task = $consumer->generateAllowanceUploadTask();
        $this->assertEquals('App\Payroll\PayrollAllowanceUploadTask', get_class($task));
    }

    public function testGenerateBonusUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);
        $consumer = new PayrollUploadResponseConsumer();
        $task = $consumer->generateBonusUploadTask();
        $this->assertEquals('App\Payroll\PayrollBonusUploadTask', get_class($task));
    }

    public function testGenerateCommissionUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);
        $consumer = new PayrollUploadResponseConsumer();
        $task = $consumer->generateCommissionUploadTask();
        $this->assertEquals('App\Payroll\PayrollCommissionUploadTask', get_class($task));
    }

    public function testGenerateDeductionUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);
        $consumer = new PayrollUploadResponseConsumer();
        $task = $consumer->generateDeductionUploadTask();
        $this->assertEquals('App\Payroll\PayrollDeductionUploadTask', get_class($task));
    }
}
