<?php

namespace Tests\Consumers;

use App\Consumers\BroadcastNotificationConsumer;
use App\Events\UserNotificationReceived;
use Illuminate\Support\Facades\Event;

class BroadcastNotificationConsumerTest extends \Tests\TestCase
{
    public function testCallback()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'user_id' => 1,
            'notification' => ['body' => 'test'],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastNotificationConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertDispatched(UserNotificationReceived::class);
    }

    public function testCallbackEmptyUserId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'notification' => ['body' => 'test'],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastNotificationConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertNotDispatched(UserNotificationReceived::class);
    }

    public function testCallbackEmptyNotification()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'user_id' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastNotificationConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertNotDispatched(UserNotificationReceived::class);
    }
}
