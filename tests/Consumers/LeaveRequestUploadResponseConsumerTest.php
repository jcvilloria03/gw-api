<?php

namespace Tests\Consumers;

use App\Consumers\LeaveRequestUploadResponseConsumer;
use Illuminate\Http\JsonResponse;
use Mockery as m;

class LeaveRequestUploadResponseConsumerTest extends \Tests\TestCase
{
    public function setUp()
    {
        parent::setUp();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->andReturn($mockS3Client);
        app()->instance('aws', $dependency);
    }

    public function testCallbackOnValidValidationWithErrorFileLocation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 'validation',
            'status' => 'validated',
            'company_id' => 1,
            'error_file_s3_key' => 's3_key'
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\LeaveRequest\LeaveRequestUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once()
            ->shouldReceive('updateErrorFileLocation')
            ->once();

        $auditService = m::mock('App\LeaveRequest\LeaveRequestAuditService');
        $userService = m::mock('App\User\UserRequestService');

        $mockConsumer = m::mock(
            'App\Consumers\LeaveRequestUploadResponseConsumer[generateTask]',
            [
                $auditService,
                $userService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackOnValidSave()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 'save',
            'status' => 'saved',
            'company_id' => 1,
            'leave_requests_ids' => [1, 2]
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\LeaveRequest\LeaveRequestUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once()
            ->shouldReceive('getUserId')
            ->once()
            ->andReturn(1);

        $auditService = m::mock('App\LeaveRequest\LeaveRequestAuditService');
        $auditService
            ->shouldReceive('logBatchCreate')
            ->once();

        $userService = m::mock('App\User\UserRequestService');
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'name' => 'userName',
            'account_id' => 2
        ]));
        $userService
            ->shouldReceive('get')
            ->once()
            ->andReturn($response);

        $mockConsumer = m::mock(
            'App\Consumers\LeaveRequestUploadResponseConsumer[generateTask]',
            [
                $auditService,
                $userService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackOnIncompletedMessage()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'task' => 'save',
            'status' => 'saved',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\LeaveRequest\LeaveRequestAuditService');
        $userService = m::mock('App\User\UserRequestService');
        $mockConsumer = m::mock(
            'App\Consumers\LeaveRequestUploadResponseConsumer[generateTask]',
            [
                $auditService,
                $userService
            ]
        );

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testGenerateTask()
    {
        $auditService = m::mock('App\LeaveRequest\LeaveRequestAuditService');
        $userService = m::mock('App\User\UserRequestService');
        $consumer = new LeaveRequestUploadResponseConsumer($auditService, $userService);

        $task = $consumer->generateTask();
        $this->assertEquals('App\LeaveRequest\LeaveRequestUploadTask', get_class($task));
    }
}
