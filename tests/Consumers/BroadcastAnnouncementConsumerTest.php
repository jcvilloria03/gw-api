<?php

namespace Tests\Consumers;

use App\Consumers\BroadcastAnnouncementConsumer;
use App\Events\AnnouncementPublished;
use Illuminate\Support\Facades\Event;

class BroadcastAnnouncementConsumerTest extends \Tests\TestCase
{
    public function testCallback()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'user_id' => 1,
            'announcement' => ['message' => 'test', 'subject' => 'test'],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastAnnouncementConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertDispatched(AnnouncementPublished::class);
    }

    public function testCallbackEmptyUserId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'announcement' => ['message' => 'test', 'subject' => 'test'],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastAnnouncementConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertNotDispatched(AnnouncementPublished::class);
    }

    public function testCallbackEmptyNotification()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'user_id' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastAnnouncementConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertNotDispatched(AnnouncementPublished::class);
    }
}
