<?php

namespace Tests\Consumers;

use App\Consumers\EmployeesUpdateStatusConsumer;
use Bschmitt\Amqp\Facades\Amqp;
use App\Employee\EmployeeUpdateUploadTask;
use Mockery as m;

class EmployeesUpdateStatusConsumerTest extends \Tests\TestCase
{
    public function setUp()
    {
        parent::setUp();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->andReturn($mockS3Client);
        app()->instance('aws', $dependency);
    }

    public function testSendEmailVerification()
    {
        $this->markTestSkipped('Deprecated. #9240');
        $message = new \stdClass();
        $message->body = $this->buildMessage();

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock(EmployeeUpdateUploadTask::class);
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('activateEmployees')
            ->once()
            ->andReturn([
                "auth0|5e6ccdd4661a0d0d1ae55770",
                "auth0|5e72e9aee259ad0ce33c34fe",
                "auth0|5e72ea81bc0f8b0ce0f5674a",
                "auth0|5eba02a774e4bf0ccbf76011",
                "auth0|5eba18964aeb740c3f1b58b1",
                "auth0|5ec50461dcf77d0cd56adc35"
            ]);

        $this->app->bind(EmployeeUpdateUploadTask::class, function () use ($fakeUploadTask) {
            return $fakeUploadTask;
        });

        $consumer = $this->app->make(EmployeesUpdateStatusConsumer::class);

        Amqp::shouldReceive('publish')
            ->times(7);
        
        $consumer->callback($message, $fakeResolver);
    }

    private function buildMessage()
    {
        return base64_encode(json_encode([
            "id"=> "employee_update_upload=>1=>5ee046ae427d9",
            "company_id"=> 1,
            "account_id"=> 1,
            "task"=> "personal",
            "status"=> "processing_accounts_status",
            "next_status"=> "saved",
            "employee_ids"=> [
                17,
                15,
                13,
                16,
                18,
                19
            ],
            "account_processing"=> [
                "for_activation"=> [
                    [
                        "id"=> 17,
                        "user_id"=> 18,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "FP07-00004",
                        "email"=> "johndoe+FP07-00004@gmail.com",
                        "name"=> "Massimo Hahn",
                        "first_name"=> "Massimo",
                        "last_name"=> "Hahn",
                        "middle_name"=> ""
                    ],
                    [
                        "id"=> 15,
                        "user_id"=> 16,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "FP07-00002",
                        "email"=> "johndoe+FP07-00002@gmail.com",
                        "name"=> "Victor Benton",
                        "first_name"=> "Victor",
                        "last_name"=> "Benton",
                        "middle_name"=> ""
                    ],
                    [
                        "id"=> 13,
                        "user_id"=> 14,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "emp-014",
                        "email"=> "johndoe+emp-014-upload-employee@salarium.com",
                        "name"=> "Stop Email Verification",
                        "first_name"=> "Stop",
                        "last_name"=> "Email Verification",
                        "middle_name"=> ""
                    ],
                    [
                        "id"=> 16,
                        "user_id"=> 17,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "FP07-00003",
                        "email"=> "johndoe+FP07-00003@gmail.com",
                        "name"=> "Akshay Mcknight",
                        "first_name"=> "Akshay",
                        "last_name"=> "Mcknight",
                        "middle_name"=> ""
                    ],
                    [
                        "id"=> 18,
                        "user_id"=> 19,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "FP07-00005",
                        "email"=> "johndoe+FP07-00005@gmail.com",
                        "name"=> "Yvonne Leal",
                        "first_name"=> "Yvonne",
                        "last_name"=> "Leal",
                        "middle_name"=> ""
                    ],
                    [
                        "id"=> 19,
                        "user_id"=> 20,
                        "account_id"=> 1,
                        "company_id"=> 1,
                        "employee_id"=> "FP07-00006",
                        "email"=> "johndoe+FP07-00006@gmail.com",
                        "name"=> "Nicolas Hodge",
                        "first_name"=> "Nicolas",
                        "last_name"=> "Hodge",
                        "middle_name"=> ""
                    ]
                ],
                "for_deactivation"=> [],
                "for_semi_activation"=> []
            ],
            "s3_bucket"=> "salarium-v3-integration-development-uploads"

        ]));
    }
}
