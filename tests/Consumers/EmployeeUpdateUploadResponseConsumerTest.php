<?php

namespace Tests\Consumers;

use App\Consumers\EmployeeUpdateUploadResponseConsumer;
use App\Employee\EmployeeUpdateUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\App;
use Mockery as m;

class EmployeeUpdateUploadResponseConsumerTest extends \Tests\TestCase
{
    public function testCallbackValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUpdateUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUpdateUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService,
            ]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackSave()
    {
        $employeeIds = [1, 2, 3];

        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUpdateUploadTask::PROCESS_SAVE,
            'status' => 1,
            'employee_ids' => $employeeIds,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUpdateUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->andReturn(1)
            ->shouldReceive('updateSaveStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');

        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUpdateUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackTriggerWrite()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
            'status' => EmployeeUpdateUploadTask::STATUS_VALIDATED,
            'employee_ids' => [1, 2, 3],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Amqp::shouldReceive('publish')
            ->times(1);

        $fakeUploadTask = m::mock('App\Employee\EmployeeUpdateUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('getId')
            ->once()
            ->andReturn(1)
            ->shouldReceive('getProcess')
            ->once()
            ->andReturn(1)
            ->shouldReceive('updateValidationStatus')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once()
            ->shouldReceive('getS3Bucket')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');

        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUpdateUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService
            ]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackErrorFileS3Key()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUpdateUploadTask::PROCESS_SAVE,
            'status' => 1,
            'error_file_s3_key' => 'key',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUpdateUploadTask');
        $fakeUploadTask
            ->shouldReceive('updateErrorFileLocation')
            ->once()
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once()
            ->shouldReceive('getProcess')
            ->once()
            ->andReturn('save');

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->never();

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUpdateUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService
            ]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->never();

        $consumer = new EmployeeUpdateUploadResponseConsumer($auditService, $mockIndexQueueService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyCompanyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->never();

        $consumer = new EmployeeUpdateUploadResponseConsumer($auditService, $mockIndexQueueService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyTask()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->never();

        $consumer = new EmployeeUpdateUploadResponseConsumer($auditService, $mockIndexQueueService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->never();

        $consumer = new EmployeeUpdateUploadResponseConsumer($auditService, $mockIndexQueueService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateUploadTask()
    {

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency =  m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');

        $consumer = new EmployeeUpdateUploadResponseConsumer($auditService, $mockIndexQueueService);
        $task = $consumer->generateUploadTask();
        $this->assertEquals('App\Employee\EmployeeUpdateUploadTask', get_class($task));
    }
}
