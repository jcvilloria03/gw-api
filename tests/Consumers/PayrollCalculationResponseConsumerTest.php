<?php

namespace Tests\Consumers;

use App\Payroll\PayrollCalculationTask;
use App\Consumers\PayrollCalculationResponseConsumer;
use Mockery as m;
use Tests\TestCase;

class PayrollCalculationResponseConsumerTest extends TestCase
{
    public function testCallback()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeTask = m::mock(PayrollCalculationTask::class);
        $fakeTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateCalculationStatus')
            ->once();

        $mockConsumer = m::mock('App\Consumers\PayrollCalculationResponseConsumer[generateTask]');
        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'payroll_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollCalculationResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyPayrollId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollCalculationResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'payroll_id' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayrollCalculationResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateTask()
    {
        $consumer = new PayrollCalculationResponseConsumer();
        $task = $consumer->generateTask();
        $this->assertEquals(PayrollCalculationTask::class, get_class($task));
    }
}
