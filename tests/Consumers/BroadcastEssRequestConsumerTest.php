<?php

namespace Tests\Consumers;

use App\Consumers\BroadcastEssRequestConsumer;
use App\Events\EssRequestUpdated;
use Illuminate\Support\Facades\Event;

class BroadcastEssRequestConsumerTest extends \Tests\TestCase
{
    public function testCallback()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'request' => ['body' => 'test'],
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastEssRequestConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertDispatched(EssRequestUpdated::class);
    }

    public function testCallbackEmptyRequest()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        Event::fake();

        $consumer = new BroadcastEssRequestConsumer();
        $consumer->callback($message, $fakeResolver);

        Event::assertNotDispatched(EssRequestUpdated::class);
    }
}
