<?php

namespace Tests\Consumers;

use App\Payroll\PayslipGenerationTask;
use App\Consumers\PayslipGenerationResponseConsumer;
use Mockery as m;
use Tests\TestCase;

class PayslipGenerationResponseConsumerTest extends TestCase
{
    public function testCallback()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'payroll_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeTask = m::mock(PayslipGenerationTask::class);
        $fakeTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateGenerationStatus')
            ->once();

        $mockConsumer = m::mock('App\Consumers\PayslipGenerationResponseConsumer[generateTask]');
        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyPayrollId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayslipGenerationResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'payroll_id' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };
        $consumer = new PayslipGenerationResponseConsumer();
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateTask()
    {
        $consumer = new PayslipGenerationResponseConsumer();
        $task = $consumer->generateTask();
        $this->assertEquals(PayslipGenerationTask::class, get_class($task));
    }
}
