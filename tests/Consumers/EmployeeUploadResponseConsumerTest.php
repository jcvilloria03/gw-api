<?php

namespace Tests\Consumers;

use Mockery as m;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeUploadTask;
use App\Consumers\EmployeeUploadResponseConsumer;

class EmployeeUploadResponseConsumerTest extends \Tests\TestCase
{
    public function testCallbackValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService,
                $requestService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackSave()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_SAVE,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService,
                $requestService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackErrorFileS3Key()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_SAVE,
            'status' => 1,
            'error_file_s3_key' => 'key',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once()
            ->shouldReceive('updateErrorFileLocation')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->never();
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService,
                $requestService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->never();
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $consumer = new EmployeeUploadResponseConsumer($auditService, $mockIndexQueueService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyCompanyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->never();
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $consumer = new EmployeeUploadResponseConsumer($auditService, $mockIndexQueueService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyTask()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->never();
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $consumer = new EmployeeUploadResponseConsumer($auditService, $mockIndexQueueService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->never();
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $consumer = new EmployeeUploadResponseConsumer($auditService, $mockIndexQueueService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateUploadTask()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue');
        $requestService = m::mock('App\Employee\EmployeeRequestService');

        $consumer = new EmployeeUploadResponseConsumer($auditService, $mockIndexQueueService, $requestService);
        $task = $consumer->generateUploadTask();
        $this->assertEquals('App\Employee\EmployeeUploadTask', get_class($task));
    }

    public function testCallbackCreatedEmployees()
    {
        $employees = [
            [
                'id'         => 1,
                'user_id'    => 1,
                'account_id' => 1,
                'company_id' => 1
            ],
            [
                'id'         => 2,
                'user_id'    => 2,
                'account_id' => 1,
                'company_id' => 1
            ]
        ];

        $employeeIds = Arr::pluck($employees, 'id');

        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_SAVE,
            'status' => 1,
            'employees' => $employees,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Employee\EmployeeUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once()
            ->shouldReceive('getUserId')
            ->andReturn(1)
            ->shouldReceive('createUserRecords')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $auditService
            ->shouldReceive('logCreatedEmployees')
            ->once();

        $mockIndexQueueService = m::mock('App\Employee\EmployeeESIndexQueueService');
        $mockIndexQueueService
            ->shouldReceive('queue')
            ->with($employeeIds)
            ->once();

        $mockConsumer = m::mock(
            'App\Consumers\EmployeeUploadResponseConsumer[generateUploadTask]',
            [
                $auditService,
                $mockIndexQueueService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }
}
