<?php

namespace Tests\Consumers;

use Mockery as m;
use Illuminate\Support\Arr;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeUploadTask;
use App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer;

class EmployeeTimeAttendanceUploadResponseConsumerTest extends \Tests\TestCase
{
    public function testCallbackValidation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayslipGenerationTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $mockConsumer = m::mock(
            'App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer[generateUploadTask]',
            [$auditService, $requestService]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackSave()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_TA_SAVE,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayslipGenerationTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateTaSaveStatus')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $mockConsumer = m::mock(
            'App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer[generateUploadTask]',
            [$auditService, $requestService]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackErrorFileS3Key()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_TA_SAVE,
            'status' => 1,
            'error_file_s3_key' => 'key',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayslipGenerationTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateTaSaveStatus')
            ->once()
            ->shouldReceive('updateErrorFileLocation')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $mockConsumer = m::mock(
            'App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer[generateUploadTask]',
            [$auditService, $requestService]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'company_id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $consumer = new EmployeeTimeAttendanceUploadResponseConsumer($auditService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyCompanyId()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $consumer = new EmployeeTimeAttendanceUploadResponseConsumer($auditService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyTask()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'status' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $consumer = new EmployeeTimeAttendanceUploadResponseConsumer($auditService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testCallbackEmptyStatus()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => 1,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $consumer = new EmployeeTimeAttendanceUploadResponseConsumer($auditService, $requestService);
        $consumer->callback($message, $fakeResolver);
    }

    public function testGenerateUploadTask()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->once()->andReturn($mockS3Client);
        App::instance('aws', $dependency);

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $consumer = new EmployeeTimeAttendanceUploadResponseConsumer($auditService, $requestService);
        $task = $consumer->generateUploadTask();
        $this->assertEquals('App\Employee\EmployeeUploadTask', get_class($task));
    }

    public function testCallbackCreatedEmployees()
    {
        $employees = [
            [
                'id'         => 1,
                'user_id'    => 1,
                'account_id' => 1,
                'company_id' => 1
            ],
            [
                'id'         => 2,
                'user_id'    => 2,
                'account_id' => 1,
                'company_id' => 1
            ]
        ];

        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'company_id' => 1,
            'task' => EmployeeUploadTask::PROCESS_TA_SAVE,
            'status' => 1,
            'employees' => $employees,
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Payroll\PayslipGenerationTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateTaSaveStatus')
            ->once()
            ->shouldReceive('getUserId')
            ->times(1)
            ->andReturn(1)
            ->shouldReceive('createUserRecords')
            ->once();

        $auditService = m::mock('App\Employee\EmployeeAuditService');
        $auditService
            ->shouldReceive('logCreatedTaEmployees')
            ->once();
        $mockResponse = new JsonResponse('{"data": []}', JsonResponse::HTTP_OK);
        $requestService = m::mock('App\Employee\EmployeeRequestService');
        $requestService
            ->shouldReceive('getCompanyTaEmployeesByAttribute')
            ->once()
            ->andReturn($mockResponse);
        $mockConsumer = m::mock(
            'App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer[generateUploadTask]',
            [$auditService, $requestService]
        );
        $mockConsumer
            ->shouldReceive('generateUploadTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }
}
