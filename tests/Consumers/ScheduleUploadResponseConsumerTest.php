<?php

namespace Tests\Consumers;

use Illuminate\Support\Facades\App;
use App\Consumers\ScheduleUploadResponseConsumer;
use \Mockery as m;

class ScheduleUploadResponseConsumerTest extends \Tests\TestCase
{
    public function setUp()
    {
        parent::setUp();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $dependency = m::mock();
        $dependency->shouldReceive('createClient')->andReturn($mockS3Client);
        app()->instance('aws', $dependency);
    }

    public function testCallbackOnValidValidationWithErrorFileLocation()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 'validation',
            'status' => 'validated',
            'company_id' => 1,
            'error_file_s3_key' => 's3_key'
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Schedule\ScheduleUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateValidationStatus')
            ->once()
            ->shouldReceive('updateErrorFileLocation')
            ->once();

        $auditService = m::mock('App\Schedule\ScheduleAuditService');

        $mockConsumer = m::mock(
            'App\Consumers\ScheduleUploadResponseConsumer[generateTask]',
            [
                $auditService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackOnValidSave()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'id' => 1,
            'task' => 'save',
            'status' => 'saved',
            'company_id' => 1
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $fakeUploadTask = m::mock('App\Schedule\ScheduleUploadTask');
        $fakeUploadTask
            ->shouldReceive('create')
            ->once()
            ->shouldReceive('updateSaveStatus')
            ->once();

        $auditService = m::mock('App\Schedule\ScheduleAuditService');

        $mockConsumer = m::mock(
            'App\Consumers\ScheduleUploadResponseConsumer[generateTask]',
            [
                $auditService
            ]
        );

        $mockConsumer
            ->shouldReceive('generateTask')
            ->once()
            ->andReturn($fakeUploadTask);

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testCallbackOnIncompletedMessage()
    {
        $message = new \stdClass();
        $message->body = base64_encode(json_encode([
            'task' => 'save',
            'status' => 'saved',
        ]));

        $fakeResolver = new class() {
            public function acknowledge($a)
            {
                return $a;
            }
        };

        $auditService = m::mock('App\Schedule\ScheduleAuditService');
        $mockConsumer = m::mock(
            'App\Consumers\ScheduleUploadResponseConsumer[generateTask]',
            [
                $auditService
            ]
        );

        $mockConsumer->callback($message, $fakeResolver);
    }

    public function testGenerateTask()
    {
        $auditService = m::mock('App\Schedule\ScheduleAuditService');
        $consumer = new ScheduleUploadResponseConsumer($auditService);

        $task = $consumer->generateTask();
        $this->assertEquals('App\Schedule\ScheduleUploadTask', get_class($task));
    }
}
