<?php

namespace Tests\Bank;

use App\Audit\AuditService;
use App\Bank\CompanyBankAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CompanyBankAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'bank_branch' => 'JP Rizal Makati',
            'account_number' => '1800293884',
            'account_type' => 'SAVINGS',
            'company_code' => 'TEST',
            'bank' => [
                'id' => 20,
                'name' => 'Banco De Oro',
            ]
        ]);
        $item = [
            'action' => CompanyBankAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $bankAuditService = new CompanyBankAuditService($mockAuditService);
        $bankAuditService->log($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => CompanyBankAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $companyBankAuditService = new CompanyBankAuditService($mockAuditService);
        $companyBankAuditService->log($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'account_number' => '1809938875',
        ]);
        $item = [
            'action' => CompanyBankAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $companyBankAuditService = new CompanyBankAuditService($mockAuditService);
        $companyBankAuditService->log($item);
    }
}
