<?php

namespace Tests\Bank;

use App\Bank\BankRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BankRequestServiceTest extends TestCase
{
    public function testIndex()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->index();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->index();

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->index();

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->index();
    }

    public function testGetEmployeePaymentMethods()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getEmployeePaymentMethods(12);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getEmployeePaymentMethods(12);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getEmployeePaymentMethods(12);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getEmployeePaymentMethods(12);
    }

    public function testGetCompanyBankAccounts()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyBankAccounts(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getCompanyBankAccounts(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyBankAccounts(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyBankAccounts(1);
    }

    public function testAddCompanyBankAccount()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        $attributes = [
            'data' => [
                'type' => 'company-bank-account',
                'attributes' => [
                    'bank_id' => 11,
                    'bank_branch' => '1650 Penafrancia, Makati, 1208 Metro Manila',
                    'account_number' => '12121',
                    'account_type' => 'SAVINGS',
                    'company_code' => '1212',
                ],
            ],
        ];

        // test Response::HTTP_OK
        $response = $requestService->addCompanyBankAccount(1, $attributes);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->addCompanyBankAccount(1, $attributes);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->addCompanyBankAccount(1, $attributes);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->addCompanyBankAccount(1, $attributes);
    }

    public function testAddEmployeeBankAccount()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        $attributes = [
            'data' => [
                'type' => 'employee-payment-method',
                'attributes' => [
                    'bank_id' => 2,
                    'account_number' => '12121',
                    'account_type' => 'SAVINGS',
                ],
            ],
        ];

        // test Response::HTTP_OK
        $response = $requestService->addEmployeeBankAccount(1, $attributes);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->addEmployeeBankAccount(1, $attributes);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->addEmployeeBankAccount(1, $attributes);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->addEmployeeBankAccount(1, $attributes);
    }

    public function testDeleteMultipleEmployeePaymentMethod()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new BankRequestService($client);

        $requestPayload = collect([
            "data" => [
                "type" => 'employee-payment-method',
                "id" => [
                    5,6
                ]
            ]
        ]);

        // test Response::HTTP_OK
        $response = $requestService->deleteEmployeePaymentMethods($requestPayload->toJson(), 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->deleteEmployeePaymentMethods($requestPayload->toJson(), 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->deleteEmployeePaymentMethods($requestPayload->toJson(), 1);
    }
}
