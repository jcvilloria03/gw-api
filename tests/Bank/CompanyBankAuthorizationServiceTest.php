<?php

namespace Tests\Bank;

use Tests\TestCase;
use App\Bank\CompanyBankAuthorizationService;
use Illuminate\Http\JsonResponse;
use Dingo\Api\Http\Response;

class CompanyBankAuthorizationServiceTest extends TestCase
{
    public function testGenerateCompanyModel()
    {
        $mockCompany = [
            "id" => 822,
            "name" => "AB Corporation Inc.",
            "account_id" => 145,
        ];

        $response = new JsonResponse(collect($mockCompany)->toJson(), Response::HTTP_OK);
    
        $json = CompanyBankAuthorizationService::generateCompanyModel($response);

        $this->assertEquals($json->company_id, $mockCompany['id']);
    }

    public function testGenerateCompanyModelInvalidPayloadNoId()
    {
        $mockCompany = [
            "name" => "AB Corporation Inc.",
            "account_id" => 145,
        ];

        $expected = $mockCompany + ['company_id' => 0];

        $response = new JsonResponse(collect($mockCompany)->toJson(), Response::HTTP_OK);
    
        $json = CompanyBankAuthorizationService::generateCompanyModel($response);

        $this->assertEquals(collect($expected)->toJson(), json_encode($json));
    }

    public function testGenerateCompanyModelInvalidPayloadNoAccountID()
    {
        $mockCompany = [
            "id" =>  1,
            "name" => "AB Corporation Inc.",
        ];

        $expected = $mockCompany + ['company_id' => $mockCompany['id'], 'account_id' => 0];

        $response = new JsonResponse(collect($mockCompany)->toJson(), Response::HTTP_OK);
    
        $json = CompanyBankAuthorizationService::generateCompanyModel($response);

        $this->assertEquals(collect($expected)->toJson(), json_encode($json));
    }

    public function testGenerateCompanyModelInvalidPayloadFailsafe()
    {
        $mockCompany = [
            "name" => "AB Corporation Inc.",
        ];

        $expected = $mockCompany + ['company_id' => 0, 'account_id' => 0];

        $response = new JsonResponse(collect($mockCompany)->toJson(), Response::HTTP_OK);
    
        $json = CompanyBankAuthorizationService::generateCompanyModel($response);

        $this->assertEquals(collect($expected)->toJson(), json_encode($json));
    }
}
