<?php

namespace Tests\PayrollLoanType;

use App\Audit\AuditService;
use App\PayrollLoanType\PayrollLoanTypeAuditService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollLoanTypeAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => PayrollLoanTypeAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockRequestService = m::mock(PayrollLoanTypeRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanTypeAuditService = new PayrollLoanTypeAuditService($mockRequestService, $mockAuditService);
        $payrollLoanTypeAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => PayrollLoanTypeAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockRequestService = m::mock(PayrollLoanTypeRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanTypeAuditService = new PayrollLoanTypeAuditService($mockRequestService, $mockAuditService);
        $payrollLoanTypeAuditService->logUpdate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => PayrollLoanTypeAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock(PayrollLoanTypeRequestService::class);
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $payrollLoanTypeAuditService = new PayrollLoanTypeAuditService($mockRequestService, $mockAuditService);
        $payrollLoanTypeAuditService->logDelete($item);
    }
}
