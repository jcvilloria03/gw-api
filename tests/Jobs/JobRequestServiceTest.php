<?php

namespace Tests\Jobs;

use App\Jobs\JobsRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JobsRequestServiceTest extends TestCase
{
    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new JobsRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getJobStatus('test', 'results,errors');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getJobStatus('test', 'results,errors');

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getJobStatus('test', 'results,errors');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getJobStatus('test', 'results,errors');
    }

    public function testCreateJob()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new JobsRequestService($client);

        $jobPayload = [
            'data' => [
                'type' => 'jobs',
                'attributes' => [
                    'payload' => [
                        'name' => 'test-validation-job',
                        'fileBucket' => 'v3-uploads-local-test',
                        'fileKey' => 'test-s3-key.csv',
                        'startDate' => '2019-01-01',
                        'endDate' => '2019-01-31',
                        'companyId' => 132,
                        'payrollGroupId' => 987
                    ]
                ]
            ]
        ];

        // test Response::HTTP_OK
        $response = $requestService->createJob($jobPayload);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->createJob($jobPayload);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->createJob($jobPayload);
    }
}
