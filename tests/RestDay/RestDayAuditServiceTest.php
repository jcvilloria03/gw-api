<?php

namespace Tests\RestDay;

use App\Audit\AuditService;
use App\RestDay\RestDayAuditService;
use App\RestDay\RestDayRequestService;
use App\User\UserRequestService;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class RestDayAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'start_date' => '2017-12-10',
            'end_date' => '2017-12-16'
        ]);
        $item = [
            'action' => RestDayAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockRequestService = m::mock(RestDayRequestService::class);
        $mockUserRequestService = $this->getMockUserRequestService();

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new RestDayAuditService(
            $mockRequestService,
            $mockUserRequestService,
            $mockAuditService
        );
        $scheduleAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'start_date' => '2017-12-10',
            'end_date' => '2017-12-16'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name',
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'start_date' => '2017-12-10',
            'end_date' => '2017-12-16'
        ]);
        $item = [
            'action' => RestDayAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockRequestService = m::mock(RestDayRequestService::class);
        $mockUserRequestService = $this->getMockUserRequestService();
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new RestDayAuditService(
            $mockRequestService,
            $mockUserRequestService,
            $mockAuditService
        );
        $scheduleAuditService->logUpdate($item);
    }

    public function testLogUnassign()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name',
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'start_date' => '2017-12-10',
            'end_date' => '2017-12-16'
        ]);
        $item = [
            'action' => RestDayAuditService::ACTION_UNASSIGN,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock(RestDayRequestService::class);
        $mockUserRequestService = $this->getMockUserRequestService();
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $scheduleAuditService = new RestDayAuditService(
            $mockRequestService,
            $mockUserRequestService,
            $mockAuditService
        );
        $scheduleAuditService->logUnassign($item);
    }

    private function getMockUserRequestService()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $response = new JsonResponse();
        $response->setdata(json_encode([
            'id' => 1,
            'name' => 'userName',
            'account_id' => 2
        ]));
        $mockUserRequestService->shouldReceive('get')
            ->andReturn($response);

        return $mockUserRequestService;
    }
}
