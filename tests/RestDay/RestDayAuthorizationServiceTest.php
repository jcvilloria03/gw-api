<?php

namespace Tests\RestDay;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\RestDay\RestDayAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class RestDayAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRestDayDetails, $user));
    }

    public function testGetCompanyRestDaysPassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testGetCompanyRestDaysInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyRestDays($targetRestDayDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRestDayDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRestDayDetails, $user));
    }

    public function testUnassignPassAccountLevel()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRestDayDetails, $user));
    }

    public function testUnassignPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }

    public function testUnassignInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RestDayAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetRestDayDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RestDayAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUnassign($targetRestDayDetails, $user));
    }
}
