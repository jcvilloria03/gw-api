<?php

namespace Tests\Request;

use App\Request\DownloadRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DownloadRequestServiceTest extends TestCase
{
    public function testDownload()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(
                Response::HTTP_NOT_ACCEPTABLE,
                [],
                json_encode([
                    '1' => '1',
                    '2' => 'this is a message'
                ])
            ),
            new GuzzleResponse(
                Response::HTTP_NOT_FOUND,
                [],
                json_encode([
                    'status_code' => Response::HTTP_NOT_FOUND,
                    'message' => 'this is a message'
                ])
            ),
        ]);
        $request = new Request(
            'GET',
            '/test'
        );

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $class = new ReflectionClass('App\Request\DownloadRequestService');
        $sendMethod = $class->getMethod('download');
        $sendMethod->setAccessible(true);

        $requestService = new DownloadRequestService($client);

        // test Response::HTTP_OK
        $response = $sendMethod->invokeArgs($requestService, [$request]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT ACCEPTABLE with contents
        $this->expectException(HttpException::class);
        $response = $sendMethod->invokeArgs($requestService, [$request]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $sendMethod->invokeArgs($requestService, [$request]);
    }
}
