<?php

namespace Tests\Rank;

use App\Rank\RankEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class RankESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $rankIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')->once();

        $rankESIndexQueueService = new RankEsIndexQueueService($mockIndexQueueService);
        $rankESIndexQueueService->queue($rankIds);
    }
}
