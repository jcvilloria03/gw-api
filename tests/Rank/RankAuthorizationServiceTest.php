<?php

namespace Tests\Rank;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Rank\RankAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class RankAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetRankDetails, $user));
    }

    public function testGetCompanyRanksPassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testGetCompanyRanksInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyRanks($targetRankDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetRankDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailableNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetRankDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetRankDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRankDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetRankDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            RankAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetRankDetails, $user));
    }
}
