<?php

namespace Tests\Permission;

use App\Model\Role;
use App\Model\Task;
use App\Permission\PermissionService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PermissionServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testCreatePermission()
    {
        $service = new PermissionService();
        $role = factory(Role::class)->create();
        $task = Task::first();
        $scope = [
            'Account'=> [
                $role->account_id
            ]
        ];
        $permission = $service->create([
            'task_id' => $task->id,
            'role_id' => $role->id,
            'scope' => $scope,
        ]);
        $this->assertNotEmpty($permission->id);
        $this->assertEquals($task->id, $permission->task_id);
        $this->assertEquals($role->id, $permission->role_id);
        $this->assertEquals($role->id, $permission->role->id);
        $this->assertEquals($role->name, $permission->role->name);
        $this->assertEquals($scope, $permission->scope);
    }

    public function testGetRolePermissions()
    {
        $service = new PermissionService();
        $role = factory(Role::class)->create();
        $tasks = Task::take(5)->get();
        $scope = [
            'Account'=> [
                $role->account_id
            ]
        ];
        $permissions = [];
        foreach ($tasks as $task) {
            $permissions[] = $service->create([
                'task_id' => $task->id,
                'role_id' => $role->id,
                'scope' => $scope,
            ]);
        }

        $permissions = $service->getRolePermissions($role->id);
        $this->assertNotEmpty($permissions);
    }
}
