<?php

namespace Tests\Permission;

use App\Permission\AuthorizationPermissions;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class AuthorizationPermissionsTest extends TestCase
{

    public function testAddTaskScopeFirstOfType()
    {
        $permissions = new AuthorizationPermissions();

        $task1 = 'task1';
        $taskScope1 = new TaskScopes($task1);
        $permissions->addTaskScope($taskScope1);
        $taskScope = $permissions->getTaskScopesForTask($task1);
        $this->assertEquals($task1, $taskScope->getTaskName());

        $task2 = 'task2';
        $taskScope2 = new TaskScopes($task2);
        $permissions->addTaskScope($taskScope2);
        $taskScope = $permissions->getTaskScopesForTask($task2);
        $this->assertEquals($task2, $taskScope->getTaskName());
    }

    public function testGetTaskScopesForTask()
    {
        $permissions = new AuthorizationPermissions();

        $task1 = 'task1';
        $taskScope1 = new TaskScopes($task1);
        $permissions->addTaskScope($taskScope1);
        $taskScope = $permissions->getTaskScopesForTask($task1);
        $task2 = 'task2';
        $taskScope = $permissions->getTaskScopesForTask($task2);
        $this->assertNull($taskScope);
    }

    public function testAddTaskScopeSecondOfTask()
    {
        $permissions = new AuthorizationPermissions();

        $task1 = 'task1';
        $taskScope1 = new TaskScopes($task1);
        $permissions->addTaskScope($taskScope1);
        $taskScope = $permissions->getTaskScopesForTask($task1);

        $taskScope2 = new TaskScopes($task1);
        $permissions->addTaskScope($taskScope2);
        $taskScope = $permissions->getTaskScopesForTask($task1);
        $this->assertEquals($task1, $taskScope->getTaskName());
    }
}
