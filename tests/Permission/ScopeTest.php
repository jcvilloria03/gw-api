<?php

namespace Tests\Permission;

use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class ScopeTest extends TestCase
{
    public function testConstructor()
    {
        // valid
        $type = 'type';
        $target = Scope::TARGET_ALL;
        $scope = new Scope($type, $target);
        $this->assertInstanceOf(Scope::class, $scope);

        // valid
        $type = 'type';
        $target = [1, 2, 3];
        $scope = new Scope($type, $target);
        $this->assertInstanceOf(Scope::class, $scope);

        // invalid
        $type = 'type';
        $target = 'invalid';
        $this->expectException(\InvalidArgumentException::class);
        $scope = new Scope($type, $target);
    }

    public function testInScope()
    {
        // TARGET_ALL should always return true
        $type = 'type';
        $target = Scope::TARGET_ALL;
        $scope = new Scope($type, $target);
        $this->assertTrue($scope->inScope(1));

        // if not TARGET_ALL, passed $id should be in target array
        $type = 'type';
        $target = [1, 2, 3];
        $scope = new Scope($type, $target);
        $this->assertTrue($scope->inScope(1));

        $type = 'type';
        $target = [1, 2, 3];
        $scope = new Scope($type, $target);
        $this->assertFalse($scope->inScope(4));
    }

    public function testGetType()
    {
        // TARGET_ALL should always return true
        $type = 'type';
        $target = Scope::TARGET_ALL;
        $scope = new Scope($type, $target);
        $this->assertEquals($type, $scope->getType());
    }
}
