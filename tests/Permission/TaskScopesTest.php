<?php

namespace Tests\Permission;

use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

class TaskScopesTest extends TestCase
{
    public function testConstructor()
    {
        $task = 'task';
        $taskScope = new TaskScopes($task);
        $this->assertInstanceOf(TaskScopes::class, $taskScope);
        $this->assertEquals($task, $taskScope->getTaskName());
        $this->assertEmpty($taskScope->getScopes());
    }

    public function testAddScopeFirstOfType()
    {
        $task = 'task';
        $taskScope = new TaskScopes($task);
        $scope1 = new Scope('company', Scope::TARGET_ALL);
        $taskScope->addScope($scope1);
        $scopes = $taskScope->getScopes();
        $this->assertEquals(1, count($scopes));
        $this->assertEquals($scope1, $scopes['company']);

        $scope2 = new Scope('account', [1,2,3]);
        $taskScope->addScope($scope2);
        $scopes = $taskScope->getScopes();
        $this->assertEquals(2, count($scopes));
        $this->assertEquals($scope2, $scopes['account']);
    }

    public function testGetScopeBasedOnType()
    {
        $task = 'task';
        $taskScope = new TaskScopes($task);
        $scope1 = new Scope('company', Scope::TARGET_ALL);
        $taskScope->addScope($scope1);
        $scope = $taskScope->getScopeBasedOnType('company');
        $this->assertEquals($scope1, $scope);

        $scope2 = new Scope('account', [1,2,3]);
        $taskScope->addScope($scope2);
        $scope = $taskScope->getScopeBasedOnType('account');
        $this->assertEquals($scope2, $scope);

        $scope = $taskScope->getScopeBasedOnType('nonexistent');
        $this->assertNull($scope);
    }

    public function testAddScopeSecondOfType()
    {
        $task = 'task';

        $taskScope = new TaskScopes($task);
        $scope1 = new Scope('company', Scope::TARGET_ALL);
        $scope2 = new Scope('company', [1, 2, 3]);
        $taskScope->addScope($scope1);
        $taskScope->addScope($scope2);
        $scopes = $taskScope->getScopes();
        $this->assertEquals(1, count($scopes));
        $this->assertEquals(Scope::TARGET_ALL, $scopes['company']->getTarget());

        $taskScope = new TaskScopes($task);
        $scope1 = new Scope('company', [1, 2, 3]);
        $scope2 = new Scope('company', Scope::TARGET_ALL);
        $taskScope->addScope($scope1);
        $taskScope->addScope($scope2);
        $scopes = $taskScope->getScopes();
        $this->assertEquals(1, count($scopes));
        $this->assertEquals(Scope::TARGET_ALL, $scopes['company']->getTarget());

        $taskScope = new TaskScopes($task);
        $scope1 = new Scope('company', [1, 2, 3]);
        $scope2 = new Scope('company', [2, 3, 4]);
        $taskScope->addScope($scope1);
        $taskScope->addScope($scope2);
        $scopes = $taskScope->getScopes();
        $this->assertEquals(1, count($scopes));
        $this->assertEquals([1, 2, 3, 4], $scopes['company']->getTarget());
    }
}
