<?php

namespace Tests\CommissionType;

use Tests\Common\CommonAuthorizationService;

class CommissionTypeAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\CommissionType\CommissionTypeAuthorizationService::class;
}
