<?php

namespace Tests\LeaveType;

use App\LeaveEntitlement\LeaveEntitlementRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LeaveEntitlementRequestServiceTest extends TestCase
{
    public function testBulkDelete()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->bulkDelete([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->bulkDelete([]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->bulkDelete([]);
    }

    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->get(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->get(1);
    }

    public function testCreate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->create([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->create([]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->create([]);
    }

    public function testGetCompanyLeaveEntitlements()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyLeaveEntitlements(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getCompanyLeaveEntitlements(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyLeaveEntitlements(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyLeaveEntitlements(1);
    }

    public function testUpdate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->update([], 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->update([], 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->update([], 1);
    }

    public function testIsNameAvailable()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        $data = [
            'name' => 'testName'
        ];

        // test Response::HTTP_OK
        $response = $requestService->isNameAvailable(1, $data);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, $data);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, $data);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, $data);
    }

    public function testCheckInUse()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->checkInUse([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->checkInUse([]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->checkInUse([]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->checkInUse([]);
    }

    public function testGetForEmployeeAndLeaveType()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new LeaveEntitlementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getForEmployeeAndLeaveType(1, '');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getForEmployeeAndLeaveType(1, '');

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getForEmployeeAndLeaveType(1, '');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getForEmployeeAndLeaveType(1, '');
    }
}
