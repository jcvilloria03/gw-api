<?php

namespace Tests\User;

use App\Audit\AuditService;
use App\User\UserAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\User\UserRequestService;

class UserAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'email' => 'email@email.com',
        ]);
        $item = [
            'action' => UserAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockUserService = m::mock(UserRequestService::class);

        $mockAuditService->shouldReceive('log')
            ->once();

        $rankAuditService = new UserAuditService($mockAuditService, $mockUserService);
        $rankAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => UserAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockUserService = m::mock(UserRequestService::class);

        $mockAuditService->shouldReceive('log')
            ->once();

        $rankAuditService = new UserAuditService($mockAuditService, $mockUserService);
        $rankAuditService->logDelete($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1
        ]);
        $item = [
            'action' => UserAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockUserService = m::mock(UserRequestService::class);

        $mockAuditService->shouldReceive('log')
            ->once();

        $rankAuditService = new UserAuditService($mockAuditService, $mockUserService);
        $rankAuditService->logUpdate($item);
    }
}
