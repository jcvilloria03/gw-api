<?php

namespace Tests\User;

use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use App\User\UserAuthorizationService;
use App\Model\Role;
use App\Model\UserRole;
use Tests\TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;
    use DatabaseTransactions;

    public function testGetPass()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::VIEW_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => $targetAccountId
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertTrue($authorizationService->authorizeGet($authData, $userData));
    }

    public function testGetNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            null
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeGet($authData, $userData));
    }

    public function testGetInvalidScope()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeGet($authData, $userData));
    }

    public function testAuthorizeGetAccountUsersPass()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::VIEW_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertTrue($authorizationService->authorizeGet($authData, $userData));
    }

    public function testAuthorizeGetAccountUsersNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            null
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeGet($authData, $userData));
    }

    public function testAuthorizeGetAccountUsersInvalidScope()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::VIEW_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];
        $this->assertFalse($authorizationService->authorizeGet($authData, $userData));
    }

    public function testAuthorizeCreatePass()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::CREATE_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertTrue($authorizationService->authorizeCreate($authData, $userData));
    }

    public function testAuthorizeCreateNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            null
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeCreate($authData, $userData));
    }

    public function testAuthorizeCreateInvalidScope()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $authData = (object) [
            'company_id' => 1,
            'account_id' => 1
        ];

        $userData = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeCreate($authData, $userData));
    }

    public function testAuthorizeDeletePass()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::CREATE_TASK);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $user = (object) [
            'id' => 1,
            'account_id' => $targetAccountId
        ];

        $deletedBy = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertTrue($authorizationService->authorizeDelete($user, $deletedBy));
    }

    public function testAuthorizeDeleteNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            null
        );

        $user = (object) [
            'id' => 1,
            'account_id' => 1
        ];

        $deletedBy = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeDelete($user, $deletedBy));
    }

    public function testAuthorizeDeleteInvalidScope()
    {
        $taskScope = new TaskScopes(UserAuthorizationService::CREATE_TASK);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\User\UserAuthorizationService',
            $taskScope
        );

        $user = (object) [
            'id' => 1,
            'account_id' => 1
        ];

        $deletedBy = [
            'user_id' => 1,
            'user_type' => 'owner'
        ];

        $this->assertFalse($authorizationService->authorizeDelete($user, $deletedBy));
    }

    public function testAuthorizeUserTypeSuperAdmin()
    {
        $role = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Admin',
            'type' => Role::ADMIN,
            'custom_role' => false,
        ]);

        UserRole::create([
            'role_id' => $role->id,
            'user_id' => 1
        ]);

        $authorizationService = app('App\User\UserAuthorizationService');

        $user = (object) [
            'id' => 1,
            'user_type' => 'super admin'
        ];

        $result = $authorizationService->authorizeUserType($user, 1);
        $this->assertTrue($result);
    }

    public function testAuthorizeUserTypeAdmin()
    {
        $role = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Admin',
            'type' => Role::ADMIN,
            'custom_role' => false,
        ]);

        UserRole::create([
            'role_id' => $role->id,
            'user_id' => 1
        ]);

        $authorizationService = app('App\User\UserAuthorizationService');

        $user = (object) [
            'id' => 1,
            'user_type' => 'admin'
        ];

        $result = $authorizationService->authorizeUserType($user, 1);
        $this->assertTrue($result);
    }

    public function testAuthorizeUserTypeEmployee()
    {
        $role = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Employee',
            'type' => Role::EMPLOYEE,
            'custom_role' => false,
        ]);

        UserRole::create([
            'role_id' => $role->id,
            'user_id' => 1
        ]);

        $authorizationService = app('App\User\UserAuthorizationService');

        $user = (object) [
            'id' => 1,
            'user_type' => 'employee'
        ];

        $result = $authorizationService->authorizeUserType($user, 1);
        $this->assertFalse($result);
    }

    public function testAuthorizeUserTypeOwner()
    {
        $role = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Admin',
            'type' => Role::ADMIN,
            'custom_role' => false,
        ]);

        UserRole::create([
            'role_id' => $role->id,
            'user_id' => 1
        ]);

        $authorizationService = app('App\User\UserAuthorizationService');

        $user = (object) [
            'id' => 1,
            'user_type' => 'owner'
        ];

        $result = $authorizationService->authorizeUserType($user, 1);
        $this->assertTrue($result);
    }
}
