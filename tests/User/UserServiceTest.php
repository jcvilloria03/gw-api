<?php

namespace Tests\User;

use App\User\UserService;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\User\UserRequestService;
use Salarium\Cache\FragmentedRedisCache;
use App\Model\Auth0User;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserServiceTest extends TestCase
{
    public function testGetStatusActiveFromRedis()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(true)
        ->shouldReceive('get')
        ->once()
        ->andReturn([
            "status" => Auth0User::STATUS_ACTIVE
        ])
        ->getMock();

        $mockUserRequestService = m::mock(UserRequestService::class);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_ACTIVE, $status);
    }

    public function testGetStatusInActiveFromRedis()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(true)
        ->shouldReceive('get')
        ->once()
        ->andReturn([
            "status" => Auth0User::STATUS_INACTIVE
        ])
        ->getMock();

        $mockUserRequestService = m::mock(UserRequestService::class);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_INACTIVE, $status);
    }

    public function testGetStatusSemiActiveFromRedis()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(true)
        ->shouldReceive('get')
        ->once()
        ->andReturn([
            "status" => Auth0User::STATUS_SEMI_ACTIVE
        ])
        ->getMock();

        $mockUserRequestService = m::mock(UserRequestService::class);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_SEMI_ACTIVE, $status);
    }

    public function testGetStatusActiveFromDataStore()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(false)
        ->getMock();

        $mockResponse = new JsonResponse(json_encode([
            'status' => Auth0User::STATUS_ACTIVE
        ]));

        $mockUserRequestService = m::mock(UserRequestService::class);

        $mockUserRequestService->shouldReceive('getUserStatus')
                               ->once()
                               ->andReturn($mockResponse);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_ACTIVE, $status);
    }

    public function testGetStatusInActiveFromDataStore()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(false)
        ->getMock();

        $mockResponse = new JsonResponse(json_encode([
            'status' => Auth0User::STATUS_INACTIVE
        ]));

        $mockUserRequestService = m::mock(UserRequestService::class);

        $mockUserRequestService->shouldReceive('getUserStatus')
                               ->once()
                               ->andReturn($mockResponse);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_INACTIVE, $status);
    }

    public function testGetStatusSemiActiveFromDataStore()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(false)
        ->getMock();

        $mockResponse = new JsonResponse(json_encode([
            'status' => Auth0User::STATUS_SEMI_ACTIVE
        ]));

        $mockUserRequestService = m::mock(UserRequestService::class);

        $mockUserRequestService->shouldReceive('getUserStatus')
                               ->once()
                               ->andReturn($mockResponse);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(Auth0User::STATUS_SEMI_ACTIVE, $status);
    }

    public function testGetStatusInvalidUserId()
    {
        $mockCacheService = m::mock(FragmentedRedisCache::class)
        ->shouldReceive('setPrefix')
        ->once()
        ->shouldReceive('setHashFieldPrefix')
        ->once()
        ->shouldReceive('exists')
        ->once()
        ->andReturn(false)
        ->getMock();

        $mockResponse = new JsonResponse(null, Response::HTTP_NOT_FOUND);

        $mockUserRequestService = m::mock(UserRequestService::class);

        $mockUserRequestService->shouldReceive('getUserStatus')
                               ->once()
                               ->andReturn($mockResponse);

        $userService = new UserService($mockUserRequestService, $mockCacheService);

        $status = $userService->getStatus("1");

        $this->assertEquals(null, $status);
    }
}


