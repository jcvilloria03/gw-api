<?php

namespace Tests\Commission;

use Tests\Common\CommonAuthorizationService;

class CommissionAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Commission\CommissionAuthorizationService::class;
}
