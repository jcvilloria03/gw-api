<?php

namespace Tests\FormOption;

use App\BasePay\BasePayRequestService;
use App\Contribution\ContributionRequestService;
use App\FormOption\EmployeeFormOptionService;
use App\Tax\TaxRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Http\JsonResponse;
use Mockery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class EmployeeFormOptionServiceTest extends TestCase
{
    public function testGetPhilippineEmployeeFormOptions()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $mockBasePayRequestService = Mockery::mock(BasePayRequestService::class . '[getUnits]', [$client]);
        $mockBasePayRequestService->shouldReceive('getUnits')
            ->andReturn(
                new JsonResponse(
                    '[{"value": "TEST UNIT", "label": "Test Unit"}, {"value": "TEST UNIT 2", "label": "Test Unit 2"}]'
                )
            );
        $mockTaxRequestService = Mockery::mock(TaxRequestService::class, [$client]);
        $mockTaxRequestService->shouldReceive('getTypes')
            ->andReturn(
                new JsonResponse(
                    '[{"value": "TEST TYPE", "label": "Test Type"}, {"value": "TEST TYPE 2", "label": "Test Type 2"}]'
                )
            )
            ->shouldReceive('getStatuses')
            ->andReturn(
                new JsonResponse(
                    '[{"value": "S", "label": "Single"}, {"value": "Z", "label": "No Exemption"}]'
                )
            )
            ->shouldReceive('getConsultantTaxRates')
            ->andReturn(
                new JsonResponse(
                    '[{"value": "0.02", "label": "2%"}, {"value": "0.1", "label": "10%"}]'
                )
            );
        $mockContributionRequestService = Mockery::mock(ContributionRequestService::class . '[getBasis]', [$client]);
        $mockContributionRequestService->shouldReceive('getBasis')
            ->andReturn(
                new JsonResponse(
                    '[{"value": "TEST BASIS", "label": "Test Basis"}, ' .
                    '{"value": "TEST BASIS 2", "label": "Test Basis 2"}]'
                )
            );

        $service = new EmployeeFormOptionService(
            $mockBasePayRequestService,
            $mockTaxRequestService,
            $mockContributionRequestService
        );

        $actual = $service->getPhilippineEmployeeFormOptions();
        $expected = [
            'base_pay_unit' => [
                [
                    'label' => 'Test Unit',
                    'value' => 'TEST UNIT'
                ],
                [
                    'label' => 'Test Unit 2',
                    'value' => 'TEST UNIT 2'
                ],
            ],
            'sss_basis' => [
                [
                    'label' => 'Test Basis',
                    'value' => 'TEST BASIS'
                ],
                [
                    'label' => 'Test Basis 2',
                    'value' => 'TEST BASIS 2'
                ],
            ],
            'philhealth_basis' => [
                [
                    'label' => 'Test Basis',
                    'value' => 'TEST BASIS'
                ],
                [
                    'label' => 'Test Basis 2',
                    'value' => 'TEST BASIS 2'
                ],
            ],
            'hdmf_basis' => [
                [
                    'label' => 'Test Basis',
                    'value' => 'TEST BASIS'
                ],
                [
                    'label' => 'Test Basis 2',
                    'value' => 'TEST BASIS 2'
                ],
            ],
            'tax_type' => [
                [
                    'label' => 'Test Type',
                    'value' => 'TEST TYPE'
                ],
                [
                    'label' => 'Test Type 2',
                    'value' => 'TEST TYPE 2'
                ],
            ],
            'tax_status' => [
                [
                    'label' => 'Single',
                    'value' => 'S'
                ],
                [
                    'label' => 'No Exemption',
                    'value' => 'Z',
                ],
            ],
            'consultant_tax_rate' => [
                [
                    'label' => '2%',
                    'value' => 0.02
                ],
                [
                    'label' => '10%',
                    'value' => 0.1
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }
}
