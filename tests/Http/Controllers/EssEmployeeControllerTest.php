<?php

namespace Tests\Http\Controllers;

use App\Auth0\Auth0UserService;
use App\Employee\EssEmployeeAuthorizationService;
use App\Ess\EssPayrollRequestService;
use App\Ess\EssEmployeeRequestService;
use App\Shift\ShiftRequestService;
use App\Shift\EssShiftsAuthorizationService;
use App\ESS\EssNotificationRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class EssEmployeeControllerTest extends \Tests\TestCase
{
    public function testChangePassword()
    {
        // Dummy Test to get rid of warnings
        $this->assertEquals("OK", "OK");
    }

    // TODO Uncomment test
    // public function testChangePassword()
    // {
    //     $this->mockDependencies();

    //     $employeeId = 123;

    //     $mockAuth0UserService = \Mockery::mock(Auth0UserService::class);
    //     $mockAuth0UserService->shouldReceive('changeEmployeePassword')
    //         ->with($employeeId, 'test-password');
    //     $this->app->instance(Auth0UserService::class, $mockAuth0UserService);

    //     $mockEssEmployeeNotificationService = \Mockery::mock(EssNotificationRequestService::class);
    //     $mockEssEmployeeNotificationService->shouldReceive('sendEmailNotificationToUser')
    //         ->andReturn(['email' => 'test@test.com']);
    //     $this->app->instance(EssNotificationRequestService::class, $mockEssEmployeeNotificationService);

    //     $mockEssEmployeeAuthorizationService = \Mockery::mock(EssEmployeeAuthorizationService::class);
    //     $mockEssEmployeeAuthorizationService->shouldReceive('authorizeGet')->andReturn(true);
    //     $this->app->instance(EssEmployeeAuthorizationService::class, $mockEssEmployeeAuthorizationService);

    //     $this->currentUri = $this->prepareUrlForRequest('/ess/change_password');
    //     $symfonyRequest = SymfonyRequest::create($this->currentUri, 'POST', [
    //         'new_password' => 'test-password',
    //         'new_password_confirmation' => 'test-password',
    //     ], [], [], [], null);

    //     $request = Request::createFromBase($symfonyRequest);
    //     $request->attributes->set('user', [
    //         'employee_id' => $employeeId,
    //         'user_id' => 123123
    //     ]);

    //     $this->handle($request);

    //     $result = json_decode($this->response->getContent(), true);

    //     $this->assertArrayHasKey('password_changed', $result);
    //     $this->assertTrue($result['password_changed']);
    // }

    // protected function mockDependencies()
    // {
    //     $this->app->instance(
    //         EssEmployeeRequestService::class,
    //         \Mockery::mock(EssEmployeeRequestService::class)
    //     );
    //     $this->app->instance(
    //         EssPayrollRequestService::class,
    //         \Mockery::mock(EssPayrollRequestService::class)
    //     );
    //     $this->app->instance(
    //         ShiftRequestService::class,
    //         \Mockery::mock(ShiftRequestService::class)
    //     );
    //     $this->app->instance(
    //         EssShiftsAuthorizationService::class,
    //         \Mockery::mock(EssShiftsAuthorizationService::class)
    //     );
    // }
}
