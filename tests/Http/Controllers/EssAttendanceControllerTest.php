<?php

namespace Tests\Http\Controllers;

use Mockery as M;
use Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Attendance\AttendanceRequestService;
use App\Http\Controllers\EssAttendanceController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssAttendanceControllerTest extends TestCase
{

    public function testGetSingleAttendanceReturns401OnEmployeeIdMismatch()
    {
        $controller = new EssAttendanceController();

        $request = Request::create(
            '/test/ess/attendance/2/2019-01-01',
            'GET'
        );

        $userData = [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ];

        $attendanceRequestService = M::mock(AttendanceRequestService::class);
        $attendanceRequestService->shouldReceive('getSingleAttendanceRecord')
            ->never();

        $request->attributes->set('companies_users', [$userData]);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Unauthorized');
        $controller->getSingleAttendanceRecord(
            $request,
            $attendanceRequestService,
            2,
            '2019-01-01'
        );
    }

    public function testGetSingleAttendanceReturns200OnEmployeeMatch()
    {
        $controller = new EssAttendanceController();

        $request = Request::create(
            '/test/ess/attendance/1/2019-01-01',
            'GET'
        );

        $userData = [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ];

        $attendanceRequestService = M::mock(AttendanceRequestService::class);
        $attendanceRequestService->shouldReceive('getSingleAttendanceRecord')
            ->with(1, '2019-01-01')
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    'employee' => [
                        'uid' => '1',
                    ],

                    'company_id'       => '1',
                    'date'             => '2019-01-01',
                    'locked'           => null,
                    'attendance'       => [],
                    'scheduled_shifts' => []
                ]
            ])));

        $request->attributes->set('companies_users', [$userData]);
        $response = $controller->getSingleAttendanceRecord(
            $request,
            $attendanceRequestService,
            1,
            '2019-01-01'
        );

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());
    }
}
