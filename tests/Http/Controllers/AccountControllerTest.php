<?php

namespace Tests\Http\Controllers;

use App\Account\AccountRequestService;
use App\User\UserRequestService;
use App\Audit\AuditService;
use App\Role\DefaultRoleService;
use App\Role\UserRoleService;
use App\Auth0\Auth0UserService;
use App\Authentication\AuthenticationRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Account\AccountAuthorizationService;
use App\Company\CompanyRequestService;
use App\Holiday\HolidayRequestService;
use App\Http\Controllers\AccountController;
use App\Model\Auth0User;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Mockery;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;
use function GuzzleHttp\json_encode;
use Bschmitt\Amqp\Facades\Amqp;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AccountControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function getInstance()
    {
        $mockResponse = collect([
            "id" => 1,
            "user_id" => 123,
            "company_id" => 456,
            "module_access" => ["HRIS", "T&A", "Payroll"]
        ]);
        $mockAuthRequestResponse = [
            'data' => [
                'user' => [
                    'auth0_user_id' => 1,
                    'id_token' => 1234,
                    'access_token' => 'sadfuioprewq',
                ]
            ]
        ];

        $mockOwnerRole = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test custom role',
            'type' => Role::ADMIN,
            'custom_role' => true,
        ]);
        $mockSuperAdminRole = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test custom role',
            'type' => Role::ADMIN,
            'custom_role' => true,
        ]);
        $mockAccountRequestService = Mockery::mock(AccountRequestService::class);
        $mockAccountRequestService->shouldReceive('signUp')
            ->andReturn(new JsonResponse($mockResponse->toJson()));

        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('createSystemDefinedRoles')
            ->andReturn(new JsonResponse(collect([
                'created_roles' => [],
                'updated_roles' => [],
                'assigned_user_ids' => []
            ])->toJson()));

        $mockUserRequestService = Mockery::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(collect(['product_seats' => 2])->toJson()));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockDefaultRoleService = Mockery::mock(DefaultRoleService::class);
        $mockDefaultRoleService->shouldReceive('createOwnerRole')
            ->andReturn($mockOwnerRole);
        $mockDefaultRoleService->shouldReceive('createSuperAdminRole')
            ->andReturn($mockSuperAdminRole);
        $mockDefaultRoleService->shouldReceive('createAdminRole');
        $mockDefaultRoleService->shouldReceive('createCompanyEmployeeRole');
        $mockUserRoleService = Mockery::mock(UserRoleService::class);
        $mockUserRoleService->shouldReceive('create');
        $mockAuth0UserService = Mockery::mock(Auth0UserService::class);
        $mockAuth0UserService->shouldReceive('create');

        $mockAuthenticationRequestService = Mockery::mock(AuthenticationRequestService::class);
        $mockAuthenticationRequestService->shouldReceive('signUp')
            ->andReturn(new JsonResponse(json_encode($mockAuthRequestResponse)));
        $mockAccountAuthorizationService = Mockery::mock(AccountAuthorizationService::class);
        $mockSubscriptionsService = Mockery::mock(SubscriptionsRequestService::class);
        $mockSubscriptionsService
            ->shouldReceive('createCustomer')
            ->shouldReceive('getModuleAccessByPlanId')
            ->andReturn(["HRIS", "T&A"]);

        return new AccountController(
            $mockAccountRequestService,
            $mockUserRequestService,
            $mockAuditService,
            $mockDefaultRoleService,
            $mockUserRoleService,
            $mockAuth0UserService,
            $mockAuthenticationRequestService,
            $mockAccountAuthorizationService,
            $mockSubscriptionsService,
            $mockCompanyRequestService
        );
    }

    public function testSignUp()
    {
        $mockAccountController = $this->getInstance();

        $request = Request::create('account/signUp', 'POST', [
            'name' => 'test',
            'first_name' => 'testing',
            'last_name' => 'testing',
            'company' => 'testing',
            'email' => 'test@email.com',
            'password' => '12345678',
            'plan_id' => 1
        ]);
        $request->attributes->add([
            'decodedToken' => (object)['sub' => 1223]
        ]);
        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('createSystemDefinedRoles')
            ->andReturn(new JsonResponse(collect([
                'created_roles' => [],
                'updated_roles' => [],
                'assigned_user_ids' => []
            ])->toJson()));

        $mockHolidayRequestService = Mockery::mock(HolidayRequestService::class);
        $mockHolidayRequestService->shouldReceive('createCompanyDefaultHolidays')
            ->andReturn(new JsonResponse(collect([])->toJson()));

        Amqp::shouldReceive('publish')
            ->times(1);

        $actual = $mockAccountController->signUp($request, $mockCompanyRequestService, $mockHolidayRequestService);
        $expected = [
            'data' => [
                'user' => [
                    'id_token' => 1234,
                    'access_token' => 'sadfuioprewq'
                ]
            ]
        ];

        $this->assertEquals($actual, $expected);
    }
}
