<?php

namespace Tests\Http\Controllers;

use App\Account\AccountRequestService;
use Tests\TestCase;
use Mockery;
use App\CSV\CsvValidator;
use App\Http\Controllers\AnnualEarningController;
use App\AnnualEarning\AnnualEarningAuditService;
use App\AnnualEarning\AnnualEarningRequestService;
use App\AnnualEarning\AnnualEarningAuthorizationService;
use App\Authz\AuthzDataScope;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Http\Middleware\AuthzMiddleware;
use Illuminate\Http\Request;
use App\Storage\UploadService;
use App\User\UserRequestService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Illuminate\Http\JsonResponse;

/**
 * @SuppressWarnings(PHPMD.Generic.Files.LineLength.TooLong)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */

class AnnualEarningControllerTest extends TestCase
{
    use RequestServiceTrait;

    public function testAnnualEarningUploadOk()
    {
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
        ];

        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')->andReturn(1);
        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);
        $mockAuthzRequestService = Mockery::mock(AuthzRequestService::class);
        $mockAuthzRequestService->shouldReceive('checkSalariumClearance')->andReturn([
            'employees.annual_earnings' => [
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1]
                ]
            ]
        ]);
        $this->app->instance(AuthzRequestService::class, $mockAuthzRequestService);

        $mockUserRequestService = Mockery::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('getEssentialData')->andReturn([
            'subject' => [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1
            ],
            'userData' => [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ]
        ]);
        $this->app->instance(UserRequestService::class, $mockUserRequestService);

        $mockAccountRequestService = Mockery::mock(AccountRequestService::class);
        $mockAccountRequestService->shouldReceive('getAccountEssentialData')->andReturn(new JsonResponse(json_encode([
            [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1]
            ]
        ])));
        $this->app->instance(AccountRequestService::class, $mockAccountRequestService);

        $mockAuthService = Mockery::mock(AnnualEarningAuthorizationService::class);
        $mockAuthService->shouldReceive('authorizeCreate')->andReturn(true);

        $mockUploadService = Mockery::mock(UploadService::class);
        $mockUploadService->shouldReceive('saveFile')->andReturn('somekey');
        $mockUploadService->shouldReceive('getS3Bucket')->andReturn('somebucket');

        $mockRequestService = Mockery::mock(AnnualEarningRequestService::class);
        $mockRequestService->shouldReceive('processFileUpload')->andReturn(
            response()->json(
                collect($mockData)->toJson()
            )
        );
        $mockCsvValidator = Mockery::mock(CsvValidator::class);

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\AnnualEarningController[getCompanyAccountId]',
            [
                $mockRequestService,
                $mockAuthService,
                $mockCsvValidator,
                $mockUploadService
            ]
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        $this->app->instance(AnnualEarningController::class, $partialMockedController);

        $mockFile = UploadedFile::fake()->create('test.csv');
        $mockFile->mimeType ="text/csv";

        $request = Request::create(
            "/annual_earning/upload",
            'POST',
            ['company_id' => 1],
            [],
            ['file'=>$mockFile],
            $this->transformHeadersToServerVars(["X-Authz-Entities" => "employees.annual_earnings"])
        );
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request)
            ->shouldReturnJson(
                [
                    'job_id' => 'foo'
                ]
            );
    }

    public function testGetAnnualEarningUploadStatusVE110()
    {

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    'user_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.cost_centers' => [
                    'data_scope' => [
                        'COMPANY' => [123]
                    ]
                ]
            ]
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => $this->getJsonResponse([
                'body' => [
                    'account_id' => 1
                ]
            ]),
        ]);

        $this->mockRequestService(AccountRequestService::class, [
            'body' => [
                'body' => [
                    1 => [
                        'department' => [1],
                        'position' => [1],
                        'team' => [1],
                        'location' => [1],
                        'payroll_group' => [1],
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(AnnualEarningRequestService::class, [
            'body' => [
                'body' => []
            ]
        ]);

        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(UploadService::class);
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-110",
                                            "name" => "Missing Headers",
                                            "details" => [
                                                [
                                                    "parameters" => [
                                                        "Year",
                                                        "Employee ID"
                                                    ]
                                                ]
                                            ]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AnnualEarningAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AnnualEarningAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(AnnualEarningRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );
        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\AnnualEarningController[getCompanyAccountId]',
            [
                $mockRequestService,
                $mockAuthorizationService,
                $mockValidator,
                $mockUploadService
            ]
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(1223);
        $this->app->instance(AnnualEarningController::class, $partialMockedController);

        $request = Request::create(
            "/annual_earning/upload/status",
            'GET',
            [
                'job_id' => 'foo',
                'company_id' => 123,
            ],
            [],
            [],
            $this->transformHeadersToServerVars(["X-Authz-Entities" => "employees.annual_earnings"])
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Year header is required.",
                        "Employee ID header is required."
                    ]
                ]
            ]
        );
    }

    public function testDeleteAnnualEarning()
    {
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(UploadService::class);

        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AnnualEarningAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AnnualEarningAuthorizationService::class, $mockAuthorizationService);
        $mockData = [
            "test" => "ok"
        ];

        $mockRequestService = Mockery::mock(AnnualEarningRequestService::class);
        $mockRequestService->shouldReceive('deleteAnnualEarning')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );
        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\AnnualEarningController[getCompanyAccountId]',
            [
                $mockRequestService,
                $mockAuthorizationService,
                $mockValidator,
                $mockUploadService
            ]
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        $this->app->instance(AnnualEarningController::class, $partialMockedController);

        $request = Request::create(
            "/company/123/annual_earning/111",
            'DELETE',
            [],
            [],
            [],
            $this->transformHeadersToServerVars(["X-Authz-Entities" => "employees.annual_earnings"])
        );
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);

        $response->shouldReturnJson(
            [
                "test" => "ok"
            ]
        );

    }

    public function testDeleteAnnualEarningBulk()
    {

        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(UploadService::class);
        //setup mocks::
        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')->andReturn(1);
        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);

        $mockAuthzRequestService = Mockery::mock(AuthzRequestService::class);
        $mockAuthzRequestService->shouldReceive('checkSalariumClearance')->andReturn([
            'employees.annual_earnings' => [
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1]
                ]
            ]
        ]);
        $this->app->instance(AuthzRequestService::class, $mockAuthzRequestService);

        $mockUserRequestService = Mockery::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('getEssentialData')->andReturn([
            'subject' => [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1
            ],
            'userData' => [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ]
        ]);
        $this->app->instance(UserRequestService::class, $mockUserRequestService);


        $mockAccountRequestService = Mockery::mock(AccountRequestService::class);
        $mockAccountRequestService->shouldReceive('getAccountEssentialData')->andReturn(new JsonResponse(json_encode([
            [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1]
            ]
        ])));
        $this->app->instance(AccountRequestService::class, $mockAccountRequestService);

        $mockAuthorizationService = Mockery::mock(AnnualEarningAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);
        $this->app->instance(AnnualEarningAuthorizationService::class, $mockAuthorizationService);
        $mockData = [
            "test" => "ok"
        ];


        $mockRequestService = Mockery::mock(AnnualEarningRequestService::class);
        $mockRequestService->shouldReceive('deleteAnnualEarning')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );
        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\AnnualEarningController[getCompanyAccountId]',
            [
                $mockRequestService,
                $mockAuthorizationService,
                $mockValidator,
                $mockUploadService
            ]
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        $this->app->instance(AnnualEarningController::class, $partialMockedController);

        $request = Request::create(
            "/company/1/annual_earning/bulk_delete",
            'POST',
            [],
            [],
            [],
            $this->transformHeadersToServerVars([
                "Content-type" => "application/json",
                "X-Authz-Entities" => "employees.annual_earnings"
            ]),
            json_encode(["ids" => [1, 2, 3]])
        );
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ],
            'authz_data_scope'=> [
                'COMPANY' => [1]
            ]
        ]);

        $response = $this->handle($request);

        $response->shouldReturnJson(
            [
                "test" => "ok"
            ]
        );

    }
}
