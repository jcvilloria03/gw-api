<?php

namespace Tests\Http\Controllers;

use App\Bank\BankRequestService;
use App\Http\Controllers\CompanyBankController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpFoundation\Response;
use Mockery;
use App\Audit\AuditService;
use App\Bank\CompanyBankAuthorizationService;
use App\Company\PhilippineCompanyRequestService;

class CompanyBankControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testGet()
    {
        $expected = [
            "data" => [
                [
                    "type" => "company-bank-account",
                    "id" => "9",
                    "attributes" => [
                        "bankBranch" => "1650 Peñafrancia, Makati, 1208 Metro Manila",
                        "accountNumber" => "12121",
                        "accountType" => "SAVINGS",
                        "companyCode" => "1212",
                        "bank" => [
                            "id" => "9",
                            "name" => "BANK OF TOKYO-MITSUBISHI UFJ LIMITED"
                        ]
                    ]
                ],
                [
                    "type" => "company-bank-account",
                    "id" => "10",
                    "attributes" => [
                        "bankBranch" => "1651 Peñafrancia, Makati, 1208 Metro Manila",
                        "accountNumber" => "12122",
                        "accountType" => "SAVINGS",
                        "companyCode" => "1213",
                        "bank" => [
                            "id" => "10",
                            "name" => "BANCO FILIPINO SAVINGS AND MORTGAGE BANK"
                        ]
                    ]
                ]
            ]
        ];

        $mockCompany = collect([
            "id" => 1,
            "name" => "Company XYZ Inc.",
            "account_id" => 1
        ]);
        $mockCompanyId = 1;
        

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('getCompanyBankAccounts')
            ->andReturn(new JsonResponse(collect($expected)->toJson(), 200));
            
        $mockAuditService = Mockery::mock(AuditService::class);

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockCompanyService->shouldReceive(['get' => new JsonResponse($mockCompany->toJson())]);

        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockAuthorizationService->shouldReceive(['authorizeGet' => true]);
        
        $request = Request::create('/company/' . $mockCompanyId . '/banks');
        $request->attributes->add(['user' => [ 'user_id' => 1, 'account_id' => 1 ]]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);
        $this->app->instance(Request::class, $request);

        $mockCompanyBankController = new CompanyBankController();

        $actualResponse = $mockCompanyBankController->get($mockCompanyId);
        $actual = json_decode($actualResponse->getData(), true);

        $this->assertEquals(Response::HTTP_OK, $actualResponse->getStatusCode());
        $this->assertEquals($expected, $actual);
    }

    public function testGetInvalidCompanyId()
    {
        $mockCompanyId = 3.14;
        $mockAuditService = Mockery::mock(AuditService::class);

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        
        $request = Request::create('/company/' . $mockCompanyId . '/banks');
        $request->attributes->add(['user' => [ 'user_id' => 1, 'account_id' => 1 ]]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);
        $this->app->instance(Request::class, $request);
        
        $mockCompanyBankController = new CompanyBankController();
        
        $this->expectException(NotAcceptableHttpException::class);
        $mockCompanyBankController->get($mockCompanyId);
    }

    public function testStore()
    {
        $mockCompanyId = 12;
        $expected = [
            'data' => [
                'type' => 'company-bank-account',
                'id' => '888',
                'attributes' => [
                    'bankBranch' => '1650 Peñafrancia, Makati, 1208 Metro Manila',
                    'accountNumber' => '324323242342',
                    'accountType' => 'SAVINGS',
                    'companyCode' => '21321',
                    'bank' => [
                        'id' => '33441',
                        'name' => 'FOOBAR BANK OF EXAMPLE',
                    ],
                ],
            ],
        ];
        $mockCompany = collect([
            "id" => 1,
            "name" => "Company XYZ Inc.",
            "account_id" => 1
        ]);

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addCompanyBankAccount')
        ->andReturn(new JsonResponse(collect($expected)->toJson(), 201));

        $mockAuditService = Mockery::mock(AuditService::class);

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockCompanyService->shouldReceive(['get' => new JsonResponse($mockCompany->toJson())]);

        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockAuthorizationService->shouldReceive(['authorizeCreate' => true]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);

        $mockCompanyBankController = new CompanyBankController();

        $mockRequest = Request::create(
            "/company/{$mockCompanyId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([ // $content
                'data' => [
                    'type' => 'company-bank-account',
                    'attributes' => [
                        'bank_id' => '33441',
                        'bank_branch' => '1650 Peñafrancia, Makati, 1208 Metro Manila',
                        'account_number' => '324323242342',
                        'account_type' => 'SAVINGS',
                        'company_code' => '21321',
                    ],
                ],
            ])
        );

        $mockRequest->attributes->add(['user' => [ 'user_id' => 1, 'account_id' => $mockCompanyId ]]);

        $actualResponse = $mockCompanyBankController->store($mockRequest, $mockCompanyId);
        $actual = json_decode($actualResponse->getData(), true);

        $this->assertEquals(Response::HTTP_CREATED, $actualResponse->getStatusCode());
        $this->assertEquals($expected, $actual);
    }

    public function testStoreInvalidJsonBody()
    {
        $mockCompanyId = 12;
        $expected = [
            'data' => [
                'type' => 'company-bank-account',
                'id' => '888',
                'attributes' => [
                    'bankBranch' => '1650 Peñafrancia, Makati, 1208 Metro Manila',
                    'accountNumber' => '324323242342',
                    'accountType' => 'SAVINGS',
                    'companyCode' => '21321',
                    'bank' => [
                        'id' => '33441',
                        'name' => 'FOOBAR BANK OF EXAMPLE',
                    ],
                ],
            ],
        ];

        $mockCompany = collect([
            "id" => 1,
            "name" => "Company XYZ Inc.",
            "account_id" => 1
        ]);

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addCompanyBankAccount')
        ->andReturn(new JsonResponse(collect($expected)->toJson(), 201));

        $mockAuditService = Mockery::mock(AuditService::class);

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockCompanyService->shouldReceive(['get' => new JsonResponse($mockCompany->toJson())]);

        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockAuthorizationService->shouldReceive(['authorizeCreate' => true]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);
        
        $mockCompanyBankController = new CompanyBankController();

        $mockRequest = Request::create(
            "/company/{$mockCompanyId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            '{"data:{"type":"company-bank-account","attributes":{"bank_id":"33441",' .
                '"bank_branch":"1650 Pe\u00f1afrancia, Makati, 1208 Metro Manila",' .
                '"account_number":"324323242342","account_type":"SAVINGS",' .
                '"company_code":"21321"}}}'
        );

        $this->expectException(NotAcceptableHttpException::class);
        $mockCompanyBankController->store($mockRequest, $mockCompanyId);
    }

    public function testStoreExceptionInRequestService()
    {
        $mockCompanyId = 12;

        $mockCompany = collect([
            "id" => 1,
            "name" => "Company XYZ Inc.",
            "account_id" => 1
        ]);

        $mockAuditService = Mockery::mock(AuditService::class);
        
        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addCompanyBankAccount')
        ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockCompanyService->shouldReceive(['get' => new JsonResponse($mockCompany->toJson())]);

        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockAuthorizationService->shouldReceive(['authorizeCreate' => true]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);

        $mockCompanyBankController = new CompanyBankController();

        $mockRequest = Request::create(
            "/company/{$mockCompanyId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([ // $content
                'data' => [
                    'type' => 'company-bank-account',
                    'attributes' => [
                        'bank_id' => '33441',
                        'bank_branch' => '1650 Peñafrancia, Makati, 1208 Metro Manila',
                        'account_number' => '324323242342',
                        'account_type' => 'SAVINGS',
                        'company_code' => '21321',
                    ],
                ],
            ])
        );
        $mockRequest->attributes->add(['user' => [ 'user_id' => 1, 'account_id' => $mockCompanyId ]]);

        $this->expectException(HttpException::class);
        $mockCompanyBankController->store($mockRequest, $mockCompanyId);
    }

    public function testDelete()
    {
        $mockRequestBody =[ // $content
            'data' => [
                [
                'type' => 'company-bank-account',
                'id' => '888'
                
                ],
            ],
        ];
        $mockCompanyId = 12;
        $expected = [
            'data' => [[
                'type' => 'company-bank-account',
                'id' => '888',
                'attributes' => [
                    'bankBranch' => '1650 Peñafrancia, Makati, 1208 Metro Manila',
                    'accountNumber' => '324323242342',
                    'accountType' => 'SAVINGS',
                    'companyCode' => '21321',
                    'bank' => [
                        'id' => '33441',
                        'name' => 'FOOBAR BANK OF EXAMPLE',
                    ],
                ],
            ]],
        ];

        $mockCompany = collect([
            "id" => 1,
            "name" => "Company XYZ Inc.",
            "account_id" => 1
        ]);

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('deleteCompanyBanks')
        ->andReturn(new JsonResponse(collect($expected)->toJson(), 200));
        
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue')->once();

        $mockCompanyService = Mockery::mock(PhilippineCompanyRequestService::class);
        $mockCompanyService->shouldReceive(['get' => new JsonResponse($mockCompany->toJson())]);

        $mockAuthorizationService = Mockery::mock(CompanyBankAuthorizationService::class);
        $mockAuthorizationService->shouldReceive(['authorizeDelete' => true]);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(CompanyBankAuthorizationService::class, $mockAuthorizationService);
        $this->app->instance(PhilippineCompanyRequestService::class, $mockCompanyService);

        $mockCompanyBankController = new CompanyBankController();

        $mockRequest = Request::create(
            "/company/{$mockCompanyId}/bank",
            'DELETE',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($mockRequestBody)
        );

        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => $mockCompanyId,
            ]
        ]);

        $actualResponse = $mockCompanyBankController->deleteCompanyBanks($mockRequest, $mockCompanyId);
        $actual = json_decode($actualResponse->getData(), true);

        $this->assertEquals(Response::HTTP_OK, $actualResponse->getStatusCode());
        $this->assertEquals($expected, $actual);
    }
}
