<?php

namespace Tests\Http\Controllers;

use App\CompanyUser\CompanyUserService;
use App\Model\CompanyUser;
use App\Payroll\PayrollAuthorizationService;
use App\Payroll\PayrollFinalPayRequestService;
use Dingo\Api\Event\RequestWasMatched;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class PayrollFinalPayControllerTest extends TestCase
{
    /**
     * @group api
     * @group payroll-final
     */
    public function testCreatePayrollFinalSuccess()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(PayrollAuthorizationService::class, function () {
            $authorizationService = $this->createMock(PayrollAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(true);

            return $authorizationService;
        });
        $this->app->bind(PayrollFinalPayRequestService::class, function () {
            $mock = new MockHandler([new Response(201)]);
            $handler = HandlerStack::create($mock);
            $client = new Client(['handler' => $handler]);

            return new PayrollFinalPayRequestService($client);
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/payroll/final', []);
        $response->assertResponseStatus(201);
    }

    /**
     * @group api
     * @group payroll-final
     */
    public function testSaveFinalPayUnauthorized()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(PayrollAuthorizationService::class, function () {
            $authorizationService = $this->createMock(PayrollAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(false);

            return $authorizationService;
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/payroll/final', []);
        $response->assertResponseStatus(401);
    }

    /**
     * @group api
     * @group payroll-final
     */
    public function testSaveFinalPayUnprocessable()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(PayrollAuthorizationService::class, function () {
            $authorizationService = $this->createMock(PayrollAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(true);

            return $authorizationService;
        });
        $this->app->bind(PayrollFinalPayRequestService::class, function () {
            $mock = new MockHandler([new Response(422)]);
            $handler = HandlerStack::create($mock);
            $client = new Client(['handler' => $handler]);

            return new PayrollFinalPayRequestService($client);
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/payroll/final', []);
        $response->assertResponseStatus(422);
    }
}
