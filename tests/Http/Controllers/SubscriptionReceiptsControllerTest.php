<?php
namespace Tests\Http\Controllers;

use Mockery as M;
use Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User\UserRequestService;
use Illuminate\Http\JsonResponse;
use App\Subscriptions\SubscriptionsRequestService;
use App\Http\Controllers\SubscriptionReceiptsController;
use App\Subscriptions\SubscriptionsAuthorizationService;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SubscriptionReceiptsControllerTest extends TestCase
{
    public function testSearchUnauthorized()
    {
        $requestService = M::mock(SubscriptionsRequestService::class);
        $requestService->shouldNotReceive('searchReceipts');

        $userRequestService = M::mock(UserRequestService::class);
        $userRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode([
                'account_id' => 1,
                'companies'  => [
                    ['id' => 1]
                ]
            ])));

        $authorizationService = M::mock(SubscriptionsAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeGet')
            ->andReturn(false);

        $controller = new SubscriptionReceiptsController(
            $requestService,
            $authorizationService,
            $userRequestService
        );

        $mockRequest = Request::create(
            '/subscriptions/receipts',
            'GET',
            [
                'filter' => [
                    'invoice_id' => [
                        1
                    ]
                ]
            ], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          =>  1
        ]);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Unauthorized');
        $controller->search($mockRequest);
    }

    public function testSearchWithoutInvoiceId()
    {
        $requestService = M::mock(SubscriptionsRequestService::class);
        $requestService->shouldReceive('searchReceipts')
            ->with([1], [])
            ->andReturn(new JsonResponse(json_encode([
                'data' => []
            ])));

        $userRequestService = M::mock(UserRequestService::class);
        $userRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode([
                'account_id' => 1,
                'companies'  => [
                    ['id' => 1]
                ]
            ])));

        $authorizationService = M::mock(SubscriptionsAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $controller = new SubscriptionReceiptsController(
            $requestService,
            $authorizationService,
            $userRequestService
        );

        $mockRequest = Request::create(
            '/subscriptions/receipts',
            'GET',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          =>  1
        ]);

        $res = $controller->search($mockRequest);

        $this->assertTrue(array_key_exists(
            'data',
            json_decode($res->getData(), true)
        ));
    }

    public function testSearchWithInvoiceId()
    {
        $requestService = M::mock(SubscriptionsRequestService::class);
        $requestService->shouldReceive('searchReceipts')
            ->with([1], [1])
            ->andReturn(new JsonResponse(json_encode([
                'data' => []
            ])));

        $userRequestService = M::mock(UserRequestService::class);
        $userRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode([
                'account_id' => 1,
                'companies'  => [
                    ['id' => 1]
                ]
            ])));

        $authorizationService = M::mock(SubscriptionsAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $controller = new SubscriptionReceiptsController(
            $requestService,
            $authorizationService,
            $userRequestService
        );

        $mockRequest = Request::create(
            '/subscriptions/receipts',
            'GET',
            [
                'filter' => [
                    'invoice_id' => [
                        1
                    ]
                ]
            ], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          =>  1
        ]);

        $res = $controller->search($mockRequest);

        $this->assertTrue(array_key_exists(
            'data',
            json_decode($res->getData(), true)
        ));
    }
}
