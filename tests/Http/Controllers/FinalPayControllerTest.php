<?php

namespace Tests\Http\Controllers;

use App\CompanyUser\CompanyUserService;
use App\FinalPay\FinalPayAuthorizationService;
use App\FinalPay\FinalPayRequestService;
use App\Model\CompanyUser;
use Dingo\Api\Event\RequestWasMatched;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class FinalPayControllerTest extends TestCase
{
    /**
     * @group api
     * @group final-pay
     */
    public function testSaveFinalPaySuccess()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(FinalPayAuthorizationService::class, function () {
            $authorizationService = $this->createMock(FinalPayAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(true);

            return $authorizationService;
        });
        $this->app->bind(FinalPayRequestService::class, function () {
            $mock = new MockHandler([new Response(201)]);
            $handler = HandlerStack::create($mock);
            $client = new Client(['handler' => $handler]);

            return new FinalPayRequestService($client);
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/final_pay', []);
        $response->assertResponseStatus(201);
    }

    /**
     * @group api
     * @group final-pay
     */
    public function testSaveFinalPayUnauthorized()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(FinalPayAuthorizationService::class, function () {
            $authorizationService = $this->createMock(FinalPayAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(false);

            return $authorizationService;
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/final_pay', []);
        $response->assertResponseStatus(401);
    }

    /**
     * @group api
     * @group final-pay
     */
    public function testSaveFinalPayUnprocessable()
    {
        $this->markTestSkipped('Temporarily disable Final Pay.');
        $this->app->bind(CompanyUserService::class, function () {
            $companyUserService = $this->createMock(CompanyUserService::class);
            $companyUserService->method('getByUserId')->willReturn(new CompanyUser(['company_id' => 1]));

            return $companyUserService;
        });
        $this->app->bind(FinalPayAuthorizationService::class, function () {
            $authorizationService = $this->createMock(FinalPayAuthorizationService::class);
            $authorizationService->method('authorizeCreate')->willReturn(true);

            return $authorizationService;
        });
        $this->app->bind(FinalPayRequestService::class, function () {
            $mock = new MockHandler([new Response(422)]);
            $handler = HandlerStack::create($mock);
            $client = new Client(['handler' => $handler]);

            return new FinalPayRequestService($client);
        });

        Event::listen(RequestWasMatched::class, function (RequestWasMatched $event) {
            $event->request->attributes->set('user', ['user_id' => 1, 'account_id' => 1]);
        });

        $response = $this->json('POST', '/final_pay', []);
        $response->assertResponseStatus(422);
    }
}
