<?php

namespace Tests\Http\Controllers;

use App\FinalPay\FinalPayAuthorizationService;
use App\FinalPay\FinalPayRequestService;
use App\Jobs\JobsRequestService;
use Illuminate\Http\Request;
use App\Payroll\PayrollRequestService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\Payroll\PayrollAuthorizationService;
use App\Audit\AuditService;
use App\Storage\PayrollUploadService;
use App\Http\Controllers\PayrollController;
use Mockery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App;
use App\Payroll\PayrollService;
use Dingo\Api\Http\Response as DingoResponse;
use App\User\UserRequestService;
use App\Employee\EmployeeRequestService;
use App\Company\CompanyRequestService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class PayrollControllerTest extends \Tests\TestCase
{
    public function getInstance()
    {
        $mockRequestServiceResponse = collect([
            'start_date' => '2019-04-28',
            'end_date' => '2019-04-29',
            'company_id' => 1,
            'payroll_group_id' => 1,
            'type' => 'regular'
        ]);

        $mockJobsRequestServiceResponse = collect([
            'data' => [
                'id' => 1
            ]
        ]);

        $mockRequestService = Mockery::mock(PayrollRequestService::class);
        $mockRequestService->shouldReceive('addUploadJobToPayroll');
        $mockRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockRequestServiceResponse->toJson()));
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeUpload')
            ->andReturn(true);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockJobsRequestService->shouldReceive('getJobStatus');
        $mockJobsRequestService->shouldReceive('createJob')
            ->andReturn(new JsonResponse($mockJobsRequestServiceResponse->toJson()));
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockPayrollUploadService->shouldReceive('saveFile')
            ->andReturn(1);
        $mockPayrollUploadService->shouldReceive('getS3Bucket');
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        return new PayrollController(
            $mockRequestService,
            $mockPayrollGroupRequestService,
            $mockAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );
    }

    public function testCalculationStatus()
    {
        $payrollId = 12;

        $mockRequestServiceResponse = collect([
            'start_date' => '2019-04-28',
            'end_date' => '2019-04-29',
            'company_id' => 1,
            'payroll_group_id' => 1
        ]);

        $mockEmployeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $mockEmployeeRequestService->shouldReceive('getEmployeeInfo')
            ->andReturn(new JsonResponse(json_encode([
                'payroll_group' => [
                    'id' => 1
                ]
            ])));

        $this->app->instance(EmployeeRequestService::class, $mockEmployeeRequestService);

        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')
            ->andReturn(1);

        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);

        $mockRequestService = Mockery::mock(PayrollRequestService::class);
        $mockRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockRequestServiceResponse->toJson()));
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeUpload')
            ->andReturn(true);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockJobsRequestService->shouldReceive('getJobStatus')
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    'attributes' => [
                        'payload' => [
                            'employee_id' => 1212,
                            'companyId' => 1,
                        ]
                    ]
                ]
            ])));
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $mockPayrollController = new PayrollController(
            $mockRequestService,
            $mockPayrollGroupRequestService,
            $mockAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $request = Request::create("/payroll/{$payrollId}/calculate/status", 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $mockPayrollController->calculationStatus($request, $payrollId);

        $request = Request::create("/payroll/{$payrollId}/calculate/status", 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $mockPayrollController->calculationStatus($request, $payrollId);

        $request = Request::create("/payroll/{$payrollId}/calculate/status", 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $mockPayrollController->calculationStatus($request, $payrollId);

        $request = Request::create("/payroll/{$payrollId}/calculate/status", 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $mockPayrollController->calculationStatus($request, $payrollId);
    }

    public function testUploadAllowanceStatus()
    {
        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/allowance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $mockPayrollController->uploadAllowanceStatus($request);

        $request = Request::create('/payroll/upload/allowance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $mockPayrollController->uploadAllowanceStatus($request);

        $request = Request::create('/payroll/upload/allowance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $mockPayrollController->uploadAllowanceStatus($request);

        $request = Request::create('/payroll/upload/allowance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $mockPayrollController->uploadAllowanceStatus($request);
    }

    public function testUploadAllowanceStatusInvalidIncludeParameter()
    {
        $expected = 406;

        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/allowance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'asdsadadsadasd',
        ]);

        $actual = $mockPayrollController->uploadAllowanceStatus($request);

        $this->assertEquals($expected, $actual->getStatusCode());
    }

    public function testUploadAttendanceStatus()
    {
        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/attendance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $mockPayrollController->uploadAttendanceStatus($request);

        $request = Request::create('/payroll/upload/attendance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $mockPayrollController->uploadAttendanceStatus($request);

        $request = Request::create('/payroll/upload/attendance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $mockPayrollController->uploadAttendanceStatus($request);

        $request = Request::create('/payroll/upload/attendance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $mockPayrollController->uploadAttendanceStatus($request);
    }

    public function testUploadAttendanceStatusInvalidIncludeParameter()
    {
        $expected = 406;

        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/attendance/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'asdsadadsadasd',
        ]);

        $actual = $mockPayrollController->uploadAttendanceStatus($request);

        $this->assertEquals($expected, $actual->getStatusCode());
    }

    public function testUploadBonusStatus()
    {
        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/bonus/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $mockPayrollController->uploadBonusStatus($request);

        $request = Request::create('/payroll/upload/bonus/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $mockPayrollController->uploadBonusStatus($request);

        $request = Request::create('/payroll/upload/bonus/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $mockPayrollController->uploadBonusStatus($request);

        $request = Request::create('/payroll/upload/bonus/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $mockPayrollController->uploadBonusStatus($request);
    }

    public function testUploadBonusStatusInvalidIncludeParameter()
    {
        $expected = 406;

        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/bonus/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'asdsadadsadasd',
        ]);

        $actual = $mockPayrollController->uploadBonusStatus($request);

        $this->assertEquals($expected, $actual->getStatusCode());
    }

    public function testUploadCommissionStatus()
    {
        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/commission/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $mockPayrollController->uploadCommissionStatus($request);

        $request = Request::create('/payroll/upload/commission/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $mockPayrollController->uploadCommissionStatus($request);

        $request = Request::create('/payroll/upload/commission/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $mockPayrollController->uploadCommissionStatus($request);

        $request = Request::create('/payroll/upload/commission/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $mockPayrollController->uploadCommissionStatus($request);
    }

    public function testUploadCommissionStatusInvalidIncludeParameter()
    {
        $expected = 406;

        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/commission/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'asdsadadsadasd',
        ]);

        $actual = $mockPayrollController->uploadCommissionStatus($request);

        $this->assertEquals($expected, $actual->getStatusCode());
    }

    public function testUploadDeductionStatus()
    {
        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/deduction/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
        ]);

        $mockPayrollController->uploadDeductionStatus($request);

        $request = Request::create('/payroll/upload/deduction/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results',
        ]);

        $mockPayrollController->uploadDeductionStatus($request);

        $request = Request::create('/payroll/upload/deduction/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'errors',
        ]);

        $mockPayrollController->uploadDeductionStatus($request);

        $request = Request::create('/payroll/upload/deduction/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'results,errors',
        ]);

        $mockPayrollController->uploadDeductionStatus($request);
    }

    public function testUploadDeductionStatusInvalidIncludeParameter()
    {
        $expected = 406;

        $mockPayrollController = $this->getInstance();

        $request = Request::create('/payroll/upload/deduction/status', 'GET', [
            'job_id' => 'test-job-id-from-jm-api',
            'include' => 'asdsadadsadasd',
        ]);

        $actual = $mockPayrollController->uploadDeductionStatus($request);

        $this->assertEquals($expected, $actual->getStatusCode());
    }

    public function testHandleUploadAttendance()
    {
        $mockPayrollController = $this->getInstance();

        Storage::fake('local');
        $file = UploadedFile::fake()->create('file.csv');

        $request = Request::create('/payroll/upload/attendance', 'POST', [
            'payroll_id' => 1], [], [
            'file' => $file
            ]);

        $request->attributes->add([
                'user' => [
                    'user_id' => 1,
                    'account_id' => 2
                ]
            ]);

        $mockPayrollController->uploadAttendance($request);
    }

    public function testCalculate()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockPayrollId = 1;
        $mockJobId = uniqid();
        $mockAttendanceJobId = uniqid();
        $mockAllowancesJobId = uniqid();
        $mockBonusesJobId = uniqid();
        $mockCommissionsJobId = uniqid();
        $mockDeductionsJobId = uniqid();

        $mockJobResponse = collect([
            'links' => [
                'self' => "/jobs/{$mockJobId}"
            ],
            'data' => [
                'type' => 'jobs',
                'id' => $mockJobId,
                'attributes' => [
                    'createdAt' => '1557121114',
                    'expiresAt' => '1564897114',
                    'payload' => [
                        'name' => 'compute-payroll',
                        'payrollId' => $mockPayrollId,
                        'attendanceJobId' => $mockAttendanceJobId,
                        'allowancesJobId' => $mockAllowancesJobId,
                        'bonusesJobId' => $mockBonusesJobId,
                        'commissionsJobId' => $mockCommissionsJobId,
                        'deductionsJobId' => $mockDeductionsJobId,
                        'jobId' => $mockJobId
                    ],
                    'status' => 'PENDING',
                    'tasksCount' => '-1',
                    'tasksDone' => '0',
                    'updatedAt' => 'null'
                ]
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 1,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'DRAFT',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockPayrollRequestService->shouldReceive('calculate')
            ->andReturn(new JsonResponse($mockJobResponse->toJson(), Response::HTTP_CREATED));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeCalculate')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/{$mockPayrollId}/calculate", 'POST');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $actual = $mockPayrollController->calculate($mockRequest, $mockPayrollId);
        $actualData = json_decode($actual->getContent(), true);

        $this->assertEquals(Response::HTTP_CREATED, $actual->getStatusCode());
        $this->assertEquals($mockJobResponse->toJson(), $actualData);
    }

    public function testCalculateDraftPayrollsOnly()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockPayrollId = 1;
        $mockJobId = uniqid();
        $mockAttendanceJobId = uniqid();
        $mockAllowancesJobId = uniqid();
        $mockBonusesJobId = uniqid();
        $mockCommissionsJobId = uniqid();
        $mockDeductionsJobId = uniqid();

        $mockJobResponse = collect([
            'links' => [
                'self' => "/jobs/{$mockJobId}"
            ],
            'data' => [
                'type' => 'jobs',
                'id' => $mockJobId,
                'attributes' => [
                    'createdAt' => '1557121114',
                    'expiresAt' => '1564897114',
                    'payload' => [
                        'name' => 'compute-payroll',
                        'payrollId' => $mockPayrollId,
                        'attendanceJobId' => $mockAttendanceJobId,
                        'allowancesJobId' => $mockAllowancesJobId,
                        'bonusesJobId' => $mockBonusesJobId,
                        'commissionsJobId' => $mockCommissionsJobId,
                        'deductionsJobId' => $mockDeductionsJobId,
                        'jobId' => $mockJobId
                    ],
                    'status' => 'PENDING',
                    'tasksCount' => '-1',
                    'tasksDone' => '0',
                    'updatedAt' => 'null'
                ]
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 1,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'OPEN',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockPayrollRequestService->shouldReceive('calculate')
            ->andReturn(new JsonResponse($mockJobResponse->toJson(), Response::HTTP_CREATED));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeCalculate')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/{$mockPayrollId}/calculate", 'POST');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $this->expectException(NotAcceptableHttpException::class);
        $mockPayrollController->calculate($mockRequest, $mockPayrollId);
    }

    public function testCalculateUnauthorized()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockPayrollId = 1;
        $mockJobId = uniqid();
        $mockAttendanceJobId = uniqid();
        $mockAllowancesJobId = uniqid();
        $mockBonusesJobId = uniqid();
        $mockCommissionsJobId = uniqid();
        $mockDeductionsJobId = uniqid();

        $mockJobResponse = collect([
            'links' => [
                'self' => "/jobs/{$mockJobId}"
            ],
            'data' => [
                'type' => 'jobs',
                'id' => $mockJobId,
                'attributes' => [
                    'createdAt' => '1557121114',
                    'expiresAt' => '1564897114',
                    'payload' => [
                        'name' => 'compute-payroll',
                        'payrollId' => $mockPayrollId,
                        'attendanceJobId' => $mockAttendanceJobId,
                        'allowancesJobId' => $mockAllowancesJobId,
                        'bonusesJobId' => $mockBonusesJobId,
                        'commissionsJobId' => $mockCommissionsJobId,
                        'deductionsJobId' => $mockDeductionsJobId,
                        'jobId' => $mockJobId
                    ],
                    'status' => 'PENDING',
                    'tasksCount' => '-1',
                    'tasksDone' => '0',
                    'updatedAt' => 'null'
                ]
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 1,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'OPEN',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockPayrollRequestService->shouldReceive('calculate')
            ->andReturn(new JsonResponse($mockJobResponse->toJson(), Response::HTTP_CREATED));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeCalculate')
            ->andReturn(false);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/{$mockPayrollId}/calculate", 'POST');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $this->expectException(HttpException::class);
        $mockPayrollController->calculate($mockRequest, $mockPayrollId);
    }

    public function testRecalculate()
    {
        $mockPayrollId = 1;
        $mockJobId = uniqid();
        $mockAttendanceJobId = uniqid();
        $mockAllowancesJobId = uniqid();
        $mockBonusesJobId = uniqid();
        $mockCommissionsJobId = uniqid();
        $mockDeductionsJobId = uniqid();

        $mockJobResponse = collect([
            'links' => [
                'self' => "/jobs/{$mockJobId}"
            ],
            'data' => [
                'type' => 'jobs',
                'id' => $mockJobId,
                'attributes' => [
                    'createdAt' => '1557121114',
                    'expiresAt' => '1564897114',
                    'payload' => [
                        'name' => 'compute-payroll',
                        'payrollId' => $mockPayrollId,
                        'attendanceJobId' => $mockAttendanceJobId,
                        'allowancesJobId' => $mockAllowancesJobId,
                        'bonusesJobId' => $mockBonusesJobId,
                        'commissionsJobId' => $mockCommissionsJobId,
                        'deductionsJobId' => $mockDeductionsJobId,
                        'jobId' => $mockJobId
                    ],
                    'status' => 'PENDING',
                    'tasksCount' => '-1',
                    'tasksDone' => '0',
                    'updatedAt' => 'null'
                ]
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 1,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'OPEN',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockPayrollRequestService->shouldReceive('calculate')
            ->andReturn(new JsonResponse($mockJobResponse->toJson(), Response::HTTP_CREATED));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeCalculate')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/{$mockPayrollId}/recalculate", 'POST');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $mockPayrollRequestService->shouldReceive('createCompanyPayrollStatistics')
            ->andReturn(new JsonResponse(collect([])->toJson()));

        $actual = $mockPayrollController->recalculate($mockRequest, $mockPayrollId);
        $actualData = json_decode($actual->getContent(), true);

        $this->assertEquals(Response::HTTP_CREATED, $actual->getStatusCode());
        $this->assertEquals($mockJobResponse->toJson(), $actualData);
    }

    public function testRecalculateOpenPayrollsOnly()
    {
        $mockPayrollId = 1;
        $mockJobId = uniqid();
        $mockAttendanceJobId = uniqid();
        $mockAllowancesJobId = uniqid();
        $mockBonusesJobId = uniqid();
        $mockCommissionsJobId = uniqid();
        $mockDeductionsJobId = uniqid();

        $mockJobResponse = collect([
            'links' => [
                'self' => "/jobs/{$mockJobId}"
            ],
            'data' => [
                'type' => 'jobs',
                'id' => $mockJobId,
                'attributes' => [
                    'createdAt' => '1557121114',
                    'expiresAt' => '1564897114',
                    'payload' => [
                        'name' => 'compute-payroll',
                        'payrollId' => $mockPayrollId,
                        'attendanceJobId' => $mockAttendanceJobId,
                        'allowancesJobId' => $mockAllowancesJobId,
                        'bonusesJobId' => $mockBonusesJobId,
                        'commissionsJobId' => $mockCommissionsJobId,
                        'deductionsJobId' => $mockDeductionsJobId,
                        'jobId' => $mockJobId
                    ],
                    'status' => 'PENDING',
                    'tasksCount' => '-1',
                    'tasksDone' => '0',
                    'updatedAt' => 'null'
                ]
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 1,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'DRAFT',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockPayrollRequestService->shouldReceive('calculate')
            ->andReturn(new JsonResponse($mockJobResponse->toJson(), Response::HTTP_CREATED));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeCalculate')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/{$mockPayrollId}/recalculate", 'POST');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $this->expectException(NotAcceptableHttpException::class);
        $mockPayrollController->recalculate($mockRequest, $mockPayrollId);
    }

    public function testDeleteUploadFailsWhenUnauthorized()
    {
        $mockPayrollId = 1;
        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 9999,
            'account_id' => 9999,
            'start_date' => '2019-03-26',
            'end_date' => '2019-04-25',
            'payroll_date' => '2019-04-30',
            'total_employees' => 2,
            'status' => 'OPEN',
            'gross' => 0,
            'net' => 0,
            'total_deductions' => 0,
            'total_contributions' => 0,
            'type' => 'regular',
            'selected_releases' => [],
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeEdit')
            ->andReturn(false);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/upload/attendance", 'DELETE', [], [], [], [], '{"payroll_id":1}');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $this->expectException(HttpException::class);
        $mockPayrollController->deleteUploadedPayrollAttendance($mockRequest);
    }

    public function testUserCanDeleteUploadedPayrollJobFileWhenPayrollIsDraft()
    {
        $mockPayrollId = 2;
        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'DRAFT',
        ]);

        $mockPayrollJobResponse = collect([
            'data' => [
                'affected_records' => 1
            ]
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()))
            ->shouldReceive('deleteUploadedJobToPayroll')
            ->andReturn(new Response($mockPayrollJobResponse->toJson()));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeEdit')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        //attendance
        $mockRequest = Request::create(
            "/payroll/upload/attendance",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":'.$mockPayrollId.'}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollAttendance($mockRequest);

        $this->assertEquals($response->getContent(), $mockPayrollJobResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);

        //Allowance
        $mockRequest = Request::create(
            "/payroll/upload/allowance",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":'.$mockPayrollId.'}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollAllowance($mockRequest);

        $this->assertEquals($response->getContent(), $mockPayrollJobResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);

        //Bonus
        $mockRequest = Request::create(
            "/payroll/upload/bonus",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":'.$mockPayrollId.'}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollBonus($mockRequest);

        $this->assertEquals($response->getContent(), $mockPayrollJobResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);

        //Commission
        $mockRequest = Request::create(
            "/payroll/upload/commission",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":'.$mockPayrollId.'}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollCommission($mockRequest);

        $this->assertEquals($response->getContent(), $mockPayrollJobResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);

        //Deduction
        $mockRequest = Request::create(
            "/payroll/upload/deduction",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":'.$mockPayrollId.'}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollDeduction($mockRequest);

        $this->assertEquals($response->getContent(), $mockPayrollJobResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);
    }

    public function testUserCannotDeleteUploadedPayrollJobFileWhenPayrollIsntSetAsDraft()
    {
        $mockPayrollId = 2;
        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_job_id' => 'd3842add-1e1f-42eb-a76e-b117c38be10b',
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'OPEN',
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($mockPayroll->toJson()))
            ->shouldReceive('deleteUploadedJobToPayroll')
            ->andReturn(new Response('Payroll is not draft', 406));
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeEdit')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        //attendance
        $mockRequest = Request::create(
            "/payroll/upload/attendance",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollAttendance($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //allowance
        $mockRequest = Request::create(
            "/payroll/upload/allowance",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollAllowance($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //bonus
        $mockRequest = Request::create(
            "/payroll/upload/bonus",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollBonus($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //commission
        $mockRequest = Request::create(
            "/payroll/upload/commission",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollCommission($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //deduction
        $mockRequest = Request::create(
            "/payroll/upload/deduction",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollDeduction($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //allowance
        $mockRequest = Request::create(
            "/payroll/upload/allowance",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollAllowance($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //bonus
        $mockRequest = Request::create(
            "/payroll/upload/bonus",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollBonus($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //commission
        $mockRequest = Request::create(
            "/payroll/upload/commission",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollCommission($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);

        //deduction
        $mockRequest = Request::create(
            "/payroll/upload/deduction",
            'DELETE',
            [],
            [],
            [],
            [],
            '{"payroll_id":' . $mockPayrollId . '}'
        );
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2,
            ]
        ]);

        $response = $mockPayrollController->deleteUploadedPayrollDeduction($mockRequest);

        $this->assertEquals($response->getContent(), 'Payroll is not draft');
        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_ACCEPTABLE);
    }

    private function uploadRequestStub($type)
    {
        $type = strtolower($type);
        $file = UploadedFile::fake()->create("file-{$type}.csv");
        $request = Request::create("/payroll/upload/$type", 'POST', [
            'payroll_id' => 1], [], [
            'file' => $file
            ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        return $request;
    }

    public function testUserUploadsDifferentPayrollData()
    {
        $mockPayrollController = $this->getInstance();

        $requestAllowance = $mockPayrollController->uploadAllowance(
            $this->uploadRequestStub(PayrollUploadService::ALLOWANCE_UPLOAD),
            PayrollUploadService::ALLOWANCE_UPLOAD
        );

        $requestBonus = $mockPayrollController->uploadBonus(
            $this->uploadRequestStub(PayrollUploadService::BONUS_UPLOAD),
            PayrollUploadService::BONUS_UPLOAD
        );

        $requestCommission = $mockPayrollController->uploadCommission(
            $this->uploadRequestStub(PayrollUploadService::COMMISSION_UPLOAD),
            PayrollUploadService::COMMISSION_UPLOAD
        );

        $requestDeduction = $mockPayrollController->uploadDeduction(
            $this->uploadRequestStub(PayrollUploadService::DEDUCTION_UPLOAD),
            PayrollUploadService::DEDUCTION_UPLOAD
        );

        $this->assertEquals(Response::HTTP_CREATED, $requestAllowance->getStatusCode());
        $this->assertEquals(Response::HTTP_CREATED, $requestBonus->getStatusCode());
        $this->assertEquals(Response::HTTP_CREATED, $requestCommission->getStatusCode());
        $this->assertEquals(Response::HTTP_CREATED, $requestDeduction->getStatusCode());
    }

    public function testOpenPayrollUnauthorized()
    {
        $mockPayrollId = 1;
        $request = Request::create("/payroll/$mockPayrollId", 'POST');
        $request->attributes->add([
            'user' => [
                'user_id' => 9999999,
                'account_id' => 999999
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_group_id' => 1,
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'DRAFT',
        ]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollService->shouldreceive([
            'get' => new JsonResponse($mockPayroll->toJson())
        ]);

        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive(['authorizeOpen' => false]);

        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);

        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);

        $this->expectException(HttpException::class);

        $payrollController = App::make(PayrollController::class);
        $payrollController->open($mockPayrollId, $request);
    }

    public function testOpenPayroll()
    {
        $mockPayrollId = 1;
        $request = Request::create("/payroll/$mockPayrollId", 'POST');
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_group_id' => 1,
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'DRAFT',
        ]);

        $mockPayrollResponse = collect([
            'id' => $mockPayrollId,
            'payroll_group_id' => 1,
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'DRAFT',
        ]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollService->shouldreceive([
            'get' => new JsonResponse($mockPayroll->toJson()),
            'open' => new JsonResponse($mockPayrollResponse->toJson())
        ]);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive(['authorizeOpen' => true]);

        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);

        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);

        $payrollController = App::make(PayrollController::class);
        $response = $payrollController->open($mockPayrollId, $request);

        $this->assertEquals($response->getOriginalContent(), $mockPayrollResponse->toJson());
        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);
    }

    public function testGetEmployeeActiveDisbursementSettings()
    {
        $mockPayrollId = 1;
        $mockEmployeesDisbursementMethod = collect([
            1 => [
                'id' => 2,
                'disbursementMethod' => 'Bank Account',
                'bankName' => 'BANCO DE ORO',
                'accountNumber' => '2321341242 (Savings)'
            ],
            2 => [
                'id' => 3,
                'disbursementMethod' => 'Bank Account',
                'bankName' => 'UNIONBANK',
                'accountNumber' => '2343241243 (Savings)'
            ],
            3 => [
                'id' => 4,
                'disbursementMethod' => 'Cash',
                'bankName' => null,
                'accountNumber' => null
            ],
        ]);
        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'OPEN',
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive([
            'get' => new JsonResponse($mockPayroll->toJson()),
            'getEmployeeActiveDisbursementMethod' =>
                new JsonResponse($mockEmployeesDisbursementMethod->toJson(), Response::HTTP_OK)
        ]);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/$mockPayrollId/employee_disbursement_method", 'GET');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);
        $result = $mockPayrollController->getEmployeeDisbursementMethod($mockPayrollId, $mockRequest);

        $this->assertEquals($result->getOriginalContent(), $mockEmployeesDisbursementMethod->toJson());
        $this->assertEquals($result->getStatusCode(), Response::HTTP_OK);
    }

    public function testGetDisbursementSummary()
    {
        $mockPayrollId = 1;
        $mockDisbursementSummary = collect([
            "data" => [
                [
                    "type" => "disbursement-method",
                    "name" => "Cash",
                    "attributes" => [
                        "count" => 1,
                        "employees" => [3],
                        "amount" => 500.63
                    ]
                ],
                [
                    "type" => "disbursement-method",
                    "name" => "UNIONBANK",
                    "attributes" => [
                        "count" => 1,
                        "employees" => [1],
                        "amount" => 2506.43
                    ]
                ],
                [
                    "type" => "disbursement-method",
                    "name" => "BANCO DE ORO",
                    "attributes" => [
                        "count" => 1,
                        "employees" => [2],
                        "amount" => 1200
                    ]
                ]
            ]
        ]);
        $mockPayroll = collect([
            'id' => $mockPayrollId,
            'payroll_group_id' => 1,
            'payroll_group_name' => 'Monthly PG1',
            'company_id' => 1,
            'account_id' => 2,
            'status' => 'OPEN',
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive([
            'get' => new JsonResponse($mockPayroll->toJson()),
            'getDisbursementSummary' =>
                new JsonResponse($mockDisbursementSummary->toJson(), Response::HTTP_OK)
        ]);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create("/payroll/$mockPayrollId/employee_disbursement_method", 'GET');
        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);
        $result = $mockPayrollController->getDisbursementSummary($mockRequest, $mockPayrollId);

        $this->assertEquals($result->getOriginalContent(), $mockDisbursementSummary->toJson());
        $this->assertEquals($result->getStatusCode(), Response::HTTP_OK);
        $this->assertEquals(
            json_decode($result->getOriginalContent(), true)['data'][0]['type'],
            'disbursement-method'
        );
    }

    public function testDeleteMultiplePayroll()
    {
        $payrollId = 1;
        $mockResponse = [
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 1,
                    "start_date" => "2019-01-26",
                    "end_date" => "2019-02-25",
                    "posting_date" => "2019-02-28",
                    "payroll_date" => "2019-02-28",
                    "total_employees" => 2,
                    "status" => "DRAFT",
                    "gross" => "0.0000",
                    "net" => "0.0000",
                    "total_deductions" => "0.0000",
                    "total_contributions" => "0.0000",
                    "payslips_sent_at" => null,
                    "payroll_type" => "regular"
                ]
            ]
        ];

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService->shouldReceive([
            'get' => new JsonResponse(collect($mockResponse['data'][0])->toJson(), Response::HTTP_OK),
            'deleteMany' => new JsonResponse(collect($mockResponse)->toJson(), Response::HTTP_OK),
        ]);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive([
            'queue' => true
        ]);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeDelete')
            ->andReturn(true);
        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockFinalPayAuthorizationService = Mockery::mock(FinalPayAuthorizationService::class);
        $mockFinalPayService = Mockery::mock(FinalPayRequestService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $mockPayrollController = new PayrollController(
            $mockPayrollRequestService,
            $mockPayrollGroupRequestService,
            $mockPayrollAuthorizationService,
            $mockAuditService,
            $mockJobsRequestService,
            $mockPayrollUploadService,
            $mockFinalPayAuthorizationService,
            $mockFinalPayService
        );

        $mockRequest = Request::create(
            "/payrolls",
            'DELETE',
            ['id' => [$payrollId]],
            [],
            [],
            ['Content-Type' => 'application/x-www-form-urlencoded']
        );

        $mockRequest->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $result = $mockPayrollController->deleteMany($mockRequest);

        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
    }

    public function testGetPayrollJobByNameOk()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $payrollId = 1;
        $mockResponse = [
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 1,
                    "start_date" => "2019-01-26",
                    "end_date" => "2019-02-25",
                    "posting_date" => "2019-02-28",
                    "payroll_date" => "2019-02-28",
                    "total_employees" => 2,
                    "status" => "DRAFT",
                    "gross" => "0.0000",
                    "net" => "0.0000",
                    "total_deductions" => "0.0000",
                    "total_contributions" => "0.0000",
                    "payslips_sent_at" => null,
                    "payroll_type" => "regular"
                ]
            ]
        ];

        $mockJobResponse = [
            "data" => [
                "type" => "payroll-jobs",
                "id" => "test",
                "attributes" => [],
            ]
        ];

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with(1)
            ->andReturn(new JsonResponse(collect($mockResponse['data'][0])->toJson()));
        $mockPayrollRequestService
            ->shouldReceive('getPayrollJobDetail')
            ->with(1, 'payslip')
            ->andReturn(new JsonResponse(collect($mockJobResponse)->toJson()));

        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);
        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);
        //$this->app->instance(AuditService::class, $mockAuditService);
        $request = Request::create(
            "/payroll/1/job/payslip",
            'GET',
            [],
            [],
            [],
            []
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);

        $this->assertEquals(
            collect(json_decode($this->response->getContent()))->toJson(),
            collect($mockJobResponse)->toJson()
        );
    }

    public function testCreatePayrollRegister()
    {
        $payrollId = 1;
        $mockPayrollResponse = collect([
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "OPEN"
                ]
            ]
        ]);

        $mockRegisterResponse = collect([
            'data' => [
                "type" => 'payroll-register',
                "id" => "1",
                "attributes" => [
                    "file" =>
                        "account_1/company_1/payroll_register/1" .
                        "/payroll-register-1562648108-LXt3gZEqIWAp21zZCNa6LnMc1.csv",
                    "payroll_id" => $payrollId,
                    "hash" => "gRSNtMfsLrksSI0lrnGsxBzMLvWC61lRZDxnmlFN"
                ]
            ]
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('createPayrollRegister')
            ->with($payrollId)
            ->andReturn(new JsonResponse($mockRegisterResponse->toJson(), Response::HTTP_CREATED));
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId)
            ->andReturn(new JsonResponse($mockPayrollResponse->toJson()));


        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $request = Request::create(
            "/payroll_register/$payrollId",
            'POST',
            [],
            [],
            [],
            $this->transformHeadersToServerVars([
                "X-Authz-Entities" => "payroll.payroll_summary"
            ])
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);

        $this->assertEquals(
            $this->response->getContent(),
            $mockRegisterResponse->toJson()
        );

        $this->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testCreateMultiplePayrollRegister()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $payrollId = [1, 2];
        $mockPayrollResponse = [
            collect([
                "data" => [
                    "id" => $payrollId[0],
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "OPEN"
                ]
            ]),
            collect([
                "data" => [
                    "id" => $payrollId[1],
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "CLOSED"
                ]
            ])
        ];

        $mockRegisterResponse = collect([
            'data' => [
                "type" => 'payroll-register',
                "id" => "1",
                "attributes" => [
                    "file" =>
                        "account_1/company_1/payroll_register/1" .
                        "/payroll-register-1562648108-LXt3gZEqIWAp21zZCNa6LnMc1.csv",
                    "payroll_id" => "JSDBBVSLKSFBS",
                    "hash" => "gRSNtMfsLrksSI0lrnGsxBzMLvWC61lRZDxnmlFN"
                ]
            ]
        ]);

        $mockUserRequestService = Mockery::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive(
            [
                'getUserInformations' =>
                new JsonResponse(
                    collect(['id' => 1, 'first_name' => 'Jam', 'email' => 'test@example.com'])->toJson()
                )
            ]
        );

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('createMultiplePayrollRegisters')
            ->andReturn(new JsonResponse($mockRegisterResponse->toJson(), Response::HTTP_CREATED));
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId[0])
            ->andReturn(new JsonResponse($mockPayrollResponse[0]->toJson()));
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId[1])
            ->andReturn(new JsonResponse($mockPayrollResponse[1]->toJson()));


        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->twice()
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);
        $this->app->instance(UserRequestService::class, $mockUserRequestService);

        $request = Request::create(
            "/payroll_register/multiple",
            'POST',
            ['id' => [1,2]],
            [],
            [],
            ['Content-type' => 'application/x-www-form-urlencode', 'Accept' => 'application/json']
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);

        $this->assertEquals(
            $this->response->getContent(),
            $mockRegisterResponse->toJson()
        );

        $this->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testCheckIfPayrollHasPayrollRegisterOK()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $payrollId = 1;
        $mockPayrollResponse = collect([
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "OPEN"
                ]
            ]
        ]);

        $mockRegisterResponse = collect([
            'data' => [
                "type" => 'payroll-register',
                "id" => "1",
                "attributes" => [
                    "file" =>
                        "account_1/company_1/payroll_register/1" .
                        "/payroll-register-1562648108-LXt3gZEqIWAp21zZCNa6LnMc1.csv",
                    "payroll_id" => $payrollId,
                    "hash" => "gRSNtMfsLrksSI0lrnGsxBzMLvWC61lRZDxnmlFN"
                ]
            ]
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('getPayrollRegister')
            ->with($payrollId)
            ->andReturn(new JsonResponse([$mockRegisterResponse->toJson()], Response::HTTP_OK));
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId)
            ->andReturn(new JsonResponse($mockPayrollResponse->toJson()));


        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $request = Request::create(
            "/payroll_register/$payrollId/has_payroll_register",
            'GET'
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);

        $this->assertEquals(
            $this->response->getContent(),
            '{"has_payroll_register":true}'
        );
    }

    public function testCheckIfPayrollHasPayrollRegisterNotFound()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $payrollId = 1;
        $mockPayrollResponse = collect([
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "OPEN"
                ]
            ]
        ]);

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('getPayrollRegister')
            ->with($payrollId)
            ->andReturn(new JsonResponse("[]", Response::HTTP_NOT_FOUND));
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId)
            ->andReturn(new JsonResponse($mockPayrollResponse->toJson()));


        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $request = Request::create(
            "/payroll_register/$payrollId/has_payroll_register",
            'GET'
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);


        $this->assertEquals(
            $this->response->getContent(),
            '{"has_payroll_register":false}'
        );
    }

    public function testSendSinglePayslip()
    {
        $this->markTestSkipped('Will deprecate this test soon');
        $payrollId = 1;
        $mockPayrollResponse = collect([
            "data" => [
                [
                    "id" => $payrollId,
                    "payroll_group_id" => 1,
                    "company_id" => 1,
                    "account_id" => 2,
                    "status" => "CLOSED"
                ]
            ]
        ]);
        $employeeId = 1021;

        $mockPayrollRequestService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollRequestService
            ->shouldReceive('get')
            ->with($payrollId)
            ->andReturn(new JsonResponse($mockPayrollResponse->toJson()));
        $mockPayrollRequestService
            ->shouldReceive('sendSinglePayslip')
            ->with($payrollId, $employeeId)
            ->andReturn(new JsonResponse($mockPayrollResponse->toJson()));


        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        $mockPayrollAuthorizationService->shouldReceive('authorizeSendPayslips')
            ->andReturn(true);

        $mockPayrollGroupRequestService = Mockery::mock(PayrollGroupRequestService::class);
        $mockJobsRequestService = Mockery::mock(JobsRequestService::class);
        $mockPayrollUploadService = Mockery::mock(PayrollUploadService::class);
        $mockAuditService = Mockery::mock(AuditService::class);

        $this->app->instance(PayrollRequestService::class, $mockPayrollRequestService);
        $this->app->instance(PayrollGroupRequestService::class, $mockPayrollGroupRequestService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(AuditService::class, $mockAuditService);
        $this->app->instance(JobsRequestService::class, $mockJobsRequestService);
        $this->app->instance(PayrollUploadService::class, $mockPayrollUploadService);

        $request = Request::create(
            "/payroll/$payrollId/send_single_payslip",
            'POST',
            compact('employeeId')
        );

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $this->handle($request);

        $this->assertEquals($mockPayrollResponse->toJson(), $this->response->getContent());
    }
}
