<?php

namespace Tests\Http\Controllers;

use Tests\TestCase;
use Mockery;
use App\Payroll\PayrollRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Payroll\PayrollService;
use Illuminate\Http\Request;
use App\Allowance\AllowanceAuthorizationService;
use App\Bonus\BonusAuthorizationService;
use App\Commission\CommissionAuthorizationService;
use App\Deduction\DeductionAuthorizationService;
use App\Adjustment\AdjustmentAuthorizationService;
use App\OtherIncome\OtherIncomeAuthorizationServiceFactory;
use App\Http\Controllers\OtherIncomeController;
use App\CSV\CsvValidator;
use App\OtherIncome\OtherIncomeAuditService;
use App\Storage\OtherIncomeUploadService;

/**
 * @SuppressWarnings(PHPMD.Generic.Files.LineLength.TooLong)
 */

class OtherIncomeControllerTest extends TestCase
{
    public function testGetUploadStatusVE110()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockAuditService = Mockery::mock(OtherIncomeAuditService::class);
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(OtherIncomeUploadService::class);
        
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-110",
                                            "name" => "Missing Headers",
                                            "details" => [
                                                [
                                                    "parameters" => [
                                                        "Prorated?",
                                                        "Recurring?"
                                                    ]
                                                ]
                                            ]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AllowanceAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AllowanceAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(OtherIncomeRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );
        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\OtherIncomeController[getCompanyAccountId]',
            array($mockRequestService, $mockAuditService, $mockValidator, $mockUploadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        //$this->app->instance(OtherIncomeRequestService::class, $mockRequestService);
        $this->app->instance(OtherIncomeController::class, $partialMockedController);
        // $controller = new OtherIncomeController(
        //     $mockRequestService,
        //     $mockAuditService,
        //     $mockValidator,
        //     $mockUploadService
        // );
        
        $request = Request::create("/company/123/other_income/allowance/upload/status", 'GET', [
            'job_id' => 'foo',
        ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Prorated? header is required.",
                        "Recurring? header is required."
                    ]
                ]
            ]
        );
    }


    public function testGetUploadStatusVE101()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockAuditService = Mockery::mock(OtherIncomeAuditService::class);
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(OtherIncomeUploadService::class);
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-101",
                                            "name" => "Missing Headers",
                                            "details" => [
                                                [
                                                    "parameters" => [
                                                        "Some Date",
                                                    ],
                                                    "expectedFormat" => "MM/DD/YYYY"
                                                ]
                                            ]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AllowanceAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AllowanceAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(OtherIncomeRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\OtherIncomeController[getCompanyAccountId]',
            array($mockRequestService, $mockAuditService, $mockValidator, $mockUploadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        //$this->app->instance(OtherIncomeRequestService::class, $mockRequestService);
        $this->app->instance(OtherIncomeController::class, $partialMockedController);
        $request = Request::create("/company/123/other_income/allowance/upload/status", 'GET', [
            'job_id' => 'foo',
        ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Invalid value for field Some Date. Date must be in MM/DD/YYYY format."
                    ]
                ]
            ]
        );
    }

    public function testGetUploadStatusVE102()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockAuditService = Mockery::mock(OtherIncomeAuditService::class);
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(OtherIncomeUploadService::class);
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-102",
                                            "name" => "Invalid Value",
                                            "details" => [
                                                [
                                                    "parameters" => [
                                                        "Some Param",
                                                    ],
                                                    "expectedValue" => "1/2/3/4"
                                                ]
                                            ]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AllowanceAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AllowanceAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(OtherIncomeRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\OtherIncomeController[getCompanyAccountId]',
            array($mockRequestService, $mockAuditService, $mockValidator, $mockUploadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        //$this->app->instance(OtherIncomeRequestService::class, $mockRequestService);
        $this->app->instance(OtherIncomeController::class, $partialMockedController);
        $request = Request::create("/company/123/other_income/allowance/upload/status", 'GET', [
            'job_id' => 'foo',
        ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Invalid value for field Some Param. Possible values are 1, 2, 3 or 4."
                    ]
                ]
            ]
        );
    }


    public function testGetUploadStatusVE203()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockAuditService = Mockery::mock(OtherIncomeAuditService::class);
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(OtherIncomeUploadService::class);
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-203",
                                            "name" => "Processing Error",
                                            "details" => ["Some processing error"]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AllowanceAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AllowanceAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(OtherIncomeRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\OtherIncomeController[getCompanyAccountId]',
            array($mockRequestService, $mockAuditService, $mockValidator, $mockUploadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        //$this->app->instance(OtherIncomeRequestService::class, $mockRequestService);
        $this->app->instance(OtherIncomeController::class, $partialMockedController);
        $request = Request::create("/company/123/other_income/allowance/upload/status", 'GET', [
            'job_id' => 'foo',
        ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Some processing error"
                    ]
                ]
            ]
        );
    }


    public function testGetUploadStatusVE102B()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $mockAuditService = Mockery::mock(OtherIncomeAuditService::class);
        $mockValidator = Mockery::mock(CsvValidator::class);
        $mockUploadService = Mockery::mock(OtherIncomeUploadService::class);
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-102",
                                            "name" => "Invalid Value",
                                            "details" => [[
                                                "parameters" => ["Amount"],
                                                "expectedFormat" => "number",
                                                "expectedValue" => [
                                                    "min" => 1
                                                ]
                                            ]]
                                        ],
                                        [
                                            "code" => "VE-102",
                                            "name" => "Invalid Value",
                                            "details" => [[
                                                "parameters" => ["Amount"],
                                                "expectedFormat" => "number"
                                            ]]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ],
                
            ]
        ];
        //setup mocks::
        $mockAuthorizationService = Mockery::mock(AllowanceAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeCreate')->andReturn(true);

        $this->app->instance(AllowanceAuthorizationService::class, $mockAuthorizationService);

        $mockRequestService = Mockery::mock(OtherIncomeRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')
            ->andReturn(
                response()->json(
                    collect($mockData)->toJson()
                )
            );

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\OtherIncomeController[getCompanyAccountId]',
            array($mockRequestService, $mockAuditService, $mockValidator, $mockUploadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        //$this->app->instance(OtherIncomeRequestService::class, $mockRequestService);
        $this->app->instance(OtherIncomeController::class, $partialMockedController);
        $request = Request::create("/company/123/other_income/allowance/upload/status", 'GET', [
            'job_id' => 'foo',
        ]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);

        $response = $this->handle($request);
        $response->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Invalid value for field Amount. " .
                        "This field can only accept numeric values greater than or equal to 1.",
                        "Invalid value for field Amount. This field can only accept numeric values."
                    ]
                ]
            ]
        );
    }
}
