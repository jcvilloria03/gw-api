<?php

namespace Tests\Http\Controllers;

use Mockery as M;
use Tests\TestCase;
use Illuminate\Http\Request;
use App\Http\Controllers\EssScheduleController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Schedule\ScheduleRequestService;
use Illuminate\Http\JsonResponse;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\DefaultSchedule\EssDefaultScheduleAuthorizationService;

class EssScheduleControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->markTestSkipped('will deprecate soon');
    }

    public function testGetCompaniesDefaultScheduleReturn401()
    {
        $scheduleRequestService = $this->app->make(ScheduleRequestService::class);
        $employeeRequestAuthorizationService = $this->app->make(
            EssEmployeeRequestAuthorizationService::class
        );
        
        $defaultScheduleRequestService = M::mock(DefaultScheduleRequestService::class);
        $defaultScheduleRequestService->shouldReceive('index')
            ->never();

        $controller = new EssScheduleController(
            $scheduleRequestService,
            $employeeRequestAuthorizationService,
            $defaultScheduleRequestService
        );

        $request = Request::create(
            '/test/ess/company/1/default_schedule',
            'GET'
        );

        $userData = [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 2,
            'account_id'          => 1,
        ];


        $request->attributes->set('companies_users', [$userData]);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Unauthorized');
        $controller->getCompaniesDefaultSchedule(
            1,
            $request,
            $this->app->make(EssDefaultScheduleAuthorizationService::class)
        );
    }

    public function testGetCompaniesDefaultScheduleReturn200()
    {
        $scheduleRequestService = $this->app->make(ScheduleRequestService::class);
        $employeeRequestAuthorizationService = $this->app->make(
            EssEmployeeRequestAuthorizationService::class
        );
        
        $defaultScheduleRequestService = M::mock(DefaultScheduleRequestService::class);
        $defaultScheduleRequestService->shouldReceive('index')
            ->with(1)
            ->andReturn(new JsonResponse(json_encode([
                'data' => []
            ])));

        $controller = new EssScheduleController(
            $scheduleRequestService,
            $employeeRequestAuthorizationService,
            $defaultScheduleRequestService
        );

        $request = Request::create(
            '/test/ess/company/1/default_schedule',
            'GET'
        );

        $userData = [
            'user_id'             => 1,
            'employee_id'         => 1,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ];


        $request->attributes->set('companies_users', [$userData]);

        $essDefaultScheduleAuthorizationService = M::mock(EssDefaultScheduleAuthorizationService::class);
        $essDefaultScheduleAuthorizationService->shouldReceive('authorizeView')
            ->with(1)
            ->andReturn(true);

        $response = $controller->getCompaniesDefaultSchedule(
            1,
            $request,
            $essDefaultScheduleAuthorizationService
        );

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());
    }
}
