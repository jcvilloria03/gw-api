<?php

namespace Tests\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Company\CompanyRequestService;
use App\CostCenter\CostCenterAuthorizationService;
use App\CostCenter\CostCenterEsIndexQueueService;
use App\CostCenter\CostCenterRequestService;
use App\Http\Controllers\CostCenterController;
use Mockery;
use App\Facades\Company;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;

class CostCenterControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function getInstance()
    {
        $getCostCenterData = collect([
            'id' => 1,
            'name' => 'test_name',
            'company_id' => 1,
            'account_id' => 1,
            'description' => 'test_description',
        ]);

        $updateCostCenterData = collect([
            'id' => 1,
            'name' => 'new_test_name',
            'company_id' => 1,
            'account_id' => 1,
            'description' => 'new_test_description'
        ]);

        $deleteCostCenterData = collect([
        ]);

        $mockRequestService = Mockery::mock(CostCenterRequestService::class);
        $mockRequestService->shouldReceive('update')
            ->andReturn(new JsonResponse($updateCostCenterData->toJson()));
        $mockRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse($getCostCenterData->toJson()));
        $mockRequestService->shouldReceive('bulkDelete')
            ->andReturn(new JsonResponse($deleteCostCenterData->toJson(), 204));
        $mockAuthorizationService = Mockery::mock(CostCenterAuthorizationService::class);
        $mockAuthorizationService->shouldReceive('authorizeDelete')
            ->andReturn(true);
        $mockAuthorizationService->shouldReceive('authorizeUpdate')
            ->andReturn(true);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockIndexQueueService = Mockery::mock(CostCenterEsIndexQueueService::class);

        return new CostCenterController(
            $mockRequestService,
            $mockAuthorizationService,
            $mockAuditService,
            $mockIndexQueueService
        );
    }

    public function testDeleteCostCenter()
    {
        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')->andReturn(1);

        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);
        $mockCostCenterController = $this->getInstance();

        $costCenterIds = [1, 2];
        $request = Request::create("/cost_center/bulk_delete", 'DELETE', [
            'company_id' => 1,
            'cost_center_ids' => $costCenterIds
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 1
            ]
        ]);
        $result = $mockCostCenterController->bulkDelete($request);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $result->getStatusCode());
    }

    public function testUpdateCostCenter()
    {
        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')->andReturn(1);

        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);

        $mockCostCenterController = $this->getInstance();

        $costCenterId = 1;
        $request = Request::create("/cost_center/{$costCenterId}", 'PUT', [
            'name' => 'new_test_name',
            'description' => 'new_test_description',
            'company_id' => 1
        ]);

        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 1
            ]
        ]);

        $result = $mockCostCenterController->update($request, $costCenterId);
        $response = json_decode($result->getData(), true);
        $this->assertEquals('new_test_name', $response['name']);
        $this->assertEquals('new_test_description', $response['description']);
    }
}
