<?php

namespace Tests\Http\Controllers;

use Tests\TestCase;
use Mockery;
use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Payroll\PayrollRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Payroll\PayrollService;
use Illuminate\Http\Request;
use App\PayrollLoan\PayrollLoanAuthorizationService;
use App\Http\Controllers\PayrollLoanController;
use App\Storage\UploadService;
use Illuminate\Http\UploadedFile;
use App\PayrollLoan\PayrollLoanRequestService;
use App\PayrollLoan\PayrollLoanDownloadRequestService;
use App\GovernmentForm\GovernmentFormAuthorizationService;
use App\Audit\AuditService;
use App\CSV\CsvValidator;
use Illuminate\Support\Facades\Storage;

/**
 * @SuppressWarnings(PHPMD.Generic.Files.LineLength.TooLong)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PayrollLoanControllerTest extends TestCase
{

    public function testUploadOk()
    {
        $this->markTestSkipped('Old RBAC Implementation. will deprecate soon');
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
        ];

        $mockAuthService = Mockery::mock(PayrollLoanAuthorizationService::class);
        $mockAuthService->shouldReceive('authorizeCreate')->andReturn(true);

        $mockUploadService = Mockery::mock(UploadService::class);
        $mockUploadService->shouldReceive('saveFile')->andReturn('somekey');
        $mockUploadService->shouldReceive('getS3Bucket')->andReturn('somebucket');

        $mockRequestService = Mockery::mock(PayrollLoanRequestService::class);
        $mockRequestService->shouldReceive('processFileUpload')->andReturn(
            response()->json(
                collect($mockData)->toJson()
            )
        );
        $mockCsvValidator = Mockery::mock(CsvValidator::class);
        $downloadService = Mockery::mock(PayrollLoanDownloadRequestService::class);
        $govAuthService = Mockery::mock(GovernmentFormAuthorizationService::class);

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\PayrollLoanController[getCompanyAccountId]',
            array($mockRequestService, $mockAuthService, $govAuthService, $mockCsvValidator, $downloadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        $this->app->instance(PayrollLoanController::class, $partialMockedController);
        $this->app->instance(UploadService::class, $mockUploadService);

        $mockFile = UploadedFile::fake()->create('test.csv');
        $mockFile->mimeType ="text/csv";

        $request = Request::create("/payroll_loan/upload", 'POST', ['company_id' => 123], [], ['file'=>$mockFile]);
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);
        $this->handle($request)
        ->shouldReturnJson(
            [
                'job_id' => 'foo'
            ]
        );
    }

    public function testUploadStatusVE110()
    {
        $mockData = [
            "data" => [
                "id" => "foo",
                "type" => "jobs",
                "attributes" => [
                    "payload" => [
                    ],
                    "status" => "FINISHED"
                ]
            ],
            "included" => [
                [
                    "id" => "foo1",
                    "type" => "job-errors",
                    "attributes" => [
                        "description" => [
                            "name" => "validation-errors",
                            "errors" => [
                                "1" => [
                                        [
                                            "code" => "VE-110",
                                            "name" => "Missing Headers",
                                            "details" => [
                                                [
                                                    "parameters" => [
                                                        "Amount",
                                                        "Monthly Amortization"
                                                    ]
                                                ]
                                            ]
                                        ]
                                ]
                            ]
                        ]
                    ]
                ]

            ]
        ];

        $mockUserService = Mockery::mock(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->app->instance(UserRequestService::class, $mockUserService);

        $mockAccountService = Mockery::mock(AccountRequestService::class, [
            'getAccountEssentialData' => response()->json(json_encode([
                1 => [
                    'department' => [],
                    'position' => [],
                    'team' => [],
                    'location' => [],
                    'payroll_group' => [],
                ]
            ]))
        ]);

        $this->app->instance(AccountRequestService::class, $mockAccountService);

        $mockAuthzService = Mockery::mock(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.loans' => [
                    'data_scope' => [
                        'COMPANY' => [123]
                    ]
                ]
            ]
        ]);

        $this->app->instance(AuthzRequestService::class, $mockAuthzService);

        $mockAuthService = Mockery::mock(PayrollLoanAuthorizationService::class);
        $mockAuthService->shouldReceive('authorizeCreate')->andReturn(true);

        $mockRequestService = Mockery::mock(PayrollLoanRequestService::class);
        $mockRequestService->shouldReceive('getFileUploadStatus')->andReturn(
            response()->json(
                collect($mockData)->toJson()
            )
        );
        $mockCsvValidator = Mockery::mock(CsvValidator::class);
        $downloadService = Mockery::mock(PayrollLoanDownloadRequestService::class);
        $govAuthService = Mockery::mock(GovernmentFormAuthorizationService::class);

        $partialMockedController = Mockery::mock(
            'App\Http\Controllers\PayrollLoanController[getCompanyAccountId]',
            array($mockRequestService, $mockAuthService, $govAuthService, $mockCsvValidator, $downloadService)
        );
        $partialMockedController->shouldReceive('getCompanyAccountId')->andReturn(123);
        $this->app->instance(PayrollLoanController::class, $partialMockedController);

        $request = Request::create(
            "/payroll_loan/upload/status",
            'GET',
            ['company_id'=>123,'job_id' => "foo"],
            [],
            [],
            $this->transformHeadersToServerVars([
                "X-Authz-Entities" => "employees.loans"
            ])
        );
        $request->attributes->add([
            'user' => [
                'user_id' => 1,
                'account_id' => 2
            ]
        ]);
        $this->handle($request)
        ->shouldReturnJson(
            [
                "status" => "validation_failed",
                "errors" => [
                    "1" => [
                        "Amount header is required.",
                        "Monthly Amortization header is required."
                    ]
                ]
            ]
        );
    }

}
