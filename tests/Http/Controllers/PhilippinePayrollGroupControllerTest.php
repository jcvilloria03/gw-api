<?php

namespace Tests\Http\Controllers;

use App\Http\Controllers\PhilippinePayrollGroupController;
use App\PayrollGroup\PayrollGroupEsIndexQueueService;
use App\PayrollGroup\PhilippinePayrollGroupRequestService;
use App\PayrollGroup\PayrollGroupAuthorizationService;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mockery;

class PhilippinePayrollGroupControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testUpdateValid()
    {
        $mockPayrollGroup = [
            'id' => 12
        ];

        $mockPhilippinePayrollGroupRequestService = Mockery::mock(PhilippinePayrollGroupRequestService::class);
        $mockPhilippinePayrollGroupRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup)));
        $mockPhilippinePayrollGroupRequestService->shouldReceive('update')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup), Response::HTTP_CREATED));
        $mockPayrollGroupAuthorizationService = Mockery::mock(PayrollGroupAuthorizationService::class);
        $mockPayrollGroupAuthorizationService->shouldReceive('authorizeUpdate')
            ->andReturn(true);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollGroupEsIndexQueueService = Mockery::mock(PayrollGroupEsIndexQueueService::class);

        $mockPhilippinePayrollGroupController = new PhilippinePayrollGroupController(
            $mockPhilippinePayrollGroupRequestService,
            $mockPayrollGroupAuthorizationService,
            $mockAuditService,
            $mockPayrollGroupEsIndexQueueService
        );

        $request = Request::create(
            "/philippine/payroll_group/{$mockPayrollGroup['id']}",
            'PATCH',
            [
                'name' => uniqid(),
            ]
        );

        $request->attributes->add([
            'decodedToken' => (object)['sub' => 1223],
            'user' => [
                'account_id' => 1,
                'user_id' => 1,
            ],
        ]);

        $actual = $mockPhilippinePayrollGroupController->update($request, $mockPayrollGroup['id']);

        $this->assertEquals(Response::HTTP_CREATED, $actual->getStatusCode());
    }

    public function testUpdateUnauthorized()
    {
        $mockPayrollGroup = [
            'id' => 12
        ];

        $mockPhilippinePayrollGroupRequestService = Mockery::mock(PhilippinePayrollGroupRequestService::class);
        $mockPhilippinePayrollGroupRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup)));
        $mockPhilippinePayrollGroupRequestService->shouldReceive('update')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup), Response::HTTP_UNAUTHORIZED));
        $mockPayrollGroupAuthorizationService = Mockery::mock(PayrollGroupAuthorizationService::class);
        $mockPayrollGroupAuthorizationService->shouldReceive('authorizeUpdate')
            ->andReturn(false);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollGroupEsIndexQueueService = Mockery::mock(PayrollGroupEsIndexQueueService::class);

        $mockPhilippinePayrollGroupController = new PhilippinePayrollGroupController(
            $mockPhilippinePayrollGroupRequestService,
            $mockPayrollGroupAuthorizationService,
            $mockAuditService,
            $mockPayrollGroupEsIndexQueueService
        );

        $request = Request::create(
            "/philippine/payroll_group/{$mockPayrollGroup['id']}",
            'PATCH',
            [
                'name' => uniqid(),
            ]
        );

        $request->attributes->add([
            'decodedToken' => (object)['sub' => 1223],
            'user' => [
                'account_id' => 1,
                'user_id' => 1,
            ],
        ]);

        $this->expectException(HttpException::class);
        $mockPhilippinePayrollGroupController->update($request, $mockPayrollGroup['id']);
    }

    public function testUpdateNotAcceptable()
    {
        $mockPayrollGroup = [
            'id' => 12
        ];

        $mockPhilippinePayrollGroupRequestService = Mockery::mock(PhilippinePayrollGroupRequestService::class);
        $mockPhilippinePayrollGroupRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup)));
        $mockPhilippinePayrollGroupRequestService->shouldReceive('update')
            ->andReturn(new JsonResponse(json_encode($mockPayrollGroup), Response::HTTP_NOT_ACCEPTABLE));
        $mockPayrollGroupAuthorizationService = Mockery::mock(PayrollGroupAuthorizationService::class);
        $mockPayrollGroupAuthorizationService->shouldReceive('authorizeUpdate')
            ->andReturn(false);
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('queue');
        $mockPayrollGroupEsIndexQueueService = Mockery::mock(PayrollGroupEsIndexQueueService::class);

        $mockPhilippinePayrollGroupController = new PhilippinePayrollGroupController(
            $mockPhilippinePayrollGroupRequestService,
            $mockPayrollGroupAuthorizationService,
            $mockAuditService,
            $mockPayrollGroupEsIndexQueueService
        );

        $request = Request::create(
            "/philippine/payroll_group/{$mockPayrollGroup['id']}",
            'PATCH',
            [
                'name' => uniqid(),
            ]
        );

        $request->attributes->add([
            'decodedToken' => (object)['sub' => 1223],
            'user' => [
                'account_id' => 1,
                'user_id' => 1,
            ],
        ]);

        $this->expectException(HttpException::class);
        $mockPhilippinePayrollGroupController->update($request, $mockPayrollGroup['id']);
    }
}
