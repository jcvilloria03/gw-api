<?php

namespace Tests\Http\Controllers;

use App\Bank\BankRequestService;
use App\Http\Controllers\BankController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;
use Mockery;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class BankControllerTest extends \Tests\TestCase
{
    public function getInstance()
    {
        $mockBanks = collect(["data" => [
            [
              "id" => 1,
              "type" => "bank",
              "attributes" => [
                "name" => "AL-AMANAH ISLAMIC INVESTMENT BANK OF THE PHILIPPINES"
                ]
            ],
            [
              "id" => 2,
              "type" => "bank",
              "attributes" => [
                "name" => "ALLIED BANKING CORPORATION"
              ]
            ]
        ]]);
        $mockRequestService = Mockery::mock(BankRequestService::class);
        $mockRequestService->shouldReceive('index')
            ->andReturn(new JsonResponse($mockBanks->toJson(), 200));

        return new BankController(
            $mockRequestService
        );
    }

    public function testIndex()
    {
        $expectedBanks = [
            "AL-AMANAH ISLAMIC INVESTMENT BANK OF THE PHILIPPINES",
            "ALLIED BANKING CORPORATION"
        ];
        $mockBankController = $this->getInstance();
        $result = $mockBankController->index();
        $response = json_decode($result->getData(), true);
        $actual = $response['data'];
        $actualBanks = [];
        foreach ($actual as $data) {
            $actualBanks[] = $data['attributes']['name'];
        }

        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $this->assertEquals($expectedBanks, $actualBanks);
    }
}

