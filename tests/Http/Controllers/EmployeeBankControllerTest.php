<?php

namespace Tests\Http\Controllers;

use Mockery;
use Illuminate\Http\Request;
use App\Bank\BankRequestService;
use Illuminate\Http\JsonResponse;
use App\Employee\EmployeeRequestService;
use App\Bank\EmployeeBankAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Company\PhilippineCompanyRequestService;
use App\Http\Controllers\EmployeeBankController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class EmployeeBankControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testStore()
    {
        $mockEmployeeId = 12;

        $expected = [
            'data' => [
                'type' => 'employee-payment-method',
                'id' => '1',
                'attributes' => [
                    'active' => false,
                    'bank' => [
                        'id' => '23',
                        'name' => 'test bank name',
                    ],
                    'bankAccount' => [
                        'accountNumber' => '23232323',
                        'accountType' => 'SAVINGS',
                    ],
                ],
            ],
        ];

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addEmployeeBankAccount')
        ->andReturn(new JsonResponse(collect($expected)->toJson(), 201));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeCreate')
            ->andReturn(true);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->andReturn(new JsonResponse(json_encode(['company_id' => 1])));

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode(['id' => 1, 'account_id' => 1])));

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $mockRequest = Request::create(
            "/employee/{$mockEmployeeId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([ // $content
                'data' => [
                    'type' => 'employee-payment-method',
                    'attributes' => [
                        'bank_id' => '33441',
                        'account_number' => '324323242342',
                        'account_type' => 'SAVINGS',
                    ],
                ],
            ])
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => null,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ]);

        $actualResponse = $mockEmployeeBankController->store($mockRequest, $mockEmployeeId);
        $actual = json_decode($actualResponse->getData(), true);

        $this->assertEquals(Response::HTTP_CREATED, $actualResponse->getStatusCode());
        $this->assertEquals($expected, $actual);
    }

    public function testIndex()
    {
        $mockEmployeeId = 12;

        $expected = [
            'data' => [
                [
                    'type' => 'employee-payment-method',
                    'id' => '4',
                    'attributes' => [
                        'disbursementMethod' => 'Bank Account',
                        'details' => 'AL-AMANAH ISLAMIC INVESTMENT BANK OF THE PHILIPPINES',
                        'accountNumber' => '122134 (Savings)',
                        'active' => false
                    ]
                ],
                [
                    'type' => 'employee-payment-method',
                    'id' => '5',
                    'attributes' => [
                        'disbursementMethod' => 'Bank Account',
                        'details' => 'BANCO DE ORO PRIVATE BANK INCORPORATED',
                        'accountNumber' => '122134 (Current)',
                        'active' => false
                    ]
                ],
                [
                    'type' => 'employee-payment-method',
                    'id' => '13',
                    'attributes' => [
                        'disbursementMethod' => 'Cash',
                        'details' => null,
                        'accountNumber' => null,
                        'active' => false
                    ]
                ],
                [
                    'type' => 'employee-payment-method',
                    'id' => '14',
                    'attributes' => [
                        'disbursementMethod' => 'Cheque',
                        'details' => null,
                        'accountNumber' => null,
                        'active' => false
                    ]
                ]
            ]
        ];

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('getEmployeePaymentMethods')
            ->andReturn(new JsonResponse(collect($expected)->toJson(), 200));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeGet')
            ->andReturn(true);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->andReturn(new JsonResponse(json_encode(['company_id' => 1])));

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode(['id' => 1, 'account_id' => 1])));

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $mockRequest = Request::create(
            "/employee/1/payment_methods",
            'GET'
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => null,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ]);

        $actualResponse = $mockEmployeeBankController->index($mockRequest, $mockEmployeeId);
        $actual = json_decode($actualResponse->getData(), true);

        $this->assertEquals(Response::HTTP_OK, $actualResponse->getStatusCode());
        $this->assertEquals($expected, $actual);
    }

    public function testStoreInvalidJsonBody()
    {
        $mockEmployeeId = 12;

        $expected = [
            'data' => [
                'type' => 'employee-payment-method',
                'id' => '1',
                'attributes' => [
                    'active' => false,
                    'bank' => [
                        'id' => '23',
                        'name' => 'test bank name',
                    ],
                    'bankAccount' => [
                        'accountNumber' => '23232323',
                        'accountType' => 'SAVINGS',
                    ],
                ],
            ],
        ];

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addEmployeeBankAccount')
        ->andReturn(new JsonResponse(collect($expected)->toJson(), 201));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeCreate')
            ->never();

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->never();

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->never();

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $mockRequest = Request::create(
            "/employee/{$mockEmployeeId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            '{data":{"type":"employee-payment-method","attributes":{"bank_id":159,' .
                '"account_number":"12121","account_type":"SAVINGS"}}}'
        );

        $this->expectException(NotAcceptableHttpException::class);
        $mockEmployeeBankController->store($mockRequest, $mockEmployeeId);
    }

    public function testStoreExceptionInRequestService()
    {
        $mockEmployeeId = 12;

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('addEmployeeBankAccount')
        ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeCreate')
            ->andReturn(true);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->andReturn(new JsonResponse(json_encode(['company_id' => 1])));

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode(['id' => 1, 'account_id' => 1])));

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $mockRequest = Request::create(
            "/employee/{$mockEmployeeId}/bank",
            'POST',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([ // $content
                'data' => [
                    'type' => 'employee-payment-method',
                    'attributes' => [
                        'bank_id' => '33441',
                        'account_number' => '324323242342',
                        'account_type' => 'SAVINGS',
                    ],
                ],
            ])
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => null,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ]);

        $this->expectException(HttpException::class);
        $mockEmployeeBankController->store($mockRequest, $mockEmployeeId);
    }

    public function testDestroy()
    {
        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('deleteEmployeePaymentMethod')
            ->andReturn(new JsonResponse(collect([])->toJson(), 204));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $actualResponse = $mockEmployeeBankController->destroy(12);

        $this->assertEquals(Response::HTTP_NO_CONTENT, $actualResponse->getStatusCode());
    }

    public function testActivatePaymentMethod()
    {
        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('activatePaymentMethod')
            ->andReturn(new JsonResponse(json_encode(["data" => []]), 200));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeUpdate')
            ->andReturn(true);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->andReturn(new JsonResponse(json_encode(['company_id' => 1])));

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode(['id' => 1, 'account_id' => 1])));

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $employeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $mockEmployeeId = 2;
        $mockPaymentId = 1234;

        $mockRequest = Request::create(
            "/employee/{$mockEmployeeId}/payment_methods/{$mockPaymentId}",
            'PATCH',
            [], // $parameters
            [], // $cookies
            [], // $files
            [ // $server
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode([ // $content
                'data' => [
                    'type' => 'employee-payment-method',
                    'attributes' => [
                        'active' => true,

                    ],
                ],
            ])
        );

        $mockRequest->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => null,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ]);

        $response = $employeeBankController->activatePaymentMethod($mockRequest, $mockEmployeeId, $mockPaymentId);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testDestroyExceptionInRequestService()
    {
        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('deleteEmployeePaymentMethod')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);

        $this->app->instance(BankRequestService::class, $mockBankRequestService);

        $mockEmployeeBankController = new EmployeeBankController(
            $authorizationService,
            $employeeRequestService,
            $companyRequestService
        );

        $this->expectException(HttpException::class);
        $mockEmployeeBankController->destroy(12);
    }

    public function testDestroyMultipleEntries()
    {
        $this->markTestSkipped('Old RBAC implementation, will deprecate soon');
        $employeeId = 155;
        $mockResponse = [
            [
                "id" => 5,
                "employee_id" => $employeeId,
                "payable_method_id" => 2
            ],
            [
                "id" => 6,
                "employee_id" => $employeeId,
                "payable_method_id" => 3
            ]
        ];

        $requestPayload = [
            "data" => [
                "type" => 'employee-payment-method',
                "id" => [
                    5,6
                ]
            ]
        ];

        $request = Request::create(
            "/employee/$employeeId/payment_method",
            "DELETE",
            [],
            [],
            [],
            ['Content-Type'=>'application/json'],
            collect($requestPayload)->toJson()
        );

        $request->attributes->set('user', [
            'user_id'             => 1,
            'employee_id'         => null,
            'employee_company_id' => 1,
            'account_id'          => 1,
        ]);

        $mockBankRequestService = Mockery::mock(BankRequestService::class);
        $mockBankRequestService->shouldReceive('deleteEmployeePaymentMethods')
            ->andReturn(new JsonResponse(collect($mockResponse)->toJson(), 200));

        $authorizationService = Mockery::mock(EmployeeBankAuthorizationService::class);
        $authorizationService->shouldReceive('authorizeDelete')
            ->andReturn(true);

        $employeeRequestService = Mockery::mock(EmployeeRequestService::class);
        $employeeRequestService->shouldReceive('getEmployee')
            ->andReturn(new JsonResponse(json_encode(['company_id' => 1])));

        $companyRequestService = Mockery::mock(PhilippineCompanyRequestService::class);
        $companyRequestService->shouldReceive('get')
            ->andReturn(new JsonResponse(json_encode(['id' => 1, 'account_id' => 1])));

        $this->app->instance(BankRequestService::class, $mockBankRequestService);
        $this->app->instance(EmployeeBankAuthorizationService::class, $authorizationService);
        $this->app->instance(EmployeeRequestService::class, $employeeRequestService);
        $this->app->instance(PhilippineCompanyRequestService::class, $companyRequestService);

        $response = app()->handle($request);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals(collect($mockResponse)->toJson(), $response->getContent());

    }
}
