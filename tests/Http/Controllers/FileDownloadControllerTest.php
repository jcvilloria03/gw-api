<?php

namespace Tests\Http\Controllers;

use Tests\TestCase;
use App\Http\Controllers\FileDownloadController;
use Mockery;
use App\Payroll\PayrollRequestService;
use App\Payslip\PayslipRequestService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Payroll\PayrollAuthorizationService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FileDownloadControllerTest extends TestCase
{
    public function testGetDownloadFileFromPayrollJobsApi()
    {
        $payrollJobData = [
            "data" => [
                "id" => 320,
                "payroll_id" => 219,
                "job_id" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                "type" => "ATTENDANCE",
                "uploaded_file" => "08-01-2019_to_08-31-2019-348-905-156213701740245d1c51b962408668631388.csv",
                "deleted_at" => null
            ]
        ];

        $payrollData = [
            'id' => 219,
            'account_id' => 461,
            'company_id' => 202
        ];

        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
        
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getPayrollJob' => new JsonResponse(collect($payrollJobData)->toJson(), Response::HTTP_OK)
        ]);
        
        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => true
        ]);

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 461]]);

        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();

        $response = $fileDownloadController
            ->getPayrollUpload(
                $payrollJobData['data']['job_id'],
                strtolower($payrollJobData['data']['type'])
            );

        $this->assertEquals(
            $response,
            '/' . $payrollJobData['data']['uploaded_file']
        );
    }

    public function testGetDownloadFileFromPayrollJobsApiNotMatchingType()
    {
        
        $payrollJobData = [
            "data" => [
                "id" => 320,
                "payroll_id" => 219,
                "job_id" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                "type" => "ATTENDANCE",
                "uploaded_file" => "08-01-2019_to_08-31-2019-348-905-156213701740245d1c51b962408668631388.csv",
                "deleted_at" => null
            ]
        ];

        $payrollData = [
            'id' => 219,
            'account_id' => 461,
            'company_id' => 202
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['id' => 99, 'account_id' => 461]]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getPayrollJob' => new JsonResponse(collect($payrollJobData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => true
        ]);
        
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $this->expectException(HttpException::class);
        $fileDownloadController->getPayrollUpload(
            $payrollJobData['data']['job_id'],
            'bonus'
        );
    }

    public function testGetDownloadFileFromPayrollJobsApiNotAuthorized()
    {
        $payrollJobData = [
            "data" => [
                "id" => 320,
                "payroll_id" => 219,
                "job_id" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                "type" => "ATTENDANCE",
                "uploaded_file" => "08-01-2019_to_08-31-2019-348-905-156213701740245d1c51b962408668631388.csv",
                "deleted_at" => null
            ]
        ];

        $payrollData = [
            'id' => 219,
            'account_id' => 461,
            'company_id' => 202
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 462]]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getPayrollJob' => new JsonResponse(collect($payrollJobData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => false
        ]);
        
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $this->expectException(HttpException::class);
        $fileDownloadController->getPayrollUpload(
            $payrollJobData['data']['job_id'],
            'bonus'
        );
    }

    public function testGetDownloadFileFromPayrollRegister()
    {
        $payrollRegisterData = [
            "data" => [
                "type" => "payroll-register",
                "id" => 320,
                "attributes" => [
                    "payroll_id" => 219,
                    "hash" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                    "file" => "account_461/company_202/payroll_register/219/
                        payroll_register-154434662773-156213701740245d1c51b962408668631388.csv",
                    "deleted_at" => null
                ]

            ]
        ];

        $payrollData = [
            'id' => 219,
            'account_id' => 461,
            'company_id' => 202
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 461]]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getPayrollRegister' => new JsonResponse(collect($payrollRegisterData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => true
        ]);
        
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $fileDownloadController->getPayrollRegister(
            $payrollRegisterData['data']['attributes']['hash'],
            'payroll_register'
        );
    }

    public function testGetDownloadFileFromPayrollRegisterUnauthorized()
    {
        $payrollRegisterData = [
            "data" => [
                "type" => "payroll-register",
                "id" => 320,
                "attributes" => [
                    "payroll_id" => 299,
                    "hash" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                    "file" => "account_461/company_202/payroll_register/219/
                        payroll_register-154434662773-156213701740245d1c51b962408668631388.csv",
                    "deleted_at" => null
                ]

            ]
        ];

        $payrollData = [
            'id' => 299,
            'account_id' => 462,
            'company_id' => 222
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 461]]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getPayrollRegister' => new JsonResponse(collect($payrollRegisterData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => false
        ]);
        
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $this->expectException(HttpException::class);
        $fileDownloadController->getPayrollRegister(
            $payrollRegisterData['data']['attributes']['hash'],
            'payroll_register'
        );
    }

    public function testGetDownloadFileFromZippedPayrollRegisters()
    {
        $payrollRegisterData = [
            "data" => [
                "type" => "payroll-register",
                "id" => 320,
                "attributes" => [
                    "payroll_ids" => '219,220,221',
                    "hash" => "89de77a6-51ff-4b26-87da-8644596b01b6",
                    "uri" => "account_461/company_202/payroll_register/219/
                        payroll_register-154434662773-156213701740245d1c51b962408668631388.zip",
                    "deleted_at" => null,
                    "signed_url" => 'https://bucketname.s3-ap-southeast-1.amazonaws.com/account_461/
                        company_202/payroll_register/219/payroll_register-154434662773-1562137017402
                        45d1c51b962408668631388.csv?response-content-disposition=inline%3B%20filenam
                        e%3D%22payroll_register_SnhLX2X2Ecw49jGQynhsGKtq928Xpz1jExuuIW4q.zip%22&X-Am
                        z-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Cre
                        dential=AKIASYORSKCUWURNQ7VP%2F20190815%2Fap-southeast-1%2Fs3%2Faws4_request
                        &X-Amz-Date=20190815T030650Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz
                        -Signature=593a7b8211109b09e586b708aaf289800feb4685cadc6d69aba8e6597218ddc1'
                ]

            ]
        ];

        $payrollData = [
            'id' => 219,
            'account_id' => 461,
            'company_id' => 202
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 461]]);

        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getZippedPayrollRegisterByHash' =>
                new JsonResponse(collect($payrollRegisterData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => true
        ]);
        
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $fileDownloadController->getZippedPayrollRegister(
            $payrollRegisterData['data']['attributes']['hash'],
            'payroll_registers'
        );
    }

    public function testGetDownloadFileFromZippedPayrollRegistersUnauthorized()
    {
        $payrollData = [
            'id' => 299,
            'account_id' => 422,
            'company_id' => 232
        ];

        $hash = Str::random(40);
        $payrollRegisterData = [
            "data" => [
                "type" => "payroll-register",
                "id" => 320,
                "attributes" => [
                    "payroll_ids" => '219,220,221',
                    "hash" => $hash,
                    "uri" => "account_462/company_222/payroll_register/78sad78saf/
                        payroll_register-154434662773-156213701740245d1c51b962408668631388.zip",
                    "deleted_at" => null,
                    "signed_url" => 'https://bucketname.s3-ap-southeast-1.amazonaws.com/account_462/
                        company_222/78sad78saf/219/payroll_register-154434662773-156213701740245d1c5
                        1b962408668631388.csv?response-content-disposition=inline%3B%20filename%3D%2
                        2payroll_register_SnhLX2X2Ecw49jGQynhsGKtq928Xpz1jExuuIW4q.zip%22&X-Amz-Cont
                        ent-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credentia
                        l=AKIASYORSKCUWURNQ7VP%2F20190815%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz
                        -Date=20190815T030650Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz-Signa
                        ture=593a7b8211109b09e586b708aaf289800feb4685cadc6d69aba8e6597218ddc1'
                ]

            ]
        ];

        $request = new Request();
        $request->attributes->add(['user' => ['user_id' => 99, 'account_id' => 461]]);

        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
                
        $mockPayrollService->shouldReceive([
            'get' => new JsonResponse(collect($payrollData)->toJson(), Response::HTTP_OK),
            'getZippedPayrollRegisterByHash' =>
                new JsonResponse(collect($payrollRegisterData)->toJson(), Response::HTTP_OK)
        ]);

        $mockPayrollAuthorizationService->shouldReceive([
            'authorizeGet' => false
        ]);
        
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        $this->app->instance(Request::class, $request);
        
        $fileDownloadController = new FileDownloadController();
        
        $this->expectException(HttpException::class);
        $fileDownloadController->getZippedPayrollRegister(
            $hash,
            'payroll_registers'
        );
    }

    public function testGetDownloadFileFromPayslip()
    {
        $payslipData = [
            "uri" => "https://test-bucket-payslips.s3-ap-southeast-1.amazonaws.com/company_1/1/payslip-1.pdf",
        ];

        $payslipId = 1;

        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
           
        $mockPayslipService
            ->shouldReceive('download')
            ->with($payslipId, ['download' => true, 'read' => false])
            ->andReturn(new JsonResponse(collect($payslipData)->toJson(), Response::HTTP_OK));

        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        
        $fileDownloadController = new FileDownloadController();
        
        $fileDownloadController->getPayslip(
            $payslipId,
            'payslip'
        );
    }

    public function testGetDownloadFileFromZippedPayslip()
    {
        $payslipData = [
            "uri" => "https://test-bucket-payslips.s3-ap-southeast-1.amazonaws.com/20191607_2043_AB%20Corporation.zip",
        ];

        $payslipId = 1;

        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
   
        $mockPayslipService
            ->shouldReceive('getZippedPayslipsUrl')
            ->andReturn(new JsonResponse(collect($payslipData)->toJson(), Response::HTTP_OK));

        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);
        
        $fileDownloadController = new FileDownloadController();
        
        $fileDownloadController->getZippedPayslips(
            $payslipId,
            'payslips'
        );
    }

    public function testMakeDownloadHeader()
    {
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);
    
        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);

        $fileDownloadController = new FileDownloadController();
        
        $response = $fileDownloadController
            ->makeHeaders('csv', 'commission');
        
        $this->assertArrayHasKey('Content-Disposition', $response);
        $this->assertArrayHasKey('Cache-Control', $response);
        $this->assertArrayHasKey('Content-Type', $response);

        $this->assertContains('download_commission_', $response['Content-Disposition']);
        $this->assertStringEndsWith('.csv', $response['Content-Disposition']);
    }

    public function testMakeDownloadHeaderInvalidMimeType()
    {
        $mockPayslipService = Mockery::mock(PayslipRequestService::class);
        $mockPayrollService = Mockery::mock(PayrollRequestService::class);
        $mockPayrollAuthorizationService = Mockery::mock(PayrollAuthorizationService::class);

        $this->app->instance(PayslipRequestService::class, $mockPayslipService);
        $this->app->instance(PayrollRequestService::class, $mockPayrollService);
        $this->app->instance(PayrollAuthorizationService::class, $mockPayrollAuthorizationService);

        $fileDownloadController = new FileDownloadController();
    
        $this->expectException(\Exception::class);
        $fileDownloadController
            ->makeHeaders('test', 'commission');
    }
}
