<?php

namespace Tests\Http\Controllers;

use App\Auth0\Auth0UserService;
use App\ClockState\EssClockStateAuthorizationService;
use App\ESS\EssClockStateService;
use App\Events\ClockStateChanged;
use Auth0\SDK\JWTVerifier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use App\Attendance\ClockStateAttendanceTriggerService;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ClockStateControllerTest extends \Tests\TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->markTestSkipped('will deprecate soon');
    }
    
    public function testLogEventDispatched()
    {
        $employeeId = 1;
        $companyId = 2;

        Event::fake();

        $this->mockEssClockStateService([
            'log' => [
                'employee_uid' => $employeeId,
            ],
            'lastTimeclock' => ""
        ]);

        $this->mockJWTVerifier([
            "verifyAndDecode" => (object)['sub' => 'dummy']
        ]);

        $this->mockUserRequestService([
            "getUserByAuth0UserId" => (object)[
                'employee_id' => $employeeId,
                'employee_company_id' => $companyId,
                'user_id' => 123,
                'account_id' => 23,
            ]
        ]);

        $this->mockEssClockStateAuthorizationService([
            "authorizeLog" => true,
            "isIPAllowed" => true,
            "validateTimeMethods" => true
        ]);

        
        $this->mockClockStateAttendanceTriggerService([
            "calculateAttendance" => ""
        ]);

        $request = $this->mockRequestObject($employeeId, $companyId, [
            'state' => 1,
            'tags' => [],
            'origin' => 'Web Bundy',
            'timestamp' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->handle($request);

        Event::assertDispatched(ClockStateChanged::class, function ($event) use ($employeeId) {
            return $event->response['employee_uid'] === $employeeId;
        });
    }

    private function mockEssClockStateService(array $responses)
    {
        $essClockStateMock = \Mockery::mock(EssClockStateService::class);

        foreach ($responses as $method => $response) {
            $essClockStateMock->shouldReceive($method)
                ->andReturn(response()->json($response));
        }

        $this->app->instance(EssClockStateService::class, $essClockStateMock);
    }

    private function mockJWTVerifier(array $responses)
    {
        $jwtVerifierMock = \Mockery::mock(JWTVerifier::class);
        $jwtVerifierMock->shouldReceive('verifyAndDecode')->andReturn((object)['sub' => 'dummy']);

        $this->app->instance(JWTVerifier::class, $jwtVerifierMock);

        foreach ($responses as $method => $response) {
            $jwtVerifierMock->shouldReceive($method)
                ->andReturn($response);
        }

        $this->app->instance(JWTVerifier::class, $jwtVerifierMock);
    }

    private function mockUserRequestService(array $responses)
    {
        $userRequestServiceMock = \Mockery::mock(Auth0UserService::class);

        foreach ($responses as $method => $response) {
            $userRequestServiceMock->shouldReceive($method)
                ->andReturn($response);
        }

        $this->app->instance(Auth0UserService::class, $userRequestServiceMock);
    }

    private function mockEssClockStateAuthorizationService(array $responses)
    {
        $authorizationServiceMock = \Mockery::mock(EssClockStateAuthorizationService::class);

        foreach ($responses as $method => $response) {
            $authorizationServiceMock->shouldReceive($method)
                ->andReturn($response);
        }

        $this->app->instance(EssClockStateAuthorizationService::class, $authorizationServiceMock);
    }

    private function mockClockStateAttendanceTriggerService(array $responses)
    {
        $clockStateAttendanceTriggerServiceMock = \Mockery::mock(ClockStateAttendanceTriggerService::class);

        foreach ($responses as $method => $response) {
            $clockStateAttendanceTriggerServiceMock->shouldReceive($method)
                ->andReturn(response()->json($response));
        }
        
        $this->app->instance(ClockStateAttendanceTriggerService::class, $clockStateAttendanceTriggerServiceMock);
    }

    private function mockRequestObject($employeeId, $companyId, $body)
    {
        $this->currentUri = $this->prepareUrlForRequest('/ess/clock_state/log');

        $symfonyRequest = SymfonyRequest::create(
            $this->currentUri,
            'POST',
            $body,
            [],
            [],
            ['HTTP_X_FORWARDED_FOR' => '127.0.0.1', 'HTTP_X_REAL_IP' => '127.0.0.1'],
            null
        );

        $request = Request::createFromBase($symfonyRequest);
        $request->attributes->set('user', [
            'employee_id' => $employeeId,
            'employee_company_id' => $companyId,
            'user_id' => 123,
            'account_id' => 23
        ]);
        $request->attributes->set('companies_users', [
            [
                'employee_id' => $employeeId,
                'employee_company_id' => $companyId,
                'user_id' => 123,
                'account_id' => 23
            ]
        ]);
        
        return $request;
    }

    public function testConsecutiveClockIns()
    {
        $employeeId = 1;
        $companyId = 2;
        $currentTime = time() - 10;
        $dateTime = Carbon::createFromTimestamp($currentTime)->format('Y-m-d H:i');

        $this->mockEssClockStateService([
            'log' => [
                'employee_uid' => $employeeId,
            ],
            'lastTimeclock' => "{
                \"employee_uid\": 1.0,
                \"company_uid\": 1,
                \"tags\": [],
                \"state\": true,
                \"timestamp\": $currentTime
            }"
        ]);

        $this->mockJWTVerifier([
            "verifyAndDecode" => (object)['sub' => 'dummy']
        ]);

        $this->mockUserRequestService([
            "getUserByAuth0UserId" => (object)[
                'employee_id' => $employeeId,
                'employee_company_id' => $companyId,
                'user_id' => 123,
                'account_id' => 23,
            ]
        ]);

        $this->mockEssClockStateAuthorizationService([
            "authorizeLog" => true,
            "isIPAllowed" => true,
            "validateTimeMethods" => true
        ]);

        
        $this->mockClockStateAttendanceTriggerService([
            "calculateAttendance" => ""
        ]);

        $request = $this->mockRequestObject($employeeId, $companyId, [
            'state' => 1,
            'tags' => [],
            'origin' => 'Web Bundy',
            'timestamp' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->handle($request);

        $responseBody = json_decode($this->response->getContent(), true);

        $error = "Clock-in made at {$dateTime} has no matching Clock-out information";

        $this->assertEquals(HttpResponse::HTTP_NOT_ACCEPTABLE, $this->response->getStatusCode());
        $this->assertEquals($error, $responseBody['message']);
    }

    public function testConsecutiveClockOuts()
    {
        $employeeId = 1;
        $companyId = 2;
        $currentTime = time() - 10;
        $dateTime = Carbon::createFromTimestamp($currentTime)->format('Y-m-d H:i');

        $this->mockEssClockStateService([
            'log' => [
                'employee_uid' => $employeeId,
            ],
            'lastTimeclock' => "{
                \"employee_uid\": 1.0,
                \"company_uid\": 1,
                \"tags\": [],
                \"state\": false,
                \"timestamp\": $currentTime
            }"
        ]);

        $this->mockJWTVerifier([
            "verifyAndDecode" => (object)['sub' => 'dummy']
        ]);

        $this->mockUserRequestService([
            "getUserByAuth0UserId" => (object)[
                'employee_id' => $employeeId,
                'employee_company_id' => $companyId,
                'user_id' => 123,
                'account_id' => 23,
            ]
        ]);

        $this->mockEssClockStateAuthorizationService([
            "authorizeLog" => true,
            "isIPAllowed" => true,
            "validateTimeMethods" => true
        ]);

        
        $this->mockClockStateAttendanceTriggerService([
            "calculateAttendance" => ""
        ]);

        $request = $this->mockRequestObject($employeeId, $companyId, [
            'state' => 0,
            'tags' => [],
            'origin' => 'Web Bundy',
            'timestamp' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->handle($request);

        $responseBody = json_decode($this->response->getContent(), true);
        
        $error = "Clock-out for this day have already been recorded at {$dateTime}";

        $this->assertEquals(HttpResponse::HTTP_NOT_ACCEPTABLE, $this->response->getStatusCode());
        $this->assertEquals($error, $responseBody['message']);
    }
}
