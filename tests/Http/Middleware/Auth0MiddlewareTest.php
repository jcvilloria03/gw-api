<?php

namespace Tests\Http\Middleware;

use App\Http\Middleware\Auth0Middleware;
use Auth0\SDK\JWTVerifier;
use Auth0\SDK\Exception\CoreException as Auth0CoreException;
use Illuminate\Http\Request;
use Mockery as m;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use PHPUnit\Framework\TestCase;

class Auth0MiddlewareTest extends TestCase
{
    public function testInvalidHeader()
    {
        // no Authorization tag
        $request = Request::create('http://test', 'GET');

        $middleware = new Auth0Middleware();
        $response = $middleware->handle($request, function () {
        });
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testMissingToken()
    {
        // invalid format of authorization
        $request = Request::create('http://test', 'GET');
        $request->headers->set('Authorization', 'Barer ');

        $middleware = new Auth0Middleware();
        $response = $middleware->handle($request, function () {
        });
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->getStatusCode());

        // no token following 'Bearer' placeholder
        $request = Request::create('http://test', 'GET');
        $request->headers->set('Authorization', 'Bearer ');

        $middleware = new Auth0Middleware();
        $response = $middleware->handle($request, function () {
        });
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testInvalidToken()
    {
        // no token following 'Bearer' placeholder
        $request = Request::create('http://test', 'GET');
        $request->headers->set('Authorization', 'Bearer invalidToken');

        $mockVerifier = m::mock(JWTVerifier::class)
            ->shouldReceive('verifyAndDecode')
            ->once()
            ->andThrow(Auth0CoreException::class, 'Unauthorized')
            ->getMock();

        $middleware = new Auth0Middleware($mockVerifier);
        $response = $middleware->handle($request, function () {
        });
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testToken()
    {
        // no token following 'Bearer' placeholder
        $request = Request::create('http://test', 'GET');
        $request->headers->set('Authorization', 'Bearer mockToken');

        $mockVerifier = m::mock(JWTVerifier::class)
            ->shouldReceive('verifyAndDecode')
            ->once()
            ->andReturn('success')
            ->getMock();

        $middleware = new Auth0Middleware($mockVerifier);
        $response = $middleware->handle($request, function () {
        });
        // if response is null, middleware ran successfully
        $this->assertNull($response);
    }
}
