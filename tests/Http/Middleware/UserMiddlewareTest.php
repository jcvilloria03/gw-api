<?php

namespace Tests\Http\Middleware;

use Mockery as m;
use App\Model\Auth0User;
use App\Model\CompanyUser;
use Illuminate\Http\Request;
use App\Auth0\Auth0UserService;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;
use App\CompanyUser\CompanyUserService;
use App\Http\Middleware\UserMiddleware;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserMiddlewareTest extends TestCase
{
    public function testMissingToken()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $middleware = new UserMiddleware($mockCompanyUserService);

        // no token found
        $request = Request::create('http://test', 'GET');
        $response = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());

        // empty sub property for decoded token
        $request = Request::create('http://test', 'GET');
        $request->attributes->add([
            'user_id' => 0
        ]);

        $response = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testUserNotFound()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $middleware = new UserMiddleware($mockCompanyUserService);

        // no user found based on token details
        $request = Request::create('http://test', 'GET');

        $middleware->handle($request, function () {
        });
    }

    public function testUserDataReturnedMissingUserId()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $middleware = new UserMiddleware($mockCompanyUserService);

        $request = Request::create('http://test', 'GET');
        $decodedToken = (object) [
            'sub' => 'id'
        ];
        $request->attributes->add([
            'decodedToken' => $decodedToken
        ]);

        $response = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertEquals('Unauthorized.', $response->getContent());
    }

    public function testUserDataReturnedMissingAccountId()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $middleware = new UserMiddleware($mockCompanyUserService);

        $request = Request::create('http://test', 'GET');
        $decodedToken = (object) [
            'sub' => 'id'
        ];
        $request->attributes->add([
            'decodedToken' => $decodedToken
        ]);

        $response = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertEquals('Unauthorized.', $response->getContent());
    }

    public function testUserFound()
    {
        $userId = mt_rand(1, 4);
        $accountId = mt_rand(1, 4);
        $mockCompanyUserService = m::mock(CompanyUserService::class)
            ->shouldReceive('getAllByUserId')
            ->once()
            ->andReturn(collect([new CompanyUser]))
            ->getMock();
        $middleware = new UserMiddleware($mockCompanyUserService);

        // no user found based on token details
        $request = Request::create('http://test', 'GET');

        $request->attributes->add([
            'user_id' => $userId,
            'account_id' => $accountId
        ]);

        $request = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $userData = $request->attributes->get('user');
        $this->assertEquals($userId, $userData['user_id']);
        $this->assertEquals($accountId, $userData['account_id']);
        $this->assertEmpty($userData['employee_id']);
        $this->assertEmpty($userData['employee_company_id']);
    }

    public function testUserInactive()
    {
        $mockCompanyUserService = m::mock(CompanyUserService::class);
        $middleware = new UserMiddleware($mockCompanyUserService);

        // no user found based on token details
        $request = Request::create('http://test', 'GET');
        $decodedToken = (object) [
            'sub' => 'id'
        ];
        $request->attributes->add([
            'decodedToken' => $decodedToken
        ]);

        $response = $middleware->handle($request, function () use ($request) {
            return $request;
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        $this->assertEquals('Unauthorized.', $response->getContent());
    }
}
