<?php

namespace Tests\Http\Middleware;

use Mockery as M;
use Tests\TestCase;
use App\Model\AuthnUser;
use App\Model\CompanyUser;
use App\Authn\AuthnService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use App\CompanyUser\CompanyUserService;
use App\Http\Middleware\AuthnMiddleware;
use App\Subscriptions\SubscriptionsRequestService;

class SubscriptionMiddlewareTest extends TestCase
{
    const TEST_URL = '/unit_test/middleware/subscription';

    public function setUp()
    {
        parent::setUp();

        $this->userData = new AuthnUser([
            'id' => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'status'        => 'active'
        ]);

        $this->companyUser = new CompanyUser([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $middleware = M::mock(AuthnMiddleware::class);
        $middleware->shouldReceive('handle')
            ->andReturnUsing(function ($request, $next) {
                $request->attributes->add([
                    'user_id' => 1,
                    'account_id' => 1,
                ]);

                return $next($request);
            });

        $authnService = M::mock(AuthnService::class);
        $authnService->shouldReceive('validateToken')
            ->andReturn($this->userData);

        $companyUserService = M::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getAllByUserId')
            ->andReturn(collect([$this->companyUser]));

        $this->app->instance(AuthnMiddleware::class, $middleware);
        $this->app->instance(AuthnService::class, $authnService);
        $this->app->instance(CompanyUserService::class, $companyUserService);

        $api = $this->app->make('Dingo\Api\Routing\Router');

        $api->version('v1', ['middleware' => ['authn', 'user', 'subscription']], function ($api) {
            $api->any(self::TEST_URL, function () {
                return 'OK';
            });
        });
    }

    public function testMiddlewareReturns418OnNoSubscription()
    {
        $subscriptionService = M::mock(SubscriptionsRequestService::class);
        $subscriptionService->shouldReceive('getCustomerByAccountId')
            ->andReturn(new JsonResponse(json_encode([
                'data' => []
            ])));

        $this->app->instance(SubscriptionsRequestService::class, $subscriptionService);

        $response = $this->get(self::TEST_URL);
        $this->assertEquals(JsonResponse::HTTP_I_AM_A_TEAPOT, $response->response->status());
    }

    public function testMiddlewareReturns418OnExpiredSubscription()
    {
        $subscriptionService = M::mock(SubscriptionsRequestService::class);
        $subscriptionService->shouldReceive('getCustomerByAccountId')
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    [
                        'subscriptions' => [
                            [
                                'is_expired' => true,
                                'status' => 'SUSPENDED',
                                'days_left' =>  0
                            ]
                        ]
                    ]
                ]
            ])));

        $this->app->instance(SubscriptionsRequestService::class, $subscriptionService);

        $response = $this->get(self::TEST_URL);
        $this->assertEquals(JsonResponse::HTTP_I_AM_A_TEAPOT, $response->response->status());
    }

    public function testMiddlewareReturns200OnActiveSubscription()
    {
        $subscriptionService = M::mock(SubscriptionsRequestService::class);
        $subscriptionService->shouldReceive('getCustomerByAccountId')
            ->andReturn(new JsonResponse(json_encode([
                'data' => [
                    [
                        'subscriptions' => [
                            [
                                'is_expired' => false,
                                'status' => 'ACTIVE',
                                'days_left' =>  0
                            ]
                        ]
                    ]
                ]
            ])));

        $this->app->instance(SubscriptionsRequestService::class, $subscriptionService);

        $response = $this->get(self::TEST_URL, ['Authorization' => 'Bearer mockToken']);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->response->status());
    }
}
