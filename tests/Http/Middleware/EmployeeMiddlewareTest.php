<?php

namespace Tests\Http\Middleware;

use App\Http\Middleware\EmployeeMiddleware;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class EmployeeMiddlewareTest extends TestCase
{
    public function testMissingEmployeeId()
    {
        $middleware = new EmployeeMiddleware();

        $request = Request::create('http://test', 'GET');
        $request->attributes->add([
            'user' => [],
            'companies_users' => []
        ]);

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function testHandle()
    {
        $middleware = new EmployeeMiddleware();

        $request = Request::create('http://test', 'GET');
        $request->attributes->add([
            'user' => ['employee_id' => 1],
            'companies_users' => [['employee_id' => 1]]
        ]);

        $response = $middleware->handle($request, function () {
            return 'hey';
        });

        $this->assertEquals('hey', $response);
    }
}
