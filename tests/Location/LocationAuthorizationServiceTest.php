<?php

namespace Tests\Location;

use App\Location\LocationAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */

class LocationAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetNoScope()
    {
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsPassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsNoScope()
    {
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testGetCompanyLocationsInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGetCompanyLocations($targetLocationDetails, $user));
    }

    public function testAuthorizeCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreateNoScope()
    {
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailableNoScope()
    {
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailableInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailableInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailableInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testAuthorizeIsNameAvailableInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Location\LocationAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetLocationDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLocationDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LocationAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetLocationDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LocationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLocationDetails, $user));
    }
}
