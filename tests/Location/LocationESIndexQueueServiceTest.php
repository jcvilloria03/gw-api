<?php

namespace Tests\Location;

use App\Location\LocationEsIndexQueueService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class LocationESIndexQueueServiceTest extends TestCase
{
    public function testQueue()
    {
        $locationsIds = [1, 2];

        $mockIndexQueueService = m::mock('App\ES\ESIndexQueueService');
        $mockIndexQueueService->shouldReceive('queue')
            ->with([
                'id' => $locationsIds,
                'type' => 'location',
            ])
            ->once();

        $locationsESIndexQueueService = new LocationEsIndexQueueService($mockIndexQueueService);
        $locationsESIndexQueueService->queue($locationsIds);
    }
}
