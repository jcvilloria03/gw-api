<?php

namespace Tests\Payroll;

use App\FinalPay\FinalPayRequestService;
use App\Payroll\PayrollFinalPayRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class PayrollFinalPayRequestServiceTest extends TestCase
{
    /**
     * @group payroll-final
     */
    public function testSaveFinalPaySuccess()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_CREATED, [])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollFinalPayRequestService($client);
        $response = $requestService->createFinalPayroll('');

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @group payroll-final
     */
    public function testSaveFinalPayError()
    {
        $this->expectException(HttpException::class);
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_UNPROCESSABLE_ENTITY, [])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollFinalPayRequestService($client);
        $requestService->createFinalPayroll('');
    }
}
