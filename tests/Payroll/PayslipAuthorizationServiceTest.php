<?php

namespace Tests\Payroll;

use App\Payroll\PayslipAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class PayslipAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }


    public function testAuthorizeCreateNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::GENERATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }


    public function testAuthorizeGetNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayslipAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }
}
