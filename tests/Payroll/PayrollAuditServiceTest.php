<?php

namespace Tests\Payroll;

use App\Payroll\PayrollAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollAuditServiceTest extends TestCase
{
    public function testLogCalculate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'job_id' => 1,
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_CALCULATE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logCalculate($item);
    }

    public function testLogClose()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'status' => 1,
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_CLOSE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logClose($item);
    }

    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'status' => 1,
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'status' => 1,
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logDelete($item);
    }

    public function testLogEdit()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'gross' => 4,
            'employees' => [
                [
                    'id' => 1,
                    'gross' => 5,
                ]
            ]
        ]);
        $newData = json_encode([
            'gross' => 5,
            'employees' => [
                [
                    'id' => 1,
                    'gross' => 6,
                ]
            ]
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_EDIT,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->times(2);

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logEdit($item);
    }

    public function testLogEditNoSummaryChange()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'gross' => 4,
            'employees' => [
                [
                    'id' => 1,
                    'gross' => 5,
                ]
            ]
        ]);
        $newData = json_encode([
            'employees' => [
                [
                    'id' => 1,
                    'gross' => 6,
                ]
            ]
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_EDIT,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logEdit($item);
    }

    public function testLogEditNoEmployeeChange()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'company_id' => 1,
            'id' => 1,
            'gross' => 4,
            'employees' => [
                [
                    'id' => 1,
                    'gross' => 5,
                ]
            ]
        ]);
        $newData = json_encode([
            'gross' => 5
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_EDIT,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData,
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logEdit($item);
    }

    public function testLogUpload()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $data = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'status' => 1,
            'job_id' => 1,
            'type' => 1,
        ]);
        $item = [
            'action' => PayrollAuditService::ACTION_UPLOAD,
            'user' => $user,
            'old' => $data
        ];
        $mockRequestService = m::mock('App\Payroll\PayrollRequestService');
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $employeeAuditService = new PayrollAuditService($mockRequestService, $mockAuditService);
        $employeeAuditService->logUpload($item);
    }
}
