<?php

namespace Tests\Payroll;

use App\Payroll\PayrollDeductionUploadTask;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollDeductionUploadTaskTest extends TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollDeductionUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollDeductionUploadTask::class, $task);
    }
}
