<?php

namespace Tests\Payroll;

use App\Payroll\PayrollAuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class PayrollAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeUploadPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testUploadInvalidCompanyLevelValidPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }


    public function testAuthorizeUploadNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeUploadInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpload($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreatePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }


    public function testAuthorizeCreateNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCreateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetPayrollDetails, $user));
    }


    public function testAuthorizeEditPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }


    public function testAuthorizeEditNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeEditInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::EDIT_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeEdit($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }


    public function testAuthorizeGetNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeGetInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetPayrollDetails, $user));
    }

    public function testAuthorizeClosePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeClosePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeClosePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeClosePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }


    public function testAuthorizeCloseNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeCloseInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeCloseInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeCloseInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeCloseInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::CLOSE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }


    public function testAuthorizeDeleteNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetPayrollDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsNoScope()
    {
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeViewCompanyPayrollsInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetCompanyDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope('Invalid Target Type', [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeViewCompanyPayrolls($targetCompanyDetails, $user));
    }

    public function testAuthorizeCalculatePassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculatePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }


    public function testAuthorizeCalculateNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculateInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculateInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculateInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeCalculateInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::RUN_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCalculate($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipPassAccountLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $accountId = 1;
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipPassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeSendPayslipInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::SEND_PAYSLIP_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSendPayslips($targetPayrollDetails, $user));
    }

    public function testAuthorizeOpenNoScope()
    {
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeOpenInvalidAccountScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::OPEN_TASK);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeOpenInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::OPEN_TASK);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeOpenInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::OPEN_TASK);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }

    public function testAuthorizeOpenInvalidOtherScope()
    {
        $taskScope = new TaskScopes(PayrollAuthorizationService::OPEN_TASK);
        $targetPayrollDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payroll\PayrollAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeClose($targetPayrollDetails, $user));
    }


}
