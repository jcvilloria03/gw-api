<?php

namespace Tests\Payroll;

use App\Payroll\PayrollUploadTask;
use App\Payroll\PayrollTaskException;
use Illuminate\Support\Facades\Redis;
use Mockery as m;

class PayrollUploadTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollUploadTask::class, $task);
    }

    public function testCreateNoJobId()
    {
        $payrollId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($payrollId);

        $idPrefix =  PayrollUploadTask::ID_PREFIX . $payrollId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateWithJobIdExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);

        $this->assertEquals($task->getId(), $jobId);
    }

    public function testCreateWithJobIdNonExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
    }

    public function testIsPayrollIdSameValidNewJob()
    {
        $payrollId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($payrollId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameValidExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . 1 . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->never();

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
        $this->assertFalse($task->isPayrollIdSame());
    }

    public function testGetS3Bucket()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        $this->assertNotEmpty($task->getS3Bucket());
    }

    public function testSaveFile()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hSet')
            ->times(2);

        $task->create($payrollId, $jobId);
        $s3Key = $task->saveFile('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  PayrollUploadTask::ID_PREFIX . $payrollId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }

    public function testUpdateValidationStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);
        $this->setExpectedException(PayrollTaskException::class);
        $task->updateValidationStatus('invalid status');
    }

    public function testUpdateValidationStatusPersonal()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusPayroll()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToQueued()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToQueuedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => PayrollUploadTask::STATUS_VALIDATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_QUEUED);
    }

    public function testUpdateValidationStatusToValidating()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => PayrollUploadTask::STATUS_VALIDATION_QUEUED]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATING);
    }

    public function testUpdateValidationStatusToValidatingInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_VALIDATING,
            PayrollUploadTask::STATUS_VALIDATED,
            PayrollUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATING);
    }

    public function testUpdateValidationStatusToValidated()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => PayrollUploadTask::STATUS_VALIDATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATED);
    }

    public function testUpdateValidationStatusToValidatedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_VALIDATION_QUEUED,
            PayrollUploadTask::STATUS_VALIDATED,
            PayrollUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATED);
    }

    public function testUpdateValidationStatusToValidationFailed()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => PayrollUploadTask::STATUS_VALIDATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_FAILED);
    }

    public function testUpdateValidationStatusToValidationFailedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_VALIDATION_QUEUED,
            PayrollUploadTask::STATUS_VALIDATED,
            PayrollUploadTask::STATUS_VALIDATION_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateValidationStatus(PayrollUploadTask::STATUS_VALIDATION_FAILED);
    }

    public function testUpdateSaveStatusInvalidStatus()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);
        $this->setExpectedException(PayrollTaskException::class);
        $task->updateSaveStatus('invalid status');
    }

    public function testUpdateSaveStatusValid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_VALIDATING,
            PayrollUploadTask::STATUS_VALIDATION_QUEUED,
            PayrollUploadTask::STATUS_VALIDATION_FAILED,
        ];

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)],
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $this->setExpectedException(PayrollTaskException::class);
        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueued()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToQueuedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => PayrollUploadTask::STATUS_SAVING
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_QUEUED);
    }

    public function testUpdateSaveStatusToValidating()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => PayrollUploadTask::STATUS_SAVE_QUEUED
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidatingInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_SAVING,
            PayrollUploadTask::STATUS_SAVED,
            PayrollUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVING);
    }

    public function testUpdateSaveStatusToValidated()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => PayrollUploadTask::STATUS_SAVING
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidatedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_SAVE_QUEUED,
            PayrollUploadTask::STATUS_SAVED,
            PayrollUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVED);
    }

    public function testUpdateSaveStatusToValidationFailed()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => PayrollUploadTask::STATUS_SAVING
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateSaveStatusToValidationFailedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollUploadTask::STATUS_SAVE_QUEUED,
            PayrollUploadTask::STATUS_SAVED,
            PayrollUploadTask::STATUS_SAVE_FAILED,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([
                'validation_status' => PayrollUploadTask::STATUS_VALIDATED,
                'save_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]
            ]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateSaveStatus(PayrollUploadTask::STATUS_SAVE_FAILED);
    }

    public function testUpdateErrorFileLocation()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $validProcesses = [
            PayrollUploadTask::PROCESS_VALIDATION,
            PayrollUploadTask::PROCESS_SAVE,
        ];
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['blah']);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateErrorFileLocation($validProcesses[array_rand($validProcesses)], 'key');
    }

    public function testFetch()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->times(2)
            ->andReturn(['a' => 'b'], []);

        $task->create($payrollId, $jobId);
        $task->fetch();
    }

    public function testFetchSelectedFields()
    {
        $payrollId = 999;
        $jobId = PayrollUploadTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        Redis::shouldReceive('hMGet')
            ->once();

        $task->create($payrollId, $jobId);
        $task->fetch(['field1', 'field2']);
    }

    public function testFetchErrorFileFromS3Invalid()
    {
        // empty result and no result body should return null
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->times(2)
            ->andReturn([], ['Header' => 'haha']);
        $task = new PayrollUploadTask($mockS3Client);

        $this->assertNull($task->fetchErrorFileFromS3('key'));
        $this->assertNull($task->fetchErrorFileFromS3('key'));
    }

    public function testFetchErrorFileFromS3Valid()
    {
        // empty result and no result body should return null
        $mockStream = new class() {
            public function getContents()
            {
                return '{"a" : 1}';
            }
        };
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('getObject')
            ->once()
            ->andReturn(['Body' => $mockStream]);
        $task = new PayrollUploadTask($mockS3Client);

        $this->assertEquals(["a" => 1], $task->fetchErrorFileFromS3('key'));
    }

    public function testSetUserId()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hSet')
            ->once();

        $task->setUserId(1);
    }

    public function testGetUserId()
    {
        $userId = 2;
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollUploadTask($mockS3Client);

        Redis::shouldReceive('hGet')
            ->once()
            ->andReturn($userId);

        $this->assertEquals($userId, $task->getUserId());
    }
}
