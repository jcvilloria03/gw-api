<?php

namespace Tests\Payroll;

use App\Payroll\PayrollBonusUploadTask;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollBonusUploadTaskTest extends TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollBonusUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollBonusUploadTask::class, $task);
    }
}
