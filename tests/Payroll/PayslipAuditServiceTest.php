<?php

namespace Tests\Payroll;

use App\Payroll\PayslipAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayslipAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $data = json_encode([
            'company_id' => 1,
            'id' => 1,
            'payroll_group_id' => 1,
            'start_date' => 1,
            'end_date' => 1,
            'payroll_date' => 1,
            'status' => 1
        ]);
        $item = [
            'action' => PayslipAuditService::ACTION_CREATE,
            'user' => $user,
            'old' => $data
        ];
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $payslipAuditService = new PayslipAuditService($mockAuditService);
        $payslipAuditService->log($item);
    }

    public function testLogView()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1,
        ]);
        $data = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => PayslipAuditService::ACTION_VIEW,
            'user' => $user,
            'new' => $data
        ];
        $mockAuditService = m::mock('App\Audit\AuditService');
        $mockAuditService->shouldReceive('log')
            ->once();

        $payslipAuditService = new PayslipAuditService($mockAuditService);
        $payslipAuditService->log($item);
    }
}
