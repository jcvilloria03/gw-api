<?php

namespace Tests\Payroll;

use App\Payroll\PayrollAllowanceUploadTask;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollAllowanceUploadTaskTest extends TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollAllowanceUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollAllowanceUploadTask::class, $task);
    }
}
