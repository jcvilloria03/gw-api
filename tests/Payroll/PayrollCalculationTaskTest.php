<?php

namespace Tests\Payroll;

use App\Payroll\PayrollCalculationTask;
use App\Payroll\PayrollTaskException;
use Illuminate\Support\Facades\Redis;

class PayrollCalculationTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $task = new PayrollCalculationTask();

        $this->assertInstanceOf(PayrollCalculationTask::class, $task);
    }

    public function testCreateNoJobId()
    {
        $payrollId = 999;

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($payrollId);

        $idPrefix =  PayrollCalculationTask::ID_PREFIX . $payrollId . ':';
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateWithJobIdExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);

        $this->assertEquals($task->getId(), $jobId);
    }

    public function testCreateWithJobIdNonExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
    }

    public function testIsPayrollIdSameValidNewJob()
    {
        $payrollId = 999;

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hMSet')
            ->once();

        $task->create($payrollId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameValidExistingJob()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . 1 . ':' . uniqid();

        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->never();

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
        $this->assertFalse($task->isPayrollIdSame());
    }

    public function testUpdateCalculationStatusInvalidStatus()
    {
        $task = new PayrollCalculationTask();
        $this->setExpectedException(PayrollTaskException::class);
        $task->updateCalculationStatus('invalid status');
    }

    public function testUpdateCalculationStatusPersonal()
    {
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_QUEUED);
    }

    public function testUpdateCalculationStatusPayroll()
    {
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_QUEUED);
    }

    public function testUpdateCalculationStatusToQueued()
    {
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_QUEUED);
    }

    public function testUpdateCalculationStatusToQueuedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => PayrollCalculationTask::STATUS_CALCULATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_QUEUED);
    }

    public function testUpdateCalculationStatusToCalculating()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => PayrollCalculationTask::STATUS_CALCULATION_QUEUED]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATING);
    }

    public function testUpdateCalculationStatusToCalculatingInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollCalculationTask::STATUS_CALCULATING,
            PayrollCalculationTask::STATUS_CALCULATED,
            PayrollCalculationTask::STATUS_CALCULATION_FAILED,
        ];
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATING);
    }

    public function testUpdateCalculationStatusToCalculated()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => PayrollCalculationTask::STATUS_CALCULATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATED);
    }

    public function testUpdateCalculationStatusToCalculatedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollCalculationTask::STATUS_CALCULATION_QUEUED,
            PayrollCalculationTask::STATUS_CALCULATED,
            PayrollCalculationTask::STATUS_CALCULATION_FAILED,
        ];
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATED);
    }

    public function testUpdateCalculationStatusToCalculationFailed()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => PayrollCalculationTask::STATUS_CALCULATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_FAILED);
    }

    public function testUpdateCalculationStatusToCalculationFailedInvalid()
    {
        $payrollId = 999;
        $jobId = PayrollCalculationTask::ID_PREFIX . $payrollId . ':' . uniqid();
        $invalidCurrentStatus = [
            PayrollCalculationTask::STATUS_CALCULATION_QUEUED,
            PayrollCalculationTask::STATUS_CALCULATED,
            PayrollCalculationTask::STATUS_CALCULATION_FAILED,
        ];
        $task = new PayrollCalculationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['calculation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateCalculationStatus(PayrollCalculationTask::STATUS_CALCULATION_FAILED);
    }
}
