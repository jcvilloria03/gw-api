<?php

namespace Tests\Payroll;

use App\Payroll\PayslipGenerationTask;
use App\Payroll\PayrollTaskException;
use Illuminate\Support\Facades\Redis;

class PayslipGenerationTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $task = new PayslipGenerationTask();

        $this->assertInstanceOf(PayslipGenerationTask::class, $task);
    }

    public function testCreateNoJobId()
    {
        $payrollId = 999;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hMSet')
            ->once()
            ->shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $task->create($payrollId);

        $idPrefix = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $this->assertTrue(substr($task->getId(), 0, strlen($idPrefix)) === $idPrefix);
    }

    public function testCreateNoJobIdPayslipGenerationOngoing()
    {
        $payrollId = 999;
        $invalidCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATING,
            PayslipGenerationTask::STATUS_GENERATION_QUEUED,
        ];

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task = new PayslipGenerationTask();

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId);
    }

    public function testCreateWithJobIdExistingJob()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);

        $this->assertEquals($task->getId(), $jobId);
    }

    public function testCreateWithJobIdNonExistingJob()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
    }

    public function testIsPayrollIdSameValidNewJob()
    {
        $payrollId = 999;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hMSet')
            ->once()
            ->shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $task->create($payrollId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameValidExistingJob()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['a' => 'b']);

        $task->create($payrollId, $jobId);
        $this->assertTrue($task->isPayrollIdSame());
    }

    public function testIsPayrollIdSameInvalid()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . 1;

        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->never();

        $this->setExpectedException(PayrollTaskException::class);
        $task->create($payrollId, $jobId);
        $this->assertFalse($task->isPayrollIdSame());
    }

    public function testUpdateGenerationStatusInvalidStatus()
    {
        $task = new PayslipGenerationTask();
        $this->setExpectedException(PayrollTaskException::class);
        $task->updateGenerationStatus('invalid status');
    }

    public function testUpdateGenerationStatusPersonal()
    {
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_QUEUED);
    }

    public function testUpdateGenerationStatusPayroll()
    {
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_QUEUED);
    }

    public function testUpdateGenerationStatusToQueued()
    {
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_QUEUED);
    }

    public function testUpdateGenerationStatusToQueuedInvalid()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => PayslipGenerationTask::STATUS_GENERATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_QUEUED);
    }

    public function testUpdateGenerationStatusToGenerating()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => PayslipGenerationTask::STATUS_GENERATION_QUEUED]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATING);
    }

    public function testUpdateGenerationStatusToGeneratingInvalid()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $invalidCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATING,
            PayslipGenerationTask::STATUS_GENERATED,
            PayslipGenerationTask::STATUS_GENERATION_FAILED,
        ];
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATING);
    }

    public function testUpdateGenerationStatusToCalculated()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => PayslipGenerationTask::STATUS_GENERATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATED);
    }

    public function testUpdateGenerationStatusToCalculatedInvalid()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $invalidCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATION_QUEUED,
            PayslipGenerationTask::STATUS_GENERATED,
            PayslipGenerationTask::STATUS_GENERATION_FAILED,
        ];
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATED);
    }

    public function testUpdateGenerationStatusToGenerationFailed()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => PayslipGenerationTask::STATUS_GENERATING]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->once();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_FAILED);
    }

    public function testUpdateGenerationStatusToGenerationFailedInvalid()
    {
        $payrollId = 999;
        $jobId = PayslipGenerationTask::ID_PREFIX . $payrollId;
        $invalidCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATION_QUEUED,
            PayslipGenerationTask::STATUS_GENERATED,
            PayslipGenerationTask::STATUS_GENERATION_FAILED,
        ];
        $task = new PayslipGenerationTask();

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task->create($payrollId, $jobId);

        Redis::shouldReceive('hSet')
            ->never();

        $task->updateGenerationStatus(PayslipGenerationTask::STATUS_GENERATION_FAILED);
    }

    public function testCanGenerateNoJobExists()
    {
        $payrollId = 999;

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([]);

        $task = new PayslipGenerationTask();
        $this->assertTrue($task->canGenerate($payrollId));
    }

    public function testCanGeneratePayslips()
    {
        $payrollId = 999;
        $validCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATED,
            PayslipGenerationTask::STATUS_GENERATION_FAILED,
        ];

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $validCurrentStatus[array_rand($validCurrentStatus)]]);

        $task = new PayslipGenerationTask();
        $this->assertTrue($task->canGenerate($payrollId));
    }

    public function testCanGeneratePayslipsStillGenerating()
    {
        $payrollId = 999;
        $invalidCurrentStatus = [
            PayslipGenerationTask::STATUS_GENERATING,
            PayslipGenerationTask::STATUS_GENERATION_QUEUED,
        ];

        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn(['generation_status' => $invalidCurrentStatus[array_rand($invalidCurrentStatus)]]);

        $task = new PayslipGenerationTask();
        $this->assertFalse($task->canGenerate($payrollId));
    }
}
