<?php

namespace Tests\Payroll;

use App\Payroll\PayrollAttendanceUploadTask;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollAttendanceUploadTaskTest extends TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollAttendanceUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollAttendanceUploadTask::class, $task);
    }
}
