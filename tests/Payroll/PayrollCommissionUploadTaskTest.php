<?php

namespace Tests\Payroll;

use App\Payroll\PayrollCommissionUploadTask;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PayrollCommissionUploadTaskTest extends TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new PayrollCommissionUploadTask($mockS3Client);

        $this->assertInstanceOf(PayrollCommissionUploadTask::class, $task);
    }
}
