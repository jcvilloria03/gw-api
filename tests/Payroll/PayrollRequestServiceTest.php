<?php

namespace Test\Payroll;

use App\Payroll\PayrollRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Str;

class PayrollRequestServiceTest extends TestCase
{
    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->get(1);
    }

    public function testHasGapLoanCandidates()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->hasGapLoanCandidates(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->hasGapLoanCandidates(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->hasGapLoanCandidates(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->hasGapLoanCandidates(1);
    }

    public function testGetCompanyPayrolls()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyPayrolls(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrolls(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrolls(1);
    }

    public function testGetCompanyPayrollGroupsWithPeriods()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyPayrollGroupsWithPeriods(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrollGroupsWithPeriods(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrollGroupsWithPeriods(1);
    }

    public function testCreate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->create([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->create([]);
    }

    public function testCreateSpecial()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->createSpecial([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->createSpecial([]);
    }

    public function testUpdate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->update(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->update(1, []);
    }

    public function testClose()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->close(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->close(1);
    }

    public function testDelete()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_NO_CONTENT, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_NO_CONTENT
        $response = $requestService->delete(1);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->delete(1);
    }

    public function testHasPayslips()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->hasPayslips(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->hasPayslips(1);
    }

    public function testSendPayslips()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->sendPayslips(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->sendPayslips(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->sendPayslips(1);
    }

    public function testGetGapLoanCandidates()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getGapLoanCandidates(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getGapLoanCandidates(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getGapLoanCandidates(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getGapLoanCandidates(1);
    }

    public function testGetCompanyPayrollAvailableItems()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyPayrollAvailableItems(1, 'special');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrollAvailableItems(1, 'special');

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyPayrollAvailableItems(1, 'special');
    }

    public function testAddUploadJobToPayroll()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->addUploadJobToPayroll([
            'payroll_id' => 1,
            'job_id' => uniqid(),
            'type' => 'BONUS',
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->addUploadJobToPayroll([
            'payroll_id' => 1,
            'job_id' => uniqid(),
            'type' => 'BONUS',
        ]);
    }

    public function testDeleteUploadJob()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->deleteUploadedJobToPayroll([
            'payroll_id' => 1,
            'type' => 'ATTENDANCE',
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->deleteUploadedJobToPayroll([
            'job_id' => uniqid(),
            'type' => 'ATTENDANCE',
        ]);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->deleteUploadedJobToPayroll([
            'job_id' => uniqid(),
            'type' => 'ATTENDANCE',
        ]);
    }

    public function testCalculate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_CREATED, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_CREATED
        $response = $requestService->calculate(1, []);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->calculate(1, []);
    }

    public function testGetEmployeeActiveDisbursementMethod()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $param = ['perPage' => 1, 'page' => 1, 'limit' => 3];

        // test Response::HTTP_CREATED
        $response = $requestService->getEmployeeActiveDisbursementMethod(1, $param);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        
        $param = ['page' => 1];

        //test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getEmployeeActiveDisbursementMethod(1, $param);
            
        $param = [];

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getEmployeeActiveDisbursementMethod(1, $param);
    }

    public function testExplicitBooleanValuesOnPayrollGetEndpoint()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_OK, []),
        ]);

        $handler = HandlerStack::create($mock);

        $container = [];
        $history = Middleware::history($container);
        $handler->push($history);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);
        
        $requestService->get(1, true);
        $requestService->get(1, false);
        $requestService->get(1);
            
        $this->assertTrue(str_contains($container[0]['request']->getUri()->getQuery(), 'with_employees=true'));
        $this->assertTrue(str_contains($container[1]['request']->getUri()->getQuery(), 'with_employees=false'));
        $this->assertTrue(str_contains($container[2]['request']->getUri()->getQuery(), 'with_employees=false'));
    }

    public function testGetPayrollJob()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getPayrollJob("89de77a6-51ff-4b26-87da-8644596b01b6");

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getPayrollJob("89de77a6-51ff-4b26-87da-8644596b01b6");
    }

    public function testGetDisbursementSummary()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getDisbursementSummary(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        
        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->getDisbursementSummary(1);
    }

    public function testDeleteMultiplePayroll()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $payload = ['id' => [1]];

        // test Response::HTTP_OK
        $response = $requestService->deleteMany($payload);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        
        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->deleteMany($payload);
    }

    public function testCreateMultiplePayrollRegisters()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_CREATED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $payload = ['id' => [1]];
        $user = ['first_name' => 'Maj', 'email' => 'test@example.com'];

        // test Response::HTTP_CREATED
        $response = $requestService->createMultiplePayrollRegisters($payload, $user);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        
        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->createMultiplePayrollRegisters($payload, $user);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->createMultiplePayrollRegisters($payload, $user);
    }

    public function testCreatePayrollRegisters()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_CREATED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $payload = 1;

        // test Response::HTTP_CREATED
        $response = $requestService->createPayrollRegister($payload);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        
        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->createPayrollRegister($payload);
        
        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->createPayrollRegister($payload);
    }

    public function testGetPayrollRegister()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $payload = 1;

        // test Response::HTTP_OK
        $response = $requestService->getPayrollRegister($payload);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        
        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->getPayrollRegister($payload);
    }

    public function testGetZippedPayrollRegister()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        $payload = Str::random(40);

        // test Response::HTTP_OK
        $response = $requestService->getZippedPayrollRegisterByHash($payload);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        
        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->getZippedPayrollRegisterByHash($payload);
    }

    public function testSendSinglePayslip()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PayrollRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->sendSinglePayslip(1, 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->sendSinglePayslip(1, 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $response = $requestService->sendSinglePayslip(1, 1);
    }
}
