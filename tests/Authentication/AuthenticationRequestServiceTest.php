<?php

namespace Tests\Authentication;

use App\Authentication\AuthenticationRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthenticationRequestServiceTest extends TestCase
{
    public function testSignup()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_CREATED, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AuthenticationRequestService($client);

        // test Response::HTTP_CREATED
        $response = $requestService->signUp([]);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());


        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $requestService->signUp([]);

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $requestService->signUp([]);
    }

    public function testLogin()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AuthenticationRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->login([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $requestService->login([]);

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $requestService->login([]);
    }

    public function testVerifyResend()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AuthenticationRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->verifyResend([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $requestService->verifyResend([]);

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $requestService->verifyResend([]);
    }
}
