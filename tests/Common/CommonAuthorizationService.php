<?php

namespace Tests\Common;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use Tests\Authorization\AuthorizationServiceTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexityiu7)
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class CommonAuthorizationService extends TestCase
{
    use AuthorizationServiceTestTrait;

    protected $authorizationServiceClass;
    protected $authorizationService;

    public function setUp()
    {
        parent::setUp();

        if (!$this->authorizationServiceClass) {
            throw new \Exception('Please set $authorizationServiceClass property!');
        }
        $this->authorizationService = app()->make($this->authorizationServiceClass);
    }

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetCommonDetails, $user));
    }

    public function testGetAllPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testGetAllInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetAll($targetCommonDetails, $user)
        );
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetCommonDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->updateTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetCommonDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->deleteTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetCommonDetails, $user));
    }

    public function testIsNameAvailablePassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailablePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailablePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailableNoScope()
    {
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailableInvalidAccountScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailableInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }

    public function testIsNameAvailableInvalidOtherScope()
    {
        $taskScope = new TaskScopes($this->authorizationService->createTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeIsNameAvailable($targetCommonDetails, $user));
    }
}
