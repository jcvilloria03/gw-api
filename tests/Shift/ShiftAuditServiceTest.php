<?php

namespace Tests\Shift;

use App\Audit\AuditService;
use App\Shift\ShiftAuditService;
use App\User\UserRequestService;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ShiftAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'schedule_id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'schedule' => [
                'id' => 2
            ],
            'dates' => [
                '2017-12-10',
                '2017-12-11'
            ]
        ]);
        $item = [
            'action' => ShiftAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();
        $mockUserRequestService = $this->getMockUserRequestService();

        $shiftAuditService = new ShiftAuditService($mockAuditService, $mockUserRequestService);
        $shiftAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'schedule_id' => 1,
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'schedule' => [
                'id' => 2
            ],
            'dates' => [
                '2017-12-10',
                '2017-12-11'
            ]
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'schedule_id' => 1,
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'schedule' => [
                'id' => 2
            ],
            'dates' => [
                '2017-12-10',
                '2017-12-11'
            ]
        ]);
        $item = [
            'action' => ShiftAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();
        $mockUserRequestService = $this->getMockUserRequestService();

        $shiftAuditService = new ShiftAuditService($mockAuditService, $mockUserRequestService);
        $shiftAuditService->logUpdate($item);
    }

    public function testLogUnassign()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'schedule_id' => 1,
            'employee' => [
                'first_name' => 'Test name',
                'last_name' => 'Test last name'
            ],
            'schedule' => [
                'id' => 2
            ],
            'dates' => [
                '2017-12-10',
                '2017-12-11'
            ]
        ]);
        $item = [
            'action' => ShiftAuditService::ACTION_UNASSIGN,
            'user' => $user,
            'new' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();
        $mockUserRequestService = $this->getMockUserRequestService();

        $shiftAuditService = new ShiftAuditService($mockAuditService, $mockUserRequestService);
        $shiftAuditService->logUnassign($item);
    }

    private function getMockUserRequestService()
    {
        $mockUserRequestService = m::mock(UserRequestService::class);
        $response = new JsonResponse();
        $response->setdata(json_encode([
            'id' => 1,
            'name' => 'userName',
            'account_id' => 2
        ]));
        $mockUserRequestService->shouldReceive('get')
            ->andReturn($response);

        return $mockUserRequestService;
    }
}
