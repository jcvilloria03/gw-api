<?php

namespace Tests\Shift;

use App\Permission\TaskScopes;
use App\Shift\EssShiftsAuthorizationService;
use PHPUnit\Framework\TestCase;

class EssShiftsAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeGetEmployeeShifts()
    {
        $userId = 1;
        $taskScope = new TaskScopes(EssShiftsAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Shift\EssShiftsAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($userId));
    }

    public function testFailToAuthorizeGetEmployeeShifts()
    {
        $userId = 1;
        $authorizationService = $this->createMockAuthorizationService(
            'App\Shift\EssShiftsAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($userId));
    }
}
