<?php

namespace Tests;

class BasePathTest extends TestCase
{
    /**
     * @test
     *
     * Base Path should load API Swagger UI documentation
     *
     */
    public function appRootShouldGoToApiDocs()
    {
        $this->get('/'.env('L5_SWAGGER_BASE_URL'));

        $view = $this->response->getContent();

        //check that we are displaying the API documentation
        $data = $view->getData();
        $this->assertArrayHasKey('urlToDocs', $data);
        $this->assertArrayHasKey('validatorUrl', $data);
        $this->assertNull($data['validatorUrl']);
    }
}
