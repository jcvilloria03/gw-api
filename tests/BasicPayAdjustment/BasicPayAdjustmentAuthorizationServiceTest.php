<?php

namespace Tests\BasicPayAdjustment;

use Tests\Common\CommonAuthorizationService;

class BasicPayAdjustmentAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\BasicPayAdjustment\BasicPayAdjustmentAuthorizationService::class;
}
