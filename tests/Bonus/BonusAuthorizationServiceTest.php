<?php

namespace Tests\Bonus;

use Tests\Common\CommonAuthorizationService;

class BonusAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Bonus\BonusAuthorizationService::class;
}
