<?php

namespace Tests\NightShift;

use App\Audit\AuditService;
use App\NightShift\NightShiftAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class NightShiftAuditServiceTest extends TestCase
{
    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'new name'
        ]);
        $item = [
            'action' => NightShiftAuditService::ACTION_UPDATE,
            'user' => $user,
            'old' => $oldData,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $projectAuditService = new NightShiftAuditService($mockAuditService);
        $projectAuditService->logUpdate($item);
    }
}
