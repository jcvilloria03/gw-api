<?php

namespace Tests\NightShift;

use App\NightShift\NightShiftAuthorizationService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class NightShiftAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetNightShiftDetails, $user));
    }

    public function testGetCompanyNightShiftPassAccountLevel()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            null
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testGetCompanyNightShiftInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse(
            $authorizationService->authorizeGetCompanyNightShift($targetNightShiftDetails, $user)
        );
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(NightShiftAuthorizationService::UPDATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetNightShiftDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            NightShiftAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetNightShiftDetails, $user));
    }
}
