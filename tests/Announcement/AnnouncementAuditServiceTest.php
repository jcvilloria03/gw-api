<?php

namespace Tests\Announcement;

use App\Audit\AuditService;
use App\Announcement\AnnouncementAuditService;
use \Mockery as m;
use PHPUnit\Framework\TestCase;

class AnnouncementAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $announcement = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name',
            'description' => 'description',
        ]);
        $item = [
            'action' => AnnouncementAuditService::ACTION_CREATE,
            'user' => $user,
            'announcement' => $announcement
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $announcementAuditService = new AnnouncementAuditService($mockAuditService);
        $announcementAuditService->logCreate($item);
    }

    public function testLogRecipientSawAnnouncement()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $announcementRecipient = json_encode([
            'id' => 1,
            'announcement_id' => 1,
            'recipient_id' => 1,
            'seen' => true,
            'ip_address' => '192.168.10.1',
            'recipient' => [
                'employee_id' => 651,
                'user_id' => 6,
                'name' => 'J Wallace Bird'
            ],
            'seen_at' => '12:25 March 13, 2018'
        ]);

        $item = [
            'action' => AnnouncementAuditService::ACTION_RECIPIENT_SEEN,
            'user' => $user,
            'announcement_recipient' => $announcementRecipient
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $announcementAuditService = new AnnouncementAuditService($mockAuditService);
        $announcementAuditService->log($item);
    }

    public function testLogSenderSawAnnouncementResponse()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $announcementResponse = json_encode([
            'id' => 1,
            'announcement_id' => 1,
            'sender_id' => 1,
            'message' => 'response message',
            'seen' => true,
            'sender' => [
                'employee_id' => 1,
                'user_id' => 1,
                'name' => 'J Wallace Bird'
            ],
            'created_at' => '12:25 March 13, 2018'
        ]);

        $item = [
            'action' => AnnouncementAuditService::ACTION_RESPONSE_SEEN,
            'user' => $user,
            'announcement_response' => $announcementResponse
        ];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $announcementAuditService = new AnnouncementAuditService($mockAuditService);
        $announcementAuditService->log($item);
    }

    public function testSendReplyCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1,
            'employee_company_id' => 1
        ]);
        $reply = json_encode([
            'id' => 1,
            'sender_id' => 1,
            'message' => 'test message'
        ]);
        $item = [
            'action' => AnnouncementAuditService::ACTION_SEND_REPLY,
            'user' => $user,
            'reply' => $reply
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $departmentAuditService = new AnnouncementAuditService($mockAuditService);
        $departmentAuditService->log($item);
    }
}
