<?php

namespace Tests\Announcement;

use Tests\Common\CommonAuthorizationService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;

class AnnouncementAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Announcement\AnnouncementAuthorizationService::class;

    public function testSenderViewPassAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);

        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertTrue($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::COMPANY, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            null
        );
        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $role = (object) [
            'sender' => 1,
            'recipient' => null
        ];

        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidRoleCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => null,
            'recipient' => 1
        ];

        $scope = new Scope(TargetType::COMPANY, [1]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidRoleAccountLevel()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);

        $accountId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => null,
            'recipient' => 1
        ];

        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }

    public function testSenderViewInvalidRoleCompanyLevelAll()
    {
        $taskScope = new TaskScopes($this->authorizationService->viewTask);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $targetCommonDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $role = (object) [
            'sender' => null,
            'recipient' => 1
        ];

        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            $this->authorizationServiceClass,
            $taskScope
        );

        $this->assertFalse($authorizationService->authorizeSenderView($targetCommonDetails, $user, $role));
    }
}
