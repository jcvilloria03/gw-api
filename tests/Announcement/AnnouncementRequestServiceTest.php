<?php

namespace Tests\Allowance;

use App\Announcement\AnnouncementRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AnnouncementRequestServiceTest extends TestCase
{
    public function testGetCompanyAnnouncements()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyAnnouncements(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyAnnouncements(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyAnnouncements(1, []);
    }

    public function testCreate()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->create([]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->create([]);
    }

    public function testGet()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->get(1, 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->get(1, 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->get(1, 1);
    }

    public function testReply()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        $response = $requestService->reply(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->reply(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->reply(1, []);
    }

    public function testDownloadCompanyAnnouncements()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->downloadCompanyAnnouncementsCsv(1, [1]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->downloadCompanyAnnouncementsCsv(1, [1]);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->downloadCompanyAnnouncementsCsv(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->downloadCompanyAnnouncementsCsv(1, [null]);
    }

    public function testGetUserRoleForAnnouncement()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getUserRoleForAnnouncement(1, 1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getUserRoleForAnnouncement(1, 1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getUserRoleForAnnouncement(1, 1);
    }

    public function testMarkAnnouncementAsSeenByRecipient()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AnnouncementRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->updateAnnouncementAsSeen(1, 1, '192.168.1.1');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->updateAnnouncementAsSeen(1, 1, '192.168.1.1');

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->updateAnnouncementAsSeen(1, 1, '192.168.1.1');
    }
}
