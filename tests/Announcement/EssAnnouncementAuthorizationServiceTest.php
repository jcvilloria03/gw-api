<?php

namespace Tests\Announcement;

use App\Announcement\EssAnnouncementAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssAnnouncementAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeCreate()
    {
        $user = ['user_id' => 1];
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($user['user_id']));
    }

    public function testFailToAuthorizeCreate()
    {
        $user = ['user_id' => 1];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($user['user_id']));
    }

    public function testAuthorizeSingleView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 1;
        $role->recipient = 0;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSingleView($user['user_id'], $role));
    }

    public function testFailNotSubscribedToAuthorizeSingleView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 0;
        $role->recipient = 0;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSingleView($user['user_id'], $role));
    }

    public function testFailToAuthorizeSingleView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 1;
        $role->recipient = 0;
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeSingleView($user['user_id'], $role));
    }

    public function testAuthorizeView()
    {
        $user = ['user_id' => 1];
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($user['user_id']));
    }

    public function testFailToAuthorizeView()
    {
        $user = ['user_id' => 1];
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($user['user_id']));
    }

    public function testAuthorizeSenderView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 1;
        $role->recipient = 0;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeSenderView($user['user_id'], $role));
    }

    public function testFailNotSubscribedToAuthorizeSenderView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 0;
        $role->recipient = 0;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeSenderView($user['user_id'], $role));
    }

    public function testFailToAuthorizeSenderView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 1;
        $role->recipient = 0;
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeSenderView($user['user_id'], $role));
    }

    public function testAuthorizeRecipientView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 0;
        $role->recipient = 1;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeRecipientView($user['user_id'], $role));
    }

    public function testFailNotSubscribedToAuthorizeRecipientView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 0;
        $role->recipient = 0;
        $taskScope = new TaskScopes(EssAnnouncementAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeRecipientView($user['user_id'], $role));
    }

    public function testFailToAuthorizeRecipientView()
    {
        $user = ['user_id' => 1];
        $role = new \stdClass();
        $role->sender = 0;
        $role->recipient = 1;
        $authorizationService = $this->createMockAuthorizationService(
            'App\Announcement\EssAnnouncementAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeRecipientView($user['user_id'], $role));
    }
}
