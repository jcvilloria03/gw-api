<?php

namespace Tests\Tag;

use App\Tag\TagRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TagRequestServiceTest extends TestCase
{
    public function testGetCompanyTags()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new TagRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getCompanyTags(1);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->getCompanyTags(1);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getCompanyTags(1);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->getCompanyTags(1);
    }
}
