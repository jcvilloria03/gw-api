<?php

namespace Tests\Traits;

use App\Traits\DateTrait;
use PHPUnit\Framework\TestCase;

class DateTraitTest extends TestCase
{
    use DateTrait;

    public function testGenerateDateRange()
    {
        $startDate = '2018-10-01';
        $endDate = '2018-10-10';

        $expected = [
            '2018-10-01',
            '2018-10-02',
            '2018-10-03',
            '2018-10-04',
            '2018-10-05',
            '2018-10-06',
            '2018-10-07',
            '2018-10-08',
            '2018-10-09',
            '2018-10-10',
        ];

        $result = $this->generateDateRange($startDate, $endDate);

        $this->assertEquals($result, $expected);
    }

    public function testGenerateDateRangeWhenEndIsBeforeStartDate()
    {
        $startDate = '2018-10-10';
        $endDate = '2018-10-01';

        $expected = [
            '2018-10-01',
            '2018-10-02',
            '2018-10-03',
            '2018-10-04',
            '2018-10-05',
            '2018-10-06',
            '2018-10-07',
            '2018-10-08',
            '2018-10-09',
            '2018-10-10',
        ];

        $result = $this->generateDateRange($startDate, $endDate);

        $this->assertEquals($result, $expected);
    }
}
