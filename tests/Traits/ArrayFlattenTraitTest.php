<?php

namespace Tests\Traits;

use App\Traits\ArrayFlattenTrait;
use PHPUnit\Framework\TestCase;

class ArrayFlattenTraitTest extends TestCase
{
    use ArrayFlattenTrait;

    public function testArrayFlatten()
    {
        $inputArray = [
            1,
            [
                2,
                [3]
            ],
            4
        ];

        $expected = [
            1,
            2,
            3,
            4
        ];

        $result = $this->arrayFlatten($inputArray);

        $this->assertEquals($result, $expected);
    }
}
