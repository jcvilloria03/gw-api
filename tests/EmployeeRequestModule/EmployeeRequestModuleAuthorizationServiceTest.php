<?php

namespace Tests\EmployeeRequestModule;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\EmployeeRequestModule\EmployeeRequestModuleAuthorizationService;
use Tests\Authorization\AuthorizationServiceTestTrait;
use PHPUnit\Framework\TestCase;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class EmployeeRequestModuleAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, SCOPE::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeRequestModuleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $tartgetEmployeeRequestModuleDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeRequestModuleAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($tartgetEmployeeRequestModuleDetails, $user));
    }
}
