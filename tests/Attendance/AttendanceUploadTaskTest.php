<?php

namespace Tests\Attendance;

use \ReflectionProperty;
use App\Attendance\AttendanceUploadTask;
use Mockery as m;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class AttendanceUploadTaskTest extends \Tests\TestCase
{
    public function testConstruct()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AttendanceUploadTask($mockS3Client);

        $this->assertInstanceOf(AttendanceUploadTask::class, $task);
    }

    public function testCreate()
    {
        $companyId = 999;
        $mockS3Client = m::mock('Aws\S3\S3Client');

        $task = new AttendanceUploadTask($mockS3Client);
        $task->create($companyId);

        $reflectedCompanyId = new ReflectionProperty(AttendanceUploadTask::class, 'companyId');
        $reflectedCompanyId->setAccessible(true);
        $actualCompanyId = $reflectedCompanyId->getValue($task);

        $reflectedcurrentValues = new ReflectionProperty(AttendanceUploadTask::class, 'currentValues');
        $reflectedcurrentValues->setAccessible(true);
        $actualCurrentValues = $reflectedcurrentValues->getValue($task);


        $this->assertEquals($companyId, $actualCompanyId);
        $this->assertNotEmpty($actualCurrentValues);
    }

    public function testGetS3Bucket()
    {
        $mockS3Client = m::mock('Aws\S3\S3Client');
        $task = new AttendanceUploadTask($mockS3Client);

        $this->assertNotEmpty($task->getS3Bucket());
    }

    public function testSaveAttendanceInfo()
    {
        $companyId = 999;

        $mockS3Client = m::mock('Aws\S3\S3Client');
        $mockS3Client
            ->shouldReceive('putObject')
            ->once()
            ->andReturn(true);
        $task = new AttendanceUploadTask($mockS3Client);

        $task->create($companyId);
        $s3Key = $task->saveAttendanceInfo('testPath');

        $this->assertNotEmpty($s3Key);
        $s3Prefix =  'time_attendance_import:' . $companyId . ':';
        $this->assertTrue(substr($s3Key, 0, strlen($s3Prefix)) === $s3Prefix);
    }
}
