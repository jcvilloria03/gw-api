<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\ClockStateAttendanceTriggerService;
use Mockery;
use PHPUnit\Framework\TestCase;

class ClockStateAttendanceTriggerServiceTest extends TestCase
{
    public function testClockStateAttendanceTriggerServiceClockIn()
    {
        $clockStateData = [
            'employee_id' => 1,
            'timestamp' => '2018-11-30 10:50:00',
            'state' => true
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $shiftAttendanceTriggerService = Mockery::mock(
            ClockStateAttendanceTriggerServiceTest::class . '[calculateAttendance]',
            [
                $mockAttendanceRequestService
            ]
        );

        $expectedResponse = [
            'item' => [
                'company_uid' => ['S' => [1]],
                'employee_uid' => ['N' => 703],
                'timestamp' => ['N' => 1543575000],
                'state' => [
                    'BOOL' => true,
                    'tags' => [
                        'L' => [
                            ['S' => 'trigger service response']
                        ]
                    ]
                ]
            ],
            'job_id' => 'e2640193-7ecf-4718-b773-74d7a532b41f'
        ];

        $shiftAttendanceTriggerService
            ->shouldReceive('calculateAttendance')
            ->once()
            ->with($clockStateData)
            ->andReturn($expectedResponse);

        $response = $shiftAttendanceTriggerService->calculateAttendance($clockStateData);
        $this->assertEquals($response, $expectedResponse);
    }

    public function testClockStateAttendanceTriggerServiceClockOut()
    {
        $clockStateData = [
            'employee_id' => 1,
            'timestamp' => '2018-11-30 10:55:00',
            'state' => false
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $shiftAttendanceTriggerService = Mockery::mock(
            ClockStateAttendanceTriggerServiceTest::class . '[calculateAttendance]',
            [
                $mockAttendanceRequestService
            ]
        );

        $expectedResponse = [
            'item' => [
                'company_uid' => ['S' => [1]],
                'employee_uid' => ['N' => 703],
                'timestamp' => ['N' => 1543574595],
                'state' => [
                    'BOOL' => false,
                    'tags' => [
                        'L' => [
                            ['S' => 'trigger service response']
                        ]
                    ]
                ]
            ],
            'job_id' => '18905901-231a-4000-a3be-c5d3df6a801c'
        ];

        $shiftAttendanceTriggerService
            ->shouldReceive('calculateAttendance')
            ->once()
            ->with($clockStateData)
            ->andReturn($expectedResponse);

        $response = $shiftAttendanceTriggerService->calculateAttendance($clockStateData);
        $this->assertEquals($response, $expectedResponse);
    }
}
