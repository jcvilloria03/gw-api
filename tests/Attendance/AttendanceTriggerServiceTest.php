<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\AttendanceTriggerService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;

class AttendanceTriggerServiceTest extends TestCase
{
    public function testValidCalculateAttendanceRecordsForEmployeeOnADay()
    {
        $employeeId = 1;
        $date = '2018-10-10';
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('calculateAttendanceRecord')
            ->with($employeeId, $date)
            ->once();

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);

        $attendanceTrigger->calculateAttendanceRecordsForEmployeeOnADay(
            $employeeId,
            $date,
            $companyId
        );
    }

    public function testInvalidCalculateAttendanceRecordsForEmployeeOnADay()
    {
        $employeeId = 1;
        $date = '2018-10-10';
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('calculateAttendanceRecord')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);

        $result = $attendanceTrigger->calculateAttendanceRecordsForEmployeeOnADay(
            $employeeId,
            $date,
            $companyId
        );

        $this->assertNull($result);
    }

    public function testValidCalculateAttendanceRecordsForEmployeeOnDates()
    {
        $employeeId = 1;
        $dates = ['2018-10-10', '2018-10-11'];
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [$employeeId],
                    'dates' => $dates,
                    'company_id' => $companyId
                ]
            )
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode([
                        'data' => ['job_id' => '1']
                    ])
                )
            );

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);
        $attendanceTrigger->calculateAttendanceRecordsForEmployeeOnDates(
            $employeeId,
            $dates,
            $companyId
        );
    }

    public function testInvalidCalculateAttendanceRecordsForEmployeeOnDates()
    {
        $employeeId = 1;
        $dates = ['2018-10-10', '2018-10-11'];
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);

        $result = $attendanceTrigger->calculateAttendanceRecordsForEmployeeOnDates(
            $employeeId,
            $dates,
            $companyId
        );

        $this->assertNull($result);
    }

    public function testValidCalculateAttendanceRecordsForMultipleEmployeesOnDates()
    {
        $employeeIds = [1, 2];
        $dates = ['2018-10-10', '2018-10-11'];
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => $employeeIds,
                    'dates' => $dates,
                    'company_id' => $companyId
                ]
            )
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode([
                        'data' => ['job_id' => 1]
                    ])
                )
            );

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);

        $attendanceTrigger->calculateAttendanceRecordsForMultipleEmployeesOnDates(
            $employeeIds,
            $dates,
            $companyId
        );
    }

    public function testInvalidCalculateAttendanceRecordsForMultipleEmployeesOnDates()
    {
        $employeeIds = [1, 2];
        $dates = ['2018-10-10', '2018-10-11'];
        $companyId = 1;

        $serviceMock = Mockery::mock(AttendanceRequestService::class);

        $serviceMock
            ->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->andThrow(new HttpException(Response::HTTP_NOT_ACCEPTABLE));

        $attendanceTrigger = new AttendanceTriggerService($serviceMock);

        $result = $attendanceTrigger->calculateAttendanceRecordsForMultipleEmployeesOnDates(
            $employeeIds,
            $dates,
            $companyId
        );

        $this->assertNull($result);
    }
}
