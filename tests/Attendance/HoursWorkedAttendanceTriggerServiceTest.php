<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\HoursWorkedAttendanceTriggerService;
use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;

class HoursWorkedAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendanceRecords()
    {
        $dataRequest = ['data' => [
            [
                'employee_id' => 1,
                'date' => '2018-10-08',
                'shift_id' => '1',
                'hours_worked' => [
                    'time' => '08:00',
                    'type' => 'regular'
                ]
            ],
            [
                'employee_id' => 1,
                'date' => '2018-10-09',
                'shift_id' => '1',
                'hours_worked' => [
                    'time' => '08:00',
                    'type' => 'regular'
                ]
            ],
            [
                'employee_id' => 2,
                'date' => '2018-10-08',
                'shift_id' => '1',
                'hours_worked' => [
                    'time' => '08:00',
                    'type' => 'regular'
                ]
            ],
            [
                'employee_id' => 2,
                'date' => '2018-10-09',
                'shift_id' => '1',
                'hours_worked' => [
                    'time' => '08:00',
                    'type' => 'regular'
                ]
            ],
            [
                'employee_id' => 2,
                'date' => '2018-10-10',
                'shift_id' => '1',
                'hours_worked' => [
                    'time' => '08:00',
                    'type' => 'regular'
                ]
            ]
        ]];
        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [1],
                    'dates' => ['2018-10-08', '2018-10-09']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([1])));
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [2],
                    'dates' => ['2018-10-08', '2018-10-09', '2018-10-10']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([2])));

        $service = new HoursWorkedAttendanceTriggerService($mockAttendanceRequestService);

        $service->calculateAttendanceRecords(
            $dataRequest
        );
    }
}
