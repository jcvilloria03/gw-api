<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\HolidayAttendanceTriggerService;
use App\Ess\EssEmployeeRequestService;
use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;

class HolidayAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendanceRecordsByCompany()
    {
        $dataRequest = [[
            'id' => 1,
            'name' => 'fafafafafafa',
            'type' => 'Special',
            'company_id' => 1,
            'date' => '2018-10-08',
            'repeat_until' => '2018-10-10',
            'affected_employees' => [
                (object) [
                    'id' => '',
                    'name' => 'All departments',
                    'type' => 'department'
                ]
            ]
        ]];
        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [1, 2],
                    'dates' => ['2018-10-08', '2018-10-09', '2018-10-10']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([1, 2])));

        $mockEssEmployeeRequestService = Mockery::mock(EssEmployeeRequestService::class);
        $mockEssEmployeeRequestService->shouldReceive('getEntitledEmployees')
            ->with($dataRequest[0]['affected_employees'], $dataRequest[0]['company_id'])
            ->andReturn(
                new JsonResponse(
                    json_encode(
                        [
                            'data' => [
                                ['id' => 1, 'company_id' => 1],
                                ['id' => 2, 'company_id' => 1]
                            ]
                        ]
                    )
                )
            );

        $service = new HolidayAttendanceTriggerService($mockAttendanceRequestService, $mockEssEmployeeRequestService);

        $service->calculateAttendanceRecordsByCompany(
            $dataRequest
        );
    }
}
