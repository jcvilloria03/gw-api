<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AttendanceRequestServiceTest extends TestCase
{
    public function testSearchAttendanceRecords()
    {
        $query = [
            "ids" => [5507],
            "company_id" => 1,
            "start_date" => "2018-01-01",
            "end_date" => "2018-01-01",
            "page" => 1,
            "page_size" => 22,
            "search_terms" => [
                "employee_uid" => 5507,
                "employee_name" => "Naomi Jordan",
                "expected_shift" => "morning"
            ],
            "filters" => [
                "location" => [1],
                "department" => [332],
                "position" => [4234]
            ],
            "sort_by" => [
                "date" => "ASC",
                "employee_name" => "ASC",
                "employee_uid" => "ASC",
            ]
        ];

        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->searchAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->searchAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->searchAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testExportAttendanceRecords()
    {
        $query = [
            "ids" => [5507],
            "company_id" => 1,
            "start_date" => "2018-01-01",
            "end_date" => "2018-01-01",
            "page" => 1,
            "page_size" => 22,
            "search_terms" => [
                "employee_uid" => 5507,
                "employee_name" => "Naomi Jordan",
                "expected_shift" => "morning"
            ],
            "filters" => [
                "location" => [1],
                "department" => [332],
                "position" => [4234]
            ],
            "sort_by" => [
                "date" => "ASC",
                "employee_name" => "ASC",
                "employee_uid" => "ASC",
            ]
        ];

        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->exportAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->exportAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->exportAttendanceRecords($query);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testGetSingleAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSingleAttendanceRecord(1, '2018-01-01');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getSingleAttendanceRecord(999, '1971-01-01');
    }

    public function testGetJobDetails()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getJobDetails('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getJobDetails('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $requestService->getJobDetails('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGetJobErrors()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getJobErrors('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getJobErrors('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $requestService->getJobErrors('a1733799-c44a-4086-97e0-5e6c4f9603cd');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGetSingleAttendanceRecordLockStatus()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->getSingleAttendanceRecordLockStatus(1, '2018-01-01');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->getSingleAttendanceRecordLockStatus(999, '1971-01-01');
    }

    public function testCalculateAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_ACCEPTED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_FORBIDDEN, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_ACCEPTED
        $response = $requestService->calculateAttendanceRecord(1, '2018-01-01');
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->calculateAttendanceRecord(999, '1971-01-01');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_FORBIDDEN
        $this->expectException(HttpException::class);
        $response = $requestService->calculateAttendanceRecord(23, '2018-01-12');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testLockAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->lockAttendanceRecord(1, '2018-01-01');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->lockAttendanceRecord(999, '1971-01-01');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testUnlockAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->unlockAttendanceRecord(1, '2018-01-01');
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->unlockAttendanceRecord(999, '1971-01-01');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testBulkUnlockAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
        ]);

        $data = [
            'data' => [
                [
                    'employee_uid' => 122,
                    'date' => '2018-01-01',
                ],
                [
                    'employee_uid' => 34,
                    'date' => '2018-01-11',
                ],
            ]
        ];

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->bulkUnlockAttendanceRecord($data);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->bulkUnlockAttendanceRecord($data);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testBulkLockAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
        ]);

        $data = [
            'data' => [
                [
                    'employee_uid' => 122,
                    'date' => '2018-01-01',
                ],
                [
                    'employee_uid' => 34,
                    'date' => '2018-01-11',
                ],
            ]
        ];

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->bulkLockAttendanceRecord($data);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->bulkLockAttendanceRecord($data);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testEditAttendanceRecord()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
        ]);

        $employeeId = 12;
        $attendanceDate = '2018-01-12';
        $data = [
            'RH' => 480,
            'UH' => 480,
        ];

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->editAttendanceRecord($employeeId, $attendanceDate, $data);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->editAttendanceRecord($employeeId, $attendanceDate, $data);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->editAttendanceRecord($employeeId, $attendanceDate, $data);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testBulkAttendanceCalculateByEmployees()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_ACCEPTED, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_METHOD_NOT_ALLOWED, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, []),
        ]);

        $data = [
            'employee_ids' => [1, 22, 232],
            'dates' => ['2018-01-01'],
        ];

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_ACCEPTED
        $response = $requestService->bulkAttendanceCalculateByEmployees($data);
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByEmployees($data);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByEmployees($data);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_METHOD_NOT_ALLOWED
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByEmployees($data);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $response->getStatusCode());

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByEmployees($data);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }

    public function testBulkAttendanceCalculateByCompany()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_ACCEPTED, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_METHOD_NOT_ALLOWED, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        // test Response::HTTP_ACCEPTED
        $response = $requestService->bulkAttendanceCalculateByCompany(1, '2018-01-06');
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByCompany(1, '2018-01-06');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByCompany(1, '2018-01-06');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_METHOD_NOT_ALLOWED
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByCompany(1, '2018-01-06');
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $response->getStatusCode());

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $response = $requestService->bulkAttendanceCalculateByCompany(1, '2018-01-06');
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }

    public function testUploadAttendanceInfo()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_ACCEPTED, []),
            new GuzzleResponse(Response::HTTP_BAD_REQUEST, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_METHOD_NOT_ALLOWED, []),
            new GuzzleResponse(Response::HTTP_INTERNAL_SERVER_ERROR, []),
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new AttendanceRequestService($client);

        $data = [
            'company_id' => 1,
            's3_bucket' => 'test-s3-bucket',
            's3_key' => 'attendance_upload_time_attendance:1:4b3403665fea6',
        ];

        // test Response::HTTP_ACCEPTED
        $response = $requestService->uploadAttendanceInfo($data);
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());

        // test Response::HTTP_BAD_REQUEST
        $this->expectException(HttpException::class);
        $response = $requestService->uploadAttendanceInfo($data);
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $response = $requestService->uploadAttendanceInfo($data);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        // test Response::HTTP_METHOD_NOT_ALLOWED
        $this->expectException(HttpException::class);
        $response = $requestService->uploadAttendanceInfo($data);
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $response->getStatusCode());

        // test Response::HTTP_INTERNAL_SERVER_ERROR
        $this->expectException(HttpException::class);
        $response = $requestService->uploadAttendanceInfo($data);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
