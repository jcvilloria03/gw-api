<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\EmployeeRequestAttendanceTriggerService;
use Illuminate\Http\JsonResponse;
use Mockery;
use PHPUnit\Framework\TestCase;

class EmployeeRequestAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendanceForOvertimeRequest()
    {
        $overtimeRequest = [
            'company_id' => 1,
            'request_type' => 'overtime_request',
            'employee_id' => 1,
            'request' => [
                'date' => '2018-10-10'
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeRequestAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnADay]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnADay')
            ->with($overtimeRequest['employee_id'], $overtimeRequest['request']['date'])
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $service->calculateAttendance(
            $overtimeRequest
        );
    }

    public function testCalculateAttendanceForUndertimeRequest()
    {
        $undertimeRequest = [
            'request_type' => 'undertime_request',
            'employee_id' => 1,
            'request' => [
                'date' => '2018-10-10'
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeRequestAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnADay]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnADay')
            ->with($undertimeRequest['employee_id'], $undertimeRequest['request']['date'])
            ->once();

        $service->calculateAttendance(
            $undertimeRequest
        );
    }

    public function testCalculateAttendanceForLeaveRequest()
    {
        $leaveRequest = [
            'company_id' => 1,
            'request_type' => 'leave_request',
            'employee_id' => 1,
            'request' => [
                'leaves' => [
                    [
                        'date' => '2018-10-10'
                    ],
                    [
                        'date' => '2018-10-08'
                    ]
                ]
            ]
        ];

        $dates = [
            '2018-10-10',
            '2018-10-08'
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeRequestAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnDates')
            ->with($leaveRequest['employee_id'], $dates, $leaveRequest['company_id'])
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $service->calculateAttendance(
            $leaveRequest
        );
    }

    public function testCalculateAttendanceForTimeDisputeRequest()
    {
        $timeDisputeRequest = [
            'company_id' => 1,
            'request_type' => 'time_dispute_request',
            'employee_id' => 1,
            'request' => [
                'params' => [
                    'shifts' => [
                        [
                            'date' => '2018-10-10'
                        ],
                        [
                            'date' => '2018-10-08'
                        ]
                    ]
                ]
            ]
        ];

        $dates = [
            '2018-10-10',
            '2018-10-08'
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeRequestAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnDates')
            ->with($timeDisputeRequest['employee_id'], $dates, $timeDisputeRequest['company_id'])
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $service->calculateAttendance(
            $timeDisputeRequest
        );
    }

    public function testCalculateAttendanceForShiftChangeRequest()
    {
        $shiftChangeRequest = [
            'company_id' => 1,
            'request_type' => 'shift_change_request',
            'employee_id' => 1,
            'request' => [
                'params' => [
                    'shift_change_dates' => [
                        [
                            'date' => '2018-10-10'
                        ],
                        [
                            'date' => '2018-10-08'
                        ]
                    ]
                ]
            ]
        ];

        $dates = [
            '2018-10-10',
            '2018-10-08'
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeRequestAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnDates')
            ->with($shiftChangeRequest['employee_id'], $dates, $shiftChangeRequest['company_id'])
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $service->calculateAttendance(
            $shiftChangeRequest
        );
    }
}
