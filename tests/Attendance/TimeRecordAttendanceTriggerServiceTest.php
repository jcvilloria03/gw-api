<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\TimeRecordAttendanceTriggerService;
use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Http\JsonResponse;

class TimeRecordAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendanceRecordsCreated()
    {
        $dataRequest = [
            'created' => [
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538352060'],
                    'state' => ['N' => true]
                ],
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538438460'],
                    'state' => ['N' => true]
                ]
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [2],
                    'dates' => ['2018-10-01', '2018-10-02']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([2])));

        $service = new TimeRecordAttendanceTriggerService($mockAttendanceRequestService);
        $service->calculateAttendanceRecords([], $dataRequest);
    }

    public function testCalculateAttendanceRecordsDeleted()
    {
        $dataRequest = [
            'deleted' => [
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538352060'],
                    'state' => ['N' => true]
                ],
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538438460'],
                    'state' => ['N' => true]
                ]
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [2],
                    'dates' => ['2018-10-01', '2018-10-02']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([2])));

        $service = new TimeRecordAttendanceTriggerService($mockAttendanceRequestService);
        $service->calculateAttendanceRecords(
            ['delete' => [
                [
                    'employee_id' => 2,
                    'timestamp' => 1538352060
                ],
                [
                    'employee_id' => 2,
                    'timestamp' => 1538438460
                ],
            ]],
            $dataRequest
        );
    }

    public function testCalculateAttendanceRecords()
    {
        $dataRequest = [
            'created' => [
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538352060'],
                    'state' => ['N' => true]
                ]
            ],
            'deleted' => [
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538352060'],
                    'state' => ['N' => true]
                ],
                [
                    'company_uid' => ['S' => 1],
                    'employee_uid' => ['N' => 2],
                    'timestamp' => ['N' => '1538438460'],
                    'state' => ['N' => true]
                ]
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $mockAttendanceRequestService->shouldReceive('bulkAttendanceCalculateByEmployees')
            ->with(
                [
                    'employee_ids' => [2],
                    'dates' => ['2018-10-01', '2018-10-02']
                ]
            )
            ->andReturn(new JsonResponse(json_encode([2])));

        $service = new TimeRecordAttendanceTriggerService($mockAttendanceRequestService);
        $service->calculateAttendanceRecords(
            ['delete' => [
                [
                    'employee_id' => 2,
                    'timestamp' => 1538352060
                ],
                [
                    'employee_id' => 2,
                    'timestamp' => 1538438460
                ],
            ]],
            $dataRequest
        );
    }
}
