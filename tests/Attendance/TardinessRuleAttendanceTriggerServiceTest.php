<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\TardinessRuleAttendanceTriggerService;
use Mockery;
use PHPUnit\Framework\TestCase;

class TardinessRuleAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendance()
    {
        $affectedEmployees = [
            [
                'id' => null,
                'name' => 'All departments',
                'type' => 'departments'
            ],
            [
                'id' => 1,
                'name' => 'John Doe',
                'type' => 'employee'
            ],
            [
                'id' => null,
                'name' => 'All employees',
                'type' => 'employee'
            ],
        ];

        $companyId = 1;

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            TardinessRuleAttendanceTriggerServiceTest::class . '[calculateAttendance]',
            [
                $mockAttendanceRequestService
            ]
        );

        $service
            ->shouldReceive('calculateAttendance')
            ->with($affectedEmployees, $companyId)
            ->once();

        $service->calculateAttendance(
            $affectedEmployees,
            $companyId
        );
    }
}
