<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\EmployeeAttendanceTriggerService;
use App\Traits\ArrayFlattenTrait;
use App\Traits\DateTrait;
use Illuminate\Http\JsonResponse;
use Mockery;
use PHPUnit\Framework\TestCase;

class EmployeeAttendanceTriggerServiceTest extends TestCase
{
    use DateTrait, ArrayFlattenTrait;

    public function testPrepEmployeeCalculationDates()
    {
        $attributes = [
            'birth_date' => '1991-10-15',
            'date_hired' => '2017-10-15',
            'date_ended' => '2018-10-15',
        ];
        $employeeData = [
            'birth_date' => '1995-10-15',
            'date_hired' => '2016-10-15',
            'date_ended' => '2017-10-15',
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $employeeAttendanceTriggerService = new EmployeeAttendanceTriggerService($mockAttendanceRequestService);
        $dates = $employeeAttendanceTriggerService->prepEmployeeCalculationDates($attributes, $employeeData);

        $this->assertCount(2193, $dates);
    }

    public function testCalculateAttendance()
    {
        $employeeId = 1;
        $attributes = [
            'company_id' => 1,
            'birth_date' => '1991-10-15',
            'date_hired' => '2017-10-15',
            'date_ended' => '2018-10-15',
        ];
        $employeeData = [
            'birth_date' => '1995-10-15',
            'date_hired' => '2016-10-15',
            'date_ended' => '2017-10-15',
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $service = Mockery::mock(
            EmployeeAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $employeeAttendanceTriggerService = new EmployeeAttendanceTriggerService($mockAttendanceRequestService);
        $dates = $employeeAttendanceTriggerService->prepEmployeeCalculationDates($attributes, $employeeData);

        $service
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnDates')
            ->with($employeeId, $dates, $attributes['company_id'])
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $service->calculateAttendance(
            $attributes,
            $employeeData,
            $employeeId
        );
    }
}
