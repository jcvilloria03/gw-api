<?php

namespace Tests\Attendance;

use App\Attendance\AttendanceRequestService;
use App\Attendance\ShiftAttendanceTriggerService;
use Illuminate\Http\JsonResponse;
use Mockery;
use PHPUnit\Framework\TestCase;

class ShiftAttendanceTriggerServiceTest extends TestCase
{
    public function testCalculateAttendanceForShiftRequest()
    {
        $shiftUpdateRequest = [
            'company_id' => 1,
            'employee_id' => 1,
            'dates' => [
                '2018-10-10',
                '2018-10-11'
            ]
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $shiftAttendanceTriggerService = Mockery::mock(
            ShiftAttendanceTriggerService::class . '[calculateAttendanceRecordsForEmployeeOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $shiftAttendanceTriggerService
            ->shouldReceive('calculateAttendanceRecordsForEmployeeOnDates')
            ->with(
                $shiftUpdateRequest['employee_id'],
                $shiftUpdateRequest['dates'],
                $shiftUpdateRequest['company_id']
            )
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $shiftAttendanceTriggerService->calculateAttendance(
            $shiftUpdateRequest['employee_id'],
            $shiftUpdateRequest['dates'],
            $shiftUpdateRequest['company_id']
        );
    }

    public function testCalculateAttendanceForShiftRequestWithMultipleEmployees()
    {
        $emplyoeeIds = [1, 2, 3];
        $dates = [
            '2018-10-10',
            '2018-10-08'
        ];
        $companyId = 1;

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);

        $shiftAttendanceTriggerService = Mockery::mock(
            ShiftAttendanceTriggerService::class . '[calculateAttendanceRecordsForMultipleEmployeesOnDates]',
            [
                $mockAttendanceRequestService
            ]
        );

        $shiftAttendanceTriggerService
            ->shouldReceive('calculateAttendanceRecordsForMultipleEmployeesOnDates')
            ->with($emplyoeeIds, $dates, $companyId)
            ->once()
            ->andReturn(
                new JsonResponse(
                    json_encode(['job_id' => 'string'])
                )
            );

        $shiftAttendanceTriggerService->calculateAttendanceForMultipleEmployees(
            $emplyoeeIds,
            $dates,
            $companyId
        );
    }

    public function testGenerateDatesFromRange()
    {
        $startDate = '2018-10-10';
        $endDate = '2018-10-12';

        $expected = [
            '2018-10-10',
            '2018-10-11',
            '2018-10-12'
        ];

        $mockAttendanceRequestService = Mockery::mock(AttendanceRequestService::class);
        $shiftAttendanceTriggerService = new ShiftAttendanceTriggerService(
            $mockAttendanceRequestService
        );

        $result = $shiftAttendanceTriggerService->generateDateRange($startDate, $endDate);

        $this->assertEquals($expected, $result);
    }
}
