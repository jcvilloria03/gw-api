<?php

namespace Tests\CSV;

use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use Illuminate\Http\UploadedFile;
use PHPUnit\Framework\TestCase;

class CsvValidatorTest extends TestCase
{
    protected $fileName = __DIR__.'/file.csv';

    public function tearDown()
    {
        unlink($this->fileName);
    }

    private function createCsv(array $data)
    {
        $list = $data;
        $fp = fopen($this->fileName, 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    public function testValidateInvalidExtension()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
            ['1', '2', '3'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName.'.exe',
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $this->expectException(CsvValidatorException::class);
        $validator = new CsvValidator();
        $validator->validate($file);
    }

    public function testValidateValidCSV()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
            ['1', '2', '3'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $validator = new CsvValidator();
        $validator->validate($file);
    }

    public function testValidateInvalidCSV()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $this->expectException(CsvValidatorException::class);
        $validator = new CsvValidator();
        $validator->validate($file);
    }

    public function testValidateBlankHeader()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', '', 'd'],
            ['1', '2', '3', '4'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $this->expectException(CsvValidatorException::class);
        $validator = new CsvValidator();
        $validator->validate($file);
    }

    public function testValidateDuplicateHeader()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'd', 'd'],
            ['1', '2', '3', '4'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $this->expectException(CsvValidatorException::class);
        $validator = new CsvValidator();
        $validator->validate($file);
    }
}
