<?php

namespace Tests\CSV;

use PHPUnit\Framework\TestCase;
use Illuminate\Http\UploadedFile;
use App\CSV\CsvValidatorException;
use App\CSV\PersonalInfoCsvValidator;

class PersonalInfoCsvValidatorTest extends TestCase
{
    protected $fileName = __DIR__.'/file.csv';

    protected $subscriptionStats =  array(
        'total_used' => 2,
        'total_remaining' => 0,
        'products' =>
            array(
            0 =>
            array(
                'product_id' => 1,
                'product_code' => 'time and attendance',
                'product_name' => 'Time & Attendance',
                'licenses_used' => 2,
                'licenses_available' => 1,
            ),
            1 =>
            array(
                'product_id' => 2,
                'product_code' => 'payroll',
                'product_name' => 'Payroll',
                'licenses_used' => 0,
                'licenses_available' => 0,
            ),
            2 =>
            array(
                'product_id' => 3,
                'product_code' => 'time and attendance plus payroll',
                'product_name' => 'Time and attendance + Payroll',
                'licenses_used' => 0,
                'licenses_available' => 0,
            ),
            ),
    );

    public function tearDown()
    {
        unlink($this->fileName);
    }

    private function createCsv(array $data)
    {
        $list = $data;
        $fp = fopen($this->fileName, 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    public function testValidateInvalidExtension()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
            ['1', '2', '3'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName.'.exe',
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;

        $this->expectException(CsvValidatorException::class);
        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }

    public function testValidateValidCSV()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
            ['1', '2', '3'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;

        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }

    public function testValidateInvalidCSV()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;

        $this->expectException(CsvValidatorException::class);
        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }

    public function testValidateBlankHeader()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', '', 'd'],
            ['1', '2', '3', '4'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;

        $this->expectException(CsvValidatorException::class);
        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }

    public function testValidateDuplicateHeader()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'd', 'd'],
            ['1', '2', '3', '4'],
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;

        $this->expectException(CsvValidatorException::class);
        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }

    public function testValidateThrowsErrorOnNotEnoughRemainingUnits()
    {
        // write temp CSV
        $this->createCsv([
            ['a', 'b', 'c', 'd'],
            ['1', '2', '3'],
            ['1', '2', '3']
        ]);

        $file = new UploadedFile(
            $this->fileName,
            $this->fileName,
            null,
            filesize($this->fileName),
            UPLOAD_ERR_OK
        );

        $subscriptionStats = $this->subscriptionStats;
        $this->expectException(CsvValidatorException::class);
        $this->expectExceptionMessage(
            'Account does not have enough licenses. Time And Attendance has 1 available licenses.'
        );

        $validator = new PersonalInfoCsvValidator();
        $validator->validate($file, $subscriptionStats, "time and attendance");
    }
}
