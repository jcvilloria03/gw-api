<?php

namespace Tests\Account;

use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use App\Role\RoleAuthorizationService;
use PHPUnit\Framework\TestCase;

class RoleAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeGetAccountRolesPass()
    {
        $taskScope = new TaskScopes(RoleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            $taskScope
        );

        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertTrue($authorizationService->authorizeGetAccountRoles($targetPositionDetails, $user));
    }

    public function testAuthorizeGetAccountRolesNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            null
        );
        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertFalse($authorizationService->authorizeGetAccountRoles($targetPositionDetails, $user));
    }

    public function testAuthorizeGetAccountRolesInvalidScope()
    {
        $taskScope = new TaskScopes(RoleAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            $taskScope
        );
        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertFalse($authorizationService->authorizeGetAccountRoles($targetPositionDetails, $user));
    }

    public function testAuthorizeDeleteAccountRolesPass()
    {
        $taskScope = new TaskScopes(RoleAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $targetAccountId = 1;
        $scope = new Scope(TargetType::ACCOUNT, [$targetAccountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            $taskScope
        );
        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertTrue($authorizationService->authorizeDeleteAccountRoles($targetPositionDetails, $user));
    }

    public function testAuthorizeDeleteAccountRolesNoScope()
    {
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            null
        );
        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertFalse($authorizationService->authorizeDeleteAccountRoles($targetPositionDetails, $user));
    }

    public function testAuthorizeDeleteAccountRolesInvalidScope()
    {
        $taskScope = new TaskScopes(RoleAuthorizationService::DELETE_TASK);
        $taskScope->addModuleScope(['HRIS']);

        $scope = new Scope(TargetType::ACCOUNT, [3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Role\RoleAuthorizationService',
            $taskScope
        );
        $accountId = 1;
        $targetPositionDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];

        $this->assertFalse($authorizationService->authorizeDeleteAccountRoles($targetPositionDetails, $user));
    }
}
