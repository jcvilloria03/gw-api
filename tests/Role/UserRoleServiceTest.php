<?php

namespace Tests\Role;

use Mockery as m;
use App\Model\Role;
use App\Model\Task;
use App\Task\TaskService;
use App\Role\UserRoleService;
use App\Role\DefaultRoleService;
use App\User\UserRequestService;
use Illuminate\Http\JsonResponse;
use App\Permission\PermissionService;
use Illuminate\Database\Eloquent\Collection;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserRoleServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->app->instance(UserRequestService::class, m::mock(UserRequestService::class));
    }

    public function testCreateUserRole()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);

        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);
        $this->assertNotEmpty($userRole->id);
        $this->assertEquals($role->id, $userRole->role_id);
        $this->assertEquals($userId, $userRole->user_id);

        $role = $defaultRoleService->createSuperAdminRole($accountId);

        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);
        $this->assertNotEmpty($userRole->id);
        $this->assertEquals($role->id, $userRole->role_id);
        $this->assertEquals($userId, $userRole->user_id);
    }

    public function testGetUserRoles()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $role = $defaultRoleService->createSuperAdminRole($accountId);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoles = $userRoleService->getUserRoles($userId);
        $this->assertEquals(2, count($userRoles));

        foreach ($userRoles as $userRole) {
            $this->assertNotEmpty($userRole->role->id);
            $this->assertNotEmpty($userRole->role->name);
        }
    }

    public function testDeleteUserRoles()
    {
        $this->markTestSkipped(
            'Deprecated since [https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8035]'
        );
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;
        $companyId = 1213;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];

        $role = $defaultRoleService->createOnlyAdminRole($accountId, $company);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $role = $defaultRoleService->createOnlyAdminRole($accountId, $company);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoleService->deleteUserRoles($userId);
        $this->assertEmpty($userRoleService->getUserRoles($userId));
    }

    public function testGetWithConditions()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $role = $defaultRoleService->createSuperAdminRole($accountId);
        $userRole = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoles = $userRoleService->getWithConditions([
            'user_id' => $userId
        ]);
        $this->assertEquals(2, $userRoles->count());

        $userRoles = $userRoleService->getWithConditions([
            'role_id' => $role->id
        ]);
        $this->assertEquals(1, $userRoles->count());
    }

    public function testGetCompanyIdForAdminRole()
    {
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);

        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getAdminTasks')->andReturn(new Collection([$task]));
        $mockTaskService->shouldReceive('getTasksByAttributeValues')->andReturn(collect([['id' => $task->id]]));
        $this->app->instance(TaskService::class, $mockTaskService);

        $mockUserRequestService = m::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('getAccountOwnerAndSuperAdmins')
            ->andReturn(new JsonResponse(json_encode(['data' => []])));
        $this->app->instance(UserRequestService::class, $mockUserRequestService);

        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1213;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $role = $service->createAdminRole($accountId, $company);

        $service = new PermissionService();

        $userRoleService = $this->app->make(UserRoleService::class);
        $taskService = $this->app->make(TaskService::class);

        $result = $userRoleService->getCompanyIdForAdminRole([$role], $taskService);

        $this->assertEquals($companyId, $result);
    }

    public function testIsOwnerOrSuperAdmin()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);

        $userRoleService = $this->app->make(UserRoleService::class);

        $result = $userRoleService->isOwnerOrSuperAdmin([$role]);
        $this->assertEquals($accountId, $result->account_id);
    }

    public function testIsTypeAdminReturnFalse()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);

        $userRoleService = $this->app->make(UserRoleService::class);

        $result = $userRoleService->isTypeAdmin([$role]);
        $this->assertFalse($result);
    }

    public function testGetCompaniesIfUserIsOwnerAndTypeAdmin()
    {
        $user = (object) [
            'user_type' => 'owner',
            'companies' => [
                (object) [
                    'name' => 'companyName',
                    'id' => 1
                ]
            ]
        ];

        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);
        $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoleService = $this->app->make(UserRoleService::class);

        $result = $userRoleService->getCompanies($user, [$role], UserRoleService::ADMIN);

        $this->assertEquals($result[0]['company_name'], $user->companies[0]->name);
    }

    public function testGetCompaniesIfUserIsOwnerAndTypeEmployeeReturnEmptyArray()
    {
        $user = (object) [
            'user_type' => 'Owner',
            'companies' => [
                (object) [
                    'name' => 'companyName',
                    'id' => 1
                ]
            ]
        ];

        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);
        $userId = 12345;
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);
        $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoleService = $this->app->make(UserRoleService::class);

        $result = $userRoleService->getCompanies($user, [$role], UserRoleService::EMPLOYEE);

        $this->assertEmpty($result);
    }

    public function testGetTotalAssignedUsers()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $accountId = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);
        $userRoleService = $this->app->make(UserRoleService::class);

        $result = $userRoleService->getTotalAssignedUsers($accountId, $role);
        $this->assertEquals('0/0 Users', $result);
    }

    public function testUpdateUserRolesModuleAccess()
    {
        $this->markTestSkipped(
            'Deprecated since [https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8035]'
        );
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService    = $this->app->make(UserRoleService::class);

        $userId      = 12345;
        $otherUserId = 54321;
        $accountId   = 12345;

        $role = $defaultRoleService->createOwnerRole($accountId);

        $userRoleOwner = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $role = $defaultRoleService->createSuperAdminRole($accountId);

        $userRoleSuperAdmin = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $userId,
        ]);

        $userRoleDifferentUser = $userRoleService->create([
            'role_id' => $role->id,
            'user_id' => $otherUserId,
        ]);

        $userRoleService->updateUserRolesModuleAccess($userId, [Role::ACCESS_MODULE_PAYROLL]);

        $userRoleOwner = $userRoleOwner->fresh();
        $userRoleSuperAdmin = $userRoleSuperAdmin->fresh();
        $userRoleDifferentUser = $userRoleDifferentUser->fresh();

        $expected = [
            Role::ACCESS_MODULE_HRIS,
            Role::ACCESS_MODULE_PAYROLL,
        ];

        $this->assertSame($expected, $userRoleOwner->module_access);
        $this->assertSame($expected, $userRoleSuperAdmin->module_access);
        $this->assertNotSame($expected, $userRoleDifferentUser->module_access);
        $this->assertEmpty($userRoleDifferentUser->module_access);
    }
}
