<?php

namespace Tests\Role;

use App\Model\Permission;
use App\Model\Role;
use App\Model\Task;
use App\Permission\TargetType;
use App\Role\DefaultRoleService;
use App\Role\RoleService;
use App\Task\TaskService;
use App\Role\UserRoleService;
use App\User\UserRequestService;
use Mockery as m;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class DefaultRoleServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->app->instance(UserRequestService::class, m::mock(UserRequestService::class));
    }

    public function testCreateOwnerRole()
    {
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);
        $task2 = Task::create([
            'name' => 'do.something2',
            'module' => 'module2',
            'submodule' => 'submodule2',
        ]);
        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getOwnerTasks')->andReturn(new Collection([$task]));
        $mockTaskService->shouldReceive('getAdminTasks')->andReturn(new Collection([$task2]));
        $this->app->instance(TaskService::class, $mockTaskService);
        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $role = $service->createOwnerRole($accountId);
        $this->assertNotEmpty($role->id);
        $this->assertEquals($accountId, $role->account_id);
        $this->assertEquals(0, $role->company_id);
        $this->assertEquals(Role::OWNER_NAME, $role->name);

        $permissions = Permission::where('role_id', $role->id)->get();

        $this->assertEquals(2, $permissions->count());
    }

    public function testCreateSuperAdminRole()
    {
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);

        $suTask = Task::create([
            'name' => 'do.superadmin',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);

        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getSuperAdminTasks')->andReturn(new Collection([$suTask]));
        $mockTaskService->shouldReceive('getAdminTasks')->andReturn(new Collection([$task]));
        $this->app->instance(TaskService::class, $mockTaskService);
        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $role = $service->createSuperAdminRole($accountId);
        $this->assertNotEmpty($role->id);
        $this->assertEquals($accountId, $role->account_id);
        $this->assertEquals(0, $role->company_id);
        $this->assertEquals(Role::SUPERADMIN_NAME, $role->name);
    }

    public function testCreateAdminRole()
    {
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);
        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getAdminTasks')->andReturn(new Collection([$task]));
        $this->app->instance(TaskService::class, $mockTaskService);

        $mockUserRequestService = m::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('getAccountOwnerAndSuperAdmins')
            ->andReturn(new JsonResponse(json_encode(['data' => []])));

        $this->app->instance(UserRequestService::class, $mockUserRequestService);

        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $role = $service->createAdminRole($accountId, $company);
        $this->assertNotEmpty($role->id);
        $this->assertEquals($accountId, $role->account_id);
        $this->assertEquals($companyId, $role->company_id);
        $this->assertEquals(Role::ADMIN, $role->name);
        $samplePermission = $role->permissions->first();
        $scope = $samplePermission->scope;
        $this->assertEquals(TargetType::COMPANY, key($scope));
        $this->assertEquals([$companyId], $scope[key($scope)]);
    }

    public function testAssignAdminRoleToOwnerAndSuperAdmins()
    {
        $userId = 33;
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);
        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getAdminTasks')->andReturn(new Collection([$task]));
        $this->app->instance(TaskService::class, $mockTaskService);

        $mockUserRequestService = m::mock(UserRequestService::class);
        $mockUserRequestService->shouldReceive('getAccountOwnerAndSuperAdmins')
            ->andReturn(new JsonResponse(json_encode(['data' => [['id' => $userId]]])));

        $this->app->instance(UserRequestService::class, $mockUserRequestService);

        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $role = $service->createAdminRole($accountId, $company);

        $this->assertCount(1, $role->userRoles);
        $this->assertEquals($userId, $role->userRoles->first()->user_id);
    }

    public function testCreateEmployeeRole()
    {
        $task = Task::create([
            'name' => 'do.something',
            'module' => 'module',
            'submodule' => 'submodule',
        ]);
        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->shouldReceive('getEmployeeTasks')->andReturn(new Collection([$task]));
        $this->app->instance(TaskService::class, $mockTaskService);
        $service = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $role = $service->createCompanyEmployeeRole($accountId, $company);
        $this->assertNotEmpty($role->id);
        $this->assertEquals($accountId, $role->account_id);
        $this->assertEquals($companyId, $role->company_id);
        $this->assertEquals(Role::EMPLOYEE, $role->name);
        $samplePermission = $role->permissions->first();
        $scope = $samplePermission->scope;
        $this->assertEmpty($scope);
    }

    public function testGetAccountRoles()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $roleService = $this->app->make(RoleService::class);

        $accountId1 = 1;
        $accountId2 = 2;
        $accountId3 = 3;
        $defaultRoleService->createSuperAdminRole($accountId1);
        $defaultRoleService->createOwnerRole($accountId1);
        $defaultRoleService->createSuperAdminRole($accountId2);

        $this->assertEquals(2, $roleService->getRoles($accountId1)->count());
        $this->assertEquals(1, $roleService->getRoles($accountId2)->count());
        $this->assertEquals(0, $roleService->getRoles($accountId3)->count());
    }

    public function testGetOwnerRole()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);

        $accountId = 1;
        $defaultRoleService->createSuperAdminRole($accountId);
        $ownerRole = $defaultRoleService->createOwnerRole($accountId);

        $actualOwnerRole = $defaultRoleService->getOwnerRole($accountId);

        $this->assertEquals($ownerRole->id, $actualOwnerRole->id);
        $this->assertEquals($ownerRole->account_id, $actualOwnerRole->account_id);
        $this->assertEquals($ownerRole->name, $actualOwnerRole->name);
    }

    public function testGetCompanyEmployeeRole()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $employeeRole = $defaultRoleService->createCompanyEmployeeRole($accountId, $company);

        $actualEmployeeRole = $defaultRoleService->getCompanyEmployeeRole($accountId);

        $this->assertEquals($employeeRole->id, $actualEmployeeRole->id);
        $this->assertEquals($employeeRole->account_id, $actualEmployeeRole->account_id);
        $this->assertEquals($employeeRole->company_id, $actualEmployeeRole->company_id);
        $this->assertEquals($employeeRole->name, $actualEmployeeRole->name);
    }

    public function testGetCompanyEmployeeSystemRole()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $accountId = 1;
        $companyId = 1;
        $companyName = 'MyCompany';
        $company = [
            'id' => $companyId,
            'name' => $companyName
        ];
        $employeeRole = $defaultRoleService->createCompanyEmployeeRole($accountId, $company);

        $actualEmployeeRole = $defaultRoleService->getEmployeeSystemRole($accountId, $companyId);

        $this->assertEquals($employeeRole->id, $actualEmployeeRole->id);
        $this->assertEquals($employeeRole->account_id, $actualEmployeeRole->account_id);
        $this->assertEquals($employeeRole->company_id, $actualEmployeeRole->company_id);
        $this->assertEquals($employeeRole->name, $actualEmployeeRole->name);
    }

    public function testGetAccountOwners()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);

        $accountId = 1;
        $userId = 1;
        $superAdminRole = $defaultRoleService->createSuperAdminRole($accountId);
        $userRoleService->create([
            'role_id' => $superAdminRole->id,
            'user_id' => $userId,
        ]);
        $ownerRole = $defaultRoleService->createOwnerRole($accountId);
        $userRole = $userRoleService->create([
            'role_id' => $ownerRole->id,
            'user_id' => $userId,
        ]);

        $actualUserRoles = $defaultRoleService->getAccountOwners($accountId);

        $this->assertEquals(1, $actualUserRoles->count());
        $actualUserRole = $actualUserRoles->first();
        $this->assertEquals($userRole->id, $actualUserRole->id);
        $this->assertEquals($userRole->role_id, $actualUserRole->role_id);
        $this->assertEquals($userRole->user_id, $actualUserRole->user_id);
    }

    public function testIsUserOwner()
    {
        $defaultRoleService = $this->app->make(DefaultRoleService::class);
        $userRoleService = $this->app->make(UserRoleService::class);

        $accountId = 1;

        $userId1 = 2; // super admin
        $superAdminRole = $defaultRoleService->createSuperAdminRole($accountId);
        $userRoleService->create([
            'role_id' => $superAdminRole->id,
            'user_id' => $userId1,
        ]);

        $userId2 = 3; // owner
        $ownerRole = $defaultRoleService->createOwnerRole($accountId);
        $userRoleService->create([
            'role_id' => $ownerRole->id,
            'user_id' => $userId2,
        ]);

        $this->assertFalse($defaultRoleService->isUserOwner($accountId, $userId1));
        $this->assertTrue($defaultRoleService->isUserOwner($accountId, $userId2));
    }
}
