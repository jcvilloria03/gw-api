<?php

namespace Tests\Role;

use App\Audit\AuditService;
use App\Role\RoleAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class RoleAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'company_id' => 1,
            'name' => 'name'
        ]);
        $item = [
            'action' => RoleAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $roleAuditService = new RoleAuditService($mockAuditService);
        $roleAuditService->logCreate($item);
    }

    public function testLogDelete()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1
        ]);
        $item = [
            'action' => RoleAuditService::ACTION_DELETE,
            'user' => $user,
            'old' => $oldData
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $roleAuditService = new RoleAuditService($mockAuditService);
        $roleAuditService->logDelete($item);
    }
}
