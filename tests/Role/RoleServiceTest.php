<?php

namespace Tests\Role;

use App\Authn\AuthnService;
use App\Model\Permission;
use App\Model\Role;
use App\Model\Task;
use App\Model\UserRole;
use App\Permission\PermissionService;
use App\Permission\TargetType;
use App\Role\RoleService;
use App\Task\TaskService;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Mockery as m;

class RoleServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    private function getInstance()
    {
        $mockTaskService = m::mock(TaskService::class);
        $mockTaskService->makePartial();
        $mockTaskService->shouldReceive('getEmployeeTasks')
            ->andReturn(collect([
                ['id' => 2],
            ]));

        $mockPermissionService = m::mock(PermissionService::class);
        $mockPermissionService->makePartial();
        $mockPermissionService->shouldReceive('create');

        return m::mock(RoleService::class, [
            $mockTaskService,
            $mockPermissionService
        ]);
    }

    public function testGet()
    {
        $expected = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test custom role',
            'type' => Role::ADMIN,
            'custom_role' => true,
        ]);

        $service = $this->getInstance();
        $service->makePartial();

        $actual = $service->get($expected->id);

        $this->assertEquals($expected->account_id, $actual->account_id);
        $this->assertEquals($expected->company_id, $actual->company_id);
        $this->assertEquals($expected->name, $actual->name);
        $this->assertEquals($expected->type, $actual->type);
        $this->assertEquals($expected->custom_role, $actual->custom_role);
    }

    public function testGetNonExistingRole()
    {
        $service = $this->getInstance();
        $service->makePartial();

        $actual = $service->get(9999999);

        $this->assertNull($actual);
    }

    public function testUpdate()
    {
        $expected = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test custom role',
            'type' => Role::ADMIN,
            'custom_role' => true,
        ]);

        $attributes = [
            'tasks' => [1],
        ];

        $service = $this->getInstance();
        $service->makePartial();

        $actual = $service->update($expected->id, $attributes);

        $this->assertEquals($expected->account_id, $actual->account_id);
        $this->assertEquals(0, $actual->company_id);
        $this->assertEquals($expected->name, $actual->name);
        $this->assertEquals($expected->type, $actual->type);
        $this->assertEquals($expected->custom_role, $actual->custom_role);
    }

    public function testUpdateFail()
    {
        $attributes = [
            'permissions' => [
                [
                    'task_id' => 1,
                    'scopes' => [
                        [
                            'type' => 'Payroll Group',
                            'targets' => [
                                'PG1',
                                'PG2',
                            ],
                        ]
                    ],
                ]
            ],
        ];

        $service = $this->getInstance();
        $service->makePartial();

        $this->expectException(ModelNotFoundException::class);
        $service->update(99999, $attributes);
    }

    public function testGetCommonValidationRulesForRoleIds()
    {
        Role::create([
            'account_id' => 2,
            'company_id' => 1,
            'name' => 'test role',
            'type' => 'Admin',
            'custom_role' => true,
        ]);

        $accountId = 2;
        $expected = [
            'role_ids.*' => [
                'required',
                Rule::exists('roles', 'id')->where(function ($query) use ($accountId) {
                    $query->where('account_id', $accountId)->where('custom_role', true);
                }),
            ],
            'role_ids' => 'required|array',
        ];

        $taskService = new TaskService();
        $permissionService = new PermissionService();
        $service = new RoleService($taskService, $permissionService);

        $actual = $service->getCommonValidationRulesForRoleIds($accountId);

        $this->assertEquals($expected, $actual);
    }

    public function testCreateAdminRole()
    {
        $service = $this->app->make(RoleService::class);
        $role = $service->create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);
        $this->assertNotEmpty($role->id);
        $this->assertEquals(1, $role->account_id);
        $this->assertEquals(1, $role->company_id);
        $this->assertEquals('Test Role', $role->name);
        $this->assertEquals(Role::ADMIN, $role->type);
    }

    public function testCreateEmployeeRole()
    {
        $service = $this->app->make(RoleService::class);
        $role = $service->create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $this->assertNotEmpty($role->id);
        $this->assertEquals(1, $role->account_id);
        $this->assertEquals(1, $role->company_id);
        $this->assertEquals('Test Role', $role->name);
        $this->assertEquals(Role::EMPLOYEE, $role->type);
    }

    public function testGetAllAccountRoles()
    {
        $service = $this->app->make(RoleService::class);
        $accountId = 1;
        $service->create([
            'account_id' => $accountId,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 5,
            'name' => 'Test Role 2',
            'type' => Role::ADMIN
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 12,
            'name' => 'Test Role 3',
            'type' => Role::ADMIN
        ]);

        $roles = $service->getAllAccountRoles($accountId);

        $this->assertCount(3, $roles);
    }

    public function testBulkDelete()
    {
        $service = $this->app->make(RoleService::class);
        $accountId = 1;
        $service->create([
            'account_id' => $accountId,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 5,
            'name' => 'Test Role 2',
            'type' => Role::ADMIN
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 12,
            'name' => 'Test Role 3',
            'type' => Role::ADMIN
        ]);

        $roles = $service->getAllAccountRoles($accountId);
        $this->assertCount(3, $roles);
        $ids = $roles->pluck('id')->all();

        $service->bulkDelete($accountId, $ids);

        $roles = $service->getAllAccountRoles($accountId);
        $this->assertCount(0, $roles);
    }

    public function testCheckInUse()
    {
        $service = $this->app->make(RoleService::class);
        $accountId = 1;
        $role = $service->create([
            'account_id' => $accountId,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);

        $mockAuditService = m::mock(AuthnService::class);

        $mockAuditService
            ->shouldReceive('getActiveUserIds')
            ->once()
            ->andReturn([
                [
                    'status' => 'inactive'
                ],
                [
                    'status' => 'inactive'
                ]
            ]);

        $inUse = $service->checkInUse($accountId, [$role->id], $mockAuditService);

        $this->assertEquals($inUse, 0);
    }

    public function testCheckInUseWithUsedRole()
    {
        $service = $this->app->make(RoleService::class);
        $accountId = 1;
        $userId = 1;
        $role = $service->create([
            'account_id' => $accountId,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE
        ]);
        UserRole::create([
            'role_id' => $role->id,
            'user_id' => $userId
        ]);
        $mockAuditService = m::mock(AuthnService::class);

        $mockAuditService
            ->shouldReceive('getActiveUserIds')
            ->once()
            ->andReturn([
                [
                    'status' => 'active'
                ],
                [
                    'status' => 'inactive'
                ]
            ]);

        $inUse = $service->checkInUse($accountId, [$role->id], $mockAuditService);

        $this->assertEquals($inUse, 1);
    }

    public function testCreateCustomRole()
    {
        $mockAdminTask = Task::create([
            'name' => 'test.task',
            'display_name' => 'Test Admin Task',
            'description' => 'Test Admin Task',
            'module' => 'TESTMODULE',
            'submodule' => 'TESTSUBMODULE',
            'allowed_scopes' => json_encode([
                TargetType::ACCOUNT,
                TargetType::COMPANY,
                TargetType::PAYROLL_GROUP
            ]),
            'ess' => 0,
        ]);

        $accountId = 4;
        $attributes = [
            'name' => 'test custom role A',
            'tasks' => [$mockAdminTask->id],
            'company_id' => 0,
            'account_id' => 4,
        ];

        $taskService = new TaskService();
        $permissionService = new PermissionService();
        $service = new RoleService($taskService, $permissionService);

        $actual = $service->createCustomRole($attributes);

        $permission = Permission::where([
            'task_id' => $mockAdminTask->id,
            'role_id' => $actual->id,
        ])->get();

        $this->assertEquals($accountId, $actual->account_id);
        $this->assertEquals($attributes['name'], $actual->name);
        $this->assertEmpty($actual->type);
        $this->assertEmpty($actual->company_id);
        $this->assertNotEmpty($permission->count());
        $this->assertTrue($actual->custom_role);
    }

    public function testGetCompanyRoles()
    {
        $accountId = 99;
        $companyId = 101;

        $service = $this->app->make(RoleService::class);
        $service->create([
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'Test Role',
            'type' => Role::EMPLOYEE,
            'custom_role' => 1
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 123,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);
        $service->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);

        $result = $service->getCompanyRoles($accountId, $companyId);
        $this->assertEquals(2, $result->count());
    }

    public function testGetAccountRolesWithSystemDefinedAndWIthOutSystemDefinedRoles()
    {
        $accountId = 1;
        $service = $this->app->make(RoleService::class);

        for ($i = 0; $i < 5; $i++) {
            $service->create([
                'account_id' => $accountId,
                'company_id' => $i,
                'name' => 'Test Role',
                'type' => Role::ADMIN
            ]);
        }

        for ($i = 0; $i < 5; $i++) {
            $service->create([
                'account_id' => $accountId,
                'company_id' => $i,
                'name' => 'Test Role',
                'type' => Role::EMPLOYEE
            ]);
        }

        $withSystemDefinedAndAdminType = $service->getRoles($accountId);
        $this->assertEquals(5, count($withSystemDefinedAndAdminType));

        $withSystemDefinedAndAdminType = $service->getRoles($accountId, false);
        $this->assertEquals(5, count($withSystemDefinedAndAdminType));

        $withoutSystemDefined = $service->getRoles($accountId, true, false);
        $this->assertEquals(0, count($withoutSystemDefined));

        $withSystemDefinedAndAdminType = $service->getRoles($accountId, 0);
        $this->assertEquals(5, count($withSystemDefinedAndAdminType));

        $withoutSystemDefined = $service->getRoles($accountId, 1, false);
        $this->assertEquals(0, count($withoutSystemDefined));

        $companyRoles = $service->getRoles($accountId, 1, false, 1);
        $this->assertEquals(0, count($companyRoles));
    }

    public function testIsNameAvailable()
    {
        $accountId = 99;
        $companyId = 101;

        $service = $this->app->make(RoleService::class);

        // system defined role without company id, return false
        $name1 = 'Owner';
        $service->create([
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => $name1,
            'type' => Role::ADMIN,
            'custom_role' => 0
        ]);
        $inputs1 = [
            'name' => $name1,
            'account_id' => $accountId
        ];
        $this->assertTrue($service->isNameAvailable($inputs1));

        // system defined role with company id, return false
        $name2 = 'Super Admin';
        $service->create([
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => $name2,
            'type' => Role::ADMIN,
            'custom_role' => 0
        ]);
        $inputs2 = [
            'name' => $name1,
            'account_id' => $accountId,
            'company_id' => 1234
        ];
        $this->assertFalse($service->isNameAvailable($inputs2));

        // system defined role without company id, return true
        $inputs2 = [
            'name' => 'test name test',
            'account_id' => $accountId
        ];
        $this->assertFalse($service->isNameAvailable($inputs2));

        // system defined role with company id, return true
        $inputs2 = [
            'name' => 'test name test',
            'account_id' => $accountId,
            'company_id' => 1234
        ];
        $this->assertFalse($service->isNameAvailable($inputs2));
    }
}
