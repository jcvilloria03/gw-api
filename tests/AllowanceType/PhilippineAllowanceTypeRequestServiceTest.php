<?php

namespace Tests\CommissionType;

use App\AllowanceType\PhilippineAllowanceTypeRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use PHPUnit\Framework\TestCase;

class PhilippineAllowanceTypeRequestServiceTest extends TestCase
{
    public function testBulkCreateCompanyAllowanceTypes()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PhilippineAllowanceTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->bulkCreate(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->bulkCreate(1, []);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->bulkCreate(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->bulkCreate(1, []);
    }

    public function testisNameAvailable()
    {
        $mock = new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_UNAUTHORIZED, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        $requestService = new PhilippineAllowanceTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->isNameAvailable(1, ['name' => 'testName']);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


        // test Response::HTTP_UNAUTHORIZED
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, []);

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->isNameAvailable(1, []);
    }

    public function testUpdate()
    {
        $handler = HandlerStack::create(new MockHandler([
            new GuzzleResponse(Response::HTTP_OK, []),
            new GuzzleResponse(Response::HTTP_NOT_FOUND, []),
            new GuzzleResponse(Response::HTTP_NOT_ACCEPTABLE, [])
        ]));

        $client = new Client(['handler' => $handler]);

        $requestService = new PhilippineAllowanceTypeRequestService($client);

        // test Response::HTTP_OK
        $response = $requestService->update(1, []);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        // test Response::HTTP_NOT_FOUND
        $this->expectException(HttpException::class);
        $requestService->update(1, []);

        // test Response::HTTP_NOT_ACCEPTABLE
        $this->expectException(HttpException::class);
        $requestService->update(1, []);
    }
}
