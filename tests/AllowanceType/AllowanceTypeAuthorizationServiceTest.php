<?php

namespace Tests\AllowanceType;

use Tests\Common\CommonAuthorizationService;

class AllowanceTypeAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\AllowanceType\AllowanceTypeAuthorizationService::class;
}
