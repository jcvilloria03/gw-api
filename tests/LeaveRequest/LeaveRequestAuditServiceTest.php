<?php

namespace Tests\LeaveRequest;

use App\Audit\AuditService;
use App\LeaveRequest\LeaveRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\LeaveRequest\LeaveRequestRequestService;
use Illuminate\Http\JsonResponse;

class LeaveRequestAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => LeaveRequestAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockLeaveRequestRequestService = m::mock(LeaveRequestRequestService::class);

        $leaveRequestAuditService = new LeaveRequestAuditService(
            $mockAuditService,
            $mockLeaveRequestRequestService
        );

        $leaveRequestAuditService->logCreate($item);
    }

    public function testLogUpdate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $oldData = json_encode([
            'id' => 1,
            'leave_type_id' => 1
        ]);
        $newData = json_encode([
            'id' => 1,
            'leave_type_id' => 2
        ]);
        $item = [
            'action' => LeaveRequestAuditService::ACTION_UPDATE,
            'user' => $user,
            'new' => $newData,
            'old' => $oldData,
            'company_id' => 1
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockLeaveRequestRequestService = m::mock(LeaveRequestRequestService::class);

        $leaveRequestAuditService = new LeaveRequestAuditService(
            $mockAuditService,
            $mockLeaveRequestRequestService
        );

        $leaveRequestAuditService->logUpdate($item);
    }

    public function testLogCreateByAdmin()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => LeaveRequestAuditService::ACTION_CREATE_BY_ADMIN,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $mockLeaveRequestRequestService = m::mock(LeaveRequestRequestService::class);

        $leaveRequestAuditService = new LeaveRequestAuditService(
            $mockAuditService,
            $mockLeaveRequestRequestService
        );

        $leaveRequestAuditService->logCreateByAdmin($item);
    }

    public function testLogBatchCreate()
    {
        $user = [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];

        $leaveRequestIds = [1, 2];

        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log');

        $mockLeaveRequestRequestService = m::mock(LeaveRequestRequestService::class);
        $response = new JsonResponse();
        $response->setData(json_encode([
            'id' => 1,
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'full_name' => 'Test name'
            ]
        ]));
        $mockLeaveRequestRequestService
            ->shouldReceive('get')
            ->andReturn($response);

        $leaveRequestAuditService = new LeaveRequestAuditService(
            $mockAuditService,
            $mockLeaveRequestRequestService
        );

        $leaveRequestAuditService->logBatchCreate($leaveRequestIds, $user);
    }
}
