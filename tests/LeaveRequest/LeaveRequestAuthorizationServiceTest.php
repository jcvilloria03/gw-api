<?php

namespace Tests\LeaveRequest;

use App\Permission\Scope;
use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\LeaveRequest\LeaveRequestAuthorizationService;
use PHPUnit\Framework\TestCase;
use Tests\Authorization\AuthorizationServiceTestTrait;

/**
* @SuppressWarnings(PHPMD.ExcessiveClassLength)
* @SuppressWarnings(PHPMD.ExcessivePublicCount)
*/
class LeaveRequestAuthorizationServiceTest extends TestCase
{
    use AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeGet($targetLeaveRequestDetails, $user));
    }

    public function testCreatePassAccountLevel()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testCreateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeCreate($targetLeaveRequestDetails, $user));
    }

    public function testUpdatePassAccountLevel()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdatePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdatePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdateNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdateInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $companyId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
            'company_id' => $companyId,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdateInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdateInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testUpdateInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'test name'
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeUpdate($targetLeaveRequestDetails, $user));
    }

    public function testDeletePassAccountLevel()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $accountId = 1;
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => $accountId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeletePassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeletePassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 1
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeletePassPayrollGroupLevel()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [1, 2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeleteNoScope()
    {
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeleteInvalidAccountScope()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeleteInvalidCompanyAllScope()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeleteInvalidCompanySpecificScope()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }

    public function testDeleteInvalidOtherScope()
    {
        $taskScope = new TaskScopes(LeaveRequestAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['T&A']);
        $targetLeaveRequestDetails = (object) [
            'id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            LeaveRequestAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeDelete($targetLeaveRequestDetails, $user));
    }
}
