<?php

namespace Tests\DeductionType;

use Tests\Common\CommonAuthorizationService;

class DeductionTypeAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\DeductionType\DeductionTypeAuthorizationService::class;
}
