<?php

namespace App\Tests;

use Illuminate\Http\Request;
use App\Broadcast\BroadcastAuthRequest;

class BroadcastAuthRequestTest extends \Tests\TestCase
{
    public function testChannelName()
    {
        $request = Request::create('http://test', 'GET');

        $request->replace([
            'channel_name' => 'test.1'
        ]);

        $request = new BroadcastAuthRequest($request);

        $this->assertEquals('test.1', $request->channel_name);
    }

    public function testUser()
    {
        $request = Request::create('http://test', 'GET');

        $request->attributes->add([
            'user' => []
        ]);

        $request = new BroadcastAuthRequest($request);

        $this->assertEquals([], $request->user());
    }
}
