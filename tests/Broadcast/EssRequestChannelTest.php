<?php

namespace Tests\Broadcast;

use App\Broadcast\EssRequestChannel;
use App\Company\CompanyRequestService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\ESS\EssEmployeeRequestRequestService;
use App\Workflow\WorkflowRequestService;
use Illuminate\Http\JsonResponse;
use Mockery as m;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssRequestChannelTest extends \Tests\TestCase
{
    const USER = ['user_id' => 1];
    const COMPANY_ID = 3;
    const ACCOUNT_ID = 4;
    const REQUEST_ID = 23;

    public function setUp()
    {
        parent::setUp();

        $mockCompanyRequestService = m::mock(CompanyRequestService::class);
        $mockCompanyRequestService->shouldReceive('getAccountId')->andReturn(self::ACCOUNT_ID);
        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);
    }

    public function testValidAuthorizedAsAdmin()
    {
        $channel = new EssRequestChannel(
            $this->mockEmployeeRequestAuthorizationService(),
            $this->mockEssEmployeeRequestAuthorizationService(false),
            $this->mockEssEmployeeRequestRequestService(),
            $this->mockWorkflowService()
        );

        $this->assertTrue($channel(self::USER, self::REQUEST_ID));
    }

    public function testValidAuthorizedAsEmployee()
    {
        $channel = new EssRequestChannel(
            $this->mockEmployeeRequestAuthorizationService(false),
            $this->mockEssEmployeeRequestAuthorizationService(),
            $this->mockEssEmployeeRequestRequestService(),
            $this->mockWorkflowService()
        );

        $this->assertTrue($channel(self::USER, self::REQUEST_ID));
    }

    public function testValidUnauthorized()
    {
        $channel = new EssRequestChannel(
            $this->mockEmployeeRequestAuthorizationService(false),
            $this->mockEssEmployeeRequestAuthorizationService(false),
            $this->mockEssEmployeeRequestRequestService(),
            $this->mockWorkflowService()
        );

        $this->assertFalse($channel(self::USER, self::REQUEST_ID));
    }

    public function testWorkflowsError()
    {
        Log::shouldReceive('error');
        $mockWorkflowService = $this->mockWorkflowService(function ($mock) {
            $mock->shouldReceive('getUserWorkflows')
                ->andThrow(new HttpException(500));
        });

        $channel = new EssRequestChannel(
            $this->mockEmployeeRequestAuthorizationService(),
            $this->mockEssEmployeeRequestAuthorizationService(),
            $this->mockEssEmployeeRequestRequestService(),
            $mockWorkflowService
        );

        $this->assertFalse($channel(self::USER, self::REQUEST_ID));
    }

    public function testEmployeeRequestError()
    {
        Log::shouldReceive('error');
        $mockEssEmployeeRequestRequestService = $this->mockEssEmployeeRequestRequestService(function ($mock) {
            $mock->shouldReceive('get')
                ->andThrow(new HttpException(404));
        });

        $channel = new EssRequestChannel(
            $this->mockEmployeeRequestAuthorizationService(),
            $this->mockEssEmployeeRequestAuthorizationService(),
            $mockEssEmployeeRequestRequestService,
            $this->mockWorkflowService()
        );

        $this->assertFalse($channel(self::USER, self::REQUEST_ID));
    }

    private function mockWorkflowService($value = null)
    {
        $mock = m::mock(WorkflowRequestService::class);

        if (is_callable($value)) {
            $value($mock);

            return $mock;
        }

        $value = $value ?? [
            ['workflow_id' => 1, 'workflow_level_id' => 1, 'position' => 1]
        ];

        $mock->shouldReceive('getUserWorkflows')->andReturn(new JsonResponse(json_encode($value)));

        return $mock;
    }

    private function mockEssEmployeeRequestRequestService($value = null)
    {
        $mock = m::mock(EssEmployeeRequestRequestService::class);

        if (is_callable($value)) {
            $value($mock);

            return $mock;
        }

        $value = $value ?? [
            'employee_id' => 1,
            'company_id' => self::COMPANY_ID,
        ];

        $mock->shouldReceive('get')->andReturn(new JsonResponse(json_encode($value)));

        return $mock;
    }

    private function mockEmployeeRequestAuthorizationService($value = true)
    {
        $mock = m::mock(EmployeeRequestAuthorizationService::class);

        $mock->shouldReceive('authorizeViewSingleRequest')->andReturn($value);

        return $mock;
    }

    private function mockEssEmployeeRequestAuthorizationService($value = true)
    {
        $mock = m::mock(EssEmployeeRequestAuthorizationService::class);

        $mock->shouldReceive('authorizeViewSingleRequest')->andReturn($value);

        return $mock;
    }
}
