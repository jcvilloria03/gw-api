<?php

namespace Tests\Allowance;

use Tests\Common\CommonAuthorizationService;

class AllowanceAuthorizationServiceTest extends CommonAuthorizationService
{
    protected $authorizationServiceClass = \App\Allowance\AllowanceAuthorizationService::class;
}
