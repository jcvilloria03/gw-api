<?php

namespace Tests\EventStream;

use Mockery as M;
use App\Model\Role;
use App\Role\UserRoleService;
use Illuminate\Support\Facades\Log;
use App\Authorization\AuthorizationService;
use Salarium\LumenEventStreamMQ\Facades\EventFactory;
use Salarium\LumenEventStreamMQ\Facades\EventHandlerFactory;
use Salarium\LumenEventStreamMQ\Exceptions\EventStreamException;

class LicenseAppliedEventHandlerTest extends \Tests\TestCase
{
    public function testHandleThrowsExceptionOnIncompleteData()
    {
        $noProductNameEvent = EventFactory::make([
            'name' => 'license.applied',
            'data' => [
                'user_id' => 1,
                'product_name' => ''
            ]
        ]);

        $noUserIdEvent = EventFactory::make([
            'name' => 'license.applied',
            'data' => [
                'user_id' => '',
                'product_name' => 'tests'
            ]
        ]);

        $handler = EventHandlerFactory::make('license.applied');

        $this->expectException(EventStreamException::class);
        $this->expectExceptionMessage(
            'Incomplete event data for '
            . 'license.applied'
            .' event.'
        );
        $handler->handle($noProductNameEvent);

        $this->expectException(EventStreamException::class);
        $this->expectExceptionMessage(
            'Incomplete event data for '
            . 'license.applied'
            .' event.'
        );
        $handler->handle($noUserIdEvent);
    }

    public function testHandleThrowsExceptionOnUnmappedProductName()
    {
        $unmapped = EventFactory::make([
            'name' => 'license.applied',
            'data' => [
                'user_ids' => [1],
                'product_name' => 'tests'
            ]
        ]);

        $handler = EventHandlerFactory::make('license.applied');

        $this->expectException(EventStreamException::class);
        $this->expectExceptionMessage('Product to module access mapping not found for tests.');
        $handler->handle($unmapped);
    }

    public function testHandle()
    {
        $userRoleService = M::mock(UserRoleService::class);
        $userRoleService->shouldReceive('updateUserRolesModuleAccess')
            ->with(1, [Role::ACCESS_MODULE_TA])
            ->once();

        $authorizationService = M::mock(AuthorizationService::class);
        $authorizationService->shouldReceive('invalidateCachedPermissions')
            ->with(1)
            ->once();
        $authorizationService->shouldReceive('buildUserPermissions')
            ->with(1)
            ->once();

        $userRequestService = M::mock(UserRequestService::class);
        $userRequestService->shouldReceive('getAccountOrCompanyUsers')
            ->with(1, '[{"id":"1"}]');

        $this->app->instance(UserRoleService::class, $userRoleService);
        $this->app->instance(AuthorizationService::class, $authorizationService);
        $this->app->instance(UserRequestService::class, $userRequestService);

        $event = EventFactory::make([
            'name' => 'license.applied',
            'data' => [
                'user_ids' => [1],
                'product_name' => 'time and attendance'
            ]
        ]);

        $handler = EventHandlerFactory::make('license.applied');
        $handler->handle($event);
    }
}
