<?php

namespace Tests\ShiftChangeRequest;

use App\Audit\AuditService;
use App\ShiftChangeRequest\ShiftChangeRequestAuditService;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ShiftChangeRequestAuditServiceTest extends TestCase
{
    public function testLogCreate()
    {
        $user = json_encode([
            'id' => 1,
            'account_id' => 1
        ]);
        $newData = json_encode([
            'employee_company_id' => 1,
            'id' => 1,
        ]);
        $item = [
            'action' => ShiftChangeRequestAuditService::ACTION_CREATE,
            'user' => $user,
            'new' => $newData,
        ];
        $mockAuditService = m::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $shiftChangeRequestAuditService = new ShiftChangeRequestAuditService($mockAuditService);
        $shiftChangeRequestAuditService->logCreate($item);
    }
}
