<?php

namespace Tests\Auth0;

use Mockery as m;
use Auth0\SDK\API\Management;
use Illuminate\Http\JsonResponse;
use Auth0\SDK\API\Management\Jobs;
use Auth0\SDK\API\Management\Users;
use App\Auth0\Auth0ManagementService;
use App\Authentication\AuthenticationRequestService;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Auth0ManagementServiceTest extends \Tests\TestCase
{
    public function testCreateUser()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('create')
            ->once()
            ->andReturn([]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->createUser([
            'email' => 'test',
            'first_name' => 'test first name',
            'middle_name' => 'test middle name',
            'last_name' => 'test last name',
            'account_name' => 'test',
        ]);
        $this->assertEquals([], $result);
    }

    public function testUpdateUser()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('update')
            ->once()
            ->andReturn([]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->updateUser(
            1,
            [
                'email' => 'test',
            ]
        );
        $this->assertEquals([], $result);
    }

    public function testGetAuth0UserProfile()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('search')
            ->once()
            ->andReturn([['foo' => 'bar']]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->getAuth0UserProfile('user_id');
        $this->assertEquals(['foo' => 'bar'], $result);
    }

    public function testGetAuth0UserProfileNoMatch()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('search')
            ->once()
            ->andReturn([]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->getAuth0UserProfile('user_id');
        $this->assertEquals(null, $result);
    }

    public function testGetAuth0UserProfileByEmail()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('search')
            ->once()
            ->andReturn([['foo' => 'bar']]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->getAuth0UserProfileByEmail('email');
        $this->assertEquals(['foo' => 'bar'], $result);
    }

    public function testGetAuth0UserProfileByEmailNoMatch()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('search')
            ->once()
            ->andReturn([]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->getAuth0UserProfileByEmail('email');
        $this->assertEquals(null, $result);
    }

    public function testDeleteUser()
    {
        $managementMock = m::mock(Management::class);
        $usersMock = m::mock(Users::class);
        $usersMock->shouldReceive('delete')
            ->once()
            ->andReturn([]);

        $managementMock->users = $usersMock;

        $managementService = m::mock('App\Auth0\Auth0ManagementService[connect]', [$managementMock]);
        $managementService->shouldAllowMockingProtectedMethods();
        $managementService->shouldReceive('connect')
            ->once();

        $result = $managementService->deleteUser('auth|j3kn42kjh3jnk24n3jk3nj');
        $this->assertEquals([], $result);
    }

    public function testVerifyEmail()
    {
        $auth0UserId       = 'auth0|testId1234abcd';

        $responseObject = new JsonResponse(
            json_encode([
                'data' => [
                    'sent'  => true,
                    'links' => '/auth/user/verify_resend?None'
                ]
            ]),
            200
        );

        $uaService = m::mock(AuthenticationRequestService::class);
        $uaService->shouldReceive('verifyResend')
            ->with([
                'data' => [
                    'user' => [
                        'auth0_user_id' => $auth0UserId,
                    ]
                ]
            ])
            ->once()
            ->andReturn($responseObject);

        app()->instance(AuthenticationRequestService::class, $uaService);

        $managementService = app()->make(Auth0ManagementService::class);

        $result = $managementService->verifyEmail($auth0UserId);
        $this->assertTrue($result['data']['sent']);
    }

    public function testVerifyEmailBubblesExceptions()
    {
        $wronngAuth0UserId = 'auth0|missing';

        $missingException = new HttpException(400, 'BadRequestError: The user does not exist');

        $uaService = m::mock(AuthenticationRequestService::class);

        $uaService->shouldReceive('verifyResend')
            ->with([
                'data' => [
                    'user' => [
                        'auth0_user_id' => $wronngAuth0UserId,
                    ]
                ]
            ])
            ->once()
            ->andThrow($missingException);


        app()->instance(AuthenticationRequestService::class, $uaService);

        $managementService = app()->make(Auth0ManagementService::class);

        $this->expectException(HttpException::class);
        $managementService->verifyEmail($wronngAuth0UserId);
    }
}
