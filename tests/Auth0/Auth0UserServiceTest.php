<?php

namespace Tests\Auth0;

use \Mockery as m;
use App\Model\Auth0User;
use App\Audit\AuditService;
use App\Role\UserRoleService;
use App\Auth0\Auth0UserService;
use App\Role\DefaultRoleService;
use App\User\UserRequestService;
use App\Auth0\Auth0ManagementService;
use Illuminate\Support\Facades\Redis;
use App\Account\AccountRequestService;
use App\CompanyUser\CompanyUserService;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Laravel\Lumen\Testing\DatabaseTransactions;

class Auth0UserServiceTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    public function testCreateEmployeeUserInManagementProfileExists()
    {
        $auth0User = new Auth0User;
        $auth0User->auth0_user_id = 'filler';

        $attributes = [
            'email'           => 'test@example.com',
            'first_name'      => 'test',
            'last_name'       => 'tset',
            'middle_name'     => 'etts',
            'account_id'      => 0,
            'company_user_id' => 0,
            'active'          => true
        ];

        $auth0Id = 'auth0|test';

        $newAuth0User = clone $auth0User;
        $newAuth0User->auth0_user_id = $auth0Id;

        $mockManagementService = m::mock(Auth0ManagementService::class);
        $mockManagementService->shouldReceive('getAuth0UserProfileByEmail')
            ->once()
            ->andReturn([
                'user_id' => $auth0Id
            ]);

        $mockManagementService->shouldReceive('getAuth0UserProfileByEmail')
            ->never();

        $mockAccountResponse = m::mock(GuzzleResponse::class);
        $mockAccountResponse->shouldReceive('getData')
            ->once()
            ->andReturn('{"name": 1}');
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockAccountRequestService->shouldReceive('get')
            ->once()
            ->andReturn($mockAccountResponse);

        $mockCompanyUserService = m::mock(CompanyUserService::class);

        $service = m::mock(
            Auth0UserService::class . '[create, update]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $service->shouldReceive('create')
            ->never();

        $service->shouldReceive('update')
            ->once()
            ->andReturn($newAuth0User);

        $auth0UserResponse = $service->createEmployeeUserInManagement($attributes, $auth0User);
        $this->assertNotEquals($auth0User->auth0_user_id, $auth0UserResponse->auth0_user_id);
        $this->assertEquals($auth0UserResponse->auth0_user_id, $auth0Id);
    }

    public function testCreateEmployeeUserInManagementNoProfile()
    {
        $attributes = [
            'email'           => 'test@example.com',
            'first_name'      => 'test',
            'last_name'       => 'tset',
            'middle_name'     => 'etts',
            'account_id'      => 0,
            'company_user_id' => 0,
            'active'          => true
        ];

        $auth0Id = 'auth0|test';

        $newAuth0User = new Auth0User;
        $newAuth0User->auth0_user_id = $auth0Id;

        $mockManagementService = m::mock(Auth0ManagementService::class);
        $mockManagementService->shouldReceive('getAuth0UserProfileByEmail')
            ->once()
            ->andReturn(null);

        $mockManagementService->shouldReceive('createUser')
            ->once()
            ->andReturn([
                'user_id' => $auth0Id
            ]);

        $mockAccountResponse = m::mock(GuzzleResponse::class);
        $mockAccountResponse->shouldReceive('getData')
            ->once()
            ->andReturn('{"name": 1}');
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockAccountRequestService->shouldReceive('get')
            ->once()
            ->andReturn($mockAccountResponse);

        $mockCompanyUserService = m::mock(CompanyUserService::class);

        $service = m::mock(
            Auth0UserService::class . '[create, update]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $service->shouldReceive('create')
            ->once()
            ->andReturn($newAuth0User);

        $service->shouldReceive('update')
            ->never();

        $auth0UserResponse = $service->createEmployeeUserInManagement($attributes);
        $this->assertInstanceOf(Auth0User::class, $auth0UserResponse);
    }

    public function testUpdateEmployeeUser()
    {
        $mockManagementService = m::mock(Auth0ManagementService::class);
        $mockManagementService->shouldReceive('updateUser')
            ->once();

        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[create]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $auth0User = new Auth0User;
        $auth0User->auth0_user_id = 1;

        $email = 'test@test.com';
        $attributes = [
            'email' => $email
        ];

        $auth0User = $auth0UserService->updateEmployeeUser($auth0User, $attributes);

        $this->assertEquals($auth0User, $auth0User);
    }

    public function testCreateUser()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0User = $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $this->assertNotEmpty($auth0User->id);
        $this->assertEquals($userId, $auth0User->user_id);
        $this->assertEquals($accountId, $auth0User->account_id);
        $this->assertEquals($auth0UserId, $auth0User->auth0_user_id);
    }

    public function testGetAuth0UserProfile()
    {
        $mockManagementService = m::mock(Auth0ManagementService::class);
        $mockReturn = ['id' => rand(1, 5)];
        $mockManagementService->shouldReceive('getAuth0UserProfile')
            ->once()
            ->andReturn($mockReturn);

        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $auth0User = $auth0UserService->getAuth0UserProfile('user_id');
        $this->assertEquals($mockReturn, $auth0User);
    }

    public function testGetAuth0UserProfileByEmail()
    {
        $mockManagementService = m::mock(Auth0ManagementService::class);
        $mockReturn = ['id' => rand(1, 5)];
        $mockManagementService->shouldReceive('getAuth0UserProfileByEmail')
            ->once()
            ->andReturn($mockReturn);

        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $auth0User = $auth0UserService->getAuth0UserProfileByEmail('email@email.com');
        $this->assertEquals($mockReturn, $auth0User);
    }

    public function testGetUser()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );
        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $auth0User = $auth0UserService->getUser($userId);

        $this->assertNotEmpty($auth0User->id);
        $this->assertEquals($userId, $auth0User->user_id);
        $this->assertEquals($accountId, $auth0User->account_id);
        $this->assertEquals($auth0UserId, $auth0User->auth0_user_id);
    }

    public function testGetUserByIds()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );
        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $auth0User = $auth0UserService->getUsersByIds([$userId]);

        $this->assertNotEmpty($auth0User);
        $this->assertEquals($userId, $auth0User[0]->user_id);
        $this->assertEquals($accountId, $auth0User[0]->account_id);
        $this->assertEquals($auth0UserId, $auth0User[0]->auth0_user_id);
    }

    public function testGetUserByAuth0UserId()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );
        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $auth0User = $auth0UserService->getUserByAuth0UserId($auth0UserId);

        $this->assertNotEmpty($auth0User->id);
        $this->assertEquals($userId, $auth0User->user_id);
        $this->assertEquals($accountId, $auth0User->account_id);
        $this->assertEquals($auth0UserId, $auth0User->auth0_user_id);
    }

    public function testGetUserByEmployeeId()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        $userId      = 12345;
        $accountId   = 12345;
        $auth0UserId = 'auth012345';
        $employeeId  = 12345;

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);
        $mockCompanyUserService->shouldReceive('getByEmployeeId')
            ->once()
            ->andReturn(
                (object) [
                    'user_id' => $userId,
                    'company_id' => 1,
                    'employee_id' => $employeeId
                ]
            );

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId
        ]);

        $auth0User = $auth0UserService->getUserByEmployeeId($employeeId);

        $this->assertNotEmpty($auth0User->id);
        $this->assertEquals($userId, $auth0User->user_id);
        $this->assertEquals($accountId, $auth0User->account_id);
        $this->assertEquals($auth0UserId, $auth0User->auth0_user_id);
    }

    public function testDeleteUser()
    {

        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('del')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        Redis::shouldReceive('del')
            ->andReturnNull();

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );
        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $auth0UserService->deleteUser($userId);

        $auth0User = $auth0UserService->getUserByAuth0UserId($auth0UserId);

        $this->assertEmpty($auth0User);
    }

    public function testGetAuth0UserId()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('del')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0UserService = m::mock(
            Auth0UserService::class . '[createEmployeeUser]',
            [
                $mockManagementService,
                $mockAccountRequestService,
                $mockCompanyUserService
            ]
        );

        $userId = 12345;
        $accountId = 12345;
        $auth0UserId = 'auth012345';

        $auth0UserService->create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'auth0_user_id' => $auth0UserId,
        ]);

        $result = $auth0UserService->getAuth0UserId($userId);

        $this->assertEquals($result, $auth0UserId);
    }

    public function testSetStatus()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('del')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $auth0User = Auth0User::create([
            'id' => 1,
            'user_id' => 1,
            'account_id' => 1,
            'auth0_user_id' => 'auth0|test',
            'status' => Auth0User::STATUS_INACTIVE
        ]);

        $auth0UserService = new Auth0UserService(
            $mockManagementService,
            $mockAccountRequestService,
            $mockCompanyUserService
        );

        $actual = $auth0UserService->setStatus($auth0User->user_id, Auth0User::STATUS_ACTIVE);

        $this->assertEquals($actual['status'], Auth0User::STATUS_ACTIVE);
    }

    public function testUpdateSuccess()
    {
        Redis::shouldReceive('set')
            ->andReturnNull();

        Redis::shouldReceive('del')
            ->andReturnNull();

        Redis::shouldReceive('exists')
            ->andReturn(false);

        Redis::shouldReceive('keys')
            ->andReturn(false);

        Redis::shouldReceive('getCacheKey')
            ->andReturn(false);

        $oldModel = Auth0User::create([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'auth0_user_id' => 'auth0|test',
            'status'        => Auth0User::STATUS_INACTIVE
        ]);

        $updatedModel = new Auth0User([
            'auth0_user_id' => 'new'
        ]);

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $service = new Auth0UserService(
            $mockManagementService,
            $mockAccountRequestService,
            $mockCompanyUserService
        );

        $newModel = $service->update($oldModel, ['auth0_user_id' => 'new']);

        $this->assertSame($newModel->auth0_user_id, $updatedModel->auth0_user_id);
    }

    public function testUpdateFailedThrowsException()
    {
        $oldModel = new Auth0User;

        $mockManagementService     = m::mock(Auth0ManagementService::class);
        $mockAccountRequestService = m::mock(AccountRequestService::class);
        $mockCompanyUserService    = m::mock(CompanyUserService::class);

        $service = new Auth0UserService(
            $mockManagementService,
            $mockAccountRequestService,
            $mockCompanyUserService
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unable to update Auth0 user.');
        $this->expectExceptionCode(500);
        $service->update($oldModel, ['auth0_user_id' => 'test']);
    }
}
