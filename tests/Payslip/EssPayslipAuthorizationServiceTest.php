<?php

namespace Tests\Payslip;

use App\Payslip\EssPayslipAuthorizationService;
use App\Permission\TaskScopes;
use App\Rank\RankAuthorizationService;
use PHPUnit\Framework\TestCase;

class EssPayslipAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testViewPass()
    {
        $taskScope = new TaskScopes(EssPayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payslip\EssPayslipAuthorizationService',
            $taskScope
        );

        $user = [
            'user_id' => 1,
            'employee_id' => 49,
        ];

        $payslip = [
            'employee_id' => 49,
        ];

        $this->assertTrue($authorizationService->authorizeView($user, $payslip));
    }

    public function testViewFailUnauthorized()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payslip\EssPayslipAuthorizationService',
            $taskScope
        );

        $user = [
            'user_id' => 12,
            'employee_id' => 49,
        ];

        $payslip = [
            'employee_id' => 49,
        ];

        $this->assertTrue($authorizationService->authorizeView($user, $payslip));
    }

    public function testViewFailPayslipDoesNotBelongToEmployee()
    {
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payslip\EssPayslipAuthorizationService',
            $taskScope
        );

        $user = [
            'user_id' => 1,
            'employee_id' => 1,
        ];

        $payslip = [
            'employee_id' => 49,
        ];

        $this->assertFalse($authorizationService->authorizeView($user, $payslip));
    }

    public function testListPass()
    {
        $userId = 1;
        $taskScope = new TaskScopes(EssPayslipAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payslip\EssPayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeList($userId));
    }

    public function testListFail()
    {
        $userId = 1;
        $taskScope = new TaskScopes(RankAuthorizationService::CREATE_TASK);
        $taskScope->addModuleScope(['Payroll']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Payslip\EssPayslipAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeList($userId));
    }
}
