<?php

namespace Tests;

use App\Model\Account;
use App\Model\Role;
use App\Transformer\RoleTransformer;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RoleTransformerTest extends TestCase
{
    use DatabaseTransactions;

    public function testTransformRoleModel()
    {
        $roleTransformer = app()->make(RoleTransformer::class);
        $role = Role::firstOrNew([
            'id' => 1,
            'account_id' => 2,
            'name' => 'name',
            'company_id' => 1,
            'module_access' => (array) Role::ACCESS_MODULE_TA
        ]);
        $transform = $roleTransformer->transform($role);
        $this->assertArrayHasKey('id', $transform);
        $this->assertEmpty($transform['id']);
        $this->assertArrayHasKey('account_id', $transform);
        $this->assertEquals(2, $transform['account_id']);
        $this->assertArrayHasKey('name', $transform);
        $this->assertEquals('name', $transform['name']);
        $this->assertArrayHasKey('custom_role', $transform);
        $this->assertEquals(false, $transform['custom_role']);
        $this->assertArrayHasKey('users_count', $transform);
        $this->assertEquals(0, $transform['users_count']);
    }
}
