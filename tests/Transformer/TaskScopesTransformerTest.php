<?php

namespace Tests;

use App\Permission\TargetType;
use App\Permission\TaskScopes;
use App\Permission\Scope;
use App\Transformer\TaskScopesTransformer;

class TaskScopesTransformerTest extends TestCase
{
    public function testTransformRoleModel()
    {
        $taskName = 'create.something';
        $taskScope = new TaskScopes($taskName);
        $scope = new Scope(TargetType::ACCOUNT, [1]);
        $taskScope->addScope($scope);
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $taskScope->addModuleScope(['HRIS']);

        $taskScopesTransformer = new TaskScopesTransformer();
        $transform = $taskScopesTransformer->transform($taskScope);

        $this->assertArrayHasKey($taskName, $transform);
        $this->assertArrayHasKey('scopes', $transform[$taskName]);
        $this->assertArrayHasKey('modules', $transform[$taskName]);

        foreach ($transform[$taskName]['scopes'] as $scope) {
            $this->assertArrayHasKey('type', $scope);
            $this->assertNotEmpty($scope['type']);
            $this->assertArrayHasKey('target', $scope);
            $this->assertNotEmpty($scope['target']);
        }

        $this->assertEquals(1, count($transform[$taskName]['modules']));
    }
}
