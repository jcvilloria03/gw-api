<?php

namespace Tests\Notification;

use App\Notification\EmployeeNotificationAuthorizationService;
use App\Permission\TargetType;
use App\Permission\Scope;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EmployeeNotificationAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testGetPassAccountLevel()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $authParams = (object) ['account_id' => $accountId];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::ACCOUNT, [$accountId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetPassCompanyLevelAll()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $authParams = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetPassCompanyLevelSpecific()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $authParams = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $scope = new Scope(TargetType::COMPANY, [$companyId]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetNoScopeShouldReturnFalse()
    {
        $accountId = 1;
        $companyId = 1;
        $authParams = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => $accountId
        ];
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            null
        );
        $this->assertFalse($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetInvalidAccountScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $accountId = 1;
        $companyId = 1;
        $authParams = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::ACCOUNT, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetInvalidCompanyAllScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authParams = (object) [
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, Scope::TARGET_ALL);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetInvalidCompanySpecificScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authParams = (object) [
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::COMPANY, [2, 3]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($authParams, $user));
    }

    public function testGetInvalidOtherScopeShouldReturnFalse()
    {
        $taskScope = new TaskScopes(EmployeeNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authParams = (object) [
            'account_id' => 1,
            'company_id' => 1,
        ];
        $user = [
            'user_id' => 1,
            'account_id' => 2
        ];
        $scope = new Scope(TargetType::PAYROLL_GROUP, [2]);
        $taskScope->addScope($scope);
        $authorizationService = $this->createMockAuthorizationService(
            EmployeeNotificationAuthorizationService::class,
            $taskScope
        );
        $this->assertFalse($authorizationService->authorizeView($authParams, $user));
    }
}
