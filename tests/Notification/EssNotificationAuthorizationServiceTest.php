<?php

namespace Tests\Notification;

use App\Notification\EssNotificationAuthorizationService;
use App\Permission\TaskScopes;
use PHPUnit\Framework\TestCase;

class EssNotificationAuthorizationServiceTest extends TestCase
{
    use \Tests\Authorization\AuthorizationServiceTestTrait;

    public function testAuthorizeViewUserNotifications()
    {
        $userId = 1;
        $taskScope = new TaskScopes(EssNotificationAuthorizationService::VIEW_TASK);
        $taskScope->addModuleScope(['HRIS']);
        $authorizationService = $this->createMockAuthorizationService(
            'App\Notification\EssNotificationAuthorizationService',
            $taskScope
        );
        $this->assertTrue($authorizationService->authorizeView($userId));
    }

    public function testFailToAuthorizeViewUserNotifications()
    {
        $userId = 1;
        $authorizationService = $this->createMockAuthorizationService(
            'App\Notification\EssNotificationAuthorizationService',
            null
        );
        $this->assertFalse($authorizationService->authorizeView($userId));
    }
}
