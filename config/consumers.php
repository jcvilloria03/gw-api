<?php

return [
     /**
      * Mapping of consumer classes to consumer names
      */
    'employee_upload_response' => '\App\Consumers\EmployeeUploadResponseConsumer',
    'employee_update_upload_response' => '\App\Consumers\EmployeeUpdateUploadResponseConsumer',
    'employee_time_attendance_upload_response' => '\App\Consumers\EmployeeTimeAttendanceUploadResponseConsumer',
    'payroll_upload_response' => '\App\Consumers\PayrollUploadResponseConsumer',
    'payroll_calculation_response' => '\App\Consumers\PayrollCalculationResponseConsumer',
    'payslip_generation_response' => '\App\Consumers\PayslipGenerationResponseConsumer',
    'schedule_upload_response' => '\App\Consumers\ScheduleUploadResponseConsumer',
    'payroll_loan_upload_response' => '\App\Consumers\PayrollLoanUploadResponseConsumer',
    'audit' => '\App\Consumers\AuditTrailConsumer',
    'broadcast_announcement' => '\App\Consumers\BroadcastAnnouncementConsumer',
    'broadcast_notification' => '\App\Consumers\BroadcastNotificationConsumer',
    'broadcast_ess_request' => '\App\Consumers\BroadcastEssRequestConsumer',
    'payroll_group_upload_response' => '\App\Consumers\PayrollGroupUploadResponseConsumer',
    'leave_credit_upload_response' => '\App\Consumers\LeaveCreditUploadResponseConsumer',
    'other_income_upload_response' => '\App\Consumers\OtherIncomeUploadResponseConsumer',
    'leave_request_upload_response' => '\App\Consumers\LeaveRequestUploadResponseConsumer',
    'workflow_entitlement_upload_response' => '\App\Consumers\WorkflowEntitlementUploadResponseConsumer',
    'annual_earning_upload_response' => '\App\Consumers\AnnualEarningUploadResponseConsumer',
    'earning_upload_response' => '\App\Consumers\EarningUploadResponseConsumer',
    'user_profile_upload_response' => '\App\Consumers\UserProfileUploadResponseConsumer',
    'employees_update_status' => '\App\Consumers\EmployeesUpdateStatusConsumer',
    'employees_update_send_verification_email' => '\App\Consumers\EmployeesUpdateSendVerificationEmailConsumer',
    'event_stream' => '\Salarium\LumenEventStreamMQ\ExchangeConsumer',
    'user_essential_data_update' => '\App\Consumers\UserEssentialDataUpdateConsumer',
    'account_essential_data_update' => '\App\Consumers\AccountEssentialDataUpdateConsumer',
    'demo_company_gw' => '\App\Consumers\DemoCompanyConsumer',
    'authn_user_verified' => '\App\Consumers\VerifiedAuthnUserConsumer',
    'revoke_authn_tokens' => '\App\Consumers\RevokeAuthnTokensConsumer',
    'account_creation' => '\App\Consumers\AccountCreationConsumer',
    'authn_user_create_update' => '\App\Consumers\AuthnUserCreateUpdateConsumer',
];
