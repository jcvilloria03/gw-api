<?php

return [
    [
        'method' => 'POST',
        'url' => '/ess/undertime_request',
        'modules' => [
            'ess.requests.undertime' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/calendar',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/{subscriptionId}/draft',
        'modules' => [
            'control_panel.subscriptions.licenses_tab' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/roles/all',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/team/is_name_available',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_credit/upload/status',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/upload/deduction',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['CREATE']]
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/other_income_types/{type}',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['READ'],
            ],
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['READ'],
            ],
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['READ'],
            ],
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['READ'],
            ],
            'employees.people.adjustments' => [
                'actions' => ['READ'],
            ],
            'employees.adjustments' => [
                'actions' => ['READ'],
            ],
            'employees.people.allowances' => [
                'actions' => ['READ'],
            ],
            'employees.allowances' => [
                'actions' => ['READ'],
            ],
            'employees.people.bonuses' => [
                'actions' => ['READ'],
            ],
            'employees.bonuses' => [
                'actions' => ['READ'],
            ],
            'employees.people.commissions' => [
                'actions' => ['READ'],
            ],
            'employees.commissions' => [
                'actions' => ['READ'],
            ],
            'employees.people.deductions' => [
                'actions' => ['READ'],
            ],
            'employees.deductions' => [
                'actions' => ['READ'],
            ],
            'payroll.bonuses' => [
                'actions' => ['READ'],
            ],
            'payroll.commissions' => [
                'actions' => ['READ'],
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => 'philippine/company/{companyId}/government_form_periods',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll/{id}/send_payslips',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/role/{roleId}/assigned_users',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/employee/{employee_id}',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/upload/bonus',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['DELETE']],
        ],
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/{country}/companies',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/account',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.licenses_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.invoices_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.receipts_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ],
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles' => [
            //     'actions' => ['READ']
            // ],
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ],
            'root.admin' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll_loan/upload/preview',
        'modules' => [
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/philippine/payroll_group/{payroll_group_id}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/workflow_entitlement/upload',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_group/upload/save',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_payroll' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/invoices/{id}/payments/generate_paynamics',
        'modules' => [
            'control_panel.subscriptions.invoices_tab.invoice_history' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/invoices/{id}/billed_users',
        'modules' => [
            'control_panel.subscriptions.invoices_tab.invoice_history' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/rank/is_name_available',
        'modules' => [
            'company_settings.company_structure.ranks' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/employee/{employeeId}/payroll_loans',
        'modules' => [
            'employees.people.loans' => [
                'actions' => ['READ']
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/cost_center',
        'modules' => [
            'company_settings.company_structure.cost_centers' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{employeeId}/rest_days',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/holiday/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE'],
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/other_income_type/is_edit_available',
        'modules' => [
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['UPDATE'],
            ],
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['UPDATE'],
            ],
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['UPDATE'],
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/shifts',
        'modules' => [
            'ess.teams' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/company/{id}/payroll_groups',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => ['actions' => ['READ']],
            'employees.people' => ['actions' => ['READ']],
            'employees.people.payroll_information' => ['actions' => ['READ']],
            'employees.people.bonuses' => ['actions' => ['READ']],
            'employees.people.commissions' => ['actions' => ['READ']],
            'employees.government_forms' => ['actions' => ['READ']],
            'payroll.bonuses' => ['actions' => ['READ']],
            'payroll.commissions' => ['actions' => ['READ']],
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{employeeId}/final_pay/available_items',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payroll_loans',
        'modules' => [
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/project/{id}',
        'modules' => [
            'company_settings.company_structure.projects' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/allowance/bulk_create',
        'modules' => [
            'employees.people.allowances' => ['actions' => ['CREATE']],
            'employees.allowances' => ['actions' => ['CREATE']],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/annual_earning/upload/preview',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/clock_state/timesheet',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.requests' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ],
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.calendar' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/clock_state/timesheet/last_entry',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/current-schedules',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/hours_worked',
        'modules' => [
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/cost_center/{cost_center_id}',
        'modules' => [
            'company_settings.company_structure.cost_centers' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/annual_earning/set',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/workflow/check_in_use',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/request/bulk_approve',
        'modules' => [
            'ess.approvals' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/request/bulk_cancel',
        'modules' => [
            'ess.approvals' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/night_shift/{id}',
        'modules' => [
            'company_settings.schedule_settings.night_shift' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payroll_group_periods',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/cost_center/is_name_available',
        'modules' => [
            'company_settings.company_structure.cost_centers' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/payslips/authorized_payroll_groups',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/shift_change_request/{id}',
        'modules' => [
            'ess.requests.shift_change' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/shift_change_request/{id}',
        'modules' => [
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/subscriptions/plans',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/employee/batch_update/status',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/employee/{employeeId}/annual_earning',
        'modules' => [
            'employees.people.annual_earnings' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/other_income/{type}/upload/preview',
        'modules' => [
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'employees.allowances' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'DELETE',
    //     'url' => '/salpay/companies/{company_id}/employee-invitations',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/deduction/upload/save',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/schedule/upload',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/schedule/upload/save',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/shift/bulk_create',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/shifts_data',
        'modules' => [
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/request/send_message',
        'modules' => [
            'ess.approvals' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.overtime' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.undertime' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.shift_change' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/account/progress',
        'modules' => [
            'company_settings.company_structure' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.day_hour_rates' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.schedule_settings.default_schedule' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.schedule_settings.night_shift' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['UPDATE']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles' => [
            //     'actions' => ['UPDATE']
            // ],
            // 'company_settings.users_and_roles.company_roles' => [
            //     'actions' => ['UPDATE']
            // ],
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['UPDATE']
            // ],
            'company_settings.workflow_automation' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.max_clock_outs' => [
                'actions' => ['UPDATE']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/account/setup_progress',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_update/personal_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_update/people',
        'modules' => [
            'employees.people' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/subscriptions/{subscriptionsId}/stats',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/leave_request/upload/status',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/user',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['CREATE']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['CREATE']
            // ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/leave_type/is_name_available',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['UPDATE', 'CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'control_panel.companies.company_information' => [
                'actions' => ['UPDATE']
            ],
            'control_panel.companies.government_issued_id_number' => [
                'actions' => ['UPDATE']
            ],
            'control_panel.companies.contact_information' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/ranks',
        'modules' => [
            'company_settings.company_structure.ranks' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/workflow_entitlement/{id}',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/upload/commission/status',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/schedules/download/{fileName}',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'PATCH',
    //     'url' => '/auth/user/change_password',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/deduction_type/{id}',
        'modules' => [
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/leave_credits/download',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/announcement/recipients/download',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'wrapper',
    //     'url' => 'wrapper',
    //     'modules' => []
    // ],
    [
        'method' => 'PUT',
        'url' => '/ess/announcement/reply/{id}/seen',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/announcement/reply/{id}/seen',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/hours_worked/leaves/bulk_create_or_update_or_delete',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/hours_worked/leaves/bulk_create_or_update_or_delete',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance.leaves' => [
                'actions' => ['CREATE', 'UPDATE', 'DELETE'],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/other_income/{id}',
        'modules' => [
            'employees.people.allowances' => [
                'actions' => ['READ'],
            ],
            'employees.people.adjustments' => [
                'actions' => ['READ'],
            ],
            'employees.people.bonuses' => [
                'actions' => ['READ'],
            ],
            'employees.people.commissions' => [
                'actions' => ['READ'],
            ],
            'employees.people.deductions' => [
                'actions' => ['READ'],
            ],
            'employees.adjustments' => [
                'actions' => ['READ'],
            ],
            'employees.allowances' => [
                'actions' => ['READ'],
            ],
            'employees.deductions' => [
                'actions' => ['READ'],
            ],
            'payroll.bonuses' => [
                'actions' => ['READ'],
            ],
            'payroll.commissions' => [
                'actions' => ['READ'],
            ],
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/position/bulk_update',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/other_income/adjustment/upload/preview',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/invoices/{id}',
        'modules' => [
            'control_panel.subscriptions.invoices_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.invoices_tab.invoice_history' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{company_id}/government_forms/bir_2316/employees',
        'modules' => [
            'employees.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/team/bulk_delete',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/employee/approvals',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/leave_entitlement/bulk_delete',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/upload/allowance/status',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/attachments',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/schedule/is_name_available',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/shifts',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/other_income_type/is_delete_available',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['DELETE'],
            ],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/company/{id}/other_income_type',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['DELETE'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/other_income/is_delete_available',
        'modules' => [
            'payroll.bonuses' => ['actions' => ['READ']],
            'payroll.commissions' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/leave_request/{id}/admin',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['UPDATE']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/time_attendance_locations',
        'modules' => [
            'company_settings.company_structure.locations' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.locations.location' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.locations.ip_address' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ],
            'company_settings.schedule_settings' => [
                'actions' => ['READ']
            ],
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/personal_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/people',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/time_attendance_locations',
        'modules' => [
            'company_settings.company_structure.locations' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/time_attendance_team',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/tardiness_rule/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/request/bulk_decline',
        'modules' => [
            'ess.approvals' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/leave_credit/{id}',
        'modules' => [
            'employees.people.leave_credits' => [
                'actions' => ['UPDATE']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['UPDATE']
            ],
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/day_hour_rate/{id}',
        'modules' => [
            'company_settings.day_hour_rates' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/calendar_data',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.calendar' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/account/philippine/companies',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ],
            'root.ess' => [
                'actions' => ['READ']
            ],
            'control_panel.companies' => [
                'actions' => ['READ']
            ],
            'control_panel.roles' => [
                'actions' => ['READ'],
            ],
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.company_details' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.company_details.company_information' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.company_details.government_issued_id_number' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.company_details.contact_information' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ],
            // 'company_settings.users_and_roles.company_roles' => [
            //     'actions' => ['READ']
            // ],
            'control_panel.device_management' => [
                'actions' => ['READ']
            ],
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/allowance_type/is_name_available',
        'modules' => [
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['CREATE'],
            ],
        ],
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/employee/{employeeId}/other_incomes/bonus_type',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/cost_centers',
        'modules' => [
            'company_settings.company_structure.cost_centers' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/workflow_entitlement/create_or_update',
        'modules' => [
            'employees.people.approval_groups' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/upload/deduction/status',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/payroll_loan_type/bulk_delete',
        'modules' => [
            'company_settings.company_payroll.loan_type_settings' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/government_forms/{id}',
        'modules' => [
            'payroll.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/rank/bulk_create',
        'modules' => [
            'company_settings.company_structure.ranks' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/payroll_loan/{loanUid}/update_amortization_preview',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/allowance/upload',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/attendance/calculate/bulk',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => ['actions' => ['CREATE']],
            'main_page.dashboard' => ['actions' => ['UPDATE']],
        ],
    ],
    // [
    //     'method' => 'PUT',
    //     'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements/{disbursementId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/undertime_request/{id}',
        'modules' => [
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/undertime_request/{id}',
        'modules' => [
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_credit/upload/preview',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/company/{id}/government_form_periods',
        'modules' => [
            'payroll.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/payroll_group/is_name_available',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['READ'],
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_request/upload/save',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/time_attendance_locations/bulk_delete',
        'modules' => [
            'company_settings.company_structure.locations' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll_loan/upload/status',
        'modules' => [
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payroll/{type}/available_items',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/workflow',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/adjustment/bulk_create',
        'modules' => [
            'employees.people.adjustments' => ['actions' => ['CREATE']],
            'employees.adjustments' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/attendance/{employee_id}/{date}/edit',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance.computed_attendance' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll_group/bulk_delete',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['DELETE'],
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll_group/{payroll_group_id}/periods',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{company_id}/leave_credits',
        'modules' => [
            'employees.people.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/adjustment/{id}',
        'modules' => [
            'employees.people.adjustments' => [
                'actions' => ['UPDATE']
            ],
            'employees.adjustments' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/workflow_entitlement/bulk_create_or_update',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/shift/{id}',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/announcement/{announcementId}/recipient_seen',
        'modules' => [
            'main_page.announcements' => ['actions' => ['READ']],
            'root.admin' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/announcement_notifications',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/announcement/reply/{id}',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/departments',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ],
            'employees.people.bonuses' => [
                'actions' => ['READ']
            ],
            'employees.people.commissions' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ],
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'employees.adjustments' => [
                'actions' => ['READ']
            ],
            'employees.allowances' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.workflows' => [
                'actions' => ['READ']
            ],
            'employees.government_forms' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['READ']
            ],
            'payroll.bonuses' => [
                'actions' => ['READ']
            ],
            'payroll.commissions' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/attendance/job/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/holiday/is_name_available',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/workflow_entitlement/upload/preview',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/payslip/{id}',
        'modules' => [
            'ess.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_incomes/deduction_type/download',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/announcement/{id}/reply',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/attendance/job/{jobId}/errors',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/other_income_types/allowance_type',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'PATCH',
    //     'url' => '/philippine/allowance_type/{id}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/employee/batch_update/time_attendance_info',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_incomes/deduction_type/download',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/ess/announcement/{announcementId}/reply',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/attendance/job/{jobId}/errors',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/allowance_type/{id}',
        'modules' => [
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['UPDATE'],
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_update/time_attendance_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/employment_type/is_name_available',
        'modules' => [
            'company_settings.company_structure.employment_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],

    [
        'method' => 'POST',
        'url' => '/company/{id}/department/is_name_available',
        'modules' => [
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/deduction/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'PUT',
        'url' => '/leave_type/{id}',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{company_id}/government_forms/bir_2316/download',
        'modules' => [
            'employees.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{id}/leave_entitlement',
        'modules' => [
            'employees.people.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employee/{employeeId}/leave_types',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/upload/deduction',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['DELETE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/bonus_type/is_name_available',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['CREATE'],
            ],
        ],
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/link/company-integrations',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/holiday/bulk_delete',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/employee/{employeeId}/other_incomes/deduction_type',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/dashboard_statistics',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/user/{id}',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['UPDATE']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['UPDATE']
            // ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_incomes/bonus_type/download',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/leave_request/admin/calculate_leaves_total_value',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/affected_employees/search',
        'modules' => [
            'main_page.dashboard' => ['actions' => ['READ']],
            'main_page.announcements' => ['actions' => ['READ']],
            'time_and_attendance.shifts.assign_shift' => ['actions' => ['READ']],
            'company_settings.leave_settings' => ['actions' => ['READ']],
            'company_settings.leave_settings.leave_entitlements' => ['actions' => ['READ']],
            'company_settings.schedule_settings' => ['actions' => ['READ']],
            'company_settings.schedule_settings.tardiness_rules' => ['actions' => ['READ']],
            'time_and_attendance.schedules' => ['actions' => ['READ']],
            'company_settings.workflow_automation' => ['actions' => ['READ']],
            'company_settings.max_clock_outs' => ['actions' => ['READ']]
        ],
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/account/role',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll/{id}/open',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['UPDATE']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/records',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/final',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employees/final_pay',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/payroll_register/{id}/has_payroll_register',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/position/is_name_available',
        'modules' => [
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/philippine/company/${companyId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/rest_day',
        'modules' => [
            'time_and_attendance.shifts.assign_rest_day' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/has_payslips',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/leave_request/admin',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['CREATE']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/department/bulk_update',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/view/{companyId}/attendance/user',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/bonus_type/{id}',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/rest_day/{id}',
        'modules' => [
            'time_and_attendance.shifts.assign_rest_day' => [
                'actions' => ['UPDATE']
            ],
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_entitlement',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/government_forms/bir_2316/bulk/generate',
        'modules' => [
            'employees.government_forms' => ['actions' => ['CREATE']]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/attendance/regenerate/{jobId}/errors/download',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/default_schedule/bulk_update',
        'modules' => [
            'company_settings.schedule_settings.default_schedule' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/commission_type/{id}',
        'modules' => [
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['UPDATE'],
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/night_shift',
        'modules' => [
            'company_settings.schedule_settings.night_shift' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/user/{id}/set_status',
        'modules' => [
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['UPDATE']
            // ],
            'control_panel.users' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payroll_loan_types',
        'modules' => [
            'company_settings.company_payroll.loan_type_settings' => [
                'actions' => ['READ']
            ],
            'employees.people.loans' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/philippine/company_types',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/time_records/bulk_create_or_delete',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance.time_records' => [
                'actions' => ['CREATE', 'UPDATE', 'DELETE'],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/ess/attendance/{employee_id}/{date}',
        'modules' => [
            'ess.requests.overtime' => [
                'actions' => ['READ']
            ],
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payroll_id}/calculate',
        'modules' => [
            'employees.people.termination_information' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => ['actions' => ['CREATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/ess/shift/overlapping_requests',
        'modules' => [
            'ess.requests.shift_change' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/invoices',
        'modules' => [
            'control_panel.subscriptions.invoices_tab' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/companies/{company_id}/employee-invitations',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/payroll_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan',
        'modules' => [
            'employees.people.loans' => ['actions' => ['CREATE', 'UPDATE']],
            'employees.loans' => ['actions' => ['CREATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/deduction_type/bulk_create',
        'modules' => [
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/payroll_registers/{hash}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/philippine/company/{id}/deduction_type/bulk_create',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/download/{type}/{uuid}',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ'],
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ'],
            ],
            'payroll.government_forms' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    // [ // Endpoint reserved for audit trail
    //     'method' => 'GET',
    //     'url' => 'TBD',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/schedule/bulk_delete',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/months_with_payroll_loans/{type}',
        'modules' => [
            'payroll.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/bonus/{id}',
        'modules' => [
            'employees.people.bonuses' => [
                'actions' => ['UPDATE'],
            ],
            'employees.bonuses' => [
                'actions' => ['UPDATE'],
            ],
            'payroll.bonuses' => [
                'actions' => ['UPDATE'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/{id}/update_loan_preview',
        'modules' => [
            'employees.people.loans' => ['actions' => ['CREATE', 'UPDATE']],
            'employees.loans' => ['actions' => ['CREATE', 'UPDATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/annual_earning/download',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/workflow_entitlement/has_pending_requests',
        'modules' => [
            'employees.people.approval_groups' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee_request/{id}/download_attachments',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/upload/save',
        'modules' => [
            'employees.loans' => ['actions' => ['CREATE']]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/adjustment/upload/save',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/regular_payroll_job',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/regular_payroll_job/{job_id}',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/payslips',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/unlock/bulk',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => ['actions' => ['UPDATE']],
        ],
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/adjustment/upload',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/other_incomes/{type}',
        'modules' => [
            'payroll.bonuses' => ['actions' => ['READ']],
            'payroll.commissions' => ['actions' => ['READ']],
            'employees.deductions' => ['actions' => ['READ']],
            'employees.adjustments' => ['actions' => ['READ']],
            'employees.allowances' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'PUT',
        'url' => '/team/{id}',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/batch_add/preview',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/announcement',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_credit',
        'modules' => [
            'employees.people.leave_credits' => [
                'actions' => ['CREATE']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/employee/{id}',
        'modules' => [
            'employees.people.basic_information' => [
                'actions' => ['UPDATE']
            ],
            'employees.people.employment_information' => [
                'actions' => ['UPDATE']
            ],
            'employees.people.payroll_information' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/upload/commission',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['DELETE']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{employee_id}/payment_methods',
        'modules' => [
            'employees.people.payment_methods' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/receipts',
        'modules' => [
            'control_panel.subscriptions.receipts_tab' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/payslips',
        'modules' => [
            'ess.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/upload',
        'modules' => [
            'employees.loans' => ['actions' => ['CREATE']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/request/{id}/cancel',
        'modules' => [
            'ess.requests' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.overtime' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.undertime' => [
                'actions' => ['UPDATE']
            ],
            'ess.requests.shift_change' => [
                'actions' => ['UPDATE']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/announcement/{id}',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/other_income/{type}/upload/status',
        'modules' => [
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'employees.adjustments'  => [
                'actions' => ['READ']
            ],
            'employees.allowances'  => [
                'actions' => ['READ']
            ],
            'payroll.bonuses'  => [
                'actions' => ['READ']
            ],
            'payroll.commissions'  => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/government_forms/1604c/available_years',
        'modules' => [
            'payroll.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payrolls',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/payrolls/delete_payroll_job',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/payrolls/delete_payroll_job/{job_id}',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/allowance/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/employee/{id}/termination_informations',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/payslips/generate',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/activate_gap_loans',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/tardiness_rule/bulk_delete',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/deduction/{id}',
        'modules' => [
            'employees.people.deductions' => [
                'actions' => ['UPDATE']
            ],
            'employees.deductions' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'PATCH',
    //     'url' => '/account/role/{id}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'DELETE',
    //     'url' => '/account/roles/bulk_delete',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/time_dispute_request/{id}',
        'modules' => [
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/time_dispute_request/{id}',
        'modules' => [
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/bonus/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/ta_employees',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.attendance_computation' => [
                'actions' => ['READ']
            ],
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/commission/upload',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/{employee_id}/bank',
        'modules' => [
            'employees.people.payment_methods' => [
                'actions' => ['CREATE'],
            ],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll_loan/bulk_delete',
        'modules' => [
            'employees.people.loans' => ['actions' => ['DELETE']],
            'employees.loans' => ['actions' => ['DELETE']],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll_loan_type/subtypes/Pag-ibig',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/payroll_loans/generate_csv',
        'modules' => [
            'employees.people.loans' => ['actions' => ['READ']],
            'employees.loans' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/payroll_loans/download',
        'modules' => [
            'employees.people.loans' => ['actions' => ['READ']],
            'employees.loans' => ['actions' => ['READ']]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/other_income_types/bonus_type',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/schedule/upload/status',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/employee/{employeeId}/other_incomes/adjustment_type',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => 'company/{companyId}/other_incomes/allowance_type/download',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/{id}/update_amortization_preview',
        'modules' => [
            'employees.people.loans' => ['actions' => ['CREATE', 'UPDATE']],
            'employees.loans' => ['actions' => ['CREATE', 'UPDATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE', 'UPDATE']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/workflow_entitlement/upload/save',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/account/payroll_groups',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['READ']
            ],
            'employees.people.loans' => [
                'actions' => ['READ']
            ],
            'employees.people.deductions' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ],
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ],
            'payroll.bonuses' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/team/{id}',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_entitlement/{id}',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/payroll/{payload}/recalculate',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/payroll/{payrollId}/{fileType}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/workflow_entitlements',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'PATCH',
    //     'url' => '/philippine/payroll_group/{id}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/available_product_seats',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payroll_id}/recalculate',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payroll_id}/bank_file',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payroll_id}/bank_advise',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/workflow_entitlements',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/payroll_group/{payroll_group_id}',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => ['actions' => ['UPDATE']]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/available_product_seats',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/bonus/bulk_create',
        'modules' => [
            'employees.people.bonuses' => ['actions' => ['CREATE']],
            'payroll.bonuses' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_entitlement/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/annual_earning/upload/status',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/allowance/{allowanceId}',
        'modules' => [
            'employees.people.allowances' => [
                'actions' => ['UPDATE']
            ],
            'employees.allowances' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/allowance_type/bulk_create',
        'modules' => [
            'company_settings.company_payroll.allowance_types' => [
                'actions' => ['CREATE']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/position/bulk_create',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/rest_day/unassign/{id}',
        'modules' => [
            'time_and_attendance.shifts.assign_rest_day' => [
                'actions' => ['DELETE']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/leave_types',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/shifts',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ],
            'ess.requests.overtime' => [
                'actions' => ['READ']
            ],
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.requests.shift_change' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/company/{id}/default_schedule',
        'modules' => [
            'ess.dashboard' => ['actions' => ['READ']],
            'ess.requests' => ['actions' => ['CREATE']],
            'ess.requests.leaves' => ['actions' => ['READ']],
            'ess.requests.time_dispute' => ['actions' => ['READ']],
            'ess.requests.overtime' => ['actions' => ['READ']],
            'ess.requests.undertime' => ['actions' => ['READ']],
            'ess.requests.shift_change' => ['actions' => ['READ']],
            'ess.calendar' => ['actions' => ['READ']],
            'ess.approvals' => ['actions' => ['READ']],
            'ess.teams' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{company_id}/government_forms/bir_2316/regenerate',
        'modules' => [
            'employees.government_forms' => ['actions' => ['CREATE']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payrollId}/send_single_payslip',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_incomes/allowance_type/download',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/schedule/{id}',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/get_gap_loan_candidates',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/schedule/upload/preview',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/time_attendance_locations/{id}',
        'modules' => [
            'company_settings.company_structure.locations' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_structure.locations.location' => [
                'actions' => ['UPDATE']
            ],
            'company_settings.company_structure.locations.ip_address' => [
                'actions' => ['UPDATE']
            ],
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/holiday/{id}',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/commission/bulk_create',
        'modules' => [
            'employees.people.commissions' => ['actions' => ['CREATE']],
            'payroll.commissions' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{company_id}/banks',
        'modules' => [
            'company_settings.company_payroll.disbursements' => [
                'actions' => ['READ']
            ],
            'employees.people.payment_methods' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/leave_types',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['READ']
            ],
            'employees.people.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'PATCH',
    //     'url' => '/account/role/{roleId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/leave_type/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_type/{id}',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/batch_assign_shifts/save',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/shifts/download/{fileName}',
        'modules' => [
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/other_income/commission/upload/preview',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/final_pay',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['CREATE'],
            ],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/company/{id}/other_income',
        'modules' => [
            'employees.people.allowances' => ['actions' => ['DELETE']],
            'employees.people.bonuses' => ['actions' => ['DELETE']],
            'employees.people.commissions' => ['actions' => ['DELETE']],
            'employees.people.deductions' => ['actions' => ['DELETE']],
            'employees.people.adjustments' => ['actions' => ['DELETE']],
            'employees.deductions' => ['actions' => ['DELETE']],
            'employees.adjustments' => ['actions' => ['DELETE']],
            'employees.allowances' => ['actions' => ['DELETE']],
            'payroll.bonuses' => ['actions' => ['DELETE']],
            'payroll.commissions' => ['actions' => ['DELETE']]

        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_request/upload',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/user/{id}',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ],
            'main_page.profile_information' => [
                'actions' => ['READ']
            ],
            'root.admin' => [
                'actions' => ['READ']
            ],
            'control_panel.device_management' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/user/{id}/initial-data',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ],
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/bonus/upload',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/requests',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.requests' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ],
            'ess.requests.overtime' => [
                'actions' => ['READ']
            ],
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.requests.shift_change' => [
                'actions' => ['READ']
            ],
            'root.ess' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{id:\d+}/members',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/projects',
        'modules' => [
            'company_settings.company_structure.projects' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/countries',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/commission/upload/save',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/termination_informations/{id}',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/default_schedule',
        'modules' => [
            'company_settings.schedule_settings.default_schedule' => [
                'actions' => ['READ']
            ],
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ],
            'main_page.dashboard' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/schedules',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_incomes/adjustment_type/download',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/{id}',
        'modules' => [
            'employees.people.loans' => ['actions' => ['UPDATE']],
            'employees.loans' => ['actions' => ['UPDATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/{subscriptionId}/stats',
        'modules' => [
            'control_panel.subscriptions.licenses_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/rest_day',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employees',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ],

            'employees.people.leave_credits' => [
                'actions' => ['READ']
            ],

            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],

            'employees.people.allowances' => [
                'actions' => ['READ']
            ],

            'employees.people.bonuses' => [
                'actions' => ['READ']
            ],

            'employees.people.commissions' => [
                'actions' => ['READ']
            ],

            'employees.people.loans' => [
                'actions' => ['READ']
            ],

            'employees.people.deductions' => [
                'actions' => ['READ']
            ],

            'employees.people.adjustments' => [
                'actions' => ['READ']
            ],

            'employees.loans' => [
                'actions' => ['READ']
            ],

            'employees.deductions' => [
                'actions' => ['READ']
            ],

            'employees.adjustments' => [
                'actions' => ['READ']
            ],

            'employees.allowances' => [
                'actions' => ['READ']
            ],

            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ],

            'employees.leaves.filed_leaves' => [
                'actions' => ['READ']
            ],

            'employees.annual_earnings' => [
                'actions' => ['READ']
            ],

            'employees.workflows' => [
                'actions' => ['READ']
            ],

            'employees.government_forms' => [
                'actions' => ['READ']
            ],

            'payroll.bonuses' => [
                'actions' => ['READ']
            ],

            'payroll.commissions' => [
                'actions' => ['READ']
            ],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/attendance/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employee/{employeeId}/shifts',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/subscriptions/billing_information',
        'modules' => [
            'control_panel.subscriptions.invoices_tab.billing_information' => [
                'actions' => ['UPDATE'],
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/other_income_type/{id}',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/upload/commission',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/upload/attendance/status',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/subscriptions/invoices/{id}/payments/paypal/capture/{orderId}',
        'modules' => [
            'control_panel.subscriptions.invoices_tab.invoice_history' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/termination_information/{terminationId}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/payroll_register/{payrollId}',
    //     'modules' => []
    // ],
    [
        'method' => 'PUT',
        'url' => '/workflow/{id}',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee_request_module',
        'modules' => [
            'employees.people.approval_groups' => [
                'actions' => ['READ']
            ],
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/upload/bonus/status',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/schedules/generate_csv',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_type',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_request/{id}',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/user/{id}/resend_verification',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['CREATE']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['CREATE']
            // ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{id:\d+}/calendar',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employment_type/bulk_create',
        'modules' => [
            'company_settings.company_structure.employment_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/employee/{employeeId}/basic_pay_adjustment/{adjustmentId}/update',
        'modules' => [
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/leave_credits',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.profile_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/employee/{employee_id}/payment_method',
        'modules' => [
            'employees.people.payment_methods' => [
                'actions' => ['DELETE'],
            ],
        ],
    ],
    [
        'method' => 'PATCH',
        'url' => '/payroll/{id}',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['UPDATE'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/annual_earning/upload/save',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/deduction_type/is_name_available',
        'modules' => [
            'company_settings.company_payroll.deduction_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/upload/allowance',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['DELETE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/annual_earning/{annualEarningId}',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/disbursement_summary',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/subscriptions/{subscriptionId}/stats',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/leave_request',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/employee/events',
    //     'modules' => []
    // ],
    [
        'method' => 'PUT',
        'url' => '/employment_type/{id}',
        'modules' => [
            'company_settings.company_structure.employment_types' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/termination_informations/{id}',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/bonus_type/bulk_create',
        'modules' => [
            'company_settings.company_payroll.bonus_types' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/banks',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{id}/card-designs',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'PUT',
    //     'url' => '/leave_credit/{leaveCreditId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{company_id}/bank',
        'modules' => [
            'company_settings.company_payroll.disbursements' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employees/search',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles.user_management' => [
            //     'actions' => ['READ']
            // ],
            'employees.deductions' => [
                'actions' => ['READ']
            ]
        ]
    ],

    [
        'method' => 'POST',
        'url' => '/company/{id}/workflow/is_name_available',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'GET',
    //     'url' => '/company/{id}/users',
    //     'modules' => [
    //         'company_settings.users_and_roles.user_management' => [
    //             'actions' => ['READ']
    //         ]
    //     ]
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/leave_requests/download',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/tardiness_rule/{id}',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll_loan/{id}/initial_preview',
        'modules' => [
            'employees.people.loans' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{payroll_id}/as-api/attendance',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['CREATE']
            ],
            'payroll.payroll_summary.generate_regular_payroll' => [
                'actions' => ['CREATE']
            ],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/subscriptions/{id}/plan/update',
        'modules' => [
            'control_panel.subscriptions.licenses_tab' => [
                'actions' => ['UPDATE'],
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/location/is_name_available',
        'modules' => [
            'company_settings.company_structure.locations' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/philippine/government_forms/bir/1601c_generate',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/rest_day/{id}',
        'modules' => [
            'time_and_attendance.shifts.assign_rest_day' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/basic_pay_adjustment/{id}',
        'modules' => [
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/basic_pay_adjustment/{adjustmentId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/leave_credits/download',
        'modules' => [
            'employees.people.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/{id}/close',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/hours_worked/leaves',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance.leaves' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/announcement/{announcementId}',
        'modules' => [
            'main_page.announcements' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/announcement/{announcementId}/reply',
        'modules' => [
            'main_page.announcements' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/project/bulk_create',
        'modules' => [
            'company_settings.company_structure.projects' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/department/{id}',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['DELETE']
            ],
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/batch_assign_shifts/status',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/rank/{id}',
        'modules' => [
            'company_settings.company_structure.ranks' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/batch_assign_shifts/status',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/final_pay/{id}',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['UPDATE'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/records/export',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/records/export/job',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.attendance_computation' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/leave_requests',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/default_schedule/bulk_create',
        'modules' => [
            'company_settings.schedule_settings.default_schedule' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/employee_disbursement_method',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/payslip/{id}',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/workflow/bulk_delete',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/employee/{employee_id}/payment_methods/{payment_method_id}',
        'modules' => [
            'employees.people.payment_methods' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/ta_save',
        'modules' => [
            'employees.people' => ['actions' => ['CREATE']],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll_loan_type/subtypes/SSS',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/employee/count',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/leave_credit/upload/save',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/shift/{id}',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/allowance/upload/save',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/bonus/upload/save',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/subscriptions/invoices/{id}/payments/paypal/orders',
        'modules' => [
            'control_panel.subscriptions.invoices_tab.invoice_history' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
        'modules' => [
            'employees.people.allowances' => [
                'actions' => ['READ']
            ],
            'employees.people.bonuses' => [
                'actions' => ['READ']
            ],
            'employees.people.commissions' => [
                'actions' => ['READ']
            ],
            'employees.people.deductions' => [
                'actions' => ['READ']
            ],
            'employees.people.adjustments' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'DELETE',
    //     'url' => '{{POST}} /account/roles/bulk_delete',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/leave_request/bulk_delete',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/role/{roleId}',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/shift',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/companies/{company_id}/withdrawn-employee-invitations',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/tardiness_rule',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll_register/multiple',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/attendance/regenerate/{jobId}',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/leave_request/{id}',
        'modules' => [
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/roles/tasks',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/overtime_request',
        'modules' => [
            'ess.requests.overtime' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/clock_state',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/rank/bulk_delete',
        'modules' => [
            'company_settings.company_structure.ranks' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_request/upload/preview',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/day_hour_rates',
        'modules' => [
            'company_settings.day_hour_rates' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{company_id}/payrolls/{id}/disbursements',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/employee/batch_add/status',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/cost_center/bulk_delete',
        'modules' => [
            'company_settings.company_structure.cost_centers' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{employee_id}/government_forms/bir_2316/{id}',
        'modules' => [
            'employees.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{payroll_id}/job/{job_name}',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/leave_entitlement/is_name_available',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/link/company-authorizations',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan_type',
        'modules' => [
            'company_settings.company_payroll.loan_type_settings' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/company/{id}/locations',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ],
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'employees.adjustments' => [
                'actions' => ['READ']
            ],
            'employees.allowances' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.workflows' => [
                'actions' => ['READ']
            ],
            'employees.government_forms' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['READ']
            ],
            'payroll.bonuses' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/upload/attendance',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']],
            'payroll.payroll_summary.generate_regular_payroll' => ['actions' => ['DELETE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/leave_credit/upload',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/ta_employees/search',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/ta_employees/id',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{type}/is_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.ranks' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.locations' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.employment_types' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['DELETE'],
            ],
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['DELETE'],
            ],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/ess/employee/approvals',
    //     'modules' => []
    // ],
    [
        'method' => 'PATCH',
        'url' => '/employee/{employee_id}/government_forms/bir_2316/{id}',
        'modules' => [
            'employees.government_forms' => ['actions' => ['UPDATE']]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/philippine/commission/{commissionId}',
        'modules' =>  [
            'employees.people.commissions' => [
                'actions' => ['UPDATE'],
            ],
            'employees.commissions' => [
                'actions' => ['UPDATE'],
            ],
            'payroll.commissions' => [
                'actions' => ['UPDATE'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/commission_type/is_name_available',
        'modules' => [
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['CREATE'],
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/upload/bonus',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'DELETE',
        'url' => '/leave_credit/bulk_delete',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/leave_credit/{id}',
        'modules' => [
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [ // Endpoint reserved for audit trail
    //     'method' => 'POST',
    //     'url' => 'TBD',
    //     'modules' => []
    // ],
    [
        'method' => 'DELETE',
        'url' => '/subscriptions/{subscriptionId}/draft',
        'modules' => [
            'control_panel.subscriptions.licenses_tab' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}/has_gap_loan_candidates',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/ess/time_dispute_request',
        'modules' => [
            'ess.requests.time_dispute' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/leave_requests/download',
        'modules' => [
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/shifts/generate_csv',
        'modules' => [
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/team/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/time_types',
        'modules' => [
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/company/bulk_delete',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/role_templates',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/annual_earning/upload',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employment_types',
        'modules' => [
            'company_settings.company_structure.employment_types' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/{id}',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{id}/employees',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/workflow/{id}',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/government_forms/{id}',
        'modules' => [
            'payroll.government_forms' => ['actions' => ['UPDATE']]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{id}/basic_pay_adjustments',
        'modules' => [
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{company_id}/payrolls/{id}/employees',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{id:\d+}',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/payroll_loan_type/{id}',
        'modules' => [
            'company_settings.company_payroll.loan_type_settings' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/holiday/{id}',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll/{id}/calculate/status',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/payroll/{payroll_id}/calculate/status',
        'modules' => [
            'payroll.payroll_summary.generate_regular_payroll' => [
                'actions' => ['READ']
            ],
            'employees.people.termination_information' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/workflow_entitlement/has_pending_requests',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/payroll_group',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/announcement',
        'modules' => [
            'main_page.announcements' => ['actions' => ['CREATE']],
            'main_page.dashboard' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payslip/download_multiple',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/schedule/{id}',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/company/{company_id}/bank',
        'modules' => [
            'company_settings.company_payroll.disbursements' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/tardiness_rule/is_name_available',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['CREATE', 'UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{companyId}/remaining-balance',
    //     'modules' => []/philippine/company/{id}/commission_type/bulk_create
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/project/is_name_available',
        'modules' => [
            'company_settings.company_structure.projects' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_update/payroll_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/commission_type/bulk_create',
        'modules' => [
            'company_settings.company_payroll.commission_types' => [
                'actions' => ['CREATE']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/payroll_group/{id}',
        'modules' => [
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['READ'],
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ'],
            ]
        ],
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/philippine/company/{companyId}/commission_type/bulk_create',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/tardiness_rules',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/annual_earning/group_by_year',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/employees',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{company_id}',
        'modules' => [
            'control_panel.companies' => ['actions' => ['UPDATE']],
            'control_panel.companies.company_information' => ['actions' => ['UPDATE']],
            'control_panel.companies.government_issued_id_number' => ['actions' => ['UPDATE']],
            'control_panel.companies.contact_information' => ['actions' => ['UPDATE']],
            'company_settings.company_structure.company_details' => ['actions' => ['UPDATE']],
            'company_settings.company_structure.company_details.company_information' => ['actions' => ['UPDATE']],
            'company_settings.company_structure.company_details.contact_information' => ['actions' => ['UPDATE']],
            'company_settings.company_structure.company_details.government_issued_id_number' =>
            ['actions' => ['UPDATE']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/lock/bulk',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => ['actions' => ['UPDATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/company/{id}/deduction/bulk_create',
        'modules' => [
            'employees.people.deductions' => ['actions' => ['CREATE']],
            'employees.deductions' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/has_usermatch',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/government_forms',
        'modules' => [
            'payroll.government_forms' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/schedules',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'DELETE',
    //     'url' => '{{POST}} /user/bulk_delete',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/rest_days',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ],
            'ess.requests' => [
                'actions' => ['CREATE']
            ],
            'ess.requests.leaves' => [
                'actions' => ['READ']
            ],
            'ess.requests.time_dispute' => [
                'actions' => ['READ']
            ],
            'ess.requests.overtime' => [
                'actions' => ['READ']
            ],
            'ess.requests.undertime' => [
                'actions' => ['READ']
            ],
            'ess.requests.shift_change' => [
                'actions' => ['READ']
            ],
            'ess.calendar' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/department/bulk_create',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['CREATE']
            ],
            'company_settings.company_structure.organizational_chart.department' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/salpay/link/company-otp-verifications',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll_loan/create/{uid}',
        'modules' => [
            'employees.people.loans' => ['actions' => ['CREATE']],
            'employees.loans' => ['actions' => ['CREATE']],
            'payroll.payroll_summary' => ['actions' => ['CREATE']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/employee/announcements',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/people',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/ess/announcement/{id}/recipient_seen',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/other_income/bonus/upload/preview',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/view/employee/user',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/teams/{teamId:\d+}/attendance/regenerate',
        'modules' => [
            'ess.teams' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/leave_requests',
        'modules' => [
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/leave_types/search',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance.leaves' => [
                'actions' => ['READ']
            ],
            'company_settings.leave_settings' => [
                'actions' => ['READ']
            ],
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/payslip/zipped/{id}/url',
        'modules' => [
            'payroll.payslips' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/tardiness_rule/{id}',
        'modules' => [
            'company_settings.schedule_settings.tardiness_rules' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/schedule',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/schedule/check_in_use',
        'modules' => [
            'control_panel.companies' => [
                'actions' => ['CREATE']
            ],
            'time_and_attendance.schedules' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/payroll_loan/{gap_loan_id}/initial_preview',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/users/{accountDetailsUserId}/companies',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/payroll_loan_types/is_name_available',
        'modules' => [
            'company_settings.company_payroll.loan_type_settings' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/holidays',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['READ']
            ],
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/company/{id}',
        'modules' => [
            'control_panel.companies.company_information' => [
                'actions' => ['READ']
            ],
            'control_panel.companies.government_issued_id_number' => [
                'actions' => ['READ']
            ],
            'control_panel.companies.contact_information' => [
                'actions' => ['READ']
            ],
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // 'company_settings.users_and_roles' => [
            //     'actions' => ['READ']
            // ],
            // 'company_settings.users_and_roles.company_roles' => [
            //     'actions' => ['READ']
            // ],
            'root.admin' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/special',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/special_payroll_job',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/payroll/special_payroll_job/{job_id}',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/tags',
        'modules' => [
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/philippine/employee/form_options',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/payroll/upload/allowance',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/account/users',
        'modules' => [
            'control_panel.users' => [
                'actions' => ['READ']
            ],
            'control_panel.device_management' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/workflow_entitlement/bulk_delete',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/other_income/{bonusId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payrolls',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ],
            'company_settings.company_payroll' => [
                'actions' => ['READ']
            ],
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/payroll_data_counts',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ],
            'company_settings.company_payroll' => [
                'actions' => ['READ']
            ],
            'company_settings.company_payroll.payroll_groups' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/billing_information',
        'modules' => [
            'control_panel.subscriptions.invoices_tab' => [
                'actions' => ['READ']
            ],
            'control_panel.subscriptions.invoices_tab.billing_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/roles',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/payroll_loan/{id}/update_amortization_preview',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'POST',
    //     'url' => '/user/informations',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{company_id}/government_forms/bir_2316',
        'modules' => [
            'employees.government_forms' => ['actions' => ['READ']]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/holiday',
        'modules' => [
            'company_settings.schedule_settings.holidays' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/leave_type/bulk_delete',
        'modules' => [
            'company_settings.leave_settings.leave_types' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/annual_earning/bulk_delete',
        'modules' => [
            'employees.annual_earnings' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/philippine/employee/{id}',
        'modules' => [
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.basic_information' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/project/bulk_delete',
        'modules' => [
            'company_settings.company_structure.projects' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/clock_state/log',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/payroll/upload/attendance',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/ess/affected_employees/search',
        'modules' => [
            'ess.approvals' => [
                'actions' => ['READ']
            ],
            'ess.announcements' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/teams',
        'modules' => [
            'ess.teams' => [
                'actions' => ['READ']
            ],
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/workflow_entitlement/upload/has_pending_requests',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{id}',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['READ']
            ],
            'employees.people.basic_information' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ],
            'employees.people.payroll_information' => [
                'actions' => ['READ']
            ],
            'employees.people.payment_methods' => [
                'actions' => ['READ']
            ],
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['READ']
            ],
            'employees.people.loans' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/termination_informations',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/{id}/basic_pay_adjustment',
        'modules' => [
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/rank/is_in_use',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/workflows',
        'modules' => [
            'company_settings.workflow_automation' => [
                'actions' => ['READ']
            ],
            'employees.people.approval_groups' => [
                'actions' => ['READ']
            ],
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/inactive_employees',
        'modules' => [
            'main_page.announcements' => ['actions' => ['READ']],
            'main_page.dashboard' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{company_id}/dashboard/attendance',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{company_id}/active_employees_count',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/leave_entitlements',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['READ']
            ],
            'employees.people.filed_leaves' => [
                'actions' => ['READ']
            ],
            'employees.leaves.filed_leave' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/salpay/companies/{company_id}/remaining-balance',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/time_attendance_info',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/company/{companyId}/other_income/deduction/upload',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/other_income/{type}/upload',
        'modules' => [
            'employees.deductions' => ['actions' => ['CREATE']],
            'employees.adjustments' => ['actions' => ['CREATE']],
            'employees.allowances' => ['actions' => ['CREATE']],
            'payroll.bonuses' => ['actions' => ['CREATE']],
            'payroll.commissions' => ['actions' => ['CREATE']],
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/employment_type/bulk_delete',
        'modules' => [
            'company_settings.company_structure.employment_types' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/payroll/{id}',
        'modules' => [
            'payroll.payroll_summary' => ['actions' => ['DELETE']]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/employee/{employeeId}/basic_pay_adjustment/delete',
        'modules' => [
            'employees.people.basic_pay_adjustments' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/months_with_payroll_loans/pag-ibig',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/shift_change_request',
        'modules' => [
            'ess.requests.shift_change' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/account/role/{id}',
    //     'modules' => []
    // ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/other_income/allowance/upload/preview',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/ess/teams/{teamId:\d+}/members/{memberId:\d+}/rest_day',
        'modules' => [
            'ess.teams' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/commission/{jobId}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/workflow_entitlement/upload/status',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/schedules',
        'modules' => [
            'ess.requests.shift_change' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/announcements',
        'modules' => [
            'main_page.announcements' => [
                'actions' => ['READ']
            ],
            'main_page.dashboard' => [
                'actions' => ['READ']
            ],
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/company/{companyId}/employee/{employeeId}/other_incomes/commission_type',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/default_schedule/related_requests_to_updated_days_of_week',
        'modules' => [
            'company_settings.schedule_settings.default_schedule' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/leave_entitlement/{id}',
        'modules' => [
            'company_settings.leave_settings.leave_entitlements' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/teams',
        'modules' => [
            'company_settings.company_structure.teams' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['READ']
            ]
        ]
    ],
    // [
    //     'method' => 'POST',
    //     'url' => '/account/role/is_name_available',
    //     'modules' => []
    // ],
    [
        'method' => 'POST',
        'url' => '/employee/batch_add/save',
        'modules' => [
            'employees.people' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    // [
    //     'method' => 'DELETE',
    //     'url' => '{{POST}} /company/{companyId}/other_income_type',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/termination_informations/{id}',
        'modules' => [
            'employees.people.termination_information' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/positions',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['READ']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['READ']
            ],
            'employees.people' => [
                'actions' => ['READ']
            ],
            'employees.people.employment_information' => [
                'actions' => ['READ']
            ],
            'employees.loans' => [
                'actions' => ['READ']
            ],
            'employees.deductions' => [
                'actions' => ['READ']
            ],
            'employees.adjustments' => [
                'actions' => ['READ']
            ],
            'employees.allowances' => [
                'actions' => ['READ']
            ],
            'employees.leaves.leave_credits' => [
                'actions' => ['READ']
            ],
            'employees.workflows' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.schedules' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.shifts' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_special_payroll_final_pay_run' => [
                'actions' => ['READ']
            ],
            'payroll.bonuses' => [
                'actions' => ['READ']
            ],
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/{id}/workflow_entitlements',
        'modules' => [
            'employees.people.approval_groups' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/other_incomes/{type}/download',
        'modules' => [
            'employees.people.allowances' => ['actions' => ['READ']],
            'employees.people.bonuses' => ['actions' => ['READ']],
            'employees.people.commissions' => ['actions' => ['READ']],
            'employees.people.deductions' => ['actions' => ['READ']],
            'employees.people.adjustments' => ['actions' => ['READ']],
            'employees.deductions' => ['actions' => ['READ']],
            'employees.adjustments' => ['actions' => ['READ']],
            'employees.allowances' => ['actions' => ['READ']],
            'payroll.bonuses' => ['actions' => ['READ']],
            'payroll.commissions' => ['actions' => ['READ']]
        ]
    ],
    // [
    //     'method' => 'GET',
    //     'url' => '/download/govt_form/{id}',
    //     'modules' => []
    // ],
    [
        'method' => 'GET',
        'url' => '/overtime_request/{id}',
        'modules' => [
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/overtime_request/{id}',
        'modules' => [
            'ess.requests.overtime' => [
                'actions' => ['READ']
            ],
            'ess.approvals' => [
                'actions' => ['READ']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/announcements/download',
        'modules' => [
            'main_page.announcements' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'PUT',
        'url' => '/notifications/{id}/clicked',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/admin_dashboard/calendar_data',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/admin_dashboard/calendar_data_counts',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/position/{id}',
        'modules' => [
            'company_settings.company_structure.organizational_chart' => [
                'actions' => ['DELETE']
            ],
            'company_settings.company_structure.organizational_chart.position' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employee/events',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/user/approvals',
        'modules' => [
            'time_and_attendance.approvals_list' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/user/last_active_company',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/attendance_stats',
        'modules' => [
            'main_page.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/notifications',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/request/send_message',
        'modules' => [
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/request/bulk_decline',
        'modules' => [
            'time_and_attendance.approvals_list' => [
                'actions' => ['UPDATE']
            ],
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/request/bulk_approve',
        'modules' => [
            'time_and_attendance.approvals_list' => [
                'actions' => ['UPDATE']
            ],
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/request/bulk_cancel',
        'modules' => [
            'time_and_attendance.approvals_list' => [
                'actions' => ['UPDATE']
            ],
            'time_and_attendance.approvals_list.request_details' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/announcements/unread',
        'modules' => [
            'ess.announcements' => [
                'actions' => ['READ']
            ],
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/accounts/{accountId}/roles',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['READ']
            ],
            'control_panel.users' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/accounts/{accountId}/roles/{roleId}',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/accounts/{accountId}/roles',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/accounts/{accountId}/roles/{roleId}',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/accounts/{accountId}/roles/{roleId}',
        'modules' => [
            'control_panel.roles' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'GET',
    //     'url' => '/companies/{companyId}/roles',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => [
    //             'actions' => ['READ']
    //         ],
    //         'company_settings.users_and_roles.user_management' => [
    //             'actions' => ['READ']
    //         ]
    //     ]
    // ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'GET',
    //     'url' => '/companies/{companyId}/roles/{roleId}',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => [
    //             'actions' => ['READ']
    //         ]
    //     ]
    // ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'POST',
    //     'url' => '/companies/{companyId}/roles',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => [
    //             'actions' => ['CREATE']
    //         ]
    //     ]
    // ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'PUT',
    //     'url' => '/companies/{companyId}/roles/{roleId}',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => [
    //             'actions' => ['UPDATE']
    //         ]
    //     ]
    // ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'DELETE',
    //     'url' => '/companies/{companyId}/roles/{roleId}',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => [
    //             'actions' => ['DELETE']
    //         ]
    //     ]
    // ],
    [
        'method' => 'PUT',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements/{disbursementId}',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/resend',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/verify',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/payrolls/{payrollId}/employees',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/card-designs',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/invite-shipping-details',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/employees',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/employee-invitations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/salpay/companies/{companyId}/employee-invitations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/salpay/companies/{companyId}/employee-reinvitations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/salpay/companies/{companyId}/employee-unlink-invitation',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/withdrawn-employee-invitations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/remaining-balance',
        'modules' => [
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/users/{userId}/companies',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/link/company-authorizations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/link/company-integrations',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/link/company-otp-verifications',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/payslips/unread',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/user/{id}',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/notifications',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/ess/notifications_status',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/notifications_status',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'PUT',
        'url' => '/ess/notifications/{id}/clicked',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/batch_assign_shifts/validate',
        'modules' => [
            'time_and_attendance.shifts.assign_shift' => ['actions' => ['CREATE']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/shift/overlapping_requests',
        'modules' => [
            'time_and_attendance.shifts' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'PUT',
        'url' => '/shift/unassign/{id}',
        'modules' => [
            'time_and_attendance.shifts' => ['actions' => ['UPDATE']],
        ],
    ],
    // [ // Deprecated due to ticket 7702: Removal of company settings users and roles option
    //     'method' => 'GET',
    //     'url' => '/accounts/{accountId}/companies/{companyId}/essential_data',
    //     'modules' => [
    //         'company_settings.users_and_roles.company_roles' => ['actions' => ['READ']],
    //     ],
    // ],
    [
        'method' => 'GET',
        'url' => '/user/details/{userId}',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ'],
            ],
            'ess.requests.leaves' => [
                'actions' => ['READ'],
            ],
            'ess.profile_information' => [
                'actions' => ['READ'],
            ],
            'root.dashboard' => [
                'actions' => ['READ'],
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/ess/employee/user',
        'modules' => [
            'root.ess' => [
                'actions' => ['READ'],
            ],
            'ess.requests.leaves' => [
                'actions' => ['READ'],
            ],
            'ess.profile_information' => [
                'actions' => ['READ'],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => '/company/{companyId}/shifts',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => ['actions' => ['READ']]
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/employee',
        'modules' => [
            'employees.people' => ['actions' => ['CREATE']]
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/philippine/employee',
        'modules' => [
            'employees.people' => ['actions' => ['CREATE']]
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/workflow_entitlement/{id}/has_pending_requests',
        'modules' => [
            'employees.workflows' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/affected_entities/search',
        'modules' => [
            'company_settings.workflow_automation' => ['actions' => ['READ']],
        ],
    ],
    [
        'method' => 'POST',
        'url' => '/attendance/records/upload',
        'modules' => [
            'time_and_attendance.attendance_computation' => [
                'actions' => ['CREATE']
            ],
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['CREATE']
            ],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/employees/export',
        'modules' => [
            'employees.people.basic_information' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/employees/generate_masterfile/status',
        'modules' => [
            'employees.people' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{id}/employees/generate_masterfile',
        'modules' => [
            'employees.people' => ['actions' => ['READ']],
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/ess/change_password',
        'modules' => [
            'ess.profile_information' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/api/face_pass/ra08t/devices',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/api/face_pass/ra08t/devices/{id}',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/api/face_pass/ra08t/users',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/api/face_pass/ra08t/users/{user_id}',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/api/face_pass/ra08t/users/{user_id}/sync',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/api/face_pass/ra08t/users/{user_id}/devices',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/api/face_pass/ra08t/accounts/unlinked_device_users',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/api/face_pass/ra08t/accounts/unlinked_device_users',
        'modules' => [
            'control_panel.device_management' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/salpay-business/administrators',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/salpay-account/administrators',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/salpay/companies/{companyId}/salpay-account/administrators',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/salpay/companies/{companyId}/salpay-account/administrators/{adminId}',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/salpay/companies/{companyId}/salpay-settings',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/salpay-settings/{userId}',
        'modules' => [
            'control_panel.salpay_integration' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/salpay/companies/{companyId}/integration-status',
        'modules' => [
            'root.admin' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/company/{companyId}/employees_by_ids',
        'modules' => [
            'time_and_attendance.attendance_computation.attendance' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/upload/picture/{id}',
        'modules' => [
            'employees.people.basic_information' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/employee/employees_for_payroll_validation',
        'modules' => [
            'employees.people.payroll_information' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_regular_payroll' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/employee/employees_for_payroll_validation/{job_id}',
        'modules' => [
            'employees.people.payroll_information' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary.generate_regular_payroll' => [
                'actions' => ['READ']
            ],
            'payroll.payroll_summary' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/audit-trail/logs',
        'modules' => [
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/audit-trail/logs/export',
        'modules' => [
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/account/allusers',
        'modules' => [
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/audit-trail/logs/modules',
        'modules' => [
            'control_panel.audit_trail' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/company/{id}/max_clock_outs',
        'modules' => [
            'company_settings.max_clock_outs' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/max_clock_out',
        'modules' => [
            'company_settings.max_clock_outs' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/max_clock_out/{id}',
        'modules' => [
            'company_settings.max_clock_outs' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/max_clock_out/bulk_delete',
        'modules' => [
            'company_settings.max_clock_outs' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/max_clock_out/{id}',
        'modules' => [
            'company_settings.max_clock_outs' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/max_clock_out/get_rule/{emp_id}',
        'modules' => [
            'ess.dashboard' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'GET',
        'url' => '/subscriptions/account_deletion',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['READ']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/subscriptions/account_deletion',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['CREATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/subscriptions/account_deletion/{id}/confirm_otp',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'POST',
        'url' => '/subscriptions/account_deletion/{id}/resend_otp',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'PATCH',
        'url' => '/subscriptions/account_deletion/{id}/confirm',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['UPDATE']
            ]
        ]
    ],
    [
        'method' => 'DELETE',
        'url' => '/subscriptions/account_deletion/{id}/cancel',
        'modules' => [
            'control_panel.subscriptions.subscriptions_tab' => [
                'actions' => ['DELETE']
            ]
        ]
    ],
];
