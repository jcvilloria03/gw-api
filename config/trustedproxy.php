<?php

use Illuminate\Http\Request;

$trustedProxies = env('TRUSTED_PROXIES', '**');
if (!($trustedProxies === '*' || $trustedProxies === '**')) {
    $trustedProxies = explode(',', $trustedProxies);
}

return [
    'proxies' => $trustedProxies,
    'headers' => [
        Request::HEADER_FORWARDED => env('TRUSTED_HEADER_FORWARDED', 'FORWARDED'),
        Request::HEADER_CLIENT_IP => env('TRUSTED_HEADER_CLIENT_IP', 'X_FORWARDED_FOR'),
        Request::HEADER_CLIENT_HOST => env('TRUSTED_HEADER_CLIENT_HOST', 'X_FORWARDED_HOST'),
        Request::HEADER_CLIENT_PROTO => env('TRUSTED_HEADER_CLIENT_PROTO', 'X_FORWARDED_PROTO'),
        Request::HEADER_CLIENT_PORT => env('TRUSTED_HEADER_CLIENT_PORT', 'X_FORWARDED_PORT'),
    ],
];
