<?php

return [
    'exchange' => env('EVENT_STREAM_EXCHANGE'),
    'queue'    => env('EVENT_STREAM_QUEUE'),

    'mapping' => [
        'namespace' => 'App\EventStream',
        'handlers'  => [
            'license' => [
                'applied' => 'LicenseAppliedEventHandler'
            ],
            'subscription' => [
                'updated' => 'SubscriptionUpdatedEventHandler'
            ],
            'userrole' => [
                'created' => 'UserRoleCreatedEventHandler'
            ],
            'auth0user' => [
                'created' => 'Auth0UserCreatedEventHandler'
            ],
            'democompany' => [
                'created' => 'DemoCompanyCreatedEventHandler'
            ]
        ]
    ]
];
