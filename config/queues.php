<?php

return [
    'audit_queue' => env('AUDIT_QUEUE', 'dev_audit'),
    'audit_trail_queue' => env('AUDIT_TRAIL_QUEUE', 'dev_audit_trail'),
    'audit_trail_exchange' => env('AUDIT_TRAIL_EXCHANGE', 'dev_audit_trail_exchange'),
    'index_queue' => env('INDEX_QUEUE', 'dev_index'),
    'employee_validation_queue' => env('EMPLOYEE_VALIDATION_QUEUE', 'dev_employee_validation'),
    'people_validation_queue' => env('PEOPLE_VALIDATION_QUEUE', 'dev_people_validation'),
    'people_update_validation_queue' => env('PEOPLE_UPDATE_VALIDATION_QUEUE', 'dev_people_update_validation'),
    'employee_write_queue' => env('EMPLOYEE_WRITE_QUEUE', 'dev_employee_write'),
    'people_write_queue' => env('PEOPLE_WRITE_QUEUE', 'dev_people_write'),
    'employee_uploads_queue' => env('EMPLOYEE_UPLOADS_QUEUE', 'dev_employee_upload_response'),
    'employee_update_validation_queue' => env('EMPLOYEE_UPDATE_VALIDATION_QUEUE', 'dev_employee_update_validation'),
    'employee_update_upload_response_queue' => env(
        'EMPLOYEE_UPDATE_UPLOAD_RESPONSE_QUEUE',
        'dev_employee_update_upload_response'
    ),
    'employee_time_attendance_validation_queue' => env(
        'EMPLOYEE_TIME_ATTENDANCE_VALIDATION_QUEUE',
        'dev_employee_time_attendance_validation'
    ),
    'employee_time_attendance_write_queue' => env(
        'EMPLOYEE_TIME_ATTENDANCE_WRITE_QUEUE',
        'dev_employee_time_attendance_write'
    ),
    'employee_time_attendance_uploads_queue' => env(
        'EMPLOYEE_TIME_ATTENDANCE_UPLOADS_QUEUE',
        'dev_employee_time_attendance_upload_response'
    ),
    'payroll_calculation_queue' => env('PAYROLL_CALCULATION_QUEUE', 'dev_payroll_calculation'),
    'payroll_calculation_response_queue' => env(
        'PAYROLL_CALCULATION_RESPONSE_QUEUE',
        'dev_payroll_calculation_response'
    ),
    'payroll_upload_validation_queue' => env('PAYROLL_UPLOAD_VALIDATION_QUEUE', 'dev_payroll_upload_validation'),
    'payroll_upload_response_queue' => env('PAYROLL_UPLOAD_RESPONSE_QUEUE', 'dev_payroll_upload_response'),
    'payslip_generation_queue' => env('PAYSLIP_GENERATION_QUEUE', 'dev_payslip_generation'),
    'payslip_generation_response_queue' => env(
        'PAYSLIP_GENERATION_RESPONSE_QUEUE',
        'dev_payslip_generation_response'
    ),
    'schedule_upload_response_queue' => env('SCHEDULE_UPLOAD_RESPONSE_QUEUE', 'dev_schedule_upload_response'),
    'schedule_write_queue' => env('SCHEDULE_WRITE_QUEUE', 'dev_schedule_write'),
    'schedule_validation_queue' => env('SCHEDULE_VALIDATION_QUEUE', 'dev_schedule_validation'),
    'broadcast_notification_queue' => env('BROADCAST_NOTIFICATION_QUEUE', 'dev_broadcast_notification'),
    'broadcast_announcement_queue' => env('BROADCAST_ANNOUNCEMENT_QUEUE', 'dev_broadcast_announcement'),
    'broadcast_ess_request_queue' => env('BROADCAST_ESS_REQUEST_QUEUE', 'dev_broadcast_ess_request'),
    'payroll_group_validation_queue' => env('PAYROLL_GROUP_VALIDATION_QUEUE', 'dev_payroll_group_upload_validation'),
    'payroll_group_upload_response_queue' => env('PAYROLL_GROUP_UPLOAD_RESPONSE_QUEUE', 'dev_payroll_group_response'),
    'payroll_group_write_queue' => env('PAYROLL_GROUP_WRITE_QUEUE', 'dev_payroll_group_write_queue'),
    'leave_credit_upload_response_queue' => env(
        'LEAVE_CREDIT_UPLOAD_RESPONSE_QUEUE',
        'dev_leave_credit_upload_response'
    ),
    'leave_credit_validation_queue' => env('LEAVE_CREDIT_VALIDATION_QUEUE', 'dev_leave_credit_validation'),
    'leave_credit_write_queue' => env('LEAVE_CREDIT_WRITE_QUEUE', 'dev_leave_credit_write'),
    'other_income_validation_queue' => env('OTHER_INCOME_VALIDATION_QUEUE', 'dev_other_income_upload_validation'),
    'other_income_upload_response_queue' => env('OTHER_INCOME_UPLOAD_RESPONSE_QUEUE', 'dev_other_income_response'),
    'other_income_write_queue' => env('OTHER_INCOME_WRITE_QUEUE', 'dev_other_income_write_queue'),
    'leave_request_upload_response_queue' => env(
        'LEAVE_REQUEST_UPLOAD_RESPONSE_QUEUE',
        'dev_leave_request_upload_response'
    ),
    'leave_request_validation_queue' => env('LEAVE_REQUEST_VALIDATION_QUEUE', 'leave_request_validation'),
    'leave_request_write_queue' => env('LEAVE_REQUEST_WRITE_QUEUE', 'dev_leave_request_write'),
    'workflow_entitlement_upload_response_queue' => env(
        'WORKFLOW_ENTITLEMENT_UPLOAD_RESPONSE_QUEUE',
        'dev_workflow_entitlement_upload_response'
    ),
    'workflow_entitlement_validation_queue' => env(
        'WORKFLOW_ENTITLEMENT_VALIDATION_QUEUE',
        'dev_workflow_entitlement_validation'
    ),
    'workflow_entitlement_write_queue' => env('WORKFLOW_ENTITLEMENT_WRITE_QUEUE', 'dev_workflow_entitlement_write'),
    'annual_earning_write_queue' => env('ANNUAL_EARNING_WRITE_QUEUE', 'dev_annual_earning_write_queue'),
    'annual_earning_upload_response_queue' => env(
        'ANNUAL_EARNING_UPLOAD_RESPONSE_QUEUE',
        'dev_annual_earning_upload_response_queue'
    ),
    'annual_earning_validation_queue' => env(
        'ANNUAL_EARNING_VALIDATION_QUEUE',
        'dev_annual_earning_validation_queue'
    ),
    'earning_write_queue' => env('EARNING_WRITE_QUEUE', 'dev_earning_write_queue'),
    'earning_upload_response_queue' => env(
        'EARNING_UPLOAD_RESPONSE_QUEUE',
        'dev_earning_upload_response_queue'
    ),
    'earning_validation_queue' => env(
        'EARNING_VALIDATION_QUEUE',
        'dev_earning_validation_queue'
    ),
    'payroll_loan_validation_queue' => env('PAYROLL_LOAN_VALIDATION_QUEUE', 'local_payroll_loan_validation_queue'),
    'payroll_loan_upload_response_queue' => env(
        'PAYROLL_LOAN_UPLOAD_RESPONSE_QUEUE',
        'local_payroll_loan_upload_response_queue'
    ),
    'payroll_loan_write_queue' => env('PAYROLL_LOAN_WRITE_QUEUE', 'local_payroll_loan_write_queue'),
    'user_profile_validation_queue' => env('USER_PROFILE_VALIDATION_QUEUE', 'local_user_profile_validation'),
    'user_profile_upload_response_queue' => env(
        'USER_PROFILE_UPLOAD_RESPONSE_QUEUE',
        'local_user_profile_upload_response'
    ),
    'user_profile_write_queue' => env('USER_PROFILE_WRITE_QUEUE', 'local_user_profile_write'),
    'employees_update_status_queue' => env(
        'EMPLOYEES_UPDATE_STATUS_QUEUE',
        'local_employees_update_status_queue'
    ),
    'employees_update_send_verification_email_queue' => env(
        'EMPLOYEES_UPDATE_SEND_VERIFICATION_EMAIL_QUEUE',
        'local_employees_update_send_verification_email_queue'
    ),
    'employee_batch_assign_shifts_queue' => env(
        'EMPLOYEE_BATCH_ASSIGN_SHIFTS_QUEUE',
        'dev_employee_batch_assign_shifts'
    ),
    'leave_credits_download_queue' => env(
        'LEAVE_CREDITS_DOWNLOAD_QUEUE',
        'dev_leave_credits_download_queue'
    ),
    'leave_requests_download_queue' => env(
        'LEAVE_REQUESTS_DOWNLOAD_QUEUE',
        'dev_leave_requests_download_queue'
    ),
    'user_essential_data_update_queue' => env(
        'USER_ESSENTIAL_DATA_UPDATE_QUEUE',
        'dev_user_essential_data_update_queue'
    ),
    'account_essential_data_update_queue' => env(
        'ACCOUNT_ESSENTIAL_DATA_UPDATE_QUEUE',
        'dev_account_essential_data_update_queue'
    ),
    'assign_shifts_write_queue' => env(
        'ASSIGN_SHIFTS_WRITE_QUEUE',
        'dev_assign_shifts_write'
    ),
    'payroll_loans_download_queue' => env(
        'PAYROLL_LOANS_DOWNLOAD_QUEUE',
        'local_payroll_loans_download_queue'
    ),
    'demo_company_gw_queue' => env(
        'DEMO_COMPANY_GW_QUEUE',
        'dev_demo_company_gw'
    ),
    'account_creation' => env(
        'ACCOUNT_CREATION_QUEUE',
        'authn_account_creation'
    ),
    'authn_user_verified' => env(
        'GATEWAY_AUTHN_USER_VERIFIED_QUEUE',
        'gateway_authn_user_verified'
    ),
    'revoke_authn_tokens' => env(
        'GATEWAY_REVOKE_AUTHN_TOKENS_QUEUE',
        'gateway_revoke_authn_tokens'
    ),
    'authn_user_create_update' => env(
        'GATEWAY_AUTHN_USER_CREATE_UPDATE_QUEUE',
        'gateway_authn_user_create_update'
    ),
];
