<?php

return [
    'is_enable' => env('ENABLE_AUDIT_TRAIL', false),
    'enabled_modules' => [
        // SUBSCRIPTIONS
        'control_panel.subscriptions.invoices_tab',
        'control_panel.subscriptions.licenses_tab',
        'control_panel.subscriptions.receipts_tab',
        'control_panel.subscriptions.subscriptions_tab',
        // EMPLOYEES
        'employees.adjustments',
        'employees.allowances',
        'employees.deductions',
        'employees.annual_earnings',
        'employees.loans',
        'employees.people',
        'employees.people.adjustments',
        'employees.people.allowances',
        'employees.people.annual_earnings',
        'employees.people.approval_groups',
        'employees.people.basic_information',
        'employees.people.basic_pay_adjustments',
        'employees.people.bonuses',
        'employees.people.commissions',
        'employees.people.deductions',
        'employees.people.filed_leaves',
        'employees.people.leave_credits',
        'employees.people.loans',
        'employees.people.payment_methods',
        'employees.people.termination_information',
        'employees.leaves.leave_credits',
        'employees.leaves.filed_leave',
        // PAYROLL
        'payroll.bonuses',
        'payroll.commissions',
        'payroll.payroll_summary',
        'payroll.payroll_summary.generate_regular_payroll',
        // TIME AND ATTENDANCE
        'time_and_attendance.attendance_computation',
        'time_and_attendance.attendance_computation.attendance',
        'time_and_attendance.schedules',
        'time_and_attendance.shifts',
        'time_and_attendance.shifts.assign_rest_day',
        'time_and_attendance.shifts.assign_shift',
        'company_settings.schedule_settings',
        'company_settings.max_clock_outs'
    ],
    'enabled_non_authz_endpoints' => [
        '/auth/user/login'
    ],
    'module_names' => [
        // URIs
        '/user/{id}/initial-data' => 'Login',
        '/ess/change_password' => 'ESS Change Password',
        '/employee/{id}' => 'Employee',
        // Authz Entities
        // 'company_settings.company_payroll' => 'Company Payroll',
        // 'company_settings.company_payroll.allowance_types' => 'Allowance Types',
        // 'company_settings.company_payroll.bonus_types' => 'Bonus Types',
        // 'company_settings.company_payroll.commission_types' => 'Commission Types',
        // 'company_settings.company_payroll.deduction_types' => 'Deduction Types',
        // 'company_settings.company_payroll.disbursements' => 'Disbursements',
        // 'company_settings.company_payroll.loan_type_settings' => 'Loan Types',
        // 'company_settings.company_payroll.payroll_groups' => 'Payroll Groups',
        // 'company_settings.company_structure' => 'Company',
        // 'company_settings.company_structure.company_details' => 'Company',
        // 'company_settings.company_structure.company_details.company_information' => 'Company',
        // 'company_settings.company_structure.company_details.contact_information' => 'Company',
        // 'company_settings.company_structure.company_details.government_issued_id_number' => 'Company',
        // 'company_settings.company_structure.cost_centers' => 'Cost Center',
        // 'company_settings.company_structure.employment_types' => 'Employment Types',
        // 'company_settings.company_structure.locations' => 'Locations',
        // 'company_settings.company_structure.locations.ip_address' => 'Locations',
        // 'company_settings.company_structure.locations.location' => 'Locations',
        // 'company_settings.company_structure.organizational_chart' => 'Organizational Chart',
        // 'company_settings.company_structure.organizational_chart.department' => 'Organizational Chart',
        // 'company_settings.company_structure.organizational_chart.position' => 'Organizational Chart',
        // 'company_settings.company_structure.projects' => 'Projects',
        // 'company_settings.company_structure.ranks' => 'Ranks',
        // 'company_settings.company_structure.teams' => 'Teams',
        // 'company_settings.day_hour_rates' => 'Day/Hour Rates',
        // 'company_settings.leave_settings' => 'Leave Settings',
        // 'company_settings.leave_settings.leave_entitlements' => 'Leave Entitlements',
        // 'company_settings.leave_settings.leave_types' => 'Leave Types',
        // 'company_settings.schedule_settings' => 'Schedule Settings',
        // 'company_settings.schedule_settings.default_schedule' => 'Default Schedule',
        // 'company_settings.schedule_settings.holidays' => 'Holidays',
        // 'company_settings.schedule_settings.night_shift' => 'Night Shift Rule',
        // 'company_settings.schedule_settings.tardiness_rules' => 'Tardiness Rule',
        // 'company_settings.workflow_automation' => 'Workflow Automation',
        'company_settings.max_clock_outs' => 'Maximum Clock-Out Rules',
        // 'control_panel.audit_trail' => 'Audit Trail',
        // 'control_panel.companies' => 'Company',
        // 'control_panel.companies.company_information' => 'Company',
        // 'control_panel.companies.contact_information' => 'Company',
        // 'control_panel.companies.government_issued_id_number' => 'Company',
        // 'control_panel.device_management' => 'Device Management',
        // 'control_panel.roles' => 'Roles',
        // 'control_panel.salpay_integration' => 'SALPay Integration', # to be improved
        'control_panel.subscriptions.invoices_tab' => 'Subscription Invoices',
        'control_panel.subscriptions.invoices_tab.billing_information' => 'Subscription Billing Information',
        'control_panel.subscriptions.invoices_tab.invoice_history' => 'Subscription Invoices',
        'control_panel.subscriptions.licenses_tab' => 'Subscription Licenses',
        'control_panel.subscriptions.receipts_tab' => 'Subscription Receipts',
        'control_panel.subscriptions.subscriptions_tab' => 'Subscriptions',
        // 'control_panel.users' => 'Users',
        'employees.adjustments' => 'Adjustments',
        'employees.allowances' => 'Allowances',
        'employees.annual_earnings' => 'Annual Earnings',
        'employees.bonuses' => 'Bonuses',
        'employees.commissions' => 'Commissions',
        'employees.deductions' => 'Deductions',
        'employees.government_forms' => 'Government_forms',
        'employees.leaves.filed_leave' => 'Filed Leaves',
        'employees.leaves.filed_leaves' => 'Filed Leaves',
        'employees.leaves.leave_credits' => 'Leave Credits',
        // 'employees.loans' => 'Loans',
        'employees.people' => 'Employees',
        'employees.people.adjustments' => 'Employee Adjustments',
        'employees.people.allowances' => 'Employee Allowances',
        'employees.people.annual_earnings' => 'Employee Annual Earnings',
        'employees.people.approval_groups' => 'Employee Approval Groups',
        'employees.people.basic_information' => 'Employee Basic Information',
        'employees.people.basic_pay_adjustments' => 'Employee Basic Pay Adjustments',
        'employees.people.bonuses' => 'Employee Bonuses',
        'employees.people.commissions' => 'Employee Commissions',
        'employees.people.deductions' => 'Employee Deductions',
        'employees.people.employment_information' => 'Employee Employment Details',
        'employees.people.filed_leaves' => 'Employee Filed Leaves',
        'employees.people.leave_credits' => 'Employee Leave Credits',
        'employees.people.loans' => 'Employee Loans',
        'employees.people.payment_methods' => 'Employee Payment Methods',
        'employees.people.payroll_information' => 'Employee Payroll Information',
        'employees.people.termination_information' => 'Employee Termination Information',
        'employees.workflows' => 'Workflows',
        // 'ess.announcements' => 'ESS Announcements',
        // 'ess.approvals' => 'Approvals',
        // 'ess.calendar' => 'Calendar',
        // 'ess.dashboard' => 'ESS Timeclock', #clockin/clockout
        // 'ess.payslips' => 'ESS Payslips',
        // 'ess.profile_information' => 'ESS Profile Information',
        // 'ess.requests' => 'ESS Requests',
        // 'ess.requests.leaves' => 'ESS Leave Request',
        // 'ess.requests.overtime' => 'ESS Overtime Request',
        // 'ess.requests.shift_change' => 'ESS Shift Change Request',
        // 'ess.requests.time_dispute' => 'ESS Time Correct Request',
        // 'ess.requests.undertime' => 'ESS Undertime Request',
        // 'ess.teams' => 'ESS Teams',
        // 'main_page.announcements' => 'Admin Announcements',
        // 'main_page.dashboard' => 'Admin Dashboard',
        // 'main_page.profile_information' => 'Admin Profile Information',
        'payroll.bonuses' => 'Payroll Bonuses',
        'payroll.commissions' => 'Payroll Commissions',
        'payroll.government_forms' => 'Payroll Government Forms',
        'payroll.payroll_summary' => 'Payroll Summary',
        'payroll.payroll_summary.generate_regular_payroll' => 'Regular Pay Run',
        'payroll.payroll_summary.generate_special_payroll_final_pay_run' => 'None Regular Pay Run', #Special/Final
        'payroll.payslips' => 'Payslips',
        'root.admin' => 'Admin',
        // 'root.ess' => 'ESS',
        // 'time_and_attendance.approvals_list' => 'Approvals',
        // 'time_and_attendance.approvals_list.request_details' => 'Approval List Request Details',
        'time_and_attendance.attendance_computation' => 'Attendance Computation',
        'time_and_attendance.attendance_computation.attendance' => 'Attendance Computation',
        'time_and_attendance.attendance_computation.attendance.computed_attendance' => 'Attendance Computation',
        'time_and_attendance.attendance_computation.attendance.leaves' => 'Attendance Computation',
        'time_and_attendance.attendance_computation.attendance.time_records' => 'Attendance Computation',
        'time_and_attendance.schedules' => 'Schedules',
        'time_and_attendance.shifts' => 'Shifts',
        'time_and_attendance.shifts.assign_rest_day' => 'Shifts',
        'time_and_attendance.shifts.assign_shift' => 'Shifts',
    ],
    'transaction_types' => [
        'GET' => 'VIEW',
        'READ' => 'VIEW',
        'ADD' => 'ADD',
        'POST' => 'ADD',
        'CREATE' => 'ADD',
        'UPDATE' => 'UPDATE',
        'PUT' => 'UPDATE',
        'PATCH' => 'UPDATE',
        'DELETE' => 'DELETE',

        // FOR EXPORT/DOWNLOAD ENDPOINTS
        'EXPORT' => 'EXPORT'
    ],

    // Non Authz endpoints and multi action endpoints
    'uri_actions' => [
        '/auth/user/login' => 'ADD',
        '/auth/reset_password' => 'UPDATE',
        '/ess/reset_password' => 'UPDATE',
        '/company/{id}/annual_earning/set' => 'UPDATE',
        '/company/{id}/hours_worked/leaves/bulk_create_or_update_or_delete' => 'UPDATE',
        '/employee/{id}/workflow_entitlement/create_or_update' => 'UPDATE',
        '/workflow_entitlement/bulk_create_or_update' => 'UPDATE',
        '/company/{id}/time_records/bulk_create_or_delete' => 'UPDATE',
        '/payroll_loan/{id}/update_loan_preview' => 'UPDATE',
        '/payroll_loan/{id}/update_amortization_preview' => 'UPDATE',
        '/payroll/{payroll_id}/recalculate' => 'UPDATE',

        '/payroll/upload/allowance/status' => 'GET',
        '/payroll/upload/bonus/status' => 'GET',
        '/payroll/upload/deduction/status' => 'GET',
        '/payroll/upload/commission/status' => 'GET',
        '/payroll/upload/commission/attendance' => 'GET',
        '/payroll/upload/calculate/attendance' => 'GET',

        // FOR EXPORT/DOWNLOAD ENDPOINTS
        '/employee/{id}/leave_requests/download' => 'EXPORT',
        '/company/{id}/employees/generate_masterfile' => 'EXPORT',
        '/company/{id}/other_incomes/{type}/download' => 'EXPORT',
        '/company/{companyId}/shifts/generate_csv' => 'EXPORT',
        '/attendance/records/export' => 'EXPORT',
        '/download/{type}/{uuid}' => 'EXPORT',
        '/payroll/{id}/payslips/generate' => 'EXPORT',
        '/payroll_register/multiple' => 'EXPORT',
        '/payroll/{payroll_id}/bank_advise' => 'EXPORT',
        '/payroll/{payroll_id}/bank_file' => 'EXPORT',
    ],

    // Get endpoints with special handling, will not process automatically in the audit trail middleware
    'get_endpoints_manual' => [
        '/employee/batch_update/status',
        '/employee/batch_add/status',
        '/company/{id}/employees/generate_masterfile/status',
        '/payroll/{payroll_id}/calculate/status',
        '/workflow_entitlement/upload/status',
        '/company/{id}/other_income/{type}/upload/status',
        '/company/{companyId}/batch_assign_shifts/status',
        '/annual_earning/upload/status',
        '/employee/batch_add/preview',
        '/download/{type}/{uuid}',
        '/payroll/{id}/payslips/generate',
        '/payroll/{payroll_id}/job/{job_name}',

        '/payroll/upload/allowance/status',
        '/payroll/upload/bonus/status',
        '/payroll/upload/deduction/status',
        '/payroll/upload/commission/status',
        '/payroll/upload/attendance/status',
        '/payroll/upload/calculate/status',
        '/payroll/{payroll_id}/bank_advise',
        '/payroll/{payroll_id}/bank_file',
        '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements'
    ],

    'other_incomes_type_entity' => [
        'App\\Model\\PhilippineBonus' => 'employees.people.bonuses',
        'App\\Model\\PhilippineCommission' => 'employees.people.commissions',
        'App\\Model\\PhilippineDeduction' => 'employees.people.deductions',
        'App\\Model\\PhilippineAdjustment' => 'employees.people.adjustments',
        'App\\Model\\PhilippineAllowance' => 'employees.people.allowances',
    ],

    'ignore_input_endpoints' => [
        '/auth/user/login',
        '/auth/reset_password',
        '/ess/reset_password'
    ],
];
