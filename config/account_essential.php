<?php

return [
    'redis_ttl' => env('REDIS_ACCOUNT_DATA_ESSENTIAL_TTL', 60),
    'redis_cache_key' => env('REDIS_ACCOUNT_DATA_CACHE_KEY', 'ACCOUNT:ED:%s'),
    'cache' => [
        'clear' => [
            'endpoints' => [
                ['method' => 'POST', 'uri' => '/philippine/company'],
                ['method' => 'DELETE', 'uri' => '/company/bulk_delete'],
                ['method' => 'POST', 'uri' => '/department/bulk_create'],
                ['method' => 'DELETE', 'uri' => '/department/{id}'],
                ['method' => 'POST', 'uri' => '/time_attendance_locations'],
                ['method' => 'DELETE', 'uri' => '/time_attendance_locations/bulk_delete'],
                ['method' => 'POST', 'uri' => '/philippine/payroll_group'],
                ['method' => 'POST', 'uri' => '/payroll_group/upload'],
                ['method' => 'DELETE', 'uri' => '/payroll_group/bulk_delete'],
                ['method' => 'POST', 'uri' => '/time_attendance_team'],
                ['method' => 'DELETE', 'uri' => '/team/bulk_delete'],
                ['method' => 'POST', 'uri' => '/position/bulk_create'],
                ['method' => 'DELETE', 'uri' => '/position/{id}'],
            ],
            'status_codes' => [200, 201, 204],
        ],
    ],
];
