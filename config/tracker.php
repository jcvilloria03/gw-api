<?php

return [
    'app_name' => env('APP_NAME', 'gw-api'),
    'header_name' => env('TRACKER_HEADER_NAME', 'X-Tracking-Id'),
    'tracking_id_length' => env('TRACKER_ID_LENGTH', 20),
];
