<?php

return [
    'company_settings' => [
        'company_payroll' => [
            'allowance_types' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income_type',
                    '/philippine/allowance_type/{id}',
                    '/philippine/company/{id}/allowance_type/bulk_create'
                ]
            ],
            'bonus_types' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income_type',
                    '/philippine/bonus_type/{id}',
                    '/other_income_type/{id}',
                    '/philippine/company/{id}/bonus_type/bulk_create'
                ]
            ],
            'commission_types' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income_type',
                    '/philippine/commission_type/{id}',
                    '/philippine/company/{id}/commission_type/bulk_create'
                ]
            ],
            'deduction_types' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/philippine/deduction_type/{id}',
                    '/company/{id}/other_income_type',
                    '/philippine/company/{id}/deduction_type/bulk_create',
                ]
            ],
            'disbursements' => [
                'endpoints' => [
                    '/company/{company_id}/banks',
                    '/company/{company_id}/bank',
                ]
            ],
            'loan_type_settings' => [
                'endpoints' => [
                    '/payroll_loan_type/bulk_delete',
                    '/company/{id}/payroll_loan_types',
                    '/payroll_loan_type',
                    '/payroll_loan_type/{id}'
                ]
            ],
            'payroll_groups' => [
                'endpoints' => [
                    '/payroll_group/upload/save',
                    '/philippine/company/{id}/payroll_groups',
                    '/payroll_group/bulk_delete',
                    '/philippine/payroll_group/{payroll_group_id}',
                    '/philippine/payroll_group',
                    '/philippine/payroll_group/{id}',
                    '/company/{id}/payrolls'
                ]
            ]
        ],
        'company_structure' => [
            'endpoints' => [
                '/account/progress',
                '/user/{id}'
            ],
            'cost_centers' => [
                'endpoints' => [
                    '/cost_center',
                    '/cost_center/{cost_center_id}',
                    '/company/{id}/cost_centers',
                    '/cost_center/bulk_delete'
                ]
            ],
            'employment_types' => [
                'endpoints' => [
                    '/employment_type/bulk_create',
                    '/employment_type/{id}',
                    '/company/{id}/employment_types',
                    '/employment_type/bulk_delete'
                ]
            ],
            'locations' => [
                'endpoints' => [
                    '/company/{id}/time_attendance_locations',
                    '/time_attendance_locations',
                    '/time_attendance_locations/bulk_delete',
                    '/time_attendance_locations/{id}',
                ]
            ],
            'organizational_chart' => [
                'endpoints' => [
                    '/position/bulk_update',
                    '/company/{id}/departments',
                    '/position/bulk_create',
                    '/company/{id}/positions',
                    '/position/{id}'
                ]
            ],
            'projects' => [
                'endpoints' => [
                    '/project/{id}',
                    '/company/{id}/projects',
                    '/project/bulk_create',
                    '/project/bulk_delete'
                ]
            ],
            'ranks' => [
                'endpoints' => [
                    '/company/{id}/ranks',
                    '/rank/bulk_create',
                    '/rank/{id}',
                    '/rank/bulk_delete',
                ]
            ],
            'teams' => [
                'endpoints' => [
                    '/team/bulk_delete',
                    '/time_attendance_team',
                    '/team/{id}',
                    '/team/{id}',
                    '/company/{id}/ta_employees/search',
                    '/company/{id}/teams'
                ]
            ]
        ],
        'day_hour_rates' => [
            'endpoints' => [
                '/day_hour_rate/{id}',
                '/company/{id}/day_hour_rates'
            ]
        ],
        'max_clock_outs' => [
            'endpoints' => [
                '/max_clock_out',
                '/max_clock_out/{id}',
                '/company/{id}/max_clock_out',
                '/max_clock_out/bulk_delete'
            ]
        ],
        'leave_settings' => [
            'leave_entitlements' => [
                'endpoints' => [
                    '/leave_entitlement/bulk_delete',
                    '/leave_entitlement',
                    '/company/{id}/leave_entitlements',
                    '/leave_entitlement/{id}'
                ]
            ],
            'leave_types' => [
                'endpoints' => [
                    '/company/{id}/leave_types',
                    '/leave_type/{id}',
                    '/leave_type',
                    '/leave_type/bulk_delete'
                ]
            ]
        ],
        'schedule_settings' => [
            'endpoints' => [
                '/default_schedule',
            ],
            'default_schedule' => [
                'endpoints' => [
                    '/default_schedule/bulk_update',
                    '/default_schedule',
                    '/default_schedule/bulk_create',
                ]
            ],
            'holidays' => [
                'endpoints' => [
                    '/holiday/bulk_delete',
                    '/holiday/{id}',
                    '/company/{id}/holidays',
                    '/holiday'
                ]
            ],
            'night_shift' => [
                'endpoints' => [
                    '/night_shift/{id}',
                    '/company/{id}/night_shift'
                ]
            ],
            'tardiness_rules' => [
                'endpoints' => [
                    '/tardiness_rule/bulk_delete',
                    '/tardiness_rule',
                    '/company/{id}/tardiness_rules',
                    '/tardiness_rule/{id}'
                ]
            ]
        ],
        'workflow_automation' => [
            'endpoints' => [
                '/workflow',
                '/workflow/bulk_delete',
                '/workflow/{id}',
                '/company/{id}/workflows',
            ]
        ]
    ],
    'control_panel' => [
        'audit_trail' => [
            'endpoints' => [
                '/audit-trail/logs',
                '/audit-trail/logs/export'
            ]
        ],
        'companies' => [
            'endpoints' => [
                '/philippine/company',
                '/account/philippine/companies',
                '/company/bulk_delete',
                '/company/{company_id}',
            ],
            'company_information' => [
                'endpoints' => [
                    '/philippine/company',
                    '/company/{company_id}',
                    '/philippine/company/{id}'
                ]
            ]
        ],
        'device_management' => [
            'endpoints' => [
                '/user/{id}',
                '/api/face_pass/ra08t/devices',
                '/api/face_pass/ra08t/devices/{id}',
                '/api/face_pass/ra08t/users',
                '/api/face_pass/ra08t/users/{user_id}',
                '/api/face_pass/ra08t/users/{user_id}/sync',
                '/api/face_pass/ra08t/users/{user_id}/devices',
                '/api/face_pass/ra08t/accounts/unlinked_device_users',
            ]
        ],
        'roles' => [
            'endpoints' => [
                '/accounts/{accountId}/roles/{roleId}',
                '/accounts/{accountId}/roles'
            ]
        ],
        'salpay_integration' => [
            'endpoints' => [
                '/salpay/companies/{companyId}/card-designs',
                '/salpay/companies/{companyId}/invite-shipping-details',
                '/salpay/companies/{companyId}/employees',
                '/salpay/companies/{companyId}/employee-invitations',
                '/salpay/companies/{companyId}/employee-reinvitations',
                '/salpay/companies/{companyId}/employee-unlink-invitation',
                '/salpay/companies/{companyId}/withdrawn-employee-invitations',
                '/salpay/users/{userId}/companies',
                '/salpay/link/company-authorizations',
                '/salpay/link/company-integrations',
                '/salpay/link/company-otp-verifications',
                '/salpay/companies/{companyId}/salpay-business/administrators',
                '/salpay/companies/{companyId}/salpay-account/administrators',
                '/salpay/companies/{companyId}/salpay-account/administrators/{adminId}',
                '/salpay/companies/salpay-settings/{userId}'
            ]
        ],
        'subscriptions' => [
            'invoices_tab' => [
                'endpoints' => [
                    '/subscriptions/billing_information',
                    '/subscriptions/invoices',
                    '/subscriptions/invoices/{id}/payments/generate_paynamics',
                    '/subscriptions/invoices/{id}/billed_users',
                    '/subscriptions/invoices/{id}'
                ]
            ],
            'licenses_tab' => [
                'endpoints' => [
                    '/subscriptions/{subscriptionId}/draft',
                    '/subscriptions/{id}/plan/update',
                ]
            ],
            'receipts_tab' => [
                'endpoints' => [
                    '/subscriptions/receipts'
                ]
            ],
            'subscriptions_tab' => [
                'endpoints' => [
                    '/account',
                    '/subscriptions/{subscriptionId}/stats'
                ]
            ]
        ],
        'users' => [
            'endpoints' => [
                '/user',
                '/user/{id}',
                '/user/{id}/set_status',
                '/user/{id}/resend_verification',
                '/account/users',
            ]
        ]
    ],
    'employees' => [
        'adjustments' => [
            'endpoints' => [
                '/other_income/{id}',
                '/philippine/adjustment/{id}',
                '/philippine/company/{id}/adjustment/bulk_create',
            ]
        ],
        'allowances' => [
            'endpoints' => [
                '/other_income/{id}',
                '/philippine/allowance/{allowanceId}',
                '/philippine/company/{id}/allowance/bulk_create'
            ]
        ],
        'annual_earnings' => [
            'endpoints' => [
                '/company/{id}/annual_earning/set',
                '/annual_earning/upload/status',
                '/annual_earning/upload/save',
                '/company/{id}/annual_earning/{annualEarningId}',
                '/annual_earning/upload',
                '/company/{companyId}/annual_earning/bulk_delete'
            ]
        ],
        'deductions' => [
            'endpoints' => [
                '/company/{id}/other_income_types/{type}',
                '/company/other_income/{type}/upload/preview',
                '/other_income/{id}',
                '/company/{id}/other_incomes/{type}',
                '/philippine/deduction/{id}',
                '/company/{id}/other_income',
                '/company/{id}/employees/search',
                '/philippine/company/{id}/deduction/bulk_create',
                '/company/{id}/other_income/{type}/upload',
                '/company/{id}/other_incomes/{type}/download'
            ]
        ],
        'government_forms' => [
            'endpoints' => [
                '/company/{company_id}/government_forms/bir_2316/employees',
                '/company/{company_id}/government_forms/bir_2316/download',
                '/employee/government_forms/bir_2316/bulk/generate',
                '/company/{company_id}/government_forms/bir_2316/regenerate',
                '/employee/{employee_id}/government_forms/bir_2316/{id}',
                '/company/{company_id}/government_forms/bir_2316'
            ]
        ],
        'leaves' => [
            'filed_leave' => [
                'endpoints' => [
                    '/employee/{employeeId}/rest_days',
                    '/leave_request/{id}/admin',
                    '/leave_request/upload/save',
                    '/company/{id}/employee/{employeeId}/leave_types',
                    '/leave_request/admin',
                    '/leave_request/upload',
                    '/leave_request/{id}',
                    '/company/{id}/leave_requests/download',
                    '/leave_request/bulk_delete',
                    '/leave_request/upload/preview',
                    '/company/{id}/leave_requests',
                ]
            ],
            'leave_credits' => [
                'endpoints' => [
                    '/company/{id}/leave_credits/download',
                    '/leave_credit/{id}',
                    '/leave_credit/upload/preview',
                    '/company/{company_id}/leave_credits',
                    '/employee/{id}/leave_entitlement',
                    '/leave_credit',
                    '/company/{id}/leave_types',
                    '/leave_credit/upload/save',
                    '/leave_credit/upload',
                    '/leave_credit/bulk_delete',
                    '/leave_credit/{id}',
                ]
            ]
        ],
        'loans' => [
            'endpoints' => [
                '/payroll_loan/upload/preview',
                '/company/{id}/payroll_loans',
                '/company/{id}/departments',
                '/payroll_loan',
                '/payroll_loan/{id}/update_loan_preview',
                '/payroll_loan/upload/save',
                '/payroll_loan/upload',
                '/payroll_loan/bulk_delete',
                '/company/{companyId}/payroll_loans/generate_csv',
                '/payroll_loan/{id}/update_amortization_preview',
                '/payroll_loan/{id}',
                '/payroll_loan/{id}/initial_preview',
                '/payroll_loan/create/{uid}',
            ]
        ],
        'people' => [
            'endpoints' => [
                '/employee/batch_update/status',
                '/employee/batch_update/personal_info',
                '/employee/batch_update/people',
                '/employee/batch_add/personal_info',
                '/employee/batch_add/people',
                '/employee/batch_update/time_attendance_info',
                '/employee/batch_add/payroll_info',
                '/download/{type}/{uuid}',
                '/employee/batch_add/preview',
                '/company/{id}/employees',
                '/employee/batch_add/ta_save',
                '/company/{id}/employment_types',
                '/employee/batch_update/payroll_info',
                '/employee/has_usermatch',
                '/people',
                '/philippine/employee/{id}',
                '/employee/batch_add/time_attendance_info',
                '/company/{id}/teams',
                '/employee/batch_add/save',
                '/employee',
                '/philippine/employee',
                '/company/{id}/employees/generate_masterfile'
            ],
            'adjustments' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income',
                    '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                    '/company/{id}/other_incomes/{type}/download'
                ]
            ],
            'allowances' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income',
                    '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                    '/company/{id}/other_incomes/{type}/download'
                ]
            ],
            'annual_earnings' => [
                'endpoints' => [
                    '/company/{companyId}/employee/{employeeId}/annual_earning'
                ]
            ],
            'approval_groups' => [
                'endpoints' => [
                    '/employee/{id}/workflow_entitlement/create_or_update',
                    '/employee/{id}/workflow_entitlements'
                ]
            ],
            'basic_information' => [
                'endpoints' => [
                    '/philippine/employee/{id}',
                    '/employee/{id}',
                    '/employee/upload/picture/{id}'
                ]
            ],
            'basic_pay_adjustments' => [
                'endpoints' => [
                    '/employee/{employeeId}/basic_pay_adjustment/{adjustmentId}/update',
                    '/basic_pay_adjustment/{id}',
                    '/employee/{id}',
                    '/employee/{id}/basic_pay_adjustment',
                    '/employee/{employeeId}/basic_pay_adjustment/delete'
                ]
            ],
            'bonuses' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/company/{id}/other_income',
                    '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                    '/company/{id}/other_incomes/{type}/download'
                ]
            ],
            'commissions' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/other_income/{id}',
                    '/philippine/company/{id}/commission/bulk_create',
                    '/company/{id}/other_income',
                    '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                    '/philippine/commission/{commissionId}',
                    '/company/{id}/other_incomes/{type}/download',
                ]
            ],
            'deductions' => [
                'endpoints' => [
                    '/company/{id}/other_income_types/{type}',
                    '/other_income/{id}',
                    '/company/{id}/other_income',
                    '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                    '/philippine/company/{id}/deduction/bulk_create',
                    '/company/{id}/other_incomes/{type}/download'
                ]
            ],
            'filed_leaves' => [
                'endpoints' => [
                    '/leave_request/{id}/admin',
                    '/leave_request/admin',
                    '/leave_request/{id}',
                    '/employee/{id}/leave_requests',
                    '/employee/{id}/leave_requests/download',
                ]
            ],
            'leave_credits' => [
                'endpoints' => [
                    '/leave_credit/{id}',
                    '/company/{company_id}/leave_credits',
                    '/employee/{id}/leave_entitlement',
                    '/leave_credit',
                    '/employee/{id}/leave_credits/download'
                ]
            ],
            'loans' => [
                'endpoints' => [
                    '/company/{companyId}/employee/{employeeId}/payroll_loans',
                    '/payroll_loan',
                    '/payroll_loan/{id}/update_loan_preview',
                    '/payroll_loan/bulk_delete',
                    '/company/{companyId}/payroll_loans/generate_csv',
                    '/payroll_loan/{id}/update_amortization_preview',
                    '/payroll_loan/{id}',
                    '/payroll_loan/{id}/initial_preview',
                    '/payroll_loan/create/{uid}',
                    '/employee/{id}'
                ]
            ],
            'payment_methods' => [
                'endpoints' => [
                    '/employee/{employee_id}/payment_methods',
                    '/employee/{employee_id}/bank',
                    '/company/{company_id}/banks',
                    '/employee/{employee_id}/payment_method',
                    '/employee/{employee_id}/payment_methods/{payment_method_id}',
                    '/employee/{id}'
                ]
            ],
            'termination_information' => [
                'endpoints' => [
                    '/employee/{employeeId}/final_pay/available_items',
                    '/payroll/{payroll_id}/calculate',
                    '/employee/{id}/termination_informations',
                    '/final_pay',
                    '/termination_informations/{id}',
                    '/final_pay/{id}',
                    '/termination_informations',
                    '/termination_informations/{id}'
                ]
            ]
        ],
        'workflows' => [
            'endpoints' => [
                '/workflow_entitlement/upload',
                '/workflow_entitlement/{id}',
                '/workflow_entitlement/bulk_create_or_update',
                '/workflow_entitlement/upload/preview',
                '/workflow_entitlement/upload/save',
                '/company/{id}/workflow_entitlements',
                '/workflow_entitlement/has_pending_requests',
                '/workflow_entitlement/bulk_delete',
                '/company/{id}/workflows',
                '/workflow_entitlement/upload/status',
            ]
        ]
    ],
    'ess' => [
        'announcements' => [
            'endpoints' => [
                '/ess/announcement/reply/{id}',
                '/ess/announcement/{id}/reply',
                '/ess/announcement',
                '/ess/announcement/{id}',
                '/ess/employee/announcements',
                '/ess/affected_employees/search',
                '/ess/employee/announcements/unread'
            ]
        ],
        'approvals' => [
            'endpoints' => [
                '/ess/request/bulk_approve',
                '/ess/request/bulk_cancel',
                '/ess/shift_change_request/{id}',
                '/ess/request/send_message',
                '/ess/employee/approvals',
                '/ess/request/bulk_decline',
                '/ess/undertime_request/{id}',
                '/ess/employee_request/{id}/download_attachments',
                '/ess/time_dispute_request/{id}',
                '/ess/company/{id}/default_schedule',
                '/ess/leave_request/{id}',
                '/ess/affected_employees/search',
                '/ess/overtime_request/{id}'
            ]
        ],
        'dashboard' => [
            'endpoints' => [
                '/ess/clock_state/timesheet',
                '/ess/clock_state/timesheet/last_entry',
                '/ess/employee/current-schedules',
                '/ess/employee/approvals',
                '/ess/employee/calendar_data',
                '/ess/attendance/{employee_id}/{date}',
                '/ess/shifts',
                '/ess/company/{id}/default_schedule',
                '/ess/employee/requests',
                '/ess/clock_state',
                '/ess/employee/rest_days',
                '/ess/clock_state/log'
            ]
        ],
        'payslips' => [
            'endpoints' => [
                '/ess/payslip/{id}',
                '/ess/payslips'
            ]
        ],
        'profile_information' => [
            'endpoints' => [
                '/ess/leave_credits',
                '/user/details/{userId}',
                '/ess/employee/user',
                '/ess/change_password'
            ]
        ],
        'requests' => [
            'endpoints' => [
                '/ess/clock_state/timesheet',
                '/ess/request/{id}/cancel',
                '/ess/company/{id}/default_schedule',
                '/ess/employee/requests',
                '/ess/employee/rest_days'
            ],
            'leaves' => [
                'endpoints' => [
                    '/ess/request/bulk_approve',
                    '/ess/request/bulk_cancel',
                    '/ess/request/send_message',
                    '/ess/attachments',
                    '/ess/request/bulk_decline',
                    '/ess/employee_request/{id}/download_attachments',
                    '/ess/request/{id}/cancel',
                    '/ess/employee/leave_types',
                    '/ess/company/{id}/default_schedule',
                    '/ess/employee/requests',
                    '/ess/leave_credits',
                    '/ess/leave_request',
                    '/ess/leave_request/{id}',
                    '/ess/employee/rest_days',
                    '/user/details/{userId}',
                    '/ess/employee/user'
                ]
            ],
            'overtime' => [
                'endpoints' => [
                    '/ess/request/send_message',
                    '/ess/attendance/{employee_id}/{date}',
                    '/ess/request/{id}/cancel',
                    '/ess/company/{id}/default_schedule',
                    '/ess/employee/requests',
                    '/ess/overtime_request',
                    '/ess/overtime_request/{id}'
                ]
            ],
            'shift_change' => [
                'endpoints' => [
                    '/ess/shift_change_request/{id}',
                    '/ess/request/send_message',
                    '/ess/shift/overlapping_requests',
                    '/ess/request/{id}/cancel',
                    '/ess/company/{id}/default_schedule',
                    '/ess/employee/requests',
                    '/ess/shift_change_request',
                    '/ess/employee/schedules'
                ]
            ],
            'time_dispute' => [
                'endpoints' => [
                    '/ess/clock_state/timesheet',
                    '/ess/hours_worked',
                    '/ess/request/send_message',
                    '/ess/request/{id}/cancel',
                    '/ess/time_dispute_request/{id}',
                    '/ess/company/{id}/default_schedule',
                    '/ess/employee/requests',
                    '/ess/time_dispute_request',
                    '/ess/time_types',
                ]
            ],
            'undertime' => [
                'endpoints' => [
                    '/ess/undertime_request',
                    '/ess/clock_state/timesheet',
                    '/ess/request/send_message',
                    '/ess/undertime_request/{id}',
                    '/ess/attendance/{employee_id}/{date}',
                    '/ess/request/{id}/cancel',
                    '/ess/company/{id}/default_schedule',
                    '/ess/employee/requests',
                ]
            ]
        ],
        'teams' => [
            'endpoints' => [
                '/ess/teams/{teamId:d+}/members/{memberId:d+}/calendar',
                '/ess/teams/{teamId:d+}/attendance/regenerate/{jobId}/errors/download',
                '/ess/teams/{id:d+}/members',
                '/ess/teams/{teamId:d+}/members/{memberId:d+}',
                '/ess/teams/{id:d+}/calendar',
                '/ess/teams/{teamId:d+}/attendance/regenerate/{jobId}',
                '/ess/teams/{id:d+}',
                '/ess/teams/{teamId:d+}/attendance/regenerate',
                '/ess/teams'
            ]
        ]
    ],
    'main_page' => [
        'announcements' => [
            'endpoints' => [
                '/announcement/{announcementId}/recipient_seen',
                '/company/{id}/affected_employees/search',
                '/announcement/{announcementId}',
                '/announcement/{announcementId}/reply',
                '/announcement',
                '/company/{id}/inactive_employees',
                '/company/{companyId}/announcements',
                '/company/{companyId}/announcements/download'
            ]
        ],
        'dashboard' => [
            'endpoints' => [
                '/attendance/calculate/bulk',
                '/company/{id}/affected_employees/search',
                '/company/{companyId}/ta_employees',
                '/default_schedule',
                '/company/{id}/ta_employees/id',
                '/announcement',
                '/company/{company_id}/dashboard/attendance',
                '/company/{company_id}/active_employees_count',
                '/company/{companyId}/announcements',
                '/admin_dashboard/calendar_data',
                '/company/{id}/employee/events',
            ]
        ]
    ],
    'payroll' => [
        'bonuses' => [
            'endpoints' => [
                '/company/{id}/other_income_types/{type}',
                '/other_income/{id}',
                '/philippine/bonus/{id}',
                '/company/{id}/other_incomes/{type}',
                '/philippine/company/{id}/bonus/bulk_create',
                '/company/{id}/other_income',
                '/company/{id}/other_income/{type}/upload',
                '/company/{id}/other_incomes/{type}/download'
            ]
        ],
        'commissions' => [
            'endpoints' => [
                '/company/{id}/other_income_types/{type}',
                '/other_income/{id}',
                '/philippine/company/{id}/commission/bulk_create',
                '/company/{id}/other_income',
                '/company/{companyId}/employee/{employeeId}/other_incomes/{type}',
                '/philippine/commission/{commissionId}',
                '/company/{id}/other_income/{type}/upload',
                '/company/{id}/other_incomes/{type}/download'
            ]
        ],
        'government_forms' => [
            'endpoints' => [
                '/government_forms/{id}',
                '/download/{type}/{uuid}',
                '/company/{id}/months_with_payroll_loans/{type}',
                '/government_forms/{id}',
                '/government_forms'
            ]
        ],
        'payroll_summary' => [
            'endpoints' => [
                '/payroll/upload/deduction',
                '/payroll/{id}/send_payslips',
                '/payroll/upload/bonus',
                '/company/{id}/payroll/{type}/available_items',
                '/payroll/upload/deduction',
                '/payroll/{id}/open',
                '/payroll/final',
                '/company/{id}/employees/final_pay',
                '/payroll/{payroll_id}/calculate',
                '/payroll_loan',
                '/download/{type}/{uuid}',
                '/payroll_loan/{id}/update_loan_preview',
                '/payroll',
                '/payroll/upload/commission',
                '/payrolls',
                '/payrolls/delete_payroll_job',
                '/payroll/{id}/payslips/generate',
                '/company/{id}/activate_gap_loans',
                '/payroll_loan/{id}/update_amortization_preview',
                '/payroll/{payroll_id}/recalculate',
                '/payroll/{payroll_id}/bank_file',
                '/payroll/{payroll_id}/bank_advise',
                '/payroll/{id}/get_gap_loan_candidates',
                '/payroll_loan/{id}',
                '/payroll/upload/commission',
                '/payroll/{id}',
                '/payroll/upload/allowance',
                '/payroll/{id}/disbursement_summary',
                '/payroll_loan/{id}/initial_preview',
                '/payroll/{payroll_id}/as-api/attendance',
                '/payroll/{id}/close',
                '/payroll_register/multiple',
                '/payroll/{payroll_id}/job/{job_name}',
                '/payroll/upload/attendance',
                '/payroll/upload/bonus',
                '/payroll/{id}',
                '/philippine/payroll_group/{id}',
                '/payroll_loan/create/{uid}',
                '/payroll/special',
                '/payroll/upload/allowance',
                '/company/{id}/payrolls',
                '/payroll/upload/attendance',
                '/payroll/{id}',
                '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements/{disbursementId}',
                '/salpay/companies/{companyId}/payrolls/{payrollId}/disbursements',
                '/salpay/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/resend',
                '/salpay/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/verify',
                '/salpay/companies/{companyId}/payrolls/{payrollId}/employees',
                '/salpay/companies/{companyId}/remaining-balance',
                '/payroll/upload/allowance/status',
                '/payroll/upload/bonus/status',
                '/payroll/upload/deduction/status',
                '/payroll/upload/commission/status'
            ],
            'generate_regular_payroll' => [
                'endpoints' => [
                    '/payroll/upload/deduction',
                    '/payroll/upload/bonus',
                    '/payroll/upload/deduction',
                    '/payroll/{payroll_id}/calculate',
                    '/payroll',
                    '/payroll/upload/commission',
                    '/payroll/upload/allowance',
                    '/payroll/{payroll_id}/as-api/attendance',
                    '/payroll/upload/attendance',
                ]
            ]
        ],
        'payslips' => [
            'endpoints' => [
                '/company/{companyId}/payslips',
                '/payroll/{payrollId}/send_single_payslip',
                '/payslip/{id}',
                '/payslip/download_multiple',
                '/payslip/zipped/{id}/url'
            ]
        ],
    ],
    'root' => [
        'admin' => [
            'endpoints' => [
                '/account',
                '/announcement/reply/{id}/seen',
                '/account/philippine/companies',
                '/announcement/{announcementId}/recipient_seen',
                '/announcement_notifications',
                '/user/{id}',
                '/philippine/company/{id}',
                '/notifications/{id}/clicked',
                '/notifications',
                '/salpay/companies/{companyId}/integration-status'
            ]
        ],
        'ess' => [
            'endpoints' => [
                '/ess/employee/announcements/unread',
                '/ess/payslips/unread',
                '/ess/user/{id}',
                '/ess/notifications',
                '/ess/notifications_status',
                '/ess/notifications/{id}/clicked',
                '/user/details/{userId}',
                '/ess/employee/user'
            ]
        ]
    ],
    'time_and_attendance' => [
        'approvals_list' => [
            'endpoints' => [
                '/user/approvals',
                '/request/bulk_decline',
                '/request/bulk_approve'
            ],
            'request_details' => [
                'endpoints' => [
                    '/shift_change_request/{id}',
                    '/undertime_request/{id}',
                    '/time_dispute_request/{id}',
                    '/leave_request/{id}',
                    '/overtime_request/{id}',
                    '/request/send_message',
                    '/request/bulk_decline',
                    '/request/bulk_approve'
                ]
            ]
        ],
        'attendance_computation' => [
            'endpoints' => [
                '/attendance/records/upload',
                '/attendance/records',
                '/company/{companyId}/hours_worked/leaves',
                '/attendance/calculate/bulk',
                '/attendance/unlock/bulk',
                '/view/{companyId}/attendance/user',
                '/attendance/unlock/bulk',
                '/attendance/records/export',
                '/attendance/lock/bulk',
                '/attendance/{employee_id}/{date}/edit',
                '/company/{id}/hours_worked/leaves/bulk_create_or_update_or_delete',
                '/company/{id}/time_records/bulk_create_or_delete',
            ],
            'attendance' => [
                'endpoints' => [
                    '/company/{id}/time_records/bulk_create_or_delete'
                ]
            ]
        ],
        'schedules' => [
            'endpoints' => [
                '/schedule/upload',
                '/schedule/upload/save',
                '/company/{companyId}/schedules/download/{fileName}',
                '/company/{id}/affected_employees/search',
                '/schedule/bulk_delete',
                '/schedule/{id}',
                '/schedule/upload/preview',
                '/company/{id}/schedules',
                '/company/{companyId}/schedules/generate_csv',
                '/schedule/{id}',
                '/schedule',
            ]
        ],
        'shifts' => [
            'endpoints' => [
                '/company/{companyId}/shifts_data',
                '/company/{companyId}/shifts/download/{fileName}',
                '/company/{companyId}/shifts/generate_csv',
                '/shift/unassign/{id}'
            ],
            'assign_rest_day' => [
                'endpoints' => [
                    '/rest_day',
                    '/rest_day/unassign/{id}',
                    '/rest_day/{id}'
                ]
            ],
            'assign_shift' => [
                'endpoints' => [
                    '/shift/bulk_create',
                    '/shift/{id}',
                    '/rest_day/{id}',
                    '/company/{companyId}/batch_assign_shifts/save',
                    '/shift/{id}',
                    '/shift',
                    '/company/{companyId}/batch_assign_shifts/validate',
                ]
            ]
        ]
    ]
];
