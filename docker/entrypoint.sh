#!/bin/sh

echo $@

if [ "$1" = "supervisord" ]
then
    sed -i "s/#consumer.name#/$(echo $CONSUMER_NAME | sed -e 's/\\/\\\\/g; s/\//\\\//g; s/&/\\\&/g')/g" /app/supervisord.conf
    exec "$@";
else
    exec "$@";
fi
