<?php

namespace TestsNew\Unit\Http\Middleware;

use App\Account\AccountRequestService;
use App\Authz\AuthzDataScope;
use App\Authz\AuthzRequestService;
use App\Http\Middleware\AuthzMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use Mockery;
use Symfony\Component\HttpKernel\Exception\HttpException;
use TestsNew\Unit\TestCase;

class AuthzMiddlewareTest extends TestCase
{
    public function testHandle()
    {
        $this->markTestSkipped('Depricated since https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8052.');
        $origRequest = Request::create('/');
        $origRequest->attributes->set('user', [
            'user_id' => 1,
            'account_id' => 1,
        ]);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_COMPANY_ID_HEADER, 1);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_ENTITIES_HEADER, 'testmodule.testsubmodule');
        $origRequest->setRouteResolver(function () {
            return [[], ['uri' => '/']];
        });
        Config::shouldReceive('get')->with('modules_map')->once()->andReturn([
            [
                'method' => 'GET',
                'url' => '/',
                'modules' => [
                    'testmodule.testsubmodule' => [
                        'actions' => ['READ']
                    ]
                ]
            ],
        ]);
        Config::shouldReceive('get')->with('database.redis')->andReturn([]);
        Config::shouldReceive('get')
            ->with('account_essential.redis_ttl', Mockery::type('integer'))
            ->once()
            ->andReturn(60);
        Config::shouldReceive('get')->with('account_essential.redis_cache_key')->once()->andReturn('ACCOUNT:ED:%s');

        Redis::shouldReceive('get')
            ->with(env('APP_ENV') . ':authz:userData:1')
            ->once()
            ->andReturn(json_encode([
                [
                    'user_id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'role_id' => null,
                    'employee_id' => null,
                    'department_id' => null,
                    'cost_center_id' => null,
                    'position_id' => null,
                    'location_id' => null,
                    'payroll_group_id' => null,
                    'team_id' => null,
                ]
            ]));
        Redis::shouldReceive('get')
            ->with('ACCOUNT:ED:1')
            ->once()
            ->andReturn(json_encode([
                1 => [
                    'department' => [1, 2],
                    'position' => [1],
                    'team' => [1, 2],
                    'location' => [1],
                    'payroll_group' => [1],
                ],
            ]));

        $accountRequestService = Mockery::mock(AccountRequestService::class);
        $authzRequestService = Mockery::mock(AuthzRequestService::class);
        $authzRequestService->shouldReceive('checkSalariumClearance')->once()->andReturn([
            'testmodule.testsubmodule' => [
                'data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2], 'DEPARTMENT' => ['__ALL__']]
            ]
        ]);

        $middleware = new AuthzMiddleware($authzRequestService, $accountRequestService);
        $middleware->handle($origRequest, function (Request $request) use ($origRequest) {
            $this->assertSame($origRequest, $request);
            $this->assertTrue($request->attributes->get('authz_enabled'));
            $this->assertInstanceOf(AuthzDataScope::class, $request->attributes->get('authz_data_scope'));
            $this->assertEquals(
                ['COMPANY' => [1], 'TEAM' => [2], 'DEPARTMENT' => [1, 2]],
                $request->attributes->get('authz_data_scope')->getDataScope()
            );
        });
    }

    public function testMissingXAuthzEntitiesHeader()
    {
        $this->markTestSkipped('Depricated since https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8052.');
        $origRequest = Request::create('/');
        $origRequest->attributes->set('user', [
            'user_id' => 1,
            'account_id' => 1,
        ]);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_COMPANY_ID_HEADER, 1);
        $origRequest->setRouteResolver(function () {
            return [[], ['uri' => '/']];
        });


        $accountRequestService = Mockery::mock(AccountRequestService::class);
        $authzRequestService = Mockery::mock(AuthzRequestService::class);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Missing required authentication header.');
        $middleware = new AuthzMiddleware($authzRequestService, $accountRequestService);
        $middleware->handle($origRequest, function () {});
    }

    public function testEmptyXAuthzEntitiesHeader()
    {
        $this->markTestSkipped('Depricated since https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8052.');
        $origRequest = Request::create('/');
        $origRequest->attributes->set('user', [
            'user_id' => 1,
            'account_id' => 1,
        ]);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_COMPANY_ID_HEADER, 1);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_ENTITIES_HEADER, '');

        $origRequest->setRouteResolver(function () {
            return [[], ['uri' => '/']];
        });

        $accountRequestService = Mockery::mock(AccountRequestService::class);
        $authzRequestService = Mockery::mock(AuthzRequestService::class);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Missing required authentication header.');
        $middleware = new AuthzMiddleware($authzRequestService, $accountRequestService);
        $middleware->handle($origRequest, function () {});
    }

    public function testDuplicateXAuthzEntitiesHeader()
    {
        $this->markTestSkipped('Depricated since https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8052.');
        $origRequest = Request::create('/');
        $origRequest->attributes->set('user', [
            'user_id' => 1,
            'account_id' => 1,
        ]);
        $origRequest->headers->set(AuthzMiddleware::AUTHZ_COMPANY_ID_HEADER, 1);
        $origRequest->headers->set(
            AuthzMiddleware::AUTHZ_ENTITIES_HEADER,
            'control_panel.subscriptions, control_panel.subscriptions.invoices_tab.invoice_history, employees.people'
        );
        $origRequest->setRouteResolver(function () {
            return [[], ['uri' => '/']];
        });

        $accountRequestService = Mockery::mock(AccountRequestService::class);
        $authzRequestService = Mockery::mock(AuthzRequestService::class);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Module names must be from the same accordion.');
        $middleware = new AuthzMiddleware($authzRequestService, $accountRequestService);
        $middleware->handle($origRequest, function () {});
    }
}
