<?php

namespace TestsNew\Unit\Http\Middleware;

use App\Authz\AuthzService;
use App\Http\Middleware\EssentialMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mockery;
use TestsNew\Unit\TestCase;

class EssentialMiddlewareTest extends TestCase
{
    public function testHandle()
    {
        $request = Request::create('/philippine/company', 'POST');
        $request->setRouteResolver(function () {
            return [[], ['uri' => '/philippine/company']];
        });
        $request->attributes->set('user', [
            'user_id' => 1,
            'account_id' => 1,
        ]);
        $authzService = Mockery::mock(AuthzService::class);
        $authzService->shouldReceive('isUriIncludesToClearEssential')
            ->with('POST', '/philippine/company', 201)
            ->once()
            ->andReturn(true)
        ;
        $authzService->shouldReceive('clearAccountCacheEssentialData')->once()->with(1);

        $essentialMiddleware = new EssentialMiddleware($authzService);
        $essentialMiddleware->handle($request, function ($passedRequest) use ($request) {
            return new Response('', 201);
        });
    }
}