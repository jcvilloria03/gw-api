<?php

namespace TestsNew\Unit\DeviceManagement;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use App\DeviceManagement\DeviceManagementAuditService;
use Mockery;
use TestsNew\Unit\TestCase;

class DeviceManagementAuditServiceTest extends TestCase
{
    public function testLogSync()
    {
        $user = json_encode([
            'id' => 132,
            'account_id' => 199
        ]);
        $newData = json_encode([
            'user_id' => 4352,
            'device_ids' => [1324, 324, 4],
        ]);
        $item = [
            'action' => DeviceManagementAuditService::ACTION_SYNC,
            'user' => $user,
            'new' => $newData
        ];
        $mockAuditService = Mockery::mock(AuditService::class);
        $mockAuditService->shouldReceive('log')
            ->once();

        $deviceManagementAuditService = new DeviceManagementAuditService($mockAuditService);
        $deviceManagementAuditService->log($item);
    }
}
