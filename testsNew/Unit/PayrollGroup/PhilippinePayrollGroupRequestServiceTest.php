<?php

namespace TestsNew\Unit\PayrollGroup;

use App\Authz\AuthzDataScope;
use App\PayrollGroup\PhilippinePayrollGroupRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Http\JsonResponse;
use Mockery;
use TestsNew\Unit\TestCase;

class PhilippinePayrollGroupRequestServiceTest extends TestCase
{
    public function testGetShouldBeSuccess()
    {
        $dataScope = new AuthzDataScope([], []);
        $expectedResponse = json_encode(['id' => 1, 'company_id' => 1, 'account_id' => 1]);
        $client = Mockery::mock(Client::class);
        $client
            ->shouldReceive('send')
            ->once()
            ->with(Mockery::type(Request::class), Mockery::any())
            ->andReturn(new GuzzleResponse(JsonResponse::HTTP_OK, [], $expectedResponse));

        $requestService = new PhilippinePayrollGroupRequestService($client);
        $response = $requestService->get(1, $dataScope);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $response->getData());
    }

    public function testCreateShouldBeSuccess()
    {
        $expectedResponse = json_encode(['id' => 1, 'company_id' => 1, 'account_id' => 1]);
        $client = Mockery::mock(Client::class);
        $client
            ->shouldReceive('send')
            ->once()
            ->andReturn(new GuzzleResponse(200, [], $expectedResponse));
        $requestService = new PhilippinePayrollGroupRequestService($client);
        $response = $requestService->create(['company_id' => 1, 'account_id' => 1], new AuthzDataScope([], []));

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $response->getData());
    }
}
