<?php

namespace TestsNew\Unit;

use TestsNew\Helpers\TestCase as BaseTestCase;
use TestsNew\Helpers\Traits\MockConnectionTrait;

class TestCase extends BaseTestCase
{
    use MockConnectionTrait;

    protected function setUpTraits()
    {
        parent::setUpTraits();
        $uses = array_flip(class_uses_recursive(get_class($this)));
        if (isset($uses[MockConnectionTrait::class])) {
            $this->setUpMockConnectionTrait();
        }
    }
}
