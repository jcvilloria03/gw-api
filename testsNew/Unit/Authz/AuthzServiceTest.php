<?php

namespace TestsNew\Unit\Authz;

use App\Authz\AuthzService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Unit\TestCase;

class AuthzServiceTest extends TestCase
{
    private $endpoints = [
        ['method' => 'POST', 'uri' => '/philippine/company'],
        ['method' => 'DELETE', 'uri' => '/company/bulk_delete'],
        ['method' => 'POST', 'uri' => '/department/bulk_create'],
        ['method' => 'DELETE', 'uri' => '/department/{id}'],
        ['method' => 'POST', 'uri' => '/time_attendance_locations'],
        ['method' => 'DELETE', 'uri' => '/time_attendance_locations/bulk_delete'],
        ['method' => 'POST', 'uri' => '/philippine/payroll_group'],
        ['method' => 'POST', 'uri' => '/payroll_group/upload'],
        ['method' => 'DELETE', 'uri' => '/payroll_group/bulk_delete'],
        ['method' => 'POST', 'uri' => '/time_attendance_team'],
        ['method' => 'DELETE', 'uri' => '/team/bulk_delete'],
        ['method' => 'POST', 'uri' => '/position/bulk_create'],
        ['method' => 'DELETE', 'uri' => '/position/{id}'],
    ];

    public function testClearAccountCacheEssentialData()
    {
        Config::shouldReceive('get')->with('account_essential.redis_cache_key')->once()->andReturn('ACCOUNT:ED:%s');
        Config::shouldReceive('get')
            ->with('account_essential.cache.clear.endpoints', [])
            ->once()
            ->andReturn($this->endpoints)
        ;
        Config::shouldReceive('get')
            ->with('account_essential.cache.clear.status_codes', [])
            ->once()
            ->andReturn([200, 201, 204])
        ;
        Redis::shouldReceive('del')->with('ACCOUNT:ED:1')->once();

        $authzService = new AuthzService();
        $authzService->clearAccountCacheEssentialData(1);
    }

    /**
     * @dataProvider dataIsUriIncludesToClearEssential
     */
    public function testIsUriIncludesToClearEssential(string $method, string $uri, int $statusCode, bool $expected)
    {
        Config::shouldReceive('get')->with('account_essential.redis_cache_key')->once()->andReturn('ACCOUNT:ED:%s');
        Config::shouldReceive('get')
            ->with('account_essential.cache.clear.endpoints', [])
            ->once()
            ->andReturn($this->endpoints)
        ;
        Config::shouldReceive('get')
            ->with('account_essential.cache.clear.status_codes', [])
            ->once()
            ->andReturn([200, 201, 204])
        ;

        $authzService = new AuthzService();
        $this->assertSame($expected, $authzService->isUriIncludesToClearEssential($method, $uri, $statusCode));
    }

    public function dataIsUriIncludesToClearEssential()
    {
        yield ['method' => 'POST', 'uri' => '/philippine/company', 'status_code' => 201, 'expected' => true];
        yield ['method' => 'DELETE', 'uri' => '/company/bulk_delete', 'status_code' => 204, 'expected' => true];
        yield ['method' => 'POST', 'uri' => '/department/bulk_create', 'status_code' => 201, 'expected' => true];
        yield ['method' => 'DELETE', 'uri' => '/department/{id}', 'status_code' => 204, 'expected' => true];
        yield ['method' => 'GET', 'uri' => '/position/{id}', 'status_code' => 200, 'expected' => false];
        yield ['method' => 'POST', 'uri' => '/user', 'status_code' => 201, 'expected' => false];
        yield ['method' => 'DELETE', 'uri' => '/payroll/{id}', 'status_code' => 204, 'expected' => false];
        yield ['method' => 'POST', 'uri' => '/philippine/company', 'status_code' => 401, 'expected' => false];
    }
}