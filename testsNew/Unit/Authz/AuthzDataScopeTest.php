<?php

namespace TestsNew\Unit\Authz;

use App\Authz\AuthzDataScope;
use TestsNew\Unit\TestCase;

class AuthzDataScopeTest extends TestCase
{
    public function testGetDataScope()
    {
        $authz = AuthzDataScope::makeFromClearance(
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]]
        );

        $this->assertInstanceOf(AuthzDataScope::class, $authz);
        $this->assertSame([
            'COMPANY' => [1, 2, 3],
            'TEAM' => [2],
        ], $authz->getDataScope());
    }

    public function testGetClearance()
    {
        $authz = AuthzDataScope::makeFromClearance(
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]]
        );

        $this->assertInstanceOf(AuthzDataScope::class, $authz);
        $this->assertSame([
            'employees.people' => [
                'COMPANY' => [1, 2, 3],
                'TEAM' => [2],
            ]
        ], $authz->getClearance());
    }

    /**
     * @dataProvider dataIsAuthorized
     */
    public function testIsAuthorized(
        array $clearance,
        array $accountEssential,
        $module,
        string $scope,
        $values,
        bool $expected
    ) {
        $authz = AuthzDataScope::makeFromClearance($clearance, $accountEssential);

        $this->assertSame($expected, $authz->isAuthorized($scope, $values, $module));
    }

    public function dataIsAuthorized()
    {
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            AuthzDataScope::SCOPE_COMPANY,
            1,
            true
        ];
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            AuthzDataScope::SCOPE_COMPANY,
            [1, 2],
            true
        ];
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            AuthzDataScope::SCOPE_COMPANY,
            [],
            false
        ];
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            AuthzDataScope::SCOPE_COMPANY,
            [1, 4],
            false
        ];
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            'unexistingmodule',
            AuthzDataScope::SCOPE_COMPANY,
            [1, 4],
            false
        ];
        yield [
            [
                'employees.people' => ['data_scope' => ['COMPANY' => [1, 3], 'TEAM' => [2]]],
                'employees.termination' => ['data_scope' => ['COMPANY' => [2]]]
            ],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            'employees.people',
            AuthzDataScope::SCOPE_COMPANY,
            2,
            false
        ];
        yield [
            [
                'employees.people' => ['data_scope' => ['COMPANY' => [1, 3], 'TEAM' => [2]]],
                'employees.termination' => ['data_scope' => ['COMPANY' => [2]]]
            ],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            'employees.termination',
            AuthzDataScope::SCOPE_COMPANY,
            2,
            true
        ];
    }

    /**
     * @dataProvider dataIsAllAuthorized
     */
    public function testIsAllAuthorized(
        array $clearance,
        array $accountEssential,
        $module,
        array $scopes,
        bool $expected
    ) {
        $authz = AuthzDataScope::makeFromClearance($clearance, $accountEssential);

        $this->assertSame($expected, $authz->isAllAuthorized($scopes, $module));
    }

    public function dataIsAllAuthorized()
    {
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            [AuthzDataScope::SCOPE_COMPANY => 1],
            true
        ];
        yield [
            ['employees.people' => ['data_scope' => ['COMPANY' => ['__ALL__'], 'TEAM' => [2]]]],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            null,
            [AuthzDataScope::SCOPE_COMPANY => 4],
            false
        ];
        yield [
            [
                'employees.people' => ['data_scope' => ['COMPANY' => [1, 3], 'TEAM' => [2]]],
                'employees.termination' => ['data_scope' => ['COMPANY' => [2]]]
            ],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            'employees.people',
            [AuthzDataScope::SCOPE_COMPANY => [1, 3]],
            true
        ];
        yield [
            [
                'employees.people' => ['data_scope' => ['COMPANY' => [1, 3], 'TEAM' => [2]]],
                'employees.termination' => ['data_scope' => ['COMPANY' => [2]]]
            ],
            ['COMPANY' => [1, 2, 3], 'TEAM' => [1, 2, 3]],
            'employees.people',
            [AuthzDataScope::SCOPE_COMPANY => [1, 2]],
            false
        ];
    }
}
