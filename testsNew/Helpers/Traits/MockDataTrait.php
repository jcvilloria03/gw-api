<?php

namespace TestsNew\Helpers\Traits;

trait MockDataTrait
{
    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function mockGetDataFromAsset(string $fileName)
    {
        list ($name, $ext) = explode('.', $fileName);
        if ($ext === 'json') {
            $content = file_get_contents(dirname(__DIR__) . '/assets/' . $fileName);

            return json_decode($content, true);
        } elseif ($ext === 'php') {
            $path = dirname(__DIR__) . '/assets/' . $fileName;

            return include($path);
        }
    }
}
