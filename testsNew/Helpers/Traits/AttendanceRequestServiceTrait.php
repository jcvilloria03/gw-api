<?php

namespace TestsNew\Helpers\Traits;

use App\Attendance\AttendanceRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

trait AttendanceRequestServiceTrait
{
    protected function mockAttendanceRequestService(array $responses = [])
    {
        $httpResponse = [];

        foreach($responses as $response){
            array_push($httpResponse,
                new Response(
                    $response["statusCode"], 
                    [], 
                    json_encode($response["responseBody"]))
            );
        }

        $mock = new MockHandler($httpResponse);
        $handlerStack = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handlerStack]);

        $this->app->bind(AttendanceRequestService::class, function () use ($mockClient) {
            return new AttendanceRequestService($mockClient);
        });
    }
}

