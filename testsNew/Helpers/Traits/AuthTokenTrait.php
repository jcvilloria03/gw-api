<?php

namespace TestsNew\Helpers\Traits;

use Firebase\JWT\JWT;

trait AuthTokenTrait
{
    public function generateAuthorizationToken(array $payload)
    {
        return JWT::encode($payload, 'test');
    }
}
