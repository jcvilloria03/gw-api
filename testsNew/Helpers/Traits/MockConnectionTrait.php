<?php


namespace TestsNew\Helpers\Traits;

use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Mockery;
use PDO;

trait MockConnectionTrait
{
    public function setUpMockConnectionTrait()
    {
        $this->app->singleton('db.factory', function ($app) {
            $cf = Mockery::mock(ConnectionFactory::class);
            $cf->shouldReceive('make')->andThrow(\Exception::class, 'You should mock your connection');

            return $cf;
        });

        $this->app->singleton('db', function ($app) {
            return new DatabaseManager($app, $app['db.factory']);
        });

        $this->app->singleton('db.connection', function ($app) {
            return $app['db']->connection();
        });

        Model::setConnectionResolver($this->app['db']);
    }

    /**
     * @param string $driver
     * @param array $methods
     * @return \Mockery\MockInterface|\Illuminate\Database\Connection
     */
    public function mockConnection(string $driver): Connection
    {
        $mockDriverFunction = 'mock' . ucwords($driver) . 'Connection';
        $connection = call_user_func([$this, $mockDriverFunction]);
        $connection->useDefaultQueryGrammar();
        $connection->useDefaultPostProcessor();
        $connection->setPdo($this->mockPdo());

        return $connection;
    }

    /**
     * @param string $model
     * @param \Illuminate\Database\Connection|null $connection
     */
    public function mockConnectionResolver(Connection $connection)
    {
        $connectionResolver = new ConnectionResolver(['test' => $connection]);
        $connectionResolver->setDefaultConnection('test');

        return $connectionResolver;
    }

    public function mockPdo(): PDO
    {
        $pdo = Mockery::mock(PDO::class);
        $pdo->shouldReceive('prepare')->andReturn(new \PDOStatement($pdo));

        return $pdo;
    }

    protected function mockMysqlConnection(): MySqlConnection
    {
        return Mockery::mock(MySqlConnection::class)->makePartial();
    }
}
