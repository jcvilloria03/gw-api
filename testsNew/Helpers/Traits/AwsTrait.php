<?php

namespace TestsNew\Helpers\Traits;

use Aws\MockHandler;
use Aws\Result;
use Aws\S3\S3Client;
use Aws\Sdk;
use Illuminate\Support\Collection;
use Mockery;
use TestsNew\Helpers\Steam\TestStreamWrapper;
use function Aws\manifest;

trait AwsTrait
{
    protected $app;

    protected function mockAwsSdk(array $results = [])
    {
        $this->app->singleton('aws', function () use ($results) {
            $config = $this->app->make('config')->get('aws');
            $config['handler'] = new MockHandler(Collection::make($results)->map(function ($result) {
                return new Result($result);
            })->all());

            return Mockery::mock(Sdk::class, [$config])->makePartial();
        });
    }

    /**
     * @return Sdk|Mockery\MockInterface
     */
    protected function getMockAwsSdk(): Sdk
    {
        return $this->app->make('aws');
    }

    public function mockAwsS3Client()
    {
        $s3Mocker = Mockery::mock(S3Client::class);
        $aws = $this->getMockAwsSdk();
        $aws->shouldReceive('createClient')
            ->with('s3')
            ->andReturn($s3Mocker)
        ;

        return $s3Mocker;
    }


    public function mockS3StreamWrapper()
    {
        $this->mockAwsS3Client();
        $s3 = $this->getMockAwsSdk()->createClient('s3');
        $s3->shouldReceive('registerStreamWrapper')
            ->andReturnUsing(function () {
                stream_wrapper_register('s3', TestStreamWrapper::class);
            })
        ;
    }
}
