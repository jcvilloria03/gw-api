<?php

namespace TestsNew\Helpers\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\JsonResponse;
use Mockery;

trait RequestServiceTrait
{
    protected function mockRequestService(string $requestServiceClass, array $responses = [])
    {
        $httpResponses = array_map(function ($response) {
            return new Response(
                $response['code'] ?? 200,
                $response['headers'] ?? [],
                json_encode($response['body'])
            );
        }, $responses);

        $mock = new MockHandler($httpResponses);
        $handlerStack = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handlerStack]);

        $this->app->bind($requestServiceClass, function () use ($requestServiceClass, $mockClient) {
            return new $requestServiceClass($mockClient);
        });
    }

    protected function mockClass(string $requestServiceClass, array $values)
    {
        $mockRequestServiceClass = Mockery::mock($requestServiceClass, $values);

        app()->instance($requestServiceClass, $mockRequestServiceClass);
    }

    protected function getJsonResponse($response)
    {
        return new JsonResponse(
            isset($response['body']) ? json_encode($response['body']) : null,
            $response['code'] ?? 200,
            $response['headers'] ?? []
        );
    }
}

