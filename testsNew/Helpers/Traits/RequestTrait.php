<?php

namespace TestsNew\Helpers\Traits;

use App\Account\AccountRequestService;
use App\Adjustment\PhilippineAdjustmentRequestService;
use App\AdminDashboard\AdminDashboardRequestService;
use App\AffectedEmployee\AffectedEmployeeRequestService;
use App\AffectedEntity\AffectedEntityRequestService;
use App\Allowance\PhilippineAllowanceRequestService;
use App\AllowanceType\PhilippineAllowanceTypeRequestService;
use App\Announcement\AnnouncementRequestService;
use App\AnnualEarning\AnnualEarningRequestService;
use App\Approval\ApprovalRequestService;
use App\Attendance\AttendanceRequestService;
use App\Authentication\AuthenticationRequestService;
use App\Authz\AuthzRequestService;
use App\Bank\BankRequestService;
use App\BasePay\BasePayRequestService;
use App\BasicPayAdjustment\BasicPayAdjustmentRequestService;
use App\Bonus\PhilippineBonusRequestService;
use App\BonusType\PhilippineBonusTypeRequestService;
use App\Commission\PhilippineCommissionRequestService;
use App\CommissionType\PhilippineCommissionTypeRequestService;
use App\Company\CompanyRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Contribution\ContributionRequestService;
use App\CostCenter\CostCenterRequestService;
use App\Country\CountryRequestService;
use App\DayHourRate\DayHourRateRequestService;
use App\Deduction\PhilippineDeductionRequestService;
use App\DeductionType\PhilippineDeductionTypeRequestService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\Department\DepartmentRequestService;
use App\Earning\EarningRequestService;
use App\Employee\EmployeeRequestService;
use App\EmployeeRequestModule\EmployeeRequestModuleRequestService;
use App\EmploymentType\EmploymentTypeRequestService;
use App\ESS\EssAnnouncementRequestService;
use App\ESS\EssApprovalRequestService;
use App\ESS\EssAttachmentRequestService;
use App\ESS\EssClockStateService;
use App\ESS\EssEmployeeRequestRequestService;
use App\Ess\EssEmployeeRequestService;
use App\ESS\EssNotificationRequestService;
use App\ESS\EssOvertimeRequestRequestService;
use App\Ess\EssPayrollRequestService;
use App\ESS\EssPayslipRequestService;
use App\ESS\EssShiftChangeRequestRequestService;
use App\ESS\EssTimeDisputeRequestRequestService;
use App\ESS\EssUndertimeRequestRequestService;
use App\FinalPay\FinalPayRequestService;
use App\GovernmentForm\GovernmentFormRequestService;
use App\Holiday\HolidayRequestService;
use App\HoursWorked\HoursWorkedRequestService;
use App\Http\Controllers\SalpayIntegrationController;
use App\Http\Controllers\SubscriptionPaymentController;
use App\Http\Controllers\AuditTrailController;
use App\Jobs\JobsRequestService;
use App\LeaveCredit\LeaveCreditRequestService;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use App\LeaveRequest\LeaveRequestRequestService;
use App\LeaveType\LeaveTypeRequestService;
use App\Location\LocationRequestService;
use App\Location\TimeAttendanceLocationRequestService;
use App\MaxClockOut\MaxClockOutRequestService;
use App\NightShift\NightShiftRequestService;
use App\Notification\NotificationRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use App\Payroll\PayrollFinalPayRequestService;
use App\Payroll\PayrollRequestService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\PayrollGroup\PhilippinePayrollGroupRequestService;
use App\PayrollLoan\PayrollLoanDownloadRequestService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use App\Payslip\PayslipRequestService;
use App\Position\PositionRequestService;
use App\ProductSeat\ProductSeatRequestService;
use App\Project\ProjectRequestService;
use App\Rank\RankRequestService;
use App\RestDay\RestDayRequestService;
use App\Role\RoleRequestService;
use App\Schedule\ScheduleRequestService;
use App\Shift\ShiftRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Tag\TagRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\Tax\TaxRequestService;
use App\Team\TeamRequestService;
use App\Termination\TerminationInformationRequestService;
use App\TimeRecord\TimeRecordRequestService;
use App\User\UserRequestService;
use App\Workflow\WorkflowRequestService;
use App\WorkflowEntitlement\WorkflowEntitlementRequestService;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Collection;
use TestsNew\Helpers\Request\CustomerRequest;
use TestsNew\Helpers\Request\AccountRequest;
use TestsNew\Helpers\Request\CompanyRequest;
use TestsNew\Helpers\Request\NotificationRequest;
use TestsNew\Helpers\Request\UndertimeRequest;
use TestsNew\Helpers\Request\WorkflowRequest;
use TestsNew\Helpers\Request\EssClockStateRequest;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Request\Client;
use TestsNew\Helpers\Request\EmployeeRequest;
use TestsNew\Helpers\Request\Expectation\Expectation;
use TestsNew\Helpers\Request\PayrollGroupRequest;
use TestsNew\Helpers\Request\RequestBus;
use TestsNew\Helpers\Request\UserRequest;
use TestsNew\Helpers\Request\PayrollAvailableItemRequest;
use TestsNew\Helpers\Request\TimeDisputeRequest;
use TestsNew\Helpers\Request\ShiftChangeRequest;
use TestsNew\Helpers\Request\OvertimeRequest;
use TestsNew\Helpers\Request\AttendanceRequest;
use TestsNew\Helpers\Request\EmployeeRequestRequest;

/**
 *
 */
trait RequestTrait
{
    protected $app;

    protected function assertPostConditionsRequestTrait()
    {
        if (!$this->app->bound(RequestBus::class)) {
            return;
        }

        $response = $this->app->make(RequestBus::class)->assertExpectations();
        $this->assertTrue($response['valid'], $response['message'] ?? '');
    }

    protected function mockClientRequestService()
    {
        $requestBus = new RequestBus([
            CustomerRequest::create(),
            AuthzRequest::create(),
            UserRequest::create(),
            NotificationRequest::create(),
            PayrollGroupRequest::create(),
            AccountRequest::create(),
            CompanyRequest::create(),
            UndertimeRequest::create(),
            WorkflowRequest::create(),
            EssClockStateRequest::create(),
            EmployeeRequest::create(),
            PayrollAvailableItemRequest::create(),
            TimeDisputeRequest::create(),
            ShiftChangeRequest::create(),
            OvertimeRequest::create(),
            AttendanceRequest::create(),
            EmployeeRequestRequest::create(),
        ]);

        $this->app->singleton(RequestBus::class, function () use ($requestBus) {
            return $requestBus;
        });

        $this->registerCompany();
        $this->registerPayroll();
        $this->registerAuthzApi();
        $this->registerTaApi();
        $this->registerNotificationsApi();
        $this->registerRequestsApi();
        $this->registerSubscriptionsApi();
        $this->registerTimeclock();
        $this->registerPayrollLoanApi();
        $this->registerAttendanceApi();
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    protected function registerSubscriptionsApi()
    {
        $subscriptionsUri = getenv('SUBSCRIPTIONS_API_URI');
        $this->app->bind(SubscriptionsRequestService::class, function () use ($subscriptionsUri) {
            return new SubscriptionsRequestService($this->createClient([
                'base_uri' => $subscriptionsUri
            ]));
        });
    }

    protected function registerRequestsApi()
    {
        $requestsApi = getenv('REQUESTS_API_URI');
        $this->app->bind(EssEmployeeRequestRequestService::class, function () use ($requestsApi) {
            return new EssEmployeeRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssOvertimeRequestRequestService::class, function () use ($requestsApi) {
            return new EssOvertimeRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssAttachmentRequestService::class, function () use ($requestsApi) {
            return new EssAttachmentRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssApprovalRequestService::class, function () use ($requestsApi) {
            return new EssApprovalRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(ApprovalRequestService::class, function () use ($requestsApi) {
            return new ApprovalRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssTimeDisputeRequestRequestService::class, function () use ($requestsApi) {
            return new EssTimeDisputeRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssShiftChangeRequestRequestService::class, function () use ($requestsApi) {
            return new EssShiftChangeRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssUndertimeRequestRequestService::class, function () use ($requestsApi) {
            return new EssUndertimeRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(LeaveRequestRequestService::class, function () use ($requestsApi) {
            return new LeaveRequestRequestService($this->createClient([
                'base_uri' => $requestsApi
            ]));
        });
    }

    public function registerPayroll()
    {
        $this->app->bind(SalpayIntegrationController::class, function () {
            return new SalpayIntegrationController(
                $this->createClient([
                    'base_uri' => getenv('SALPAY_INTEGRATION_API_URI')
                ])
            );
        });
        $payrollUri = getenv('PAYROLL_API_URI');


        $this->app->bind(SubscriptionPaymentController::class, function () {
            return new SubscriptionPaymentController(
                $this->createClient([
                    'base_uri' => getenv('SUBSCRIPTIONS_PAYMENT_API_URI')
                ])
            );
        });

        $this->app->bind(AuditTrailController::class, function () {
            return new AuditTrailController(
                $this->createClient([
                    'base_uri' => getenv('AUDIT_TRAIL_API_URI')
                ])
            );
        });

        $this->app->bind(AnnualEarningRequestService::class, function () use ($payrollUri) {
            return new AnnualEarningRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PayrollRequestService::class, function () use ($payrollUri) {
            return new PayrollRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PayrollFinalPayRequestService::class, function () use ($payrollUri) {
            return new PayrollFinalPayRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });

        $this->app->bind(PayslipRequestService::class, function () use ($payrollUri) {
            return new PayslipRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(GovernmentFormRequestService::class, function () use ($payrollUri) {
            return new GovernmentFormRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(EssPayrollRequestService::class, function () use ($payrollUri) {
            return new EssPayrollRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(EssPayslipRequestService::class, function () use ($payrollUri) {
            return new EssPayslipRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineBonusTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineBonusTypeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAllowanceTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineAllowanceTypeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineCommissionTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineCommissionTypeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineDeductionTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineDeductionTypeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(OtherIncomeTypeRequestService::class, function () use ($payrollUri) {
            return new OtherIncomeTypeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(OtherIncomeRequestService::class, function () use ($payrollUri) {
            return new OtherIncomeRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineBonusRequestService::class, function () use ($payrollUri) {
            return new PhilippineBonusRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineCommissionRequestService::class, function () use ($payrollUri) {
            return new PhilippineCommissionRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAllowanceRequestService::class, function () use ($payrollUri) {
            return new PhilippineAllowanceRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineDeductionRequestService::class, function () use ($payrollUri) {
            return new PhilippineDeductionRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAdjustmentRequestService::class, function () use ($payrollUri) {
            return new PhilippineAdjustmentRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(FinalPayRequestService::class, function () use ($payrollUri) {
            return new FinalPayRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });

        $this->app->bind(BankRequestService::class, function () use ($payrollUri) {
            return new BankRequestService($this->createClient([
                'base_uri' => $payrollUri
            ]));
        });

        $authenticationUri = getenv('AUTHENTICATION_API_URI');

        $this->app->bind(AuthenticationRequestService::class, function () use ($authenticationUri) {
            return new AuthenticationRequestService($this->createClient([
                'base_uri' => $authenticationUri
            ]));
        });

        $jobsManagementUri = getenv('JOBS_MANAGEMENT_API_URI');

        $this->app->bind(JobsRequestService::class, function () use ($jobsManagementUri) {
            return new JobsRequestService($this->createClient([
                'base_uri' => $jobsManagementUri
            ]));
        });
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function registerCompany()
    {
        $companyUri = getenv('COMPANY_API_URI');

        $this->app->bind(AccountRequestService::class, function () use ($companyUri) {
            return new AccountRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CountryRequestService::class, function () use ($companyUri) {
            return new CountryRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PhilippineCompanyRequestService::class, function () use ($companyUri) {
            return new PhilippineCompanyRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CompanyRequestService::class, function () use ($companyUri) {
            return new CompanyRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(UserRequestService::class, function () use ($companyUri) {
            return new UserRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(LocationRequestService::class, function () use ($companyUri) {
            return new LocationRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TimeAttendanceLocationRequestService::class, function () use ($companyUri) {
            return new TimeAttendanceLocationRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PayrollGroupRequestService::class, function () use ($companyUri) {
            return new PayrollGroupRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PhilippinePayrollGroupRequestService::class, function () use ($companyUri) {
            return new PhilippinePayrollGroupRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EmployeeRequestService::class, function () use ($companyUri) {
            return new EmployeeRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EssEmployeeRequestService::class, function () use ($companyUri) {
            return new EssEmployeeRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CostCenterRequestService::class, function () use ($companyUri) {
            return new CostCenterRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(DepartmentRequestService::class, function () use ($companyUri) {
            return new DepartmentRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PositionRequestService::class, function () use ($companyUri) {
            return new PositionRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(RankRequestService::class, function () use ($companyUri) {
            return new RankRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EmploymentTypeRequestService::class, function () use ($companyUri) {
            return new EmploymentTypeRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TeamRequestService::class, function () use ($companyUri) {
            return new TeamRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(BasePayRequestService::class, function () use ($companyUri) {
            return new BasePayRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TaxRequestService::class, function () use ($companyUri) {
            return new TaxRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(ContributionRequestService::class, function () use ($companyUri) {
            return new ContributionRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(AffectedEmployeeRequestService::class, function () use ($companyUri) {
            return new AffectedEmployeeRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(AffectedEntityRequestService::class, function () use ($companyUri) {
            return new AffectedEntityRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(BasicPayAdjustmentRequestService::class, function () use ($companyUri) {
            return new BasicPayAdjustmentRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(EarningRequestService::class, function () use ($companyUri) {
            return new EarningRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(ProductSeatRequestService::class, function () use ($companyUri) {
            return new ProductSeatRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(TerminationInformationRequestService::class, function () use ($companyUri) {
            return new TerminationInformationRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(RoleRequestService::class, function () use ($companyUri) {
            return new RoleRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(MaxClockOutRequestService::class, function () use ($companyUri) {
            return new MaxClockOutRequestService($this->createClient([
                'base_uri' => $companyUri
            ]));
        });
    }

    protected function registerTaApi()
    {
        $taUri = getenv('TA_API_URI');
        $this->app->bind(AdminDashboardRequestService::class, function () use ($taUri) {
            return new AdminDashboardRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(DefaultScheduleRequestService::class, function () use ($taUri) {
            return new DefaultScheduleRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ProjectRequestService::class, function () use ($taUri) {
            return new ProjectRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });

        $this->app->bind(DayHourRateRequestService::class, function () use ($taUri) {
            return new DayHourRateRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveTypeRequestService::class, function () use ($taUri) {
            return new LeaveTypeRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveEntitlementRequestService::class, function () use ($taUri) {
            return new LeaveEntitlementRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(HolidayRequestService::class, function () use ($taUri) {
            return new HolidayRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(TardinessRuleRequestService::class, function () use ($taUri) {
            return new TardinessRuleRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(WorkflowRequestService::class, function () use ($taUri) {
            return new WorkflowRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(NightShiftRequestService::class, function () use ($taUri) {
            return new NightShiftRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(EmployeeRequestModuleRequestService::class, function () use ($taUri) {
            return new EmployeeRequestModuleRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(WorkflowEntitlementRequestService::class, function () use ($taUri) {
            return new WorkflowEntitlementRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ScheduleRequestService::class, function () use ($taUri) {
            return new ScheduleRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ShiftRequestService::class, function () use ($taUri) {
            return new ShiftRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(TagRequestService::class, function () use ($taUri) {
            return new TagRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(RestDayRequestService::class, function () use ($taUri) {
            return new RestDayRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveCreditRequestService::class, function () use ($taUri) {
            return new LeaveCreditRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(HoursWorkedRequestService::class, function () use ($taUri) {
            return new HoursWorkedRequestService($this->createClient([
                'base_uri' => $taUri
            ]));
        });
    }

    protected function registerNotificationsApi()
    {
        $notificationsApi = getenv('NOTIFICATIONS_API_URI');
        $this->app->bind(EssNotificationRequestService::class, function () use ($notificationsApi) {
            return new EssNotificationRequestService($this->createClient([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(EssAnnouncementRequestService::class, function () use ($notificationsApi) {
            return new EssAnnouncementRequestService($this->createClient([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(AnnouncementRequestService::class, function () use ($notificationsApi) {
            return new AnnouncementRequestService($this->createClient([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(NotificationRequestService::class, function () use ($notificationsApi) {
            return new NotificationRequestService($this->createClient([
                'base_uri' => $notificationsApi
            ]));
        });
    }

    protected function registerAuthzApi()
    {
        $authzUri = getenv('AUTHZ_API_URL');

        $this->app->bind(AuthzRequestService::class, function () use ($authzUri) {
            return new AuthzRequestService($this->createClient(['base_uri' => $authzUri]));
        });
    }

    protected function registerPayrollLoanApi()
    {
        $payrollLoanApi = getenv('PAYROLL_LOAN_API_URI');

        $this->app->bind(PayrollLoanRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanRequestService($this->createClient([
                'base_uri' => $payrollLoanApi
            ]));
        });

        $this->app->bind(PayrollLoanTypeRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanTypeRequestService($this->createClient([
                'base_uri' => $payrollLoanApi
            ]));
        });

        $this->app->bind(PayrollLoanDownloadRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanDownloadRequestService($this->createClient([
                'base_uri' => $payrollLoanApi
            ]));
        });
    }

    protected function registerTimeclock()
    {
        $timeclockUrl = getenv('TIMECLOCK_URL');

        $this->app->bind(EssClockStateService::class, function () use ($timeclockUrl) {
            return new EssClockStateService($this->createClient([
                'base_uri' => $timeclockUrl
            ]));
        });

        $this->app->bind(TimeRecordRequestService::class, function () use ($timeclockUrl) {
            return new TimeRecordRequestService($this->createClient([
                'base_uri' => $timeclockUrl
            ]));
        });
    }

    protected function registerAttendanceApi()
    {
        $attendanceUrl = getenv('ATTENDANCE_API_URI');

        $this->app->bind(AttendanceRequestService::class, function () use ($attendanceUrl) {
            return new AttendanceRequestService($this->createClient([
                'base_uri' => $attendanceUrl
            ]));
        });
    }

    protected function createClient(array $options)
    {
        $handlerStack = HandlerStack::create($this->app->make(RequestBus::class));
        $options['handler'] = $handlerStack;

        return new Client($options);
    }

    protected function getRequestBus(): RequestBus
    {
        return $this->app->make(RequestBus::class);
    }

    protected function getRequestStorage(string $name): Collection
    {
        return $this->getRequestBus()->getDataStorage($name);
    }

    protected function shouldExpectRequest(string $method, string $uri): Expectation
    {
        return $this->getRequestBus()->newExpectation($method, $uri);
    }
}
