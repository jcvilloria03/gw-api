<?php

namespace TestsNew\Helpers\Traits;

use TestsNew\Helpers\Request\AccountRequest;
use TestsNew\Helpers\Request\UserRequest;

trait EssentialTrait
{
    public function addUserEssentialTrait(array $subject, array $userdata)
    {
        $this->getRequestStorage(UserRequest::STORAGE_USER_ESSENTIAL_DATA)->push(
            [
                'subject' => $subject,
                'userData' => [$userdata]
            ]
        );
    }

    public function addAccountEssentialTrait(int $accountId, array $data)
    {
        $this->getRequestStorage(AccountRequest::STORAGE_ACCOUNT_ESSENTIAL)->push([
            'account_id' => $accountId,
            'essential_data' => $data
        ]);
    }
}
