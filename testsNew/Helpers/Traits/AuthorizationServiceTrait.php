<?php

namespace TestsNew\Helpers\Traits;

use Dingo\Api\Routing\Router;
use TestsNew\Helpers\Middlewares\UserMiddleware;
use ReflectionClass;

trait AuthorizationServiceTrait
{
    protected function mockUserData($userData)
    {
        $reflection = new ReflectionClass(app());

        $property = $reflection->getProperty('middleware');
        $property->setAccessible(true);

        $middlewares = array_merge($property->getValue(app()), [
            new UserMiddleware($userData)
        ]);

        $property->setValue(app(), $middlewares);
        $property->setAccessible(false);
    }

    public function mockUserMiddleware($userData)
    {
        $this->app->middleware(['user' => UserMiddleware::class]);
        $this->app->instance(UserMiddleware::class, new UserMiddleware($userData));
    }

    private function getRouter(): Router
    {
        return $this->app->make(Router::class);
    }
}

