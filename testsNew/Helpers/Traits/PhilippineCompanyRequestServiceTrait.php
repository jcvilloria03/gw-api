<?php

namespace TestsNew\Helpers\Traits;

use App\Company\PhilippineCompanyRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

trait PhilippineCompanyRequestServiceTrait
{

    protected function mockPhilippineCompanyRequestService(array $responses = [])
    {
        $httpResponse = [];

        foreach($responses as $response){
            array_push($httpResponse,
                new Response(
                    $response["statusCode"], 
                    [], 
                    json_encode($response["responseBody"]))
            );
        }

        $mock = new MockHandler($httpResponse);
        $handlerStack = HandlerStack::create($mock);
        $mockClient = new Client(['handler' => $handlerStack]);

        $this->app->bind(PhilippineCompanyRequestService::class, function () use ($mockClient) {
            return new PhilippineCompanyRequestService($mockClient);
        });
    }
}

