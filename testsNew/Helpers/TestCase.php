<?php

namespace TestsNew\Helpers;

use Illuminate\Support\Arr;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

/**
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 *
 */
class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    use MockeryPHPUnitIntegration {
        MockeryPHPUnitIntegration::assertPostConditions as assertPostConditionsMockeryPHPUnitIntegration;
    }

    protected function assertPostConditions()
    {
        $this->assertPostConditionsMockeryPHPUnitIntegration();
        $postConditions = ['assertPostConditionsMockeryPHPUnitIntegration' => true];
        foreach (class_uses($this) as $traitName) {
            $paths = explode('\\', $traitName);
            $functionName = 'assertPostConditions' . $paths[count($paths) - 1];
            if (method_exists($this, $functionName) && !Arr::get($functionName, false)) {
                call_user_func([$this, $functionName]);
                $postConditions[$functionName] = true;
            }
        }
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require base_path() . '/bootstrap/app.php';
    }

    /**
     * See if the response has a header.
     *
     * @param $header
     * @return $this
     */
    public function seeHasHeader($header)
    {
        $this->assertTrue(
            $this->response->headers->has($header),
            "Response should have the header '{$header}' but does not."
        );

        return $this;
    }

    /**
     * Asserts that the response header matches a given regular expression
     *
     * @param $header
     * @param $regexp
     * @return $this
     */
    public function seeHeaderWithRegExp($header, $regexp)
    {
        $this
            ->seeHasHeader($header)
            ->assertRegExp(
                $regexp,
                $this->response->headers->get($header)
            );

        return $this;
    }

    /**
     * Helper function to randomize decimals,
     * as by default you can only randomize integers
     *
     * @param double $min
     * @param double $max
     * @param int $scale
     * @return double
     */
    public function randomDecimal($min, $max, $scale = 2)
    {
        $multiplier = pow(10, $scale);
        return (mt_rand($min * $multiplier, $max * $multiplier)) / $multiplier;
    }

    public static function assertThrows(string $class, callable $execute, callable $inspect = null)
    {
        try {
            $execute();
        } catch (\PHPUnit_Framework_ExpectationFailedException $e) {
            throw $e;
        } catch (\Throwable $e) {
            static::assertThat($e, new \PHPUnit_Framework_Constraint_Exception($class));
            if ($e instanceof $class &&  $inspect !== null) {
                $inspect($e);
            }
            return;
        }

        static::assertThat(null, new \PHPUnit_Framework_Constraint_Exception($class));
    }

    public static function assertIsArray($data, string $message = '')
    {
        static::assertInternalType('array', $data, $message);
    }
}
