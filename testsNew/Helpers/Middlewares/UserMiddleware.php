<?php

namespace TestsNew\Helpers\Middlewares;

use Closure;

class UserMiddleware
{
    /**
     * Mock user data
     *
     * @var array
     */
    protected $userData;

    /**
     * Constructs UserMiddleware
     *
     * @param array $userData Mock user data
     */
    public function __construct(array $userData)
    {
        $this->userData = $userData;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->attributes->set('user', $this->userData);

        if (array_key_exists('employee_id', $this->userData)
            && array_key_exists('employee_company_id', $this->userData)) {
            $request->attributes->set('companies_users', [
                $this->userData
            ]);
        }

        return $next($request);
    }

    public function terminate()
    {
    }
}
