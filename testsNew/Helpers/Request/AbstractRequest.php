<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use TestsNew\Helpers\Traits\MockDataTrait;

abstract class AbstractRequest
{
    use MockDataTrait {
        MockDataTrait::mockGetDataFromAsset as getDataFromAsset;
    }

    abstract public function getHandlers(): array;

    public static function create()
    {
        return new static();
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function getDataFromAsset(string $fileName)
    {
        list ($name, $ext) = explode('.', $fileName);
        if ($ext === 'json') {
            $content = file_get_contents(dirname(__DIR__) . '/assets/' . $fileName);

            return json_decode($content, true);
        } elseif ($ext === 'php') {
            $path = dirname(__DIR__) . '/assets/' . $fileName;

            return include($path);
        }
    }

    protected function generateJsonResponse(int $statusCode, array $data)
    {
        return new Response($statusCode, [], json_encode($data));
    }
}
