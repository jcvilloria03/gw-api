<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class UndertimeRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('undertimes.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'undertimes';
    }

    public function getHandlers(): array
    {
        return [
            'get_undertime_request' => [
                'method' => 'GET',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/undertime_request/{id}',
                'action' => [$this, 'getUndertimeRequest'],
            ],
        ];
    }

    public function getUndertimeRequest(Request $request): Response
    {
        $undertime = collect($this->getDataFromAsset('undertimes.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $undertime);
    }
}
