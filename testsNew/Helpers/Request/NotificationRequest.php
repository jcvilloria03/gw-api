<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class NotificationRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('announcement_notifications.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'announcement_notifications';
    }

    public function getHandlers(): array
    {
        return [
            'get_notifications' => [
                'method' => 'GET',
                'base_uri' => getenv('NOTIFICATIONS_API_URI'),
                'path' => '/user/{id}/notifications',
                'action' => [$this, 'getNotifications'],
            ],
            'get_announcement_notifications' => [
                'method' => 'GET',
                'base_uri' => getenv('NOTIFICATIONS_API_URI'),
                'path' => '/user/{id}/announcement_notifications',
                'action' => [$this, 'getAnnouncementNotifications'],
            ],
        ];
    }

    public function getNotifications(Request $request): Response
    {
        $userNotifications = collect(
            $this->getDataFromAsset('user_notifications.json')
        )->all();

        return $this->generateJsonResponse(200, $userNotifications);
    }

    public function getAnnouncementNotifications(Request $request): Response
    {
        $announcementNotifications = collect(
            $this->getDataFromAsset('announcement_notifications.json')
        )->all();

        return $this->generateJsonResponse(200, $announcementNotifications);
    }
}
