<?php

namespace TestsNew\Helpers\Request\Expectation;

use App\Authz\AuthzDataScope;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use function GuzzleHttp\Psr7\stream_for;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class Expectation
{
    private $times;
    /**
     * @var Request|null
     */
    private $request;
    private $responses;
    private $called;
    private $withBody;

    public function __construct(string $method, string $uri)
    {
        $this->times = -1;
        $this->called = 0;
        $this->request = new Request($method, $uri);
        $this->responses = [];
        $this->withBody = false;
    }

    public function once(): self
    {
        $this->times = 1;

        return $this;
    }

    public function twice(): self
    {
        $this->times = 2;

        return $this;
    }

    public function times(int $times): self
    {
        $this->times = $times;

        return $this;
    }

    public function withRequest($method, $uri, array $headers = [], $body = null): self
    {
        $this->request = new Request($method, $uri, $headers, $body);
        $this->withBody = $body !== null;

        return $this;
    }

    public function withHeaders(array $headers): self
    {
        foreach ($headers as $header => $value) {
            $this->request = $this->request->withAddedHeader($header, $value);
        }

        return $this;
    }

    public function withBody($body): self
    {
        $this->request = $this->request->withBody(stream_for($body));
        $this->withBody = $body !== null;

        return $this;
    }

    public function withQuery(array $query): self
    {
        $uri = $this->request->getUri()->withQuery(http_build_query($query));
        $this->request = $this->request->withUri($uri);

        return $this;
    }

    public function withDataScope(array $dataScope = [])
    {
        parse_str($this->request->getUri()->getQuery(), $queries);
        $queries['authz_data_scope'] = json_encode($dataScope);
        $this->withQuery($queries);

        return $this;
    }

    public function andReturnResponse(...$args): self
    {
        $this->responses = $args;

        return $this;
    }

    public function isSameRequest(Request $request): bool
    {
        return $this->request !== null
            && ($this->times >=0 ? $this->called < $this->times : true)
            && $this->request->getUri()->getPath() === $request->getUri()->getPath()
            && $this->request->getMethod() === $request->getMethod()
            && $this->hasSameBody($request)
            && $this->hasSameHeaders($request)
            && $this->checkWithQuery($request)
        ;
    }

    private function checkWithQuery(Request $request): bool
    {
        $correct = true;
        parse_str($request->getUri()->getQuery(), $requestQuery);
        parse_str($this->request->getUri()->getQuery(), $expectedQueries);

        foreach ($expectedQueries as $query => $value) {
            if (!Arr::has($requestQuery, $query)) {
                $correct = false;
                break;
            } elseif (Arr::get($requestQuery, $query, null) !== $value) {
                $correct = false;
                break;
            }
        }

        return $correct;
    }

    private function hasSameBody(Request $request): bool
    {
        if ($this->withBody) {
            $content = $this->request->getBody()->getContents();
            $requestContent = $request->getBody()->getContents();

            return $content === $requestContent;
        }

        return true;
    }


    private function hasSameHeaders(Request $request): bool
    {
        $same = true;
        foreach ($this->request->getHeaders() as $header => $value) {
            if (!($request->hasHeader($header) && $request->getHeader($header) == $value)) {
                $same = false;
                break;
            }
        }

        return $same;
    }

    /**
     * @param Request $request
     *
     * @return Response|null
     */
    public function call(Request $request)
    {
        $response = null;
        if ($this->isSameRequest($request)) {
            if (isset($this->responses[$this->called])) {
                $response = $this->responses[$this->called];
            }
            $this->called++;
        }

        return $response;
    }

    public function hasMetExpectation(): bool
    {
        if ($this->times !== -1) {
            return $this->times === $this->called;
        }

        return true;
    }

    public function getExpectedNumberOfCalls(): int
    {
        return $this->times;
    }

    public function getActualNumberOfCalls(): int
    {
        return $this->called;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function expectingBody(): bool
    {
        return $this->withBody;
    }
}
