<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class OvertimeRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('overtimes.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'overtimes';
    }

    public function getHandlers(): array
    {
        return [
            'get_overtime_request' => [
                'method' => 'GET',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/overtime_request/{id}',
                'action' => [$this, 'getOvertimeRequest'],
            ],
        ];
    }

    public function getOvertimeRequest(Request $request): Response
    {
        $overtime = collect($this->getDataFromAsset('overtimes.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $overtime);
    }
}
