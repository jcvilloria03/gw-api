<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class PayrollAvailableItemRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('payroll_items.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'payroll_items';
    }

    public function getHandlers(): array
    {
        return [
            'get_company_payroll_available_item' => [
                'method' => 'GET',
                'base_uri' => getenv('PAYROLL_API_URI'),
                'path' => '/company/{id}/payroll/{type}/available_items',
                'action' => [$this, 'getCompanyPayrollAvailableItems'],
            ],
        ];
    }

    public function getCompanyPayrollAvailableItems(Request $request): Response
    {
        $availableItems = collect($this->getDataFromAsset('payroll_items.json'))
            ->filter(function ($value, $key) use ($request) {
                return $value['other_income']['company_id'] == $request->attributes->get('id');
            })
            ->all();

        return $this->generateJsonResponse(200, ['data' => $availableItems]);
    }
}
