<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class TimeDisputeRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('time_disputes.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'time_disputes';
    }

    public function getHandlers(): array
    {
        return [
            'get_time_dispute_request' => [
                'method' => 'GET',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/time_dispute_request/{id}',
                'action' => [$this, 'getTimeDisputeRequest'],
            ],
        ];
    }

    public function getTimeDisputeRequest(Request $request): Response
    {
        $timeDispute = collect($this->getDataFromAsset('time_disputes.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $timeDispute);
    }
}
