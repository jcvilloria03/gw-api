<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class CustomerRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('customers.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'customers';
    }

    public function getHandlers(): array
    {
        return [
            'get_customer_by_account_id' => [
                'method' => 'GET',
                'base_uri' => getenv('SUBSCRIPTIONS_API_URI'),
                'path' => '/subscriptions/customers',
                'action' => [$this, 'getCustomerByAccountId'],
            ],
            'get_customer_subscription_stats' => [
                'method' => 'GET',
                'base_uri' => getenv('SUBSCRIPTIONS_API_URI'),
                'path' => '/subscriptions/{id}/stats',
                'action' => [$this, 'getCustomerSubscriptionStats'],
            ],
        ];
    }

    public function getCustomerSubscriptionStats(Request $request): Response
    {
        $customer = collect($this->getDataFromAsset('customer_subscription_stats.json'))->all();

        return $this->generateJsonResponse(200, $customer);
    }

    public function getCustomerByAccountId(Request $request): Response
    {
        $customer = collect($this->getDataFromAsset('customers.json'))->all();

        return $this->generateJsonResponse(200, $customer);
    }
}
