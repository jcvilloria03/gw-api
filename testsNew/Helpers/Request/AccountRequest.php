<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithMultipleStorageInterface;

class AccountRequest extends AbstractRequest implements WithMultipleStorageInterface
{
    const STORAGE_ACCOUNT_ESSENTIAL = 'account_essentials';

    private $storages;

    public function __construct()
    {
        $this->storages = [
            self::STORAGE_ACCOUNT_ESSENTIAL => new DefaultDataStorage([]),
        ];
    }

    public function getStorages(): array
    {
        return $this->storages;
    }

    public static function getStorageNames(): array
    {
        return [
            self::STORAGE_ACCOUNT_ESSENTIAL,
        ];
    }

    public function getStorage(string $name): AbstractDataStorage
    {
        return $this->storages[$name];
    }

    public function getHandlers(): array
    {
        return [
            'account_essential_data' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/accounts/{id}/essential_data',
                'action' => [$this, 'getAccountEssentialData'],
            ],
        ];
    }

    public function getAccountEssentialData(Request $request)
    {
        $essentialData = $this
            ->getStorage(self::STORAGE_ACCOUNT_ESSENTIAL)
            ->where('account_id', '=', $request->attributes->get('id'))
            ->first();

        if ($essentialData === null) {
            return new Response(
                HttpResponse::HTTP_NOT_FOUND,
                [],
                json_encode(['message' => 'Account not found.', 'status_code' => HttpResponse::HTTP_NOT_FOUND])
            );
        }

        return new Response(
            HttpResponse::HTTP_OK,
            [],
            json_encode($essentialData['essential_data'])
        );
    }
}