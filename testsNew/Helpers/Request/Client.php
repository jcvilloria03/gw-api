<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;

class Client extends \GuzzleHttp\Client
{
    public function send(RequestInterface $request, array $options = [])
    {
        $uri = new Uri((string) $this->getConfig('base_uri') . (string) $request->getUri());

        return parent::send($request->withUri($uri), $options);
    }
}
