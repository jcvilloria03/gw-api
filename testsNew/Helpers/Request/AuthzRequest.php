<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class AuthzRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
    }

    public function getStorage(): AbstractDataStorage
    {
        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'authz';
    }

    public function getHandlers(): array
    {
        return [
            'salarium_clearance' => [
                'method' => 'POST',
                'base_uri' => getenv('AUTHZ_API_URL'),
                'path' => '/api/v1/salarium_clearance',
                'action' => [$this, 'postSalariumClearance'],
            ],
        ];
    }

    public function postSalariumClearance(): Response
    {
        $data = $this->getStorage()->keyBy('module')->map(function ($module) {
            return ['data_scope' => $module['data_scope']];
        });

        return new Response(200, [], json_encode(['data' => $data]));
    }
}
