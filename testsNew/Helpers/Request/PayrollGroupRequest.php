<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class PayrollGroupRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
    }

    public function getStorage(): AbstractDataStorage
    {
        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'payroll_groups';
    }

    public function getHandlers(): array
    {
        return [
            'post_payroll_group_name_is_available' => [
                'method' => 'POST',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/company/{id}/payroll_group/is_name_available',
                'action' => [$this, 'postPayrollGroupNameIsAvailable'],
            ],
            'delete_bulk_payroll_group' => [
                'method' => 'DELETE',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/payroll_group/bulk_delete',
                'action' => [$this, 'deleteBulkPayrollGroup'],
            ],
            'post_philippines_payroll_group' => [
                'method' => 'POST',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/philippine/payroll_group/',
                'action' => [$this, 'postPhilippinesPayrollGroup'],
            ],
            'get_philippines_payroll_group' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/philippine/payroll_group/{id}',
                'action' => [$this, 'getPhilippinesPayrollGroup'],
            ],
        ];
    }

    public function postPayrollGroupNameIsAvailable(Request $request): Response
    {
        $totalWithSameName = $this->getStorage()
            ->where('company_id', '=', $request->attributes->get('id'))
            ->where('name', '=', $request->get('name'))
            ->count();

        return new Response(200, [], json_encode(['available' => $totalWithSameName === 0]));
    }

    public function deleteBulkPayrollGroup(Request $request): Response
    {
        $payrollGroupId = $request->get('payroll_group_ids', []);
        $keys = [];
        $this->storage->each(function ($item, $key) use (&$keys, $payrollGroupId) {
            if ($payrollGroupId === $item['id']) {
                $keys[] = $key;
            }
        });

        foreach ($keys as $key) {
            $this->storage->pull($key);
        }

        return new Response(204, []);
    }

    public function postPhilippinesPayrollGroup(Request $request): Response
    {
        $last = $this->getStorage()->sortBy('id')->last();
        $data = Collection::make($request->request->all())->except('authz_data_scope')->toArray();
        $data['id'] = $last !== null ? $last['id'] + 1 : 1;
        $this->getStorage()->push($data);

        return new Response(201, [], json_encode($data));
    }

    public function getPhilippinesPayrollGroup(Request $request): Response
    {
        $data = $this
            ->getStorage()
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        if ($data === null) {
            return new Response(404, [], json_encode(['message' => 'Not found', 'status_code' => 404]));
        }

        return new Response(200, [], json_encode($data));
    }
}
