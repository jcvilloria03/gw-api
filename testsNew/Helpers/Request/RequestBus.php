<?php


namespace TestsNew\Helpers\Request;


use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\Request as SRequest;
use TestsNew\Helpers\Request\Expectation\Expectation;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;
use TestsNew\Helpers\Request\Storage\WithMultipleStorageInterface;
use function GuzzleHttp\Promise\promise_for;
use function GuzzleHttp\Psr7\parse_query;

class RequestBus
{
    private $routes;
    private $requests = [];
    private $storage;
    private $expectations;

    /**
     * RequestBus constructor.
     * @param AbstractRequest[] $requests
     */
    public function __construct(array $requests)
    {
        $this->requests = $requests;
        $this->routes = new RouteCollection();
        $this->storage = [];
        $this->expectations = Collection::make([]);
        foreach ($requests as $request) {
            $this->appendHandlers($request->getHandlers());
            if ($request instanceof WithDataStorageInterface) {
                $this->storage[$request->getStorageName()] = $request->getStorage();
            }

            if ($request instanceof WithMultipleStorageInterface) {
                foreach ($request->getStorageNames() as $name) {
                    $this->storage[$name] = $request->getStorage($name);
                }
            }
        }
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function handle(Request $request, array $options): Response
    {
        $expectation = $this->getExpectationFromRequest($request);
        if ($expectation !== null) {
            $response = $expectation->call($request);
            if ($response !== null) {
                return $response;
            }
        }

        $requestContext = new RequestContext(
            '/',
            $request->getMethod(),
            $request->getUri()->getHost(),
            $request->getUri()->getScheme(),
            80,
            443,
            $request->getUri()->getPath(),
            $request->getUri()->getQuery()
        );

        $matcher = new UrlMatcher($this->routes, $requestContext);
        $parameters = $matcher->match($request->getUri()->getPath());

        $headers = $request->getHeaders();

        $iRequest = SRequest::create(
            (string) $request->getUri(),
            $request->getMethod(),
            parse_query($request->getUri()->getQuery()),
            [],
            [],
            [],
            $request->getBody()->getContents()
        );
        $iRequest->headers->add($headers);
        if ($iRequest->headers->get('content-type') === 'application/x-www-form-urlencoded') {
            $iRequest->request->add(parse_query($iRequest->getContent()));
        }

        $iRequest->attributes->add(collect($parameters)->except(['_controller', '_route'])->all());

        return call_user_func($parameters['_controller'], $iRequest);
    }

    public function appendHandler(string $name, array $handler): self
    {
        $baseUri = new Uri($handler['base_uri']);
        $router = new Route(
            $handler['path'],
            ['_controller' => $handler['action']],
            [],
            [],
            $baseUri->getHost(),
            [$baseUri->getScheme()],
            [$handler['method']]
        );
        $this->routes->add($name, $router);

        return $this;
    }

    public function appendHandlers(array $handlers): self
    {
        foreach ($handlers as $name => $handler) {
            $this->appendHandler($name, $handler);
        }

        return $this;
    }

    public function __invoke(RequestInterface $request, array $options)
    {
        return promise_for($this->handle($request, $options));
    }

    public function getDataStorage(string $name): AbstractDataStorage
    {
        return $this->storage[$name];
    }

    /**
     * @param Request $request
     * @return Expectation|null
     */
    public function getExpectationFromRequest(Request $request)
    {
        return $this->expectations->filter(function (Expectation $expectation) use ($request) {
            return $expectation->isSameRequest($request);
        })->first();
    }

    public function newExpectation(string $method, string $uri): Expectation
    {
        $expectation = new Expectation($method, $uri);
        $this->expectations->push($expectation);

        return $expectation;
    }

    public function assertExpectations(): array
    {
        $expectation = $this->expectations->filter(function (Expectation $expectation) {
            return !$expectation->hasMetExpectation();
        })->first();

        if ($expectation instanceof Expectation) {
            if ($expectation->expectingBody()) {
                return ['valid' => false, 'message' => sprintf(
                    'Expecting %s %s with body %s to be called %d time/s, but called %s time/s',
                    $expectation->getRequest()->getMethod(),
                    $expectation->getRequest()->getUri()->getPath(),
                    $expectation->getRequest()->getBody()->getContents(),
                    $expectation->getExpectedNumberOfCalls(),
                    $expectation->getActualNumberOfCalls()
                )];
            } else {
                return ['valid' => false, 'message' => sprintf(
                    'Expecting %s %s to be called %d time/s, but called %s time/s',
                    $expectation->getRequest()->getMethod(),
                    $expectation->getRequest()->getUri()->getPath(),
                    $expectation->getExpectedNumberOfCalls(),
                    $expectation->getActualNumberOfCalls()
                )];
            }
        }

        return ['valid' => true];
    }
}
