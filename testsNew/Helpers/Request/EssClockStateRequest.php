<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class EssClockStateRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('timesheet.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'timesheet';
    }

    public function getHandlers(): array
    {
        return [
            'get_timesheet' => [
                'method' => 'GET',
                'base_uri' => getenv('TIMECLOCK_URL'),
                'path' => '/timesheet/{id}',
                'action' => [$this, 'getTimesheet'],
            ],
        ];
    }

    public function getTimesheet(Request $request): Response
    {
        $undertime = collect($this->getDataFromAsset('timesheet.json'))
            ->where('employee_uid', '=', $request->attributes->get('id'))
            ->all();

        return $this->generateJsonResponse(200, $undertime);
    }
}
