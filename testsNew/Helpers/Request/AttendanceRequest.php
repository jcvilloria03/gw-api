<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class AttendanceRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
    }

    public function getStorage(): AbstractDataStorage
    {
        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'attendances';
    }

    public function getHandlers(): array
    {
        return [
            'get_dashboard_attendance' => [
                'method' => 'GET',
                'base_uri' => getenv('ATTENDANCE_API_URI'),
                'path' => '/attendance/company/{id}/dashboard/attendance',
                'action' => [$this, 'getDashboardAttendance'],
            ],
        ];
    }

    public function getDashboardAttendance(Request $request): Response
    {
        $activeEmployees = collect($this->getDataFromAsset('attendances.json'))->all();

        return $this->generateJsonResponse(200, $activeEmployees);
    }
}
