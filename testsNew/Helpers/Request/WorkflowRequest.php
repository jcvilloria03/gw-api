<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class WorkflowRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('user_workflows.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'user_workflows';
    }

    public function getHandlers(): array
    {
        return [
            'get_user_workflows' => [
                'method' => 'GET',
                'base_uri' => getenv('TA_API_URI'),
                'path' => '/user/{id}/workflows',
                'action' => [$this, 'getUserWorkflows'],
            ],
        ];
    }

    public function getUserWorkflows(Request $request): Response
    {
        $userWorkflows = collect($this->getDataFromAsset('user_workflows.json'))->all();

        return $this->generateJsonResponse(200, $userWorkflows);
    }
}
