<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class ShiftChangeRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('shift_changes.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'shift_changes';
    }

    public function getHandlers(): array
    {
        return [
            'get_shift_change_request' => [
                'method' => 'GET',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/shift_change_request/{id}',
                'action' => [$this, 'getShiftChangeRequest'],
            ],
        ];
    }

    public function getShiftChangeRequest(Request $request): Response
    {
        $shift_change = collect($this->getDataFromAsset('shift_changes.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $shift_change);
    }
}
