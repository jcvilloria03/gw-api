<?php

namespace TestsNew\Helpers\Request\Storage;

use Illuminate\Support\Collection;

abstract class AbstractDataStorage extends Collection
{
    public function validateData(array $data)
    {
    }

    public function push($value)
    {
        $this->validateData($value);

        return parent::push($value);
    }

    public function prepend($value, $key = null)
    {
        $this->validateData($value);

        return parent::push($value);
    }
}
