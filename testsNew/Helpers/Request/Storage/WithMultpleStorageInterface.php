<?php

namespace TestsNew\Helpers\Request\Storage;

interface WithMultpleStorageInterface
{
    public function getStorage(string $name): AbstractDataStorage;
    public function getStorages(): array;
    public static function getStorageNames(): array;
}
