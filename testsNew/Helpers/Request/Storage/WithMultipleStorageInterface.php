<?php

namespace TestsNew\Helpers\Request\Storage;

interface WithMultipleStorageInterface
{
    public function getStorage(string $name): AbstractDataStorage;
    public function getStorages(): array;
    public static function getStorageNames(): array;
}
