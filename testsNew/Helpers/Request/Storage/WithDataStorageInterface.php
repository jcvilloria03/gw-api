<?php

namespace TestsNew\Helpers\Request\Storage;

interface WithDataStorageInterface
{
    public function getStorage(): AbstractDataStorage;
    public static function getStorageName(): string;
}
