<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class CompanyRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;
    private $assetInitialized;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
        $this->assetInitialized = false;
    }

    private function init()
    {
        if (!$this->assetInitialized) {
            collect($this->getDataFromAsset('companies.json'))->each(function ($position) {
                $this->storage->push($position);
            });
            $this->assetInitialized = true;
        }
    }

    public function getStorage(): AbstractDataStorage
    {
        $this->init();

        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'companies';
    }

    public function getHandlers(): array
    {
        return [
            'get_account_id' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/company/{id}',
                'action' => [$this, 'getAccountId'],
            ],
            'get_philippine_company' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/philippine/company/{id}',
                'action' => [$this, 'getPhilippineCompany'],
            ],
            'get_max_clock_out_rule' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/max_clock_out/get_rule/{emp_id}',
                'action' => [$this, 'getMaxClockOutRule'],
            ],
        ];
    }

    public function getPhilippineCompany(Request $request): Response
    {
        $philippineCompany = collect($this->getDataFromAsset('philippine_companies.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $philippineCompany);
    }

    public function getAccountId(Request $request): Response
    {
        $company = collect($this->getDataFromAsset('companies.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $company);
    }

    public function getMaxClockOutRule(Request $request): Response
    {
        $maxClockOutRule = [
            "data" => [
                "name" => "rule 1",
                "hours" => "1.00",
                "type" => "employee"
            ]
        ];

        return $this->generateJsonResponse(200, $maxClockOutRule);
    }
}
