<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithMultipleStorageInterface;

class UserRequest extends AbstractRequest implements WithMultipleStorageInterface
{
    const STORAGE_USER_ESSENTIAL_DATA = 'user_essential_data';

    private $storages = [];

    public function __construct()
    {
        $this->storages = [
            self::STORAGE_USER_ESSENTIAL_DATA => new DefaultDataStorage([]),
        ];
    }

    public function getStorage(string $name): AbstractDataStorage
    {
        return $this->storages[$name];
    }

    public function getStorages(): array
    {
        return $this->storages;
    }

    public static function getStorageNames(): array
    {
        return [self::STORAGE_USER_ESSENTIAL_DATA];
    }

    public function getHandlers(): array
    {
        return [
            'get_user_essential_data' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/user/{userId}/essential_data',
                'action' => [$this, 'getUserEssentialData'],
            ],
            'get_user' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/user/{id}',
                'action' => [$this, 'getUser'],
            ],
        ];
    }

    public function getUser(Request $request): Response
    {
        $user = collect($this->getDataFromAsset('users.json'))
            ->where('id', '=', $request->attributes->get('id'))
            ->first();

        return $this->generateJsonResponse(200, $user);
    }

    public function getUserEssentialData(Request $request): Response
    {
        $userId = $request->attributes->get('userId');
        $data = $this
            ->getStorage(self::STORAGE_USER_ESSENTIAL_DATA)
            ->where('subject.user_id', '=', $userId)
            ->first();

        if ($data === null) {
            return new Response(404, [], json_encode(['message' => 'Not Found', 'status_code' => 404]));
        }

        return new Response(200, [], json_encode(['data' => $data]));
    }
}
