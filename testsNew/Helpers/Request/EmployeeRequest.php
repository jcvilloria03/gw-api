<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class EmployeeRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
    }

    public function getStorage(): AbstractDataStorage
    {
        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'employees';
    }

    public function getHandlers(): array
    {
        return [
            'get_company_ta_employees' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/company/{id}/ta_employees',
                'action' => [$this, 'getCompanyTaEmployees'],
            ],
            'get_active_employees_count' => [
                'method' => 'GET',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/company/{id}/active_employees_count',
                'action' => [$this, 'getActiveEmployeesCount'],
            ],
            'update_employee' => [
                'method' => 'PATCH',
                'base_uri' => getenv('COMPANY_API_URI'),
                'path' => '/philippine/employee/{id}',
                'action' => [$this, 'updateEmployee'],
            ],
        ];
    }

    public function getActiveEmployeesCount(Request $request): Response
    {
        $activeEmployees = collect($this->getDataFromAsset('active_employees_count.json'))->all();

        return $this->generateJsonResponse(200, $activeEmployees);
    }

    public function getCompanyTaEmployees(Request $request): Response
    {
        $employees = $this->getStorage()
            ->where('company_id', '=', $request->attributes->get('id'))
            ->all();

        return new Response(200, [], json_encode($employees));
    }

    public function updateEmployee(Request $request): Response
    {
        $employees = $this->getStorage()
            ->where('company_id', '=', $request->attributes->get('company_id'))
            ->first();

        return new Response(200, [], json_encode($employees));
    }
}
