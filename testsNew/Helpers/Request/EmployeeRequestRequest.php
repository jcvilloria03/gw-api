<?php

namespace TestsNew\Helpers\Request;

use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\Request;
use TestsNew\Helpers\Request\Storage\AbstractDataStorage;
use TestsNew\Helpers\Request\Storage\DefaultDataStorage;
use TestsNew\Helpers\Request\Storage\WithDataStorageInterface;

class EmployeeRequestRequest extends AbstractRequest implements WithDataStorageInterface
{
    private $storage;

    public function __construct()
    {
        $this->storage = new DefaultDataStorage([]);
    }

    public function getStorage(): AbstractDataStorage
    {
        return $this->storage;
    }

    public static function getStorageName(): string
    {
        return 'employee_request';
    }

    public function getHandlers(): array
    {
        return [
            'bulk_approve' => [
                'method' => 'POST',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/employee_request/bulk_approve',
                'action' => [$this, 'bulkApprove'],
            ],
            'bulk_decline' => [
                'method' => 'POST',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/employee_request/bulk_decline',
                'action' => [$this, 'bulkDecline'],
            ],
            'get_employee_requests' => [
                'method' => 'GET',
                'base_uri' => getenv('REQUESTS_API_URI'),
                'path' => '/employee_request',
                'action' => [$this, 'getEmployeeRequestsById'],
            ],
        ];
    }

    public function bulkDecline(Request $request): Response
    {
        return $this->generateJsonResponse(200, []);
    }

    public function bulkApprove(Request $request): Response
    {
        return $this->generateJsonResponse(200, []);
    }

    public function getEmployeeRequestsById(Request $request): Response
    {
        $employeeRequests = $this->getDataFromAsset('employee_requests.json');

        return $this->generateJsonResponse(200, $employeeRequests);
    }
}
