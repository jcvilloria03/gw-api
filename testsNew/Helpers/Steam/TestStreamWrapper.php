<?php

namespace TestsNew\Helpers\Steam;

class TestStreamWrapper
{
    public static $streams = [];
    var $position;
    var $varname;

    function stream_open($path, $mode, $options, &$opened_path)
    {
        $this->varname = $path;
        $this->position = 0;

        return true;
    }

    function stream_read($count)
    {
        $ret = substr(self::$streams[$this->varname], $this->position, $count);
        $this->position += strlen($ret);
        return $ret;
    }

    function stream_write($data)
    {
        $left = substr(self::$streams[$this->varname], 0, $this->position);
        $right = substr(self::$streams[$this->varname], $this->position + strlen($data));
        self::$streams[$this->varname] = $left . $data . $right;
        $this->position += strlen($data);
        return strlen($data);
    }

    function stream_tell()
    {
        return $this->position;
    }

    function stream_eof()
    {
        return $this->position >= strlen(self::$streams[$this->varname]);
    }

    function stream_seek($offset, $whence)
    {
        switch ($whence) {
            case SEEK_SET:
                if ($offset < strlen(self::$streams[$this->varname]) && $offset >= 0) {
                    $this->position = $offset;
                    return true;
                } else {
                    return false;
                }
                break;

            case SEEK_CUR:
                if ($offset >= 0) {
                    $this->position += $offset;
                    return true;
                } else {
                    return false;
                }
                break;

            case SEEK_END:
                if (strlen(self::$streams[$this->varname]) + $offset >= 0) {
                    $this->position = strlen(self::$streams[$this->varname]) + $offset;
                    return true;
                } else {
                    return false;
                }
                break;

            default:
                return false;
        }
    }

    function stream_metadata($path, $option, $var)
    {
        if($option == STREAM_META_TOUCH) {
            $url = parse_url($path);
            $varname = $url["host"];
            if(!isset(self::$streams[$varname])) {
                self::$streams[$varname] = '';
            }
            return true;
        }
        return false;
    }
}