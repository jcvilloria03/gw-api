<?php
namespace TestsNew\Api\Location;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Location\LocationRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetPhilippineCompanyLocationsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);
        
        $this->mockClass(LocationRequestService::class, [
            'getCompanyLocations' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'Main Office',
                            'account_id' => 1,
                            'company_id' => 1,
                            'headquarters' => true,
                            'address' => [
                                'address_line_1' => '1650',
                                'address_line_2' => 'Peñafrancia',
                                'city' => 'Makati',
                                'country' => 'Philippines',
                                'region' => 'NCR',
                                'zip_code' => '1208'
                            ]
                        ],
                        [
                            'id' => 2,
                            'name' => 'Old Office',
                            'account_id' => 1,
                            'company_id' => 1,
                            'headquarters' => false,
                            'address' => [
                                'address_line_1' => 'Jupiter',
                                'address_line_2' => 'Bel-Air',
                                'city' => 'Makati',
                                'country' => 'Philippines',
                                'region' => 'NCR',
                                'zip_code' => '1208'
                            ]
                        ]
                    ]
                ]
            ]),
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/philippine/company/1/locations',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllCompanyScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/philippine/company/1/locations',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->json(
            'GET',
            '/philippine/company/999/locations',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
