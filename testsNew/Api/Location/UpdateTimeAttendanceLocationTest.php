<?php

namespace TestsNew\Api\Location;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Location\TimeAttendanceLocationRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateTimeAttendanceLocationTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(TimeAttendanceLocationRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Makati City',
                    'account_id' => 1,
                    'company_id' => 1,
                    'is_headquarters' => true,
                    'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'location_pin' => '14.569222,121.02257199999997',
                    'first_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'second_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'city' => 'Makati City',
                    'country' => 'Philippines',
                    'region' => 'NCR',
                    'zip_code' => '1240',
                    'ip_addresses' => [],
                    'timezone' => 'gmt+8'
                ]
            ],
            [
                'body' => [
                    'company_id' => 2,
                    'name' => 'New Company Name',
                    'timezone' => 'gmt+8',
                    'is_headquarters' => true,
                    'city' => 'Makati City',
                    'country' => 'Philippines',
                    'region' => 'NCR',
                    'zip_code' => '1240',
                    'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'location_pin' => '14.569222,121.02257199999997',
                    'first_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'second_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'New Company Name',
                    'account_id' => 1,
                    'company_id' => 2,
                    'is_headquarters' => true,
                    'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'location_pin' => '14.569222,121.02257199999997',
                    'first_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'second_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'city' => 'Makati City',
                    'country' => 'Philippines',
                    'region' => 'NCR',
                    'zip_code' => '1240',
                    'ip_addresses' => [],
                    'timezone' => 'gmt+8'
                ]
            ]
        ]);
        $this->mockRequestService(CompanyRequestService::class, [
            'getAccountId' => [
                'body' => [
                    'account_id' => 9
                ]
            ]
        ]);

        $mockAuditService = Mockery::mock(AuditService::class, [
            'log' => true
        ]);

        $this->app->instance(AuditService::class, $mockAuditService);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    protected function mockCompanyFormData(): array
    {
        return [
            'company_id' => 1,
            'name' => 'New Company Name'
        ];
    }

    public function testCreateSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.locations' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->put(
            '/time_attendance_locations/1',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testCreateSuccessAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.locations' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->put(
            '/time_attendance_locations/1',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateIncorrectCompanyOldLocationData()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [2, 3]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        
        $this->put(
            '/time_attendance_locations/1',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateIncorrectCompanyNewLocationData()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $payload = $this->mockCompanyFormData();
        $payload['company_id'] = 123;
        
        $this->put(
            '/time_attendance_locations/1',
            $payload,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateIncorrectAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $payload = $this->mockCompanyFormData();
        $payload['company_id'] = 123;
        
        $this->put(
            '/time_attendance_locations/1',
            $payload,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
