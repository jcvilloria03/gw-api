<?php
namespace TestsNew\Api\Location;

use App\Authz\AuthzRequestService;
use App\Location\LocationRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetLocationNameAvailabilityTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);
        
        $this->mockClass(LocationRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'name' => 'Makati Branch',
                    'account_id' => 1,
                    'company_id' => 1,
                    'headquarters' => true,
                    'address' => [
                        'address_line_1' => 'Address line 1',
                        'address_line_2' => 'Address line 2',
                        'city' => 'Makati',
                        'country' => 'Philippines',
                        'region' => 'NCR',
                        'zip_code' => '1208'
                    ]
                ]
            ]),
            
            'isNameAvailable' => $this->getJsonResponse([
                'body' => ['available' => true]
            ]),
        ]);
    }

    public function testResponseAvailableSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.locations' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'company/1/location/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        );

        $this->assertResponseStatus(200);
        $this->seeJson(['available' => true]);
    }

    public function testDeleteSuccessAllCompany()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.locations' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'company/1/location/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        );

        $this->assertResponseStatus(200);
        $this->seeJson(['available' => true]);
    }

    public function testDeleteIncorrectCompany()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.locations' => [
                    'data_scope' => [
                        'COMPANY' => [50]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'company/1/location/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance.leaves'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
