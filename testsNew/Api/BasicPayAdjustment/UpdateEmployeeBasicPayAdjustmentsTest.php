<?php

namespace TestsNew\Api\BasicPayAdjustment;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateEmployeeBasicPayAdjustmentsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/employee/1/basic_pay_adjustment/1/update';
    const TARGET_METHOD = 'PATCH';
    const MODULE = 'employees.people.basic_pay_adjustments';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => 1,
            'department_id' => 1,
            'location_id' => 1,
            'position_id' => 1,
            'team_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->shouldExpectRequest('GET', '/basic_pay_adjustment/1')
            ->once()
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, [], json_encode([
                    'id' => 1,
                    'amount' => '23,000.00',
                    'unit' => 'per_month',
                    'basic_pay' => '24,000.00 per month',
                    'reason' => 'anniversary increase',
                    'employee_id' => 1,
                    'company_id' => 2,
                    'effective_date' => '2011-10-16',
                    'adjustment_date' => null
                ])),
                new Response(HttpResponse::HTTP_OK, [], json_encode([
                    'id' => 1,
                    'amount' => '24,000.00',
                    'unit' => 'per_month',
                    'basic_pay' => '24,000.00 per month',
                    'reason' => 'anniversary increase',
                    'employee_id' => 1,
                    'company_id' => 2,
                    'effective_date' => '2011-10-16',
                    'adjustment_date' => null
                ]))
            );

        $this
            ->shouldExpectRequest('PATCH', '/employee/1/basic_pay_adjustment/1/update')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'amount' => '24,000.00',
                'unit' => 'per_month',
                'basic_pay' => '24,000.00 per month',
                'reason' => 'anniversary increase',
                'employee_id' => 1,
                'company_id' => 2,
                'effective_date' => '2011-10-16',
                'adjustment_date' => null
            ])))
        ;

        $this->json('PATCH', self::TARGET_URL, [
            'amount' => '24,000.00',
            'unit' => 'per_month',
            'reason' => 'anniversary increase',
            'effective_date' => '2011-10-16',
        ],[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => 999,
            'department_id' => 999,
            'location_id' => 999,
            'position_id' => 999,
            'team_id' => 999
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
        ->shouldExpectRequest('GET', '/basic_pay_adjustment/1')
        ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'amount' => '23,000.00',
                'unit' => 'per_month',
                'basic_pay' => '24,000.00 per month',
                'reason' => 'anniversary increase',
                'employee_id' => 1,
                'company_id' => 2,
                'effective_date' => '2011-10-16',
                'adjustment_date' => null
            ])))
        ;

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode([
                    'id' => 1
                ]))
            );

        $this->json('PATCH', self::TARGET_URL, [
            'amount' => '24,000.00',
            'unit' => 'per_month',
            'reason' => 'anniversary increase',
            'effective_date' => '2011-10-16',
        ],[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
