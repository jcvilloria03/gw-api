<?php

namespace TestsNew\Api\Holiday;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\BasicPayAdjustment\BasicPayAdjustmentRequestService;
use App\Employee\EmployeeRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetBasicPayAdjustmentTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(BasicPayAdjustmentRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ])
        ]);
    }

    public function testGetEmployeeAnnualEarningSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.annual_earnings' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/basic_pay_adjustment/1',
            [],
            [
                'x-authz-entities' => 'employees.people.annual_earnings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetEmployeeAnnualEarningUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.annual_earnings' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/basic_pay_adjustment/1',
            [],
            [
                'x-authz-entities' => 'employees.people.annual_earnings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
