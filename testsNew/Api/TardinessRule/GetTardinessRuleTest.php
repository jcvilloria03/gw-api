<?php

namespace TestsNew\Api\TardinessRule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetTardinessRuleTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    public function testGetSuccess()
    {
        $this->mockRequestService(TardinessRuleRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'TardinessRule1',
                    'company_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.schedule_settings.tardiness_rules' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/tardiness_rule/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetSuccessCompanyAll()
    {
        $this->mockRequestService(TardinessRuleRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'TardinessRule1',
                    'company_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.schedule_settings.tardiness_rules' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/tardiness_rule/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetIncorrectCompany()
    {
        $this->mockRequestService(TardinessRuleRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'TardinessRule1',
                    'company_id' => 50
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.schedule_settings.tardiness_rules' => [
                            'data_scope' => [
                                'COMPANY' => [2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/tardiness_rule/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
