<?php

namespace TestsNew\Api\TardinessRule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetCompanyTardinessRulesTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(TardinessRuleRequestService::class, [
            'getCompanyTardinessRules' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'company_id' => 1,
                            'name' => 'Test'
                        ]
                    ]
                ]
            ])
        ]);
    }

    public function testGetCompanyTardinessRulesSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/tardiness_rules',
            [],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyTardinessRulesUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/999/tardiness_rules',
            [],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
