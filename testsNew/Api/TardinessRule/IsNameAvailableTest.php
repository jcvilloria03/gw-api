<?php

namespace TestsNew\Api\TardinessRule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class IsNameAvailableTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(TardinessRuleRequestService::class, [
            'isNameAvailable' => $this->getJsonResponse([
                'body' => [
                    'available' => true
                ]
            ])
        ]);
    }

    public function testIsNameAvailableSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/1/tardiness_rule/is_name_available', [
                'name' => 'Test'
            ],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testIsNameAvailableUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/999/tardiness_rule/is_name_available', [
                'name' => 'Test'
            ],
            [
                'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
