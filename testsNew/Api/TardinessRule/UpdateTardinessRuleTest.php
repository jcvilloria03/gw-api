<?php

namespace TestsNew\Api\TardinessRule;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateTardinessRuleTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(TardinessRuleRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Test'
                ]
            ]),

            'update' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'UpdatedTest'
                ]
            ])
        ]);
    }

    public function testUpdateTardinessRuleSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/tardiness_rule/1', [
            'company_id' => 1,
            'name' => 'Test'
        ],
        [
            'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateTardinessRuleUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.tardiness_rules' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/tardiness_rule/1', [
            'company_id' => 1,
            'name' => 'Test'
        ],
        [
            'X-Authz-Entities' => 'company_settings.schedule_settings.tardiness_rules'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
