<?php

namespace TestsNew\Api\EssentialData;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyEssentialDataTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'company_settings.users_and_roles.company_roles';
    const METHOD = 'GET';
    const URL = '/accounts/1/companies/1/essential_data';

    private function addUserAndAccountEssentialData()
    {
        $this->markTestSkipped('Deprecated due to ticket 7702: Removal of users and roles option');
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'department' => [
                [
                    'id' => 1,
                    'name' => 'Department Name'
                ]
            ],
            'position' => [
                [
                    'id' => 1,
                    'name' => 'Position Name'
                ]
            ],
            'team' => [
                [
                    'id' => 1,
                    'name' => 'Team Name'
                ]
            ],
            'location' => [
                [
                    'id' => 1,
                    'name' => 'Location Name'
                ]
            ],
            'payroll_group' => [
                [
                    'id' => 1,
                    'name' => 'Payroll Group Name'
                ]
            ]
        ];
        $this
            ->shouldExpectRequest('GET', '/accounts/1/companies/1/essential_data?show_names=true')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $dataParam = [];

        $headers = [
            self::HEADER => self::MODULE,
        ];

        $file = [];

        $server = $this->transformHeadersToServerVars($headers);
        $this->call(self::METHOD, self::URL, $dataParam, [], $file, $server);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }


    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'department' => [
                [
                    'id' => 1,
                    'name' => 'Department Name'
                ]
            ],
            'position' => [
                [
                    'id' => 1,
                    'name' => 'Position Name'
                ]
            ],
            'team' => [
                [
                    'id' => 1,
                    'name' => 'Team Name'
                ]
            ],
            'location' => [
                [
                    'id' => 1,
                    'name' => 'Location Name'
                ]
            ],
            'payroll_group' => [
                [
                    'id' => 1,
                    'name' => 'Payroll Group Name'
                ]
            ]
        ];
        $this
            ->shouldExpectRequest('GET', '/accounts/1/companies/1/essential_data?show_names=true')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $dataParam = [];

        $headers = [
            self::HEADER => self::MODULE,
        ];

        $file = [];

        $server = $this->transformHeadersToServerVars($headers);
        $this->call(self::METHOD, self::URL, $dataParam, [], $file, $server);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
