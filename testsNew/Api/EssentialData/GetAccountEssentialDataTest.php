<?php

namespace TestsNew\Api\EssentialData;

use App\Account\AccountRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetAccountEssentialDataTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);
    }

    public function testResponseSuccess()
    {
        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    '1' => [
                        'department' => [
                            [
                                'id' => 1,
                                'name' => 'Department Name'
                            ]
                        ],
                        'position' => [
                            [
                                'id' => 1,
                                'name' => 'Position Name'
                            ]
                        ],
                        'team' => [
                            [
                                'id' => 1,
                                'name' => 'Team Name'
                            ]
                        ],
                        'location' => [
                            [
                                'id' => 1,
                                'name' => 'Location Name'
                            ]
                        ],
                        'payroll_group' => [
                            [
                                'id' => 1,
                                'name' => 'Payroll Group Name'
                            ]
                        ]
                    ],
                    '2' => [
                        'department' => [
                            [
                                'id' => 2,
                                'name' => 'Another Department Name'
                            ]
                        ],
                        'position' => [
                            [
                                'id' => 2,
                                'name' => 'Another Position Name'
                            ]
                        ],
                        'team' => [
                            [
                                'id' => 2,
                                'name' => 'Another Team Name'
                            ]
                        ],
                        'location' => [
                            [
                                'id' => 2,
                                'name' => 'Another Location Name'
                            ]
                        ],
                        'payroll_group' => [
                            [
                                'id' => 2,
                                'name' => 'Another Payroll Group Name'
                            ]
                        ]
                    ]
                ]
            ])
        ]);

        $this->json(
            'GET',
            '/accounts/1/essential_data',
            [],
            [
                'x-authz-entities' => 'control_panel.roles'
            ]
        );

        $this->seeJsonEquals([
            'department' => [
                [
                    'id' => 1,
                    'name' => 'Department Name'
                ],
                [
                    'id' => 2,
                    'name' => 'Another Department Name'
                ]
            ],
            'position' => [
                [
                    'id' => 1,
                    'name' => 'Position Name'
                ],
                [
                    'id' => 2,
                    'name' => 'Another Position Name'
                ]
            ],
            'team' => [
                [
                    'id' => 1,
                    'name' => 'Team Name'
                ],
                [
                    'id' => 2,
                    'name' => 'Another Team Name'
                ]
            ],
            'location' => [
                [
                    'id' => 1,
                    'name' => 'Location Name'
                ],
                [
                    'id' => 2,
                    'name' => 'Another Location Name'
                ]
            ],
            'payroll_group' => [
                [
                    'id' => 1,
                    'name' => 'Payroll Group Name'
                ],
                [
                    'id' => 2,
                    'name' => 'Another Payroll Group Name'
                ]
            ]
        ]);
    }
}
