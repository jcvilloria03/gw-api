<?php

namespace TestsNew\Api\Allowance;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateCompanyAllowanceTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.people.allowances';
    const TARGET_URL = '/philippine/company/1/allowance/bulk_create';
    const TARGET_METHOD = 'POST';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1,
                'department_id' => 1,
                'location_id' => 1,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];

        $requestPayload = [
            [
                'type_id' => 1,
                'recipients' => [
                    'employees' => [
                        1
                    ]
                ],
                'amount' => '500.00',
                'recurring' => true,
                'credit_frequency' => 'PER_MONTH',
                'prorated' => false,
                'valid_from' => '2020-07-01',
                'valid_to' => '2020-09-01',
                'release_date' => '',
                'never_expires' => false,
                'release_details' => [
                    [
                        'disburse_through_special_pay_run' => false
                    ]
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('POST', '/company/1/employees/id')
            ->withBody('values=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->shouldExpectRequest('POST', '/philippine/company/1/allowance/bulk_create')
            ->withBody(http_build_query($requestPayload))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([1])))
        ;

        $this->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload,[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 99,
                'department_id' => 1,
                'location_id' => 99,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];


        $this
            ->shouldExpectRequest('POST', '/company/1/employees/id')
            ->withBody('values=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
                
        $requestPayload = [
            [
                'type_id' => 1,
                'recipients' => [
                    'employees' => [
                        1
                    ]
                ],
                'amount' => '500.00',
                'recurring' => true,
                'credit_frequency' => 'PER_MONTH',
                'prorated' => false,
                'valid_from' => '2020-07-01',
                'valid_to' => '2020-09-01',
                'release_date' => '',
                'never_expires' => false,
                'release_details' => [
                    [
                        'disburse_through_special_pay_run' => false
                    ]
                ]
            ]
        ];

        $this->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload,[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
