<?php

namespace TestsNew\Api\Allowance;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdatePhilippineAllowanceTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.people.allowances';
    const TARGET_METHOD = 'PATCH';
    const TARGET_URL = '/philippine/allowance/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hExists');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('hGet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $payrollGroupData = [
            'id' => 1,
            'company_id' => 1
        ];

        $requestPayload = [
            [
                'type_id' => 1,
                'recipients' => [
                    'employees' => [
                        1
                    ]
                ],
                'amount' => 100,
                'recurring' => true,
                'credit_frequency' => 'DAILY',
                'prorated' => '0',
                'valid_from' => '2020-01-01',
                'valid_to' => '2020-10-10',
                'release_details' => [
                    [
                        'disburse_through_special_pay_run' => false
                    ]
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/other_income/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollGroupData)));

        $this
            ->shouldExpectRequest('PATCH', '/philippine/allowance/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([1])));

        $this->json('PATCH', self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }


    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [66],
                'PAYROLL_GROUP' => [99],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $payrollGroupData = [
            'id' => 1,
            'company_id' => 1
        ];

        $requestPayload = [
            [
                'type_id' => 1,
                'recipients' => [
                    'employees' => [
                        1
                    ]
                ],
                'amount' => 100,
                'recurring' => true,
                'credit_frequency' => 'DAILY',
                'prorated' => '0',
                'valid_from' => '2020-01-01',
                'valid_to' => '2020-10-10',
                'release_details' => [
                    [
                        'disburse_through_special_pay_run' => false
                    ]
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/other_income/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollGroupData)));

        $this->json('PATCH', self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE]);
        
        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
