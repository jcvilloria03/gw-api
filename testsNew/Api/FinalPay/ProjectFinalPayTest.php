<?php

namespace TestsNew\Api\FinalPay;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class ProjectFinalPayTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testProjectFinalPayShouldResponseSuccess()
    {
        $module = 'employees.people.termination_information';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'TEAM' => [1]
            ],
        ]);

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'location_id' => 1
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $expected = [
            'links' => ['self' => '/jobs/bb88a596-56d4-491d-97b9-5cf216098d67'],
            'data' => [
                'types' => 'jobs',
                'id' => 'bb88a596-56d4-491d-97b9-5cf216098d67',
                'payload' => [
                    'name' => 'projected-final-payroll',
                    'employeeId' => 1,
                    'companyId' => 1,
                ],
            ],
        ];
        $this
            ->shouldExpectRequest('POST', '/final_pay/project')
            ->once()
            ->withBody(json_encode(['company_id' => 1, 'employee_id' => 1]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;
        $input = ['projectedCalculation' => true, 'employeeId' => 1, 'companyId' => 1];

        $this
            ->json('POST', '/payroll/0/calculate', $input, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testProjectFinalPayShouldResponseUnauthorized()
    {
        $module = 'employees.people.termination_information';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'TEAM' => [1]
            ],
        ]);
        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => ['id' => 99],
            'department_id' => 1,
            'team_id' => 1,
            'location_id' => 1
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $input = ['projectedCalculation' => true, 'employeeId' => 1, 'companyId' => 1];

        $this
            ->json('POST', '/payroll/0/calculate', $input, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testProjectFinalPayShouldResponseInvalid()
    {
        $module = 'employees.people.termination_information';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'TEAM' => [1]
            ],
        ]);

        $input = ['projectedCalculation' => false, 'employeeId' => 1, 'companyId' => 1];

        $this
            ->json('POST', '/payroll/0/calculate', $input, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson([
                'message' => 'Projected Calculation must be true if payroll id is 0.',
                'status_code' => HttpResponse::HTTP_NOT_ACCEPTABLE,
            ])
        ;
    }
}