<?php

namespace TestsNew\Api\PhilippineCompany;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreatePhilippineCompanyTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'control_panel.companies';
    const METHOD = 'POST';
    const URL = '/philippine/company';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('del');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/subscriptions/customers?filter[account_id]=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'user_id' => 1,
                        'account_id' => 13,
                        'subscriptions' => [
                            [
                                'id' => 1,
                                'user_id' => 1,
                                'plan_id' => 1,
                                'main_product_id' => 1,
                                'plan' => [
                                    'id' => 1,
                                    'name' => 'Time & Attendance',
                                    'main_product' => [
                                        'code' => 'time and attendance'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/accounts/1/roles/system-defined')
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode([])));

        $this
            ->shouldExpectRequest('POST', '/philippine/company/')
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode([
                'id' => 1,
                'name' => 'test company',
                'account_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/account/users')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'email' => 'user@test.com',
                        'account_id' => 1
                    ]
                ]
            ])));

        $this
            ->shouldExpectRequest('GET', '/philippine/company/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'name' => 'test company',
                'account_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/default_holidays')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, []));

        $this
            ->json(self::METHOD, self::URL, [
                'name' => 'test company',
                'email' => 'test@test.com',
                'website' => 'www.test.com',
                'mobile_number' => 9299291929,
                'telephone_number' => 92929292
            ], [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson(['id' => 1])
        ;
    }


    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this
            ->shouldExpectRequest('GET', '/subscriptions/customers?filter[account_id]=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'user_id' => 1,
                        'account_id' => 13,
                        'subscriptions' => [
                            [
                                'id' => 1,
                                'user_id' => 1,
                                'plan_id' => 1,
                                'main_product_id' => 1,
                                'plan' => [
                                    'id' => 1,
                                    'name' => 'Time & Attendance',
                                    'main_product' => [
                                        'code' => 'time and attendance'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/philippine/company/')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'name' => 'test company',
                'account_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/account/users')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'email' => 'user@test.com',
                        'account_id' => 1
                    ]
                ]
            ])));

        $this
            ->shouldExpectRequest('GET', '/philippine/company/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'name' => 'test company',
                'account_id' => 1
            ])));

        $this->json(self::METHOD, self::URL, [
            'name' => 'test company',
            'email' => 'test@test.com',
            'website' => 'www.test.com',
            'mobile_number' => 9299291929,
            'telephone_number' => 92929292
        ], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
