<?php

namespace TestsNew\Api\Bonus;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateCompanyBonusTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.people.bonuses';
    const TARGET_URL = '/philippine/company/1/bonus/bulk_create';
    const TARGET_METHOD = 'POST';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
            ],
        ]);

        $employee = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1,
                'department_id' => 1,
                'location_id' => 1,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];

        $requestPayload = [
            [
                'basis' => 'GROSS_SALARY',
                'percentage' => '100.00',
                'recipients' => ['employees' => [], 'departments' => [], 'payroll_groups' => [1]],
                'release_details' => [
                    [
                        'date' => '2020-07-01',
                        'disburse_through_special_pay_run' => false,
                        'prorate_based_on_tenure' => true,
                        'coverage_from' => '2019-07-01',
                        'coverage_to' => '2020-07-01',
                        'status' => 0
                    ]
                ],
                'auto_assign' => true,
                'type_id' => 1,
                'previousRouteName' => 'Bonuses'
            ]
        ];
        $affectedEmployees = [
            [
                "user_type" => "employee",
                "companies" => [
                    [
                        "id" => 1,
                        "employee_id" => 1,
                    ]
                ]
            ]
        ];

        $this->shouldExpectRequest('POST', '/company/1/affected_users')
            ->once()
            ->withBody(json_encode([
                'affected_entities' => [
                    ['type' => 'payroll_group', 'id' => 1]
                ]
            ]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $affectedEmployees])));

        $this
            ->shouldExpectRequest('POST', '/company/1/employees/id')
            ->once()
            ->withBody('values=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)));

        $this
            ->shouldExpectRequest('POST', '/philippine/company/1/bonus/bulk_create')
            ->once()
            ->withBody(http_build_query($requestPayload))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([1])));

        $this->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
            ],
        ]);

        $employee = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 99,
                'department_id' => 1,
                'location_id' => 99,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];

        $affectedEmployees = [
            [
                "user_type" => "employee",
                "companies" => [
                    [
                        "id" => 1,
                        "employee_id" => 1,
                    ]
                ]
            ]
        ];

        $this->shouldExpectRequest('POST', '/company/1/affected_users')
            ->once()
            ->withBody(json_encode([
                'affected_entities' => [
                    ['type' => 'employee', 'id' => 1],
                    ['type' => 'department', 'id' => 1],
                    ['type' => 'payroll_group', 'id' => 1],
                ]
            ]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $affectedEmployees])));

        $this
            ->shouldExpectRequest('POST', '/company/1/employees/id')
            ->once()
            ->withBody('values=1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)));

        $requestPayload = [
            [
                'basis' => 'GROSS_SALARY',
                'percentage' => '100.00',
                'recipients' => [
                    'employees' => [
                        1
                    ],
                    'departments' => [
                        1
                    ],
                    'payroll_groups' => [
                        1
                    ],
                ],
                'release_details' => [
                    [
                        'date' => '2020-07-01',
                        'disburse_through_special_pay_run' => false,
                        'prorate_based_on_tenure' => true,
                        'coverage_from' => '2019-07-01',
                        'coverage_to' => '2020-07-01',
                        'status' => 0
                    ]
                ],
                'auto_assign' => true,
                'type_id' => 1,
                'previousRouteName' => 'Bonuses'
            ]
        ];

        $this->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
        $this->seeJsonContains(['message' => 'Unauthorized: Unable to process employees:[1]']);
    }
}
