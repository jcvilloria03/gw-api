<?php

namespace TestsNew\Api\Account;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class CreateAccountTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use RequestServiceTrait;

    const METHOD = 'POST';
    const URL = '/account/sign_up';

    private function addUserAndAccountEssentialData()
    {

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this
            ->shouldExpectRequest('POST', '/account/users')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                [1]
            ])));

        $this
            ->shouldExpectRequest('POST', '/subscriptions/customer')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                [1]
            ])));

        $this
            ->shouldExpectRequest('POST', '/accounts/1/roles/system-defined')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'created_roles' => [],
                'already_existing_roles' => [],
                'assigned_user_ids' => []
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/default_holidays')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([])));

        $this
            ->shouldExpectRequest('POST', '/account/sign_up')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'user_id' => 1,
                'id' => 1,
                'company_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/auth/user')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    'user' => [
                        'auth0_user_id' => 1,
                        'id_token' => '123123',
                        'access_token' => '12312321'
                    ]
                ]
            ])));


        $urlResponse = [
            'data' => [
                'user' => [
                    'id_token' => 'oiuytresdfhujhdert'
                ]
            ]
        ];
        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $dataParam = [
            'name' => 'test account',
            'first_name' => 'test first name',
            'last_name' => 'test last name',
            'company' => 'test company',
            'email' => 'test@gmail.com',
            'password' => '123qwe123',
            'plan_id' => 1
        ];

        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $file = [];

        $server = $this->transformHeadersToServerVars($headers);
        $this->call(self::METHOD, self::URL, $dataParam, ['x-authz-entities' => 'root.admin'], $file, $server);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }
}
