<?php

namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetAccountSetupProgressTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';

    const MODULE = 'root.admin';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'setupProgress' => $this->getJsonResponse([
                'body' => [
                    "id" => 1,
                    "name" => "Account Name",
                    "email" => "test@salarium.com",
                    "subscriptions"  => []
                ]
            ])
        ]);

    }

    public function testGetAccountSetupProgressSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'root.admin' => [
                    'data_scope' => [
                        'ACCOUNT' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('GET', '/account/setup_progress', [], [self::HEADER => self::MODULE]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetAccountSetupProgressUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => []
        ]);

        $this->json('GET', '/account/setup_progress', [], [self::HEADER => self::MODULE]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
