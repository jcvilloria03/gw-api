<?php
namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Auth0\Auth0UserService;
use App\Authz\AuthzRequestService;
use App\Model\Auth0User;
use App\Role\UserRoleService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetAccountsUsersTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->deleteAccountEssentialDataCache();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ],
                    2 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.users' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockClass(UserRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'account_id' => 1,
                    'companies' => [
                        ['id' => 1],
                        ['id' => 2]
                    ]
                ]
            ]),
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ],
            'getAccountOrCompanyUsers' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'active' => Auth0User::STATUS_ACTIVE,
                            'product_seats' => 1
                        ]
                    ]
                ]
            ])
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getAuth0UserId' => 'auth:0'
        ]);

        $this->mockClass(SubscriptionsRequestService::class, [
            'searchReceipts' => $this->getJsonResponse([
                'body' => null
            ])
        ]);

        $this->mockClass(UserRoleService::class, [
            'getCompanies' => [
                [
                    [
                        'company_id' => 1,
                        'company_name' => 'Company 1'
                    ],
                    [
                        'company_id' => 2,
                        'company_name' => 'Company 2'
                    ]
                ]
            ],
            'getUserRoles' => [
                [
                    'id' => 1,
                    'name' => 'Admin',
                    'role' => UserRoleService::TYPE_ADMIN
                ]
            ]
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/account/users',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.users' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/account/users',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => null
        ]);

        $this->json(
            'GET',
            '/account/users',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function deleteAccountEssentialDataCache()
    {
        $redisKey = Config::get('account_essential.redis_cache_key');

        Redis::del(sprintf($redisKey, 1));
    }

    public function tearDown()
    {
        $this->deleteAccountEssentialDataCache();
        parent::tearDown();
    }
}
