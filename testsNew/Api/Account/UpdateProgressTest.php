<?php

namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateProgressTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';

    const MODULE = 'company_settings.company_structure';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);
    }

    public function testUpdateAccountProgressSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->patch(
            '/account/progress',
            ['type' => 'company'],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateAccountProgressUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => []
        ]);

        $this->patch(
            '/account/progress',
            ['type' => 'company'],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
