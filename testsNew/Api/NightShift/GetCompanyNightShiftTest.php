<?php

namespace TestsNew\Api\NightShift;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\NightShift\NightShiftRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetCompanyNightShiftTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(NightShiftRequestService::class, [
            'getCompanyNightShift' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'activated' => true,
                    'start' => '22:00',
                    'end' => '06:00',
                    'time_shift' => 'Carry over'
                ]
            ])
        ]);
    }

    public function testGetCompanyNightShiftSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.night_shift' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/night_shift',
            [],
            [
                'x-authz-entities' => 'company_settings.schedule_settings.night_shift'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyNightShiftUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.night_shift' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/999/night_shift',
            [],
            [
                'x-authz-entities' => 'company_settings.schedule_settings.night_shift'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
