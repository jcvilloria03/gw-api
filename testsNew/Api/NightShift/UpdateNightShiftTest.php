<?php

namespace TestsNew\Api\NightShift;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\NightShift\NightShiftRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateNightShiftTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(NightShiftRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'activated' => true,
                    'start' => '22:00',
                    'end' => '06:00',
                    'time_shift' => 'Carry over'
                ]
            ]),

            'update' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'activated' => true,
                    'start' => '22:00',
                    'end' => '06:00',
                    'time_shift' => 'Carry over'
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);
    }

    public function testUpdateNightShiftSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.night_shift' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/night_shift/1', [
            'company_id' => 1
        ],
        [
            'x-authz-entities' => 'company_settings.schedule_settings.night_shift'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateNightShiftUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.schedule_settings.night_shift' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/night_shift/1', [
            'company_id' => 1
        ],
        [
            'x-authz-entities' => 'company_settings.schedule_settings.night_shift'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
