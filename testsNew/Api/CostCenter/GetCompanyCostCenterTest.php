<?php

namespace TestsNew\Api\CostCenter;
use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\CostCenter\CostCenterRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetCompanyCostCenterTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CostCenterRequestService::class, [
            'getCompanyCostCenters' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        'id' => 1,
                        'name' => 'CostCenter1',
                        'company_id' => 1,
                        'account_id' => 1,
                        'description' => 'CostCenter1Description'
                    ]
                ]
            ])
        ]);
    }

    public function testGetCompanyCostCentersSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'employees.people' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/cost_centers',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyCostCentersIncorrectCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'employees.people' => [
                            'data_scope' => [
                                'COMPANY' => [999]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/cost_centers',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
