<?php

namespace TestsNew\Api\CostCenter;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\CostCenter\CostCenterRequestService;
use App\Account\AccountRequestService;

class BulkDeleteCostCenterTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockRequestService(CostCenterRequestService::class, [
            [
                'body' => [
                    'data' => []
                ]
            ]
        ]);
    }

    public function testCostCenterBulkDeleteShouldResponseOk()
    {

        $payload = [
            'company_id' => 1,
            'cost_center_ids'  =>  [1, 2, 3]
            
        ];

        $this->json(
            'DELETE',
            '/cost_center/bulk_delete',
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testCostCenterBulkDeleteShouldResponseUnauthorized()
    {
        $payload = [
            'company_id' => 999,
            'cost_center_ids'  =>  [1, 2, 3]
        ];

        $this->json(
            'DELETE',
            '/cost_center/bulk_delete',
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
