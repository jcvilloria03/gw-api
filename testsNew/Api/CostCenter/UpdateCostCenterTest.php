<?php

namespace TestsNew\Api\CostCenter;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\CostCenter\CostCenterRequestService;
use App\Account\AccountRequestService;

class UpdateCostCenterTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        // Controller Mocks
        $this->mockRequestService(CostCenterRequestService::class, [
            [ // get
                'body' => [
                    'id' => 1,
                    'name' => 'cost center 1',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'cost center desc'
                ]
            ],
            [ // update
                'body' => [
                    'id' => 1,
                    'name' => 'cost center 1',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'cost center desc'
                ]
            ],
        ]);
    }

    public function testUpdateCostCenterShouldResponseOk()
    {

        $payload = [
            'company_id' => 1,
            'name'  =>  'test team'
        ];

        $this->json(
            'PUT',
            '/cost_center/1',
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateCostCenterShouldResponseUnauthorized()
    {
        $payload = [
            'company_id' => 999,
            'name'  =>  'test team'
        ];

        $this->json(
            'PUT',
            '/cost_center/1',
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
