<?php

namespace TestsNew\Api\CostCenter;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Department\DepartmentEsIndexQueueService;
use App\Department\DepartmentRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use App\CostCenter\CostCenterRequestService;
use Illuminate\Support\Facades\Redis;

class CostCenterTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();


        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    'user_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.cost_centers' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);
    }

    public function testIsNameAvailableAuthzSuccess()
    {

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => $this->getJsonResponse([
                'body' => [1]
            ]),
        ]);

        $this->mockClass(CostCenterRequestService::class, [
            'isNameAvailable' => $this->getJsonResponse([
                'body' => ['available' => true]
            ]),
        ]);

        $this->json(
            'POST',
            '/company/1/cost_center/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.cost_centers'
            ]
        );

        $response = json_decode($this->response->getContent(), true);
        $this->assertEquals($response['available'], true);
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testIsNameAvailableAuthzError()
    {

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => $this->getJsonResponse([
                'body' => [1]
            ]),
        ]);

        $this->mockClass(CostCenterRequestService::class, [
            'isNameAvailable' => $this->getJsonResponse([
                'body' => ['available' => true]
            ]),
        ]);

        $this->json(
            'POST',
            '/company/999/cost_center/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.cost_centers'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}