<?php

namespace TestsNew\Api\CostCenter;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\CostCenter\CostCenterRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\CostCenter\CostCenterEsIndexQueueService;
use App\Audit\AuditService;
use App\CostCenter\CostCenterAuthorizationService;
use App\Account\AccountRequestService;

class CreateCostCenterTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);
        $this->mockClass(CostCenterEsIndexQueueService::class,[
            'queue' => true
        ]);
        $this->mockClass(CostCenterAuthorizationService::class,[
            'authorizeCreate' => true
        ]);

    }

    public function testCreateCostCenterShouldResponseOk()
    {

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.cost_centers' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockRequestService(CostCenterRequestService::class, [
            [
                'body' => [
                    'id' => 1
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'hello',
                    'description' => 'test desc',
                    'company_id' => 1
                ]
            ]

        ]);

        $this->post(
            '/cost_center',
            [
                'name' => 'hello',
                'description' => 'test desc',
                'company_id' => 1
            ],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.cost_centers'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }


    public function testCreateCostCenterShouldResponseUnauthorized()
    {

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => []

                ]
            ]
        ]);

        $this->mockRequestService(CostCenterRequestService::class, [
            [
                'body' => [
                    'id' => 1
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'hello',
                    'description' => 'test desc',
                    'company_id' => 1
                ]
            ]

        ]);

        $this->post(
            '/cost_center',
            [
                'name' => 'hello',
                'description' => 'test desc',
                'company_id' => 1
            ],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_structure.cost_centers'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

}
