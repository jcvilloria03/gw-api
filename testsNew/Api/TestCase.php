<?php

namespace TestsNew\Api;

use Illuminate\Support\Arr;
use Laravel\Lumen\Testing\DatabaseTransactions;
use PHPUnit\Framework\Assert as PHPUnit;
use TestsNew\Helpers\TestCase as BaseTestCase;
use TestsNew\Helpers\Traits\MockDataTrait;

class TestCase extends BaseTestCase
{
    use MockDataTrait;
    use DatabaseTransactions;

    protected function countInDatabase($table, array $data, int $expected, $onConnection = null)
    {
        $count = $this->app->make('db')->connection($onConnection)->table($table)->where($data)->count();

        $this->assertEquals($expected, $count, sprintf(
            'Unable to find row in database table [%s] that matched attributes [%s].',
            $table,
            json_encode($data)
        ));

        return $this;
    }

    public function seeJsonDeep(array $expected)
    {
        $actual = json_decode($this->response->getContent(), true);
        if (is_null($actual) || $actual === false) {
            return PHPUnit::fail('Invalid JSON was returned from the route. Perhaps an exception was thrown?');
        }

        $this->loopCheckJsonDeep($expected, $actual, $actual, [], '');
    }

    private function loopCheckJsonDeep(array $expected, array $actual, array $realActual, array $dataPath, string $path)
    {
        foreach ($expected as $key => $value) {
            $dataTemp = $dataPath;
            Arr::set($dataTemp, ltrim($path . '.' . $key, '.'), $value);
            $this->assertArrayHasKey($key, $actual, sprintf(
                'Unable to find [%s] within [%s]',
                json_encode($dataTemp),
                json_encode($realActual)
            ));
            if (is_array($value)) {
                Arr::set($dataPath, ltrim($path . '.' . $key, '.'), []);
                $this->loopCheckJsonDeep($value, $actual[$key], $realActual, $dataPath, ltrim($path . '.' . $key, '.'));
            } else {
                $this->assertEquals($value, $actual[$key], sprintf(
                    'Unable to find [%s] within [%s]',
                    json_encode($dataTemp),
                    json_encode($realActual)
                ));
            }
        }
    }
}
