<?php

namespace TestsNew\Api\Workflow;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Workflow\WorkflowRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class CreateWorkflowTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(WorkflowRequestService::class, [
            'create' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Test'
                ]
            ]),

            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Test'
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);
    }

    public function testCreateWorkflowSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.workflows' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this
            ->json('POST', '/workflow', [
                'company_id' => 1,
                'name' => 'Test'
            ],
            [
                'X-Authz-Entities' => 'employees.workflows'
            ])
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJson(['id' => 1, 'company_id' => 1, 'name' => 'Test']);
    }

    public function testCreateWorkflowUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.workflows' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/workflow', [
            'company_id' => 1,
            'name' => 'Test'
        ],
        [
            'X-Authz-Entities' => 'employees.workflows'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
