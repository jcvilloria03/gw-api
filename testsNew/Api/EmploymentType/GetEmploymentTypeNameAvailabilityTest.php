<?php

namespace TestsNew\Api\EmploymentType;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\EmploymentType\EmploymentTypeAuditService;
use App\EmploymentType\EmploymentTypeRequestService;

class GetCompanyEmploymentTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'company/%d/employment_type/is_name_available';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.employment_types' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(EmploymentTypeAuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(EmploymentTypeRequestService::class, [
            [
                'body' => [
                    'available' => true
                ]
            ]
        ]);
    }

    public function testUpdateDayHourRateShouldResponseSuccess()
    {
        $payload = [
            'name' => 'test 1'
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.employment_types'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testUpdateDayHourRateShouldResponseUnauthorized()
    {
        $payload = [
            'name' => 'test 1'
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 999),
            $payload,
            [
                'x-authz-entities' => 'company_settings.company_structure.employment_types'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
