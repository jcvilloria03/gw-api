<?php

namespace TestsNew\Api\Shift;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AwsTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class ValidateBatchAssignShiftTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use AwsTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('hSet')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    private function fileContent()
    {
        return 'Employee ID,Employee Name,Schedule Name,Shift Effective Start Date,Shift Effective End Date' . PHP_EOL
            . 'aa-11,Drake Kara,flexi 0800 to 0759 no core,04/27/2020,05/10/2020';
    }

    public function testValidateBatchAssignShiftShouldResponseSuccess()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $scopes =  [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);

        $this->mockAwsSdk([[]]);

        $file = UploadedFile::fake()->create('test.csv');
        file_put_contents($file->getPathName(), $this->fileContent());

        $this->call(
            'POST',
            '/company/1/batch_assign_shifts/validate',
            [],
            [],
            ['file' => $file],
            $this->transformHeadersToServerVars([
                AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module,
                'Content-Type' => 'multipart/form-data',
            ])
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJsonStructure(['id'])
        ;
    }

    public function testValidateBatchAssignShiftShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $scopes =  [
            'COMPANY' => [2],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);

        $this->json(
            'POST',
            '/company/1/batch_assign_shifts/validate',
            [],
            [
                AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module,
                'Content-Type' => 'multipart/form-data',
            ]
        )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testValidateBatchAssignShiftShouldResponseInvalid()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $scopes =  [
            'COMPANY' => [999],
            'PAYROLL_GROUP' => [999],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);
        $file = UploadedFile::fake()->create('test.csv');
        file_put_contents($file->getPathName(), $this->fileContent());

        $this->call(
            'POST',
            '/company/1/batch_assign_shifts/validate',
            [],
            [],
            ['file' => $file],
            $this->transformHeadersToServerVars([
                AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module,
                'Content-Type' => 'multipart/form-data',
            ])
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ;
    }
}