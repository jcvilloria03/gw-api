<?php

namespace TestsNew\Api\Shift;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Shift\ShiftRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class ShiftGenerateCsvTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [1],
                        'position' => [1],
                        'team' => [1],
                        'location' => [1],
                        'payroll_group' => [1],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);


        $this->mockClass(ShiftRequestService::class, [
            'generateCsv' => $this->getJsonResponse([
                'body' => [
                    'file_name' => 'sample_file_name'
                ]
            ]),
        ]);
    }

    public function testShiftUploadSaveSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.shifts' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->post(
            '/company/1/shifts/generate_csv',
            [],
            ['x-authz-entities' => 'time_and_attendance.shifts']
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testShiftUploadSaveError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.shifts' => [
                ]
            ]
        ]);

        $this->post(
            '/company/1/shifts/generate_csv',
            [],
            ['x-authz-entities' => 'time_and_attendance.shifts']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
