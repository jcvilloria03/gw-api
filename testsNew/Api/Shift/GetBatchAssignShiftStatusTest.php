<?php

namespace TestsNew\Api\Shift;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Shift\ShiftRequestService;

class GetBatchAssignShiftStatusTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'company/%d/batch_assign_shifts/status?job_id=1';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);        
        Redis::shouldReceive('hGetAll')
        ->andReturn(['a' => 'b']);        
        Redis::shouldReceive('sMembers')
        ->andReturn(                [
            'employee_shift:PERSONAL:MTpCYXV0aXN0YTpQYXVsOg==',
            'employee_shift:PAYROLL:MTpCYXV0aXN0YTpQYXVsOg=='
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'time_and_attendance.shifts.assign_shift' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        // Controller mocks here
        $this->mockClass(ShiftUploadTask::class,[
            'setTasksPool' => true,
            'fetch' => [
                [
                    'status' => 1
                ]
            ]
        ]);
        $this->mockRequestService(ShiftRequestService::class, [
            [
                'body' => [
                    'data' => []
                ]
            ]
        ]);
    }

    public function testGetBatchAssignShiftStatusShouldResponseSuccess()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_shift'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetBatchAssignShiftStatusShouldResponseUnauthorized()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 999),
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_shift'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
