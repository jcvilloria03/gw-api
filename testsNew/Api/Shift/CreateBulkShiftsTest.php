<?php

namespace TestsNew\Api\Shift;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateBulkShiftsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreateBulkShiftShouldResponseSuccess()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $scopes =  [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);
        $this->shouldExpectRequest('GET', '/schedule/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 1])));

        $affectedEmployees = [
            [
                "id" => 1,
                "employee_id" => 1,
                'company_id' => 1,
                'first_name' => 'first name',
                'middle_name' => 'middle name',
                'last_name' => 'last_name',
                'email' => 'test@test.com',
                'gender' => null,
                'date_hired' => '2020-01-15',
                'location_id' => 1,
                'position_id' => 1,
                'department_id' => 1,
                'payroll_group_id' => 1,
                'team_id' =>  null
            ],
            [

                "id" => 2,
                "employee_id" => 2,
                'company_id' => 1,
                'first_name' => 'first name2',
                'middle_name' => 'middle name2',
                'last_name' => 'last_name2',
                'email' => 'test@test2.com',
                'gender' => null,
                'date_hired' => '2020-01-15',
                'location_id' => 1,
                'position_id' => 1,
                'department_id' => 1,
                'payroll_group_id' => 1,
                'team_id' =>  null,
            ]
        ];

        $this->shouldExpectRequest('GET', '/company/1/active_affected_employees')
            ->once()
            ->withBody(json_encode([
                'affected_entities' => [
                    ['id' => 1, 'type' => 'employee'],
                    ['id' => 2, 'type' => 'employee'],
                    ['id' => 1, 'type' => 'department'],
                    ['id' => 1, 'type' => 'position'],
                ]
            ]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $affectedEmployees])));

        $inputs = [
            'schedule_id' => 1,
            'start' => '2020-07-05',
            'end' => '2020-07-05',
            'affected_employees' => [
                ['id' => 1, 'type' => 'employee', 'name' => 'Employee 1'],
                ['id' => 2, 'type' => 'employee', 'name' => 'Employee 2'],
                ['id' => 1, 'type' => 'department', 'name' => 'Department 1'],
                ['id' => 1, 'type' => 'position', 'name' => 'Position 1'],
            ],
            'affected_employee_ids' => [1, 2]
        ];

        $this->post(
            '/shift/bulk_create',
            $inputs,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(HttpResponse::HTTP_OK);
    }

    public function testCreateBulkShiftShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $scopes =  [
            'COMPANY' => [2],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);
        $this->shouldExpectRequest('GET', '/schedule/1')
        ->once()
        ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 1])));

        $affectedEmployees = [
            [
                "id" => 1,
                "employee_id" => 1,
                'company_id' => 1,
                'first_name' => 'first name',
                'middle_name' => 'middle name',
                'last_name' => 'last_name',
                'email' => 'test@test.com',
                'gender' => null,
                'date_hired' => '2020-01-15',
                'location_id' => null,
                'position_id' => null,
                'department_id' => null,
                'payroll_group_id' => null,
                'team_id' =>  null
            ],
            [

                "id" => 2,
                "employee_id" => 2,
                'company_id' => 1,
                'first_name' => 'first name2',
                'middle_name' => 'middle name2',
                'last_name' => 'last_name2',
                'email' => 'test@test2.com',
                'gender' => null,
                'date_hired' => '2020-01-15',
                'location_id' => null,
                'position_id' => null,
                'department_id' => null,
                'payroll_group_id' => null,
                'team_id' =>  null,
            ]
        ];

        $this->shouldExpectRequest('GET', '/company/1/active_affected_employees')
        ->once()
        ->withBody(json_encode([
            'affected_entities' => [
                ['id' => 1, 'type' => 'employee'],
                ['id' => 2, 'type' => 'employee'],
                ['id' => 1, 'type' => 'department'],
                ['id' => 1, 'type' => 'position'],
            ]
        ]))
        ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $affectedEmployees])));

        $inputs = [
            'schedule_id' => 1,
            'start' => '2020-07-05',
            'end' => '2020-07-05',
            'affected_employees' => [
                ['id' => 1, 'type' => 'employee', 'name' => 'Employee 1'],
                ['id' => 2, 'type' => 'employee', 'name' => 'Employee 2'],
                ['id' => 1, 'type' => 'department', 'name' => 'Department 1'],
                ['id' => 1, 'type' => 'position', 'name' => 'Position 1'],
            ],
            'affected_employee_ids' => [1, 2]
        ];

        $this->json('POST', '/shift/bulk_create', $inputs, [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
