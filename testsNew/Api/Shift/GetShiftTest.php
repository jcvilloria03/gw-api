<?php

namespace TestsNew\Api\Shift;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Shift\ShiftRequestService;

class GetShiftTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'shift/1';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);        
        Redis::shouldReceive('hGetAll')
        ->andReturn(null);        
        Redis::shouldReceive('sMembers')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'time_and_attendance.shifts.assign_shift' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

    }

    public function testGetShiftShouldResponseSuccess()
    {
        $this->mockRequestService(ShiftRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'schedule_id' => 1,
                    'company_id' => 1
                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_shift'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetShiftShouldResponseUnauthorized()
    {
        $this->mockRequestService(ShiftRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'schedule_id' => 1,
                    'company_id' => 999
                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_shift'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
