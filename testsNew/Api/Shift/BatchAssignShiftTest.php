<?php

namespace TestsNew\Api\Shift;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class BatchAssignShiftTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testBatchAssignShiftShouldResponseSuccess()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);
        $inputs = ['job_id' => '123456'];
        Redis::shouldReceive('hGetAll')->with('123456')->andReturn([
            'validate_shifts_status' => 'validated',
        ]);
        Redis::shouldReceive('hSet');

        $this
            ->json(
                'POST',
                '/company/1/batch_assign_shifts/save',
                $inputs,
                [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson(['id' => '123456'])
        ;
    }

    public function testBatchAssignShiftShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.shifts.assign_shift';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);

        $this
            ->json(
                'POST',
                '/company/2/batch_assign_shifts/save',
                [],
                [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}