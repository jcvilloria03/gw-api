<?php

namespace TestsNew\Api\Rank;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Rank\RankAuditService;
use App\Rank\RankEsIndexQueueService;
use App\Rank\RankRequestService;
use App\Team\TeamAuditService;
use App\Team\TeamRequestService;

class GetCompanyTeamTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'company/%d/teams';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(TeamAuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(TeamRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'id' => 1,
                        'name' => 'test rank',
                        'company_id' => 1,
                        'account_id' => 1,
                        'edit_team_schedule' => true,
                        'view_team_attendance' => true,
                        'regenerate_team_attendance' => true
                    ]
                ]
            ]
        ]);
    }

    public function testGetCompanyTeamShouldResponseSuccess()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            [],
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyTeamResponseUnauthorized()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 999),
            [],
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
