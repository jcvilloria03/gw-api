<?php

namespace TestsNew\Api\Team;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Team\TeamRequestService;
use App\Account\AccountRequestService;

class UpdateTeamTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(TeamRequestService::class, [
            [ // get
                'body' => [
                    'id' => 1,
                    'name' => 1,
                    'company_id' => 1,
                    'edit_team_schedule' => true,
                    'view_team_attendance' => true,
                    'regenerate_team_attendance' => false,
                    'leader' => null,
                    'members' => []
                ]
            ],
            [ // update
                'body' => [
                    'id' => 1,
                    'name' => 1,
                    'company_id' => 1,
                    'edit_team_schedule' => true,
                    'view_team_attendance' => true,
                    'regenerate_team_attendance' => false,
                    'leader' => null,
                    'members' => []
                ]
            ],
            [ // get
                'body' => [
                    'id' => 1,
                    'name' => 1,
                    'company_id' => 1,
                    'edit_team_schedule' => true,
                    'view_team_attendance' => true,
                    'regenerate_team_attendance' => false,
                    'leader' => null,
                    'members' => []
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

    }

    public function testUpdateTeamShouldResponseOk()
    {

        $payload = [
            'company_id' => 1,
            'name'  =>  'test team'
        ];

        $this->json(
            'PUT',
            '/team/1',
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateTeamShouldResponseUnauthorized()
    {
        $payload = [
            'company_id' => 999,
            'name'  =>  'test team'
        ];

        $this->json(
            'PUT',
            '/team/1',
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
