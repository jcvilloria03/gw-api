<?php

namespace TestsNew\Api\Team;

use App\Authz\AuthzRequestService;
use App\Team\TeamRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetTeamTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);
    }

    public function testGetSuccess()
    {
        $this->mockRequestService(TeamRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'name' => 'Test Team'
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/team/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetSuccessCompanyAll()
    {
        $this->mockRequestService(TeamRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'name' => 'Test Team'
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/team/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetIncorrectCompany()
    {
        $this->mockRequestService(TeamRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 2,
                    'name' => 'Test Team'
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/team/1',
            [],
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
