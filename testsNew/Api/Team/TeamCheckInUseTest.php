<?php

namespace TestsNew\Api\Team;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Team\TeamRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class TeamCheckInUseTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(TeamRequestService::class, [
            'checkInUse' => $this->getJsonResponse([
                'body' => [
                    'in_use' => 0
                ]
            ]),
        ]);
    }

    public function testCheckInUseSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.companies' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/team/check_in_use', [
            'company_id' => '1',
            'team_ids' => [1, 2]
        ],
        [
            'X-Authz-Entities' => 'company_settings.company_structure.teams'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testCheckInUseError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.companiess' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/team/check_in_use', [
            'company_id' => '999',
            'team_ids' => [1, 2]
        ],
        [
            'X-Authz-Entities' => 'company_settings.company_structure.teams'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
