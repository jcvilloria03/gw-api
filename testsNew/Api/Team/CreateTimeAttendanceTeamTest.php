<?php

namespace TestsNew\Api\Team;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Team\TeamAuditService;
use App\Team\TeamRequestService;

class CreateTimeAttendanceTeamTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'time_attendance_team';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);
        Redis::shouldReceive('del');

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.teams' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(TeamAuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(TeamRequestService::class, [
            [
                'body' => [
                    'id' => 1
                ],
                'code' => 201
            ],
            [
                'body' => [
                    'data' => [
                        'id' => 1
                    ]
                ],
                'code' => 201
            ],
        ]);
    }

    public function testBulkDeleteTeamShouldResponseSuccess()
    {
        $payload = [
            "name" => "halo",
            "company_id" => 1,
            "leaders_ids" => [1],
            "members_ids" => [1],
            "edit_team_schedule" => true,
            "regenerate_team_attendance" => true
        ];
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testBulkDeleteTeamShouldResponseUnauthorized()
    {
        $payload = [
            "name" => "halo",
            "company_id" => 999,
            "leaders_ids" => [1],
            "members_ids" => [1],
            "edit_team_schedule" => true,
            "regenerate_team_attendance" => true
        ];
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.teams'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
