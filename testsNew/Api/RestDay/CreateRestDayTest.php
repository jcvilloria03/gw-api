<?php

namespace TestsNew\Api\RestDay;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\RestDay\RestDayRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class CreateRestDayTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(RestDayRequestService::class, [
            'create' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Test',
                    'start_date' => '2000-01-01',
                    'end_date' => '2100-01-01'
                ]
            ]),

            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1,
                    'start_date' => '2000-01-01',
                    'end_date' => '2100-01-01'
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);
    }

    public function testCreateRestDaySuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.shifts.assign_rest_day' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/rest_day', [
            'company_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'repeat' => [
                'rest_days'=> [
                    'Monday'
                ],
                'repeat_every' => 1,
                'end_never' => false,
                'end_after' => 1
            ]
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.shifts.assign_rest_day'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testCreateRestDayUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.shifts.assign_rest_days' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/rest_day', [
            'company_id' => 999,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'repeat' => [
                'rest_days'=> [
                    'Monday'
                ],
                'repeat_every' => 1,
                'end_never' => false,
                'end_after' => 1
            ]
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.shifts.assign_rest_day'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
