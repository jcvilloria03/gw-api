<?php

namespace TestsNew\Api\RestDay;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class PutRestDayTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/rest_day/1';
    const TARGET_METHOD = 'PUT';
    const MODULE = 'time_and_attendance.shifts.assign_rest_day';

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            ]
        );

        $expectedRestDay = [
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employees' => null,
            'dates' => ['2000-01-03'],
            'repeat' => [
                'id' => 1,
                'repeat_every' => 1,
                'rest_days' => ["monday"],
                'end_after' => 1,
                'end_never' => false
            ]
        ];

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $expected = [
            'id' => 1,
            'company_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employee' => null,
            'dates' => '2020-01-03'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [
                    "start" => '2000-01-01',
                    "end" => '2100-01-01'
                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_OK)
        ->seeJson($expected)
        ;
    }

    public function testShouldResponseError()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            ]
        );

        $expectedRestDay = [
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employees' => null,
            'dates' => ['2000-01-03'],
            'repeat' => [
                'id' => 1,
                'repeat_every' => 1,
                'rest_days' => ["monday"],
                'end_after' => 1,
                'end_never' => false
            ]
        ];

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $expected = [
            'id' => 1,
            'company_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employee' => null,
            'dates' => '2020-01-03'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_UNAUTHORIZED,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                "/rest_day/1",
                [
                    "start" => '2000-01-01',
                    "end" => '2100-01-01'
                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testWithouAuthzShouldResponseError()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => '',
                'data_scope' => []
            ]
        );

        $expectedRestDay = [
            'id' => 1,
            'company_id' => 1,
            'employee_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employees' => null,
            'dates' => ['2000-01-03'],
            'repeat' => [
                'id' => 1,
                'repeat_every' => 1,
                'rest_days' => ["monday"],
                'end_after' => 1,
                'end_never' => false
            ]
        ];

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $expected = [
            'id' => 1,
            'company_id' => 1,
            'start_date' => '2000-01-01',
            'end_date' => '2100-01-01',
            'employee' => null,
            'dates' => '2020-01-03'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_UNAUTHORIZED,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this->shouldExpectRequest('GET', '/rest_day/1')
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expectedRestDay)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                "/rest_day/1",
                [
                    "start" => '2000-01-01',
                    "end" => '2100-01-01'
                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
