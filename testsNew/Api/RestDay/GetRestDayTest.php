<?php

namespace TestsNew\Api\RestDay;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Employee\EmployeeRequestService;
use App\EmploymentType\EmploymentTypeAuditService;
use App\EmploymentType\EmploymentTypeRequestService;
use App\Rank\RankAuditService;
use App\Rank\RankRequestService;
use App\RestDay\RestDayRequestService;

class GetRestDayTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'rest_day/1';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

	    // Controller mocks here
        $this->mockRequestService(RestDayRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'start_date' => '2020-06-18',
                    'end_date' => '2020-06-19',
                    'employee_id' => 1
                ]
            ]
        ]);

    }

    public function testGetRestDayShouldResponseSuccess()
    {

        $this->mockRequestService(EmployeeRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'department_id' => 1,
                    'team_id' => 1,
                    'payroll_group' => [
                        'payroll_group_id' => 1,
                    ],

                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'time_and_attendance.shifts.assign_rest_day' => [
                            'data_scope' => [
                                'COMPANY' => [1],
                                'DEPARTMENT' => [1],
                                'POSITION' => [1],
                                'LOCATION' => [1],
                                'TEAM' => [1],
                                'PAYROLL_GROUP' => [1],
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_rest_day'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetRestDayShouldResponseUnauthorized()
    {

        $this->mockRequestService(EmployeeRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 9,
                    'position_id' => 9,
                    'location_id' => 1,
                    'department_id' => 9,
                    'team_id' => 9,
                    'payroll_group' => [
                        'payroll_group_id' => 9,
                    ],

                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'time_and_attendance.shifts.assign_rest_day' => [
                            'data_scope' => [
                                'COMPANY' => [1],
                                'DEPARTMENT' => [1],
                                'POSITION' => [1],
                                'LOCATION' => [1],
                                'TEAM' => [1],
                                'PAYROLL_GROUP' => [1],
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            [],
            [
                'X-Authz-Entities' => 'time_and_attendance.shifts.assign_rest_day'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

    }
}
