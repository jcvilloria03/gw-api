<?php

namespace TestsNew\Api\Calendar;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCalendarDataTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'main_page.dashboard';
    const TARGET_METHOD = 'POST';
    const TARGET_URL = '/admin_dashboard/calendar_data';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hGetAll');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('del');
        Redis::shouldReceive('hExists');
        Redis::shouldReceive('exists');
        Amqp::shouldReceive('publish');
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $calendarData = [
            [
                'employee_id' => 1,
                'employee' => [
                    'account_id' => 1,
                    'company_id' => 1,
                    'department_id' => 1,
                    'department_name' => 'Sales',
                    'employee_id' => '0000',
                    'first_name' => 'Zero',
                    'full_name' => 'Zero Devzero',
                    'id' => 1,
                    'last_name' => 'Devzero',
                    'location_id' => 1,
                    'location_name' => 'Main Office',
                    'middle_name' => '',
                    'non_taxed_other_income_limit' => '90000.0000',
                    'payroll' => [
                        'payroll_group_id' => 1,
                        'payroll_group_name' => '4732'
                    ],
                    'payroll_group' => [
                        'id' => 1
                    ],
                    'position_id' => 1,
                    'position_name' => 'Sales Staff',
                    'team_id' => 1,
                    'user_id' => 1
                ],
                'shifts' => []
            ]
        ];

        $requestPayload = [
            'company_id' => 1,
            'group' => [
                'payroll'
            ] 
        ];

        $this
            ->shouldExpectRequest('POST', '/admin_dashboard/calendar_data')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($calendarData)));

        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [99],
                'PAYROLL_GROUP' => [99],
                'POSITION' => [99],
                'DEPARTMENT' => [99],
                'TEAM' => [99],
                'LOCATION' => [99],
            ],
        ]);

        $requestPayload = [
            'company_id' => 1,
            'group' => [
                'payroll'
            ] 
        ];

        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
