<?php

namespace TestsNew\Api\PayrollLoan;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdatePayrollLoanTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.payroll_summary';
    const METHOD = 'POST';
    const URL = '/payroll_loan/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/payroll_loan/1')
            ->twice()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'collected_to_date' => '0.00',
                'company_id' => 1,
                'created_date' => '2020-06-01',
                'employee_id' => 1,
                'final_pay_id' => null,
                'employee_id_no' => '',
                'employee_name' => ' ',
                'id' => 1,
                'is_closed' => false,
                'is_paid' => false,
                'monthly_amortization' => '50.00',
                'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
                'payment_start_date' => '2020-06-01',
                'remaining_balance' => '100.00',
                'status' => 'Approved',
                'term' => 2,
                'total_amount' => '100.00',
                'type_id' => 1,
                'type_name' => 'SSS',
                'sub_type_name' => 'SALARY',
                'reference_no' => '1312432421421',
                'payment_status' => '0%',
                'payment_end_date' => '2020-07-03',
                'employee_location_id' => null,
                'employee_department_id' => null,
                'employee_position_id' => null,
                'active' => true,
                'payroll_id' => null,
                'type' => [
                    'company_id' => 0,
                    'id' => 1,
                    'is_editable' => 0,
                    'name' => 'SSS'
                ]
            ])),
            new Response(HttpResponse::HTTP_OK, [], json_encode([
                'collected_to_date' => '0.00',
                'company_id' => 1,
                'created_date' => '2020-06-01',
                'employee_id' => 1,
                'id' => 1,
                'is_closed' => false,
                'is_paid' => false,
                'monthly_amortization' => '50.00',
                'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
                'payment_start_date' => '2020-06-01',
                'remaining_balance' => '100.00',
                'status' => 'Approved',
                'term' => 4,
                'total_amount' => '100.00',
                'type_id' => 1,
                'type_name' => 'SSS',
                'sub_type_name' => 'SALARY',
                'reference_no' => '1312432421421',
                'payment_status' => '0%',
                'payment_end_date' => '2020-07-03',
                'employee_location_id' => null,
                'employee_department_id' => null,
                'employee_position_id' => null,
                'active' => true,
                'payroll_id' => null,
                'type' => [
                    'company_id' => 0,
                    'id' => 1,
                    'is_editable' => 0,
                    'name' => 'SSS'
                ]
            ])));


        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/payroll_loan/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1
            ])));

        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '25.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '4',
            'uid' => '5efdaa4f97c6e',
            'company_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));

        Amqp::shouldReceive('publish');

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testWithPayrollIdShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1
            ])));

        $this
            ->shouldExpectRequest('GET', '/payroll_loan/1')
            ->twice()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'collected_to_date' => '0.00',
                'company_id' => 1,
                'created_date' => '2020-06-01',
                'employee_id' => 1,
                'final_pay_id' => null,
                'employee_id_no' => '',
                'employee_name' => ' ',
                'id' => 1,
                'is_closed' => false,
                'is_paid' => false,
                'monthly_amortization' => '50.00',
                'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
                'payment_start_date' => '2020-06-01',
                'remaining_balance' => '100.00',
                'status' => 'Approved',
                'term' => 2,
                'total_amount' => '100.00',
                'type_id' => 1,
                'type_name' => 'SSS',
                'sub_type_name' => 'SALARY',
                'reference_no' => '1312432421421',
                'payment_status' => '0%',
                'payment_end_date' => '2020-07-03',
                'employee_location_id' => null,
                'employee_department_id' => null,
                'employee_position_id' => null,
                'active' => true,
                'payroll_id' => null,
                'type' => [
                    'company_id' => 0,
                    'id' => 1,
                    'is_editable' => 0,
                    'name' => 'SSS'
                ]
            ])),
            new Response(HttpResponse::HTTP_OK, [], json_encode([
                'collected_to_date' => '0.00',
                'company_id' => 1,
                'created_date' => '2020-06-01',
                'employee_id' => 1,
                'id' => 1,
                'is_closed' => false,
                'is_paid' => false,
                'monthly_amortization' => '50.00',
                'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
                'payment_start_date' => '2020-06-01',
                'remaining_balance' => '100.00',
                'status' => 'Approved',
                'term' => 4,
                'total_amount' => '100.00',
                'type_id' => 1,
                'type_name' => 'SSS',
                'sub_type_name' => 'SALARY',
                'reference_no' => '1312432421421',
                'payment_status' => '0%',
                'payment_end_date' => '2020-07-03',
                'employee_location_id' => null,
                'employee_department_id' => null,
                'employee_position_id' => null,
                'active' => true,
                'payroll_id' => null,
                'type' => [
                    'company_id' => 0,
                    'id' => 1,
                    'is_editable' => 0,
                    'name' => 'SSS'
                ]
            ])));


        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => null
            ])));

        $this
            ->shouldExpectRequest('POST', '/payroll_loan/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
            ])));

        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '25.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '4',
            'uid' => '5efdaa4f97c6e',
            'company_id' => 1
        ];

        Amqp::shouldReceive('publish');

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/payroll_loan/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'collected_to_date' => '0.00',
                'company_id' => 1,
                'created_date' => '2020-06-01',
                'employee_id' => 1,
                'final_pay_id' => null,
                'employee_id_no' => '',
                'employee_name' => ' ',
                'id' => 1,
                'is_closed' => false,
                'is_paid' => false,
                'monthly_amortization' => '50.00',
                'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
                'payment_start_date' => '2020-06-01',
                'remaining_balance' => '100.00',
                'status' => 'Approved',
                'term' => 2,
                'total_amount' => '100.00',
                'type_id' => 1,
                'type_name' => 'SSS',
                'sub_type_name' => 'SALARY',
                'reference_no' => '1312432421421',
                'payment_status' => '0%',
                'payment_end_date' => '2020-07-03',
                'employee_location_id' => null,
                'employee_department_id' => null,
                'employee_position_id' => null,
                'active' => true,
                'payroll_id' => null,
                'type' => [
                    'company_id' => 0,
                    'id' => 1,
                    'is_editable' => 0,
                    'name' => 'SSS'
                ]
            ])));

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => null
            ])));

        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '50.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '2',
            'uid' => '5efdaa4f97c6e',
            'company_id' => 1
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
