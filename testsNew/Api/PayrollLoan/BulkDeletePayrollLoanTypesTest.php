<?php

namespace TestsNew\Api\PayrollLoan;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class BulkDeletePayrollLoanTypesTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.loan_type_settings' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(PayrollLoanTypeRequestService::class, [
            ['body' => null, 'code' => Response::HTTP_NO_CONTENT]
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            ['body' => ['account_id' => 1]]
        ]);

        $this->mockClass(AuditService::class, ['queue' => true]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    protected function mockCompanyFormData(): array
    {
        return [
            'company_id' => 1,
            'payroll_loan_type_ids' => [1, 2, 3, 4]
        ];
    }

    public function testDeleteSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.locations' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->delete(
            '/payroll_loan_type/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteSuccessAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.locations' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->delete(
            '/payroll_loan_type/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteIncorrectCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [2, 3]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        
        $this->delete(
            '/payroll_loan_type/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDeleteIncorrectUnrelatedCompanyAndLocation()
    {
        $this->markTestSkipped('Bug found on deleting unrelated company payroll loan type');
        $this->mockRequestService(PayrollLoanTypeRequestService::class, [
            [
                'body' => [
                    'message' => 'Payroll Loan Type should be under the company'
                ],
                'code' => Response::HTTP_CONFLICT
            ]
        ]);

        $payload = $this->mockCompanyFormData();
        $payload['company_id'] = 999;
        
        $this->delete(
            '/payroll_loan_type/bulk_delete',
            $payload,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_CONFLICT);
    }
}
