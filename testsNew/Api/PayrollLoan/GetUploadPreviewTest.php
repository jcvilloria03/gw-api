<?php

namespace TestsNew\Api\PayrollLoan;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\PayrollLoan\PayrollLoanUploadTask;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetUploadPreviewTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(PayrollLoanRequestService::class, [
            'getUploadPreview' => $this->getJsonResponse([
                'body' => [
                    'data' => []
                ]
            ])
        ]);

        $this->mockClass(PayrollLoanUploadTask::class, [
            'create' => true
        ]);
    }

    public function testGetUploadPreviewSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.loans' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll_loan/upload/preview?company_id=1&job_id=abc',
            [],
            [
                'x-authz-entities' => 'employees.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetUploadPreviewUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.loans' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);
        
        $this->json(
            'GET',
            '/payroll_loan/upload/preview?company_id=999&job_id=abc',
            [],
            [
                'x-authz-entities' => 'employees.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
