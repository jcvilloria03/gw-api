<?php

namespace TestsNew\Api\PayrollLoan;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class ActivateGapLoanTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testActivateGapLoanShouldResponseSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;
        $payrollLoans = [
            'data' => [
                ['company_id' => 1, 'id' => 1]
            ]
        ];
        $this->shouldExpectRequest('POST', '/company/1/payroll_loans/payroll_id')
            ->once()
            ->withBody(http_build_query(['values' => '1']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollLoans)))
        ;
        $expected = [];
        $inputs = ['payroll_id' => 1];
        $this->shouldExpectRequest('POST', '/company/1/activate_gap_loans')
            ->once()
            ->withBody(http_build_query($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)))
        ;

        $this->json('POST', '/company/1/activate_gap_loans', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson($expected)
        ;
    }

    public function testActivateGapLoanShouldResponseUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]],
        ]);
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this->json('POST', '/company/1/activate_gap_loans', ['payroll_id' => 1], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testActivateGapLoanShouldResponseUnauthorizedGapLoan()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;
        $payrollLoans = [
            'data' => [
                ['company_id' => 2, 'id' => 1]
            ]
        ];
        $this->shouldExpectRequest('POST', '/company/1/payroll_loans/payroll_id')
            ->once()
            ->withBody(http_build_query(['values' => '1']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollLoans)))
        ;
        $inputs = ['payroll_id' => 1];

        $this->json('POST', '/company/1/activate_gap_loans', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson([
                'message' => 'Not authorized to update all specified gap loans',
                'status_code' => HttpResponse::HTTP_UNAUTHORIZED,
            ])
        ;
    }
}