<?php

namespace TestsNew\Api\PayrollLoan;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\PayrollLoan\PayrollLoanUploadTask;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetInitialPreviewTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(PayrollLoanRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'employee_id' => 1,
                    'company_id' => 1
                ]
            ]),

            'initialPreview' => $this->getJsonResponse([
                'body' => [
                    'id' => 1
                ]
            ])
        ]);

        $this->mockClass(PayrollLoanUploadTask::class, [
            'create' => true
        ]);
    }

    public function testGetInitialPreviewSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.loans' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll_loan/1/initial_preview',
            [],
            [
                'x-authz-entities' => 'employees.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetInitialPreviewUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.loans' => [
                    'data_scope' => [
                        'COMPANY' => [123],
                        'DEPARTMENT' => [45],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll_loan/1/initial_preview',
            [],
            [
                'x-authz-entities' => 'employees.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
