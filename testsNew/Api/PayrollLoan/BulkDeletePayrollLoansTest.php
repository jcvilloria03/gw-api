<?php

namespace TestsNew\Api\PayrollLoan;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class BulkDeletePayrollLoansTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.loans';
    const METHOD = 'DELETE';
    const URL = '/payroll_loan/bulk_delete';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2, 3],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 3, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('POST', '/company/1/payroll_loans/id')
            ->withBody(http_build_query(['values' => '1,2']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'employee_id' => 1,
                        'payroll_id' => 3
                    ],
                    [
                        'id' => 2,
                        'employee_id' => 2,
                        'payroll_id' => null
                    ]
                ]
            ])));
        
        $this
            ->shouldExpectRequest('GET', '/payrolls')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                [
                    'id' => 3,
                    'company_id' => 1,
                    'payroll_group_id' => 3
                ]
            ])));
        
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));
        
        $this
            ->shouldExpectRequest('GET', '/employee/2')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 2,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 2
                ],
                'team_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/payroll_loans/id')
            ->withBody(http_build_query(['values' => '1,2']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'employee_id' => 1,
                        'payroll_id' => 3
                    ],
                    [
                        'id' => 2,
                        'employee_id' => 2,
                        'payroll_id' => null
                    ]
                ]
            ])));
        

        $this
            ->shouldExpectRequest('DELETE', '/payroll_loan/bulk_delete')
            ->andReturnResponse(new Response(HttpResponse::HTTP_NO_CONTENT, [], null));

        $requestPayload = [
            'payroll_loans_ids' => [1,2],
            'company_id' => 1,
        ];

        Amqp::shouldReceive('publish');
        
        $this->json(
            self::METHOD,
            self::URL,
            $requestPayload,
            [
                self::HEADER => self::MODULE,
            ]
        );

        $this->seeStatusCode(HttpResponse::HTTP_NO_CONTENT);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();
        
        $this
            ->shouldExpectRequest('GET', '/payrolls')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                [
                    'id' => 3,
                    'company_id' => 2,
                    'payroll_group_id' => 3
                ]
            ])));
        
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 33,
                'company_id' => 33,
                'location_id' => 33,
                'department_id' => 33,
                'position_id' => 33,
                'payroll_group' => [
                    'id' => 33
                ],
                'team_id' => 33
            ])));
        
        $this
            ->shouldExpectRequest('GET', '/employee/2')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 2,
                'account_id' => 99,
                'company_id' => 99,
                'location_id' => 99,
                'department_id' => 99,
                'position_id' => 99,
                'payroll_group' => [
                    'id' => 2
                ],
                'team_id' => 99
            ])));


        $requestPayload = [
            'payroll_loan_ids' => [1],
            'company_id' => 1
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
