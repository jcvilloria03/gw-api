<?php

namespace TestsNew\Api\PayrollLoan;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyPayrollLoanTypesTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(PayrollLoanTypeRequestService::class, [
            [
                'body' => [
                    'data' => [
                        [
                            'company_id' => 0,
                            'id' => 1,
                            'is_editable' => 0,
                            'name' => 'SSS'
                        ],
                        [
                            'company_id' => 0,
                            'id' => 2,
                            'is_editable' => 0,
                            'name' => 'Pag-ibig'
                        ],
                        [
                            'company_id' => 0,
                            'id' => 3,
                            'is_editable' => 0,
                            'name' => 'Gap'
                        ],
                        [
                            'company_id' => 1,
                            'id' => 4,
                            'is_editable' => 1,
                            'name' => 'Personal Loan'
                        ],
                        [
                            'company_id' => 1,
                            'id' => 5,
                            'is_editable' => 1,
                            'name' => 'Car Loan'
                        ],
                        [
                            'company_id' => 1,
                            'id' => 8,
                            'is_editable' => 1,
                            'name' => 'Home Loan'
                        ],
                        [
                            'company_id' => 1,
                            'id' => 10,
                            'is_editable' => 1,
                            'name' => 'Car Loan Expanded'
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            'getAccountId' => [
                'body' => [
                    'account_id' => 1
                ]
            ]
        ]);
    }
    
    public function testGetPayrollLoanTypesSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.loan_type_settings' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/payroll_loan_types',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseOk();
    }

    public function testGetPayrollLoanTypesAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.loan_type_settings' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/payroll_loan_types',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseOk();
    }

    public function testGetPayrollLoanTypesCompanyIdMismatch()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        
        $this->json(
            'GET',
            '/company/7/payroll_loan_types',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
