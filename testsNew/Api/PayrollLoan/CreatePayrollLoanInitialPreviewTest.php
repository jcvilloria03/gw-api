<?php

namespace TestsNew\Api\PayrollLoan;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreatePayrollLoanInitialPreviewTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.payroll_summary';
    const METHOD = 'POST';
    const URL = '/payroll_loan';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));

        $this
            ->shouldExpectRequest('POST', '/payroll_loan')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'company_id' => '1',
                'employee_id' => '1',
                'reference_no' => '111111111',
                'type_id' => '1',
                'subtype' => 'SALARY',
                'created_date' => '2020-06-01',
                'payment_start_date' => '2020-06-01',
                'total_amount' => '100',
                'monthly_amortization' => '50',
                'payment_scheme' => 'EVERY_PAY_OF_THE_MONTH',
                'term' => '2',
                'uid' => '5efdbb0d2e542',
                'payment_end_date' => '2020-07-03',
                'amortizations' => [
                    [
                        'amount_collected' => '0.00',
                        'amount_due' => '50.00',
                        'amount_remaining' => '100.00',
                        'amount_paid' => '0.00',
                        'due_date' => '2020-06-05',
                        'uid' => '5efdbb0e34350',
                        'is_collected' => 0,
                        'loan_id' => null,
                        'employer_remarks' => '',
                        'final_pay_id' => null
                    ],
                    [
                        'amount_collected' => '0.00',
                        'amount_due' => '50.00',
                        'amount_remaining' => '100.00',
                        'amount_paid' => '0.00',
                        'due_date' => '2020-07-03',
                        'uid' => '5efdbb0e34390',
                        'is_collected' => 0,
                        'loan_id' => null,
                        'employer_remarks' => '',
                        'final_pay_id' => null
                    ]
                ]
            ])));

        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '50.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '2',
            'uid' => '5efdaa4f97c6e',
            'previousRouteName' => 'Loans',
            'company_id' => 1
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testWithPayrollIdShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => null
            ])));

        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1
            ])));

            $this
            ->shouldExpectRequest('POST', '/payroll_loan')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'company_id' => '1',
                'employee_id' => '1',
                'reference_no' => '111111111',
                'type_id' => '1',
                'subtype' => 'SALARY',
                'created_date' => '2020-06-01',
                'payment_start_date' => '2020-06-01',
                'total_amount' => '100',
                'monthly_amortization' => '50',
                'payment_scheme' => 'EVERY_PAY_OF_THE_MONTH',
                'term' => '2',
                'uid' => '5efdbb0d2e542',
                'payment_end_date' => '2020-07-03',
                'amortizations' => [
                    [
                        'amount_collected' => '0.00',
                        'amount_due' => '50.00',
                        'amount_remaining' => '100.00',
                        'amount_paid' => '0.00',
                        'due_date' => '2020-06-05',
                        'uid' => '5efdbb0e34350',
                        'is_collected' => 0,
                        'loan_id' => null,
                        'employer_remarks' => '',
                        'final_pay_id' => null
                    ],
                    [
                        'amount_collected' => '0.00',
                        'amount_due' => '50.00',
                        'amount_remaining' => '100.00',
                        'amount_paid' => '0.00',
                        'due_date' => '2020-07-03',
                        'uid' => '5efdbb0e34390',
                        'is_collected' => 0,
                        'loan_id' => null,
                        'employer_remarks' => '',
                        'final_pay_id' => null
                    ]
                ]
            ])));

        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '50.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '2',
            'uid' => '5efdaa4f97c6e',
            'company_id' => 1,
            'payroll_id' => 1
        ];

        Amqp::shouldReceive('publish');

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);
        
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => null
            ])));
        
        $requestPayload = [
            'employee_id' => 1,
            'type_id' => 1,
            'subtype' => 'SALARY',
            'created_date' => '2020-06-01',
            'payment_start_date' => '2020-06-01',
            'total_amount' => '100.00',
            'monthly_amortization' => '50.00',
            'payment_scheme' => 'FIRST_PAY_OF_THE_MONTH',
            'reference_no' => '111111111',
            'term' => '2',
            'uid' => '5efdaa4f97c6e',
            'company_id' => 1
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
