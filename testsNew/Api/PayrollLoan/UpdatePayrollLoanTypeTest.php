<?php

namespace TestsNew\Api\PayrollLoan;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use Illuminate\Http\Response;

class UpdatePayrollLoanTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.loan_type_settings' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockClass(PayrollLoanTypeRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'company_id' => 1,
                    'id' => 4,
                    'is_editable' => 1,
                    'name' => 'Fixed loan term'
                ]
            ]),
            'update' => $this->getJsonResponse([
                'body' => [
                    'company_id' => 1,
                    'id' => 4,
                    'is_editable' => 1,
                    'name' => 'Fixed loan terms'
                ]
            ]),
            'get' => $this->getJsonResponse([
                'body' => [
                    'company_id' => 1,
                    'id' => 4,
                    'is_editable' => 1,
                    'name' => 'Fixed loan terms'
                ]
            ]),
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    public function testCreatePayrollLoanTypeSuccess()
    {
        $this->patch(
            '/payroll_loan_type/4',
            ['company_id' => 1, 'name' => 'Fixed loan terms'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );
        
        $this->assertResponseOk();
    }

    public function testCreatePayrollLoanTypeAllCompanySuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.loan_type_settings' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->patch(
            '/payroll_loan_type/4',
            ['company_id' => 1, 'name' => 'Fixed loan terms'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );

        $this->assertResponseOk();
    }

    public function testCreatePayrollLoanTypeError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.loan_type_settings' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->patch(
            '/payroll_loan_type/4',
            ['company_id' => 99, 'name' => 'Fixed loan terms'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.loan_type_settings'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
