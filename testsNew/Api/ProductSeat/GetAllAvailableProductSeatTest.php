<?php

namespace TestsNew\Api\ProductSeat;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\EmploymentType\EmploymentTypeAuditService;
use App\EmploymentType\EmploymentTypeRequestService;
use App\ProductSeat\ProductSeatRequestService;
use App\Rank\RankAuditService;
use App\Rank\RankRequestService;
use App\Subscriptions\SubscriptionsRequestService;

class GetAllAvailableProductSeatTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = '/available_product_seats';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),
            'getAccountCompanies' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ]),
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

	// Controller mocks here
        $this->mockRequestService(ProductSeatRequestService::class,[
            [
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'hris'
                        ],
                        [
                            'id' => 2,
                            'name' => 'time and attendance'
                        ],
                        [
                            'id' => 3,
                            'name' => 'payroll'
                        ],
                        [
                            'id' => 4,
                            'name' => 'time and attendance plus payroll'
                        ],
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class,[
            [
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'Time & Attendance',
                            'code' => 'time and attendance',
                            'currency' => 'USD',
                            'price' => 0
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testGetAllAvailableProductSeatResponseSuccess()
    {
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetAllAvailableProductSeatResponseUnauthorized()
    {
        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),
            'getAccountCompanies' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 999
                        ]
                    ]
                ]
            ]),
        ]);


        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
