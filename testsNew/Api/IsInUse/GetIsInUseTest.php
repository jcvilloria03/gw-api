<?php

namespace TestsNew\Api\IsInUse;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\EmploymentType\EmploymentTypeAuditService;
use App\EmploymentType\EmploymentTypeRequestService;
use App\Rank\RankAuditService;
use App\Rank\RankRequestService;

class GetIsInUseTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'company/%d/is_in_use';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.companies' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

	    // Controller mocks here

        $this->mockRequestService(PhilippineCompanyRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'company 1',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'desc'
                ]
            ],
            [
                'body' => [
                    'in_use' => 0
                ]
            ]
        ]);

    }

    public function testGetIsInUseShouldResponseSuccess()
    {
        $type = 'rank';
        $payload = [
            'company_id' => 1
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, $type),
            $payload,
            [
                'x-authz-entities' => 'control_panel.companies'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetIsInUseShouldResponseUnauthorized()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.companies' => [
                            'data_scope' => [
                                'COMPANY' => [999]
                            ]
                        ]
                    ]

                ]
            ]
        ]);
        $type = 'rank';
        $payload = [
            'company_id' => 1
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, $type),
            $payload,
            [
                'x-authz-entities' => 'control_panel.companies'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
