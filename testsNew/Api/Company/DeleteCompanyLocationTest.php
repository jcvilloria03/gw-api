<?php

namespace TestsNew\Api\Company;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Location\LocationRequestService;
use App\Location\TimeAttendanceLocationRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class DeleteCompanyLocationTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1,
                    'role_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1,
                    'department_id' => 1,
                    'cost_center_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'payroll_group_id' => 1,
                    'team_id' => null,
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.locations' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2],
                        'LOCATION' => [1, 2, 3, 4],
                    ]
                ]
            ]
        ]);


        $this->mockRequestService(TimeAttendanceLocationRequestService::class, [
            ['body' => null, 'code' => Response::HTTP_NO_CONTENT]
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            ['body' => ['account_id' => 1]]
        ]);
        $this->mockRequestService(PhilippineCompanyRequestService::class, [
            ['body' => ['in_use' => 0]]
        ]);

        $this->mockClass(AuditService::class, ['log' => true,]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    protected function mockCompanyFormData(): array
    {
        return [
            'company_id' => 1,
            'location_ids' => [1, 2, 3, 4]
        ];
    }

    public function testDeleteSuccess()
    {
        $this->delete(
            '/time_attendance_locations/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteSuccessAllCompany()
    {
        $this->mockClass(AccountRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.locations' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__'],
                        'LOCATION' => ['__ALL__'],
                    ]
                ]
            ],
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->delete(
            '/time_attendance_locations/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteIncorrectCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.locations' => [
                            'data_scope' => [
                                'COMPANY' => [2, 3]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        
        $this->delete(
            '/time_attendance_locations/bulk_delete',
            $this->mockCompanyFormData(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.locations'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
