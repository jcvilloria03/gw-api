<?php

namespace TestsNew\Api\Company;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateCompanyTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class, [
            [
                'body' => [
                    'data' => null
                ]
            ]
        ]);

        $mockAuditService = Mockery::mock(AuditService::class, [
            'queue' => true
        ]);

        $this->app->instance(AuditService::class, $mockAuditService);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    public function testUpdateSuccess()
    {
        $this->mockCompanyRequestServiceCalls([
            'body' => [
                'id' => 1,
                'name' => 'Company1',
                'account_id' => 1
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.company_details'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateSuccessCompanyAll()
    {
        $this->mockCompanyRequestServiceCalls([
            'body' => [
                'id' => 1,
                'name' => 'Company1',
                'account_id' => 1
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.company_details'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateIncorrectCompany()
    {
        $this->mockCompanyRequestServiceCalls([
            'body' => [
                'id' => 1,
                'name' => 'Company1',
                'account_id' => 1
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.company_details' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/123',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.company_details'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    protected function mockCompanyRequestServiceCalls($response)
    {
        $this->mockRequestService(PhilippineCompanyRequestService::class, array_fill(0, 3, $response));
    }
}
