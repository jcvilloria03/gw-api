<?php

namespace TestsNew\Api\Company;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Location\TimeAttendanceLocationRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyTimeAndAttendanceLocationsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(TimeAttendanceLocationRequestService::class, [
            'getCompanyLocations' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'Makati City',
                            'account_id' => 1,
                            'company_id' => 1,
                            'is_headquarters' => true,
                            'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                            'location_pin' => '14.569222,121.02257199999997',
                            'first_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                            'second_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                            'city' => 'Makati City',
                            'country' => 'Philippines',
                            'region' => 'NCR',
                            'zip_code' => '1240',
                            'ip_addresses' => [],
                            'timezone' => 'gmt+8'
                        ],
                        [
                            'id' => 3,
                            'name' => 'Ortigas Pasig City',
                            'account_id' => 1,
                            'company_id' => 1,
                            'is_headquarters' => false,
                            'address_bar' => 'Emerald Ave, Ortigas Center, Pasig, Metro Manila, Philippines',
                            'location_pin' => '14.5867798,121.06162040000004',
                            'first_address_line' => 'Emerald Ave, Ortigas Center, Pasig, Metro Manila, Philippines',
                            'second_address_line' => 'Emerald Ave, Ortigas Center, Pasig, Metro Manila, Philippines',
                            'city' => 'Ortigas Pasig City',
                            'country' => 'Philippines',
                            'region' => 'NCR',
                            'zip_code' => '1230',
                            'ip_addresses' => [],
                            'timezone' => 'gmt+8'
                        ]
                    ]
                ]
            ])
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    /**
     * @dataProvider dataGetCompanyLocation
     */
    public function testGetCompanyLocationSuccess($moduleName)
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        $moduleName => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/time_attendance_locations',
            [],
            ['X-Authz-Entities' => $moduleName]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    /**
     * @dataProvider dataGetCompanyLocation
     */
    public function testGetCompanyLocationInvalidCompany($moduleName)
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        $moduleName => [
                            'data_scope' => [
                                'COMPANY' => [999]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/time_attendance_locations',
            [],
            ['X-Authz-Entities' => $moduleName]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
    
    public function dataGetCompanyLocation()
    {
        yield [
            'moduleName' => 'company_settings.company_structure.locations',
        ];
        
        yield [
            'moduleName' => 'company_settings.company_structure.locations.location',
        ];
        
        yield [
            'moduleName' => 'company_settings.company_structure.locations.ip_address',
        ];

        yield [
            'moduleName' => 'time_and_attendance.schedules',
        ];

        yield [
            'moduleName' => 'time_and_attendance.shifts',
        ];
    }
}
