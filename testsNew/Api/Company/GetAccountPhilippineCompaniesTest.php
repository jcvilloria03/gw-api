<?php

namespace TestsNew\Api\Company;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetAccountPhilippineCompaniesTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    /**
     * @dataProvider dataGetAccountPhilippineCompaniesSuccess
     */
    public function testGetAccountPhilippineCompaniesSuccess(string $module)
    {
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);
        $expected = ['data' => [
            [
                'id' => 1,
                'account_id' => 1,
                'name' => 'Company1'
            ]
        ]];
        $this->shouldExpectRequest('POST', '/account/philippine/companies')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;
        $this->json('GET', '/account/philippine/companies', [],[AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function dataGetAccountPhilippineCompaniesSuccess()
    {
        yield ['root.admin'];
        yield ['control_panel.roles'];
        yield ['control_panel.companies'];
        yield ['control_panel.users'];
        yield ['company_settings.company_structure.company_details'];
        yield ['company_settings.company_structure.company_details.company_information'];
        yield ['company_settings.company_structure.company_details.government_issued_id_number'];
        yield ['company_settings.company_structure.company_details.contact_information'];
        // Deprecated due to ticket 7702: Removal of company settings users and roles option
        // yield ['company_settings.users_and_roles.user_management'];
    }

    public function testGetAccountPhilippineCompaniesIncorrectCompany()
    {
        $module = 'root.admin';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [2]]]);
        $expected = ['data' => []];
        $this->shouldExpectRequest('POST', '/account/philippine/companies')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;
        $this->json('GET', '/account/philippine/companies', [],[AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
