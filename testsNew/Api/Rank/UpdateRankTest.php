<?php

namespace TestsNew\Api\Rank;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Rank\RankAuditService;
use App\Rank\RankEsIndexQueueService;
use App\Rank\RankRequestService;

class UpdateRankTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'rank/1';
    const TARGET_METHOD = 'PUT';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.ranks' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(RankAuditService::class, [
            'queue' => true
        ]);
        
        $this->mockClass(RankEsIndexQueueService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(RankRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'test rank',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'desc'
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'test rank',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'desc'
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'test rank',
                    'company_id' => 1,
                    'account_id' => 1,
                    'description' => 'desc'
                ]
            ],
        ]);
    }

    public function testUpdateRankShouldResponseSuccess()
    {
        $payload = [
            'company_id' => 1,
            'name' => 'test rank'
        ];
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.ranks'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateShouldResponseUnauthorized()
    {
        $payload = [
            'company_id' => 999,
            'name' => 'test rank'
        ];
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.ranks'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
