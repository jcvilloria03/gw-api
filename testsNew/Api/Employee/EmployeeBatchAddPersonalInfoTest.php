<?php

namespace TestsNew\Api\Payroll;

use Aws\S3\S3Client;
use App\Employee\EmployeeUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use Illuminate\Http\UploadedFile;
use Mockery;

class EmployeeBatchAddPersonalInfoTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUploadPersonalInfoSuccess()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('get')
            ->andReturn(null);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/personal_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Email,Telephone Number,' .
            'Mobile Number,Address Line 1,Address Line 2,Country,City,ZIP,Birthdate,Gender' .
            PHP_EOL);
        fwrite($tmpFile, 'PL001,Tracy,Bush,Myers,test+local.tracymyersbush@example.test,3432432,' .
            '(23)1232132132,1650 Penafrancia St. Brgy. Valenzuela,2 Flr,Philippines,Makati,1208,' .
            '03/06/1969,Male');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/personal_info.csv',
            'personal_info.csv',
            'text/csv',
            filesize('/tmp/personal_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/personal_info',
            [
                'company_id' => 1,
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
            $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_OK);
        $this->seeJsonStructure([
            'id'
        ]);
    }

    public function testUploadPersonalInfoUnauthorized()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [122]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('get')
            ->andReturn(null);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/personal_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Email,Telephone Number,' .
            'Mobile Number,Address Line 1,Address Line 2,Country,City,ZIP,Birthdate,Gender' .
            PHP_EOL);
        fwrite($tmpFile, 'PL001,Tracy,Bush,Myers,test+local.tracymyersbush@example.test,3432432,' .
            '(23)1232132132,1650 Penafrancia St. Brgy. Valenzuela,2 Flr,Philippines,Makati,1208,' .
            '03/06/1969,Male');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/personal_info.csv',
            'personal_info.csv',
            'text/csv',
            filesize('/tmp/personal_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/personal_info',
            [
                'company_id' => 1,
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
            $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
        $this->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
