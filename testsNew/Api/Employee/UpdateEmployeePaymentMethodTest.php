<?php

namespace TestsNew\Api\Employee;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AuthTokenTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateEmployeePaymentMethodTest extends TestCase
{
    use AuthorizationServiceTrait;
    use AuthTokenTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  1,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => 1,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUpdatePaymentMethodShouldResponseSuccess()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'TEAM' => [1]
            ]
        ]);
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => 1,
                'department_id' => 1,
                'team_id' => 1,
            ])))
        ;
        $expected = [
            'data' => [
                'type' => 'employee-payment-method',
                'id' => 1,
                'attributes' => [
                    'disbursementMethod' => 'Cash',
                    'details' => null,
                    'accountNumber' => null,
                    'active' => false
                ]
            ]
        ];
        $inputs = [
            'data' => ['type' => 'employee-payment-method', 'attributes' => ['active' => true]]
        ];
        $this
            ->shouldExpectRequest('PATCH', '/employee/1/payment_methods/1')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;


        $this
            ->json('PATCH', '/employee/1/payment_methods/1', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testUpdatePaymentMethodShouldResponseUnauthorized()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'TEAM' => [1]
            ]
        ]);
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => 1,
                'department_id' => 1,
                'team_id' => 1,
                'location_id' => 2
            ])))
        ;
        $inputs = [
            'data' => ['type' => 'employee-payment-method', 'attributes' => ['active' => true]]
        ];

        $this
            ->json('PATCH', '/employee/1/payment_methods/1', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}