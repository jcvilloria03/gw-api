<?php
namespace TestsNew\Api\Employee;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Employee\EmployeeRequestService;
use App\Employee\EmployeeUploadTask;
use App\Tasks\UploadTask;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetEmployeeBatchAddStatusTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [3],
                        'position' => [3],
                        'team' => [],
                        'location' => [1],
                        'payroll_group' => [1],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->mockClass(EmployeeRequestService::class,[
            'getUploadPreview' => $this->getJsonResponse([
                'body' => null
            ])
        ]);
        
        $this->mockClass(EmployeeUploadTask::class, [
            'create' => null,
            'fetch' => [
                EmployeeUploadTask::PROCESS_SAVE . '_status' => UploadTask::STATUS_VALIDATING,
                EmployeeUploadTask::PROCESS_SAVE . '_error_file_s3_key' => null
            ]
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/employee/batch_add/status',
            [
                'company_id' => 1,
                'job_id' => 'test:1',
                'step' => EmployeeUploadTask::PROCESS_SAVE
            ],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/employee/batch_add/status',
            [
                'company_id' => 1,
                'job_id' => 'test:1',
                'step' => EmployeeUploadTask::PROCESS_SAVE
            ],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);
        
        $this->json(
            'GET',
            '/employee/batch_add/status',
            [
                'company_id' => 1,
                'job_id' => 'test:1',
                'step' => EmployeeUploadTask::PROCESS_SAVE
            ],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
