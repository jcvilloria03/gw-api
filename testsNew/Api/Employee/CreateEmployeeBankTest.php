<?php

namespace TestsNew\Api\Employee;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateEmployeeBankTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreateEmployeeBankShouldResponseSuccess()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['TEAM' => [1], 'DEPARTMENT' => [1], 'PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);
        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => null,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $inputs = [
            'data' => ['bank_id' => 1, 'company_id' => 1, 'account_number' => '123', 'account_type' => 'SAVINGS']
        ];
        $expected = ['data' => ['type' => 'employee-payment-method', 'id' => 1, 'attributes' => []]];
        $this
            ->shouldExpectRequest('POST', '/employee/1/bank')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)))
        ;

        $this
            ->json('POST', '/employee/1/bank', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson($expected)
        ;
    }

    public function testCreateEmployeeBankShouldResponseUnauthorized()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['TEAM' => [2], 'DEPARTMENT' => [2], 'PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);
        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => null,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $inputs = [
            'data' => ['bank_id' => 1, 'company_id' => 1, 'account_number' => '123', 'account_type' => 'SAVINGS']
        ];

        $this
            ->json('POST', '/employee/1/bank', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
