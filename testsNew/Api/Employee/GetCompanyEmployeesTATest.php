<?php
namespace TestsNew\Api\Employee;

use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Request\EmployeeRequest;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetGetCompanyEmployeesTATest extends TestCase
{
    use AuthorizationServiceTrait,
        RequestServiceTrait,
        EssentialTrait,
        RequestTrait;

    protected $payload = [
        'id' => 1,
        'account_id' => 1,
        'company_id' => 1,
        'time_attendance_id' => 16066,
        'employee_id' => '0000',
        'first_name' => 'Zero',
        'middle_name' => '',
        'last_name' => 'Devzero',
        'full_name' => 'Zero Devzero',
        'email' => 'salariumqualityautomation+0000@gmail.com',
        'gender' => null,
        'birth_date' => null,
        'telephone_number' => null,
        'mobile_number' => 'NULL',
        'address_line_1' => 'Padilla',
        'address_line_2' => 'Padilla',
        'city' => 'Antipolo',
        'zip_code' => '1870',
        'country' => '',
        'hours_per_day' => null,
        'date_hired' => '2017-09-01',
        'date_ended' => null,
        'department_id' => 1,
        'department_name' => 'Sales',
        'rank_id' => null,
        'rank_name' => null,
        'employment_type_id' => 10415,
        'employment_type_name' => 'Regular',
        'position_id' => 1,
        'position_name' => 'Sales Staff',
        'team_id' => null,
        'team_name' => null,
        'team_role' => '',
        'primary_location_id' => 1,
        'primary_location_name' => 'Main Office',
        'secondary_location_id' => null,
        'secondary_location_name' => null,
        'status' => 'single',
        'timesheet_required' => true,
        'overtime' => true,
        'differential' => true,
        'regular_holiday_pay' => true,
        'special_holiday_pay' => true,
        'holiday_premium_pay' => true,
        'rest_day_pay' => true,
        'user_id' => 1,
        'termination_information' => null
    ];

    public function testResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people',
            'data_scope' => ['COMPANY' => [1]],
        ]);
        
        $this->getRequestStorage(EmployeeRequest::getStorageName())->push($this->payload);

        $this->json(
            'GET',
            '/company/1/ta_employees',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people',
            'data_scope' => ['COMPANY' => ['__ALL__']],
        ]);
        
        $this->getRequestStorage(EmployeeRequest::getStorageName())->push($this->payload);

        $this->json(
            'GET',
            '/company/1/ta_employees',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people',
            'data_scope' => ['COMPANY' => [99]],
        ]);
        
        $this->json(
            'GET',
            '/company/1/ta_employees',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 2,
                'company_id' => 2,
                'employee_id' => 2,
                'department_id' => 2,
                'cost_center_id' => 9,
                'position_id' => 2,
                'location_id' => 2,
                'payroll_group_id' => 5,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
    }
}
