<?php

namespace TestsNew\Api\Employee;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;


class GetCompanyAffectedEmployeesSearchTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetAffectedEmployeesSearchShouldResponseSuccess()
    {
        $scopes = [
            'COMPANY' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'PAYROLL_GROUP' => [1],
            'POSITION' => [1],
            'TEAM' => [1]
        ];
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.announcements',
            'data_scope' => $scopes,
        ]);

        $expected = [['id' => 1, 'name' => 'Test Employee', 'type' => 'employee']];
        $this
            ->shouldExpectRequest('GET', '/company/1/affected_employees/search')
            ->once()
            ->withQuery(['limit' => 10, 'authz_data_scope' => json_encode($scopes)])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;
            
        $this
            ->get(
                '/company/1/affected_employees/search?limit=10',
                ['x-authz-entities' => 'main_page.announcements']
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testGetAffectedEmployeesSearchShouldResponseUnauthorized()
    {
        $scopes = [
            'COMPANY' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'PAYROLL_GROUP' => [1],
            'POSITION' => [1],
            'TEAM' => [1]
        ];
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.announcements',
            'data_scope' => $scopes,
        ]);

        $this
            ->get(
                '/company/2/affected_employees/search?limit=10',
                ['x-authz-entities' => 'main_page.announcements']
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}