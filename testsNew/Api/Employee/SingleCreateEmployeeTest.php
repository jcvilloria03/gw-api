<?php

namespace TestsNew\Api\Employee;

use App\Role\DefaultRoleService;
use App\Role\UserRoleService;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class SingleCreateEmployeeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.people';
    const METHOD = 'POST';
    const URL = '/employee';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('POST', '/employee')
            ->once()
            ->withDataScope([
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'payroll_group_id' => 1,
                    'payroll_group_name' => 'Test',
                    'employee_id' => 'Pat-test-employee',
                    'first_name' => 'P',
                    'middle_name' => '',
                    'last_name' => 'Duts',
                    'email' => 'payrolltesting01+pduts@gmail.com',
                    'gender' => null,
                    'birth_date' => null,
                    'telephone_number' => null,
                    'mobile_number' => 'NULL',
                    'base_pay' => null,
                    'base_pay_unit' => null,
                    'hours_per_day' => '8.00',
                    'day_factor' => '260.00',
                    'payroll_frequency' => 'MONTHLY',
                    'date_hired' => '2020-01-15',
                    'date_ended' => null,
                    'location_id' => 1,
                    'location_name' => 'Automation',
                    'department_id' => 1,
                    'department_name' => 'Test',
                    'rank_id' => null,
                    'rank_name' => null,
                    'cost_center_id' => null,
                    'cost_center_name' => null,
                    'position_id' => 1,
                    'position_name' => 'Test',
                    'address' => [
                        'address_line_1' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                        'address_line_2' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                        'city' => 'Makati',
                        'zip_code' => '1208',
                        'country' => ''
                    ],
                    'tax_status' => 'S',
                    'tax_type' => 'Regular',
                    'consultant_tax_rate' => null,
                    'tin' => '000-000-000-000',
                    'sss_number' => '00-0000000-0',
                    'philhealth_number' => '00-000000000-0',
                    'hdmf_number' => '0000-0000-0000',
                    'sss_basis' => 'Basic Pay',
                    'philhealth_basis' => 'Basic Pay',
                    'hdmf_basis' => 'Basic Pay',
                    'sss_amount' => null,
                    'philhealth_amount' => null,
                    'hdmf_amount' => null,
                    'rdo' => '',
                    'active' => 'active',
                    'non_taxed_other_income_limit' => '90000.0000',
                    'user' => [
                        'id' => 1,
                        'account_id' => 1,
                        'last_active_company_id' => null,
                        'role_id' => 1,
                        'first_name' => 'P',
                        'middle_name' => '',
                        'last_name' => 'Duts',
                        'email' => 'payrolltesting01+pduts@gmail.com',
                        'user_type' => 'employee',
                        'deleted_at' => null,
                        'status' => 'active',
                        'suspended_inactive' => 0,
                        'account' => [
                            'id' => 1,
                            'name' => 'Owner',
                            'email' => 'salariumqualityautomation+crbac+owner@gmail.com',
                            'setup_progress' => [
                                'company' => false,
                                'user' => false,
                                'location' => false,
                                'payroll_group' => false,
                                'employee' => false,
                                'department' => false,
                                'rank' => false,
                                'cost_center' => false,
                                'position' => false,
                                'team' => false,
                                'employment_type' => false,
                                'project' => false,
                                'default_schedule' => false,
                                'company_details' => false,
                                'onboarding' => false,
                                'first_login' => true
                            ]
                        ]
                    ]
                ]
            ])))
        ;

        $this
            ->shouldExpectRequest('GET', '/product_seat/4')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], 'time and attendance plus payroll'))
        ;
        $this
            ->shouldExpectRequest('GET', '/subscriptions/1/stats')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    'total_used' => 7,
                    'total_remaining' => 23,
                    'products' => [
                        [
                            'product_id' => 1,
                            'product_code' => 'time and attendance',
                            'product_name' => 'Time & Attendance',
                            'licenses_used' => 0,
                            'licenses_available' => 0,
                            'currency' => 'USD',
                            'unit_price' => 3,
                            'amount' => 0
                        ],
                        [
                            'product_id' => 2,
                            'product_code' => 'payroll',
                            'product_name' => 'Payroll',
                            'licenses_used' => 0,
                            'licenses_available' => 0,
                            'currency' => 'USD',
                            'unit_price' => 3,
                            'amount' => 0
                        ],
                        [
                            'product_id' => 3,
                            'product_code' => 'time and attendance plus payroll',
                            'product_name' => 'Time and attendance + Payroll',
                            'licenses_used' => 7,
                            'licenses_available' => 23,
                            'currency' => 'USD',
                            'unit_price' => 5,
                            'amount' => 150
                        ]
                    ]
                ],
                'links' => [
                    'self' => '/subscriptions/1/stats'
                ]
            ])))
        ;

        $this
            ->shouldExpectRequest('GET', '/subscriptions/customers')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        [
                            'id' => 1,
                            'user_id' => 1,
                            'first_name' => 'locke',
                            'middle_name' => null,
                            'last_name' => 'lamora',
                            'account_name' => 'payrolltesting01+manual_crbac_owner@gmail.com',
                            'account_id' => 1,
                            'email' => 'payrolltesting01+manual_crbac_owner@gmail.com',
                            'credits' => 0,
                            'billing' => [
                                'first_name' => 'locke',
                                'last_name' => 'lamora',
                                'account_name' => 'payrolltesting01+manual_crbac_owner@gmail.com',
                                'email' => 'payrolltesting01+manual_crbac_owner@gmail.com',
                                'phone' => null,
                                'address_1' => 'Makati',
                                'address_2' => 'Makati',
                                'address_3' => null,
                                'city' => 'Makati',
                                'state' => 'Makati',
                                'country' => 'Philippines',
                                'zip_code' => '1208'
                            ],
                            'subscriptions' => [
                                [
                                    'id' => 1,
                                    'user_id' => 1,
                                    'plan_id' => 3,
                                    'main_product_id' => 3,
                                    'billing_period_type' => 'MONTHLY',
                                    'start_date' => '2020-07-14',
                                    'end_date' => '2020-08-12',
                                    'free_trial_end_date' => '2020-07-14',
                                    'days_left' => 24,
                                    'is_expired' => false,
                                    'is_trial' => false,
                                    'status' => 'ACTIVE',
                                    'subscription_licenses' => [
                                        [
                                            'id' => 1,
                                            'units' => 0,
                                            'currency' => 'USD',
                                            'price' => 3,
                                            'product' => [
                                                'id' => 1,
                                                'name' => 'Time & Attendance',
                                                'code' => 'time and attendance',
                                                'currency' => 'USD',
                                                'price' => 3
                                            ]
                                        ],
                                        [
                                            'id' => 1,
                                            'units' => 0,
                                            'currency' => 'USD',
                                            'price' => 3,
                                            'product' => [
                                                'id' => 2,
                                                'name' => 'Payroll',
                                                'code' => 'payroll',
                                                'currency' => 'USD',
                                                'price' => 3
                                            ]
                                        ],
                                        [
                                            'id' => 1,
                                            'units' => 30,
                                            'currency' => 'USD',
                                            'price' => 5,
                                            'product' => [
                                                'id' => 3,
                                                'name' => 'Time and attendance + Payroll',
                                                'code' => 'time and attendance plus payroll',
                                                'currency' => 'USD',
                                                'price' => 5
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'links' => [
                        'self' => '/subscriptions/customers?filter[account_id]=1'
                    ]
                ]
            )))
        ;

        $this
            ->shouldExpectRequest('GET', '/philippine/company/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1
            ])));

        $this
            ->shouldExpectRequest('GET', '/subscriptions/products')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 3,
                        'name' => 'Time and attendance + Payroll',
                        'code' => 'time and attendance plus payroll',
                        'price' => 5,
                        'currency' => 'USD'
                    ]
                ],
                'links' => [
                    'self' => '/subscriptions/products?filter[code]=time%20and%20attendance%20plus%20payroll'
                ]
            ])));
        $this
            ->shouldExpectRequest('POST', '/subscriptions/transpose/license_units')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(null)));
        $this
            ->shouldExpectRequest('POST', '/subscriptions/licenses')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(null)));

        Amqp::shouldReceive('publish');
            
        $this->mockClass(DefaultRoleService::class, [
            'getEmployeeSystemRole' => (object) [
                'id' => 1
            ]
        ]);
        $this->mockClass(UserRoleService::class, [
            'create' => true,
            'getWithConditions' => collect([])
        ]);

        $requestPayload = [
            'employee_id' => 'Pat-test-employee',
            'last_name' => 'Duts',
            'first_name' => 'P',
            'status' => 'single',
            'email' => 'payrolltesting01+pduts@gmail.com',
            'address_line_1' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
            'address_line_2' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
            'city' => 'Makati',
            'zip_code' => '1208',
            'active' => 'inactive',
            'primary_location_name' => 'Automation',
            'employment_type_name' => 'Test',
            'position_name' => 'Test',
            'department_name' => 'Test',
            'date_hired' => 'Jan-15-2020',
            'timesheet_required' => 'Yes',
            'rest_day_pay' => 'Yes',
            'regular_holiday_pay' => 'Yes',
            'holiday_premium_pay' => 'Yes',
            'special_holiday_pay' => 'Yes',
            'differential' => 'Yes',
            'overtime' => 'Yes',
            'payroll_group_name' => 'Test',
            'tax_status' => 'S',
            'tax_type' => 'Regular',
            'base_pay' => '50,000.00',
            'base_pay_unit' => 'Per Month',
            'sss_number' => '00-0000000-0',
            'tin' => '000-000-000-000',
            'hdmf_number' => '0000-0000-0000',
            'philhealth_number' => '00-000000000-0',
            'sss_basis' => 'BASIC PAY',
            'hdmf_basis' => 'BASIC PAY',
            'philhealth_basis' => 'BASIC PAY',
            'entitled_deminimis' => 'Yes',
            'company_id' => 1,
            'payment_method' => 'cash',
            'userlink_confirmed' => false,
            'product_seat_id' => 4,
            'work_location' => 'Automation'
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $requestPayload = [
            'employee_id' => 'Pat-test-employee',
            'last_name' => 'Duts',
            'first_name' => 'P',
            'status' => 'single',
            'email' => 'payrolltesting01+pduts@gmail.com',
            'address_line_1' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
            'address_line_2' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
            'city' => 'Makati',
            'zip_code' => '1208',
            'active' => 'inactive',
            'primary_location_name' => 'Automation',
            'employment_type_name' => 'Test',
            'position_name' => 'Test',
            'department_name' => 'Test',
            'date_hired' => 'Jan-15-2020',
            'timesheet_required' => 'Yes',
            'rest_day_pay' => 'Yes',
            'regular_holiday_pay' => 'Yes',
            'holiday_premium_pay' => 'Yes',
            'special_holiday_pay' => 'Yes',
            'differential' => 'Yes',
            'overtime' => 'Yes',
            'payroll_group_name' => 'Test',
            'tax_status' => 'S',
            'tax_type' => 'Regular',
            'base_pay' => '50,000.00',
            'base_pay_unit' => 'Per Month',
            'sss_number' => '00-0000000-0',
            'tin' => '000-000-000-000',
            'hdmf_number' => '0000-0000-0000',
            'philhealth_number' => '00-000000000-0',
            'sss_basis' => 'BASIC PAY',
            'hdmf_basis' => 'BASIC PAY',
            'philhealth_basis' => 'BASIC PAY',
            'entitled_deminimis' => 'Yes',
            'company_id' => 999,
            'payment_method' => 'cash',
            'userlink_confirmed' => false,
            'product_seat_id' => 4,
            'work_location' => 'Automation'
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
