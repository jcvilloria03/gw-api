<?php

namespace TestsNew\Api\Employee;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeleteEmployeePaymentMethodsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testDeleteEmployeePaymentMethodsShouldResponseSuccess()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['TEAM' => [1], 'DEPARTMENT' => [1], 'PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);
        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => null,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $inputs = ['data' => ['ids' => [1]]];
        $expected = [['id' => 1]];
        $this
            ->shouldExpectRequest('DELETE', '/employee/1/payment_method')
            ->once()
            ->withBody(json_encode(array_merge_recursive($inputs, ['data' => ['guard' => true]])))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this
            ->json('DELETE', '/employee/1/payment_method', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testDeleteEmployeePaymentMethodsShouldResponseUnauthorized()
    {
        $module = 'employees.people.payment_methods';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['TEAM' => [2], 'DEPARTMENT' => [2], 'PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);
        $employee = [
            'id' => 1,
            'company_id' => 1,
            'department_id' => 1,
            'team_id' => 1,
            'payroll_group' => ['id' => 1],
            'location_id' => null,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $inputs = ['data' => ['ids' => [1]]];

        $this
            ->json('DELETE', '/employee/1/payment_method', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
