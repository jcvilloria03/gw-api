<?php

namespace TestsNew\Api\Employee;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Auth0\Auth0UserService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeESIndexQueueService;
use App\Employee\EmployeeRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\ProductSeat\ProductSeatService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateTAPayrollEmployeeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployeeInfo' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ]),

            'updateTAPayrollEmployee' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(PhilippineCompanyRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1
                ]
            ])
        ]);

        $this->mockClass(ProductSeatService::class, [
            'getProductSeatName' => 'test'
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUserByEmployeeId' => null
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(EmployeeESIndexQueueService::class, [
            'queue' => true
        ]);
    }

    public function testGetEmployeeBasicPayAdjustmentsSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.basic_information' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/employee/1', [
            'product_seat_id' => 1
        ],
        [
            'x-authz-entities' => 'employees.people.basic_information'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetEmployeeBasicPayAdjustmentsUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.basic_information' => [
                    'data_scope' => [
                        'COMPANY' => [123],
                        'DEPARTMENT' => [456],
                        'POSITION' => [789],
                        'LOCATION' => [987],
                        'TEAM' => [654],
                        'PAYROLL_GROUP' => [321]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/employee/1', [
            'product_seat_id' => 1
        ],
        [
            'x-authz-entities' => 'employees.people.basic_information'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
