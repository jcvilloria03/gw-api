<?php
namespace TestsNew\Api\Employee;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Employee\EmployeeRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetPhilippineEmployeeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->deleteAccountEssentialDataCache();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [3],
                        'position' => [3],
                        'team' => [],
                        'location' => [1],
                        'payroll_group' => [1],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'POSITION' => [3],
                        'TEAM' => ['__ALL__'],
                        'PAYROLL_GROUP' => [1],
                        'LOCATION' => [1],
                        'DEPARTMENT' => [3],
                    ]
                ]
            ]
        ]);
        
        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 'EMP000',
                    'first_name' => 'Monthly Emp A',
                    'middle_name' => '',
                    'last_name' => 'Scenario one',
                    'email' => 'salqateam01+EMP000@gmail.com',
                    'gender' => 'male',
                    'birth_date' => '1984-10-10',
                    'telephone_number' => 4425158,
                    'mobile_number' => '(63)9954132491',
                    'base_pay' => null,
                    'base_pay_unit' => null,
                    'hours_per_day' => '8.00',
                    'day_factor' => '260.00',
                    'payroll_frequency' => 'MONTHLY',
                    'date_hired' => '2010-10-16',
                    'date_ended' => null,
                    'location_id' => 1,
                    'location_name' => 'Makati Branch',
                    'department_id' => 3,
                    'department_name' => 'HR',
                    'rank_id' => 3,
                    'rank_name' => 'patsi',
                    'cost_center_id' => 3,
                    'cost_center_name' => 'Updated Product',
                    'position_id' => 3,
                    'position_name' => 'Admin/Purchasing',
                    'address' => [
                        'address_line_1' => '1st  Street',
                        'address_line_2' => 'Brgy 12',
                        'city' => 'Marilao',
                        'zip_code' => '14500',
                        'country' => 'Philippines'
                    ],
                    'tax_status' => 'S',
                    'tax_type' => 'Regular',
                    'consultant_tax_rate' => null,
                    'tin' => '131-456-789-019',
                    'sss_number' => '07-7451477-8',
                    'philhealth_number' => '82-560045513-0',
                    'hdmf_number' => '4177-3254-4805',
                    'sss_basis' => 'Basic Pay',
                    'philhealth_basis' => 'Basic Pay',
                    'hdmf_basis' => 'Basic Pay',
                    'sss_amount' => null,
                    'philhealth_amount' => null,
                    'hdmf_amount' => null,
                    'rdo' => '014',
                    'active' => 'inactive',
                    'non_taxed_other_income_limit' => '90000.0000',
                    "payroll_group" => [
                        "id" => 1,
                        "name" => "Test"
                    ]
                ]
            ]),
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/philippine/employee/1',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__'],
                        'POSITION' => ['__ALL__'],
                        'TEAM' => ['__ALL__'],
                        'PAYROLL_GROUP' => ['__ALL__'],
                        'LOCATION' => ['__ALL__'],
                        'DEPARTMENT' => ['__ALL__'],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/philippine/employee/1',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [2],
                        'POSITION' => [2],
                        'TEAM' => [2],
                        'PAYROLL_GROUP' => [2],
                        'LOCATION' => [2],
                        'DEPARTMENT' => [2],
                    ]
                ]
            ]
        ]);
        
        $this->json(
            'GET',
            '/philippine/employee/1',
            [],
            [
                'x-authz-entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function deleteAccountEssentialDataCache()
    {
        $redisKey = Config::get('account_essential.redis_cache_key');
        
        Redis::del(sprintf($redisKey, 1));
    }

    public function tearDown()
    {
        $this->deleteAccountEssentialDataCache();
        parent::tearDown();
    }
}
