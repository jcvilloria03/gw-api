<?php

namespace TestsNew\Api\Payroll;

use Aws\S3\S3Client;
use App\Employee\EmployeeUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use Illuminate\Http\UploadedFile;
use Mockery;

class EmployeeBatchAddPayrollInfoTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUploadPayrollInfoSuccess()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('hGetAll')
            ->andReturn([
                's3_bucket' => 'salarium-v3-test-development-uploads',
                'personal_s3_key' => 'employee_upload:1:59c9eb5fc6201',
                'personal_status' => 'validated',
            ]);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/payroll_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Tax Type,Consultant Tax ' .
            'Rate,Tax Status,Base Pay,Base Pay Unit,Date hired,Termination Date,Hours per Day,' .
            'Payroll group,Employment Type,Position,Department,Rank,Cost Center,Payment Method,' .
            'Bank Name,Bank Type,Bank Account No.,SSS Basis,SSS Amount,PhilHealth Basis,' .
            'PhilHealth Amount,HDMF Basis,HDMF Amount,Primary Location,SSS Number,TIN,RDO,HDMF ' .
            'Number,PhilHealth Number,Entitled Deminimis' . PHP_EOL);
        fwrite($tmpFile, 'PL001,Tracy,Bush,Myers,Regular,,S,50000,Per Month,01/01/2005,,8,SM PG1,' .
            'Regular,Sales Associate,Sales,Rank and File ,cc1,Cash,,,,Basic Pay,,Basic Pay,,Basic' .
            ' Pay,,HQ,04-7451477-7,124-741-586,47,4177-3254-4786,74-560045512-6,Yes');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/payroll_info.csv',
            'payroll_info.csv',
            'text/csv',
            filesize('/tmp/payroll_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/payroll_info',
            [
                'company_id' => 1,
                'job_id' => 'employee_upload:1:59c9eb5fc6201',
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
        $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_OK);
        $this->seeJsonStructure([
            'id'
        ]);
    }

    public function testUploadPayrollInfoUnauthorized()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [12323]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('hGetAll')
            ->andReturn([
                's3_bucket' => 'salarium-v3-test-development-uploads',
                'personal_s3_key' => 'employee_upload:1:59c9eb5fc6201',
                'personal_status' => 'validated',
            ]);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/payroll_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Tax Type,Consultant Tax ' .
            'Rate,Tax Status,Base Pay,Base Pay Unit,Date hired,Termination Date,Hours per Day,' .
            'Payroll group,Employment Type,Position,Department,Rank,Cost Center,Payment Method,' .
            'Bank Name,Bank Type,Bank Account No.,SSS Basis,SSS Amount,PhilHealth Basis,' .
            'PhilHealth Amount,HDMF Basis,HDMF Amount,Primary Location,SSS Number,TIN,RDO,HDMF ' .
            'Number,PhilHealth Number,Entitled Deminimis' . PHP_EOL);
        fwrite($tmpFile, 'PL001,Tracy,Bush,Myers,Regular,,S,50000,Per Month,01/01/2005,,8,SM PG1,' .
            'Regular,Sales Associate,Sales,Rank and File ,cc1,Cash,,,,Basic Pay,,Basic Pay,,Basic' .
            ' Pay,,HQ,04-7451477-7,124-741-586,47,4177-3254-4786,74-560045512-6,Yes');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/payroll_info.csv',
            'payroll_info.csv',
            'text/csv',
            filesize('/tmp/payroll_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/payroll_info',
            [
                'company_id' => 1,
                'job_id' => 'employee_upload:1:59c9eb5fc6201',
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
            $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
        $this->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
