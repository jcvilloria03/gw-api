<?php
namespace TestsNew\Api\Employee;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Employee\EmployeeRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetEmployeeUserMatchTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->deleteAccountEssentialDataCache();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);
        
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ],
            'getUserByEmail' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'last_active_company_id' => null,
                    'role_id' => null,
                    'first_name' => 'Monthly Emp A',
                    'middle_name' => '',
                    'last_name' => 'Scenario one',
                    'email' => 'salqateam01+EMP000@gmail.com',
                    'user_type' => 'employee',
                    'created_at' => '2019-06-14 16:18:03',
                    'updated_at' => '2019-06-14 16:18:03',
                    'deleted_at' => null,
                    'status' => 'inactive',
                    'suspended_inactive' => 0
                ]
            ]),
        ]);
    }

    protected function getRequestPayload()
    {
        return [
            'company_id' => 1,
            'first_name' => 'Monthly Emp A',
            'middle_name' => '',
            'last_name' => 'Scenario one',
            'email' => 'salqateam01+EMP000@gmail.com'
        ];
    }

    public function testResponseSuccess()
    {
        $this->json(
            'POST',
            'employee/has_usermatch',
            $this->getRequestPayload(),
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'employee/has_usermatch',
            $this->getRequestPayload(),
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'employees.people'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people' => [
                    'data_scope' => [
                        'COMPANY' => [99]
                    ]
                ]
            ]
        ]);
        
        $this->json(
            'POST',
            'employee/has_usermatch',
            $this->getRequestPayload(),
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'employees.people'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function deleteAccountEssentialDataCache()
    {
        $redisKey = Config::get('account_essential.redis_cache_key');
        
        Redis::del(sprintf($redisKey, 1));
    }

    public function tearDown()
    {
        $this->deleteAccountEssentialDataCache();
        parent::tearDown();
    }
}
