<?php

namespace TestsNew\Api\Team;

use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Employee\EmployeeRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetEmployeeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployeeInfo' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 2,
                    'department_id' => 3,
                    'position_id' => 4,
                    'location_id' => 5,
                    'time_attendance' => [
                        'team_id' => 6
                    ],
                    'payroll' => [
                        'payroll_group_id' => 7
                    ]
                ]
            ])
        ]);
    }

    public function testGetEmployeeSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.basic_information' => [
                    'data_scope' => [
                        'COMPANY' => [2],
                        'DEPARTMENT' => [3],
                        'POSITION' => [4],
                        'LOCATION' => [5],
                        'TEAM' => [6],
                        'PAYROLL_GROUP' => [7],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/employee/1',
            [],
            [
                'x-authz-entities' => 'employees.people.basic_information'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetEmployeeError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.basic_information' => [
                    'data_scope' => [
                        'COMPANY' => [2],
                        'DEPARTMENT' => [3],
                        'POSITION' => [999],
                        'LOCATION' => [5],
                        'TEAM' => [6],
                        'PAYROLL_GROUP' => [7],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/employee/1',
            [],
            [
                'x-authz-entities' => 'employees.people.basic_information'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
