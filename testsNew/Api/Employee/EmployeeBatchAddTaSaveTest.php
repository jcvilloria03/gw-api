<?php

namespace TestsNew\Api\Employee;

use App\Employee\EmployeeUploadTask;
use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class EmployeeBatchAddTaSaveTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testEmployeeBatchAddShouldResponseSuccess()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);
        $this->shouldExpectRequest('GET', '/philippine/company/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1])))
        ;
        $statusKey = EmployeeUploadTask::PROCESS_TIME_ATTENDANCE . '_status';
        Redis::shouldReceive('hGetAll')
            ->once()
            ->andReturn([$statusKey => EmployeeUploadTask::STATUS_VALIDATED])
        ;
        Redis::shouldReceive('hSet')->twice();

        $this->json(
            'POST',
            '/employee/batch_add/ta_save',
            ['company_id' => 1, 'job_id' => EmployeeUploadTask::ID_PREFIX . '1'],
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson(['id' => 'employee_upload:1'])
        ;
    }

    public function testEmployeeBatchAddShouldResponseUnauthorized()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);

        $this->json(
            'POST',
            '/employee/batch_add/ta_save',
            ['company_id' => 2],
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
