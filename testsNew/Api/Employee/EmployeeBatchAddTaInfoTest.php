<?php

namespace TestsNew\Api\Payroll;

use Aws\S3\S3Client;
use App\Employee\EmployeeUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use Illuminate\Http\UploadedFile;
use Mockery;

class EmployeeBatchAddTaInfoTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUploadTaInfoSuccess()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['TEAM' => [1], 'COMPANY' => [1]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('del');
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('hGetAll')
            ->andReturn([
                's3_bucket' => 'salarium-v3-test-development-uploads',
                'personal_s3_key' => 'employee_upload:1:59c9eb5fc6201',
                'personal_status' => 'validated',
            ]);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/time_attendance_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Hours Per Day,Status,' .
            'Date Hired,Termination Date,Employment Type,Position,Department,Rank,Team,Team Role,' .
            'Primary Location,Secondary Location,Timesheet Required?,Entitled to Overtime?,' .
            'Entitled Night Differential?,Entitled Unworked Regular Holiday Pay?,' .
            'Entitled Unworked Special Holiday Pay?,Entitled Holiday Premium Pay?,' .
            'Entitled Rest Day Pay?' . PHP_EOL);
        fwrite($tmpFile, 'JP001,Cody,Hardy,Hudson,8,Single,Jan-01-2005,,Regular,position1,' .
            'Engineering,rank1,,,HQ,,Yes,Yes,Yes,Yes,Yes,Yes,Yes');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/time_attendance_info.csv',
            'time_attendance_info.csv',
            'text/csv',
            filesize('/tmp/time_attendance_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/time_attendance_info',
            [
                'company_id' => 1,
                'job_id' => 'employee_upload:1:59c9eb5fc6201',
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
            $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_OK);
        $this->seeJsonStructure([
            'id'
        ]);
    }

    public function testUploadTaInfoUnauthorized()
    {
        $module = 'employees.people';
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people',
            'data_scope' => ['TEAM' => [1], 'COMPANY' => [12222]],
        ]);

        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('set');
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('hGetAll')
            ->andReturn([
                's3_bucket' => 'salarium-v3-test-development-uploads',
                'personal_s3_key' => 'employee_upload:1:59c9eb5fc6201',
                'personal_status' => 'validated',
            ]);

        Amqp::shouldReceive('publish');

        $mockS3Client = Mockery::mock(S3Client::class);
        $mockS3Client->shouldReceive('putObject');

        $this->app->instance(EmployeeUploadTask::class, new EmployeeUploadTask($mockS3Client));

        $tmpFile = fopen('/tmp/time_attendance_info.csv', 'w');
        fwrite($tmpFile, 'Employee ID,First Name,Last Name,Middle Name,Hours Per Day,Status,' .
            'Date Hired,Termination Date,Employment Type,Position,Department,Rank,Team,Team Role,' .
            'Primary Location,Secondary Location,Timesheet Required?,Entitled to Overtime?,' .
            'Entitled Night Differential?,Entitled Unworked Regular Holiday Pay?,' .
            'Entitled Unworked Special Holiday Pay?,Entitled Holiday Premium Pay?,' .
            'Entitled Rest Day Pay?' . PHP_EOL);
        fwrite($tmpFile, 'JP001,Cody,Hardy,Hudson,8,Single,Jan-01-2005,,Regular,position1,' .
            'Engineering,rank1,,,HQ,,Yes,Yes,Yes,Yes,Yes,Yes,Yes');
        fclose($tmpFile);

        $mockUploadedFile = new UploadedFile(
            '/tmp/time_attendance_info.csv',
            'time_attendance_info.csv',
            'text/csv',
            filesize('/tmp/time_attendance_info.csv') / 1000,
            null,
            true
        );

        $this->call(
            'POST',
            '/employee/batch_add/time_attendance_info',
            [
                'company_id' => 1,
                'job_id' => 'employee_upload:1:59c9eb5fc6201',
            ],
            [],
            [
                'file' => $mockUploadedFile,
            ],
            $this->transformHeadersToServerVars([
                'X-Authz-Entities' => $module,
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
        $this->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
