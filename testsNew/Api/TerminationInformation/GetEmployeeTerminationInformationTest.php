<?php

namespace TestsNew\Api\TerminationInformation;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\FinalPay\FinalPayRequestService;
use App\Termination\TerminationInformationRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetEmployeeTerminationInformationTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(TerminationInformationRequestService::class, [
            'getByEmployeeId' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        'attributes' => [
                            'company_id' => 1
                        ]
                    ]
                ]
            ])
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'location_id' => 1,
                    'department_id' => 3,
                    'position_id' => 3,
                    'team_id' => 3,
                    'payroll_group' => ['id' => 1],
                ]
            ]),
        ]);
    
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.termination_information' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [1],
                        'DEPARTMENT' => [3],
                        'LOCATION' => [1],
                        'POSITION' => [3],
                        'TEAM' => [3],
                    ]
                ]
            ]
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/employee/1/termination_informations',
            [],
            ['X-Authz-Entities' => 'employees.people.termination_information']
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.termination_information' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [99],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/employee/1/termination_informations',
            [],
            ['X-Authz-Entities' => 'employees.people.termination_information']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
