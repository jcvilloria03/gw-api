<?php

namespace TestsNew\Api\TerminationInformation;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateTerminationInformationTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreateTerminationInformationShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people.termination_information',
            'data_scope' => ['TEAM' => [1], 'DEPARTMENT' => [1], 'PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        $expected = ['data' => ['id' => 1, 'attributes' => ['employee_id' => 1, 'company_id' => 1]]];
        $terminationRequest = ['employee_id' => 1, 'company_id' => 1];
        $employee = [
            'data' => ['id' => 1, 'company_id' => 1, 'payroll_group' => 1, 'department_id' => 1, 'team_id' => 1],
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;
        $this
            ->shouldExpectRequest('POST', '/termination_informations')
            ->once()
            ->withBody(http_build_query($terminationRequest))
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)))
        ;
        $this
            ->shouldExpectRequest('GET', '/termination_informations/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)))
        ;

        $this
            ->json(
                'POST',
                '/termination_informations',
                $terminationRequest,
                ['X-Authz-Entities' => 'employees.people.termination_information']
            )
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson($expected)
        ;
    }

    public function testCreateTerminationInformationShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people.termination_information',
            'data_scope' => ['TEAM' => [2], 'DEPARTMENT' => [2], 'PAYROLL_GROUP' => [2], 'COMPANY' => [2]],
        ]);

        $terminationRequest = ['employee_id' => 1, 'company_id' => 1];
        $employee = [
            'data' => ['id' => 1, 'company_id' => 1, 'payroll_group' => 1, 'department_id' => 1, 'team_id' => 1],
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->json(
                'POST',
                '/termination_informations',
                $terminationRequest,
                ['X-Authz-Entities' => 'employees.people.termination_information']
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testCreateTerminationInformationShouldResponseInvalid()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.people.termination_information',
            'data_scope' => ['TEAM' => [2], 'DEPARTMENT' => [2], 'PAYROLL_GROUP' => [2], 'COMPANY' => [2]],
        ]);

        $this
            ->json(
                'POST',
                '/termination_informations',
                [],
                ['X-Authz-Entities' => 'employees.people.termination_information']
            )
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson([
                'message' => 'The employee id field is required.',
                'status_code' => HttpResponse::HTTP_NOT_ACCEPTABLE,
            ])
        ;
    }
}
