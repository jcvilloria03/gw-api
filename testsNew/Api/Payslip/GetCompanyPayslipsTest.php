<?php

namespace TestsNew\Api\Payslip;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCompanyPayslipsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.payslips';
    const TARGET_METHOD = 'GET';
    const TARGET_URL = '/company/1/payslips';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('hExists');
        Amqp::shouldReceive('publish');
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $payrollGroupData = [
            "data" => [
                [
                    "id" => 1,
                    "account_id" => 1,
                    "company_id" => 1,
                    "name" => "pr1"
                ],
                [
                    "id" => 2,
                    "account_id" => 1,
                    "company_id" => 1,
                    "name" => "pr2"
                ]
            ]
        ];

        $requestPayload = [
            'payroll_group_ids' => "1,2",
            'date_from' => '2019-01-01',
            'date_to' => '2019-01-31',
            'include_special' => false
        ];

        $payslipsData = [
            'data' => [
                [
                    'payslip_id' => 1,
                    'employee_id' => 1,
                    'company_employee_id' => 'tapr_jan2020-12',
                    'employee_name' => 'Semi MWE, Bigbang',
                    'payroll_group_name' => '2020_Semi-Monthly',
                    'payroll_type' => 'Regular Pay',
                    'payroll_status' => 'CLOSED',
                    'payroll_id' => 1,
                    'payroll_date' => '2019-01-31',
                    'status' => 'unread',
                    'first_viewed_at' => null
                ],
                [
                    'payslip_id' => 2,
                    'employee_id' => 3,
                    'company_employee_id' => 'tapr_jan2020-12',
                    'employee_name' => 'Semi MWE, Bigbang',
                    'payroll_group_name' => '2020_Semi-Monthly',
                    'payroll_type' => 'Regular Pay',
                    'payroll_status' => 'CLOSED',
                    'payroll_id' => 1,
                    'payroll_date' => '2019-01-31',
                    'status' => 'unread',
                    'first_viewed_at' => null
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/company/1/payroll_groups')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollGroupData)));

        $this
            ->shouldExpectRequest('GET', '/payslips/search')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payslipsData)));

        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $payrollGroupData = [
            "data" => [
                [
                    "id" => 1,
                    "account_id" => 1,
                    "company_id" => 1,
                    "name" => "pr1"
                ],
                [
                    "id" => 2,
                    "account_id" => 1,
                    "company_id" => 1,
                    "name" => "pr2"
                ]
            ]
        ];

        $requestPayload = [
            'payroll_group_ids' => "1,2,3",
            'date_from' => '2019-01-01',
            'date_to' => '2019-01-31',
            'include_special' => false
        ];

        $this
            ->shouldExpectRequest('GET', '/company/1/payroll_groups')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollGroupData)));

        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
