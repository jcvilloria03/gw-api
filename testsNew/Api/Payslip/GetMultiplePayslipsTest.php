<?php

namespace TestsNew\Api\Payslip;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetMultiplePayslipsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.payslips';
    const TARGET_METHOD = 'POST';
    const TARGET_URL = '/payslip/download_multiple';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Redis::shouldReceive('hExists');
        Amqp::shouldReceive('publish');
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $payslipsData = [
            'data' => [
                [
                    'id' => 1,
                    'employee_id' => 1,
                    'payroll_id' => 1,
                    'payroll_employee_id' => 1,
                    'payroll_group_id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'first_viewed_at' => null
                ],
                [
                    'id' => 2,
                    'employee_id' => 3,
                    'payroll_id' => 1,
                    'payroll_employee_id' => 2,
                    'payroll_group_id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'first_viewed_at' => null
                ]
            ]
        ];

        $payslipDownloadData = [
            "is_email" => true,
            "email_address" => "email@example.com",
            "id" => null
        ];
        
        $requestPayload = [
            "payslip_ids" => "1,2"
        ];

        $this
            ->shouldExpectRequest('POST', '/payslip/get_multiple')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payslipsData)));
        
        $this
            ->shouldExpectRequest('POST', '/payslip/download_multiple')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payslipDownloadData)));
        
        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [99],
                'PAYROLL_GROUP' => [99],
                'POSITION' => [99],
                'DEPARTMENT' => [99],
                'TEAM' => [99],
                'LOCATION' => [99],
            ],
        ]);

        $payslipsData = [
            'data' => [
                [
                    'id' => 1,
                    'employee_id' => 1,
                    'payroll_id' => 1,
                    'payroll_employee_id' => 1,
                    'payroll_group_id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'first_viewed_at' => null
                ],
                [
                    'id' => 2,
                    'employee_id' => 3,
                    'payroll_id' => 1,
                    'payroll_employee_id' => 2,
                    'payroll_group_id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'first_viewed_at' => null
                ]
            ]
        ];

        $requestPayload = [
            "payslip_ids" => "1,2"
        ];


        $this
            ->shouldExpectRequest('POST', '/payslip/get_multiple')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payslipsData)));

        $this
            ->json(self::TARGET_METHOD, self::TARGET_URL, $requestPayload, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
