<?php

namespace TestsNew\Api\WorkflowEntitlement;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\WorkflowEntitlement\WorkflowEntitlementRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class HasPendingRequestTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(WorkflowEntitlementRequestService::class, [
            'checkEmployeeHasPendingRequests' => $this->getJsonResponse([
                'body' => []
            ])
        ]);
    }

    public function testHasPendingRequestSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.loans' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/employee/1/workflow_entitlement/has_pending_requests',
            [],
            [
                'X-Authz-Entities' => 'employees.people.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testHasPendingRequestUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.loans' => [
                    'data_scope' => [
                        'COMPANY' => [999],
                        'DEPARTMENT' => [999],
                        'POSITION' => [999],
                        'LOCATION' => [999],
                        'TEAM' => [999],
                        'PAYROLL_GROUP' => [999]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/employee/1/workflow_entitlement/has_pending_requests',
            [],
            [
                'X-Authz-Entities' => 'employees.people.loans'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
