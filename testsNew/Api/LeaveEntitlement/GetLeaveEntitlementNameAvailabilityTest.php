<?php

namespace TestsNew\Api\LeaveEntitlement;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetLeaveEntitlementNameAvailabilityTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.leave_settings.leave_entitlements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(LeaveEntitlementRequestService::class, [
            [
                'body' => [
                    'available' => true
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    protected function getRequestPayload()
    {
        return [
            'name' => 'Sick Leave Entitlement'
        ];
    }
    
    public function testGetLeaveEntitlementNameAvailabilitySuccess()
    {
        $this->json(
            'POST',
            '/company/1/leave_entitlement/is_name_available',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetLeaveEntitlementNameAvailabilityAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/1/leave_entitlement/is_name_available',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetLeaveEntitlementNameAvailabilityCompanyIdMismatch()
    {
        $this->json(
            'POST',
            '/company/4/leave_entitlement/is_name_available',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
