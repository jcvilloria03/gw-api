<?php

namespace TestsNew\Api\LeaveEntitlement;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyLeaveEntitlementsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.leave_settings.leave_entitlements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(LeaveEntitlementRequestService::class, [
            [
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'No SL Conversion',
                            'company_id' => 1,
                            'accrue_leave_credits' => 15,
                            'leave_credit_unit' => 'Days',
                            'accrue_every' => 1,
                            'accrual_period' => 'Monthly',
                            'start_accruing_after' => 6,
                            'after_accrual_period' => 'Months',
                            'leave_conversion_run' => 'Annual',
                            'leave_conversion_type' => 'Carry Over the Next Period',
                            'accrue_date' => null,
                            'termination_leave_conversion_type' => 'Forfeit',
                            'leave_conversion_run_date' => null,
                            'affected_employees' => [
                                [
                                    'id' => null,
                                    'name' => 'All employees',
                                    'type' => 'employee'
                                ]
                            ],
                            'leave_types' => [
                                [
                                    'id' => 1,
                                    'name' => 'Sickness Leave',
                                    'leave_credit_required' => false,
                                    'payable' => false
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    public function testGetCompanyLeaveEntitlementsSuccess()
    {
        $this->json(
            'GET',
            '/company/1/leave_entitlements',
            [],
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetCompanyLeaveEntitlementsAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/leave_entitlements',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetCompanyLeaveEntitlementsCompanyIdMismatch()
    {
        $this->json(
            'GET',
            '/company/4/leave_entitlements',
            [],
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
