<?php

namespace TestsNew\Api\LeaveEntitlement;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Facades\Company;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class UpdateLeaveEntitlementTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.leave_settings.leave_entitlements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(LeaveEntitlementRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Entitlement',
                    'company_id' => 1,
                    'accrue_leave_credits' => 1,
                    'leave_credit_unit' => 'Days',
                    'accrue_every' => 1,
                    'accrual_period' => 'Monthly',
                    'start_accruing_after' => 1,
                    'after_accrual_period' => 'Months',
                    'leave_conversion_run' => 'Specific date',
                    'leave_conversion_type' => 'Forfeit',
                    'accrue_date' => '2020-06-01',
                    'termination_leave_conversion_type' => 'Forfeit',
                    'leave_conversion_run_date' => '2020-06-01',
                    'affected_employees' => [
                        [
                            'id' => null,
                            'name' => 'All employees',
                            'type' => 'employee'
                        ]
                    ],
                    'leave_types' => [
                        [
                            'id' => 2,
                            'name' => 'Sick Leaves',
                            'leave_credit_required' => true,
                            'payable' => true
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Regular Vacation',
                    'company_id' => 1,
                    'accrue_leave_credits' => 15,
                    'leave_credit_unit' => 'Days',
                    'accrue_every' => 1,
                    'accrual_period' => 'Monthly',
                    'start_accruing_after' => 6,
                    'after_accrual_period' => 'Months',
                    'leave_conversion_run' => 'Annual',
                    'leave_conversion_type' => 'Carry Over the Next Period',
                    'accrue_date' => null,
                    'termination_leave_conversion_type' => 'Forfeit',
                    'leave_conversion_run_date' => null,
                    'affected_employees' => [
                        [
                            'id' => null,
                            'name' => 'All employees',
                            'type' => 'employee'
                        ]
                    ],
                    'leave_types' => [
                        [
                            'id' => 1,
                            'name' => 'Vacation Leave',
                            'leave_credit_required' => true,
                            'payable' => true
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Regular Vacation',
                    'company_id' => 1,
                    'accrue_leave_credits' => 15,
                    'leave_credit_unit' => 'Days',
                    'accrue_every' => 1,
                    'accrual_period' => 'Monthly',
                    'start_accruing_after' => 6,
                    'after_accrual_period' => 'Months',
                    'leave_conversion_run' => 'Annual',
                    'leave_conversion_type' => 'Carry Over the Next Period',
                    'accrue_date' => null,
                    'termination_leave_conversion_type' => 'Forfeit',
                    'leave_conversion_run_date' => null,
                    'affected_employees' => [
                        [
                            'id' => null,
                            'name' => 'All employees',
                            'type' => 'employee'
                        ]
                    ],
                    'leave_types' => [
                        [
                            'id' => 1,
                            'name' => 'Vacation Leave',
                            'leave_credit_required' => true,
                            'payable' => true
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        Company::shouldReceive('getAccountId')->andReturn(1);
    }

    protected function getRequestPayload()
    {
        return [
            'name' => 'Regular Vacation',
            'company_id' => 1,
            'accrue_leave_credits' => 15,
            'leave_credit_unit' => 'Days',
            'accrue_every' => 1,
            'accrual_period' => 'Monthly',
            'start_accruing_after' => 6,
            'after_accrual_period' => 'Months',
            'leave_conversion_run' => 'Annual',
            'leave_conversion_type' => 'Carry Over the Next Period',
            'accrue_date' => null,
            'termination_leave_conversion_type' => 'Forfeit',
            'leave_conversion_run_date' => null,
            'affected_employees' => [
                [
                    'id' => null,
                    'type' => 'employee'
                ]
            ],
            'leave_type_ids' => [1]
        ];
    }

    public function testUpdateLeaveEntitlementSuccess()
    {
        $this->json(
            'PUT',
            '/leave_entitlement/1',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testUpdateLeaveEntitlementAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_entitlements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PUT',
            '/leave_entitlement/1',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testUpdateLeaveEntitlementCompanyIdMismatch()
    {
        $payload = $this->getRequestPayload();
        $payload['company_id'] = 21;
        
        $this->json(
            'PUT',
            '/leave_entitlement/1',
            $payload,
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_entitlements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
