<?php

namespace TestsNew\Api\Bank;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Bank\BankRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class CreateCompanyBankTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.disbursements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(BankRequestService::class, [
            [
                'body' => [
                    "data" => [
                        'data' => [
                            'type' => 'company-bank-account',
                            'id' => '1',
                            'attributes' => [
                                'bankBranch' => 'Angono',
                                'accountNumber' => '101010101010',
                                'accountType' => 'SAVINGS',
                                'companyCode' => '097',
                                'bank' => [
                                    'id' => '1',
                                    'name' => 'AL-AMANAH ISLAMIC INVESTMENT BANK OF THE PHILIPPINES'
                                ]
                            ]
                        ]
                    ]
                ],
                'code' => 204
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    protected function getRequestPayload(): array
    {
        return [
            'data' => [
                'type' => 'company-bank-account',
                'attributes' => [
                    'bankBranch' => 'Angono',
                    'accountNumber' => '101010101010',
                    'accountType' => 'SAVINGS',
                    'companyCode' => '097'
                ]
            ]
        ];
    }
    
    public function testCreateCompanyBankSuccess()
    {
        $this->json(
            'POST',
            '/company/1/bank',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_NO_CONTENT);
    }

    public function testCreateCompanyBankAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            '/company/1/bank',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_NO_CONTENT);
    }

    public function testCreateCompanyBankCompanyIdMismatch()
    {
        $this->json(
            'POST',
            '/company/7/bank',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
