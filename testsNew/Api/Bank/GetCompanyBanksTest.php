<?php

namespace TestsNew\Api\Bank;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Bank\BankRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyBanksTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(PhilippineCompanyRequestService::class, [
            'get' => [
                'id' => 1,
                'name' => 'First Company',
                'account_id' => 1,
                'logo' => 'https://www.example.com/logo.png',
                'email' => 'salariumqualityautomation@gmail.com',
                'website' => 'salariumautomation.com',
                'mobile_number' => '639983309682',
                'telephone_number' => '26772684',
                'telephone_extension' => '9889',
                'fax_number' => '7788212',
                'type' => 'Private',
                'tin' => '111-999-222-333',
                'rdo' => '99A',
                'sss' => '88-8444777-5',
                'hdmf' => '5556-6644-4777',
                'philhealth' => '33-388822299-9',
                'address' => [
                    'address_line_1' => '1650',
                    'address_line_2' => 'Peñafrancia',
                    'city' => 'Makati',
                    'country' => 'Philippines',
                    'region' => 'NCR',
                    'zip_code' => '1208'
                ]
            ]
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.disbursements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(BankRequestService::class, [
            [
                'body' => [
                    "data" => [
                        [
                            "type" => "company-bank-account",
                            "id" => 235,
                            "attributes" => [
                                "bankBranch" => "Makati - Reposo",
                                "accountNumber" => "123456",
                                "accountType" => "CURRENT",
                                "companyCode" => "99102",
                                "bank_details" => [],
                                "bank" => [
                                    "id" => 1,
                                    "name" => "AL-AMANAH ISLAMIC INVESTMENT BANK OF THE PHILIPPINES",
                                    "group" => null
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    public function testGetCompanyBankSuccess()
    {
        $this->json(
            'GET',
            '/company/1/banks',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->assertResponseOk();
    }

    public function testGetCompanyBankAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/banks',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->assertResponseOk();
    }

    public function testGetCompanyBankCompanyIdMismatch()
    {
        $this->json(
            'GET',
            '/company/7/banks',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
