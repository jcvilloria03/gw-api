<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Steam\TestStreamWrapper;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AwsTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DownloadPayrollTypeTest extends TestCase
{
    use AwsTrait;
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    /**
     * @param string $path
     * @param $type
     * @dataProvider dataDownloadPayrollTypeShouldResponseSuccess
     */
    public function testDownloadPayrollTypeShouldResponseSuccess(string $path, $type)
    {
        TestStreamWrapper::$streams['s3://test/download.csv'] = 'test-stream';
        $this->mockAwsSdk([]);
        $this->mockS3StreamWrapper();

        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);
        $job = ['data' => ['payroll_id' => 1, 'uploaded_file' => 'download.csv', 'type' => $type]];
        $this->shouldExpectRequest('GET', '/payroll_job/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($job)))
        ;

        $payroll = ['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this->json('GET', $path, [], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
        ;
    }

    public function dataDownloadPayrollTypeShouldResponseSuccess()
    {
        yield ['/download/attendance/1', 'attendance'];
        yield ['/download/allowance/1', 'allowance'];
        yield ['/download/bonus/1', 'bonus'];
        yield ['/download/commission/1', 'commission'];
        yield ['/download/deduction/1', 'deduction'];
    }
}