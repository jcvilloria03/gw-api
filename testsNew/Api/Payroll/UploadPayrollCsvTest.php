<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AwsTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UploadPayrollCsvTest extends TestCase
{
    use AwsTrait;
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    /**
     * @param string $path
     * @dataProvider dataUploadPayrollCsvShouldResponseSuccess
     */
    public function testUploadPayrollCsvShouldResponseSuccess(string $path)
    {
        $this->mockAwsSdk([[]]);
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]]
        ]);
        $payroll = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group_id' => 1,
            'start_date' => '2020-07-03',
            'end_date' => '2020-07-30',
            'type' => 'regular',
        ];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;
        $jobResponse = [
            'data' => ['id' => 1],
        ];
        $this->shouldExpectRequest('POST', '/jobs')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($jobResponse)))
        ;
        $this->shouldExpectRequest('POST', '/payroll_job')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED))
        ;

        $headers = [
            self::HEADER => $module,
            'CONTENT_TYPE' => 'multipart/form-data'
        ];

        $this->call(
            'POST',
            $path,
            ['payroll_id' => 1],
            [],
            ['file' => UploadedFile::fake()->create('test.csv')],
            $this->transformHeadersToServerVars($headers)
        );

        $this
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson(['data' => ['id' => 1]])
        ;
    }

    public function dataUploadPayrollCsvShouldResponseSuccess()
    {
        yield ['/payroll/upload/allowance'];
        yield ['/payroll/upload/bonus'];
        yield ['/payroll/upload/commission'];
        yield ['/payroll/upload/deduction'];
        yield ['/payroll/upload/attendance'];
    }

    /**
     * @param string $path
     * @dataProvider dataUploadPayrollCsvShouldResponseUnauthorized
     */
    public function testUploadPayrollCsvShouldResponseUnauthorized(string $path)
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]]
        ]);
        $payroll = ['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $headers = [
            self::HEADER => $module,
            'CONTENT_TYPE' => 'multipart/form-data'
        ];

        $this->call(
            'POST',
            $path,
            ['payroll_id' => 1],
            [],
            ['file' => UploadedFile::fake()->create('test.csv')],
            $this->transformHeadersToServerVars($headers)
        );

        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function dataUploadPayrollCsvShouldResponseUnauthorized()
    {
        yield ['/payroll/upload/allowance'];
        yield ['/payroll/upload/bonus'];
        yield ['/payroll/upload/commission'];
        yield ['/payroll/upload/deduction'];
        yield ['/payroll/upload/attendance'];
    }
}
