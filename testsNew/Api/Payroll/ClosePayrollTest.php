<?php
namespace TestsNew\Api\Payroll;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Payroll\PayrollRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class ClosePayrollTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->deleteAccountEssentialDataCache();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'payroll_group_id' => 1
                ],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [1],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);
        
        $this->mockClass(PayrollRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'payroll_job_id' => '3fa38a4f-f9ce-4431-a636-f5656d1cf531',
                    'payroll_group_id' => 1,
                    'payroll_group_name' => 'Disburse_semi-monthly 1',
                    'company_id' => 1,
                    'account_id' => 1,
                    'start_date' => '2019-07-11',
                    'end_date' => '2019-07-25',
                    'posting_date' => '2019-07-25',
                    'payroll_date' => '2019-07-31',
                    'total_employees' => 12,
                    'status' => 'OPEN',
                    'gross' => 443472.98,
                    'net' => 372863.24,
                    'total_deductions' => 70609.74,
                    'total_contributions' => 38748.34,
                    'type' => 'regular',
                    'annualize' => 0,
                    'selected_releases' => [],
                    'jobs' => []
                ]
            ]),
            
            'close' => $this->getJsonResponse([
                'body' => [
                    'payroll' => [
                        'id' => 1,
                        'payroll_group_id' => 1,
                        'company_id' => 1,
                        'account_id' => 1,
                        'start_date' => '2019-07-11',
                        'end_date' => '2019-07-25',
                        'posting_date' => '2019-07-25',
                        'payroll_date' => '2019-07-31',
                        'total_employees' => 12,
                        'status' => 'CLOSED',
                        'gross' => '443472.9844',
                        'net' => '372863.2400',
                        'total_deductions' => '70609.7368',
                        'total_contributions' => '38748.3400',
                        'payslips_sent_at' => null,
                        'created_at' => '2019-10-17 20:06:35',
                        'updated_at' => '2020-05-28 20:36:34',
                        'payroll_type' => 'regular',
                        'annualize' => 0,
                        'statutory_minimum_wage_for_mwe' => '0.0000',
                        'other_pays_for_mwe' => '0.0000',
                        '13th_month_pay_and_other_benefits' => '0.0000',
                        'de_minimis_benefits' => '0.0000',
                        'employee_govt_contribution' => '0.0000',
                        'other_non_taxable_compensation_amount' => '0.0000',
                        'total_non_taxable_compensation' => '0.0000',
                        'total_taxable_compensation' => '0.0000',
                        'taxable_compensation_not_subject_to_wtax' => '0.0000',
                        'net_taxable_compensation' => '0.0000',
                        'total_taxes_withheld' => '0.0000',
                        'taxes_withheld_for_remittance' => '0.0000',
                        'sss_ee_er_contribution' => '0.0000',
                        'sss_ec_contribution' => '0.0000',
                        'hdmf_total_loan_amount' => '0.0000',
                        'hdmf_total_ee_share' => '0.0000',
                        'hdmf_total_er_share' => '0.0000',
                        'hdmf_total_share' => '0.0000'
                    ]
                ]
            ]),
        ]);
    }

    public function testClosePayrollSuccessAllCompanies()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__'],
                        'PAYROLL_GROUP' => [1],
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'payroll/1/close',
            ['skipEnforceGapLoans' => true],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus(200);
        $this->seeJsonContains(['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1, 'status' => 'CLOSED']);
    }

    public function testClosePayrollSuccessAllPayrollGroup()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => ['__ALL__'],
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'payroll/1/close',
            ['skipEnforceGapLoans' => true],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus(200);
        $this->seeJsonContains(['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1, 'status' => 'CLOSED']);
    }

    public function testCloseSpecialPayrollSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => ['__ALL__'],
                    ]
                ]
            ]
        ]);

        $this->mockClass(PayrollRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 2,
                    'payroll_job_id' => '3fa38a4f-f9ce-4431-a636-f5656d1cf531',
                    'payroll_group_id' => null,
                    'payroll_group_name' => '',
                    'company_id' => 1,
                    'account_id' => 1,
                    'start_date' => '2019-07-11',
                    'end_date' => '2019-07-25',
                    'posting_date' => '2019-07-25',
                    'payroll_date' => '2019-07-31',
                    'total_employees' => 12,
                    'status' => 'OPEN',
                    'gross' => 443472.98,
                    'net' => 372863.24,
                    'total_deductions' => 70609.74,
                    'total_contributions' => 38748.34,
                    'type' => 'special',
                    'annualize' => 0,
                    'selected_releases' => [],
                    'jobs' => []
                ]
            ]),
            
            'close' => $this->getJsonResponse([
                'body' => [
                    'payroll' => [
                        'id' => 2,
                        'payroll_group_id' => null,
                        'company_id' => 1,
                        'account_id' => 1,
                        'start_date' => '2019-07-11',
                        'end_date' => '2019-07-25',
                        'posting_date' => '2019-07-25',
                        'payroll_date' => '2019-07-31',
                        'total_employees' => 12,
                        'status' => 'CLOSED',
                        'gross' => '443472.9844',
                        'net' => '372863.2400',
                        'total_deductions' => '70609.7368',
                        'total_contributions' => '38748.3400',
                        'payslips_sent_at' => null,
                        'created_at' => '2019-10-17 20:06:35',
                        'updated_at' => '2020-05-28 20:36:34',
                        'payroll_type' => 'regular',
                        'annualize' => 0,
                        'statutory_minimum_wage_for_mwe' => '0.0000',
                        'other_pays_for_mwe' => '0.0000',
                        '13th_month_pay_and_other_benefits' => '0.0000',
                        'de_minimis_benefits' => '0.0000',
                        'employee_govt_contribution' => '0.0000',
                        'other_non_taxable_compensation_amount' => '0.0000',
                        'total_non_taxable_compensation' => '0.0000',
                        'total_taxable_compensation' => '0.0000',
                        'taxable_compensation_not_subject_to_wtax' => '0.0000',
                        'net_taxable_compensation' => '0.0000',
                        'total_taxes_withheld' => '0.0000',
                        'taxes_withheld_for_remittance' => '0.0000',
                        'sss_ee_er_contribution' => '0.0000',
                        'sss_ec_contribution' => '0.0000',
                        'hdmf_total_loan_amount' => '0.0000',
                        'hdmf_total_ee_share' => '0.0000',
                        'hdmf_total_er_share' => '0.0000',
                        'hdmf_total_share' => '0.0000'
                    ]
                ]
            ]),
        ]);

        $this->json(
            'POST',
            'payroll/2/close',
            ['skipEnforceGapLoans' => true],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus(200);
        $this->seeJsonContains(['id' => 2, 'company_id' => 1, 'payroll_group_id' => null, 'status' => 'CLOSED']);
    }

    /**
     * @dataProvider dataClosePayrollError
     */
    public function testClosePayrollError($authzMockData, $responseStatusCode)
    {
        $this->mockClass(AuthzRequestService::class, $authzMockData);

        $this->json(
            'POST',
            'payroll/1/close',
            ['skipEnforceGapLoans' => true],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus($responseStatusCode);
    }

    public function dataClosePayrollError()
    {
        yield [
            'authzMockData' => [
                'checkSalariumClearance' => [
                    'payroll.payroll_summary' => [
                        'data_scope' => [
                            'COMPANY' => [2],
                            'PAYROLL_GROUP' => [1],
                        ]
                    ]
                ]
            ],
            'responseStatusCode' => 401
        ];

        yield [
            'authzMockData' => [
                'checkSalariumClearance' => [
                    'payroll.payroll_summary' => [
                        'data_scope' => [
                            'COMPANY' => [1],
                            'PAYROLL_GROUP' => [2],
                        ]
                    ]
                ]
            ],
            'responseStatusCode' => 401
        ];


        yield [
            'authzMockData' => [
                'checkSalariumClearance' => [
                    'payroll.payroll_summary' => [
                        'data_scope' => [
                            'COMPANY' => [12],
                            'PAYROLL_GROUP' => [2],
                        ]
                    ]
                ]
            ],
            'responseStatusCode' => 401
        ];
    }

    private function deleteAccountEssentialDataCache()
    {
        $redisKey = Config::get('account_essential.redis_cache_key');
        
        Redis::del(sprintf($redisKey, 1));
    }

    public function tearDown()
    {
        $this->deleteAccountEssentialDataCache();
        parent::tearDown();
    }
}
