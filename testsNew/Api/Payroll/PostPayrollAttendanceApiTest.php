<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class PostPayrollAttendanceApiTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.payroll_summary';
    const METHOD = 'POST';
    const URL = '/payroll/1/as-api/attendance';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'payroll_group_id' => 1,
            'company_id' => 1,
            'start_date' => '2020-07-07',
            'end_date' => '2020-07-08',
            'type' => 'regular'
        ];
        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $urlResponse = [
            'data' => [
                'id' => 1
            ]
        ];
        $this
            ->shouldExpectRequest('POST', '/jobs')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
    

        $urlResponse = [
            'data' => [
                'id' => 1
            ]
        ];
        $this
            ->shouldExpectRequest('POST', '/payroll_job')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
    
        $urlResponse = [
            'id' => 1
        ];
        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
            
        $dataParam = [];

        $headers = [
            self::HEADER => self::MODULE
        ];

        $file = [];

        $server = $this->transformHeadersToServerVars($headers);
        $this->call(self::METHOD, self::URL, $dataParam, [], $file, $server);

        $this->seeStatusCode(HttpResponse::HTTP_CREATED);
    }


    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [99999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'payroll_group_id' => 1,
            'company_id' => 1,
            'start_date' => '2020-07-07',
            'end_date' => '2020-07-08',
            'type' => 'regular'
        ];
        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $urlResponse = [
            'data' => [
                'id' => 1
            ]
        ];
        $this
            ->shouldExpectRequest('POST', '/jobs')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
    

        $urlResponse = [
            'data' => [
                'id' => 1
            ]
        ];
        $this
            ->shouldExpectRequest('POST', '/payroll_job')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
    
        $urlResponse = [
            'id' => 1
        ];
        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));
            
        $dataParam = [];

        $headers = [
            self::HEADER => self::MODULE
        ];

        $file = [];

        $server = $this->transformHeadersToServerVars($headers);
        $this->call(self::METHOD, self::URL, $dataParam, [], $file, $server);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
