<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeletePayrollUploadTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    /**
     * @param string $path
     * @param string $type
     * @dataProvider dataDeletePayrollUploadShouldResponseSuccess
     */
    public function testDeletePayrollUploadShouldResponseSuccess(string $path, string $type)
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]]
        ]);
        $payroll = ['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;
        $expected = ['data' => ['affected_records' => 1]];
        $this->shouldExpectRequest('DELETE', '/payroll_job')
            ->once()
            ->withBody(json_encode(['payroll_id' => 1, 'type' => $type]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this->json('DELETE', $path, ['payroll_id' => 1], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function dataDeletePayrollUploadShouldResponseSuccess()
    {
        yield ['/payroll/upload/allowance', 'ALLOWANCE'];
        yield ['/payroll/upload/bonus', 'BONUS'];
        yield ['/payroll/upload/commission', 'COMMISSION'];
        yield ['/payroll/upload/deduction', 'DEDUCTION'];
        yield ['/payroll/upload/attendance', 'ATTENDANCE'];
    }

    /**
     * @param string $path
     * @dataProvider dataDeletePayrollUploadShouldResponseUnauthorized
     */
    public function testDeletePayrollUploadShouldResponseUnauthorized(string $path)
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]]
        ]);
        $payroll = ['id' => 1, 'company_id' => 1, 'payroll_group_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this->json('DELETE', $path, ['payroll_id' => 1], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function dataDeletePayrollUploadShouldResponseUnauthorized()
    {
        yield ['/payroll/upload/allowance'];
        yield ['/payroll/upload/bonus'];
        yield ['/payroll/upload/commission'];
        yield ['/payroll/upload/deduction'];
        yield ['/payroll/upload/attendance'];
    }
}