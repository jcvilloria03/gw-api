<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreatePayrollTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreatePayrollShouldResponseSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.announcements',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 1])))
        ;

        $input = ['payroll_group_id' => 1, 'company_id' => 1];
        $expected = [];
        $this
            ->shouldExpectRequest('POST', '/payroll')
            ->once()
            ->withBody(http_build_query($input))
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)))
        ;

        $this
            ->json('POST', '/payroll', $input,  ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJson($expected)
        ;
    }

    public function testCreatePayrollShouldResponseUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.announcements',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 2])))
        ;

        $this
            ->json('POST', '/payroll', ['payroll_group_id' => 1],  ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}