<?php

namespace TestsNew\Api\Payroll;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCompanyPayrollsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetPayrollSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]]
        ]);
        $payrolls = ['data' => [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1,
                'type' => 'regular',
                'account_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group_id' => null,
                'type' => 'final',
                'account_id' => 1,
            ]
        ]];
        $this->shouldExpectRequest('GET', '/company/1/payrolls')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrolls)))
        ;
        $payollGroups = [['id' => 1, 'name' => 'Payroll Group 1', 'company_id' => 1]];
        $this->shouldExpectRequest('POST', '/company/1/payroll_groups/id')
            ->once()
            ->withBody(http_build_query(['values' => '1']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payollGroups)))
        ;

        $this->json( 'GET', '/company/1/payrolls', [], [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJsonEquals(['data' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'payroll_group_id' => 1,
                    'type' => 'regular',
                    'account_id' => 1,
                    'payroll_group_name' => 'Payroll Group 1',
                ],
                [
                    'id' => 2,
                    'company_id' => 1,
                    'payroll_group_id' => null,
                    'type' => 'final',
                    'account_id' => 1,
                    'payroll_group_name' => '',
                ]
            ]])
        ;
    }

    public function testGetPayrollUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [2], 'PAYROLL_GROUP' => [99]]
        ]);
        $payrolls = ['data' => [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group_id' => 1,
                'type' => 'regular',
                'account_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group_id' => null,
                'type' => 'final',
                'account_id' => 1,
            ]
        ]];
        $this->shouldExpectRequest('GET', '/company/1/payrolls')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrolls)))
        ;

        $this->json( 'GET', '/company/1/payrolls', [], [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
