<?php

namespace TestsNew\Api\Payroll;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Jobs\JobsRequestService;
use App\Payroll\PayrollRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use TestsNew\Helpers\Traits\AwsTrait;

class GetPayrollGeneratePayslipTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(JobsRequestService::class, [
            'createJob' => $this->getJsonResponse([
                'body' => [
                    'data' => ['id' => 'hsdhoasoo']
                ]
            ])
        ]);

        $this->mockClass(PayrollRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'payroll_group_id' => 1,
                    'account_id' => 1,
                    'status' => 'OPEN',
                    'start_date' => '06-19-2020',
                    'end_date' => '07-05-2020'
                ]
            ]),
            'addUploadJobToPayroll' => true
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [1],
                    ]
                ]
            ]
        ]);
    }

    public function testGetPayrollSuccess()
    {
        $this->json(
            'GET',
            '/payroll/1/payslips/generate',
            [],
            ['X-Authz-Entities' => 'payroll.payroll_summary']
        );

        $this->assertResponseStatus(Response::HTTP_CREATED);
    }

    public function testGetPayrollUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [99],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll/1/payslips/generate',
            [],
            ['X-Authz-Entities' => 'payroll.payroll_summary']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
