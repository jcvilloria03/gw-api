<?php

namespace TestsNew\Api\Payroll;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Jobs\JobsRequestService;
use App\Payroll\PayrollRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use TestsNew\Helpers\Traits\AwsTrait;

class GetPayrollCalculationStatusTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(JobsRequestService::class, [
            'getJobStatus' => $this->getJsonResponse([
                'body' => [
                    'data' => ['id' => 1]
                ]
            ])
        ]);

        $this->mockClass(PayrollRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'payroll_group_id' => 1,
                    'account_id' => 1,
                    'status' => 'OPEN',
                    'start_date' => '06-19-2020',
                    'end_date' => '07-05-2020'
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary.generate_regular_payroll' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [1],
                    ]
                ]
            ]
        ]);
    }

    public function testGetPayrollSuccess()
    {
        $this->json(
            'GET',
            '/payroll/1/calculate/status?job_id=skjf8sf75sa5ADSDD',
            [],
            ['X-Authz-Entities' => 'payroll.payroll_summary.generate_regular_payroll']
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetPayrollUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary.generate_regular_payroll' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [99],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll/1/calculate/status?job_id=skjf8sf75sa5ADSDD',
            [],
            ['X-Authz-Entities' => 'payroll.payroll_summary.generate_regular_payroll']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
