<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCompanyPayrollAvailableItemsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetCompanyPayrollAvailableItemsSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => 'payroll.payroll_summary',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        $expected = [
            'data' => [
                [
                    'other_income' => [
                        'id' => 6,
                        'type_id' => 7,
                        'type' => [
                            'id' => 7,
                            'name' => 'testBonusa',
                            'company_id' => 1,
                            'fully_taxable' => true,
                            'max_non_taxable' => null,
                            'basis' => 'SALARY_BASED',
                            'frequency' => 'PERIODIC'
                        ],
                        'company_id' => 1,
                        'amount' => null,
                        'percentage' => 50,
                        'auto_assign' => true,
                        'inactive' => false,
                        'basis' => 'CURRENT_BASIC_SALARY',
                        'release_details' => [
                            [
                                'id' => 6,
                                'date' => '2020-08-31',
                                'disburse_through_special_pay_run' => true,
                                'payroll_id' => null,
                                'status' => false,
                                'prorate_based_on_tenure' => 1,
                                'coverage_from' => '2020-07-01',
                                'coverage_to' => '2020-07-31',
                                'amount' => null
                            ]
                        ],
                        'recipients' => [
                            'departments' => [
                                [
                                    'id' => 10,
                                    'other_income_id' => 6,
                                    'recipient_id' => 1,
                                    'type' => 'departments',
                                    'created_at' => '2020-06-30 15:46:41',
                                    'updated_at' => '2020-06-30 15:46:41',
                                    'deleted_at' => null
                                ]
                            ]
                        ]
                    ],
                    'recipient' => [
                        'id' => 8,
                        'company_id' => 1,
                        'payroll_group_id' => 1,
                        'employee_id' => 'DEFECT4401-001',
                        'first_name' => 'Theodore',
                        'middle_name' => 'Murphy',
                        'last_name' => 'Delgado',
                        'email' => 'test+local.theodoremurphydelgado@example.test',
                        'gender' => 'male',
                        'birth_date' => '1963-04-18',
                        'telephone_number' => 1234242,
                        'mobile_number' => '(43)2432423432',
                        'hours_per_day' => null,
                        'date_hired' => '2002-04-11',
                        'date_ended' => null,
                        'location_id' => 1,
                        'department_id' => 1,
                        'rank_id' => null,
                        'cost_center_id' => null,
                        'employee_subtype_id' => 8,
                        'created_at' => '2020-04-30 14:56:12',
                        'updated_at' => '2020-04-30 14:57:20',
                        'country' => 'Philippines',
                        'region' => null,
                        'city' => 'Makati',
                        'zip_code' => '1502',
                        'first_address_line' => '1650 Penafrancia St., Brgy. Valenzuela',
                        'second_address_line' => '',
                        'position_id' => 1,
                        'non_taxed_other_income_limit' => '90000.0000',
                        'active' => 'active',
                        'department' => [
                            'id' => 1,
                            'name' => 'Engineering',
                            'description' => null,
                            'company_id' => 1,
                            'parent_id' => null,
                            'created_at' => '2020-02-25 15:26:48',
                            'updated_at' => '2020-02-25 15:26:48'
                        ],
                        'location' => [
                            'id' => 1,
                            'name' => 'HQ',
                            'company_id' => 1,
                            'is_headquarters' => true,
                            'created_at' => '2020-02-25 15:25:17',
                            'updated_at' => '2020-02-25 15:25:17',
                            'timezone' => 'Asia/Manila',
                            'country' => 'Philippines',
                            'region' => 'NCR',
                            'city' => 'Makati',
                            'zip_code' => '1208',
                            'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                            'location_pin' => '14.569222,121.022572',
                            'first_address_line' => '1650 Penafrancia',
                            'second_address_line' => '2nd Flr'
                        ],
                        'position' => [
                            'id' => 1,
                            'name' => 'Engineer',
                            'company_id' => 1,
                            'parent_id' => null,
                            'department_id' => 1,
                            'created_at' => '2020-02-25 15:29:27',
                            'updated_at' => '2020-02-25 15:29:27'
                        ],
                        'time_attendance' => [
                            'id' => 8,
                            'employee_id' => 8,
                            'status' => 'single',
                            'employment_type_id' => 1,
                            'position_id' => 1,
                            'team_id' => null,
                            'team_role' => '',
                            'primary_location_id' => 1,
                            'secondary_location_id' => null,
                            'timesheet_required' => 0,
                            'overtime' => 1,
                            'differential' => 1,
                            'regular_holiday_pay' => 1,
                            'special_holiday_pay' => 1,
                            'holiday_premium_pay' => 1,
                            'rest_day_pay' => 1,
                            'created_at' => '2020-04-30 14:56:12',
                            'updated_at' => '2020-04-30 14:56:12',
                            'team' => null
                        ]
                    ],
                    'release_detail' => [
                        'id' => 6,
                        'other_income_id' => 6,
                        'disburse_through_special_pay_run' => true,
                        'status' => false,
                        'payroll_id' => null,
                        'date' => '2020-08-31'
                    ]
                ]
            ]
        ];

        $this
            ->json(
                'GET',
                '/company/1/payroll/special/available_items',
                [],
                ['X-Authz-Entities' => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testGetCompanyPayrollAvailableItemsUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => 'payroll.payroll_summary',
            'data_scope' => ['PAYROLL_GROUP' => [12], 'COMPANY' => [12]],
        ]);

        $this
            ->json(
                'GET',
                '/company/1/payroll/special/available_items',
                [],
                ['X-Authz-Entities' => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
