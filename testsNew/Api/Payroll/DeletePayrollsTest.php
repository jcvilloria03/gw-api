<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeletePayrollsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testDeletePayrollShouldResponseSuccess()
    {
        $module = 'payroll.summary';
        $scopes = ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);
        $inputs = ['id' => [1, 2]];
        $expected = ['data' => [['id' => 1], ['id' => 2]]];
        $this->shouldExpectRequest('DELETE', '/payrolls')
            ->once()
            ->withBody(http_build_query($inputs))
            ->withDataScope($scopes)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this->json('DELETE', '/payrolls', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testDeletePayrollShouldResponseInvalid()
    {
        $module = 'payroll.summary';
        $scopes = ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scopes]);
        $inputs = ['id' => [1, 2]];
        $expected = [
            'message' => 'Error encountered while deleting your selected payroll.'
                . ' You are not allowed to delete payrolls 1, 2',
            'status_code' => HttpResponse::HTTP_FORBIDDEN,
        ];
        $this->shouldExpectRequest('DELETE', '/payrolls')
            ->once()
            ->withBody(http_build_query($inputs))
            ->withDataScope($scopes)
            ->andReturnResponse(new Response(HttpResponse::HTTP_FORBIDDEN, [], json_encode($expected)))
        ;

        $this->json('DELETE', '/payrolls', $inputs, ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_FORBIDDEN)
            ->seeJson($expected)
        ;
    }
}