<?php


namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class RecalculateSpecialPayrollTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testRecalculateSpecialPayrollShouldResponseSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);

        $expected = [
            'links' => ['self' => '/jobs/bb88a596-56d4-491d-97b9-5cf216098d67'],
            'data' => [
                'types' => 'jobs',
                'id' => 'bb88a596-56d4-491d-97b9-5cf216098d67',
                'payload' => [
                    'name' => 'compute-payroll',
                    'payrollId' => 1,
                    'companyId' => 1,
                ],
            ],
        ];
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1, 'type' => 'special', 'status' => 'OPEN'];
        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this
            ->shouldExpectRequest('POST','/payroll/1/calculate')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this
            ->shouldExpectRequest('POST', '/company/1/payroll_statistics')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this
            ->json('POST', '/payroll/1/recalculate', [], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testRecalculateSpecialPayrollShouldResponseInvalid()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1, 'type' => 'special', 'status' => 'DRAFT'];
        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this
            ->json('POST', '/payroll/1/recalculate', [], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson([
                'message' => 'Only Open Payrolls can be re-calculated.',
                'status_code' => HttpResponse::HTTP_NOT_ACCEPTABLE
            ])
        ;
    }

    public function testRecalculateSpecialPayrollShouldResponseUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [2], 'PAYROLL_GROUP' => [1]],
        ]);
        $payroll = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1, 'type' => 'special', 'status' => 'OPEN'];
        $this
            ->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payroll)))
        ;

        $this
            ->json('POST', '/payroll/1/recalculate', [], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
