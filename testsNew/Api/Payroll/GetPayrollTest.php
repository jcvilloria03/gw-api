<?php

namespace TestsNew\Api\Payroll;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Payroll\PayrollRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetPayrollTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(PayrollRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'payroll_group_id' => 1,
                ]
            ]),
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [1],
                    ]
                ]
            ]
        ]);
    }

    public function testGetPayrollSuccess()
    {
        $this->json(
            'GET',
            '/payroll/1',
            [],
            [
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetPayrollUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'payroll.payroll_summary' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'PAYROLL_GROUP' => [99],
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/payroll/1',
            [],
            [
                'x-authz-entities' => 'payroll.payroll_summary'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
