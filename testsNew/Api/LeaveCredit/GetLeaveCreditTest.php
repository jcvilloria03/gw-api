<?php

namespace TestsNew\Api\LeaveCredit;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetLeaveCreditTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/leave_credit/1';
    const TARGET_METHOD = 'GET';
    const MODULE = 'employees.leaves.leave_credits';

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            ]
        );

        $expected = [
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'employee_id' => 1,
                'company_id' => 1,
                'leave_type_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'time_attendance' => [
                    'team_id' => 1
                ],
                'payroll' => [
                    'payroll_group_id' => 1
                ]
            ]
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $employee = [
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => 1,
                'department_id' => 1,
                'location_id' => 1,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_OK)
        ->seeJson($expected)
        ;
    }

    public function testShouldResponseError()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [999],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            ]
        );

        $expected = [
            'company_id' => 1,
            'employee' => [
                'id' => 1,
                'employee_id' => 1,
                'company_id' => 1,
                'leave_type_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'time_attendance' => [
                    'team_id' => 1
                ],
                'payroll' => [
                    'payroll_group_id' => 1
                ]
            ]
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->once()
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $employee = [
            'employee' => [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => 1,
                'department_id' => 1,
                'location_id' => 1,
                'position_id' => 1,
                'team_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
