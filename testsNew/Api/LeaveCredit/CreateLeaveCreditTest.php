<?php

namespace TestsNew\Api\LeaveCredit;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateLeaveCreditTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/leave_credit';
    const TARGET_METHOD = 'POST';
    const MODULE = 'employees.people.leave_credits';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => 1,
            'department_id' => 1,
            'location_id' => 1,
            'position_id' => 1,
            'team_id' => 1
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->shouldExpectRequest('POST', '/leave_credit')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1
            ])))
        ;

        $this->shouldExpectRequest('GET', '/leave_credit/1')
        ->andReturnResponse(
            new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode([
                'id' => 1
            ]))
        );

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode([
                    'id' => 1
                ]))
            );

        $this->json(self::TARGET_METHOD, self::TARGET_URL, [
            "company_id" => 1,
            "employee_id" => 1,
            "leave_type_id" => 1,
            "value"=> 150,
            "unit" => "hours"
        ],[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $employee = [
            'id' => 1,
            'company_id' => 1,
            'payroll_group' => 999,
            'department_id' => 999,
            'location_id' => 999,
            'position_id' => 999,
            'team_id' => 999
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this
            ->shouldExpectRequest('POST', '/leave_credit')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1
            ])))
        ;


        $this->shouldExpectRequest('GET', '/leave_credit/1')
        ->andReturnResponse(
            new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode([
                'id' => 1
            ]))
        );

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode([
                    'id' => 1
                ]))
            );

        $this->json(self::TARGET_METHOD, self::TARGET_URL, [
            "company_id" => 1,
            "employee_id" => 1,
            "leave_type_id" => 1,
            "value"=> 150,
            "unit" => "hours"
        ],[self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);

    }

}
