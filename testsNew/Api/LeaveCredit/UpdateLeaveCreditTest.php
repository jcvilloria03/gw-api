<?php

namespace TestsNew\Api\LeaveCredit;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Employee\EmployeeRequestService;
use App\LeaveCredit\LeaveCreditAuditService;
use App\LeaveCredit\LeaveCreditRequestService;

class UpdateLeaveCreditTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = '/leave_credit/1';
    const TARGET_METHOD = 'PUT';
    const MODULE = 'employees.people.leave_credits';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(LeaveCreditAuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        // Controller mocks here
        $this->mockRequestService(LeaveCreditRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'employee' => [
                        'id' => 1
                    ],
                    'company_id' => 1,
                    'leave_type_id' => 1,
                    'unit' => 'hours',
                    'value' => 150
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'employee' => [
                        'id' => 1
                    ],
                    'company_id' => 1,
                    'leave_type_id' => 1,
                    'unit' => 'hours',
                    'value' => 150
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'employee' => [
                        'id' => 1
                    ],
                    'company_id' => 1,
                    'leave_type_id' => 1,
                    'unit' => 'hours',
                    'value' => 150
                ]
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        self::MODULE => [
                            'data_scope' => [
                                'COMPANY' => [1],
                                'POSITION' => [1],
                                'LOCATION' => [1],
                                'DEPARTMENT' => [1],
                                'TEAM' => [1],
                                'PAYROLL_GROUP' => [1],
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockRequestService(EmployeeRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'employee' => [
                        'position_id' => 1,
                        'department_id' => 1,
                        'location_id' => 1,
                        'team_id' => 1,
                        'payroll_group' => [
                            'id' => 1
                        ]
                    ]
                ]
            ]
        ]);

        $payload = [
            'company_id' => 1,
            'value' => 150
        ];

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'x-authz-entities' => self::MODULE
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testShouldResponseUnAuthorized()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        self::MODULE => [
                            'data_scope' => [
                                'COMPANY' => [1],
                                'POSITION' => [9],
                                'LOCATION' => [9],
                                'DEPARTMENT' => [9],
                                'TEAM' => [9],
                                'PAYROLL_GROUP' => [9],
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockRequestService(EmployeeRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'position_id' => 1,
                    'department_id' => 1,
                    'location_id' => 1,
                    'team_id' => 1,
                    'payroll_group' => [
                        'id' => 1
                    ]
                ]
            ]
        ]);

        $payload = [
            'company_id' => 999,
            'value' => 150
        ];

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            $payload,
            [
                'x-authz-entities' => self::MODULE
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
