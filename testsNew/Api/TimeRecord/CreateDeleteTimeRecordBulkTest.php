<?php

namespace TestsNew\Api\TimeRecord;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateDeleteTimeRecordBulkTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreateOrDeleteTimeRecordsShouldResponseSuccess()
    {
        $module = 'time_and_attendance.attendance_computation.attendance.time_records';
        $scope = [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => $scope,
        ]);

        $ids = [1, 2];
        $employees = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,

            ]
        ];
        $this->shouldExpectRequest('GET', '/company/1/ta_employees')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;
        $this->shouldExpectRequest('POST', '/company/1/employees_by_ids')
            ->once()
            ->withBody(json_encode(['ids' => $ids, 'mode' => 'DEFAULT', 'include' => []]))
            ->withDataScope($scope)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;
        $deleteInput = [['employee_id' => 2, 'timestamp' => 1593937955]];
        $createInput = [
            ['employee_id' => 1, 'type' => 'clock_in', 'timestamp' => 1593937955]
        ];
        $ruleInput1 = [['name' => 'rule 1', 'hours' => '1.00', 'type' => 'employee']];
        $inputs = [
            'create' => $createInput,
            'delete' => $deleteInput,
        ];
        $inputsWithCompanyId = $inputs;
        $inputsWithCompanyId['company_id'] = '1';

        
        $this->shouldExpectRequest('POST', '/delete')
            ->once()
            ->withBody(json_encode(['employee_uid' => '2', 'timestamp' => '1593937955']))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($deleteInput)))
        ;
        
        $this->shouldExpectRequest('GET', '/max_clock_out/get_rule/1')
        ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($ruleInput1)));

        $this->shouldExpectRequest('PUT', '/log')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['item' => $createInput])))
        ;

        $this->shouldExpectRequest('POST', '/attendance/calculate/bulk')
            ->once()
            ->withBody(json_encode(['employee_ids' => [2], 'dates' => ['2020-07-05'], 'company_id' => 1]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_CREATED,
                [],
                json_encode(['data' => ['job_id' => 'job1']])
            ))
        ;

        $expected = ['job_ids' => ['job1']];
        $this->json(
            'POST',
            '/company/1/time_records/bulk_create_or_delete',
            $inputs,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
        ->seeStatusCode(HttpResponse::HTTP_OK)
        ->seeJson($expected);
    }

    public function testCreateOrDeleteTimeRecordsShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.attendance_computation.attendance.time_records';
        $scope = [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => $scope,
        ]);

        $ids = [1, 2];
        $employees = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 2,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,

            ]
        ];
        $this->shouldExpectRequest('GET', '/company/1/ta_employees')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;
        $this->shouldExpectRequest('POST', '/company/1/employees_by_ids')
            ->once()
            ->withBody(json_encode(['ids' => $ids, 'mode' => 'DEFAULT', 'include' => []]))
            ->withDataScope($scope)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;
        $deleteInput = [['employee_id' => 2, 'timestamp' => 1593937955]];
        $createInput = [['employee_id' => 1, 'type' => 'clock_in', 'timestamp' => 1593937955]];
        $inputs = [
            'create' => $createInput,
            'delete' => $deleteInput,
        ];
        $this->json(
            'POST',
            '/company/1/time_records/bulk_create_or_delete',
            $inputs,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}