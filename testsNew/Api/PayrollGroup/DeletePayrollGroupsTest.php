<?php

namespace TestsNew\Api\PayrollGroup;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeletePayrollGroupsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
    }


    public function testDeletePayrollGroupsShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2, 3]],
        ]);
        Amqp::shouldReceive('publish');

        $this->json(
            'DELETE',
            '/payroll_group/bulk_delete',
            ['company_id' => 1, 'payroll_group_ids' => [2, 3]],
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => 'company_settings.company_payroll.payroll_groups']
        )
            ->seeStatusCode(HttpResponse::HTTP_NO_CONTENT)
        ;
    }

    public function testDeletePayrollGroupsShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2, 3]],
        ]);

        $this->json(
            'DELETE',
            '/payroll_group/bulk_delete',
            ['company_id' => 1, 'payroll_group_ids' => [1, 2, 3]],
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => 'company_settings.company_payroll.payroll_groups']
        );

        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}