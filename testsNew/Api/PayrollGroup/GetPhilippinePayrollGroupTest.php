<?php

namespace TestsNew\Api\PayrollGroup;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Request\PayrollGroupRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetPhilippinePayrollGroupTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetPhilippinesPayrollGroupShouldResponseSuccess()
    {
        $expectedResponse = [
            'id' => 1,
            'company_id' => 1,
            'name' => 'Test Payroll',
        ];
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]]
        ]);
        $this->getRequestStorage(PayrollGroupRequest::getStorageName())->push($expectedResponse);

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expectedResponse)));

        $this->json(
            'GET',
            '/philippine/payroll_group/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expectedResponse);
    }

    public function testGetPhilippinesPayrollGroupShouldResponseNotFound()
    {

        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]]
        ]);

        $expectedResponse = ['message' => 'Not found', 'status_code' => HttpResponse::HTTP_NOT_FOUND];

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_NOT_FOUND, [], json_encode($expectedResponse)));

        $this->json(
            'GET',
            '/philippine/payroll_group/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson($expectedResponse);
    }

    public function testGetPhilippinesPayrollGroupShouldResponseUnauthorized()
    {

        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [2], 'PAYROLL_GROUP' => [1]]
        ]);
        $this->getRequestStorage(PayrollGroupRequest::getStorageName())->push([
            'id' => 1,
            'company_id' => 1,
            'name' => 'Test Payroll',
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['company_id' => 1])));

        $this->json(
            'GET',
            '/philippine/payroll_group/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(401)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
