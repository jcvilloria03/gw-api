<?php

namespace TestsNew\Api\PayrollGroup;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AuthTokenTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetPhilippinePayrollGroupsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use AuthTokenTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1, 2],
                'position' => [1],
                'team' => [1, 2],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
    }

    public function testGetPhilippinePayrollGroupsShouldResponseSuccess()
    {
        $expectedResponse = ['data' => [['id' => 1, 'name' => 'Test Payroll']]];
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1]]
        ]);

        $this->shouldExpectRequest('GET', getenv('COMPANY_API_URI') . '/philippine/company/1/payroll_groups')
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode($expectedResponse)
            ));
        $this->json(
            'GET',
            '/philippine/company/1/payroll_groups',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expectedResponse);
    }

    public function testGetPhilippinePayrollGroupsShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [2]]
        ]);

        $authzToken = $this->generateAuthorizationToken(['sub' => 1]);
        $this->json(
            'GET',
            '/philippine/company/1/payroll_groups',
            [],
            [
                'Authorization' => 'Bearer ' . $authzToken,
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson([
                'message' => 'Unauthorized',
                'status_code' => HttpResponse::HTTP_UNAUTHORIZED
            ]);
    }

    public function testGetPhilippinePayrollGroupsShouldResponseNotFound()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]]
        ]);

        $this->shouldExpectRequest('GET', getenv('COMPANY_API_URI') . '/philippine/company/1/payroll_groups')
            ->once()
            ->withDataScope(['COMPANY' => [1], 'PAYROLL_GROUP' => [2]])
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_NOT_FOUND,
                [],
                json_encode(['message' => 'Company not found.', 'status_code' => HttpResponse::HTTP_NOT_FOUND])
            ));
        $this->json(
            'GET',
            '/philippine/company/1/payroll_groups',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson([
                'message' => 'Company not found.',
                'status_code' => HttpResponse::HTTP_NOT_FOUND
            ]);
    }

    public function testGetPhilippinePayrollGroupsShouldResponseNotAcceptable()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]]
        ]);

        $this->json(
            'GET',
            '/philippine/company/a/payroll_groups',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson([
                'message' => 'Company ID should be numeric.',
                'status_code' => HttpResponse::HTTP_NOT_ACCEPTABLE
            ]);
    }
}
