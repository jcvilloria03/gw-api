<?php

namespace TestsNew\Api\PayrollGroup;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdatePhilippinePayrollGroupTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUpdatePayrollGroupShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);

        $expected = ['id' => 1];

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->twice()
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 1, 'name' => 'Old'])),
                new Response(
                    HttpResponse::HTTP_OK,
                    [],
                    json_encode(['id' => 1, 'company_id' => 1, 'name' => 'Test Payroll Group'])
                )
            );

        $this
            ->shouldExpectRequest('PATCH', '/philippine/payroll_group/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)));

        $this
            ->patch('/philippine/payroll_group/1', ['name' => 'Test Payroll Group'], ['x-authz-entities' => 'company_settings.company_payroll.payroll_groups'])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testUpdatePayrollGroupShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [2]],
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/payroll_group/1')
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, [], json_encode(['id' => 1, 'company_id' => 999, 'name' => 'Old']))
            );

        $this
            ->patch('/philippine/payroll_group/1', ['name' => 'Test Payroll Group'], ['x-authz-entities' => 'company_settings.company_payroll.payroll_groups'])
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
