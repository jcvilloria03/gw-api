<?php

namespace TestsNew\Api\PayrollGroup;

use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Request\UserRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AuthTokenTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreatePhilippinePayrollGroupTest extends TestCase
{
    use AuthorizationServiceTrait;
    use AuthTokenTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('del');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreatePayrollGroupShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [1]]
        ]);

        $this
            ->shouldExpectRequest('POST', getenv('COMPANY_API_URI') . '/philippine/payroll_group/')
            ->once();

        $authzToken = $this->generateAuthorizationToken(['sub' => 1]);
        $this->json(
            'POST',
            '/philippine/payroll_group',
            ['company_id' => 1, 'account_id' => 1],
            [
                'Authorization' => 'Bearer ' . $authzToken,
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this->seeStatusCode(HttpResponse::HTTP_CREATED)
            ->seeJsonStructure(['id'])
            ->seeJson(['company_id' => '1', 'account_id' => '1']);
    }

    public function testCreatePayrollGroupShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.payroll_groups',
            'data_scope' => ['COMPANY' => [2]]
        ]);

        $this
            ->shouldExpectRequest('POST', getenv('AUTHZ_API_URL') . '/api/v1/salarium_clearance')
            ->once();

        $this->json(
            'POST',
            '/philippine/payroll_group',
            ['company_id' => 1, 'account_id' => 1],
            [
                'x-authz-entities' => 'company_settings.company_payroll.payroll_groups'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
