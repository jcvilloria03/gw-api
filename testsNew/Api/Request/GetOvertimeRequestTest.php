<?php

namespace TestsNew\Api\Request;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetOvertimeRequestTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetOvertimeRequestSuccess()
    {
        $module = 'time_and_attendance.approvals_list.request_details';
        $this->addUserAndAccountEssentialData([
            'module' => 'time_and_attendance.approvals_list.request_details',
            'data_scope' => [
                'COMPANY' => [1],
                'DEPARTMENT' => [1],
                'POSITION' => [1],
                'LOCATION' => [1],
                'TEAM' => [1],
                'PAYROLL_GROUP' => [1],
            ],
        ]);

        $expected = [
            'id' => 1,
            'date' => '2020-06-18',
            'workflow_id' => 3,
            'employee_id' => 15,
            'employee_request_id' => 58,
            'status' => 'Approved',
            'messages' => [],
        ];

        $this
            ->json(
                'GET',
                '/overtime_request/1',
                [],
                ['X-Authz-Entities' => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }
}
