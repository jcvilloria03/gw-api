<?php

namespace TestsNew\Api\Project;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Project\ProjectAuditService;
use App\Account\AccountRequestService;
use App\Project\ProjectRequestService;

class DeleteBulkProjectTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'project/bulk_delete';
    const TARGET_METHOD = 'DELETE';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.projects' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(ProjectAuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(ProjectRequestService::class, [
            [
                'body' => []
            ]
        ]);
    }

    public function testBulkDeleteProjectShouldResponseSuccess()
    {
        $payload = [
            'company_id' => 1,
            'project_ids' => [1]
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.projects'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testBulkDeleteProjectShouldResponseUnauthorized()
    {
        $payload = [
            'company_id' => 999,
            'project_ids' => [1]
        ];
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 999),
            $payload,
            [
                'X-Authz-Entities' => 'company_settings.company_structure.projects'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
