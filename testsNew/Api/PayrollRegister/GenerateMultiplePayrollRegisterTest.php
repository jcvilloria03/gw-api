<?php

namespace TestsNew\Api\PayrollRegister;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GenerateMultiplePayrollRegisterTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGenerateMultiplePayrollRegisterShouldResponseSuccess()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1, 2]],
        ]);

        $payrollResponse1 = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollResponse1)))
        ;

        $payrollResponse2 = ['id' => 2, 'payroll_group_id' => 2, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/2')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollResponse2)))
        ;
        $user = ['id' => 1, 'first_name' => 'Test', 'email' => 'test@test.com'];
        $this->shouldExpectRequest('GET', '/user/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($user)))
        ;

        $inputs = ['id' => [1, 2]];
        $expected = [
            'data' => [
                'id' => 1,
                'type' => 'payroll-register',
                'attributes' => [],
            ],
        ];
        $this->shouldExpectRequest('POST', '/payroll_register/multiple')
            ->once()
            ->withBody(json_encode(array_merge(['user' => $user], $inputs)))
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode($expected)));

        $this->json('POST', '/payroll_register/multiple', $inputs, ['X-Authz-Entities' => $module])
//            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testGenerateMultiplePayrollRegisterShouldResponseUnauthorized()
    {
        $module = 'payroll.payroll_summary';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1], 'PAYROLL_GROUP' => [1]],
        ]);

        $payrollResponse1 = ['id' => 1, 'payroll_group_id' => 1, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollResponse1)))
        ;

        $payrollResponse2 = ['id' => 2, 'payroll_group_id' => 2, 'company_id' => 1];
        $this->shouldExpectRequest('GET', '/payroll/2')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($payrollResponse2)))
        ;

        $this->json('POST', '/payroll_register/multiple', ['id' => [1, 2]], ['X-Authz-Entities' => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
