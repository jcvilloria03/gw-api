<?php

namespace TestsNew\Api\DayHourRate;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use App\DeductionType\PhilippineDeductionTypeRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;

class BulkCreateCompanyDeductionTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = '/philippine/company/%d/deduction_type/bulk_create';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);
        
        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ], [
            'user_id' => 2,
            'account_id' => 2
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.deduction_types' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        // Controller mocks here

        $this->mockClass(PhilippineDeductionTypeRequestService::class, [
            'bulkCreate' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'name' => 'Deduction Type 1'
                        ],
                        [
                            'name' => 'Deduction Type 2'
                        ]
                    ]

                ]
            ]),
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);
    }

    public function testBulkCreateRateShouldResponseSuccess()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 1),
            [
                [
                    'name' => 'Deduction Type 1'
                ],
                [
                    'name' => 'Deduction Type 2'
                ]
            ],
            [
                'x-authz-entities' => 'company_settings.company_payroll.deduction_types'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testBulkCreateShouldResponseUnauthorized()
    {
        $this->json(
            self::TARGET_METHOD,
            sprintf(self::TARGET_URL, 999),
            [
                [
                    'name' => 'Deduction Type 1'
                ],
                [
                    'name' => 'Deduction Type 2'
                ]
            ],
            [
                'x-authz-entities' => 'company_settings.company_payroll.deduction_types'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
