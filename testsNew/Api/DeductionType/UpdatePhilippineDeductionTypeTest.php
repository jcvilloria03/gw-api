<?php

namespace TestsNew\Api\DeductionType;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use App\DeductionType\PhilippineDeductionTypeRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use App\Audit\OtherIncomeTypeAuditService;
use App\Account\AccountRequestService;
use Bschmitt\Amqp\Facades\Amqp;

class UpdatePhilippineDeductionTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'philippine/deduction_type/%d';
    const TARGET_METHOD = 'PATCH';

    public function setUp()
    {
        parent::setUp();

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);
        
        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);
        // Controller mocks here

        $this->mockClass(OtherIncomeTypeRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'name' => 'Deduction Type Name',
                    'company_id' => 1
                ]
            ]),
        ]);

        $this->mockClass(PhilippineDeductionTypeRequestService::class, [
            'update' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'name' => 'Test Type Name',
                    'company_id' => 1
                ]
            ]),
        ]);

        $this->mockClass(OtherIncomeTypeAuditService::class, [
            'mapAndQueueAuditItems' => true
        ]);
        
        Amqp::shouldReceive('publish')->andReturnNull();
    }

    public function testUpdatePhilippineDeductionTypeShouldResponseSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.deduction_types' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->patch(
            sprintf(self::TARGET_URL, 1),
            [
                'name' => 'Deduction Type Other Name'
            ],
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_payroll.deduction_types'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdatePhilippineDeductionTypeShouldResponseUnauthorized()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.deduction_types' => [
                        ]
                    ]
                ]
            ]
        ]);
        
        $this->patch(
            sprintf(self::TARGET_URL, 12),
            [
                'name' => 'Deduction Type Other Name'
            ],
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'company_settings.company_payroll.deduction_types'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
