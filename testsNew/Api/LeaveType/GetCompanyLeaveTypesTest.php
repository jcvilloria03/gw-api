<?php

namespace TestsNew\Api\LeaveType;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\LeaveType\LeaveTypeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class GetCompanyLeaveTypesTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_payroll.disbursements' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(LeaveTypeRequestService::class, [
            [
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'Sick Leave',
                            'abbreviation' => 'SL',
                            'leave_credit_required' => true,
                            'payable' => true,
                            'documents' => null,
                            'company_id' => 1,
                            'leave_credits' => [],
                            'selectable_leave_types' => []
                        ],
                        [
                            'id' => 2,
                            'name' => 'Service Incentive Leave',
                            'abbreviation' => 'SIL',
                            'leave_credit_required' => true,
                            'payable' => true,
                            'documents' => null,
                            'company_id' => 1,
                            'leave_credits' => [],
                            'selectable_leave_types' => []
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }
    
    public function testGetCompanyLeaveTypesSuccess()
    {
        $this->json(
            'GET',
            '/company/1/leave_types',
            [],
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetCompanyLeaveTypesAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_payroll.disbursements' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/leave_types',
            [],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testGetCompanyLeaveTypesCompanyIdMismatch()
    {
        $this->json(
            'GET',
            '/company/4/leave_types',
            [],
            [
                'Content-type' => 'application/json',
                'X-Authz-Entities' => 'company_settings.company_payroll.disbursements'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
