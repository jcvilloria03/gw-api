<?php

namespace TestsNew\Api\LeaveType;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Facades\Company;
use App\LeaveType\LeaveTypeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class CreateCompanyLeaveTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockRequestService(LeaveTypeRequestService::class, [
            [
                'body' => [
                    'id' => 1
                ],
                'code' => 201
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Sick Leave',
                    'abbreviation' => 'SL',
                    'leave_credit_required' => true,
                    'payable' => true,
                    'documents' => null,
                    'company_id' => 1,
                    'leave_credits' => [],
                    'selectable_leave_types' => []
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_types' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        Company::shouldReceive('getAccountId')->andReturn(1);
    }
    
    protected function getRequestPayload()
    {
        return [
            'name' => 'Sick Leave',
            'company_id' => 1,
            'abbreviation' => 'SL',
            'leave_credit_required' => true,
            'payable' => true
        ];
    }

    public function testCreateCompanyLeaveTypeSuccess()
    {
        $this->post(
            '/leave_type',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testCreateCompanyLeaveTypeAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_types' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->post(
            '/leave_type',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_CREATED);
    }

    public function testCreateCompanyLeaveTypeCompanyIdMismatch()
    {
        $payload = $this->getRequestPayload();
        $payload['company_id'] = 21;
        
        $this->post(
            '/leave_type',
            $payload,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
