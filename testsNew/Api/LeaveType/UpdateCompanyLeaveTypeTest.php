<?php

namespace TestsNew\Api\LeaveType;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Facades\Company;
use App\LeaveType\LeaveTypeRequestService;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class UpdateCompanyLeaveTypeTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [
                    'user_id' => 1,
                    'account_id' => 1
                ],
                'userData' => [
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 1
                    ],
                    [
                        'user_id' => 1,
                        'account_id' => 1,
                        'company_id' => 2
                    ]
                ]
            ]
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockRequestService(LeaveTypeRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Sick Leave',
                    'abbreviation' => 'SL',
                    'leave_credit_required' => true,
                    'payable' => true,
                    'documents' => null,
                    'company_id' => 1,
                    'leave_credits' => [],
                    'selectable_leave_types' => []
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Sickness Leave',
                    'abbreviation' => 'SL',
                    'leave_credit_required' => false,
                    'payable' => false,
                    'documents' => null,
                    'company_id' => 1,
                    'leave_credits' => [],
                    'selectable_leave_types' => []
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'name' => 'Sickness Leave',
                    'abbreviation' => 'SL',
                    'leave_credit_required' => false,
                    'payable' => false,
                    'documents' => null,
                    'company_id' => 1,
                    'leave_credits' => [],
                    'selectable_leave_types' => []
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_types' => [
                            'data_scope' => [
                                'COMPANY' => [1, 2]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        Company::shouldReceive('getAccountId')->andReturn(1);
    }
    
    protected function getRequestPayload()
    {
        return [
            'name' => 'Sickness Leave',
            'company_id' => 1,
            'leave_credit_required' => false,
            'payable' => false
        ];
    }

    public function testCreateCompanyLeaveTypeSuccess()
    {
        $this->put(
            '/leave_type/1',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testCreateCompanyLeaveTypeAllCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.leave_settings.leave_types' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->put(
            '/leave_type/1',
            $this->getRequestPayload(),
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testCreateCompanyLeaveTypeCompanyIdMismatch()
    {
        $this->mockRequestService(LeaveTypeRequestService::class, [
            [
                'body' => [
                    'id' => 4,
                    'name' => 'Sick Leave',
                    'abbreviation' => 'SL',
                    'leave_credit_required' => true,
                    'payable' => true,
                    'documents' => null,
                    'company_id' => 56,
                    'leave_credits' => [],
                    'selectable_leave_types' => []
                ]
            ]
        ]);

        $payload = $this->getRequestPayload();
        $payload['company_id'] = 21;
        
        $this->put(
            '/leave_type/4',
            $payload,
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.leave_settings.leave_types'
            ]
        );

        $this->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
