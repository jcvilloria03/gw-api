<?php

namespace TestsNew\Api\OtherIncomeType;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class BulkCreateBonusTypeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testBonusTypeBulkCreateShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $body = [
            [
                'name' => 'Bonus test',
                'basis' => 'FIXED',
                'frequency' => 'ONE_TIME',
                'fully_taxable' => true,
                'max_non_taxable' => "100"
            ]
        ];
        $expected = [
            'data' => [[
                'id' => 1,
                'name' => 'Bonus test',
                'company_id' => 1,
                'fully_taxable' => true,
                'max_non_taxable' => '100',
                'basis' => 'FIXED',
                'frequency' => 'ONE_TIME',
                'type_name' => 'App\Model\PhilippineBonusType',
                'deleted_at' => null
            ]]
        ];
        $this
            ->shouldExpectRequest('POST', '/philippine/company/1/bonus_type/bulk_create')
            ->once()
            ->withBody(http_build_query($body))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)));
        $this->json(
            'POST',
            '/philippine/company/1/bonus_type/bulk_create',
            $body,
            [
                'x-authz-entities' => 'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testBonusTypeBulkCreateShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $body = [
            [
                'name' => 'Bonus test',
                'basis' => 'FIXED',
                'frequency' => 'ONE_TIME',
                'fully_taxable' => true,
                'max_non_taxable' => "100"
            ]
        ];

        $this->json(
            'POST',
            '/philippine/company/2/bonus_type/bulk_create',
            $body,
            [
                'x-authz-entities' => 'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}