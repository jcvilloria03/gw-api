<?php

namespace TestsNew\Api\OtherIncomeType;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateAllowanceTypeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUpdateAllowanceTypeShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $getExpected = [
            'id' => 1,
            'name' => 'Test',
            'company_id' => 1,
            'fully_taxable' => true,
            'max_non_taxable' => false,
            'type_name' => 'App\Model\PhilippineAllowanceType',
            'deleted_at' => null,
        ];

        $updateExpected = [
            'id' => 1,
            'name' => 'Test 2',
            'company_id' => 1,
            'fully_taxable' => true,
            'max_non_taxable' => false,
            'type_name' => 'App\Model\PhilippineAllowanceType',
            'deleted_at' => null,
        ];

        $this
            ->shouldExpectRequest('GET',  '/other_income_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($getExpected)));
        $this
            ->shouldExpectRequest('PATCH', '/philippine/allowance_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($updateExpected)));

        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->json(
            'PATCH',
            '/philippine/allowance_type/1',
            ['name' => 'Test2'],
            [
            'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($updateExpected);
    }

    public function testUpdateAllowanceTypeShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $getExpected = [
            'id' => 1,
            'name' => 'Test',
            'company_id' => 2,
            'fully_taxable' => true,
            'max_non_taxable' => false,
            'type_name' => 'App\Model\PhilippineAllowanceType',
            'deleted_at' => null,
        ];

        $this
            ->shouldExpectRequest('GET',  '/other_income_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($getExpected)));

        $this->json(
            'PATCH',
            '/philippine/allowance_type/1',
            ['name' => 'Test2'],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }

    public function testUpdateAllowanceTypeShouldResponseNotAcceptable()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $getExpected = [
            'id' => 1,
            'name' => 'Test',
            'company_id' => 1,
            'fully_taxable' => true,
            'max_non_taxable' => false,
            'type_name' => 'App\Model\PhilippineAllowanceType',
            'deleted_at' => null,
        ];

        $updateExpected = [
            'name' => 'The name field is required.'
        ];

        $this
            ->shouldExpectRequest('GET',  '/other_income_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($getExpected)));
        $this
            ->shouldExpectRequest('PATCH', '/philippine/allowance_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_NOT_ACCEPTABLE, [], json_encode($updateExpected)));

        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->json(
            'PATCH',
            '/philippine/allowance_type/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson($updateExpected);
    }

    public function testUpdateAllowanceTypeShouldResponseNotFound()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['message' => 'Other Income Type not found.', 'status_code' => HttpResponse::HTTP_NOT_FOUND];

        $this->shouldExpectRequest('GET',  '/other_income_type/2')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_NOT_FOUND, [], json_encode($expected)));

        $this->json(
            'PATCH',
            '/philippine/allowance_type/2',
            ['name' => 'Test2'],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson($expected);
    }
}