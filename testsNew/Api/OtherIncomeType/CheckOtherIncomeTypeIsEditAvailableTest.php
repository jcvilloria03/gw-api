<?php

namespace TestsNew\Api\OtherIncomeType;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CheckOtherIncomeTypeIsEditAvailableTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
    }

    public function testOtherIncomeIsEditableShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['available' => true];

        $this
            ->shouldExpectRequest('POST', '/other_income_type/is_edit_available')
            ->once()
            ->withBody(http_build_query(['other_income_type_id' => 1]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)));

        $this
            ->shouldExpectRequest('GET', '/other_income_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'type_name' => 'App\Model\PhilippineAllowanceType'
            ])));

        $this->json(
            'POST',
            '/other_income_type/is_edit_available',
            ['other_income_type_id' => 1],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testOtherIncomeIsEditableShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [2]],
        ]);

        $this
            ->shouldExpectRequest('POST', '/other_income_type/is_edit_available')
            ->once()
            ->withBody(http_build_query(['other_income_type_id' => 1]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['available' => true])));

        $this
            ->shouldExpectRequest('GET', '/other_income_type/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'type_name' => 'App\Model\PhilippineAllowanceType'
            ])));

        $this->json(
            'POST',
            '/other_income_type/is_edit_available',
            ['other_income_type_id' => 1],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }

    public function testOtherIncomeIsEditableShouldResponseNotAcceptable()
    {
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.allowance_types',
            'data_scope' => ['COMPANY' => [2]],
        ]);

        $expected = [
            'message' => 'The other income type id is required',
            'status_code' => HttpResponse::HTTP_NOT_ACCEPTABLE,
        ];
        $this
            ->shouldExpectRequest('POST', '/other_income_type/is_edit_available')
            ->once()
            ->withBody(http_build_query(['other_income_type_id' => 1]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_NOT_ACCEPTABLE,
                [],
                json_encode($expected)
            ));

        $this->json(
            'POST',
            '/other_income_type/is_edit_available',
            ['other_income_type_id' => 1],
            [
                'x-authz-entities' => 'company_settings.company_payroll.allowance_types'
            ]
        );
        $this
            ->seeJson($expected);
    }
}