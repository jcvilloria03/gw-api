<?php

namespace TestsNew\Api\OtherIncomeType;

use App\Http\Middleware\AuthzMiddleware;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetAllCompanyIncomeTypesTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
    }

    /**
     * @dataProvider dataGetAllCompanyIncomeTypeShouldResponseSuccess
     */
    public function testGetAllCompanyIncomeTypeShouldResponseSuccess(string $module, string $type, string $typeName)
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => $module,
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['data' => [[
            'id' => 1,
            'name' => 'Test',
            'company_id' => 1,
            'type_name' => $typeName,
        ]]];

        $this
            ->shouldExpectRequest('GET', '/company/1/other_income_types/' . $type)
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            ));

        $this->get(
            '/company/1/other_income_types/' . $type,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function dataGetAllCompanyIncomeTypeShouldResponseSuccess()
    {
        yield [
            'company_settings.company_payroll.commission_types',
            'commission_type',
            'App\Model\PhilippineCommissionType',
        ];
        yield [
            'payroll.commissions',
            'commission_type',
            'App\Model\PhilippineCommissionType',
        ];
        yield [
            'payroll.deductions',
            'deduction_type',
            'App\Model\PhilippineDeductionType',
        ];
    }

    /**
     * @dataProvider dataGetAllCompanyIncomeTypeShouldResponseUnauthorized
     */
    public function testGetAllCompanyIncomeTypeShouldResponseUnauthorized(array $authz, int $companyId, $type)
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push($authz);

        $this->get(
            '/company/' . $companyId . '/other_income_types/' . $type,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $authz['module']]
        )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function dataGetAllCompanyIncomeTypeShouldResponseUnauthorized()
    {
        yield [
            [
                'module' => 'company_settings.company_payroll.bonus_types',
                'data_scope' => ['COMPANY' => [1]],
            ],
            2,
            'bonus_type'
        ];
        yield [
            [
                'module' => 'company_settings.company_payroll.allowance_type',
                'data_scope' => ['COMPANY' => [1]],
            ],
            2,
            'allowance_type'
        ];
        yield [
            [
                'module' => 'company_settings.company_payroll.commission_type',
                'data_scope' => ['COMPANY' => [1]],
            ],
            1,
            'allowance_type'
        ];
        yield [
            [
                'module' => 'company_settings.company_payroll.commission_type',
                'data_scope' => ['COMPANY' => [2]],
            ],
            1,
            'commission_type'
        ];

        yield [
            [
                'module' => 'company_settings.company_payroll.deduction_type',
                'data_scope' => ['COMPANY' => [2]],
            ],
            1,
            'commission_type'
        ];
    }

    public function testGetAllCompanyIncomeTypeShouldResponseInvalid()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['message' => 'Provided type not valid.', 'status_code' => 406];

        $this->get(
            '/company/1/other_income_types/notvalidtype',
            ['x-authz-entities' => 'root.admin']
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_ACCEPTABLE)
            ->seeJson($expected);
    }
}
