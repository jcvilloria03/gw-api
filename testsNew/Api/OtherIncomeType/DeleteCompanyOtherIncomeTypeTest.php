<?php

namespace TestsNew\Api\OtherIncomeType;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeleteCompanyOtherIncomeTypeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScopes = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        foreach ($dataScopes as $dataScope) {
            $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
        }
    }

    public function testDeleteOtherIncomeTypeShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            [
                'module' => 'company_settings.company_payroll.bonus_types',
                'data_scope' => ['COMPANY' => [1]],
            ],
            [
                'module' => 'company_settings.company_payroll.commission_types',
                'data_scope' => ['COMPANY' => [1]],
            ]
        ]);
        $ids = [1, 2];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_income_types')
            ->once()
            ->withBody(json_encode(['ids' => $ids]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    ['id' => 1, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonusType'],
                    ['id' => 2, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommissionType']
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->withBody(json_encode([
                'ids' => [1, 2],
                'types' => ['App\Model\PhilippineBonusType', 'App\Model\PhilippineCommissionType']
            ]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => true])
            ));
        $this
            ->shouldExpectRequest('DELETE', '/company/1/other_income_type')
            ->once()
            ->withBody(json_encode(['ids' => $ids]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_NO_CONTENT, []));

        $this->json(
            'DELETE',
            '/company/1/other_income_type',
            ['ids' => $ids],
            [
                'x-authz-entities' => 
                    'company_settings.company_payroll.bonus_types'
            ]
        );
        $this->seeStatusCode(HttpResponse::HTTP_NO_CONTENT);
    }

    public function testDeleteOtherIncomeTypeShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            [
                'module' => 'company_settings.company_payroll.bonus_types',
                'data_scope' => ['COMPANY' => [1]],
            ],
            [
                'module' => 'company_settings.company_payroll.commission_types',
                'data_scope' => ['COMPANY' => [1]],
            ]
        ]);
        $ids = [1, 2];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_income_types')
            ->once()
            ->withBody(json_encode(['ids' => $ids]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    ['id' => 1, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonusType'],
                    ['id' => 2, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommissionType']
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->withBody(json_encode([
                'ids' => $ids,
                'types' => ['App\Model\PhilippineBonusType', 'App\Model\PhilippineCommissionType']
            ]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => false, 'ids' => [1 => true, 2 => false]])
            ));

        $this->json(
            'DELETE',
            '/company/1/other_income_type',
            ['ids' => $ids],
            [
                'x-authz-entities' => 
                    'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson([
                'message' => 'Not authorized. Ids: 2',
                'status_code' => HttpResponse::HTTP_UNAUTHORIZED
            ]);
    }

    public function testDeleteOtherIncomeTypeShouldResponseNotFound()
    {
        $this->addUserAndAccountEssentialData([
            [
                'module' => 'company_settings.company_payroll.bonus_types',
                'data_scope' => ['COMPANY' => [1]],
            ],
            [
                'module' => 'company_settings.company_payroll.commission_types',
                'data_scope' => ['COMPANY' => [1]],
            ]
        ]);
        $ids = [1, 2, 3];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_income_types')
            ->once()
            ->withBody(json_encode(['ids' => $ids]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    ['id' => 1, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonusType'],
                    ['id' => 2, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommissionType']
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->withBody(json_encode([
                'ids' => $ids,
                'types' => ['App\Model\PhilippineBonusType', 'App\Model\PhilippineCommissionType']
            ]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => false, 'ids' => [1 => true, 2 => true]])
            ));

        $this->json(
            'DELETE',
            '/company/1/other_income_type',
            ['ids' => $ids],
            [
                'x-authz-entities' => 
                    'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson([
                'message' => 'Specified Other Income Types not found. Ids: 3',
                'status_code' => HttpResponse::HTTP_NOT_FOUND
            ]);
    }
}