<?php

namespace TestsNew\Api\OtherIncomeType;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CheckOtherIncomeTypeIfAvailableToDeleteTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
    }


    public function testIsOtherTypeIncomeAvailableToDeleteShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->withBody(json_encode(['ids' => [1, 2, 3], 'types' => ['App\Model\PhilippineBonusType']]))
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => true])
            ));

        $this->json(
            'POST',
            '/company/1/other_income_type/is_delete_available',
            ['ids' => [1, 2, 3]],
            [
                'x-authz-entities' => 'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson(['available' => true]);
    }

    public function testIsOtherTypeIncomeAvailableToDeleteShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => false, 'ids' => [1 => false, 2 => false, 3 => true]])
            ));

        $this->json(
            'POST',
            '/company/1/other_income_type/is_delete_available',
            ['ids' => [1, 2, 3]],
            [
                'x-authz-entities' => 'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Not authorized. Ids: 1,2', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }

    public function testIsOtherTypeIncomeAvailableToDeleteShouldResponseNotFound()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'company_settings.company_payroll.bonus_types',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('POST', '/company/1/other_income_type/is_delete_available')
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['available' => false, 'ids' => [1 => false, 2 => false]])
            ));
        $this->json(
            'POST',
            '/company/1/other_income_type/is_delete_available',
            ['ids' => [1, 2, 3]],
            [
                'x-authz-entities' => 'company_settings.company_payroll.bonus_types'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson([
                'message' => 'Specified Other Income Types not found. Ids: 3',
                'status_code' => HttpResponse::HTTP_NOT_FOUND
            ]);
    }
}
