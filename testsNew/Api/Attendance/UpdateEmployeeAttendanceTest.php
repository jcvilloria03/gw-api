<?php

namespace TestsNew\Api\Attendance;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Attendance\AttendanceRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Employee\EmployeeRequestService;

class UpdateEmployeeAttendanceTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'attendance/%s/%d/edit';
    const TARGET_METHOD = 'PUT';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        // Controller mocks here
        $this->mockRequestService(EmployeeRequestService::class, [
            [
                'body' => [
                    'company_id' => 1,
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]

            ]
        ]);

        $this->mockRequestService(PhilippineCompanyRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1
                ]

            ]
        ]);

        $this->mockRequestService(AttendanceRequestService::class, [
            [
                'body' => [
                    'data' => []
                ]

            ]
        ]);
    }

    public function testUpdateEmployeeAttendanceTestShouldResponseSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.attendance_computation.attendance.computed_attendance' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PUT',
            sprintf(self::TARGET_URL, 1, '2020-06-20'),
            [
                'RH' => 480,
                'UH' => 480
            ],
            [
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance.computed_attendance'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testUpdateEmployeeAttendanceTestShouldResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.attendance_computation.attendance.computed_attendance' => [
                    'data_scope' => [
                        'COMPANY' => [999],
                        'DEPARTMENT' => [1],
                        'POSITION' => [999],
                        'LOCATION' => [999],
                        'TEAM' => [999],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PUT',
            sprintf(self::TARGET_URL, 1, '2020-06-20'),
            [
                'RH' => 480,
                'UH' => 480
            ],
            [
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance.computed_attendance'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
