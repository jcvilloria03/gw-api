<?php

namespace TestsNew\Api\Attendance;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use App\Company\PhilippineCompanyRequestService;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Account\AccountRequestService;
use TestsNew\Helpers\Traits\EssentialTrait;
use App\Attendance\AttendanceRequestService;

class GetAttendanceRecordsExportTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestServiceTrait;

    const TARGET_URL = '/attendance/records/export';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);
        
        $this->mockClass(
            AccountRequestService::class,
            [
            'getAccountEssentialData' => $this->getJsonResponse(
                [
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
                ]
            ),

            'progress' => $this->getJsonResponse(
                [
                'body' => [
                    'company' => true
                ]
                ]
            )
            ]
        );

        $this->mockUserData(
            [
            'user_id' => 1,
            'account_id' => 1
            ]
        );

        $this->mockRequestService(
            CompanyRequestService::class,
            [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
            ]
        );

        $this->mockRequestService(
            UserRequestService::class,
            [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 2,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
            ]
        );

        $this->mockRequestService(
            AuthzRequestService::class,
            [
            [
                'body' => [
                    'data' => [
                        'time_and_attendance.attendance_computation.attendance' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
            ]
        );

        $this->mockClass(
            PhilippineCompanyRequestService::class,
            [
            'get' => $this->getJsonResponse(
                [
                'body' => [
                    'id' => 1,
                    'account_id' => 1
                ]
                ]
            ),
            ]
        );

        $this->mockClass(
            AttendanceRequestService::class,
            [
            'exportAttendanceRecords' => $this->getJsonResponse(
                [
                'body' => [
                    'data' => [
                        'link' => 's3 generated link'
                    ]
                ]
                ]
            ),
            ]
        );
    }

    public function testPostAttendanceRecordExportShouldResponseSuccess()
    {
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [
                "company_id" => 1,
                "start_date" => "2000-01-01",
                "end_date" => "2100-01-01",
                "page" => 1,
                "page_size" => 50,
                "search_terms" => [
                    "employee_uid" => 1
                ]
            ],
            [
                'Content-Type' => 'application/json',
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testPostAttendanceRecordExportShouldResponseError()
    {
        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [
                "company_id" => 9999,
                "start_date" => "2000-01-01",
                "end_date" => "2100-01-01",
                "page" => 1,
                "page_size" => 50,
                "search_terms" => [
                    "employee_uid" => 1
                ]
            ],
            [
                'Content-Type' => 'application/json',
                'x-authz-entities' => 'time_and_attendance.attendance_computation.attendance'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
