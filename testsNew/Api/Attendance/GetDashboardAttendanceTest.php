<?php

namespace TestsNew\Api\Payroll;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetDashboardAttendanceTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetActiveEmployeesCountSuccess()
    {
        $module = 'main_page.dashboard';
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.dashboard',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [1]],
        ]);

        $expected = [
            'id' => 14599,
            'employee_name' => 'KATHLEEN ALBRIGHT',
            'clock_in' => null,
            'clock_out' => null,
            'shift_name' => 'Default Schedule',
            'remarks' => 'On Time',
        ];

        $this
            ->json(
                'GET',
                '/company/1/dashboard/attendance?type=present&item_count=100&page=1',
                [],
                ['X-Authz-Entities' => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testGetCompanyPayrollAvailableItemsUnauthorized()
    {
        $module = 'main_page.dashboard';
        $this->addUserAndAccountEssentialData([
            'module' => 'main_page.dashboard',
            'data_scope' => ['PAYROLL_GROUP' => [1], 'COMPANY' => [156]],
        ]);

        $this
            ->json(
                'GET',
                '/company/1/dashboard/attendance?type=present&item_count=100&page=1',
                [],
                ['X-Authz-Entities' => $module]
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }
}
