<?php

namespace TestsNew\Api\Attendance;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UnlockEmployeesAttendanceTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testUnlockEmployeeAttendanceShouldResponseSuccess()
    {
        $module = 'time_and_attendance.attendance_computation.attendance';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'TEAM' => [1],
                'POSITION' => [1],
            ],
        ]);

        $expected = ['rows_affected' => 1];
        $inputs = [
            'company_id' => 1,
            'attendance_record_ids' => [
                1
            ]
        ];

        $this->shouldExpectRequest('POST', '/attendance/company/1/employees')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    [
                        'id' => 1,
                        'employee_id' => 1
                    ]
                ]
            ])))
        ;

        $this->shouldExpectRequest('POST', '/company/1/employees/id')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                [
                    'id' => 1,
                    'company_id' => 1,
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'team_id' => 1,
                    'payroll_group_id' => 1
                ]
            ])))
        ;

        $this->shouldExpectRequest('POST', '/attendance/unlock/bulk')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this->json('POST', '/attendance/unlock/bulk', $inputs, [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testUnlockEmployeeAttendanceShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.attendance_computation.attendance';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [88888],
                'PAYROLL_GROUP' => [1],
                'DEPARTMENT' => [1],
                'LOCATION' => [1],
                'TEAM' => [1],
                'POSITION' => [1],
            ],
        ]);

        $expected = ['rows_affected' => 1];
        $inputs = [
            'company_id' => 1,
            'attendance_record_ids' => [
                1
            ]
        ];
        $this->shouldExpectRequest('POST', '/attendance/unlock/bulk')
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this->json('POST', '/attendance/unlock/bulk', $inputs, [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ;
    }
}