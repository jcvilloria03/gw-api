<?php

namespace TestsNew\Api\Attendance;

use Mockery;
use TestsNew\Api\TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use App\Http\Middleware\AuthnMiddleware;
use App\Attendance\AttendanceAuthorizationService;
use App\Authn\AuthnService;
use App\CompanyUser\CompanyUserService;
use TestsNew\Helpers\Traits\PhilippineCompanyRequestServiceTrait;
use TestsNew\Helpers\Traits\AttendanceRequestServiceTrait;
use App\Model\AuthnUser;
use App\Model\CompanyUser;

class LockAttendanceTest extends TestCase
{
    use PhilippineCompanyRequestServiceTrait;
    use AttendanceRequestServiceTrait;

    const TARGET_URL = '/attendance/lock/bulk/mock';

    public function setUp()
    {
        parent::setUp();

        $userData = new AuthnUser([
            'id'            => 1,
            'user_id'       => 1,
            'account_id'    => 1,
            'status'        => 'active'
        ]);

        $companyUser = new CompanyUser([
            'user_id'     => 1,
            'company_id'  => 1,
            'employee_id' => 1
        ]);

        $authnMiddleware = Mockery::mock(AuthnMiddleware::class);
        $authnMiddleware->shouldReceive('handle')
            ->andReturnUsing(function ($request, $next) {
                $request->attributes->add([
                    'user_id' => 1,
                    'account_id' => 1,
                ]);

                return $next($request);
            });

        $authnService = Mockery::mock(AuthnService::class);
        $authnService->shouldReceive('validateToken')
            ->andReturn($userData);

        $companyUserService = Mockery::mock(CompanyUserService::class);
        $companyUserService->shouldReceive('getAllByUserId')
            ->andReturn(collect([$companyUser]));

        $this->app->bind(AuthnMiddleware::class, function () use ($authnMiddleware) {
            return $authnMiddleware;
        });

        $this->app->bind(AuthnService::class, function () use ($authnService) {
            return $authnService;
        });

        $this->app->bind(CompanyUserService::class, function () use ($companyUserService) {
            return $companyUserService;
        });

        $api = $this->app->make('Dingo\Api\Routing\Router');

        $api->version('v1', ['middleware' => ['authn', 'user']], function ($api) {
            $api->post(
                $this::TARGET_URL,
                'App\Http\Controllers\AttendanceController@bulkLockAttendanceRecord'
            );
        });
    }

    public function testLockSuccess()
    {
        $this->getMockPhilippineCompanyRequestService();

        $authorizationServiceMock = Mockery::mock(AttendanceAuthorizationService::class);
        $authorizationServiceMock
            ->shouldReceive('authorizeUpdate')
            ->once()
            ->andReturn(true);

        $this->app->bind(AttendanceAuthorizationService::class, function () use ($authorizationServiceMock) {
            return $authorizationServiceMock;
        });

        $this->mockAttendanceRequestService([
            // bulkLockAttendanceRecord
            [
                "statusCode" => HttpResponse::HTTP_OK,
                "responseBody" => []
            ]
        ]);

        $payload = [
            "company_id" => 1,
            "attendance_record_ids" => [
                1, 2, 3
            ]
        ];

        $this->json('POST', self::TARGET_URL, $payload, ['Authorization' => 'Bearer mockToken']);

        $this->assertEquals(HttpResponse::HTTP_OK, $this->response->getStatusCode());
    }

    /**
     * @dataProvider  dataForErrors
     */
    public function testLockError($overwritePayload, $error)
    {

        $this->getMockPhilippineCompanyRequestService();

        $payload = [
            "company_id" => 1,
            "attendance_record_ids" => [
                1, 2, 3
            ]
        ];

        $request = array_merge($payload, $overwritePayload);

        $this->json('POST', self::TARGET_URL, $request);


        $responseBody = json_decode($this->response->getContent(), true);

        $this->assertEquals(HttpResponse::HTTP_NOT_ACCEPTABLE, $this->response->getStatusCode());
        $this->assertArrayHasKey('message', $responseBody);
        $this->assertContains($error, $responseBody['message']);
    }

    public function dataForErrors()
    {
        yield [
            'payload' => [
                'company_id' => null
            ],
            'error' => 'The company id field is required.'
        ];

        yield [
            'payload' => [
                'company_id' => '0'
            ],
            'error' => 'The selected company id is invalid.'
        ];

        yield [
            'payload' => [
                'company_id' => 'test'
            ],
            'error' => 'The company id must be an integer.'
        ];

        yield [
            'payload' => [
                'attendance_record_ids' => null
            ],
            'error' => 'The attendance record ids field is required.'
        ];

        yield [
            'payload' => [
                'attendance_record_ids' => [
                    1,
                    "test"
                ]
            ],
            'error' => 'The attendance_record_ids.1 must be an integer.'
        ];
    }

    private function getMockPhilippineCompanyRequestService()
    {
        $this->mockPhilippineCompanyRequestService([
            // get
            [
                "statusCode" => HttpResponse::HTTP_OK,
                "responseBody" => [
                    "id" =>  1,
                    "name" =>  "Test",
                    "country" =>  [
                      "id" =>  173,
                      "code" =>  "PH",
                      "name" =>  "Philippines"
                    ],
                    "account_id" =>  1,
                    "logo" =>  "",
                    "email" =>  "test@salarium.com",
                    "website" =>  null,
                    "mobile_number" =>  null,
                    "telephone_number" =>  null,
                    "telephone_extension" =>  null,
                    "fax_number" =>  null
                ]
            ]
        ]);
    }
}
