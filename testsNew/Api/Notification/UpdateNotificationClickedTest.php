<?php

namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use App\ESS\EssNotificationRequestService;

class UpdateNotificationClickedTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(EssNotificationRequestService::class, [
            'notificationClicked' => $this->getJsonResponse([
                'body' => [
                    "id" => 1,
                    "user_id" => "1",
                    "activity_id" => 1,
                    "clicked" => true,
                    "activity" => []
                ]
            ])
        ]);
    }

    public function testUpdateAnnouncementClickedSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'root.admin' => [
                    'data_scope' => [
                        'ACCOUNT' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PUT',
            '/notifications/1/clicked',
            [],
            [
                'x-authz-entities' => 'root.admin'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateAnnouncementClickedUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => []
        ]);

        $this->json(
            'PUT',
            '/notifications/1/clicked',
            [],
            [
                'x-authz-entities' => 'root.admin'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
