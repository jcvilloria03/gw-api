<?php

namespace TestsNew\Api\Subscription;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\ProductSeat\ProductSeatRequestService;
use App\Subscriptions\SubscriptionsRequestService;

class GetSubscriptionStatsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'subscriptions/1/stats';
    const TARGET_METHOD = 'GET';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ]
        ]);

    	// Controller mocks here
        $this->mockRequestService(ProductSeatRequestService::class,[
            [
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'name' => 'hris'
                        ],
                        [
                            'id' => 2,
                            'name' => 'time and attendance'
                        ],
                        [
                            'id' => 3,
                            'name' => 'payroll'
                        ],
                        [
                            'id' => 4,
                            'name' => 'time and attendance plus payroll'
                        ],
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class,[
            [
                'body' => [
                    'data' => [
                        'total_used' => 1,
                        'total_remaining' => 5,
                        'products' => []
                    ]
                ]
            ]
        ]);
    }

    public function testGetSubscriptionStatsShouldResponseSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.subscriptions.licenses_tab' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.subscriptions.licenses_tab'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testGetSubscriptionStatsShouldResponseUnauthorized()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'code' => 401,
                'body' => null
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 9999
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.subscriptions.licenses_tab'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
