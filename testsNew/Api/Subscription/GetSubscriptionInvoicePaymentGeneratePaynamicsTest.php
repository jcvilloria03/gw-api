<?php
namespace TestsNew\Api\Subscription;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetSubscriptionInvoicePaymentGeneratePaynamicsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->deleteAccountEssentialDataCache();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ],
                    2 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.subscriptions.invoices_tab' => [
                    'data_scope' => [
                        'COMPANY' => [1, 2]
                    ]
                ]
            ]
        ]);

        $this->mockClass(UserRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'account_id' => 1,
                    'companies' => [
                        ['id' => 1],
                        ['id' => 2]
                    ]
                ]
            ]),
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(SubscriptionsRequestService::class, [
            'getInvoicePaynamicsPaymentDetails' => $this->getJsonResponse([
                'body' => null
            ]),

            'getInvoiceById' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        'account_id' => 1
                    ]
                ]
            ])
        ]);
    }

    public function testResponseSuccess()
    {
        $this->json(
            'GET',
            '/subscriptions/invoices/1/payments/generate_paynamics',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.subscriptions.invoices_tab'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseAllScope()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.subscriptions.invoices_tab' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/subscriptions/invoices/1/payments/generate_paynamics',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.subscriptions.invoices_tab'
            ]
        );

        $this->seeStatusCode(Response::HTTP_OK);
    }

    public function testResponseUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => null
        ]);

        $this->json(
            'GET',
            '/subscriptions/invoices/1/payments/generate_paynamics',
            [],
            [
                'Content-Type' => 'application/json',
                'X-Authz-Entities' => 'control_panel.subscriptions.invoices_tab'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function deleteAccountEssentialDataCache()
    {
        $redisKey = Config::get('account_essential.redis_cache_key');

        Redis::del(sprintf($redisKey, 1));
    }

    public function tearDown()
    {
        $this->deleteAccountEssentialDataCache();
        parent::tearDown();
    }
}
