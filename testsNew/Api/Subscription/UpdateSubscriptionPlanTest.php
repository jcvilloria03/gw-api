<?php

namespace TestsNew\Api\Subscription;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AuthTokenTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateSubscriptionPlanTest extends TestCase
{
    use AuthorizationServiceTrait;
    use AuthTokenTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/subscriptions/1/plan/update';
    const TARGET_METHOD = 'PATCH';
    const MODULE = 'control_panel.subscriptions.licenses_tab';

    private function addUserAndAccountEssentialData(array $dataScope = null)
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        if ($dataScope) {
            $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
        }
    }

    public function testUpdateTerminationInformationShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1]
                ]
            ]
        );

        $this
            ->shouldExpectRequest('POST', '/account/users')
            ->twice()
            ->andReturnResponse(
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode([
                        'data' => [
                            ['id' => 1,
                            'account_id' => 1,
                            'user_type' => 'owner',
                            'companies' => [
                                'id' => 1,
                                'account_id' => 1,
                            ]]
                        ]
                    ])
                ),
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode([
                        'data' => [
                            ['id' => 1,
                            'account_id' => 1,
                            'user_type' => 'super admin',
                            'companies' => [
                                'id' => 1,
                                'account_id' => 1,
                            ]]
                        ]
                    ])
                )
            )
        ;


        $expected = [
            'data' => [
                'plan_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('PATCH', '/subscriptions/1/update')
            ->once()
            ->andReturnResponse(
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode($expected)
                )
            )
        ;

        $planUpdate = [
            'data' => [
                'plan_id' => 1,
                'convert_to_paid' => 0,
                'licenses' => [
                    [
                        'product_id' => 1,
                        'units' => 100
                    ]
                ]
            ]
        ];

        $this
            ->patch(self::TARGET_URL, $planUpdate, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testUpdateTerminationInformationShouldResponseError()
    {
        $this->addUserAndAccountEssentialData();

        $this
            ->shouldExpectRequest('POST', '/account/users')
            ->andReturnResponse(
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode([
                        'data' => [
                            ['id' => 1,
                            'account_id' => 1,
                            'user_type' => 'owner',
                            'companies' => [
                                'id' => 1,
                                'account_id' => 1,
                            ]]
                        ]
                    ])
                ),
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode([
                        'data' => [
                            ['id' => 1,
                            'account_id' => 1,
                            'user_type' => 'super admin',
                            'companies' => [
                                'id' => 1,
                                'account_id' => 1,
                            ]]
                        ]
                    ])
                )
            )
        ;


        $expected = [
            'data' => [
                'plan_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('PATCH', '/subscriptions/1/update')
            ->andReturnResponse(
                new Response(
                    HttpResponse::HTTP_OK,
                    [
                        'Content-Type' => 'application/json'
                    ],
                    json_encode($expected)
                )
            )
        ;

        $planUpdate = [
            'data' => [
                'plan_id' => 1,
                'convert_to_paid' => 0,
                'licenses' => [
                    [
                        'product_id' => 1,
                        'units' => 100
                    ]
                ]
            ]
        ];

        $this
            ->patch(self::TARGET_URL, $planUpdate, [self::HEADER => self::MODULE])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}
