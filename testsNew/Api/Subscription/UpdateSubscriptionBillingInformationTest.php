<?php

namespace TestsNew\Api\Subscription;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use App\Account\AccountRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;

class UpdateSubscriptionBillingInformationTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'subscriptions/billing_information/';
    const TARGET_METHOD = 'PATCH';

    public function setUp()
    {
        parent::setUp();

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockClass(UserRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'account_id' => 1,
                    'companies' => [
                        ['id' => 1],
                        ['id' => 2]
                    ]
                ]
            ]),
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);
        // Controller mocks here

        $this->mockClass(SubscriptionsRequestService::class, [
            'setBillingInformation' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'customer_id' => 1,
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'email' => 'Email',
                    'phone' => '(632)1234567',
                    'account_name' => 'Account Name',
                    'address_1' => 'Address 1',
                    'address_2' => 'Address 2',
                    'address_3' => 'Address 3',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'zip_code' => '1010'
                ]
            ]),
        ]);
    }

    public function testUpdateSubscriptionBillingInformationResponseSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.subscriptions.invoices_tab.billing_information' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->patch(
            self::TARGET_URL,
            [
                'id' => 1,
                'customer_id' => 1,
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'email' => 'Email',
                'phone' => '(632)1234567',
                'account_name' => 'Account Name',
                'address_1' => 'Address 1',
                'address_2' => 'Address 2',
                'address_3' => 'Address 3',
                'city' => 'City',
                'state' => 'State',
                'country' => 'Country',
                'zip_code' => '1010'
            ],
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateSubscriptionBillingInformationResponseError()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'status' => 401,
                'body' => null
            ]
        ]);

        $this->patch(
            self::TARGET_URL,
            [
                'id' => 999,
                'customer_id' => 1,
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'email' => 'Email',
                'phone' => '(632)1234567',
                'account_name' => 'Account Name',
                'address_1' => 'Address 1',
                'address_2' => 'Address 2',
                'address_3' => 'Address 3',
                'city' => 'City',
                'state' => 'State',
                'country' => 'Country',
                'zip_code' => '1010'
            ],
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'x-authz-entities' => 'control_panel.subscriptions.invoices_tab.billing_information'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
