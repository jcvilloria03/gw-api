<?php

namespace TestsNew\Api\Subscription;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateSubscriptionPaypalOrderTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/subscriptions/invoices/1/payments/paypal/orders';
    const TARGET_METHOD = 'POST';
    const MODULE = 'control_panel.subscriptions.invoices_tab.invoice_history';

    private function addUserAndAccountEssentialData(array $dataScope = null)
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        if ($dataScope) {
            $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
        }
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1]
                ]
            ]
        );

        $invoice = [
            'data' => [
                'id' => 1,
                'account_id' => 1,
                'invoice_number' => '1',
                'customer_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/subscriptions/invoices/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($invoice)))
        ;

        $expected = [
            'order_id' => '1',
            'checkout_link' => 'http://www.link.com'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [
                    "id" => 1,

                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_OK)
        ->seeJson($expected)
        ;
    }

    public function testShouldResponseError()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $invoice = [
            'data' => [
                'id' => 1,
                'account_id' => 1,
                'invoice_number' => '1',
                'customer_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/subscriptions/invoices/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($invoice)))
        ;

        $expected = [
            'order_id' => '1',
            'checkout_link' => 'http://www.link.com'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [
                    "id" => 1,

                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testWithoutInvoiceDataShouldResponseError()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1]
                ]
            ]
        );

        $invoice = [
            'data' => [
                'id' => 1,
                'account_id' => 999,
                'invoice_number' => '1',
                'customer_id' => 1
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/subscriptions/invoices/1')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($invoice)))
        ;

        $expected = [
            'order_id' => '1',
            'checkout_link' => 'http://www.link.com'
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, self::TARGET_URL)
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [
                    "id" => 1,

                ],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
        ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
        // var_dump($this->response->getOriginalContent());
    }
}
