<?php

namespace TestsNew\Api\HoursWorked;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AwsTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateUpdateDeleteLeavesBulkTest extends TestCase
{
    use AwsTrait;
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testCreateUpdateDeleteLeavesBulkShouldResponseSuccess()
    {
        $module = 'time_and_attendance.attendance_computation.attendance.leaves';
        $scope = [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scope]);
        $ids = [1, 2];
        $employees = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,

            ]
        ];
        $this->shouldExpectRequest('POST', '/company/1/employees_by_ids')
            ->once()
            ->withBody(json_encode(['ids' => $ids, 'mode' => 'DEFAULT', 'include' => []]))
            ->withDataScope($scope)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;
        $inputs = [
            'data' => [
                ['employee_id' => 1, 'date' => '2020-07-06', 'shift_id' => 1, 'leaves' => []],
                ['employee_id' => 2, 'date' => '2020-07-06', 'shift_id' => 1, 'leaves' => []],
            ]
        ];
        $this->shouldExpectRequest('POST', '/company/1/hours_worked/leaves/bulk_create_or_update_or_delete')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, []))
        ;

        $this->shouldExpectRequest('POST', '/attendance/calculate/bulk')
            ->twice()
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_CREATED, [], json_encode(['data' => ['job_id' => '123456']])),
                new Response(HttpResponse::HTTP_CREATED, [], json_encode(['data' => ['job_id' => '456123']]))
            )
        ;


        $this->json(
            'POST',
            '/company/1/hours_worked/leaves/bulk_create_or_update_or_delete',
            $inputs,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJsonEquals(['job_ids'=> ['123456', '456123']])
        ;
    }

    public function testCreateUpdateDeleteLeavesBulkShouldResponseUnauthorized()
    {
        $module = 'time_and_attendance.attendance_computation.attendance.leaves';
        $scope = [
            'COMPANY' => [1],
            'PAYROLL_GROUP' => [1],
            'DEPARTMENT' => [1],
            'LOCATION' => [1],
            'TEAM' => [1],
            'POSITION' => [1],
        ];
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => $scope]);
        $ids = [1, 2];
        $employees = [
            [
                'id' => 1,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 1,
                'team_id' => 1,
                'location_id' => 1,
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'payroll_group' => ['id' => 1],
                'department_id' => 1,
                'position_id' => 2,
                'team_id' => 1,
                'location_id' => 1,

            ]
        ];
        $this->shouldExpectRequest('POST', '/company/1/employees_by_ids')
            ->once()
            ->withBody(json_encode(['ids' => $ids, 'mode' => 'DEFAULT', 'include' => []]))
            ->withDataScope($scope)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['data' => $employees])))
        ;

        $inputs = [
            'data' => [
                ['employee_id' => 1, 'date' => '2020-07-06', 'shift_id' => 1, 'leaves' => []],
                ['employee_id' => 2, 'date' => '2020-07-06', 'shift_id' => 1, 'leaves' => []],
            ]
        ];

        $this->json(
            'POST',
            '/company/1/hours_worked/leaves/bulk_create_or_update_or_delete',
            $inputs,
            [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module]
        )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}