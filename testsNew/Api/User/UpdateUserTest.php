<?php

namespace TestsNew\Api\User;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Role\RoleService;
use App\Validator\RoleValidator;
use App\Auth0\Auth0ManagementService;
use App\Model\Role;
use App\Auth0\Auth0UserService;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Authn\AuthnRequestService;

class UpdateUserTest extends TestCase
{
    use AuthorizationServiceTrait;
    use DatabaseTransactions;
    use RequestServiceTrait;

    const TARGET_URL = 'user/1';
    const TARGET_METHOD = 'PATCH';

    public function setUp()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);
        Redis::shouldReceive('hDel')
            ->andReturn(null);
        Redis::shouldReceive('keys')
            ->andReturn(null);
        Redis::shouldReceive('exists')
            ->andReturn(null);
        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

	    // Controller mocks here
        $this->mockClass(UserAuditService::class,[
            'queue' => true
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'name' => 'test account',
                    'email' => 'test@test.com'
                ]
            ])
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'email' => 'test1@test.com',
                    'user_type' => 'owner',
                    'first_name' => 'first name',
                    'middle_name' => 'middle name',
                    'last_name' => 'last name',
                    'product_seats' => [1],
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'email' => 'test@test.com',
                    'user_type' => 'owner',
                    'first_name' => 'first name',
                    'middle_name' => 'middle name',
                    'last_name' => 'last name',
                    'product_seats' => [1],
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'product_seats' => [1]
                ]
            ],
            [
                'body' => [
                    'user_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ]
        ]);

        $this->mockClass(Auth0ManagementService::class, [
            'getAuth0UserProfile' => [
                'email_verified' => true
            ],
            'updateUser' => [],
            'getAuth0UserProfileByEmail' => []
        ]);

        $this->mockClass(AuthorizationService::class, [
            'invalidateCachedPermissions' => true
        ]);

        $this->mockClass(RoleValidator::class, [
            'validate' => false,
            'validateAssignedRoles' => false
        ]);

        $this->mockClass(AuthnRequestService::class,[
            'data' => []
        ]);
    }

    public function testUpdateUserShouldResponseSuccess()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        $service = $this->app->make(RoleService::class);
        $role = $service->create([
            'account_id' => 1,
            'company_id' => 1,
            'name' => 'Test Role',
            'type' => Role::ADMIN
        ]);

        $payload = array (
            'user_type' => 'admin',
            'account_id' => 1,
            'first_name' => 'paul',
            'middle_name' => 'andrew',
            'last_name' => 'arboleda',
            'email' => 'test1@test.com',
            'assigned_product_seats_ids' => [1]
        );

        $this->patch(
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testUpdateUserShouldResponseUnauthorized()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        $auth0UserService  = $this->app->make(Auth0UserService::class);
        $auth0User = $auth0UserService->create([
            'status' => 'inactive',
            'auth0_user_id' => random_int(1, 999999),
            'user_id' => random_int(1, 999999),
            'account_id' => 1
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => $auth0User,
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'email' => 'test1@test.com',
                    'user_type' => 'owner',
                    'first_name' => 'first name',
                    'middle_name' => 'middle name',
                    'last_name' => 'last name',
                    'companies' => [
                        [
                            'id' => 9999
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'email' => 'test@test.com',
                    'user_type' => 'owner',
                    'first_name' => 'first name',
                    'middle_name' => 'middle name',
                    'last_name' => 'last name',
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'product_seats' => [1]
                ]
            ],
            [
                'body' => [
                    'user_id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ]
        ]);

        $payload = array (
            'user_type' => 'owner',
            'account_id' => 1,
            'first_name' => 'test first name',
            'middle_name' => 'test middle name',
            'last_name' => 'test last name',
            'assigned_product_seats_ids' => [1]
        );

        $this->patch(
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
