<?php

namespace TestsNew\Api\User;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetUserTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ],

            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'email' => 'user@example.com',
                    'account' => [
                        'id' => 1
                    ],
                    'companies' => [[
                        'id' => 1,
                        'employee_id' => '',
                        'employee_uid' => ''
                    ]],
                    'product_seats' => [[
                        'id' => 4,
                        'name' => 'time and attendance plus payroll'
                    ]],
                    'role' => [
                        'id' => 1,
                        'name' => 'RoleName'
                    ]
                ]
            ])
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    public function testGetUserSuccess()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/user/1',
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetUserIncorrectCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'status' => 401,
                'body' => null
            ]
        ]);

        $this->json(
            'GET',
            '/user/1',
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
