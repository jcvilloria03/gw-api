<?php

namespace TestsNew\Api\User;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Auth0\Auth0UserService;
use App\Authentication\AuthenticationRequestService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ResendUserVerificationTest extends TestCase
{
    use AuthorizationServiceTrait;
    use DatabaseTransactions;
    use RequestServiceTrait;

    const TARGET_URL = 'user/1/resend_verification';
    const TARGET_METHOD = 'POST';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockRequestService(AuthenticationRequestService::class,[
            [
                'body' => [
                    'sent' => true
                ]
            ]
        ]);
    }

    public function testGetSubscriptionStatsShouldResponseSuccess()
    {
        $this->markTestSkipped('Deprecated since epic#15.');

        $auth0UserService  = $this->app->make(Auth0UserService::class);
        $auth0User = $auth0UserService->create([
            'status' => 'inactive',
            'auth0_user_id' => random_int(1, 999999),
            'user_id' => random_int(1, 999999),
            'account_id' => 1
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => $auth0User,
            'getActiveUserIds' => [$auth0User->user_id],
            'setStatus' => true
        ]);

        $response = $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
                ]
            );

        ## To resend verification, user needs to be active
        $this->assertResponseStatus(Response::HTTP_NOT_ACCEPTABLE);

    }


    public function testGetSubscriptionStatsShouldResponseUnauthorized()
    {
        $this->markTestSkipped('Deprecated since epic#15.');

        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 9999
                        ]
                    ]
                ]
            ]
        ]);

        $auth0UserService  = $this->app->make(Auth0UserService::class);
        $auth0User = $auth0UserService->create([
            'status' => 'inactive',
            'auth0_user_id' => random_int(1, 999999),
            'user_id' => random_int(1, 999999),
            'account_id' => 1
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => $auth0User,
            'getActiveUserIds' => [$auth0User->user_id],
            'setStatus' => true
        ]);

        $this->json(
            self::TARGET_METHOD,
            self::TARGET_URL,
            [],
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
