<?php

namespace TestsNew\Api\User;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Role\RoleRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Auth0\Auth0ManagementService;
use App\Auth0\Auth0UserService;
use App\User\UserAuditService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UpdateUserStatusTest extends TestCase
{
    use AuthorizationServiceTrait;
    use DatabaseTransactions;
    use RequestServiceTrait;

    const TARGET_URL = 'user/%d/set_status';
    const TARGET_METHOD = 'PATCH';

    public function setUp()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'name' => 'account 1',
                    'email' => 'test email'
                ]
            ]),
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(UserAuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(Auth0ManagementService::class, [
            'getAuth0UserProfile' => [
                'email_verified' => true
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // get user
                'body' => [

                    'id' => 1,
                    'first_name' => 'first name',
                    'last_name' => 'last_name',
                    'middle_name' => 'middle name',
                    'account_id' => 1,
                    'account' => [
                        'id' => 1
                    ],
                    'companies' => [
                        [
                            'id' => 1,
                            // 'employee_id' => 1,
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
            [
                'body' => [
                    'valid' => true
                ]
            ],
            [ // get user
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
            [ // set status
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
        ]);
    }

    public function testUpdateUserStatusShouldResponseSuccess()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        $auth0UserService  = $this->app->make(Auth0UserService::class);
        $auth0User = $auth0UserService->create([
            'status' => 'inactive',
            'auth0_user_id' => random_int(1, 999999),
            'user_id' => random_int(1, 999999),
            'account_id' => 1
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => $auth0User,
            'getUser' => $auth0User,
            'getActiveUserIds' => [$auth0User->user_id],
            'setStatus' => true
        ]);

        $this->mockClass(RoleRequestService::class, [
            'getRoleByAccount' => [
                'data' => [
                    'policy' => [
                        'resource' => []
                    ]
                ]
            ],

            'getRoleType' => RoleRequestService::ROLE_ADMIN
        ]);

        $payload = [
            "status" => "active",
        ];
        $this->patch(
            sprintf(self::TARGET_URL, $auth0User->user_id),
            $payload,
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateUserStatusShouldResponseUnauthorized()
    {
        $this->markTestSkipped('Deprecated since Epic#15 Authn implementation');
        $this->mockRequestService(UserRequestService::class, [
            [ // get user
                'body' => [

                    'id' => 1,
                    'first_name' => 'first name',
                    'last_name' => 'last_name',
                    'middle_name' => 'middle name',
                    'account_id' => 1,
                    'account' => [
                        'id' => 1
                    ],
                    'companies' => [
                        [
                            'id' => 9999,
                            'employee_id' => 1,
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
            [
                'body' => [
                    'valid' => true
                ]
            ],
            [ // get user
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
            [ // set status
                'body' => [
                    'id' => 1,
                    'account_id' => 1,
                    'companies' => [
                        [
                            'id' => 1
                        ]
                    ],
                    'email' => 'test@test.com',
                    'role' => [
                        'id' => 1,
                        'name' => 'Salarium Account Owner'
                    ]
                ]
            ],
        ]);

        $auth0UserService  = $this->app->make(Auth0UserService::class);
        $auth0User = $auth0UserService->create([
            'status' => 'inactive',
            'auth0_user_id' => random_int(1, 999999),
            'user_id' => random_int(1, 999999),
            'account_id' => 1
        ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => $auth0User,
            'getUser' => $auth0User,
            'getActiveUserIds' => [$auth0User->user_id],
            'setStatus' => true
        ]);

        $payload = [
            "status" => "active",
        ];
        $this->patch(
            self::TARGET_URL,
            $payload,
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
