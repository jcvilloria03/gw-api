<?php

namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class UpdateUserLastActiveCompanyTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ],
            'setLastActiveCompany' => [
                "last_active_company_id" => 1
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);
    }

    public function testUpdateUserLastActiveCompanySuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'root.admin' => [
                    'data_scope' => [
                        'ACCOUNT' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PATCH',
            '/user/last_active_company',
            ["company_id" => 1],
            [
                'X-Authz-Entities' => 'root.admin'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateUserLastActiveCompanyUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => []
        ]);

        $this->json(
            'PATCH',
            '/user/last_active_company',
            ["company_id" => 1],
            [
                'X-Authz-Entities' => 'root.admin'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
