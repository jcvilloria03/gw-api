<?php

namespace TestsNew\Api\User;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Model\Role;
use App\Validator\RoleValidator;
use App\Auth0\Auth0ManagementService;
use App\Auth0\Auth0UserService;
use App\Role\UserRoleService;
use App\CompanyUser\CompanyUserService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CreateUserTest extends TestCase
{
    use DatabaseTransactions;
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

    }

    public function testCreateUserShouldResponseOk()
    {
        $this->markTestSkipped('Deprecated since epic#15.');

        $role = Role::create([
            'account_id' => 1,
            'company_id' => 1,
            'type' => Role::ADMIN,
            'custom_role' => true,
        ]);
        $scopeCompanyId = $role->company_id;
        $userCompanyId = $role->company_id;

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('hMSet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockClass(RoleValidator::class, [
            'validateAssignedRoles' => []
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'control_panel.users' => [
                            'data_scope' => [
                                'COMPANY' => [$scopeCompanyId]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => $userCompanyId
                        ]
                    ],
                    'product_seats' => [1]
                ]
            ],
            [ //create
                'body' => [
                    'id' => 1,
                    'account' => 'test_account_name',
                    'active' => 'inactive',
                ]
            ],
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => $userCompanyId
                        ]
                    ],
                    'product_seats' => [1]
                ]
            ]
        ]);

        $this->mockClass(UserRoleService::class, [
            'insert' => [],
            'assignOwnerOrSuperAdminRoleToUser' => []
        ]);
        $this->mockClass(CompanyUserService::class, [
            'sync' => []
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class, [
            [
                'body' => [
                    'data' => [
                        [
                            'subscriptions' => null
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'data' => []
                ]
            ],
        ]);

        $payload = [
                "user_type" => "admin",
                "account_id" => 1,
                "first_name" => "adminone",
                "middle_name"=> "adminone",
                "last_name"=> "adminone",
                "name"=> "adminone adminone adminone",
                "email"=> "p.arboleda+adminone3+@salarium.com",
                "is_company_level"=> true,
                "assigned_product_seats_ids"=> [
                    2
                ],
                "license_unit_id"=> 0,
                "subscription_product_id"=> 1
            ];
        $this->json(
            'POST',
            '/user',
            $payload,
            [
                'X-Authz-Entities' => 'control_panel.users'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }
}
