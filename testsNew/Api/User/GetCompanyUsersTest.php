<?php

namespace TestsNew\Api\User;

use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Model\Role;
use App\Validator\RoleValidator;
use App\Auth0\Auth0ManagementService;
use App\Auth0\Auth0UserService;
use App\Role\UserRoleService;
use App\CompanyUser\CompanyUserService;
use App\Account\AccountRequestService;

class GetCompanyUsersTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        $this->markTestSkipped('Deprecated due to ticket 7702: Removal of users and roles option');
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

    }

    public function testGetCompanyUsersShouldResponseOk()
    {

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('hMSet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockClass(RoleValidator::class, [
            'validateAssignedRoles' => []
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.users_and_roles.user_management' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => 1
                        ]
                    ],
                    'product_seats' => [
                        [
                            'id' => 1,
                            'name' => 'time and attendance'
                        ]    
                    ]
                ]
            ],
            [ //getAccountOrCompanyUsers
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'account_id' => 1,
                            'email' => 'testemail@test.com',
                            'user_type' => 'owner',
                            'first_name' => 'first_name',
                            'middle_name' => null,
                            'assigned_licenses_id' => [],
                            'license' => '',
                            'status' => '',
                            'product_seats' => [
                                [
                                    'id' => 1,
                                    'name' => 'time and attendance'
                                ]    
                                ],
                                'active' => true
                        ]
                    ]
                ]
            ],
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => 1
                        ]
                    ],
                    'product_seats' => [1]
                ]
            ]
        ]);

        $this->mockClass(UserRoleService::class, [
            'insert' => [],
            'assignOwnerOrSuperAdminRoleToUser' => []
        ]);
        $this->mockClass(CompanyUserService::class, [
            'sync' => []
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class, [
            [
                'body' => [
                    'data' => [
                        '1' => [
                            'id' => 1,
                            'units' => 10,
                            'currency' => 'USD',
                            'price' => 0,
                            'product' => [
                                'id' => 1,
                                'name' => 'time and attendance',
                                'code' => 'time and attendance',
                                'currency' => 'USD',
                                'price' => 0
                            ]
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'data' => []
                ]
            ],
        ]);

        $this->mockClass(Auth0UserService::class,[
            'create' => [],
            'getUser' => (object) [
                'status' => 1
            ],
            'getAuth0UserProfileByEmail' => []
        ]);

        $this->json(
            'GET', 
            '/company/1/users',
            [],
            [
                'X-Authz-Entities' => 'company_settings.users_and_roles.user_management'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyUsersShouldResponseUnauthorized()
    {

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('hMSet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockClass(RoleValidator::class, [
            'validateAssignedRoles' => []
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.users_and_roles.user_management' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => 1
                        ]
                    ],
                    'product_seats' => [
                        [
                            'id' => 1,
                            'name' => 'time and attendance'
                        ]    
                    ]
                ]
            ],
            [ //getAccountOrCompanyUsers
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'account_id' => 1,
                            'email' => 'testemail@test.com',
                            'user_type' => 'owner',
                            'first_name' => 'first_name',
                            'middle_name' => null,
                            'assigned_licenses_id' => [],
                            'license' => '',
                            'status' => '',
                            'product_seats' => [
                                [
                                    'id' => 1,
                                    'name' => 'time and attendance'
                                ]    
                                ],
                                'active' => true
                        ]
                    ]
                ]
            ],
            [ // get
                'body' => [
                    'user_id' => 29,
                    'employee_id' => null,
                    'employee_company_id' => null,
                    'account_id' => 13,
                    'user_type' => 'owner',
                    'companies' => [
                        [
                            "id" => 1
                        ]
                    ],
                    'product_seats' => [1]
                ]
            ]
        ]);

        $this->mockClass(UserRoleService::class, [
            'insert' => [],
            'assignOwnerOrSuperAdminRoleToUser' => []
        ]);
        $this->mockClass(CompanyUserService::class, [
            'sync' => []
        ]);

        $this->mockRequestService(SubscriptionsRequestService::class, [
            [
                'body' => [
                    'data' => [
                        '1' => [
                            'id' => 1,
                            'units' => 10,
                            'currency' => 'USD',
                            'price' => 0,
                            'product' => [
                                'id' => 1,
                                'name' => 'time and attendance',
                                'code' => 'time and attendance',
                                'currency' => 'USD',
                                'price' => 0
                            ]
                        ]
                    ]
                ]
            ],
            [
                'body' => [
                    'data' => []
                ]
            ],
        ]);

        $this->mockClass(Auth0UserService::class,[
            'create' => [],
            'getUser' => (object) [
                'status' => 1
            ],
            'getAuth0UserProfileByEmail' => []
        ]);

        $this->json(
            'GET', 
            '/company/999/users',
            [],
            [
                'X-Authz-Entities' => 'company_settings.users_and_roles.user_management'
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
