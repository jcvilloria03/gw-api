<?php

namespace TestsNew\Api\LeaveCredit;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateUserApprovalTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const TARGET_URL = '/user/approvals';
    const TARGET_METHOD = 'POST';
    const MODULE = 'time_and_attendance.approvals_list';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(
            [
                'user_id' => 1,
                'account_id' => 1,
                'employee_id' => 1,
                'employee_company_id' => 1
            ]
        );
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => []
        ]);

        $expected = [
            'data' => [],
            'meta' => [
                'pagination' => [
                    'total' => 2,
                    'count' => 10,
                    'per_page' => 5,
                    'current_page' => 1,
                    'total_pages' => 2,
                    'links' => []
                ]
            ]
        ];

        $this->shouldExpectRequest(self::TARGET_METHOD, '/user/1/approvals')
        ->andReturnResponse(
            new Response(
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            )
        );

        $this
            ->json(
                self::TARGET_METHOD,
                self::TARGET_URL,
                [],
                [self::HEADER => self::MODULE]
            )
        ->seeStatusCode(HttpResponse::HTTP_OK)
        ->seeJson($expected)
        ;
    }
}
