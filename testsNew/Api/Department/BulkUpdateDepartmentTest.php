<?php

namespace TestsNew\Api\Team;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Department\DepartmentEsIndexQueueService;
use App\Department\DepartmentRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class BulkUpdateDepartmentTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockRequestService(UserRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'subject' => [],
                        'userData' => []
                    ]
                ]
            ]
        ]);

        $this->mockCompanyRequestServiceCalls([
            'body' => [
                'data' => [
                    [
                        'id' => 1,
                        'name' => 'Department1',
                        'company_id' => 1,
                        'account_id' => 1,
                        'parent' => null
                    ]
                ]
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $mockCompanyRequestService = Mockery::mock(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->app->instance(CompanyRequestService::class, $mockCompanyRequestService);

        $mockAuditService = Mockery::mock(AuditService::class, [
            'queue' => true
        ]);

        $this->app->instance(AuditService::class, $mockAuditService);

        $mockDepartmentEsIndexQueueService = Mockery::mock(DepartmentEsIndexQueueService::class, [
            'queue' => true
        ]);

        $this->app->instance(DepartmentEsIndexQueueService::class, $mockDepartmentEsIndexQueueService);
    }

    public function testBulkUpdateSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.organizational_chart' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/department/bulk_update', [
            'company_id' => 1,
            'data' => [
                'id' => 1,
                'company_id' => 1,
                'name' => 'Department1',
                'parent_id' => null
            ]
            ],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testBulkUpdateCompanyAllSuccess()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.organizational_chart' => [
                            'data_scope' => [
                                'COMPANY' => ['__ALL__']
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/department/bulk_update', [
            'company_id' => 1,
            'data' => [
                'id' => 1,
                'company_id' => 1,
                'name' => 'Department1',
                'parent_id' => null
            ],
            ],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testBulkUpdateIncorrectCompany()
    {
        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.company_structure.organizational_chart' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->json('PUT', '/department/bulk_update', [
            'company_id' => 999,
            'data' => [
                'id' => 1,
                'company_id' => 999,
                'name' => 'Department1',
                'parent_id' => null
            ],
            ],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    protected function mockCompanyRequestServiceCalls($response)
    {
        $this->mockRequestService(DepartmentRequestService::class, array_fill(0, 2, $response));
    }
}
