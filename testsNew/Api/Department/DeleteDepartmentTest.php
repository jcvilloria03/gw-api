<?php

namespace TestsNew\Api\Team;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Department\DepartmentRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class DeleteDepartmentTest extends TestCase
{
    use AuthorizationServiceTrait;
    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();
        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(DepartmentRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'name' => 'Department1'
                ]
            ]),

            'delete' => $this->getJsonResponse([
                'code' => Response::HTTP_NO_CONTENT
            ]),
        ]);

        $this->mockClass(PhilippineCompanyRequestService::class, [
            'isInUse' => $this->getJsonResponse([
                'body' => [
                    'in_use' => 0
                ]
            ])
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),
        ]);
    }

    public function testDeleteSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'DELETE',
            '/department/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteSuccessAllCompany()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => ['__ALL__']
                    ]
                ]
            ]
        ]);

        $this->json(
            'DELETE',
            '/department/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeleteIncorrectCompany()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [5]
                    ]
                ]
            ]
        ]);

        $this->json(
            'DELETE',
            '/department/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
