<?php

namespace TestsNew\Api\GovernmentForm;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class BulkCreateEmployeeGovernmentFormTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.government_forms';
    const METHOD = 'POST';
    const URL = '/employee/government_forms/bir_2316/bulk/generate';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_2316',
                'attributes' => [
                    'year' => 2019,
                    'company_id' => 1,
                    'employees_id' => [1, 2]
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('POST', '/employee/government_forms/bir_2316/bulk/generate')
            ->withBody(json_encode($requestPayload))
            ->withDataScope(
                [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            )
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode(
                [
                    'data' => [
                        [
                            'id' => 1,
                            'type' => 'govt_form_bir_2316',
                            'attributes' => [
                                'employee_id' => 1,
                                'year' => '2019',
                                'agency' => 'BIR',
                                'form_name' => 'BIR 2316',
                                'payroll_period' => 'February - December 2019',
                                'employee_full_name' => 'STUHR, MARINA'
                            ]
                        ],
                        [
                            'id' => 2,
                            'type' => 'govt_form_bir_2316',
                            'attributes' => [
                                'employee_id' => 2,
                                'year' => '2019',
                                'agency' => 'BIR',
                                'form_name' => 'BIR 2316',
                                'payroll_period' => 'September - December 2019',
                                'employee_full_name' => 'BORMANN, MARTINA'
                            ]
                        ]
                    ]
                ]
            )));
        
        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_CREATED);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_2316',
                'attributes' => [
                    'year' => 2019,
                    'company_id' => 1,
                    'employees_id' => [1, 2]
                ]
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
