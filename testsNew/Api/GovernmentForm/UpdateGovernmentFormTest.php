<?php

namespace TestsNew\Api\GovernmentForm;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateGovernmentFormTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.government_forms';
    const METHOD = 'PUT';
    const URL = '/government_forms/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
        ->shouldExpectRequest('GET', '/government_forms/1')
        ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
            [
                'data' => [
                    'type' => 'govt_form_bir_1601c',
                    'attributes' => [
                        'companyId' => 1,
                        'yearFrom' => 2019,
                        'monthFrom' => 10,
                        'yearTo' => 2019,
                        'monthTo' => 10,
                    ]
                ]
            ]
        )));

        $this
            ->shouldExpectRequest('PUT', '/government_forms/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        'type' => 'govt_form_bir_1601c',
                        'id' => 1,
                        'attributes' => [
                            'companyId' => 1,
                            'yearFrom' => 2019,
                            'monthFrom' => 10,
                            'yearTo' => 2019,
                            'monthTo' => 10,
                            'isAmendedReturn' => false,
                            'anyTaxesWithheld' => true,
                            'noOfAttachedSheets' => 1,
                            'tin' => '892-489-042-398',
                            'rdo' => '048',
                            'withholdingAgentsName' => 'MARVEL CORPORATION',
                            'registeredAddress' => 'MAKATI CITY 1650 PENAFRANCIA ST. MAKATI',
                            'zipCode' => '1226',
                            'contactNumber' => '',
                            'categoryOfWithholdingAgent' => 'PRIVATE',
                            'emailAddress' => '',
                            'hasTaxRelief' => false,
                            'taxReliefSpecification' => '',
                            'totalAmountOfCompensation' => '25000.00',
                            'statutoryMinimumWageForMwe' => '0.00',
                            'otherPaysForMwe' => '0.00',
                            '13ThMonthPayAndOtherBenefits' => '0.00',
                            'deMinimisBenefits' => '0.00',
                            'employeeGovtContribution' => '725.00',
                            'otherNonTaxableCompensationAmount' => '0.00',
                            'otherNonTaxableCompensationSpecify' => '',
                            'totalNonTaxableCompensation' => '725.00',
                            'totalTaxableCompensation' => '24275.00',
                            'taxableCompensationNotSubjectToWtax' => '0.00',
                            'netTaxableCompensation' => '24275.00',
                            'totalTaxesWithheld' => '3152.00',
                            'adjustmentOfWtaxPrevMonths' => '0.00',
                            'taxesWithheldForRemittance' => '3152.00',
                            'lessTaxRemittedInReturn' => '0.00',
                            'otherRemittancesMade' => '0.00',
                            'otherRemittancesMadeSpecify' => '',
                            'totalTaxRemittancesMade' => '0.00',
                            'taxStillDue' => '3152.00',
                            'penaltySurcharge' => '0.00',
                            'penaltyInterest' => '0.00',
                            'penaltyCompromise' => '0.00',
                            'totalPenalties' => '0.00',
                            'totalAmountStillDueOverRemittance' => '3152.00',
                            'dopCashBankDrawee' => '',
                            'dopCashBankNumber' => '',
                            'dopCashBankDate' => '0000-00-00',
                            'dopCashBankAmount' => '0.00',
                            'dopCheckBankDrawee' => '',
                            'dopCheckBankNumber' => '',
                            'dopCheckBankDate' => '0000-00-00',
                            'dopCheckBankAmount' => '0.00',
                            'dopTaxBankNumber' => '',
                            'dopTaxBankDate' => '0000-00-00',
                            'dopTaxBankAmount' => '0.00',
                            'dopOtherBankSpecifics' => '',
                            'dopOtherBankDrawee' => '',
                            'dopOtherBankNumber' => '',
                            'dopOtherBankDate' => '0000-00-00',
                            'dopOtherBankAmount' => '0.00',
                            'forIndividual' => '',
                            'individualTitleDesignation' => '',
                            'individualTin' => null,
                            'forNonIndividual' => 'mimoy malicdem',
                            'nonIndividualTitleDesignation' => 'SIGNATORY',
                            'nonIndividualTin' => '821-098-391-239',
                            'taxAgentAccNo' => '',
                            'dateOfIssue' => '2019-10-01',
                            'dateOfExpiry' => '2019-10-01',
                            'lineItems' => []
                        ]
                    ]
                ]
            )));

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_1601c',
                'id' => 1,
                'attributes' => [
                    'company_id' => 1,
                    'year_from' => 2019,
                    'month_from' => 10,
                    'year_to' => 2019,
                    'month_to' => 10,
                ]
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/government_forms/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        'type' => 'govt_form_bir_1601c',
                        'attributes' => [
                            'company_id' => 1,
                            'year_from' => 2019,
                            'month_from' => 10,
                            'year_to' => 2019,
                            'month_to' => 10,
                        ]
                    ]
                ]
            )));

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_1601c',
                'id' => 1,
                'attributes' => [
                    'company_id' => 1,
                    'year_from' => 2019,
                    'month_from' => 10,
                    'year_to' => 2019,
                    'month_to' => 10,
                ]
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
