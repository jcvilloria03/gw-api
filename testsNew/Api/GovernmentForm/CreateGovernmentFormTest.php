<?php

namespace TestsNew\Api\GovernmentForm;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateGovernmentFormTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'payroll.government_forms';
    const METHOD = 'POST';
    const URL = '/government_forms';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('POST', '/government_forms')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        'type' => 'govt_form_bir_1601c',
                        'id' => 1,
                        'attributes' => [
                            'company_id' => 1,
                            'year_from' => 2019,
                            'month_from' => 10,
                            'year_to' => 2019,
                            'month_to' => 10,
                            'is_amended_return' => false,
                            'any_taxes_withheld' => true,
                            'no_of_attached_sheets' => 1,
                            'tin' => '892-489-042-398',
                            'rdo' => '048',
                            'withholding_agents_name' => 'MARVEL CORPORATION',
                            'registered_address' => 'MAKATI CITY 1650 PENAFRANCIA ST. MAKATI',
                            'zip_code' => '1226',
                            'contact_number' => '',
                            'category_of_withholding_agent' => 'PRIVATE',
                            'email_address' => '',
                            'has_tax_relief' => false,
                            'tax_relief_specification' => '',
                            'total_amount_of_compensation' => '25000.00',
                            'statutory_minimum_wage_for_mwe' => '0.00',
                            'other_pays_for_mwe' => '0.00',
                            '13th_month_pay_and_other_benefits' => '0.00',
                            'de_minimis_benefits' => '0.00',
                            'employee_govt_contribution' => '725.00',
                            'other_non_taxable_compensation_amount' => '0.00',
                            'other_non_taxable_compensation_specify' => '',
                            'total_non_taxable_compensation' => '725.00',
                            'total_taxable_compensation' => '24275.00',
                            'taxable_compensation_not_subject_to_wtax' => '0.00',
                            'net_taxable_compensation' => '24275.00',
                            'total_taxes_withheld' => '3152.00',
                            'adjustment_of_wtax_prev_months' => '0.00',
                            'taxes_withheld_for_remittance' => '3152.00',
                            'less_tax_remitted_in_return' => '0.00',
                            'other_remittances_made' => '0.00',
                            'other_remittances_made_specify' => '',
                            'total_tax_remittances_made' => '0.00',
                            'tax_still_due' => '3152.00',
                            'penalty_surcharge' => '0.00',
                            'penalty_interest' => '0.00',
                            'penalty_compromise' => '0.00',
                            'total_penalties' => '0.00',
                            'total_amount_still_due_over_remittance' => '3152.00',
                            'dop_cash_bank_drawee' => '',
                            'dop_cash_bank_number' => '',
                            'dop_cash_bank_date' => '0000-00-00',
                            'dop_cash_bank_amount' => '0.00',
                            'dop_check_bank_drawee' => '',
                            'dop_check_bank_number' => '',
                            'dop_check_bank_date' => '0000-00-00',
                            'dop_check_bank_amount' => '0.00',
                            'dop_tax_bank_number' => '',
                            'dop_tax_bank_date' => '0000-00-00',
                            'dop_tax_bank_amount' => '0.00',
                            'dop_other_bank_specifics' => '',
                            'dop_other_bank_drawee' => '',
                            'dop_other_bank_number' => '',
                            'dop_other_bank_date' => '0000-00-00',
                            'dop_other_bank_amount' => '0.00',
                            'for_individual' => '',
                            'individual_title_designation' => '',
                            'individual_tin' => null,
                            'for_non_individual' => 'mimoy malicdem',
                            'non_individual_title_designation' => 'SIGNATORY',
                            'non_individual_tin' => '821-098-391-239',
                            'tax_agent_acc_no' => '',
                            'date_of_issue' => '2019-10-01',
                            'date_of_expiry' => '2019-10-01',
                            'line_items' => []
                        ]
                    ]
                ]
            )));
        
        $requestPayload = [
            'data' => [
                'company_id' => 1,
                'year_from' => 2019,
                'month_from' => 10,
                'year_to' => 2019,
                'month_to' => 10,
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'data' => [
                'company_id' => 1,
                'year_from' => 2019,
                'month_from' => 10,
                'year_to' => 2019,
                'month_to' => 10,
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
