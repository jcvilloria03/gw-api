<?php

namespace TestsNew\Api\GovernmentForm;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCompanyGovernmentFormsBir2316EmployeesTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.government_forms';
    const METHOD = 'GET';
    const URL = '/company/1/government_forms/bir_2316/employees';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'month_from' => 1,
            'year_from' => 2019,
            'month_to' => 12,
            'year_to' => 2019,
        ];

        $this
            ->shouldExpectRequest('GET', '/company/1/government_forms/bir_2316/employees')
            ->withQuery($requestPayload)
            ->withDataScope(
                [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1, 2],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1],
                ]
            )
            ->andReturnResponse(new Response(HttpResponse::HTTP_CREATED, [], json_encode(
                [
                    'data' => [
                        [
                            'employee_id' => 1,
                            'first_name' => 'RAY',
                            'middle_name' => '',
                            'last_name' => 'CHANG',
                            'department_name' => 'Product',
                            'department_id' => 1,
                            'payroll_group_id' => 2,
                            'location_id' => 1,
                            'date_hired' => '2018-03-15',
                            'date_ended' => null,
                            'payroll' => [
                                'from' => [
                                    'month' => 1,
                                    'year' => 2019
                                ],
                                'to' => [
                                    'month' => 12,
                                    'year' => 2019
                                ]
                            ]
                        ],
                        [
                            'employee_id' => 2,
                            'first_name' => 'Chris',
                            'middle_name' => '',
                            'last_name' => 'Cope',
                            'department_name' => 'Product',
                            'department_id' => 1,
                            'payroll_group_id' => 1,
                            'location_id' => 1,
                            'date_hired' => '2019-01-28',
                            'date_ended' => null,
                            'payroll' => [
                                'from' => [
                                    'month' => 1,
                                    'year' => 2019
                                ],
                                'to' => [
                                    'month' => 12,
                                    'year' => 2019
                                ]
                            ]
                        ]
                    ]
                ]
            )));

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_CREATED);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = ['data' => [1, 2]];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
