<?php

namespace TestsNew\Api\GovernmentForm;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UpdateEmployeeGovernmentForm2316Test extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.government_forms';
    const METHOD = 'PATCH';
    const URL = '/employee/1/government_forms/bir_2316/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/company/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1
            ])));


        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));

        $this
            ->shouldExpectRequest('PATCH', '/employee/1/government_forms/bir_2316/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        'id' => 1,
                        'type' => 'govt_form_bir_2316',
                        'attributes' => [
                            'company_id' => 1,
                            'employee_id' => 1,
                            'year' => 2020,
                            'coverage_start_day' => '2020-01-01',
                            'employee_registered_zip_code' => '52019'
                        ]
                    ]
                ]
            )));

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_2316',
                'attributes' => [
                    'company_id' => 1,
                    'employee_id' => 1,
                    'year' => 2020,
                    'coverage_start_day' => '2020-01-01',
                    'employee_registered_zip_code' => '52019'
                ]
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/philippine/company/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1
            ])));

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'company_id' => 1,
                'location_id' => 1,
                'department_id' => 1,
                'position_id' => 1,
                'payroll_group' => [
                    'id' => 1
                ],
                'team_id' => 1
            ])));

        $requestPayload = [
            'data' => [
                'type' => 'govt_form_bir_2316',
                'attributes' => [
                    'company_id' => 1,
                    'employee_id' => 1,
                    'year' => 2020,
                    'coverage_start_day' => '2020-01-01',
                    'employee_registered_zip_code' => '52019'
                ]
            ]
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
