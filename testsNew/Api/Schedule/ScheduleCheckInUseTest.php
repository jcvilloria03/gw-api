<?php

namespace TestsNew\Api\Schedule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Schedule\ScheduleRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class ScheduleCheckInUseTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(ScheduleRequestService::class, [
            'checkInUse' => $this->getJsonResponse([
                'body' => [
                    'in_use' => 0
                ]
            ]),
        ]);
    }

    public function testCheckInUseSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.companies' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/schedule/check_in_use', [
            'company_id' => '1',
            'schedule_ids' => [1, 2]
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.schedules'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testCheckInUseError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'control_panel.companies' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/schedule/check_in_use', [
            'company_id' => '999',
            'schedule_ids' => [1, 2]
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.schedules'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
