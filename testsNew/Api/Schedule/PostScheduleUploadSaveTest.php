<?php

namespace TestsNew\Api\Schedule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Schedule\ScheduleUploadTask;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use Bschmitt\Amqp\Facades\Amqp;

class PostScheduleUploadSaveTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockClass(UserRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'account_id' => 1,
                    'companies' => [
                        ['id' => 1],
                        ['id' => 2]
                    ]
                ]
            ]),
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(ScheduleUploadTask::class, [
            'create' => true,
            'updateSaveStatus' => true,
            'setUserId' => true,
            'getId' => 1,
            'getS3Bucket' => 'http://bucker.aws',

        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        Amqp::shouldReceive('publish');
    }

    public function testScheduleUploadSaveSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->post(
            '/schedule/upload/save',
            [
                'company_id' => 1,
                'job_id' => 1
            ],
            ['x-authz-entities' => 'time_and_attendance.schedules']
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testScheduleUploadSaveUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->post(
            '/schedule/upload/save',
            [
                'company_id' => 999,
                'job_id' => 1
            ],
            ['x-authz-entities' => 'time_and_attendance.schedules']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
