<?php

namespace TestsNew\Api\Schedule;

use App\CSV\CsvValidator;
use App\Schedule\ScheduleUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class PostScheduleUploadTest extends TestCase
{
    use AuthorizationServiceTrait;
    use RequestServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'time_and_attendance.schedules';
    const METHOD = 'POST';
    const URL = '/schedule/upload';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest(self::METHOD, self::URL)
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, []));

        $this->mockClass(CsvValidator::class, [
            'validate' => true
        ]);

        $this->mockClass(ScheduleUploadTask::class, [
            'create' => true,
            'getId' => '123',
            'saveFile' => true,
            'getS3Bucket' => true
        ]);

        $files = [
            'file' => UploadedFile::fake()->create('schedule.csv')
        ];

        $params = [
            'company_id' => 1,
            'schedules_ids' => [1]
        ];

        $server = $this->transformHeadersToServerVars([
            self::HEADER => self::MODULE,
            'Content-Type' => 'multipart/form-data'
        ]);

        $this->call(self::METHOD, self::URL, $params, [], $files, $server);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $files = [
            'file' => UploadedFile::fake()->create('schedule.csv')
        ];

        $params = [
            'company_id' => 1,
            'schedules_ids' => [1]
        ];

        $server = $this->transformHeadersToServerVars([
            self::HEADER => self::MODULE,
            'Content-Type' => 'multipart/form-data'
        ]);

        $this->call(self::METHOD, self::URL, $params, [], $files, $server);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }

}
