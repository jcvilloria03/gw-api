<?php

namespace TestsNew\Api\Schedule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Schedule\ScheduleRequestService;
use App\User\UserRequestService;
use App\Company\CompanyRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class ScheduleIsNameAvailableTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            'getAccountId' => [
                'body' => [
                    'account_id' => 1
                ]
            ],
        ]);

        $this->mockClass(ScheduleRequestService::class, [
            'isNameAvailable' => $this->getJsonResponse([
                'body' => [
                    'available' => true
                ]
            ])
        ]);
    }

    public function testIsNameAvailableSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json('POST', '/company/1/schedule/is_name_available', [
            'name' => 'Test'
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.schedules'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testIsNameAvailableUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                ]
            ]
        ]);

        $this->json('POST', '/company/1/schedule/is_name_available', [
            'name' => 'Test'
        ],
        [
            'X-Authz-Entities' => 'time_and_attendance.schedules'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
