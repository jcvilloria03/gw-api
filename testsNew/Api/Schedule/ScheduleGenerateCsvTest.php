<?php

namespace TestsNew\Api\Schedule;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Schedule\ScheduleRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class ScheduleGenerateCsvTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1,
            'company_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);


        $this->mockClass(ScheduleRequestService::class, [
            'generateCsv' => $this->getJsonResponse([
                'body' => [
                    'file_name' => 'sample_file_name'
                ]
            ]),
        ]);
    }

    public function testScheduleUploadSaveSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->post(
            '/company/1/schedules/generate_csv',
            [],
            ['x-authz-entities' => 'time_and_attendance.schedules']
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testScheduleUploadSaveError()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'time_and_attendance.schedules' => [
                ]
            ]
        ]);

        $this->post(
            '/company/1/schedules/generate_csv',
            [],
            ['x-authz-entities' => 'time_and_attendance.schedules']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
