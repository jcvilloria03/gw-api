<?php

namespace TestsNew\Api\OtherIncome;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CheckCompanyOtherIncomeIsAvailableToDeleteTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array ...$dataScopes)
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        foreach ($dataScopes as $dataScope) {
            $this->getRequestStorage(AuthzRequest::getStorageName())
                ->push($dataScope);
        }
    }

    public function testCheckOtherIncomeIsAvailableToDeleteShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData(
            ['module' => 'payroll.commissions', 'data_scope' => ['COMPANY' => [1]]],
            ['module' => 'payroll.bonuses', 'data_scope' => ['COMPANY' => [1]]]
        );

        $inputs = ['ids' => [1, 2]];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_incomes')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    ['id' => 1, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommission'],
                    ['id' => 2, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonus'],
                ],
            ])))
        ;
        $expected = ['unavailable' => 0];
        $this
            ->shouldExpectRequest('POST', '/company/1/other_income/is_delete_available')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $this
            ->json(
                'POST',
                '/company/1/other_income/is_delete_available',
                $inputs,
                ['X-Authz-Entities' => 'payroll.commissions']
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected)
        ;
    }

    public function testCheckOtherIncomeIsAvailableToDeleteShouldResponseUnauthorizedIds()
    {
        $this->addUserAndAccountEssentialData(
            ['module' => 'payroll.bonuses', 'data_scope' => ['COMPANY' => [1]]]
        );

        $inputs = ['ids' => [1, 2]];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_incomes')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    ['id' => 1, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommission'],
                    ['id' => 2, 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonus'],
                ],
            ])))
        ;

        $this
            ->json(
                'POST',
                '/company/1/other_income/is_delete_available',
                $inputs,
                ['X-Authz-Entities' => 'payroll.commissions']
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Not authorized. Ids: 1', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testCheckOtherIncomeIsAvailableToDeleteShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'payroll.commissions',
            'data_scope' => ['COMPANY' => [2]],
        ]);

        $inputs = ['ids' => [1, 2, 3]];

        $this
            ->json(
                'POST',
                '/company/1/other_income/is_delete_available',
                $inputs,
                ['X-Authz-Entities' => 'payroll.commissions']
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }

    public function testCheckOtherIncomeIsAvailableToDeleteShouldResponseNotFound()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'payroll.commissions',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $inputs = ['ids' => [1, 2, 3]];
        $this
            ->shouldExpectRequest('GET', '/company/1/other_incomes')
            ->once()
            ->withBody(json_encode($inputs))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [['id' => 1], ['id' => 2]]
            ])))
        ;

        $this
            ->json(
                'POST',
                '/company/1/other_income/is_delete_available',
                $inputs,
                ['X-Authz-Entities' => 'payroll.commissions']
            )
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson([
                'message' => 'Specified Other Income(s) not found. Id(s): 3',
                'status_code' => HttpResponse::HTTP_NOT_FOUND
            ])
        ;
    }
}
