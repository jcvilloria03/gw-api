<?php

namespace TestsNew\Api\OtherIncome;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\UploadedFile;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\AwsTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class UploadCompanyOtherIncomeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use AwsTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.allowances';
    const METHOD = 'POST';
    const URL = '/company/1/other_income/allowance/upload';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->mockAwsSdk([
            [
                'Body' => ''
            ]
        ]);

        $this->getRequestStorage(AuthzRequest::getStorageName())->push(
            [
                'module' => self::MODULE,
                'data_scope' => [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1, 3, 2],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1, 2],
                ],
            ],
            [
                'module' => 'payroll.allowances',
                'data_scope' => [
                    'COMPANY' => [3],
                    'PAYROLL_GROUP' => [4],
                    'POSITION' => [4],
                    'DEPARTMENT' => [5],
                    'TEAM' => [6],
                    'LOCATION' => [7],
                ],
            ]
        );

        $this
            ->shouldExpectRequest('POST', '/company/1/allowance_type/upload')
            ->withDataScope([
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1, 3, 2],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1, 2],
            ])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    'id' => 'kjoasy87fsa87f6s9a0sf8'
                ]
            ])));
        
        $requestPayload = [
            'company_id' => 1
        ];
        
        $this->call(
            self::METHOD,
            self::URL,
            $requestPayload,
            [],
            ['file' => UploadedFile::fake()->create('file.csv')],
            $this->transformHeadersToServerVars([
                self::HEADER => self::MODULE,
                'Content-type' => 'multipart/form-data'
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [999],
                'PAYROLL_GROUP' => [999],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'company_id' => 1
        ];
        
        $this->call(
            self::METHOD,
            self::URL,
            $requestPayload,
            [],
            ['file' => UploadedFile::fake()->create('file.csv')],
            $this->transformHeadersToServerVars([
                self::HEADER => self::MODULE,
                'Content-type' => 'multipart/form-data'
            ])
        );

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
