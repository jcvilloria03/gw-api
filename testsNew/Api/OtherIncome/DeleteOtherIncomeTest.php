<?php

namespace TestsNew\Api\OtherIncome;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class DeleteOtherIncomeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.people.adjustments';
    const METHOD = 'DELETE';
    const URL = '/company/1/other_income';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1, 2],
            ],
        ]);
        
        $requestPayload = [
            'ids' => [1,2]
        ];

        $this
            ->shouldExpectRequest('GET', '/company/1/other_incomes')
            ->withBody(json_encode($requestPayload))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        [
                            'id' => 1,
                            'type_id' => 3,
                            'company_id' => 1,
                            'amount' => 500,
                            'reason' => 'ncov',
                            'type_name' => 'App\\Model\\PhilippineAdjustment',
                            'release_details' => [
                                [
                                    'id' => 1,
                                    'date' => '2020-05-17',
                                    'disburse_through_special_pay_run' => false,
                                    'payroll_id' => null,
                                    'status' => false
                                ]
                            ],
                            'type' => [
                                'id' => 3,
                                'company_id' => 0,
                                'name' => 'TAXABLE_INCOME',
                                'fully_taxable' => false,
                                'max_non_taxable' => 0,
                                'other_income_type_subtype_id' => 1,
                                'other_income_type_subtype_type' => 'App\\Model\\PhilippineAdjustmentType',
                                'deleted_at' => null,
                                'other_income_type_subtype' => [
                                    'id' => 1
                                ]
                            ],
                            'recipients' => [
                                'employees' => [
                                    [
                                        'id' => 1,
                                        'other_income_id' => 1,
                                        'recipient_id' => 1,
                                        'type' => 'employees',
                                        'is_from_payroll_screen' => 0
                                    ]
                                ]
                            ]
                        ],
                        [
                            'id' => 2,
                            'type_id' => 3,
                            'company_id' => 1,
                            'amount' => 500,
                            'reason' => 'ncov',
                            'type_name' => 'App\\Model\\PhilippineAdjustment',
                            'release_details' => [
                                [
                                    'id' => 2,
                                    'date' => '2020-05-17',
                                    'disburse_through_special_pay_run' => false,
                                    'payroll_id' => null,
                                    'status' => false
                                ]
                            ],
                            'type' => [
                                'id' => 3,
                                'company_id' => 0,
                                'name' => 'TAXABLE_INCOME',
                                'fully_taxable' => false,
                                'max_non_taxable' => 0,
                                'other_income_type_subtype_id' => 1,
                                'other_income_type_subtype_type' => 'App\\Model\\PhilippineAdjustmentType',
                                'deleted_at' => null,
                                'other_income_type_subtype' => [
                                    'id' => 1
                                ]
                            ],
                            'recipients' => [
                                'employees' => [
                                    [
                                        'id' => 2,
                                        'other_income_id' => 2,
                                        'recipient_id' => 2,
                                        'type' => 'employees',
                                        'is_from_payroll_screen' => 0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            )));
        
        $this
            ->shouldExpectRequest('DELETE', '/company/1/other_income')
            ->withBody(json_encode($requestPayload))
            ->withDataScope(
                [
                    'COMPANY' => [1],
                    'PAYROLL_GROUP' => [1],
                    'POSITION' => [1],
                    'DEPARTMENT' => [1],
                    'TEAM' => [1],
                    'LOCATION' => [1, 2],
                ]
            )
            ->andReturnResponse(new Response(HttpResponse::HTTP_NO_CONTENT, [], json_encode(null)));
        
        Amqp::shouldReceive('publish');

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_NO_CONTENT);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'ids' => [3]
        ];

        $this
            ->shouldExpectRequest('GET', '/company/1/other_incomes')
            ->withBody(json_encode($requestPayload))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        [
                            'id' => 3,
                            'type_id' => 3,
                            'company_id' => 44,
                            'amount' => 500,
                            'reason' => 'ncov',
                            'type_name' => 'App\\Model\\PhilippineAdjustment',
                        ]
                    ]
                ]
            )));
        

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
