<?php

namespace TestsNew\Api\OtherIncome;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetCompanyOtherIncomeUploadStatusTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testGetOtherIncomeUploadStatusShouldResponseSuccess()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.adjustments',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('GET', '/company/1/adjustment_type/upload/status?job_id=123456')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => ['attributes' => ['status' => 'PENDING']],
            ])));

        $this
            ->json(
                'GET',
                '/company/1/other_income/adjustment/upload/status?job_id=123456',
                [],
                ['x-authz-entities' => 'employees.adjustments']
            )
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson(['status' => 'validating', 'errors' => null]);

    }

    public function testGetOtherIncomeUploadStatusShouldResponseUnauthorized()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'payroll.bonuses',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->json(
                'GET',
                '/company/1/other_income/adjustment/upload/status?job_id=123456',
                [],
                ['x-authz-entities' => 'employees.adjustments']
            )
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Not authorized.', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }

    public function testGetOtherIncomeUploadStatusShouldResponseNotFound()
    {
        $this->addUserAndAccountEssentialData([
            'module' => 'employees.adjustments',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $this
            ->shouldExpectRequest('GET', '/company/1/adjustment_type/upload/status?job_id=123456')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_BAD_REQUEST, [], json_encode([
                'message' => 'Job not found'
            ])));

        $this
            ->json(
                'GET',
                '/company/1/other_income/adjustment/upload/status?job_id=123456',
                [],
                ['x-authz-entities' => 'employees.adjustments']
            )
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson(['message' => 'Job not found', 'status_code' => HttpResponse::HTTP_NOT_FOUND]);
    }
}