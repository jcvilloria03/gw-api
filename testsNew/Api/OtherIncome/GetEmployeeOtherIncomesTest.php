<?php

namespace TestsNew\Api\OtherIncome;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetEmployeeOtherIncomesTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(OtherIncomeRequestService::class, [
            'getEmployeeOtherIncomes' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'company_id' => 1
                        ]
                    ]
                ]
            ])
        ]);
    }

    /**
     * @dataProvider getValidIncomeTypes
     */
    public function testGetEmployeeOtherIncomesSuccess(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/1/employee/1/other_incomes/%s', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    /**
     * @dataProvider getInvalidIncomeTypes
     */
    public function testGetEmployeeOtherIncomesUnauthorized(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/1/employee/1/other_incomes/%s', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @dataProvider getValidIncomeTypes
     */
    public function testGetEmployeeOtherIncomesInvalidCompany(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/999/employee/1/other_incomes/%s', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );
        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function getValidIncomeTypes()
    {
        yield [
            'module' => 'employees.people.allowances',
            'type' => 'allowance_type'
        ];

        yield [
            'module' => 'employees.people.bonuses',
            'type' => 'bonus_type'
        ];

        yield [
            'module' => 'employees.people.commissions',
            'type' => 'commission_type'
        ];

        yield [
            'module' => 'employees.people.deductions',
            'type' => 'deduction_type'
        ];

        yield [
            'module' => 'employees.people.adjustments',
            'type' => 'adjustment_type'
        ];
    }

    public function getInvalidIncomeTypes()
    {
        yield [
            'module' => 'employees.people.allowances',
            'type' => 'adjustment_type'
        ];

        yield [
            'module' => 'employees.people.bonuses',
            'type' => 'deduction_type'
        ];

        yield [
            'module' => 'employees.people.commissions',
            'type' => 'bonus_type'
        ];

        yield [
            'module' => 'employees.people.deductions',
            'type' => 'allowance_type'
        ];

        yield [
            'module' => 'employees.people.adjustments',
            'type' => 'allowance_type'
        ];
    }

}
