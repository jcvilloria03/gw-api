<?php

namespace TestsNew\Api\Holiday;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use App\OtherIncome\OtherIncomeUploadTask;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetUploadPreviewTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(OtherIncomeRequestService::class, [
            'getUploadPreview' => $this->getJsonResponse([
                'body' => []
            ])
        ]);

        $this->mockClass(OtherIncomeUploadTask::class, [
            'create' => true
        ]);
    }

    /**
     * @dataProvider getValidIncomeTypes
     */
    public function testGetUploadPreviewSuccess(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/other_income/%s/upload/preview?company_id=1&job_id=1', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    /**
     * @dataProvider getInvalidIncomeTypes
     */
    public function testGetUploadPreviewUnauthorized(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/other_income/%s/upload/preview?company_id=1&job_id=1', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @dataProvider getValidIncomeTypes
     */
    public function testGetUploadPreviewInvalidCompany(string $module, string $type)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                $module => [
                    'data_scope' => [
                        'COMPANY' => [123],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $url = sprintf('/company/other_income/%s/upload/preview?company_id=1&job_id=1', $type);

        $this->json(
            'GET',
            $url,
            [],
            [
                'x-authz-entities' => $module
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function getValidIncomeTypes()
    {
        yield [
            'module' => 'employees.allowances',
            'type' => 'allowance'
        ];

        yield [
            'module' => 'employees.deductions',
            'type' => 'deduction'
        ];
    }

    public function getInvalidIncomeTypes()
    {
        yield [
            'module' => 'employees.bonuses',
            'type' => 'deduction'
        ];

        yield [
            'module' => 'employees.deductions',
            'type' => 'allowance'
        ];
    }

}
