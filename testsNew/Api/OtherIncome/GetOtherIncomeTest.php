<?php

namespace TestsNew\Api\OtherIncome;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetOtherIncomeTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
    }


    public function testGetOtherIncomeShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people.bonuses',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['id' => 1, 'name' => 'Test', 'company_id' => 1, 'type_name' => 'App\Model\PhilippineBonus'];

        $this->shouldExpectRequest('GET', '/other_income/1')
            ->once()
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode($expected))
            );

        $this->json(
            'GET',
            '/other_income/1',
            [],
            [
                'x-authz-entities' => 'employees.people.bonuses'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJson($expected);
    }

    public function testGetOtherIncomeShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people.bonuses',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $response = ['id' => 1, 'name' => 'Test', 'company_id' => 1, 'type_name' => 'App\Model\PhilippineCommission'];

        $this->shouldExpectRequest('GET', '/other_income/1')
            ->once()
            ->andReturnResponse(
                new Response(HttpResponse::HTTP_OK, ['Content-Type' => 'application/json'], json_encode($response))
            );

        $this->json(
            'GET',
            '/other_income/1',
            [],
            [
                'x-authz-entities' => 'employees.people.bonuses'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED]);
    }

    public function testGetOtherIncomeShouldResponseNotFound()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => 'employees.people.bonuses',
            'data_scope' => ['COMPANY' => [1]],
        ]);

        $expected = ['message' => 'Other income not found.', 'status_code' => 404];

        $this->shouldExpectRequest('GET', '/other_income/1')
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_NOT_FOUND,
                ['Content-Type' => 'application/json'],
                json_encode($expected)
            ));

        $this->json(
            'GET',
            '/other_income/1',
            [],
            [
                'x-authz-entities' => 'employees.people.bonuses'
            ]
        );
        $this
            ->seeStatusCode(HttpResponse::HTTP_NOT_FOUND)
            ->seeJson($expected);
    }
}
