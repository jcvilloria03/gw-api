<?php
namespace TestsNew\Api\Position;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Position\PositionRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;

class GetPositionNameAvailabilityTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        Redis::shouldReceive('get')
        ->andReturn(null);
        Redis::shouldReceive('set')
        ->andReturn(null);
        Redis::shouldReceive('hGet')
        ->andReturn(null);
        Redis::shouldReceive('hSet')
        ->andReturn(null);
        Redis::shouldReceive('hMSet')
        ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);
        
        $this->mockRequestService(PositionRequestService::class, [
            'isNameAvailable' => [
                'body' => ['available' => true]
            ],
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            'getAccountId' => [
                'body' => [
                    'account_id' => 1
                ]
            ],
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

    }

    public function testResponseAvailableSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart.position' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'company/1/position/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.organizational_chart.position'
            ]
        );

        $this->assertResponseOk();
        $this->seeJson(['available' => true]);
    }

    public function testResponseAvailableUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart.position' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'POST',
            'company/99999999/position/is_name_available',
            ['name' => 'Test name 1'],
            [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Authz-Entities' => 'company_settings.company_structure.organizational_chart.position'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

}
