<?php

namespace TestsNew\Api\Position;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Position\PositionRequestService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Redis;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class DeletePositionTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(PhilippineCompanyRequestService::class, [
            'isInUse' => $this->getJsonResponse([
                'body' => [
                    'in_use' => 0
                ]
            ])
        ]);

        $this->mockClass(PositionRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'account_id' => 1,
                    'name' => 'Position1'
                ]
            ]),

            'delete' => $this->getJsonResponse([
                'code' => Response::HTTP_NO_CONTENT
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('del');
    }

    public function testDeletePositionSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'DELETE',
            '/position/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    public function testDeletePositionUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->json(
            'DELETE',
            '/position/1',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
