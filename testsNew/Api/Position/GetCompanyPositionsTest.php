<?php

namespace TestsNew\Api\Position;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Position\PositionRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetCompanyPositionsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockClass(PositionRequestService::class, [
            'getCompanyPositions' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        'id' => 1,
                        'company_id' => 1,
                        'account_id' => 1,
                        'name' => 'PositionName'
                    ]
                ]
            ])
        ]);
    }

    public function testGetCompanyPositionsSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/positions',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testGetCompanyPositionsUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);


        $this->json(
            'GET',
            '/company/1/positions',
            [],
            [
                'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
            ]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
