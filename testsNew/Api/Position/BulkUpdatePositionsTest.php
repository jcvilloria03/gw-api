<?php

namespace TestsNew\Api\Position;

use App\Account\AccountRequestService;
use App\Audit\AuditService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Position\PositionEsIndexQueueService;
use App\Position\PositionRequestService;
use App\User\UserRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class BulkUpdatePositionsTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ]),

            'progress' => $this->getJsonResponse([
                'body' => [
                    'company' => true
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(PositionRequestService::class, [
            'getCompanyPositions' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1,
                            'company_id' => 1,
                            'account_id' => 1,
                            'name' => 'Position1'
                        ]
                    ]
                ]
            ]),

            'bulkUpdate' => $this->getJsonResponse([
                'body' => [
                    'data' => [
                        [
                            'id' => 1
                        ]
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => true
        ]);

        $this->mockClass(PositionEsIndexQueueService::class, [
            'queue' => true
        ]);
    }

    public function testBulkUpdatePositionSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [1]
                    ]
                ]
            ]
        ]);

        $this->put('/position/bulk_update', [
            'company_id' => 1,
            'data' => []
        ],[
            'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
        ]);

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testBulkUpdatePositionUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'company_settings.company_structure.organizational_chart' => [
                    'data_scope' => [
                        'COMPANY' => [999]
                    ]
                ]
            ]
        ]);

        $this->put('/position/bulk_update', [
            'company_id' => 1,
            'data' => [],
        ],[
            'x-authz-entities' => 'company_settings.company_structure.organizational_chart'
        ]);

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
