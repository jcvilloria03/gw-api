<?php

namespace TestsNew\Api\Ess\HoursWorked;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetHoursWorkedTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.requests.time_dispute';
    const METHOD = 'GET';
    const URL = '/ess/hours_worked';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);

        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/company/1/hours_worked')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'data' => [
                    'Regular'
                ]
            ])))
        ;

        $this->json('GET', '/ess/hours_worked', [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->json('GET', '/ess/hours_worked', [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
