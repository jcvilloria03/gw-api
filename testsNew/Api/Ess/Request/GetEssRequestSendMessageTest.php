<?php

namespace TestsNew\Api\Ess\Request;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssRequestSendMessageTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.approvals';
    const METHOD = 'POST';
    const URL = '/ess/request/send_message';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/employee/1/workflows')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([])));

        $this
            ->shouldExpectRequest('GET', '/employee_request/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                (object) [
                    'id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1,
                    'request_type' => 'leave_request',
                    'request' => [
                        'id' => 1,
                        'leaves' => [
                            [
                                'date' => '2020-07-01'
                            ]
                        ]
                    ],
                    'status' => 'Pending',
                    'messages_count' => 0,
                    'created_at' => '2020-06-22 08:43'
                ]
            ])));

        $this
            ->shouldExpectRequest('POST', '/request_message')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'request_id' => 1,
                'content' => 'test',
                'sender_id' => 1,
                'params' => [
                    'sender' => [
                        'employee_id' => 1,
                        'user_id' => 1,
                        'name' => 'Test Employee',
                        'email' => 'test@example.com'
                    ]
                ]
            ])));

        
        $requestPayload = [
            'content' => 'test',
            'request_id' => 1
        ];

        Amqp::shouldReceive('publish');
        
        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $requestPayload = [
            'content' => 'test',
            'request_id' => 1
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
