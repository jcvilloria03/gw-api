<?php

namespace TestsNew\Api\Ess\Request;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssEmployeeRequestsTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.dashboard';
    const METHOD = 'GET';
    const URL = '/ess/employee/requests?page=1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestData = [
            'data' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1,
                    'workflow_id' => 1,
                    'request_type' => 'leave_request',
                    'request' => [
                        'id' => 1
                    ],
                    'status' => 'Pending',
                    'messages_count' => 0,
                    'created_at' => '2020-06-22 08:43'
                ],
                [
                    'id' => 2,
                    'company_id' => 1,
                    'employee_id' => 1,
                    'workflow_id' => 1,
                    'request_type' => 'overtime_request',
                    'request' => [
                        'id' => 2,
                        'date' => '2019-07-11'
                    ],
                    'status' => 'Approved',
                    'messages_count' => 1,
                    'created_at' => '2019-07-25 17:57'
                ]
            ],
            'meta' => [
                'pagination' => [
                    'total' => 2,
                    'count' => 2,
                    'per_page' => 2,
                    'current_page' => 1,
                    'total_pages' => 1,
                    'links' => [
                        'next' => 'http://rq-api/employee/1/requests?page=2'
                    ]
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1/requests')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($requestData)));

        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
