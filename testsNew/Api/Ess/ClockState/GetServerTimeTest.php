<?php

namespace TestsNew\Api\Ess\ClockState;

use App\Http\Middleware\AuthzMiddleware;
use Mockery;
use Carbon\Carbon;
use TestsNew\Api\TestCase;
use App\Http\Middleware\Tracking;
use App\Http\Middleware\UserMiddleware;
use App\Http\Middleware\Auth0Middleware;
use App\Http\Middleware\AccessMiddleware;
use App\Http\Middleware\EmployeeMiddleware;
use App\Http\Middleware\LogRequestResponse;
use App\Http\Middleware\SubscriptionMiddleware;

class GetServerTimeTest extends TestCase
{
    public function testGetServerTime()
    {
        $now = Carbon::create(2020, 6, 3, 12, 00, 00);

        Carbon::setTestNow($now);

        $this->json(
            'GET',
            '/ess/time',
            [],
            [
                'x-authz-entities' => 'ess.dashboard'
            ]
        );

        $this->seeJson(['data' => [
            'server_timestamp' => $now->timestamp,
            'conversion'       => [
                'datetime'   => $now->format('Y-m-d H:i:s'),
                'timezone'   => $now->timezoneName,
                'utc_offset' => $now->getOffset()
            ]
        ]]);
    }

    public function testGetServerTimeWithTimezoneThrowsErrorOnInvalidTimezone()
    {
        $now = Carbon::create(2020, 6, 3, 12, 00, 00, 'UTC');

        Carbon::setTestNow($now);

        $this->json(
            'GET',
            '/ess/time?timezone=invalid',
            [],
            [
                'x-authz-entities' => 'ess.dashboard'
            ]
        );
        $this->assertResponseStatus(406);
        $this->seeJson(['message' => "Unknown or bad timezone (invalid)", 'status_code' => 406]);
    }

    public function testGetServerTimeWithTimezone()
    {
        $now = Carbon::create(2020, 6, 3, 12, 00, 00, 'UTC');

        Carbon::setTestNow($now);

        $this->json(
            'GET',
            '/ess/time?timezone=UTC',
            [],
            [
                'x-authz-entities' => 'ess.dashboard'
            ]
        );
        $this->seeJson(['data' => [
            'server_timestamp' => $now->timestamp,
            'conversion'       => [
                'datetime'   => $now->format('Y-m-d H:i:s'),
                'timezone'   => 'UTC',
                'utc_offset' => 0
            ]
        ]]);
    }

    public function tearDown()
    {
        parent::tearDown();
        Carbon::setTestNow();
    }
}
