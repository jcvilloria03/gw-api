<?php

namespace TestsNew\Api\Ess\ClockState;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class CreateEssClockstateLogTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.dashboard';
    const METHOD = 'POST';
    const URL = '/ess/clock_state/log';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldRespondSuccess()
    {
        $this->markTestSkipped('Unable to replicate error encounter in dev. temporary skipped.');
        $now = Carbon::create(2020, 6, 30, 23, 00, 00, 'Asia/Manila');
        Carbon::setTestNow($now);

        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $requestPayload = [
            'origin' => 'Web Bundy',
            'state' => 1,
            'tags' => []
        ];

        $this
            ->shouldExpectRequest('GET', '/employee/1/shifts')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'data' => [
                        [
                            'schedule' => [
                                'allowed_time_methods' => ['Web Bundy']
                            ]
                        ]
                    ]
                ]
            )));

        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'id' => 1,
                    'location_id' => 1,
                    'location_name' => 'Makati',
                    'time_attendance' => [
                        'primary_location_id' => 1,
                        'secondary_location_id' => 2,
                    ]
                ]
            )));

        $this
            ->shouldExpectRequest('GET', '/time_attendance_locations/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'id' => 2,
                    'name' => 'Makati City',
                    'account_id' => 1,
                    'company_id' => 1,
                    'is_headquarters' => true,
                    'address_bar' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'location_pin' => '14.569222,121.02257199999997',
                    'first_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'second_address_line' => '1650 Peñafrancia, Makati, 1208 Metro Manila, Philippines',
                    'city' => 'Makati City',
                    'country' => 'Philippines',
                    'region' => 'NCR',
                    'zip_code' => '1240',
                    'ip_addresses' => [],
                ]
            )));

        $this
            ->shouldExpectRequest('GET', '/last_timeclock/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
                [
                    'state' => false,
                    'timestamp' => 1593509419
                ]
            )));

        $this
            ->shouldExpectRequest('PUT', '/log')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([])));

        // $this
        //     ->shouldExpectRequest('GET', '/attendance/1/2020-06-30/calculate')
        //     ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(
        //         [
        //             'data' => [
        //                 'job_id' => '87fds79v97zxvy87z',
        //             ]
        //         ]
        //     )));

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE, 'HTTP_X_FORWARDED_FOR' => '05.20.20.19']);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $requestPayload = [
            'origin' => 'Web Bundy',
            'state' => 1,
            'tags' => []
        ];

        $this->json(self::METHOD, self::URL, $requestPayload, [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
