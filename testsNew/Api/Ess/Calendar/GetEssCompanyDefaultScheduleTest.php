<?php

namespace TestsNew\Api\Ess\Calendar;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssCompanyDefaultScheduleTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.dashboard';
    const METHOD = 'GET';
    const URL = '/ess/company/1/default_schedule';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $scheduleData = [
            'data' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'day_of_week' => 1,
                    'day_type' => 'regular',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 2,
                    'company_id' => 1,
                    'day_of_week' => 2,
                    'day_type' => 'regular',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 3,
                    'company_id' => 1,
                    'day_of_week' => 3,
                    'day_type' => 'regular',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 4,
                    'company_id' => 1,
                    'day_of_week' => 4,
                    'day_type' => 'regular',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 5,
                    'company_id' => 1,
                    'day_of_week' => 5,
                    'day_type' => 'regular',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 6,
                    'company_id' => 1,
                    'day_of_week' => 6,
                    'day_type' => 'rest_day',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ],
                [
                    'id' => 7,
                    'company_id' => 1,
                    'day_of_week' => 7,
                    'day_type' => 'rest_day',
                    'work_start' => '08:00:00',
                    'work_break_start' => '12:00:00',
                    'work_break_end' => '13:00:00',
                    'work_end' => '17:00:00',
                    'total_hours' => '09:00'
                ]
            ]
        ];

        $this
            ->shouldExpectRequest('GET', '/default_schedule')
            ->withQuery(['company_id' => 1])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($scheduleData)));
        
        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
