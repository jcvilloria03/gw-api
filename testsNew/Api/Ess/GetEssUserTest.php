<?php

namespace TestsNew\Api\Ess;

use App\Auth0\Auth0UserService;
use App\Model\UserRole;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\MockConnectionTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssUserTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use MockConnectionTrait;
    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'root.ess';
    const METHOD = 'GET';
    const URL = '/ess/user/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldResponseSuccess()
    {
        $this->markTestSkipped('Deprecated since epic#15.');
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/user/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'email' => 'salariumqualityautomation+employee+static@gmail.com',
                'user_type' => 'employee',
                'first_name' => 'CRBAC',
                'middle_name' => '',
                'last_name' => 'Employee',
                'full_name' => 'CRBAC Employee',
                'last_active_company_id' => null,
                'active' => 'active',
                'role' => [
                    [
                        'id' => 1,
                        'account_id' => 1,
                        'company_id' => 1,
                        'name' => 'Employee',
                        'type' => 'Employee',
                        'custom_role' => false,
                        'module_access' => [
                            'HRIS',
                            'T&A',
                            'Payroll'
                        ],
                        'deleted_at' => null
                    ]
                ],
                'account' => [
                    'id' => 1,
                    'name' => 'Owner',
                    'email' => 'salariumqualityautomation+crbac+owner@gmail.com',
                    'subscriptions' => [
                        'id' => 4,
                        'name' => 'time and attendance plus payroll'
                    ],
                    'owner' => [
                        'id' => 52974,
                        'account_id' => 1,
                        'role_id' => 1710,
                        'email' => 'salariumqualityautomation+crbac+owner@gmail.com',
                        'user_type' => 'owner',
                        'first_name' => 'Automation CRBAC',
                        'middle_name' => null,
                        'last_name' => 'Owner',
                        'full_name' => 'Automation CRBAC Owner',
                        'last_active_company_id' => 1,
                        'active' => 'active',
                        'role' => [
                            'id' => 1710,
                            'owner_account_id' => 1,
                            'name' => 'Salarium Account Owner',
                            'level' => 'ALL',
                            'is_employee' => false,
                            'company_ids' => [],
                            'policy' => null
                        ],
                        'product_seats' => [
                            [
                                'id' => 4,
                                'name' => 'time and attendance plus payroll'
                            ]
                        ]
                    ],
                    'companies' => [
                        [
                            'id' => 1,
                            'name' => 'Company2020-07-12 15:12:06.113EDITED',
                            'country' => [
                                'id' => 173,
                                'code' => 'PH',
                                'name' => 'Philippines'
                            ],
                            'account_id' => 1,
                            'logo' => '',
                            'email' => null,
                            'website' => null,
                            'mobile_number' => null,
                            'telephone_number' => null,
                            'telephone_extension' => null,
                            'fax_number' => null
                        ]
                    ]
                ],
                'product_seats' => [
                    [
                        'id' => 4,
                        'name' => 'time and attendance plus payroll'
                    ]
                ],
                'companies' => [
                    [
                        'id' => 1,
                        'name' => 'Company2020-07-12 15:12:06.113EDITED',
                        'country' => [
                            'id' => 173,
                            'code' => 'PH',
                            'name' => 'Philippines'
                        ],
                        'account_id' => 1,
                        'logo' => '',
                        'email' => null,
                        'website' => null,
                        'mobile_number' => null,
                        'telephone_number' => null,
                        'telephone_extension' => null,
                        'fax_number' => null,
                        'employee_id' => 1,
                        'employee_uid' => 'emp1234'
                    ]
                ],
                'license_unit_id' => 1
            ])))
        ;

        $connection = $this->mockConnection('mysql');
        $connection->shouldReceive('select')
            ->with(
                'select * from `user_roles` where `user_id` = ? and exists (select * from `roles` ' .
                'where `user_roles`.`role_id` = `roles`.`id` and (`company_id` = ? or (`company_id`' .
                ' = ? and `account_id` = ?)) and `roles`.`deleted_at` is null)',
                [1, 1, 1, 1],
                true
            )
            ->andReturn([
                [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'name' => 'Employee',
                    'type' => 'Employee',
                    'custom_role' => false,
                    'module_access' => [
                        'HRIS',
                        'T&A',
                        'Payroll'
                    ],
                    'deleted_at' => null
                ]
            ]);

        $connection->shouldReceive('select')
            ->with('select * from `user_roles` where `user_id` = ?', [1], true)
            ->andReturn([
                [
                    'id' => 1,
                    'account_id' => 1,
                    'company_id' => 1,
                    'name' => 'Employee',
                    'type' => 'Employee',
                    'custom_role' => false,
                    'module_access' => [
                        'HRIS',
                        'T&A',
                        'Payroll'
                    ],
                    'deleted_at' => null
                ]
            ]);

        $this->mockClass(Auth0UserService::class, [
            'getUser' => (object) [
                'status' => 'active'
            ]
        ]);

        $this->json('GET', self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->json('GET', self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
