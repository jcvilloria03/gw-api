<?php

namespace TestsNew\Api\Ess\LeaveRequest;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssRequestTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'ess.approvals';
    const METHOD = 'GET';
    const URL = '/ess/leave_request/1';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1
        ]);

        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );

        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1, 2],
            ]
        ]);
    }

    public function testShouldRespondSuccess()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $this
            ->shouldExpectRequest('GET', '/employee/1/workflows')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([])));

        $this
            ->shouldExpectRequest('GET', '/leave_request/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode([
                'id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'start_date' => '2019-06-24',
                'end_date' => '2019-06-24',
                'leave_type_id' => 1,
                'unit' => 'hours',
                'total_value' => 8,
                'leave_type' => [
                    'id' => 1,
                    'name' => 'Leave Without Pay',
                    'leave_credits' => [
                        [
                            'id' => 1,
                            'employee_id' => 1,
                            'leave_type_id' => 1,
                            'unit' => 'hours'
                        ]
                    ],
                    'selectable_leave_types' => [
                        [
                            'value' => '15 - hours',
                            'text' => 'Leave Without Pay - hours',
                            'unit' => 'hours',
                            'id' => 1
                        ]
                    ]
                ],
                'leave_credit' => [
                    'id' => 1,
                    'employee_id' => 1,
                    'leave_type_id' => 1,
                    'unit' => 'hours',
                    'value' => '480.00',
                ],
                'attachments' => []
            ])));


        Amqp::shouldReceive('publish');

        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }

    public function testShouldRespondUnauthorized()
    {
        $this->mockClientRequestService();

        $this->addUserAndAccountEssentialData();

        $this->json(self::METHOD, self::URL, [], [self::HEADER => self::MODULE]);

        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }
}
