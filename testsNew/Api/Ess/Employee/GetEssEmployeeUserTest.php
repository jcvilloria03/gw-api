<?php

namespace TestsNew\Api\Ess\Employee;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class GetEssEmployeeUserTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware([
            'user_id' => 1,
            'account_id' => 1,
            'employee_id' => 1,
            'employee_company_id' => 1,
        ]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        if (!empty($dataScope)) {
            $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
        }
    }

    public function testGetEssEmployeeUserShouldResponseSuccess()
    {
        $module = 'root.ess';
        $this->addUserAndAccountEssentialData([
            'module' => $module,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);
        $employee = ['id' => 1, 'first_name' => 'Test', 'last_name' => 'test'];
        $this->shouldExpectRequest('GET', '/employee/1/personal_info')
            ->once()
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($employee)))
        ;

        $this->json('GET', '/ess/employee/user', [], [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJsonEquals([
                'user_id' => 1,
                'account_id' => 1,
                'employee_id' => 1,
                'employee_company_id' => 1,
                'employee' => $employee,
            ])
        ;
    }

    public function testGetEssEmployeeUserShouldResponseUnauthorized()
    {
        $module = 'wrong.module';
        $this->addUserAndAccountEssentialData();

        $this->json('GET', '/ess/employee/user', [], [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}