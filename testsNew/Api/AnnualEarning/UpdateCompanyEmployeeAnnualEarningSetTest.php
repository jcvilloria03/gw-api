<?php

namespace TestsNew\Api\AnnualEarning;

use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;

class UpdateCompanyEmployeeAnnualEarningSetTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;
    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';
    const MODULE = 'employees.annual_earnings';
    const METHOD = 'POST';
    const URL = '/company/1/annual_earning/set';

    private function addUserAndAccountEssentialData()
    {
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);

        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('expire');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('hGet');
        Redis::shouldReceive('hSet');
        Amqp::shouldReceive('publish');
    }

    public function testShouldResponseSuccess()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [1],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'company_id' => 1,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $urlResponse = [1];
        $this
            ->shouldExpectRequest(self::METHOD, '/company/1/annual_earning/set')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $dataParam = [
            'employee_id' => 1
        ];

        $headers = [
            self::HEADER => self::MODULE,
            "Content-Type" => "application/json"
        ];

        $this->json(self::METHOD, self::URL, $dataParam, $headers);
        $this->seeStatusCode(HttpResponse::HTTP_OK);
    }


    public function testShouldResponseUnauthorized()
    {
        $this->mockClientRequestService();
        $this->addUserAndAccountEssentialData();
        $this->getRequestStorage(AuthzRequest::getStorageName())->push([
            'module' => self::MODULE,
            'data_scope' => [
                'COMPANY' => [9999],
                'PAYROLL_GROUP' => [1],
                'POSITION' => [1],
                'DEPARTMENT' => [1],
                'TEAM' => [1],
                'LOCATION' => [1],
            ],
        ]);

        $urlResponse = [
            'company_id' => 1,
        ];
        $this
            ->shouldExpectRequest('GET', '/employee/1')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $urlResponse = [1];
        $this
            ->shouldExpectRequest(self::METHOD, '/company/1/annual_earning/set')
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($urlResponse)));

        $dataParam = [
            'employee_id' => 1
        ];

        $headers = [
            self::HEADER => self::MODULE,
            "Content-Type" => "application/json"
        ];

        $this->json(self::METHOD, self::URL, $dataParam, $headers);
        $this->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED);
    }

}
