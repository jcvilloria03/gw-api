<?php

namespace TestsNew\Api\Holiday;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\User\UserRequestService;
use App\AnnualEarning\AnnualEarningRequestService;
use Mockery;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;

class GetAnnualEarningByIdTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';

    const MODULE = 'employees.people.annual_earnings';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(CompanyRequestService::class, [
            'getAccountId' => 1
        ]);

        $this->mockClass(EmployeeRequestService::class, [
            'getEmployee' => $this->getJsonResponse([
                'body' => [
                    'department_id' => 1,
                    'position_id' => 1,
                    'location_id' => 1,
                    'time_attendance' => [
                        'team_id' => 1
                    ],
                    'payroll' => [
                        'payroll_group_id' => 1
                    ]
                ]
            ])
        ]);

        $this->mockClass(AnnualEarningRequestService::class, [
            'get' => $this->getJsonResponse([
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'employee_id' => 1
                ]
            ])
        ]);
    }

    public function testGetEmployeeAnnualEarningSuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.annual_earnings' => [
                    'data_scope' => [
                        'COMPANY' => [1],
                        'DEPARTMENT' => [1],
                        'POSITION' => [1],
                        'LOCATION' => [1],
                        'TEAM' => [1],
                        'PAYROLL_GROUP' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/annual_earning/1',
            [],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }


    /**
     * @dataProvider  dataForErrors
     */
    public function testGetEmployeeAnnualEarningUnauthorized($mockDataScope)
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'employees.people.annual_earnings' => [
                    'data_scope' => $mockDataScope
                ]
            ]
        ]);

        $this->json(
            'GET',
            '/company/1/annual_earning/1',
            [],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function dataForErrors()
    {
        // Test Company Authorization
        yield [
            "data_scope" => [
                'COMPANY' => [999],
                'DEPARTMENT' => [1],
                'POSITION' => [1],
                'LOCATION' => [1],
                'TEAM' => [1],
                'PAYROLL_GROUP' => [1]
            ]
        ];
    }
}
