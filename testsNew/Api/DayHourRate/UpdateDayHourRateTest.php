<?php

namespace TestsNew\Api\DayHourRate;

use App\Authz\AuthzRequestService;
use App\Company\CompanyRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\DayHourRate\DayHourRateAuditService;
use App\DayHourRate\DayHourRateRequestService;

class UpdateDayHourRateTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const TARGET_URL = 'day_hour_rate/1';
    const TARGET_METHOD = 'PATCH';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        Redis::shouldReceive('hSet')
            ->andReturn(null);
        Redis::shouldReceive('hGet')
            ->andReturn(null);
        Redis::shouldReceive('get')
            ->andReturn(null);
        Redis::shouldReceive('set')
            ->andReturn(null);
        Redis::shouldReceive('hMSet')
            ->andReturn(null);
        Redis::shouldReceive('expire')
        ->andReturn(null);
        Redis::shouldReceive('hExists')
        ->andReturn(null);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(AuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(CompanyRequestService::class, [
            [ // getAccount
                'body' => [
                    'company_id' => 1,
                    'account_id' => 1
                ]
            ]
        ]);

        $this->mockRequestService(UserRequestService::class, [
            [ // getEssentialData
                'body' => [
                    'data' => [
                        'subject' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ],
                        'userData' => [
                            'user_id' => 1,
                            'account_id' => 1
                        ]
                    ]
                ]
            ],
        ]);

        $this->mockRequestService(AuthzRequestService::class, [
            [
                'body' => [
                    'data' => [
                        'company_settings.day_hour_rates' => [
                            'data_scope' => [
                                'COMPANY' => [1]
                            ]
                        ]
                    ]

                ]
            ]
        ]);

        $this->mockClass(DayHourRateAuditService::class,[
            'queue' => true
        ]);

        $this->mockRequestService(DayHourRateRequestService::class, [
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'rate' => 1,
                    'day_type' => 'Regular',
                    'holiday_type' => 'Non-holiday',
                    'time_type' => 'Regular',
                    'abbreviation' => 'regular'
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'rate' => 1,
                    'day_type' => 'Regular',
                    'holiday_type' => 'Non-holiday',
                    'time_type' => 'Regular',
                    'abbreviation' => 'regular'
                ]
            ],
            [
                'body' => [
                    'id' => 1,
                    'company_id' => 1,
                    'rate' => 1,
                    'day_type' => 'Regular',
                    'holiday_type' => 'Non-holiday',
                    'time_type' => 'Regular',
                    'abbreviation' => 'regular'
                ]
            ],
        ]);
    }

    public function testUpdateDayHourRateShouldResponseSuccess()
    {
        $payload = [
            "company_id" => 1,
            "name" => "test 1",
            "rate" => 1
        ];

        $this->patch(
            self::TARGET_URL,
            $payload,
            ['x-authz-entities' => 'company_settings.day_hour_rates']
        );
        $this->assertResponseStatus(Response::HTTP_OK);

    }

    public function testUpdateDayHourRateShouldResponseUnauthorized()
    {
        $payload = [
            "company_id" => 999,
            "name" => "test 1",
            "rate" => 1
        ];
        $this->patch(
            self::TARGET_URL,
            $payload,
            ['x-authz-entities' => 'company_settings.day_hour_rates']
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
