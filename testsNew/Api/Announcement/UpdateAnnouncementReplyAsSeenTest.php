<?php

namespace TestsNew\Api\Account;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\User\UserRequestService;
use TestsNew\Helpers\TestCase;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\RequestServiceTrait;
use Symfony\Component\HttpFoundation\Response;
use App\ESS\EssAnnouncementRequestService;
use App\Audit\AuditService;

class UpdateAnnouncementReplyAsSeenTest extends TestCase
{
    use AuthorizationServiceTrait;

    use RequestServiceTrait;

    const HEADER = 'X-Authz-Entities';

    const MODULE = 'main_page.announcements';

    public function setUp()
    {
        parent::setUp();

        $this->mockUserData([
            'user_id' => 1,
            'account_id' => 1
        ]);

        // Mock essential data
        $this->mockClass(UserRequestService::class, [
            'getEssentialData' => [
                'subject' => [],
                'userData' => []
            ]
        ]);

        $this->mockClass(AccountRequestService::class, [
            'getAccountEssentialData' => $this->getJsonResponse([
                'body' => [
                    1 => [
                        'department' => [],
                        'position' => [],
                        'team' => [],
                        'location' => [],
                        'payroll_group' => [],
                    ]
                ]
            ])
        ]);

        $this->mockClass(EssAnnouncementRequestService::class, [
            'markReplyAsSeen' => $this->getJsonResponse([
                'body' => [
                    "id" => 1,
                    "company_id" => "1",
                    "announcement_id" => 1,
                    "sender_id" => 1,
                    "message" => "test",
                    "seen" => true,
                ]
            ])
        ]);

        $this->mockClass(AuditService::class, [
            'queue' => $this->getJsonResponse(true)
        ]);
    }

    public function testUpdateAnnouncementReplySuccess()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => [
                'root.admin' => [
                    'data_scope' => [
                        'ACCOUNT' => [1]
                    ]
                ]
            ]
        ]);

        $this->json(
            'PUT',
            '/announcement/reply/1/seen',
            [],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUpdateAnnouncementReplyUnauthorized()
    {
        $this->mockClass(AuthzRequestService::class, [
            'checkSalariumClearance' => []
        ]);

        $this->json(
            'PUT',
            '/announcement/reply/1/seen',
            [],
            [self::HEADER => self::MODULE]
        );

        $this->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }
}
