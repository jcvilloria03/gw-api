<?php

namespace TestsNew\Api\Announcement;

use App\Http\Middleware\AuthzMiddleware;
use Bschmitt\Amqp\Facades\Amqp;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Redis;
use TestsNew\Api\TestCase;
use TestsNew\Helpers\Request\AuthzRequest;
use TestsNew\Helpers\Traits\AuthorizationServiceTrait;
use TestsNew\Helpers\Traits\EssentialTrait;
use TestsNew\Helpers\Traits\RequestTrait;

class ReplyAnnouncementTest extends TestCase
{
    use AuthorizationServiceTrait;
    use EssentialTrait;
    use RequestTrait;

    private function addUserAndAccountEssentialData(array $dataScope = [])
    {
        $this->mockClientRequestService();
        $this->mockUserMiddleware(['user_id' => 1, 'account_id' => 1]);
        $this->addUserEssentialTrait(
            [
                'user_id' => 1,
                'account_id' =>  1,
                'role_id' =>  1,
                'company_id' =>  1,
                'employee_id' =>  1,
                'department_id' =>  1,
                'cost_center_id' =>  1,
                'position_id' =>  1,
                'location_id' =>  1,
                'payroll_group_id' =>  1,
                'team_id' =>  null,
            ],
            [
                'user_id' => 1,
                'account_id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'employee_id' => 1,
                'department_id' => 1,
                'cost_center_id' => 1,
                'position_id' => 1,
                'location_id' => 1,
                'payroll_group_id' => 1,
                'team_id' => null,
            ]
        );
        $this->addAccountEssentialTrait(1, [
            1 => [
                'department' => [1],
                'position' => [1],
                'team' => [1],
                'location' => [1],
                'payroll_group' => [1],
            ]
        ]);
        Redis::shouldReceive('get')->andReturnNull();
        Redis::shouldReceive('set');
        Redis::shouldReceive('hMSet');
        Redis::shouldReceive('expire');
        Amqp::shouldReceive('publish');

        $this->getRequestStorage(AuthzRequest::getStorageName())->push($dataScope);
    }

    public function testReplyAnnouncementShouldResponseSuccess()
    {
        $module = 'main_page.announcements';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);
        $this->shouldExpectRequest('GET', '/announcement/1')
            ->once()
            ->withQuery(['user_id' => 1])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['company_id' => 1])))
        ;
        $this->shouldExpectRequest('GET', '/announcement/1/user_role/1')
            ->once()
            ->andReturnResponse(new Response(
                HttpResponse::HTTP_OK,
                [],
                json_encode(['sender' => false, 'recipient' => true])
            ))
        ;
        $expected = ['message' => 'Test', 'sender_id' => 1];
        $this->shouldExpectRequest('POST', '/announcement/1/reply')
            ->once()
            ->withBody(json_encode(['message' => 'Test', 'sender_id' => 1]))
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode($expected)))
        ;

        $inputs = ['message' => 'Test'];
        $this->json('POST', '/announcement/1/reply', $inputs, [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_OK)
            ->seeJsonEquals($expected)
        ;
    }

    public function testReplyAnnouncementShouldResponseUnauthorized()
    {
        $module = 'main_page.announcements';
        $this->addUserAndAccountEssentialData(['module' => $module, 'data_scope' => ['COMPANY' => [1]]]);
        $this->shouldExpectRequest('GET', '/announcement/1')
            ->once()
            ->withQuery(['user_id' => 1])
            ->andReturnResponse(new Response(HttpResponse::HTTP_OK, [], json_encode(['company_id' => 2])))
        ;

        $inputs = ['message' => 'Test'];
        $this->json('POST', '/announcement/1/reply', $inputs, [AuthzMiddleware::AUTHZ_ENTITIES_HEADER => $module])
            ->seeStatusCode(HttpResponse::HTTP_UNAUTHORIZED)
            ->seeJson(['message' => 'Unauthorized', 'status_code' => HttpResponse::HTTP_UNAUTHORIZED])
        ;
    }
}