#!/bin/bash

#start NGINX
sudo /etc/init.d/nginx start

#start PHP-FPMd
sudo /etc/init.d/php7.0-fpm start

#Do no execute newman in staging (for now)
if [ "$DEPLOYMENT_GROUP_NAME" == "api-gateway-dev" ]
then
    #start Consumers via SUPERVISOR (replaces the individual calls below)
    sudo /usr/local/bin/supervisorctl start all
    
    # go to newman directory
    cd /srv/salarium-newman
    # run newman tests using tsp (run in background, not during deploy)
    tsp ./newman.sh
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-stg" ]
then
    #Reload new Supervisor configurations as necessary
    sudo supervisorctl update
    #start Consumers via SUPERVISOR (replaces the individual calls below)
    sudo /etc/init.d/supervisor restart
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-ft" ]
then
    #Reload new Supervisor configurations as necessary
    sudo supervisorctl update
    #start Consumers via SUPERVISOR (replaces the individual calls below)
    sudo /etc/init.d/supervisor restart
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-demo" ]
then
    #Reload new Supervisor configurations as necessary
    sudo supervisorctl update
    #start Consumers via SUPERVISOR (replaces the individual calls below)
    sudo /etc/init.d/supervisor restart
fi
#  sudo /usr/local/bin/supervisorctl start employee_upload_response:*
#  sudo /usr/local/bin/supervisorctl start employee_update_upload_response:*
#  sudo /usr/local/bin/supervisorctl start employee_time_attendance_upload_response:*
#  sudo /usr/local/bin/supervisorctl start payroll_upload_response:*
#  sudo /usr/local/bin/supervisorctl start payroll_calculation_response:*
#  sudo /usr/local/bin/supervisorctl start payslip_generation_response:*
#  sudo /usr/local/bin/supervisorctl start audit:*

# go to newman directory
# cd /srv/salarium-newman

# run newman tests using tsp (run in background, not during deploy)
# tsp ./newman.sh
