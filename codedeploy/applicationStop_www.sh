#!/bin/bash

# Stop NGINX
sudo /etc/init.d/nginx stop

# Stops PHP-FPM
sudo /etc/init.d/php7.0-fpm stop

#start Consumer via SUPERVISOR (replaces the individual calls below)
if [ "$DEPLOYMENT_GROUP_NAME" == "api-gateway-dev" ]
then
    sudo /usr/local/bin/supervisorctl stop all
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-stg" ]
then
    sudo /usr/bin/supervisorctl stop all
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-ft" ]
then
    sudo /usr/bin/supervisorctl stop all
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-demo" ]
then
    sudo /usr/bin/supervisorctl stop all
fi
# sudo /usr/local/bin/supervisorctl stop employee_upload_response:*
# sudo /usr/local/bin/supervisorctl stop employee_update_upload_response:*
# sudo /usr/local/bin/supervisorctl stop employee_time_attendance_upload_response:*
# sudo /usr/local/bin/supervisorctl stop payroll_upload_response:*
# sudo /usr/local/bin/supervisorctl stop payroll_calculation_response:*
# sudo /usr/local/bin/supervisorctl stop payslip_generation_response:*
# sudo /usr/local/bin/supervisorctl stop audit:*
