#!/bin/bash

# go to app directory
#cd /var/www/api-gateway
cd /srv/gw-api

# copy .env file from S3 trigger!
if [ "$DEPLOYMENT_GROUP_NAME" == "api-gateway-dev" ]
then
    aws s3 cp s3://codedeploy-us-west-2-v3env-dev/dev-api-gateway.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-stg" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-stg-gw-api.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-ft" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-ft-gw-api.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "gw-api-demo" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-demo-gw-api.env .env
fi

# run composer
COMPOSER_HOME=/var/cache/composer composer install

# run migrations
php artisan migrate

# generate Swagger UI
php artisan swagger-lume:generate

cd /srv/gw-api
find . -type d -exec chmod 755 {} \;  
find . -type f -exec chmod 644 {} \;
