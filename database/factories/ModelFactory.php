<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Model\Role::class, function (Faker\Generator $faker) {
    return [
        'account_id' => $faker->randomNumber(),
        'company_id' => $faker->randomNumber(),
        'name' => $faker->name,
    ];
});

$factory->define(App\Model\UserRole::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomNumber(),
    ];
});
