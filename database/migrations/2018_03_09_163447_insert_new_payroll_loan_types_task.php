<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertNewPayrollLoanTypesTask extends Migration
{
    use \Database\Migrations\TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payroll_loan_types',
            'display_name' => 'View Payroll Loan Types',
            'description' => 'View Payroll Loan Type',
            'module' => 'Payroll Loan',
            'submodule' => 'Payroll Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.payroll_loan_types',
            'display_name' => 'Edit Payroll Loan Types',
            'description' => 'Edit Payroll Loan Types',
            'module' => 'Payroll Loan',
            'submodule' => 'Payroll Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.payroll_loan_types',
            'display_name' => 'Delete Payroll Loan Types',
            'description' => 'Delete Payroll Loan Types',
            'module' => 'Payroll Loan',
            'submodule' => 'Payroll Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.payroll_loan_types',
            'display_name' => 'Create Payroll Loan Types',
            'description' => 'Create Payroll Loan Types',
            'module' => 'Payroll Loan',
            'submodule' => 'Payroll Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
