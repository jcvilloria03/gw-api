<?php

use App\Model\Role;
use App\Model\Task;
use App\Model\UserRole;
use App\Model\Permission;
use App\Permission\TargetType;
use App\Permission\PermissionService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Authorization\AuthorizationService;
use App\Account\AccountAuthorizationService;
use Illuminate\Database\Migrations\Migration;

class UpdatePermissionsAddViewAccountToSuperadmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $taskName = AccountAuthorizationService::VIEW_TASK;

        $task = Task::where('name', $taskName)->first();

        if (!empty($task)) {
            // Get all super admin roles
            $roles = Role::where('name', Role::SUPERADMIN_NAME)
                ->get();

            $userIds = [];
            $roles->each(function ($role) use ($task, &$userIds) {
                Permission::create([
                    'task_id' => $task->id,
                    'role_id' => $role->id,
                    'scope' => [TargetType::ACCOUNT => [$role->account_id]],
                ]);

                $userRoles = UserRole::where('role_id', $role->id)
                    ->select('user_id')
                    ->get();

                if ($userRoles->isNotEmpty()) {
                    $userIds = array_merge($userIds, $userRoles->pluck('user_id')->all());
                }
            });
        }

        if (!empty($userIds)) {
            $service = App::make(AuthorizationService::class);
            $service->invalidateUsersCachedPermissions($userIds);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $taskName = AccountAuthorizationService::VIEW_TASK;

        $task = Task::where('name', $taskName)->first();

        if (!empty($task)) {
            // Get all super admin roles
            $roles = Role::where('name', Role::SUPERADMIN_NAME)
                ->get();

            // Get list of permission ids for task view.account
            $permissionIds = [];
            $userIds = [];
            $roles->each(function ($role) use ($task, &$permissionIds, &$userIds) {
                $permission = $role->permissions->where('task_id', $task->id)->first();
                if (!empty($permission)) {
                    $permissionIds[] = $permission->id;

                    $userRoles = UserRole::where('role_id', $role->id)
                        ->select('user_id')
                        ->get();

                    if ($userRoles->isNotEmpty()) {
                        $userIds = array_merge($userIds, $userRoles->pluck('user_id')->all());
                    }
                }
            });

            //Remove permissions
            if (!empty($permissionIds)) {
                Permission::destroy($permissionIds);
            }

            if (!empty($userIds)) {
                $service = App::make(AuthorizationService::class);
                $service->invalidateUsersCachedPermissions($userIds);
            }
        }
    }
}
