<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdjustmentsTypeTasks extends Migration
{
    use \Database\Migrations\TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.adjustment_types',
            'display_name' => 'View Adjustment Types',
            'description' => 'View Adjustment Types',
            'module' => 'Payroll',
            'submodule' => 'Adjustment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.adjustment_types',
            'display_name' => 'Edit Adjustment Types',
            'description' => 'Edit Adjustment Types',
            'module' => 'Payroll',
            'submodule' => 'Adjustment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.adjustment_types',
            'display_name' => 'Create Adjustment Types',
            'description' => 'Create Adjustment Types',
            'module' => 'Payroll',
            'submodule' => 'Adjustment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.adjustment_types',
            'display_name' => 'Delete Adjustment Types',
            'description' => 'Delete Adjustment Types',
            'module' => 'Payroll',
            'submodule' => 'Adjustment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
