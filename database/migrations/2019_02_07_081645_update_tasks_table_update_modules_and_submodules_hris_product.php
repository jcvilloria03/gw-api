<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableUpdateModulesAndSubmodulesHrisProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update module column
        DB::statement(
            "UPDATE tasks 
             SET module = CASE
                 WHEN name IN (
                    'view.company','edit.company','delete.company','create.company',
                    'view.user','edit.user','delete.user','create.user',
                    'view.location','edit.location','delete.location','create.location',
                    'view.role','edit.role','delete.role','create.role'
                    ) THEN 'Company'
                 WHEN name IN (
                    'view.department','edit.department','delete.department','create.department',
                    'view.position','edit.position','delete.position','create.position',
                    'view.rank','edit.rank','delete.rank','create.rank'
                    'view.cost_center','create.cost_center'
                    'view.employment_type','edit.employment_type','delete.employment_type','create.employment_type'
                    ) THEN 'Organisation Settings'
                 WHEN name IN (
                    'view.employee','edit.employee','delete.employee','create.employee','ess.view.profile',
                    'ess.view.payslip'
                    ) THEN 'Employee'   
                 WHEN name IN (
                 'create.day_hour_rate','view.day_hour_rate','edit.day_hour_rate'
                    ) THEN 'Time and Attendance Settings'  
                 WHEN name IN (
                 'view.announcement','create.announcement','edit.announcement','delete.announcement'
                    ) THEN 'Admin'   
                 ELSE module   
             END;"
        );

        // update submodule column
        DB::statement(
            "UPDATE tasks
             SET submodule = CASE 
                 WHEN name IN (
                    'view.department','edit.department','delete.department','create.department'
                    ) THEN 'Departments'
                 WHEN name IN (
                    'view.position','edit.position','delete.position','create.position'
                    ) THEN 'Positions'
                 WHEN name IN (
                    'view.rank','edit.rank','delete.rank','create.rank'
                    ) THEN 'Ranks'
                 WHEN name IN (
                    'view.cost_center','create.cost_center'
                    ) THEN 'Cost Center'
                 WHEN name IN (
                    'view.employment_type','edit.employment_type','delete.employment_type','create.employment_type'
                    ) THEN 'Employment Type'
                 WHEN name IN (
                    'view.employee','edit.employee','delete.employee','create.employee','ess.view.profile',
                    'ess.view.payslip'
                    ) THEN 'Employee'
                 WHEN name IN (
                 'create.day_hour_rate','view.day_hour_rate','edit.day_hour_rate'
                    ) THEN 'Day/Hour Rates' 
                 WHEN name IN (
                 'view.announcement','create.announcement','edit.announcement','delete.announcement'
                    ) THEN 'Announcements' 
                 ELSE submodule
             END;"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
