<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertEssAnnouncementsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.view.announcement',
            'display_name' => 'View Announcement',
            'description' => 'View Announcement',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ],
        [
            'name' => 'ess.create.Announcement',
            'display_name' => 'Create Announcement',
            'description' => 'Create Announcement',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ],
        [
            'name' => 'ess.delete.Announcement',
            'display_name' => 'Delete Announcement',
            'description' => 'Delete Announcement',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ],
        [
            'name' => 'ess.edit.Announcement',
            'display_name' => 'Edit Announcement',
            'description' => 'Edit Announcement',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ]
    ];
}
