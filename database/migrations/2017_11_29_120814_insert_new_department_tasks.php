<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewDepartmentTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.department',
            'display_name' => 'View Departments',
            'description' => 'View Departments',
            'module' => 'HRIS',
            'submodule' => 'Department',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.department',
            'display_name' => 'Edit Departments',
            'description' => 'Edit Departments',
            'module' => 'HRIS',
            'submodule' => 'Department',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.department',
            'display_name' => 'Delete Departments',
            'description' => 'Delete Departments',
            'module' => 'HRIS',
            'submodule' => 'Department',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.department',
            'display_name' => 'Create Departments',
            'description' => 'Create Departments',
            'module' => 'HRIS',
            'submodule' => 'Department',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
