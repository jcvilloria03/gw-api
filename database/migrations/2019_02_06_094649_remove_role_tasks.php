<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRoleTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $taskRoleIds = DB::table('tasks')
            ->where('name', 'LIKE', '%role%')
            ->select([ 'id' ])
            ->get()
            ->pluck('id');

        if ($taskRoleIds->isNotEmpty()) {
            DB::table('permissions')
                ->whereIn('task_id', $taskRoleIds)
                ->delete();
            DB::table('tasks')
                ->whereIn('id', $taskRoleIds)
                ->delete();
        }
    }
}
