<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAuth0UsersDropEmployeeIdCompanyIdColumnsAddCompaniesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('employee_id')->unsigned()->nullable();
            $table->timestamps();
        });

        \App\Model\Auth0User::chunk(2000, function ($users) {
            foreach ($users as $user) {
                if ($user->company_id) {
                    \App\Model\CompanyUser::create([
                        'user_id' => $user->user_id,
                        'company_id' => $user->company_id,
                        'employee_id' => $user->employee_id
                    ]);
                }
            }
        });

        Schema::table('auth0_users', function (Blueprint $table) {
            $table->dropColumn('employee_id');
            $table->dropColumn('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->unsignedInteger('employee_id')->nullable()->after('auth0_user_id');
            $table->unsignedInteger('company_id')->nullable()->after('employee_id');

            $table->index('employee_id');
            $table->index('company_id');
        });

        \App\Model\CompanyUser::chunk(2000, function ($companyUsers) {
            foreach ($companyUsers as $companyUser) {
                $user = \App\Model\Auth0User::find($companyUser->user_id);

                $user->employee_id = $companyUser->employee_id;
                $user->company_id = $companyUser->company_id;

                $user->save();
            }
        });


        Schema::dropIfExists('companies_users');
    }
}
