<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAllowancesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.allowances',
            'display_name' => 'View Allowances',
            'description' => 'View Allowances',
            'module' => 'Payroll',
            'submodule' => 'Allowance',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.allowances',
            'display_name' => 'Edit Allowances',
            'description' => 'Edit Allowances',
            'module' => 'Payroll',
            'submodule' => 'Allowance',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.allowances',
            'display_name' => 'Create Allowances',
            'description' => 'Create Allowances',
            'module' => 'Payroll',
            'submodule' => 'Allowance',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.allowances',
            'display_name' => 'Delete Allowances',
            'description' => 'Delete Allowances',
            'module' => 'Payroll',
            'submodule' => 'Allowance',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
