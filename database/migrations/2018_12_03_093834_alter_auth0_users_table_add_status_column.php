<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuth0UsersTableAddStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->enum('status', \App\Model\Auth0User::STATUSES)
                ->after('auth0_user_id')
                ->default(\App\Model\Auth0User::STATUS_INACTIVE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
