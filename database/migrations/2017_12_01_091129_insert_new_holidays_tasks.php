<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewHolidaysTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.holiday',
            'display_name' => 'Create Holidays',
            'description' => 'Create Holidays',
            'module' => 'Time And Attendance',
            'submodule' => 'Holidays',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.holiday',
            'display_name' => 'Delete Holidays',
            'description' => 'Delete Holidays',
            'module' => 'Time And Attendance',
            'submodule' => 'Holidays',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.holiday',
            'display_name' => 'Edit Holidays',
            'description' => 'Edit Holidays',
            'module' => 'Time And Attendance',
            'submodule' => 'Holidays',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.holiday',
            'display_name' => 'View Holidays',
            'description' => 'View Holidays',
            'module' => 'Time And Attendance',
            'submodule' => 'Holidays',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
