<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertCommissionsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.commissions',
            'display_name' => 'View Commissions',
            'description' => 'View Commissions',
            'module' => 'Payroll',
            'submodule' => 'Commission',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.commissions',
            'display_name' => 'Edit Commissions',
            'description' => 'Edit Commissions',
            'module' => 'Payroll',
            'submodule' => 'Commission',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.commissions',
            'display_name' => 'Create Commissions',
            'description' => 'Create Commissions',
            'module' => 'Payroll',
            'submodule' => 'Commission',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.commissions',
            'display_name' => 'Delete Commissions',
            'description' => 'Delete Commissions',
            'module' => 'Payroll',
            'submodule' => 'Commission',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
