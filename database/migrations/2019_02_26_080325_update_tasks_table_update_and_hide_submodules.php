<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableUpdateAndHideSubmodules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "UPDATE tasks
            SET module = CASE
                WHEN name IN (
                    'create.leave_type', 'delete.leave_type', 'edit.leave_type', 'view.leave_type',
                    'create.tardiness_rule', 'delete.tardiness_rule', 'edit.tardiness_rule', 'view.tardiness_rule',
                    'edit.night_shift', 'view.night_shift', 'create.holiday', 'delete.holiday', 'edit.holiday',
                    'view.holiday', 'create.schedule', 'view.schedule', 'edit.schedule', 'delete.schedule',
                    'create.shift', 'view.shift', 'edit.shift', 'delete.shift'
                ) THEN 'T&A'
                WHEN name IN (
                    'view.basic_pay', 'edit.basic_pay', 'delete.basic_pay'
                ) THEN 'Payroll'
                ELSE module
            END;"
        );

        DB::statement(
            "UPDATE tasks
                SET hidden = CASE
                    WHEN name IN (
                        'edit.request', 'approval.request'
                    ) THEN 1
                    ELSE hidden
            END;"
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
