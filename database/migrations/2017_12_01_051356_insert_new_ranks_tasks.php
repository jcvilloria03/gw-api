<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewRanksTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.rank',
            'display_name' => 'Create Ranks',
            'description' => 'Create Ranks',
            'module' => 'HRIS',
            'submodule' => 'Rank',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.rank',
            'display_name' => 'Delete Ranks',
            'description' => 'Delete Ranks',
            'module' => 'HRIS',
            'submodule' => 'Rank',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.rank',
            'display_name' => 'Edit Ranks',
            'description' => 'Edit Ranks',
            'module' => 'HRIS',
            'submodule' => 'Rank',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.rank',
            'display_name' => 'View Ranks',
            'description' => 'View Ranks',
            'module' => 'HRIS',
            'submodule' => 'Rank',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
