<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewCostCenterTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.cost_center',
            'display_name' => 'View Cost Centers',
            'description' => 'View Cost Centers',
            'module' => 'HRIS',
            'submodule' => 'Cost Center',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.cost_center',
            'display_name' => 'Create Cost Centers',
            'description' => 'Create Cost Centers',
            'module' => 'HRIS',
            'submodule' => 'Cost Center',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
