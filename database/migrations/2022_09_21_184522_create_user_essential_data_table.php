<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEssentialDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_essential_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('account_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->unsignedInteger('role_id')->nullable(true)->index();
            $table->unsignedInteger('employee_id')->nullable(true)->index();
            $table->unsignedInteger('department_id')->nullable(true);
            $table->unsignedInteger('cost_center_id')->nullable(true);
            $table->unsignedInteger('position_id')->nullable(true);
            $table->unsignedInteger('location_id')->nullable(true);
            $table->unsignedInteger('payroll_group_id')->nullable(true);
            $table->unsignedInteger('team_id')->nullable(true);
            $table->longText('data')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_essential_data');
    }
}
