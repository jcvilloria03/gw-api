<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertUpdateCostCenterTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'edit.cost_center',
            'display_name' => 'Edit Cost Centers',
            'description' => 'Edit Cost Centers',
            'module' => 'HRIS',
            'submodule' => 'Cost Center',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
