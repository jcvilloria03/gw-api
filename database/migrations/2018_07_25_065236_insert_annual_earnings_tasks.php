<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAnnualEarningsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.annual_earnings',
            'display_name' => 'View Annual Earnings',
            'description' => 'View Annual Earnings',
            'module' => 'Payroll',
            'submodule' => 'Annual Earnings',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.annual_earnings',
            'display_name' => 'Edit Annual Earnings',
            'description' => 'Edit Annual Earnings',
            'module' => 'Payroll',
            'submodule' => 'Annual Earnings',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.annual_earnings',
            'display_name' => 'Create Annual Earnings',
            'description' => 'Create Annual Earnings',
            'module' => 'Payroll',
            'submodule' => 'Annual Earnings',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ]
    ];
}
