<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollUserDefinedLoansTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'edit.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'delete.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'request.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'approve.payroll_user_defined_loans',
            'display_name' => 'View Payroll User Loans',
            'description' => 'View Payrolls User Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
    ];
}
