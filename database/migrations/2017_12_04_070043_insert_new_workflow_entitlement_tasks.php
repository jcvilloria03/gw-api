<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewWorkflowEntitlementTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.workflow_entitlement',
            'display_name' => 'View Workflow Entitlements',
            'description' => 'View Workflow Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.workflow_entitlement',
            'display_name' => 'Create Workflow Entitlements',
            'description' => 'Create Workflow Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.workflow_entitlement',
            'display_name' => 'Delete Workflow Entitlements',
            'description' => 'Delete Workflow Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.workflow_entitlement',
            'display_name' => 'Edit Workflow Entitlements',
            'description' => 'Edit Workflow Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
