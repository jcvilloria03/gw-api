<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertEssPayrollGroupTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.view.payroll_group',
            'display_name' => 'Payroll group',
            'description' => 'Employee payroll group',
            'module' => 'ESS',
            'submodule' => 'payroll',
            'allowed_scopes' => [
                'Payroll Group'
            ],
            'ess' => true
        ]
    ];
}
