<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Role;
use App\Role\RoleAuthorizationService;
use App\Model\Permission;
use App\Model\Task;

class AddRolesToDefaultCompanyAdmins extends Migration
{
    const PERMS_TEMP_TABLE = 'temp_inserted_permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Declaration of static task types for roles
        $tasks = [
            RoleAuthorizationService::VIEW_TASK,
            RoleAuthorizationService::UPDATE_TASK,
            RoleAuthorizationService::DELETE_TASK,
            RoleAuthorizationService::CREATE_TASK
        ];

        // Create temporary table for storing of inserted role tasks.
        // This will be used for rollback.
        if (!Schema::hasTable(self::PERMS_TEMP_TABLE)) {
            Schema::create(self::PERMS_TEMP_TABLE, function (Blueprint $table) {
                $table->bigInteger('insert_id');
            });
        }

        // Search all company default roles
        $adminRoles = Role::where([
            ['type','=', Role::ADMIN],
            ['custom_role', '=', 0],
            ['company_id', '>', 0]
        ])->get();

        // Change composition of tasks where task name is the key and value
        // is the task id only.
        $roleTasks = Task::whereIn('name', $tasks)->get()->mapWithKeys(function ($item) {
            return [$item['name'] => $item['id']];
        });
        
        // Loop through admin custom roles and check
        foreach ($adminRoles as $adminRole) {
            $hasTasks = [];

            // Get permissions for each custom role
            $adminPermissions = $adminRole->permissions()
                                          ->get();

            // Check if roles are included in permissions.
            // if existing, push those in $hasTasks.
            // This will be used for checking if a certain permission
            // has role tasks already.
            foreach ($adminPermissions as $permission) {
                if (in_array($permission->task->name, $tasks)) {
                    $hasTasks[] = $permission->task->name;
                }
            }
            
            // Differentiate tasks vs $hasTasks. The purpose
            // of which is to only retrieve permissions that don't have
            // role tasks.
            $diff = array_diff($tasks, $hasTasks);

            // Insert role tasks for permissions that doesn't have any.
            foreach ($diff as $diffTaskName) {
                $scope = ['Company' => [$adminRole->company_id]];
                $id = Permission::create([
                    'task_id' => $roleTasks[$diffTaskName],
                    'role_id' => $adminRole->id,
                    'scope' => $scope,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ]);
                
                DB::table(self::PERMS_TEMP_TABLE)->insert([
                    "insert_id" => $id->id
                ]);
            }
        }
    }

    /**
     * Reverse the migrations by deleting the temporary table and
     * records in Permissions table whose ids are stored in the.
     * temporary table.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable(self::PERMS_TEMP_TABLE)) {
            $permissionIds = DB::table(self::PERMS_TEMP_TABLE)->get();

            foreach ($permissionIds as $id) {
                Permission::where("id", $id->insert_id)->delete();
            }

            Schema::drop(self::PERMS_TEMP_TABLE);
        }
    }
}
