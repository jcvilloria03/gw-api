<?php

use App\Model\Role;
use App\Model\Task;
use App\Model\UserRole;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Authorization\AuthorizationService;
use Illuminate\Database\Migrations\Migration;

class UpdateAdminsSalpayIntegrationPermissions extends Migration
{
    const SALPAY_INTEGRATION_TASKS = [
        'create.salpay_integration',
        'view.salpay_integration',
        'edit.salpay_integration',
        'delete.salpay_integration'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $adminRoles = $this->getAdmins();
        $tasks      = $this->getTasks();

        // get role ids
        $roleIdsList = $adminRoles->pluck('id')->all();

        // get task ids
        $tasksIdsList = $tasks->pluck('id')->all();

        // get user ids
        $userIdsList = $adminRoles->pluck('userroles.*.user_id')
            ->flatten() // flatten array since result of pluck is per role
            ->unique() // add unique filter lessen loop size
            ->all();

        // Delete permissions
        DB::table('permissions')
            ->whereIn('role_id', $roleIdsList)
            ->whereIn('task_id', $tasksIdsList)
            ->delete();

        // Invalidate cached permissions
        $rbacService = App::make(AuthorizationService::class);

        $rbacService->invalidateUsersCachedPermissions($userIdsList);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    /**
     * Get list of admin roles that have salpay_integration permissions
     *
     * @return void
     */
    private function getAdmins()
    {
        return Role::with('userroles')
            ->where([
                ['type','=', Role::ADMIN],
                ['custom_role', '=', 0],
                ['company_id', '>', 0]
            ])
            ->whereHas('permissions', function ($query) {
                $query->whereHas('task', function ($subquery) {
                    $subquery->whereIn('name', static::SALPAY_INTEGRATION_TASKS);
                });
            })
            ->get();
    }

    /**
     * Get task records for salpay integration tasks
     *
     * @return void
     */
    private function getTasks()
    {
        return Task::whereIn('name', static::SALPAY_INTEGRATION_TASKS)
            ->get();
    }
}
