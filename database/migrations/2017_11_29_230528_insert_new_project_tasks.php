<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewProjectTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.project',
            'display_name' => 'Create Projects',
            'description' => 'Create Projects',
            'module' => 'HRIS',
            'submodule' => 'Project',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.project',
            'display_name' => 'View Projects',
            'description' => 'View Projects',
            'module' => 'HRIS',
            'submodule' => 'Project',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.project',
            'display_name' => 'Edit Projects',
            'description' => 'Edit Projects',
            'module' => 'HRIS',
            'submodule' => 'Project',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.project',
            'display_name' => 'Delete Projects',
            'description' => 'Delete Projects',
            'module' => 'HRIS',
            'submodule' => 'Project',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
