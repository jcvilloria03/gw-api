<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeIdAuth0Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->unsignedInteger('employee_id')->nullable()->after('auth0_user_id');
            $table->index('employee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->dropColumn('employee_id');
        });
    }
}
