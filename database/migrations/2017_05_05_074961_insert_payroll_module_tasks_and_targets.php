<?php

use Illuminate\Database\Migrations\Migration;

class InsertPayrollModuleTasksAndTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'create.payroll',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);

        $this->createTask([
            'name' => 'edit.payroll',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);

        $this->createTask([
            'name' => 'view.payroll',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);

        $this->createTask([
            'name' => 'view.company_payrolls',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);

        $this->createTask([
            'name' => 'view.payroll_group_payrolls',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);

        $this->createTask([
            'name' => 'close.payroll',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);

        $this->createTask([
            'name' => 'delete.payroll',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where('module', 'Payroll')->get();
        foreach ($tasks as $task) {
            DB::table('targets')->where('task_id', $task->id)->delete();
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
            DB::table('targets')->delete();
        }
    }

    private function createTask($attributes, $targets = [])
    {
        $module = "Payroll";
        $product = "Payroll";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
        ]);
        $this->createTargets($taskId, $targets);
        $this->addPermissionToExistingRoles($taskId);
        return $taskId;
    }

    private function createTargets($taskId, $targets)
    {
        foreach ($targets as $target) {
            DB::table('targets')->insert([
                'task_id' => $taskId,
                'name' => $target['name'],
            ]);
        }
    }

    private function addPermissionToExistingRoles($taskId)
    {
        $existingPermissions = DB::table('permissions')->select('role_id', 'scope')->distinct()->get();
        foreach ($existingPermissions as $permission) {
            DB::table('permissions')->insert([
                'task_id' => $taskId,
                'role_id' => $permission->role_id,
                'scope' => $permission->scope,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
