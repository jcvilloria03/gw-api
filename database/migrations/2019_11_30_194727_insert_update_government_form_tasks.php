<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertUpdateGovernmentFormTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'edit.government_form',
            'display_name' => 'Update Government Forms',
            'description' => 'Update Government Forms',
            'module' => 'Payroll',
            'submodule' => 'Government Form',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
