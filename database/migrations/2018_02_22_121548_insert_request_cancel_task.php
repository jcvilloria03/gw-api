<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertRequestCancelTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.cancel.request',
            'display_name' => 'Request Cancel',
            'description' => 'Request Cancel',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [
                'Company'
            ],
            'ess' => true
        ]
    ];
}
