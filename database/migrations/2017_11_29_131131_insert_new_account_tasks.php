<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewAccountTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.account',
            'display_name' => 'View Account',
            'description' => 'View Account',
            'module' => 'HRIS',
            'submodule' => 'Account',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'update.account',
            'display_name' => 'Update Account',
            'description' => 'Update Account',
            'module' => 'HRIS',
            'submodule' => 'Account',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.account_logs',
            'display_name' => 'View Account Logs',
            'description' => 'View Account Logs',
            'module' => 'HRIS',
            'submodule' => 'Account',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],

    ];
}
