<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRoleExistingCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get all existing Company Admin roles, and create permissions for them
        $companyAdminRoles = DB::table('roles')->where([
            ['name', 'like', '% Admin'],
            ['name', '!=', 'Super Admin'],
        ])->get();

        foreach ($companyAdminRoles as $role) {
            $employeeRoleId = DB::table('roles')->insertGetId([
                'account_id' => $role->account_id,
                'company_id' => $role->company_id,
                'name' => preg_replace('/Admin$/', 'Employee', $role->name),
                'type' => 'Employee',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);

            // attach existing ESS permissions here
            $tasks = DB::table('tasks')->where([
                ['ess', '=', true]
            ])->get();
            foreach ($tasks as $task) {
                DB::table('permissions')->insert([
                    'task_id' => $task->id,
                    'role_id' => $employeeRoleId,
                    'scope' => '{}',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // no need for down() for this migration
    }
}
