<?php

use App\Model\Task;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSalpayIntegrationTasksAllowedScopes extends Migration
{
    const TASK_NAMES = [
        'create.salpay_integration',
        'view.salpay_integration',
        'edit.salpay_integration',
        'delete.salpay_integration'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Fix allowed_scopes json encoded twice
        foreach (self::TASK_NAMES as $taskName) {
            $task = Task::where('name', $taskName)->first();

            $task->allowed_scopes = ['Account'];

            $task->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::TASK_NAMES as $taskName) {
            $task = Task::where('name', $taskName)->first();

            $task->allowed_scopes = json_encode(['Account']);

            $task->save();
        }
    }
}
