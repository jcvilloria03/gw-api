<?php
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertOpenPayrollTask extends Migration
{
    use TasksMigrationsTrait {
        //though the migration works without this, this is to make sure that the initial
        //trait will be called first before this implementation
        up as protected migrationUp;
        down as protected migrationDown;
    }

    protected $tasks = [
        [
            'name' => 'open.payroll',
            'display_name' => 'Open Payrolls',
            'description' => 'Open Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->migrationUp();

        DB::statement(
            "UPDATE tasks
            SET sort_id = 39
            WHERE `name` = 'open.payroll'"
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->migrationDown();
    }
}
