<?php

use App\Model\Role;
use App\Model\Task;
use App\Model\UserRole;
use App\Model\Permission;
use App\Permission\TargetType;
use App\Permission\PermissionService;
use App\Authorization\AuthorizationService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class UpdatePermissionsAddSubscriptionsToSuperadmins extends Migration
{
    use TasksMigrationsTrait;

    const TASK_NAMES = [
        'create.subscriptions',
        'view.subscriptions',
        'edit.subscriptions',
        'delete.subscriptions'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userIds = [];
        foreach (self::TASK_NAMES as $taskName) {
            $affectedIds = $this->addTask($taskName);
            $userIds = array_unique(array_merge($userIds, $affectedIds));
        }

        if (!empty($userIds)) {
            $service = App::make(AuthorizationService::class);
            $service->invalidateUsersCachedPermissions($userIds);
        }
    }

    private function addTask(string $taskName)
    {
        //update task allowed scopes to include company
        $task = Task::where('name', $taskName)->first();
        $task->allowed_scopes = ['Account', 'Company'];
        $task->save();

        Role::where([
            ['name', Role::SUPERADMIN_NAME],
            ['custom_role', '=', false],
        ])
        ->each(function ($superAdminRole) use ($task) {
            $this->assignTaskPermissionToRole($task->id, $superAdminRole->id, ['Company' => 'all']);
        });

        //get ids affected
        $userIds = [];

        Role::where('name', Role::SUPERADMIN_NAME)
            ->each(function ($role) use ($task, &$userIds) {
                $userRoles = UserRole::where('role_id', $role->id)
                    ->select('user_id')
                    ->get();

                if ($userRoles->isNotEmpty()) {
                    $userIds = array_merge($userIds, $userRoles->pluck('user_id')->all());
                }
            });

        return $userIds;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $allowedScopes = ['Account'];

        foreach (self::TASK_NAMES as $taskName) {
            $this->destroyPermissions($taskName, $allowedScopes);
        }
    }
}
