<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionlessRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')
            ->where('id', 0)
            ->delete();
        $roleId = DB::table('roles')->insertGetId([
            'account_id' => 0,
            'company_id' => 0,
            'name' => 'Permissionless',
        ]);
        DB::table('roles')
            ->where('id', $roleId)
            ->update(['id' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('targets')->where('id', 0)->delete();
    }
}
