<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableUpdateModulesAndSubmodulesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "UPDATE tasks
            SET module_access = CASE
                WHEN name IN (
                    'create.leave_entitlement', 'delete.leave_entitlement', 'edit.leave_entitlement',
                    'view.leave_entitlement', 'edit.default_schedule', 'create.default_schedule',
                    'view.default_schedule', 'create.rest_day', 'view.rest_day', 'edit.rest_day', 'delete.rest_day',
                    'view.leave_credit', 'edit.leave_credit', 'create.leave_credit', 'delete.leave_credit',
                    'view.leave_request', 'edit.leave_request', 'create.leave_request', 'delete.leave_request',
                    'edit.time_records', 'view.time_records', 'edit.hours_worked', 'view.hours_worked'
                ) THEN 'T&A'
                WHEN name IN (
                    'create.basic_pay', 'request.payroll_loans', 'approve.payroll_loans',
                    'view.adjustments', 'edit.adjustments', 'create.adjustments', 'delete.adjustments',
                    'view.adjustment_types', 'edit.adjustment_types', 'create.adjustment_types',
                    'delete.adjustment_types', 'view.earnings', 'edit.earnings', 'create.earnings',
                    'view.special_pay_run', 'edit.special_pay_run', 'create.special_pay_run', 'delete.special_pay_run',
                    'close.special_pay_run', 'send.special_pay_run', 'run.special_pay_run'
                ) THEN 'Payroll'
                WHEN name IN (
                    'view.team', 'edit.team', 'delete.team', 'create.team', 'create.project', 'view.project',
                    'edit.project', 'delete.project', 'view.workflow', 'create.workflow', 'delete.workflow',
                    'edit.workflow'
                ) THEN 'HRIS'
                WHEN name IN (
                    'view.system_defined_payroll_loans', 'edit.system_defined_payroll_loans',
                    'create.system_defined_payroll_loans', 'delete.system_defined_payroll_loans',
                    'request.system_defined_payroll_loans', 'approve.system_defined_payroll_loans'
                ) THEN 'Payroll'
                ELSE module_access
            END;"
        );

        DB::statement(
            "UPDATE tasks
            SET module = CASE
                WHEN name IN (
                    'create.leave_entitlement', 'delete.leave_entitlement', 'edit.leave_entitlement',
                    'view.leave_entitlement', 'edit.default_schedule', 'create.default_schedule',
                    'view.default_schedule', 'create.rest_day', 'view.rest_day', 'edit.rest_day', 'delete.rest_day',
                    'view.leave_credit', 'edit.leave_credit', 'create.leave_credit', 'delete.leave_credit',
                    'view.leave_request', 'edit.leave_request', 'create.leave_request', 'delete.leave_request',
                    'edit.time_records', 'view.time_records', 'edit.hours_worked', 'view.hours_worked'
                ) THEN 'T&A'
                WHEN name IN (
                    'create.basic_pay', 'request.payroll_loans', 'approve.payroll_loans',
                    'create.payroll_group', 'view.payroll_group', 'delete.payroll_group', 'edit.payroll_group',
                    'view.payroll_loan_types', 'edit.payroll_loan_types', 'delete.payroll_loan_types',
                    'create.payroll_loan_types', 'view.bonus_types', 'edit.bonus_types', 'create.bonus_types',
                    'delete.bonus_types', 'view.commission_types', 'edit.commission_types', 'create.commission_types',
                    'delete.commission_types', 'view.allowance_types', 'edit.allowance_types', 'create.allowance_types',
                    'delete.allowance_types', 'view.deduction_types', 'edit.deduction_types', 'create.deduction_types',
                    'delete.deduction_types', 'view.annual_earnings', 'edit.annual_earnings', 'create.annual_earnings',
                    'view.terminations', 'edit.terminations', 'create.terminations', 'delete.terminations',
                    'view.final_pays', 'edit.final_pays', 'create.final_pays'
                ) THEN 'Payroll'
                WHEN name in (
                    'view.account', 'edit.account', 'create.company', 'view.company', 'edit.company', 'delete.company',
                    'view.user', 'create.user', 'delete.user', 'edit.user', 'view.role', 'create.role', 'delete.role',
                    'edit.role', 'view.account_logs', 'view.company_logs', 'create.location', 'view.location',
                    'edit.location', 'delete.location', 'view.department', 'edit.department', 'delete.department',
                    'create.department', 'create.position', 'view.position', 'edit.position', 'delete.position',
                    'create.rank', 'delete.rank', 'edit.rank', 'view.rank', 'view.team', 'edit.team', 'delete.team',
                    'create.team', 'view.employment_type', 'create.employment_type', 'delete.employment_type',
                    'edit.employment_type', 'create.project', 'view.project', 'edit.project', 'delete.project',
                    'view.workflow', 'create.workflow', 'delete.workflow', 'edit.workflow', 'view.workflow_entitlement',
                    'create.workflow_entitlement', 'delete.workflow_entitlement', 'edit.workflow_entitlement',
                    'edit.announcement', 'view.announcement', 'create.announcement', 'delete.announcement',
                    'view.request', 'approval.request', 'edit.request', 'view.employee', 'create.employee',
                    'edit.employee', 'edit.day_hour_rate', 'view.day_hour_rate'
                ) THEN 'HRIS'
                ELSE module
            END;"
        );


        DB::statement(
            "UPDATE tasks
            SET submodule = CASE
                WHEN name IN (
                    'create.leave_entitlement', 'delete.leave_entitlement', 'edit.leave_entitlement',
                    'view.leave_entitlement'
                ) THEN 'Leaves'
                WHEN name IN (
                    'edit.default_schedule'
                ) THEN 'Default Schedules'
                WHEN name IN (
                    'create.rest_day', 'view.rest_day', 'edit.rest_day', 'delete.rest_day'
                ) THEN 'Rest Days'
                WHEN name in ('view.account_logs', 'view.company_logs') THEN 'Audit Trail'
                WHEN name in ('view.admin_dashboard') THEN 'Admin Dashboard'
                WHEN name in ('view.employee', 'create.employee', 'edit.employee') THEN 'Employees'
                ELSE submodule
            END;"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
