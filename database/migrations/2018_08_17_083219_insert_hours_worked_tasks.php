<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertHoursWorkedTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'edit.hours_worked',
            'display_name' => 'Edit Hours Worked',
            'description' => 'Edit Hours Worked',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.hours_worked',
            'display_name' => 'View Hours Worked',
            'description' => 'View Hours',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ]
    ];
}
