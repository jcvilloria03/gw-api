<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertRequestApprovalTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.approval.request',
            'display_name' => 'Request Approval',
            'description' => 'Request Approval',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ]
    ];
}
