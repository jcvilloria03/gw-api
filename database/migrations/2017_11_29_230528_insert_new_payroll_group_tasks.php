<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollGroupTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.payroll_group',
            'display_name' => 'Create Payroll Groups',
            'description' => 'Create Payroll Groups',
            'module' => 'HRIS',
            'submodule' => 'Payroll Group',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.payroll_group',
            'display_name' => 'View Payroll Groups',
            'description' => 'View Payroll Groups',
            'module' => 'HRIS',
            'submodule' => 'Payroll Group',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.payroll_group',
            'display_name' => 'Delete Payroll Groups',
            'description' => 'Delete Payroll Groups',
            'module' => 'HRIS',
            'submodule' => 'Payroll Group',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
    ];
}
