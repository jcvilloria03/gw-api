<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewEssTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.view.payslip',
            'display_name' => 'View Payslips',
            'description' => 'View Payslips',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true,
        ],
        [
            'name' => 'ess.view.profile',
            'display_name' => 'View Profile',
            'description' => 'View Profile',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true,
        ],

    ];
}
