<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRolesAndPermissionsUpdateWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "UPDATE tasks
            SET submodule = 'Workflow Entitlements'
            WHERE `name` IN (
                'view.workflow_entitlement',
                'create.workflow_entitlement',
                'delete.workflow_entitlement',
                'edit.workflow_entitlement'
            )"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
