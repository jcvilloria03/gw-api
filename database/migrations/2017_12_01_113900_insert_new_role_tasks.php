<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewRoleTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.role',
            'display_name' => 'View Roles',
            'description' => 'View Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.role',
            'display_name' => 'Edit Roles',
            'description' => 'Edit Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.role',
            'display_name' => 'Delete Roles',
            'description' => 'Delete Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.role',
            'display_name' => 'Create Roles',
            'description' => 'Create Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],

    ];
}
