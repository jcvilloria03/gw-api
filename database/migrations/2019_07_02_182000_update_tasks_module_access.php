<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksModuleAccess extends Migration
{
    const HRIS_TASKS = [
        'create.announcement',
        'ess.create.announcement',
        'create.company',
        'create.department',
        'create.employee',
        'create.employment_type',
        'create.holiday',
        'create.location',
        'create.position',
        'create.rank',
        'create.day_hour_rate',
        'create.role',
        'create.user',
        'create.workflow_entitlement',
        'create.workflow',
        'delete.company',
        'delete.department',
        'delete.employment_type',
        'delete.holiday',
        'delete.location',
        'delete.position',
        'delete.rank',
        'delete.role',
        'delete.user',
        'delete.workflow_entitlement',
        'delete.workflow',
        'edit.company',
        'edit.department',
        'edit.employee',
        'edit.employment_type',
        'edit.holiday',
        'edit.location',
        'edit.position',
        'edit.rank',
        'edit.day_hour_rate',
        'edit.role',
        'edit.workflow_entitlement',
        'edit.workflow',
        'edit.account',
        'view.account',
        'view.announcement',
        'view.company',
        'view.company_logs',
        'view.department',
        'view.employee',
        'view.employment_type',
        'view.holiday',
        'view.location',
        'view.account_logs',
        'ess.view.notification',
        'view.position',
        'ess.view.profile',
        'view.rank',
        'view.day_hour_rate',
        'view.role',
        'view.user',
        'view.workflow_entitlement'
    ];

    const PAYROLL_TASKS = [
        'close.payroll',
        'create.adjustments',
        'create.allowances',
        'create.allowance_types',
        'create.annual_earnings',
        'create.basic_pay',
        'create.bonuses',
        'create.bonus_types',
        'create.commissions',
        'create.commission_types',
        'create.cost_center',
        'create.deductions',
        'create.deduction_types',
        'create.disbursement',
        'create.final_pays',
        'create.government_form',
        'create.payroll_loans',
        'create.payroll_loan_types',
        'create.payroll_group',
        'create.payroll',
        'create.special_pay_run',
        'create.terminations',
        'delete.adjustments',
        'delete.allowances',
        'delete.allowance_types',
        'delete.basic_pay',
        'delete.bonuses',
        'delete.bonus_types',
        'delete.commissions',
        'delete.commission_types',
        'delete.cost_center',
        'delete.deductions',
        'delete.deduction_types',
        'delete.disbursement',
        'delete.payroll_loans',
        'delete.payroll_loan_types',
        'delete.payroll_group',
        'delete.payroll',
        'delete.special_pay_run',
        'delete.terminations',
        'edit.adjustments',
        'edit.allowances',
        'edit.allowance_types',
        'edit.annual_earnings',
        'edit.basic_pay',
        'edit.bonuses',
        'edit.bonus_types',
        'edit.commissions',
        'edit.commission_types',
        'edit.cost_center',
        'edit.deductions',
        'edit.deduction_types',
        'edit.disbursement',
        'edit.final_pays',
        'edit.payroll_loans',
        'edit.payroll_loan_types',
        'edit.payroll',
        'edit.special_pay_run',
        'edit.terminations',
        'generate.payslip',
        'run.payroll',
        'send_payslip.payroll',
        'view.adjustments',
        'view.allowances',
        'view.allowance_types',
        'view.annual_earnings',
        'view.basic_pay',
        'view.bonuses',
        'view.bonus_types',
        'view.commissions',
        'view.commission_types',
        'view.cost_center',
        'view.deductions',
        'view.deduction_types',
        'view.disbursement',
        'view.final_pays',
        'view.government_form',
        'view.payroll_loans',
        'view.payroll_loan_types',
        'view.payslip',
        'ess.view.payroll_group',
        'view.payroll_group',
        'view.payroll',
        'ess.view.payslip',
        'view.special_pay_run',
        'view.terminations',
    ];

    const TA_TASKS = [
        'ess.clock.log',
        'create.default_schedule',
        'create.leave_credit',
        'create.leave_entitlement',
        'create.leave_request',
        'create.leave_type',
        'create.project',
        'ess.create.request',
        'create.rest_day',
        'create.schedule',
        'create.shift',
        'create.tardiness_rule',
        'create.team',
        'delete.leave_credit',
        'delete.leave_entitlement',
        'delete.leave_request',
        'delete.leave_request',
        'delete.project',
        'ess.cancel.request',
        'delete.rest_day',
        'delete.schedule',
        'delete.shift',
        'delete.tardiness_rule',
        'delete.team',
        'edit.default_schedule',
        'edit.hours_worked',
        'edit.leave_credit',
        'edit.leave_entitlement',
        'edit.leave_request',
        'edit.leave_type',
        'edit.night_shift',
        'edit.project',
        'edit.rest_day',
        'edit.schedule',
        'edit.shift',
        'edit.tardiness_rule',
        'edit.team',
        'edit.time_records',
        'ess.approval.request',
        'view.request',
        'ess.view.default_schedule',
        'view.default_schedule',
        'ess.view.hours_worked',
        'view.hours_worked',
        'view.leave_credit',
        'view.leave_entitlement',
        'view.leave_request',
        'view.leave_type',
        'view.night_shift',
        'view.project',
        'ess.view.request',
        'view.rest_day',
        'view.schedule',
        'view.shift',
        'view.tardiness_rule',
        'view.team',
        'ess.clock.view_time_records',
        'view.time_records',
    ];

    const TABLE = 'tasks';

    const HRIS_CODE = 'HRIS';
    const TA_CODE   = 'T&A';
    const PR_CODE   = 'Payroll';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // UPDATE tasks
        // SET module_access = <MODULE_CODE>
        // WHERE
        //    name IN <MODULE_TASK_NAMES>
        //    AND module_access != <MODULE_CODE>;

        // SET MODULE_ACCESS FOR HRIS TASKS
        DB::table(self::TABLE)
            ->whereIn('name', self::HRIS_TASKS)
            ->where('module_access', '!=', self::HRIS_CODE)
            ->update(['module_access' => self::HRIS_CODE]);

        // SET MODULE_ACCESS FOR TA TASKS
        DB::table(self::TABLE)
            ->whereIn('name', self::TA_TASKS)
            ->where('module_access', '!=', self::TA_CODE)
            ->update(['module_access' => self::TA_CODE]);

        // SET MODULE_ACCESS FOR PAYROLL TASKS
        DB::table(self::TABLE)
            ->whereIn('name', self::PAYROLL_TASKS)
            ->where('module_access', '!=', self::PR_CODE)
            ->update(['module_access' => self::PR_CODE]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
