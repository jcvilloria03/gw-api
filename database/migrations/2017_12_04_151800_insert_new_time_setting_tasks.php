<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewTimeSettingTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.default_schedule',
            'display_name' => 'Create Default Schedule',
            'description' => 'Create Default Schedule',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.default_schedule',
            'display_name' => 'Edit Default Schedule',
            'description' => 'Edit Default Schedule',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.default_schedule',
            'display_name' => 'View Default Schedule',
            'description' => 'View Default Schedule',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.day_hour_rate',
            'display_name' => 'Create Rates',
            'description' => 'Create Rates',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.day_hour_rate',
            'display_name' => 'Edit Rates',
            'description' => 'Edit Rates',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.day_hour_rate',
            'display_name' => 'View Rates',
            'description' => 'View Rates',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.tardiness_rule',
            'display_name' => 'Create Tardiness Rules',
            'description' => 'Create Tardiness Rules',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.tardiness_rule',
            'display_name' => 'Delete Tardiness Rules',
            'description' => 'Delete Tardiness Rules',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.tardiness_rule',
            'display_name' => 'Edit Tardiness Rules',
            'description' => 'Edit Tardiness Rules',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.tardiness_rule',
            'display_name' => 'View Tardiness Rules',
            'description' => 'View Tardiness Rules',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.night_shift',
            'display_name' => 'Edit Night Shifts',
            'description' => 'Edit Night Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.night_shift',
            'display_name' => 'View Night Shifts',
            'description' => 'View Night Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Time Setting',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
