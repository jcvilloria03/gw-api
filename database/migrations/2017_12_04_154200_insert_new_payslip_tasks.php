<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayslipTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payslip',
            'display_name' => 'View Payslips',
            'description' => 'View Payslips',
            'module' => 'Payroll',
            'submodule' => 'Payslip',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'generate.payslip',
            'display_name' => 'Generate Payslips',
            'description' => 'Generate Payslips',
            'module' => 'Payroll',
            'submodule' => 'Payslip',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
    ];
}
