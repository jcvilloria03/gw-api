<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewLeaveTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.leave_type',
            'display_name' => 'Create Leave Types',
            'description' => 'Create Leave Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.leave_type',
            'display_name' => 'Delete Leave Types',
            'description' => 'Delete Leave Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.leave_type',
            'display_name' => 'Edit Leave Types',
            'description' => 'Edit Leave Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.leave_type',
            'display_name' => 'View Leave Types',
            'description' => 'View Leave Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.leave_entitlement',
            'display_name' => 'Create Leave Entitlements',
            'description' => 'Create Leave Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.leave_entitlement',
            'display_name' => 'Delete Leave Entitlements',
            'description' => 'Delete Leave Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.leave_entitlement',
            'display_name' => 'Edit Leave Entitlements',
            'description' => 'Edit Leave Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.leave_entitlement',
            'display_name' => 'View Leave Entitlements',
            'description' => 'View Leave Entitlements',
            'module' => 'Time And Attendance',
            'submodule' => 'Leave',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
