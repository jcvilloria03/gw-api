<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewGovernmentFormsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.government_form',
            'display_name' => 'Create Government Forms',
            'description' => 'Create Government Forms',
            'module' => 'Payroll',
            'submodule' => 'Government Form',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.government_form',
            'display_name' => 'View Government Forms',
            'description' => 'View Government Forms',
            'module' => 'Payroll',
            'submodule' => 'Government Form',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
