<?php

use App\Model\Task;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Database\Migrations\TasksMigrationsTrait;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksAddNewEntries extends Migration
{
    use TasksMigrationsTrait;

    const TABLE = 'tasks';

    const MODULE_ACCESS_HRIS = 'HRIS';
    const MODULE_ACCESS_TA   = 'T&A';
    const MODULE_ACCESS_PR   = 'Payroll';

    protected $tasks = [
        [
            'name'           => 'export.announcement',
            'display_name'   => 'Export Announcements',
            'description'    => 'Export Announcements',
            'module_access'  => self::MODULE_ACCESS_HRIS,
            'module'         => self::MODULE_ACCESS_HRIS,
            'submodule'      => 'Announcements',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 20,
            'ess'            => 0
        ], // export announcements

        [
            'name'           => 'create.payment_method',
            'display_name'   => 'Create Payment Method',
            'description'    => 'Create Payment Method',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'Employee Payment Methods',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // create payment method

        [
            'name'           => 'view.payment_method',
            'display_name'   => 'View Payment Method',
            'description'    => 'View Payment Method',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'Employee Payment Methods',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // view payment method

        [
            'name'           => 'edit.payment_method',
            'display_name'   => 'Edit Payment Method',
            'description'    => 'Edit Payment Method',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'Employee Payment Methods',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // edit payment method

        [
            'name'           => 'delete.payment_method',
            'display_name'   => 'Delete Payment Method',
            'description'    => 'Delete Payment Method',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'Employee Payment Methods',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // delete payment method

        [
            'name'           => 'create.salpay_integration',
            'display_name'   => 'Create SALPay Integration',
            'description'    => 'Create SALPay Integration',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'SALPay Integration',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // create salpay integration

        [
            'name'           => 'view.salpay_integration',
            'display_name'   => 'View SALPay Integration',
            'description'    => 'View SALPay Integration',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'SALPay Integration',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // view salpay integration

        [
            'name'           => 'edit.salpay_integration',
            'display_name'   => 'Edit SALPay Integration',
            'description'    => 'Edit SALPay Integration',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'SALPay Integration',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // edit salpay integration

        [
            'name'           => 'delete.salpay_integration',
            'display_name'   => 'Delete SALPay Integration',
            'description'    => 'Delete SALPay Integration',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'SALPay Integration',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // delete salpay integration

        [
            'name'           => 'delete.final_pays',
            'display_name'   => 'Delete Final Pays',
            'description'    => 'Delete Final Pays',
            'module_access'  => self::MODULE_ACCESS_PR,
            'module'         => self::MODULE_ACCESS_PR,
            'submodule'      => 'Final  Pay',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 39,
            'ess'            => 0
        ], // delete final pays

        [
            'name'           => 'create.subscriptions',
            'display_name'   => 'Create Subscriptions',
            'description'    => 'Create Subscriptions',
            'module_access'  => self::MODULE_ACCESS_HRIS,
            'module'         => self::MODULE_ACCESS_HRIS,
            'submodule'      => 'Subscriptions',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 40,
            'ess'            => 0
        ], // create subscriptions

        [
            'name'           => 'view.subscriptions',
            'display_name'   => 'View Subscriptions',
            'description'    => 'View Subscriptions',
            'module_access'  => self::MODULE_ACCESS_HRIS,
            'module'         => self::MODULE_ACCESS_HRIS,
            'submodule'      => 'Subscriptions',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 40,
            'ess'            => 0
        ], // view subscriptions

        [
            'name'           => 'edit.subscriptions',
            'display_name'   => 'Edit Subscriptions',
            'description'    => 'Edit Subscriptions',
            'module_access'  => self::MODULE_ACCESS_HRIS,
            'module'         => self::MODULE_ACCESS_HRIS,
            'submodule'      => 'Subscriptions',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 40,
            'ess'            => 0
        ], // edit subscriptions

        [
            'name'           => 'delete.subscriptions',
            'display_name'   => 'Delete Subscriptions',
            'description'    => 'Delete Subscriptions',
            'module_access'  => self::MODULE_ACCESS_HRIS,
            'module'         => self::MODULE_ACCESS_HRIS,
            'submodule'      => 'Subscriptions',
            'allowed_scopes' => [
                'Account'
            ],
            'sort_id'        => 40,
            'ess'            => 0
        ], // delete subscriptions

        [
            'name'           => 'create.attendance_computation',
            'display_name'   => 'Generate Attendance Computation',
            'description'    => 'Generate Attendance Computation',
            'module_access'  => self::MODULE_ACCESS_TA,
            'module'         => self::MODULE_ACCESS_TA,
            'submodule'      => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 38,
            'ess'            => 0
        ], // create attendance computation

        [
            'name'           => 'export.attendance_computation',
            'display_name'   => 'Export Attendance Computation',
            'description'    => 'Export Attendance Computation',
            'module_access'  => self::MODULE_ACCESS_TA,
            'module'         => self::MODULE_ACCESS_TA,
            'submodule'      => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 38,
            'ess'            => 0
        ], // export attendance computation

        [
            'name'           => 'view.attendance_computation',
            'display_name'   => 'View Attendance Computation',
            'description'    => 'View Attendance Computation',
            'module_access'  => self::MODULE_ACCESS_TA,
            'module'         => self::MODULE_ACCESS_TA,
            'submodule'      => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 38,
            'ess'            => 0
        ], // view computed attendance

        [
            'name'           => 'edit.attendance_computation',
            'display_name'   => 'Edit Attendance Computation',
            'description'    => 'Edit Attendance Computation',
            'module_access'  => self::MODULE_ACCESS_TA,
            'module'         => self::MODULE_ACCESS_TA,
            'submodule'      => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'sort_id'        => 38,
            'ess'            => 0
        ], // edit computed attendance
    ];

    /**
     * Override trait method to add module_access and sort
     *
     * @param array $attributes
     * @return int
     */
    public function createTask(array $attributes)
    {
        $taskId = DB::table('tasks')->insertGetId([
            'name'           => $attributes['name'],
            'display_name'   => $attributes['display_name'],
            'description'    => $attributes['description'],
            'module_access'  => $attributes['module_access'],
            'module'         => $attributes['module'],
            'submodule'      => $attributes['submodule'],
            'allowed_scopes' => json_encode($attributes['allowed_scopes']),
            'sort_id'        => $attributes['sort_id'],
            'ess'            => $attributes['ess']
        ]);

        return $taskId;
    }
}
