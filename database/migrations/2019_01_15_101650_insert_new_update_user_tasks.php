<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewUpdateUserTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'update.user',
            'display_name' => 'Update Users',
            'description' => 'Update Users',
            'module' => 'HRIS',
            'submodule' => 'User',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ]
    ];
}
