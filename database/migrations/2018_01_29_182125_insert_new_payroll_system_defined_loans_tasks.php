<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollSystemDefinedLoansTasks extends Migration
{

    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'edit.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'delete.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan Types',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'request.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'approve.payroll_system_defined_loans',
            'display_name' => 'View Payroll System Loans',
            'description' => 'View Payrolls System Loans',
            'module' => 'Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
    ];
}
