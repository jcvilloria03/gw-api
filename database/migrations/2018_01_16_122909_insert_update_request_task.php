<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertUpdateRequestTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.update.request',
            'display_name' => 'Update Request',
            'description' => 'Update Request',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ]
    ];
}
