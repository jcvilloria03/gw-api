<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthnUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('authn_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->unique()->index();
            $table->unsignedInteger('account_id')->index();
            $table->unsignedBigInteger('authn_user_id')->unique()->index();
            $table->unsignedInteger('company_id')->nullable()->index();
            $table->unsignedInteger('employee_id')->nullable()->index();
            $table->string('status')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('authn_users');
    }
}
