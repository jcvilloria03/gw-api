<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class RemoveUserTasks extends Migration
{
    use TasksMigrationsTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $taskUserIds = DB::table('tasks')
            ->where('name', 'LIKE', '%user%')
            ->select([ 'id' ])
            ->get()
            ->pluck('id');

        if ($taskUserIds->isNotEmpty()) {
            DB::table('permissions')
                ->whereIn('task_id', $taskUserIds)
                ->delete();
            DB::table('tasks')
                ->whereIn('id', $taskUserIds)
                ->delete();
        }
    }
}
