<?php

use Database\Migrations\TasksMigrationsTrait;
use Illuminate\Database\Migrations\Migration;

class InsertSpecialPayRunTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.special_pay_run',
            'display_name' => 'View Special Pay Run',
            'description' => 'View Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.special_pay_run',
            'display_name' => 'Edit Special Pay Run',
            'description' => 'Edit Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.special_pay_run',
            'display_name' => 'Create Special Pay Run',
            'description' => 'Create Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.special_pay_run',
            'display_name' => 'Delete Special Pay Run',
            'description' => 'Delete Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'close.special_pay_run',
            'display_name' => 'Close Special Pay Run',
            'description' => 'Close Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'send.special_pay_run',
            'display_name' => 'Send Special Pay Run',
            'description' => 'Send Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'run.special_pay_run',
            'display_name' => 'Run Special Pay Run',
            'description' => 'Run Special Pay Run',
            'module' => 'Payroll',
            'submodule' => 'SpecialPayRun',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
