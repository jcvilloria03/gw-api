<?php

use App\Model\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Authorization\AuthorizationService;
use Database\Migrations\TasksMigrationsTrait;
use Illuminate\Database\Migrations\Migration;

class UpdateOwnerSuperadminPermissionsNewInvoicesTasks extends Migration
{
    use TasksMigrationsTrait {
        //though the migration works without this, this is to make sure that the initial
        //trait will be called first before this implementation
        up as protected migrationUp;
        down as protected migrationDown;
    }

    protected $tasks = [
        [
            'name'           => 'view.subscription_invoices',
            'display_name'   => 'View Subscription Invoices',
            'description'    => 'View Subscription Invoices',
            'module'         => 'HRIS',
            'submodule'      => 'Subscriptions',
            'ess'            => false,
            'sort_id'        => 40,
            'allowed_scopes' => [
                'Account'
            ]
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->migrationUp();

        $userIds = $this->getOwnersAndSuperAdminUserIds();

        // Invalidate cached permissions
        $rbacService = App::make(AuthorizationService::class);

        $rbacService->invalidateUsersCachedPermissions($userIds);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->migrationDown();

        $userIds = $this->getOwnersAndSuperAdminUserIds();

        // Invalidate cached permissions
        $rbacService = App::make(AuthorizationService::class);

        $rbacService->invalidateUsersCachedPermissions($userIds);
    }

    public function createTask(array $attributes)
    {
        $taskId = DB::table('tasks')->insertGetId([
            'name'           => $attributes['name'],
            'display_name'   => $attributes['display_name'],
            'description'    => $attributes['description'],
            'module'         => $attributes['module'],
            'submodule'      => $attributes['submodule'],
            'allowed_scopes' => json_encode($attributes['allowed_scopes']),
            'ess'            => $attributes['ess'],
            'sort_id'        => $attributes['sort_id']
        ]);
        return $taskId;
    }

    /**
     * OVERRIDE
     * GET OWNER AND SUPERADMIN ROLES
     *
     * @return Eloquent\Collection
     */
    protected function getOwnerRoles()
    {
        return DB::table('roles')
            ->select('id', 'name', 'account_id', 'company_id')
            ->where([
                ['custom_role', '=', false],
            ])
            ->whereIn('name', [Role::OWNER_NAME, Role::SUPERADMIN_NAME])
            ->get();
    }

    /**
     * Get list of admin roles that have salpay_integration permissions
     *
     * @return void
     */
    private function getOwnersAndSuperAdminUserIds()
    {
        $roles = Role::with('userroles')
            ->where([
                ['custom_role', '=', 0]
            ])
            ->whereIn('name', [Role::OWNER_NAME, Role::SUPERADMIN_NAME])
            ->get();

        return $roles->pluck('userroles.*.user_id')
            ->flatten() // flatten array since result of pluck is per role
            ->unique() // add unique filter lessen loop size
            ->all();
    }
}
