<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAnnouncementsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.announcement',
            'display_name' => 'View Announcement',
            'description' => 'View Announcement',
            'module' => 'HRIS',
            'submodule' => 'Announcements',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ],
        [
            'name' => 'create.announcement',
            'display_name' => 'Create Announcement',
            'description' => 'Create Announcement',
            'module' => 'HRIS',
            'submodule' => 'Announcements',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ],
        [
            'name' => 'delete.announcement',
            'display_name' => 'Delete Announcement',
            'description' => 'Delete Announcement',
            'module' => 'HRIS',
            'submodule' => 'Announcements',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ],
        [
            'name' => 'edit.announcement',
            'display_name' => 'Edit Announcement',
            'description' => 'Edit Announcement',
            'module' => 'HRIS',
            'submodule' => 'Announcements',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ]
    ];
}
