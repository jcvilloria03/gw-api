<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertEssDefaultScheduleTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.view.default_schedule',
            'display_name' => 'View Default Schedule',
            'description' => 'View Default Schedule',
            'module' => 'ESS',
            'submodule' => 'ESS',
            'allowed_scopes' => [],
            'ess' => true
        ]
    ];
}
