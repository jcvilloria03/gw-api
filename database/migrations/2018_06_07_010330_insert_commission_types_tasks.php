<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertCommissionTypesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.commission_types',
            'display_name' => 'View Commission Types',
            'description' => 'View Commission Types',
            'module' => 'Payroll',
            'submodule' => 'Commission Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.commission_types',
            'display_name' => 'Edit Commission Types',
            'description' => 'Edit Commission Types',
            'module' => 'Payroll',
            'submodule' => 'Commission Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.commission_types',
            'display_name' => 'Create Commission Types',
            'description' => 'Create Commission Types',
            'module' => 'Payroll',
            'submodule' => 'Commission Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.commission_types',
            'display_name' => 'Delete Commission Types',
            'description' => 'Delete Commission Types',
            'module' => 'Payroll',
            'submodule' => 'Commission Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
