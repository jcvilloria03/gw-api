<?php

use Illuminate\Database\Migrations\Migration;

class RemoveGapLoansTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $taskIds = DB::table('tasks')
            ->where('name', 'LIKE', '%gap_loans%')
            ->select([ 'id' ])
            ->get()
            ->pluck('id');
        if ($taskIds->isNotEmpty()) {
            DB::table('permissions')
                ->whereIn('task_id', $taskIds)
                ->delete();
            DB::table('tasks')
                ->whereIn('id', $taskIds)
                ->delete();
        }
    }
}
