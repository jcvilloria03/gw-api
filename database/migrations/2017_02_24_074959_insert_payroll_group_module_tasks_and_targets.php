<?php

use Illuminate\Database\Migrations\Migration;

class InsertPayrollGroupModuleTasksAndTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'create.payroll_group',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'view.payroll_group',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);
        $this->createTask([
            'name' => 'view.company_payroll_groups',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'update.payroll_group',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
            ['name' => 'Payroll Group'],
        ]);
        $this->createTask([
            'name' => 'delete.payroll_group',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where('module', 'Payroll Group')->get();
        foreach ($tasks as $task) {
            DB::table('targets')->where('task_id', $task->id)->delete();
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
            DB::table('targets')->delete();
        }
    }

    private function createTask($attributes, $targets = [])
    {
        $module = "Payroll Group";
        $product = "Payroll";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
        ]);
        $this->createTargets($taskId, $targets);
    }

    private function createTargets($taskId, $targets)
    {
        foreach ($targets as $target) {
            DB::table('targets')->insert([
                'task_id' => $taskId,
                'name' => $target['name'],
            ]);
        }
    }
}
