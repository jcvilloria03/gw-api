<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class UpdateTasksTableUpdateModulesAndSubmodulesPayrollProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "UPDATE tasks
             SET module_access = CASE 
                 WHEN name IN (
                    'create.payroll_group','view.payroll_group','edit.payroll_group','delete.payroll_group',
                    'create.payroll','view.payroll','edit.payroll','delete.payroll','close.payroll','run.payroll',
                    'create.allowances','view.allowances','edit.allowances','delete.allowances',
                    'create.allowance_types','view.allowance_types','edit.allowance_types','delete.allowance_types',
                    'create.bonus_types','view.bonus_types','edit.bonus_types','delete.bonus_types',
                    'create.bonuses','view.bonuses','edit.bonuses','delete.bonuses',
                    'create.commission_types','view.commission_types','edit.commission_types','delete.commission_types',
                    'create.commissions','view.commissions','edit.commissions','delete.commissions',
                    'create.payroll_loans','view.payroll_loans',
                    'edit.payroll_loans','delete.payroll_loans',
                    'create.payroll_loan_types','view.payroll_loan_types',
                    'edit.payroll_loan_types','delete.payroll_loan_types',
                    'create.deductions','view.deductions','edit.deductions','delete.deductions',
                    'create.deduction_types','view.deduction_types','edit.deduction_types','delete.deduction_types',
                    'create.annual_earnings','view.annual_earnings','edit.annual_earnings','delete.annual_earnings',
                    'create.terminations','view.terminations','edit.terminations','delete.terminations',
                    'send_payslip.payroll','view.payslip','generate.payslip',
                    'create.government_form','view.government_form',
                    'create.final_pays','view.final_pays','edit.final_pays','delete.final_pays'
                    'create.basic_pay','view.basic_pay','edit.basic_pay','delete.basic_pay'
                    ) THEN 'Payroll'
                 ELSE module_access                
             END;"
        );

        // update module column
        DB::statement(
            "UPDATE tasks 
             SET module = CASE
                 WHEN name IN (
                    'create.terminations','view.terminations','edit.terminations','delete.terminations'
                    ) THEN 'Final Pay'
                 WHEN name IN (
                    'create.final_pays','view.final_pays','edit.final_pays','delete.final_pays'
                    ) THEN 'Final Pay'
                 WHEN name IN (
                    'create.payroll_group','view.payroll_group','edit.payroll_group','delete.payroll_group'
                    ) THEN 'Company'
                 WHEN name IN (
                    'create.payroll','view.payroll','edit.payroll','delete.payroll','close.payroll','run.payroll',
                    'create.allowances','view.allowances','edit.allowances','delete.allowances',
                    'create.bonuses','view.bonuses','edit.bonuses','delete.bonuses',
                    'create.commissions','view.commissions','edit.commissions','delete.commissions',
                    'create.payroll_loans','view.payroll_loans',
                    'edit.payroll_loans','delete.payroll_loans',
                    'create.deductions','view.deductions','edit.deductions','delete.deductions',
                    'send_payslip.payroll','view.payslip','generate.payslip',
                    'create.government_form','view.government_form'
                 ) THEN 'Payroll'
                 WHEN name IN (
                    'create.allowance_types','view.allowance_types','edit.allowance_types','delete.allowance_types',
                    'create.bonus_types','view.bonus_types','edit.bonus_types','delete.bonus_types',
                    'create.commission_types','view.commission_types','edit.commission_types','delete.commission_types',
                    'create.deduction_types','view.deduction_types','edit.deduction_types','delete.deduction_types',
                    'create.payroll_loan_types','view.payroll_loan_types',
                    'edit.payroll_loan_types','delete.payroll_loan_types'
                 ) THEN 'Payroll Settings'
                 WHEN name IN (
                    'create.annual_earnings','view.annual_earnings','edit.annual_earnings','delete.annual_earnings',
                    'create.basic_pay','view.basic_pay','edit.basic_pay','delete.basic_pay'
                 ) THEN 'Employee'
                 ELSE module   
             END;"
        );

        // update submodule column
        DB::statement(
            "UPDATE tasks
             SET submodule = CASE 
                 WHEN name IN (
                    'create.payroll_group','view.payroll_group','edit.payroll_group','delete.payroll_group'
                    ) THEN 'Payroll Group'
                 WHEN name IN (
                    'create.payroll','view.payroll','edit.payroll','delete.payroll'
                    ) THEN 'Payroll'
                 WHEN name IN ('close.payroll') THEN 'Payroll Status'
                 WHEN name IN ('run.payroll') THEN 'Process Payroll'
                 WHEN name IN (
                    'create.allowances','view.allowances','edit.allowances','delete.allowances'
                    ) THEN 'Allowances'
                 WHEN name IN (
                    'create.allowance_types','view.allowance_types','edit.allowance_types','delete.allowance_types'
                 ) THEN 'Allowance Type Settings'
                 WHEN name IN (
                    'create.bonus_types','view.bonus_types','edit.bonus_types','delete.bonus_types'
                 ) THEN 'Bonus Types'
                 WHEN name IN (
                    'create.bonuses','view.bonuses','edit.bonuses','delete.bonuses'
                    ) THEN 'Bonuses'
                 WHEN name IN (
                    'create.commission_types','view.commission_types','edit.commission_types','delete.commission_types'
                 ) THEN 'Commission Type Settings'
                 WHEN name IN (
                    'create.commissions','view.commission','edit.commissions','delete.commissions'
                    ) THEN 'Commissions'
                 WHEN name IN (
                    'create.payroll_loan_types','view.payroll_loan_types',
                    'edit.payroll_loan_types','delete.payroll_loan_types'
                 ) THEN 'Loan Type Settings'
                 WHEN name IN (
                    'create.payroll_loans','view.payroll_loans',
                    'edit.payroll_loans','delete.payroll_loans'
                 ) THEN 'Loans'
                 WHEN name IN (
                    'create.deductions','view.deductions','edit.deductions','delete.deductions'
                    ) THEN 'Deductions'
                 WHEN name IN (
                    'create.deduction_types','view.deduction_types','edit.deduction_types','delete.deduction_types'
                 ) THEN 'Deduction Type Settings'
                 WHEN name IN (
                    'create.annual_earnings','view.annual_earnings','edit.annual_earnings','delete.annual_earnings'
                 ) THEN 'Annual Earnings'
                 WHEN name IN (
                    'send_payslip.payroll','view.payslip','generate.payslip'
                 ) THEN 'Payslip'
                 WHEN name IN (
                    'create.terminations','view.terminations','edit.terminations','delete.terminations'
                    ) THEN 'Termination Information'
                 WHEN name IN (
                    'create.final_pays','view.final_pays','edit.final_pays','delete.final_pays'
                    ) THEN 'Final Pay'
                 WHEN name IN (
                    'create.basic_pay','view.basic_pay','edit.basic_pay','delete.basic_pay'
                    ) THEN 'Basic Pay'
                 ELSE submodule
             END;"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
