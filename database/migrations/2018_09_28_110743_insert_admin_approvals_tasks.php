<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAdminApprovalsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.request',
            'display_name' => 'View Request',
            'description' => 'View Request',
            'module' => 'Time and Attendance',
            'submodule' => 'Request Approvals',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ],
        [
            'name' => 'approval.request',
            'display_name' => 'Request Approval',
            'description' => 'Request Approval',
            'module' => 'Time and Attendance',
            'submodule' => 'Request Approvals',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ]
    ];
}
