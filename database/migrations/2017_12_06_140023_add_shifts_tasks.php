<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class AddShiftsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.shift',
            'display_name' => 'Create Shifts',
            'description' => 'Create Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Shift',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.shift',
            'display_name' => 'View Shifts',
            'description' => 'View Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Shift',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.shift',
            'display_name' => 'Edit Shifts',
            'description' => 'Edit Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Shift',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.shift',
            'display_name' => 'Delete Shifts',
            'description' => 'Delete Shifts',
            'module' => 'Time And Attendance',
            'submodule' => 'Shift',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
