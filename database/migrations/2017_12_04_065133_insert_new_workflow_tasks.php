<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewWorkflowTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.workflow',
            'display_name' => 'View Workflows',
            'description' => 'View Workflows',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.workflow',
            'display_name' => 'Create Workflows',
            'description' => 'Create Workflows',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.workflow',
            'display_name' => 'Delete Workflows',
            'description' => 'Delete Workflows',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.workflow',
            'display_name' => 'Edit Workflows',
            'description' => 'Edit Workflows',
            'module' => 'Time And Attendance',
            'submodule' => 'Workflow',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
