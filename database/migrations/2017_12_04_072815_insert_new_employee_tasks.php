<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewEmployeeTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.employee',
            'display_name' => 'View Employees',
            'description' => 'View Employees',
            'module' => 'HRIS',
            'submodule' => 'Employee',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.employee',
            'display_name' => 'Create Employees',
            'description' => 'Create Employees',
            'module' => 'HRIS',
            'submodule' => 'Employee',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.employee',
            'display_name' => 'Edit Employees',
            'description' => 'Edit Employees',
            'module' => 'HRIS',
            'submodule' => 'Employee',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
