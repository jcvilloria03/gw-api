<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewGapLoanTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.gap_loans',
            'display_name' => 'View Gap Loans',
            'description' => 'View Gap Loans',
            'module' => 'Gap Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.gap_loans',
            'display_name' => 'Edit Gap Loans',
            'description' => 'Edit Gap Loans',
            'module' => 'Gap Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.gap_loans',
            'display_name' => 'Create Gap Loans',
            'description' => 'Create Gap Loans',
            'module' => 'Gap Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.gap_loans',
            'display_name' => 'Delete Gap Loans',
            'description' => 'Delete Gap Loans',
            'module' => 'Gap Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
