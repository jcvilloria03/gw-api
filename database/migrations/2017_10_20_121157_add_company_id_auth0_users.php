<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdAuth0Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('employee_id');
            $table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auth0_users', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
