<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertMaxClockOutNewTimeSettingTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.max_clock_out',
            'display_name' => 'Create Maximum Clock-outs',
            'description' => 'Create Maximum Clock-outs',
            'module_access' => 'T&A',
            'module' => 'T&A',
            'submodule' => 'Maximum Clock-outs',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
            'sort_id' => 41,
        ],
        [
            'name' => 'delete.max_clock_out',
            'display_name' => 'Delete Maximum Clock-outs',
            'description' => 'Delete Maximum Clock-outs',
            'module_access' => 'T&A',
            'module' => 'T&A',
            'submodule' => 'Maximum Clock-outs',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
            'sort_id' => 41,
        ],
        [
            'name' => 'edit.max_clock_out',
            'display_name' => 'Edit Maximum Clock-outs',
            'description' => 'Edit Maximum Clock-outs',
            'module_access' => 'T&A',
            'module' => 'T&A',
            'submodule' => 'Maximum Clock-outs',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
            'sort_id' => 41,
        ],
        [
            'name' => 'view.max_clock_out',
            'display_name' => 'View Maximum Clock-outs',
            'description' => 'View Maximum Clock-outs',
            'module_access' => 'T&A',
            'module' => 'T&A',
            'submodule' => 'Maximum Clock-outs',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
            'sort_id' => 41,
        ]
    ];

     /**
     * Override trait method to add module_access and sort
     *
     * @param array $attributes
     * @return int
     */
    public function createTask(array $attributes)
    {
        $taskId = DB::table('tasks')->insertGetId([
            'name'           => $attributes['name'],
            'display_name'   => $attributes['display_name'],
            'description'    => $attributes['description'],
            'module_access'  => $attributes['module_access'],
            'module'         => $attributes['module'],
            'submodule'      => $attributes['submodule'],
            'allowed_scopes' => json_encode($attributes['allowed_scopes']),
            'sort_id'        => $attributes['sort_id'],
            'ess'            => $attributes['ess']
        ]);

        return $taskId;
    }
}
