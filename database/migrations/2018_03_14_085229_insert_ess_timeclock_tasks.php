<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertEssTimeclockTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'ess.clock.log',
            'display_name' => 'Clock log (clock in | clock out)',
            'description' => 'Clock log (clock in | clock out)',
            'module' => 'ESS',
            'submodule' => 'clock',
            'allowed_scopes' => [
                'Company'
            ],
            'ess' => true
        ],
        [
            'name' => 'ess.clock.view_time_records',
            'display_name' => 'View time records',
            'description' => 'View time records',
            'module' => 'ESS',
            'submodule' => 'clock',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => true
        ]
    ];
}
