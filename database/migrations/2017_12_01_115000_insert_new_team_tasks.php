<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewTeamTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.team',
            'display_name' => 'View Teams',
            'description' => 'View Teams',
            'module' => 'HRIS',
            'submodule' => 'Team',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.team',
            'display_name' => 'Edit Teams',
            'description' => 'Edit Teams',
            'module' => 'HRIS',
            'submodule' => 'Team',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.team',
            'display_name' => 'Delete Teams',
            'description' => 'Delete Teams',
            'module' => 'HRIS',
            'submodule' => 'Team',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.team',
            'display_name' => 'Create Teams',
            'description' => 'Create Teams',
            'module' => 'HRIS',
            'submodule' => 'Team',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],

    ];
}
