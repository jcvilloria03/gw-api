<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertViewNotificationTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.notification',
            'display_name' => 'View Admin Notifications',
            'description' => 'View Admin Notifications',
            'module' => 'HRIS',
            'submodule' => 'Notification',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ]
    ];
}
