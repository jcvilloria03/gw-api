<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertBonusTypesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.bonus_types',
            'display_name' => 'View Bonus Types',
            'description' => 'View Bonus Types',
            'module' => 'Payroll',
            'submodule' => 'Bonus Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.bonus_types',
            'display_name' => 'Edit Bonus Types',
            'description' => 'Edit Bonus Types',
            'module' => 'Payroll',
            'submodule' => 'Bonus Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.bonus_types',
            'display_name' => 'Create Bonus Types',
            'description' => 'Create Bonus Types',
            'module' => 'Payroll',
            'submodule' => 'Bonus Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.bonus_types',
            'display_name' => 'Delete Bonus Types',
            'description' => 'Delete Bonus Types',
            'module' => 'Payroll',
            'submodule' => 'Bonus Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
