<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewLocationTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.location',
            'display_name' => 'Create Locations',
            'description' => 'Create Locations',
            'module' => 'HRIS',
            'submodule' => 'Location',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.location',
            'display_name' => 'View Locations',
            'description' => 'View Locations',
            'module' => 'HRIS',
            'submodule' => 'Location',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.location',
            'display_name' => 'Edit Locations',
            'description' => 'Edit Locations',
            'module' => 'HRIS',
            'submodule' => 'Location',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.location',
            'display_name' => 'Delete Locations',
            'description' => 'Delete Locations',
            'module' => 'HRIS',
            'submodule' => 'Location',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
