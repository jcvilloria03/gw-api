<?php

use App\Model\Role;
use Illuminate\Database\Migrations\Migration;

class ModifyTypeFieldOnRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = collect(Role::ROLE_TYPES)
            ->map(function ($role) {
                return "'" . $role . "'";
            })
            ->implode(',');
        DB::statement(
            "ALTER TABLE roles CHANGE COLUMN type type ENUM({$types}) NULL DEFAULT '" .
            Role::ADMIN .
            "'"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(
            "ALTER TABLE roles MODIFY COLUMN type VARCHAR(120) NULL DEFAULT '" .
            Role::ADMIN .
            "'"
        );
    }

}
