<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertLeaveRequestsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.leave_request',
            'display_name' => 'View Leave Request',
            'description' => 'View Leave Request',
            'module' => 'Leave',
            'submodule' => 'Leave Requests',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.leave_request',
            'display_name' => 'Edit Leave Request',
            'description' => 'Edit Leave Request',
            'module' => 'Leave',
            'submodule' => 'Leave Requests',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.leave_request',
            'display_name' => 'Create Leave Request',
            'description' => 'Create Leave Request',
            'module' => 'Leave',
            'submodule' => 'Leave Requests',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.leave_request',
            'display_name' => 'Delete Leave Request',
            'description' => 'Delete Leave Request',
            'module' => 'Leave',
            'submodule' => 'Leave Requests',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ]
    ];
}
