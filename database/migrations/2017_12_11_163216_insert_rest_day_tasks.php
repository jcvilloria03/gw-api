<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertRestDayTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.rest_day',
            'display_name' => 'Create Rest Days',
            'description' => 'Create Rest Days',
            'module' => 'Time And Attendance',
            'submodule' => 'Rest Day',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.rest_day',
            'display_name' => 'View Rest Days',
            'description' => 'View Rest Days',
            'module' => 'Time And Attendance',
            'submodule' => 'Rest Day',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.rest_day',
            'display_name' => 'Edit Rest Days',
            'description' => 'Edit Rest Days',
            'module' => 'Time And Attendance',
            'submodule' => 'Rest Day',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.rest_day',
            'display_name' => 'Delete Rest Days',
            'description' => 'Delete Rest Days',
            'module' => 'Time And Attendance',
            'submodule' => 'Rest Day',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
