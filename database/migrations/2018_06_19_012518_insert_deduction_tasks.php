<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertDeductionTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.deductions',
            'display_name' => 'View Deductions',
            'description' => 'View Deductions',
            'module' => 'Payroll',
            'submodule' => 'Deduction',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.deductions',
            'display_name' => 'Edit Deductions',
            'description' => 'Edit Deductions',
            'module' => 'Payroll',
            'submodule' => 'Deduction',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.deductions',
            'display_name' => 'Create Deductions',
            'description' => 'Create Deductions',
            'module' => 'Payroll',
            'submodule' => 'Deduction',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.deductions',
            'display_name' => 'Delete Deductions',
            'description' => 'Delete Deductions',
            'module' => 'Payroll',
            'submodule' => 'Deduction',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
