<?php

use Illuminate\Database\Migrations\Migration;

class ActivateAuth0Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Model\Auth0User::where('status', \App\Model\Auth0User::STATUS_INACTIVE)
            ->update(['status' => \App\Model\Auth0User::STATUS_ACTIVE]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
