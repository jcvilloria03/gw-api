<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Role;

class RemoveCompanyNameFromDefaultRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles = Role::whereIn('type', ['Admin', 'Employee'])
            ->where('custom_role', 0)
            ->whereNotIn('name', ['Super Admin', 'Owner', 'Permissionless'])
            ->get();

        $roles->each(function ($role) {
            $role['type'] === 'Admin'
                ? $role->update(['name' => 'Admin'])
                : $role->update(['name' => 'Employee']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
