<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertTimeRecordsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'edit.time_records',
            'display_name' => 'Edit Time Records',
            'description' => 'Edit Time Records',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.time_records',
            'display_name' => 'View Time Records',
            'description' => 'View Time Records',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ]
    ];
}
