<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewSchedulesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.schedule',
            'display_name' => 'Create Schedules',
            'description' => 'Create Schedules',
            'module' => 'Time And Attendance',
            'submodule' => 'Schedule',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.schedule',
            'display_name' => 'View Schedules',
            'description' => 'View Schedules',
            'module' => 'Time And Attendance',
            'submodule' => 'Schedule',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.schedule',
            'display_name' => 'Edit Schedules',
            'description' => 'Edit Schedules',
            'module' => 'Time And Attendance',
            'submodule' => 'Schedule',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.schedule',
            'display_name' => 'Delete Schedules',
            'description' => 'Delete Schedules',
            'module' => 'Time And Attendance',
            'submodule' => 'Schedule',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
