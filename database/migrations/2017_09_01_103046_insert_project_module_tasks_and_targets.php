<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertProjectModuleTasksAndTargets extends Migration
{

   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'create.project',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'view.project',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company']
        ]);
        $this->createTask([
            'name' => 'view.projects',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'update.project',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company']
        ]);
        $this->createTask([
            'name' => 'delete.project',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where('module', 'Project')->get();
        foreach ($tasks as $task) {
            DB::table('targets')->where('task_id', $task->id)->delete();
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
            DB::table('targets')->delete();
        }
    }

    private function createTask($attributes, $targets = [])
    {
        $module = "Project";
        $product = "Salarium";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
        ]);
        $this->createTargets($taskId, $targets);
        $this->addPermissionToExistingRoles($taskId);
        return $taskId;
    }

    private function createTargets($taskId, $targets)
    {
        foreach ($targets as $target) {
            DB::table('targets')->insert([
                'task_id' => $taskId,
                'name' => $target['name'],
            ]);
        }
    }

    private function addPermissionToExistingRoles($taskId)
    {
        $existingPermissions = DB::table('permissions')->select('role_id', 'scope')->distinct()->get();
        foreach ($existingPermissions as $permission) {
            DB::table('permissions')->insert([
                'task_id' => $taskId,
                'role_id' => $permission->role_id,
                'scope' => $permission->scope,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
