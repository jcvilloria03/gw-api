<?php

use Illuminate\Database\Migrations\Migration;

class InsertEssEmployeeModuleTasksAndTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'view.employee.ess',
        ], []);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where([
            'name' => 'view.employee.ess'
        ])->get();
        foreach ($tasks as $task) {
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
        }
    }

    private function createTask($attributes)
    {
        $module = "Employee";
        $product = "Salarium";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
            'ess' => true
        ]);
        $this->addPermissionToExistingEmployeeRoles($taskId);
    }

    private function addPermissionToExistingEmployeeRoles($taskId)
    {
        $employeeRoles = DB::table('roles')
            ->select('id')
            ->where(['type' => 'Employee'])
            ->get();
        foreach ($employeeRoles as $role) {
            DB::table('permissions')->insert([
                'task_id' => $taskId,
                'role_id' => $role->id,
                'scope' => '{}',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
