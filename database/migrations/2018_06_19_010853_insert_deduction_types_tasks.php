<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertDeductionTypesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.deduction_types',
            'display_name' => 'View Deduction Types',
            'description' => 'View Deduction Types',
            'module' => 'Payroll',
            'submodule' => 'Deduction Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.deduction_types',
            'display_name' => 'Edit Deduction Types',
            'description' => 'Edit Deduction Types',
            'module' => 'Payroll',
            'submodule' => 'Deduction Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.deduction_types',
            'display_name' => 'Create Deduction Types',
            'description' => 'Create Deduction Types',
            'module' => 'Payroll',
            'submodule' => 'Deduction Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.deduction_types',
            'display_name' => 'Delete Deduction Types',
            'description' => 'Delete Deduction Types',
            'module' => 'Payroll',
            'submodule' => 'Deduction Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
