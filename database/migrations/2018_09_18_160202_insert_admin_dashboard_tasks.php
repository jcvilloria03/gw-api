<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAdminDashboardTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.admin_dashboard',
            'display_name' => 'View Admin Dashboard',
            'description' => 'View Admin Dashboard',
            'module' => 'HRIS',
            'submodule' => 'AdminDashboard',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ]
    ];
}
