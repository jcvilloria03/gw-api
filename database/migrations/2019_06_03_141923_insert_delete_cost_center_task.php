<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertDeleteCostCenterTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'delete.cost_center',
            'display_name' => 'Delete Cost Centers',
            'description' => 'Delete Cost Centers',
            'module' => 'HRIS',
            'submodule' => 'Cost Center',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
