<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Role;

class AlterRolesTableInsertModuleAccessColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->text('module_access')
                ->after('custom_role');
        });

        $this->updateOldRolesModuleAccessFieldWithDefaultValues();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('module_access');
        });
    }

    /**
     * Iterates through roles and sets default value for system defined roles..
     */
    private function updateOldRolesModuleAccessFieldWithDefaultValues()
    {
        Role::withTrashed()->chunk(2000, function ($roles) {
            foreach ($roles as $role) {
                $role->module_access = $role->custom_role
                    ? (array)Role::ACCESS_MODULE_HRIS
                    : Role::MODULE_ACCESSES;

                $role->save();
            }
        });
    }
}
