<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollGroupTasksUpdate extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'edit.payroll_group',
            'display_name' => 'Edit Payroll Groups',
            'description' => 'Edit Payroll Groups',
            'module' => 'HRIS',
            'submodule' => 'Payroll Group',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
