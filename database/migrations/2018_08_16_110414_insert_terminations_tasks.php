<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertTerminationsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.terminations',
            'display_name' => 'View Terminations',
            'description' => 'View Terminations',
            'module' => 'Payroll',
            'submodule' => 'Termination',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.terminations',
            'display_name' => 'Edit Terminations',
            'description' => 'Edit Terminations',
            'module' => 'Payroll',
            'submodule' => 'Termination',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.terminations',
            'display_name' => 'Create Terminations',
            'description' => 'Create Terminations',
            'module' => 'Payroll',
            'submodule' => 'Termination',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.terminations',
            'display_name' => 'Delete Terminations',
            'description' => 'Delete Terminations',
            'module' => 'Payroll',
            'submodule' => 'Termination',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
