<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewEmploymentTypeTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.employment_type',
            'display_name' => 'View Employment Types',
            'description' => 'View Employment Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Employment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.employment_type',
            'display_name' => 'Create Employment Types',
            'description' => 'Create Employment Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Employment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.employment_type',
            'display_name' => 'Delete Employment Types',
            'description' => 'Delete Employment Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Employment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.employment_type',
            'display_name' => 'Edit Employment Types',
            'description' => 'Edit Employment Types',
            'module' => 'Time And Attendance',
            'submodule' => 'Employment Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
