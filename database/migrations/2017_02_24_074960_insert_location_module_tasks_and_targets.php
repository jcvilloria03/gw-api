<?php

use Illuminate\Database\Migrations\Migration;

class InsertLocationModuleTasksAndTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'create.location',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'view.location',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company']
        ]);
        $this->createTask([
            'name' => 'view.company_locations',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
        $this->createTask([
            'name' => 'update.location',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company']
        ]);
        $this->createTask([
            'name' => 'delete.location',
        ], [
            ['name' => 'Account'],
            ['name' => 'Company'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where('module', 'Location')->get();
        foreach ($tasks as $task) {
            DB::table('targets')->where('task_id', $task->id)->delete();
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
            DB::table('targets')->delete();
        }
    }

    private function createTask($attributes, $targets = [])
    {
        $module = "Location";
        $product = "Salarium";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
        ]);
        $this->createTargets($taskId, $targets);
    }

    private function createTargets($taskId, $targets)
    {
        foreach ($targets as $target) {
            DB::table('targets')->insert([
                'task_id' => $taskId,
                'name' => $target['name'],
            ]);
        }
    }
}
