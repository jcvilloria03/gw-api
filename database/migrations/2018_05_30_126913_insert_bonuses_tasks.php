<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertBonusesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.bonuses',
            'display_name' => 'View Bonuses',
            'description' => 'View Bonuses',
            'module' => 'Payroll',
            'submodule' => 'Bonus',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.bonuses',
            'display_name' => 'Edit Bonuses',
            'description' => 'Edit Bonuses',
            'module' => 'Payroll',
            'submodule' => 'Bonus',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.bonuses',
            'display_name' => 'Create Bonuses',
            'description' => 'Create Bonuses',
            'module' => 'Payroll',
            'submodule' => 'Bonus',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.bonuses',
            'display_name' => 'Delete Bonuses',
            'description' => 'Delete Bonuses',
            'module' => 'Payroll',
            'submodule' => 'Bonus',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
