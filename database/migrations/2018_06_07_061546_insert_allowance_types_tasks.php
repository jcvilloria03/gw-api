<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAllowanceTypesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.allowance_types',
            'display_name' => 'View Allowance Types',
            'description' => 'View Allowance Types',
            'module' => 'Payroll',
            'submodule' => 'Allowance Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.allowance_types',
            'display_name' => 'Edit Allowance Types',
            'description' => 'Edit Allowance Types',
            'module' => 'Payroll',
            'submodule' => 'Allowance Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.allowance_types',
            'display_name' => 'Create Allowance Types',
            'description' => 'Create Allowance Types',
            'module' => 'Payroll',
            'submodule' => 'Allowance Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.allowance_types',
            'display_name' => 'Delete Allowance Types',
            'description' => 'Delete Allowance Types',
            'module' => 'Payroll',
            'submodule' => 'Allowance Type',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
