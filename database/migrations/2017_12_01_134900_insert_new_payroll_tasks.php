<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payroll',
            'display_name' => 'View Payrolls',
            'description' => 'View Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.payroll',
            'display_name' => 'Edit Payrolls',
            'description' => 'Edit Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.payroll',
            'display_name' => 'Delete Payrolls',
            'description' => 'Delete Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.payroll',
            'display_name' => 'Create Payrolls',
            'description' => 'Create Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'close.payroll',
            'display_name' => 'Close Payrolls',
            'description' => 'Close Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'run.payroll',
            'display_name' => 'Run Payrolls',
            'description' => 'Run Payrolls',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
        [
            'name' => 'send_payslip.payroll',
            'display_name' => 'Send Payslips',
            'description' => 'Send Payslips',
            'module' => 'Payroll',
            'submodule' => 'Payroll',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Payroll Group'
            ],
            'ess' => false,
        ],
    ];
}
