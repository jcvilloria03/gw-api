<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class AlterTableTasksAddColumnsHiddenAndSortId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->integer('sort_id')->after('ess')->nullable();
            $table->boolean('hidden')->after('sort_id')->default(0);
        });

        DB::statement(
            "UPDATE tasks
                SET sort_id = CASE
                    WHEN name IN (
                        'view.account', 'edit.account'
                    ) THEN 1
                    WHEN name IN (
                        'create.company', 'view.company','edit.company' ,'delete.company'
                    ) THEN 2
                    WHEN name IN (
                        'view.user', 'create.user' ,'delete.user', 'edit.user'
                    ) THEN 3
                    WHEN name IN (
                        'view.role', 'create.role' ,'delete.role', 'edit.role'
                    ) THEN 4
                    WHEN name IN (
                        'view.account_logs'
                    ) THEN 5
                    WHEN name IN (
                        'view.company_logs'
                    ) THEN 6
                    WHEN name IN (
                        'create.location', 'view.location', 'edit.location', 'delete.location'
                    ) THEN 7
                    WHEN name IN (
                        'view.department', 'edit.department', 'delete.department', 'create.department'
                    ) THEN 8
                    WHEN name IN (
                        'create.position', 'view.position', 'edit.position', 'delete.position'
                    ) THEN 9
                    WHEN name IN (
                        'create.rank', 'delete.rank', 'edit.rank', 'view.rank'
                    ) THEN 10
                    WHEN name IN (
                        'view.team', 'edit.team', 'delete.team', 'create.team'
                    ) THEN 11
                    WHEN name IN (
                        'view.employment_type', 'create.employment_type',
                        'delete.employment_type', 'edit.employment_type'
                    ) THEN 12
                    WHEN name IN (
                        'create.project', 'view.project', 'edit.project', 'delete.project'
                    ) THEN 13
                    WHEN name IN (
                        'view.cost_center', 'create.cost_center'
                    ) THEN 14
                    WHEN name IN (
                        'view.workflow', 'create.workflow', 'delete.workflow', 'edit.workflow'
                    ) THEN 16
                    WHEN name IN (
                        'view.workflow_entitlement',
                        'create.workflow_entitlement',
                        'delete.workflow_entitlement',
                        'edit.workflow_entitlement'
                    ) THEN 17
                    WHEN name IN (
                        'ess.view.default_schedule', 'ess.view.payslip',
                        'ess.view.profile', 'view.employee_request_modules',
                        'ess.view.shift', 'ess.view.notification',
                        'ess.clock.log', 'ess.clock.view_time_records',
                        'ess.view.payroll_group', 'ess.view.hours_worked',
                        'ess.view.request', 'ess.create.request',
                        'ess.update.request', 'ess.approval.request', 'ess.cancel.request'
                    ) THEN 18
                    WHEN name IN (
                        'ess.view.announcement', 'ess.create.announcement',
                        'ess.delete.announcement', 'ess.edit.announcement'
                    ) THEN 19
                    WHEN name IN (
                        'edit.announcement', 'view.announcement',
                        'create.announcement', 'delete.announcement'
                    ) THEN 20
                    WHEN name IN (
                        'view.request', 'approval.request', 'edit.request'
                    ) THEN 21
                    WHEN name IN (
                        'view.notification'
                    ) THEN 22
                    WHEN name IN (
                        'view.admin_dashboard', 'create.scorecard.export'
                    ) THEN 23
                    WHEN name IN (
                        'view.employee', 'create.employee', 'edit.employee'
                    ) THEN 24
                    WHEN name IN (
                        'create.day_hour_rate', 'edit.day_hour_rate', 'view.day_hour_rate'
                    ) THEN 25
                    WHEN name IN (
                        'create.leave_type', 'delete.leave_type',
                        'edit.leave_type', 'view.leave_type'
                    ) THEN 26
                    WHEN name IN (
                        'create.leave_entitlement', 'delete.leave_entitlement',
                        'edit.leave_entitlement', 'view.leave_entitlement'
                    ) THEN 27
                    WHEN name IN (
                        'edit.default_schedule', 'create.default_schedule',
                        'view.default_schedule'
                    ) THEN 28
                    WHEN name IN (
                        'create.rest_day', 'view.rest_day',
                        'edit.rest_day', 'delete.rest_day'
                    ) THEN 29
                    WHEN name IN (
                        'create.tardiness_rule', 'delete.tardiness_rule',
                        'edit.tardiness_rule', 'view.tardiness_rule'
                    ) THEN 30
                    WHEN name IN (
                        'edit.night_shift', 'view.night_shift'
                    ) THEN 31
                    WHEN name IN (
                        'create.holiday', 'delete.holiday', 'edit.holiday', 'view.holiday'
                    ) THEN 32
                    WHEN name IN (
                        'create.schedule', 'view.schedule', 'edit.schedule', 'delete.schedule'
                    ) THEN 33
                    WHEN name IN (
                        'create.shift', 'view.shift', 'edit.shift', 'delete.shift'
                    ) THEN 34
                    WHEN name IN (
                        'view.leave_credit', 'edit.leave_credit',
                        'create.leave_credit', 'delete.leave_credit'
                    ) THEN 35
                    WHEN name IN (
                        'view.leave_request', 'edit.leave_request',
                        'create.leave_request', 'delete.leave_request'
                    ) THEN 36
                    WHEN name IN (
                        'edit.time_records', 'view.time_records',
                        'edit.hours_worked', 'view.hours_worked'
                    ) THEN 37
                    WHEN name IN (
                        'create.attendance_view', 'edit.attendance_view', 'view.attendance_view'
                    ) THEN 38
                    WHEN name IN (
                        'create.basic_pay', 'request.payroll_loans',
                        'approve.payroll_loans', 'view.adjustments',
                        'edit.adjustments', 'create.adjustments',
                        'delete.adjustments', 'view.adjustment_types',
                        'edit.adjustment_types', 'create.adjustment_types',
                        'delete.adjustment_types', 'close.final_pays',
                        'view.earnings', 'edit.earnings', 'create.earnings',
                        'view.special_pay_run', 'edit.special_pay_run',
                        'create.special_pay_run', 'delete.special_pay_run',
                        'close.special_pay_run', 'send.special_pay_run',
                        'run.special_pay_run', 'create.payroll_group',
                        'view.payroll_group', 'delete.payroll_group',
                        'create.government_form', 'view.government_form',
                        'view.payroll', 'edit.payroll', 'delete.payroll',
                        'create.payroll', 'close.payroll', 'run.payroll',
                        'send_payslip.payroll', 'view.payslip',
                        'generate.payslip', 'view.payroll_loan_types',
                        'edit.payroll_loan_types', 'delete.payroll_loan_types',
                        'create.payroll_loan_types', 'view.payroll_loans',
                        'edit.payroll_loans', 'create.payroll_loans',
                        'delete.payroll_loans', 'edit.payroll_group', 'view.bonus_types',
                        'edit.bonus_types', 'create.bonus_types',
                        'delete.bonus_types', 'view.bonuses', 'edit.bonuses',
                        'create.bonuses', 'delete.bonuses', 'view.commissions',
                        'edit.commissions', 'create.commissions', 'delete.commissions',
                        'view.commission_types', 'edit.commission_types',
                        'create.commission_types', 'delete.commission_types', 'view.allowances',
                        'edit.allowances', 'create.allowances',
                        'delete.allowances', 'view.allowance_types', 'edit.allowance_types',
                        'create.allowance_types', 'delete.allowance_types',
                        'view.deduction_types', 'edit.deduction_types', 'create.deduction_types',
                        'delete.deduction_types', 'view.deductions',
                        'edit.deductions', 'create.deductions', 'delete.deductions',
                        'view.annual_earnings', 'edit.annual_earnings',
                        'create.annual_earnings', 'view.terminations', 'edit.terminations',
                        'create.terminations', 'delete.terminations',
                        'view.final_pays', 'edit.final_pays', 'create.final_pays',
                        'view.basic_pay', 'edit.basic_pay', 'delete.basic_pay',
                        'view.system_defined_payroll_loans', 'edit.system_defined_payroll_loans',
                        'create.system_defined_payroll_loans', 'delete.system_defined_payroll_loans',
                        'request.system_defined_payroll_loans', 'approve.system_defined_payroll_loans'
                    ) THEN 39
                    ELSE sort_id
            END;"
        );

        DB::statement(
            "UPDATE tasks
                SET hidden = CASE
                    WHEN name IN (
                        'ess.view.default_schedule', 'ess.view.payslip',
                        'ess.view.profile', 'view.employee_request_modules',
                        'ess.view.shift', 'ess.view.notification',
                        'ess.clock.log', 'ess.clock.view_time_records',
                        'ess.view.payroll_group', 'ess.view.hours_worked',
                        'ess.view.request', 'ess.create.request',
                        'ess.update.request', 'ess.approval.request',
                        'ess.cancel.request', 'ess.view.announcement',
                        'ess.create.announcement', 'ess.delete.announcement',
                        'ess.edit.announcement', 'create.day_hour_rate',
                        'create.attendance_view', 'edit.attendance_view',
                        'view.attendance_view'
                    ) THEN 1
                    ELSE hidden
            END;"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('sort_id');
            $table->dropColumn('hidden');
        });
    }
}
