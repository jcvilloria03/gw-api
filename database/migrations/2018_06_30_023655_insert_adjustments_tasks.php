<?php

use Illuminate\Database\Migrations\Migration;

class InsertAdjustmentsTasks extends Migration
{
    use \Database\Migrations\TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.adjustments',
            'display_name' => 'View Adjustments',
            'description' => 'View Adjustments',
            'module' => 'Payroll',
            'submodule' => 'Adjustment',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.adjustments',
            'display_name' => 'Edit Adjustments',
            'description' => 'Edit Adjustments',
            'module' => 'Payroll',
            'submodule' => 'Adjustment',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.adjustments',
            'display_name' => 'Create Adjustments',
            'description' => 'Create Adjustments',
            'module' => 'Payroll',
            'submodule' => 'Adjustment',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.adjustments',
            'display_name' => 'Delete Adjustments',
            'description' => 'Delete Adjustments',
            'module' => 'Payroll',
            'submodule' => 'Adjustment',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
