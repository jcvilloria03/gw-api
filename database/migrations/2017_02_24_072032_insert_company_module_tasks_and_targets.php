<?php

use Illuminate\Database\Migrations\Migration;

class InsertCompanyModuleTasksAndTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTask([
            'name' => 'create.company',
        ], [
            [
                'name' => 'Account',
            ]
        ]);
        $this->createTask([
            'name' => 'view.company',
        ], [
            ['name' => 'Account',],
            ['name' => 'Company',],
        ]);
        $this->createTask([
            'name' => 'view.companies',
        ], [
            ['name' => 'Account',],
        ]);
        $this->createTask([
            'name' => 'update.company',
        ], [
            ['name' => 'Account',],
            ['name' => 'Company',],
        ]);
        $this->createTask([
            'name' => 'delete.company',
        ], [
            ['name' => 'Account'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tasks = DB::table('tasks')->where('module', 'Company')->get();
        foreach ($tasks as $task) {
            DB::table('targets')->where('task_id', $task->id)->delete();
            DB::table('permissions')->where('task_id', $task->id)->delete();
            DB::table('tasks')->where('id', $task->id)->delete();
            DB::table('targets')->delete();
        }
    }

    private function createTask($attributes, $targets = [])
    {
        $module = "Company";
        $product = "Salarium";
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'module' => $module,
            'product' => $product,
        ]);
        $this->createTargets($taskId, $targets);
    }

    private function createTargets($taskId, $targets)
    {
        foreach ($targets as $target) {
            DB::table('targets')->insert([
                'task_id' => $taskId,
                'name' => $target['name'],
            ]);
        }
    }
}
