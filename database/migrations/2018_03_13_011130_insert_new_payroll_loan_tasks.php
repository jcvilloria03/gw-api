<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPayrollLoanTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.payroll_loans',
            'display_name' => 'View Payroll Loans',
            'description' => 'View Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.payroll_loans',
            'display_name' => 'Edit Payroll Loans',
            'description' => 'Edit Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.payroll_loans',
            'display_name' => 'Create Payroll Loans',
            'description' => 'Create Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.payroll_loans',
            'display_name' => 'Delete Payroll Loans',
            'description' => 'Delete Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'request.payroll_loans',
            'display_name' => 'Request Payroll Loans',
            'description' => 'Request Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => true,
        ],
        [
            'name' => 'approve.payroll_loans',
            'display_name' => 'Approve Payroll Loans',
            'description' => 'Approve Payroll Loans',
            'module' => 'Payroll Loan',
            'submodule' => 'Loan',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
