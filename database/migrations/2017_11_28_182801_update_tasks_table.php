<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('display_name')->after('name');
            $table->string('description')->after('display_name')->nullable();
            $table->string('submodule')->after('module');
            $table->string('allowed_scopes')->after('submodule');
            $table->index('allowed_scopes');
            $table->dropColumn(['product']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn([
                'display_name',
                'description',
                'submodule',
                'allowed_scopes',
            ]);
            $table->string('product')->after('module')->nullable();
        });
    }
}
