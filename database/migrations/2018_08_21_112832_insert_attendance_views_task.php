<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertAttendanceViewsTask extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.attendance_view',
            'display_name' => 'Create Attendance Views',
            'description' => 'Create Attendance Views',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.attendance_view',
            'display_name' => 'Edit Attendance Views',
            'description' => 'Edit Attendance Views',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.attendance_view',
            'display_name' => 'View Attendance Views',
            'description' => 'View Attendance Views',
            'module' => 'Attendance',
            'submodule' => 'Attendance Computation',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ]
    ];
}
