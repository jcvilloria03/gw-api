<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertLeaveCreditsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.leave_credit',
            'display_name' => 'View Leave Credit',
            'description' => 'View Leave Credit',
            'module' => 'Leave',
            'submodule' => 'Leave Credits',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.leave_credit',
            'display_name' => 'Edit Leave Credit',
            'description' => 'Edit Leave Credit',
            'module' => 'Leave',
            'submodule' => 'Leave Credits',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.leave_credit',
            'display_name' => 'Create Leave Credit',
            'description' => 'Create Leave Credit',
            'module' => 'Leave',
            'submodule' => 'Leave Credits',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.leave_credit',
            'display_name' => 'Delete Leave Credit',
            'description' => 'Delete Leave Credit',
            'module' => 'Leave',
            'submodule' => 'Leave Credits',
            'allowed_scopes' => [
                'Account',
                'Company',
                'Team'
            ],
            'ess' => false,
        ]
    ];
}
