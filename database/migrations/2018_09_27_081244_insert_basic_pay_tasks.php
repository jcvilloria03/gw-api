<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertBasicPayTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.basic_pay',
            'display_name' => 'View Basic Pay',
            'description' => 'View Basic Pay',
            'module' => 'Payroll',
            'submodule' => 'Basic Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.basic_pay',
            'display_name' => 'Edit Basic Pay',
            'description' => 'Edit Basic Pay',
            'module' => 'Payroll',
            'submodule' => 'Basic Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.basic_pay',
            'display_name' => 'Create Basic Pay',
            'description' => 'Create Basic Pay',
            'module' => 'Payroll',
            'submodule' => 'Basic Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.basic_pay',
            'display_name' => 'Delete Basic Pay',
            'description' => 'Delete Basic Pay',
            'module' => 'Payroll',
            'submodule' => 'Basic Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
