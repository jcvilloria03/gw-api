<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewCompanyTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.company',
            'display_name' => 'Create Companies',
            'description' => 'Create Companies',
            'module' => 'HRIS',
            'submodule' => 'Company',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.company',
            'display_name' => 'View Companies',
            'description' => 'View Companies',
            'module' => 'HRIS',
            'submodule' => 'Company',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.company',
            'display_name' => 'Edit Companies',
            'description' => 'Edit Companies',
            'module' => 'HRIS',
            'submodule' => 'Company',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.company',
            'display_name' => 'Delete Companies',
            'description' => 'Delete Companies',
            'module' => 'HRIS',
            'submodule' => 'Company',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.company_logs',
            'display_name' => 'View Company Logs',
            'description' => 'View Company Logs',
            'module' => 'HRIS',
            'submodule' => 'Company',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],

    ];
}
