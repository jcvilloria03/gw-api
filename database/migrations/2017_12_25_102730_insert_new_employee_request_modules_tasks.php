<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewEmployeeRequestModulesTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.employee_request_modules',
            'display_name' => 'View Employee Request Modules',
            'description' => 'View Employee Request Modules',
            'module' => 'Time And Attendance',
            'submodule' => 'EmployeeRequestModule',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
    ];
}
