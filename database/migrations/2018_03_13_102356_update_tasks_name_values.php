<?php

use Illuminate\Database\Migrations\Migration;
use App\Model\Task;

class UpdateTasksNameValues extends Migration
{
    private $changesPairs = [
        'ess.create.Announcement' => 'ess.create.announcement',
        'ess.delete.Announcement' => 'ess.delete.announcement',
        'ess.edit.Announcement' => 'ess.edit.announcement'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->updateNameValues($this->changesPairs);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->updateNameValues(array_flip($this->changesPairs));
    }

    /**
     * Updating tasks names
     *
     * @param array $valuePairs Array of old-new values pairs
     * @return void
     */
    public function updateNameValues(array $valuePairs)
    {
        foreach ($valuePairs as $old => $new) {
            Task::where('name', $old)->update([
                'name' => $new
            ]);
        }
    }
}
