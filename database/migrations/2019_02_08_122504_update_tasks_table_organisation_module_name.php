<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableOrganisationModuleName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tasks')
            ->where('module', 'Organization Settings')
            ->update(['module' => 'Organisation Settings']);

        DB::table('tasks')
            ->where('name', 'update.account')
            ->update(['name' => 'edit.account']);

        DB::table('tasks')
            ->where('name', 'update.request')
            ->update(['name' => 'edit.request']);

        DB::table('tasks')
            ->where('name', 'update.user')
            ->update(['name' => 'edit.user']);

        DB::table('tasks')
            ->where('name', 'update.role')
            ->update(['name' => 'edit.role']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tasks')
            ->where('module', 'Organisation Settings')
            ->update('module', 'Organization Settings');

        DB::table('tasks')
            ->where('name', 'edit.account')
            ->update(['name' => 'update.account']);

        DB::table('tasks')
            ->where('name', 'edit.request')
            ->update(['name' => 'update.request']);

        DB::table('tasks')
            ->where('name', 'edit.user')
            ->update(['name' => 'update.user']);

        DB::table('tasks')
            ->where('name', 'edit.role')
            ->update(['name' => 'update.role']);
    }
}
