<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewPositionTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.position',
            'display_name' => 'Create Positions',
            'description' => 'Create Positions',
            'module' => 'HRIS',
            'submodule' => 'Position',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.position',
            'display_name' => 'View Positions',
            'description' => 'View Positions',
            'module' => 'HRIS',
            'submodule' => 'Position',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.position',
            'display_name' => 'Edit Positions',
            'description' => 'Edit Positions',
            'module' => 'HRIS',
            'submodule' => 'Position',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.position',
            'display_name' => 'Delete Positions',
            'description' => 'Delete Positions',
            'module' => 'HRIS',
            'submodule' => 'Position',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
