<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertFinalPayTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.final_pays',
            'display_name' => 'View Final Pays',
            'description' => 'View Final Pays',
            'module' => 'Payroll',
            'submodule' => 'Final Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.final_pays',
            'display_name' => 'Edit Final Pays',
            'description' => 'Edit Final Pays',
            'module' => 'Payroll',
            'submodule' => 'Final Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.final_pays',
            'display_name' => 'Create Final Pays',
            'description' => 'Create Final Pays',
            'module' => 'Payroll',
            'submodule' => 'Final Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'close.final_pays',
            'display_name' => 'Close Final Pays',
            'description' => 'Close Final Pays',
            'module' => 'Payroll',
            'submodule' => 'Final Pay',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
