<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTableUpdateModulesAndSubmodules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update module_access column
        DB::statement(
            "UPDATE tasks
             SET module_access = CASE 
                 WHEN name IN (
                    'view.team','edit.team','delete.team','create.team',
                    'view.project','edit.project','delete.project','create.project',
                    'view.leave_type','edit.leave_type','delete.leave_type','create.leave_type',
                    'create.default_schedule', 'view.default_schedule', 'update.default_schedule',
                    'view.tardiness_rule','edit.tardiness_rule','delete.tardiness_rule','create.tardiness_rule',
                    'view.night_shift','edit.night_shift',
                    'view.holiday','edit.holiday','delete.holiday','create.holiday',
                    'view.workflow','edit.workflow','delete.workflow','create.workflow',
                    'view.schedule','edit.schedule','delete.schedule','create.schedule',
                    'view.shift','edit.shift','delete.shift','create.shift',
                    'ess.view.request','ess.update.request','ess.create.request','ess.cancel.request',
                    'ess.approval.request'
                    
                    ) THEN 'T&A'
                 ELSE module_access                
             END;"
        );

        // update module column
        DB::statement(
            "UPDATE tasks 
             SET module = CASE
                 WHEN name IN (
                    'view.team','edit.team','delete.team','create.team',
                    'view.project','edit.project','delete.project','create.project'
                    ) THEN 'Organization Settings'
                 WHEN name IN (
                    'view.leave_type','edit.leave_type',
                    'delete.leave_type','create.leave_type'
                    ) THEN 'Leave Settings'
                 WHEN name IN (
                    'create.default_schedule','view.default_schedule','update.default_schedule',
                    'view.tardiness_rule','edit.tardiness_rule','delete.tardiness_rule','create.tardiness_rule',
                    'view.night_shift','edit.night_shift',
                    'view.holiday','edit.holiday','delete.holiday','create.holiday'
                    ) THEN 'Schedule Settings'
                 WHEN name IN (
                    'view.workflow','edit.workflow','delete.workflow','create.workflow'
                    ) THEN 'Automation Settings'
                 WHEN name IN ('view.schedule','edit.schedule','delete.schedule','create.schedule') THEN 'Schedules'
                 WHEN name IN ('view.shift','edit.shift','delete.shift','create.shift') THEN 'Shifts'
                 WHEN name IN (
                    'ess.view.request','ess.update.request','ess.create.request','ess.cancel.request',
                    'ess.approval.request'
                    ) THEN 'ESS'
                 ELSE module   
             END;"
        );

        // update submodule column
        DB::statement(
            "UPDATE tasks
             SET submodule = CASE 
                 WHEN name IN ('view.team','edit.team','delete.team','create.team') THEN 'Teams'
                 WHEN name IN ('view.project','edit.project','delete.project','create.project') THEN 'Projects'
                 WHEN name IN (
                    'view.leave_type','edit.leave_type','delete.leave_type','create.leave_type'
                    ) THEN 'Leave Types'
                 WHEN name IN (
                    'create.default_schedule','view.default_schedule','update.default_schedule'
                    ) THEN 'Default Schedules'
                 WHEN name IN (
                    'view.tardiness_rule','edit.tardiness_rule','delete.tardiness_rule','create.tardiness_rule'
                    ) THEN 'Tardiness Rules'
                 WHEN name IN ('view.night_shift','edit.night_shift') THEN 'Night Shift'
                 WHEN name IN ('view.holiday','edit.holiday','delete.holiday','create.holiday') THEN 'Holidays'
                 WHEN name IN ('view.workflow','edit.workflow','delete.workflow','create.workflow') THEN 'Workflows'
                 WHEN name IN ('view.schedule','edit.schedule','delete.schedule','create.schedule') THEN 'Schedules'
                 WHEN name IN ('view.shift','edit.shift','delete.shift','create.shift') THEN 'Assign Shift'
                 WHEN name IN (
                    'ess.view.request','ess.update.request','ess.create.request','ess.cancel.request'
                    ) THEN 'Requests'
                 WHEN name IN ('ess.approval.request') THEN 'Approvals'
                 ELSE submodule
             END;"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
