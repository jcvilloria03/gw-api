<?php
use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class AddCompanyDisbursementTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.disbursement',
            'display_name' => 'Create Disbursements',
            'description' => 'Create Disbursements',
            'module' => 'Payroll',
            'submodule' => 'Disbursements',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.disbursement',
            'display_name' => 'Edit Disbursements',
            'description' => 'Edit Disbursements',
            'module' => 'Payroll',
            'submodule' => 'Disbursements',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'view.disbursement',
            'display_name' => 'View Disbursements',
            'description' => 'View Disbursements',
            'module' => 'Payroll',
            'submodule' => 'Disbursements',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.disbursement',
            'display_name' => 'Delete Disbursements',
            'description' => 'Delete Disbursements',
            'module' => 'Payroll',
            'submodule' => 'Disbursements',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false,
        ],
    ];
}
