<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertCreateScorecardExport extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'create.scorecard.export',
            'display_name' => 'Create Scorecard Export',
            'description' => 'Create Scorecard Export',
            'module' => 'HRIS',
            'submodule' => 'Events',
            'allowed_scopes' => [
                'Account',
                'Company'
            ],
            'ess' => false
        ]
    ];
}
