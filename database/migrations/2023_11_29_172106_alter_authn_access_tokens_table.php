<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuthnAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authn_access_tokens', function (Blueprint $table) {
            $table->dropIndex('authn_access_tokens_access_token_index');
            $table->dropColumn('access_token');
        });

        Schema::table('authn_access_tokens', function (Blueprint $table) {
            $table->longText('access_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authn_access_tokens', function (Blueprint $table) {
            $table->string('access_token')->index()->change();
        });
    }
}
