<?php

use Database\Migrations\TasksMigrationsTrait;
use Illuminate\Database\Migrations\Migration;

class InsertEarningsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.earnings',
            'display_name' => 'View Earnings',
            'description' => 'View Earnings',
            'module' => 'Payroll',
            'submodule' => 'Earnings',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'edit.earnings',
            'display_name' => 'Edit Earnings',
            'description' => 'Edit Earnings',
            'module' => 'Payroll',
            'submodule' => 'Earnings',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.earnings',
            'display_name' => 'Create Earnings',
            'description' => 'Create Earnings',
            'module' => 'Payroll',
            'submodule' => 'Earnings',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
