<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertRoleTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.role',
            'display_name' => 'View Roles',
            'description' => 'View Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.role',
            'display_name' => 'Create Roles',
            'description' => 'Create Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.role',
            'display_name' => 'Delete Roles',
            'description' => 'Delete Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ],
        [
            'name' => 'update.role',
            'display_name' => 'Update Roles',
            'description' => 'Update Roles',
            'module' => 'HRIS',
            'submodule' => 'Role',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false,
        ]
    ];
}
