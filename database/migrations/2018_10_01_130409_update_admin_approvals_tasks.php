<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class UpdateAdminApprovalsTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'update.request',
            'display_name' => 'Update Request',
            'description' => 'Update Request',
            'module' => 'Time and Attendance',
            'submodule' => 'Request Approvals',
            'allowed_scopes' => [
                'Account',
                'Company',
            ],
            'ess' => false
        ]
    ];
}
