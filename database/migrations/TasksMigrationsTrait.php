<?php

namespace Database\Migrations;

use Illuminate\Support\Facades\DB;
use App\Model\Task;
use App\Model\Role;
use App\Model\UserRole;
use App\Model\Permission;

trait TasksMigrationsTrait
{
    // create task in Database
    public function createTask(array $attributes)
    {
        $taskId = DB::table('tasks')->insertGetId([
            'name' => $attributes['name'],
            'display_name' => $attributes['display_name'],
            'description' => $attributes['description'],
            'module' => $attributes['module'],
            'submodule' => $attributes['submodule'],
            'allowed_scopes' => json_encode($attributes['allowed_scopes']),
            'ess' => $attributes['ess']
        ]);
        return $taskId;
    }

    public function assignTaskToDefaultRoles(
        int $taskId,
        array $allowedScopes,
        bool $isEss
    ) {
        // assign tasks with Account scopes to Owners
        if (in_array('Account', $allowedScopes)) {
            // get existing owner roles
            $ownerRoles = $this->getOwnerRoles();
            foreach ($ownerRoles as $ownerRole) {
                $scope = ['Account' => [$ownerRole->account_id]];
                $this->assignTaskPermissionToRole($taskId, $ownerRole->id, $scope);
            }
        }

        // assign tasks with Company scopes to Super Admins and Company Admins
        if (in_array('Company', $allowedScopes)) {
            // get existing SuperAdmin roles and Company Admin roles
            $adminRoles = $this->getAdminRoles();
            foreach ($adminRoles as $adminRole) {
                if (
                    $adminRole->name === 'Super Admin' &&
                    $adminRole->company_id === 0
                ) {
                    // SuperAdmin role (make sure company name is not 'Super')
                    $scope = ['Company' => 'all'];
                } else {
                    // Company Admin role
                    $scope = ['Company' => [$adminRole->company_id]];
                }
                $this->assignTaskPermissionToRole($taskId, $adminRole->id, $scope);
            }
        }

        // assign tasks for ESS to default Employee roles
        if ($isEss) {
            // get existing Employee roles
            $employeeRoles = $this->getEmployeeRoles();
            foreach ($employeeRoles as $employeeRole) {
                $this->assignTaskPermissionToRole($taskId, $employeeRole->id, (object) null);
            }
        }
    }

    protected function assignTaskPermissionToRole(int $taskId, int $roleId, $scope)
    {
        DB::table('permissions')->updateOrInsert([
            'task_id' => $taskId,
            'role_id' => $roleId
        ], [
            'scope' => json_encode($scope),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }

    // get all owner roles
    protected function getOwnerRoles()
    {
        return DB::table('roles')
            ->select('id', 'name', 'account_id', 'company_id')
            ->where([
                ['name', '=', 'Owner'],
                ['custom_role', '=', false],
            ])
            ->get();
    }

    // get all Super Admin and Company Admin roles
    protected function getAdminRoles()
    {
        return DB::table('roles')
            ->select('id', 'name', 'account_id', 'company_id')
            ->where([
                ['name', 'like', '%Admin'],
                ['custom_role', '=', false],
            ])
            ->get();
    }

    // get all Employee roles
    protected function getEmployeeRoles()
    {
        return DB::table('roles')
            ->select('id', 'name', 'account_id', 'company_id')
            ->where([
                ['name', 'like', '%Employee'],
                ['custom_role', '=', false],
            ])
            ->get();
    }

    public function removeTask(array $task)
    {
        $task = $this->getTaskByNameAndModules(
            $task['name'],
            $task['module'],
            $task['submodule']
        );
        DB::table('permissions')->where('task_id', '=', $task->id)->delete();
        DB::table('tasks')->where('id', '=', $task->id)->delete();
    }

    // get Task by Name
    protected function getTaskByNameAndModules(string $name, string $module, string $submodule)
    {
        return DB::table('tasks')
            ->select('id', 'name')
            ->where([
                ['name', '=', $name],
                ['module', '=', $module],
                ['submodule', '=', $submodule],
            ])
            ->get()
            ->first();
    }

    private function destroyPermissions(string $taskName, array $allowedScopes = null)
    {
        $task = Task::where('name', $taskName)->first();

        if (!empty($allowedScopes)) {
            $task->allowed_scopes = ['Account'];
            $task->save();
        }

        if (!empty($task)) {
            // Get all super admin roles
            $roles = Role::where('name', Role::SUPERADMIN_NAME)
                ->get();

            // Get list of permission ids for task
            $permissionIds = [];
            $userIds = [];
            $roles->each(function ($role) use ($task, &$permissionIds, &$userIds) {
                $permissions = $role->permissions->where('task_id', $task->id);
                $permissions->each(function ($permission) use ($role, &$permissionIds, &$userIds) {
                    $permissionIds[] = $permission->id;

                    $userRoles = UserRole::where('role_id', $role->id)
                        ->select('user_id')
                        ->get();

                    if ($userRoles->isNotEmpty()) {
                        $userIds = array_merge($userIds, $userRoles->pluck('user_id')->all());
                    }
                });
            });

            //Remove permissions
            if (!empty($permissionIds)) {
                Permission::destroy($permissionIds);
            }

            if (!empty($userIds)) {
                $service = App::make(AuthorizationService::class);
                $service->invalidateUsersCachedPermissions($userIds);
            }
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tasks as $task) {
            $taskId = $this->createTask($task);
            $this->assignTaskToDefaultRoles(
                $taskId,
                $task['allowed_scopes'],
                $task['ess']
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tasks as $task) {
            $this->removeTask($task);
        }
    }
}
