<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RemovePayrollSystemAndUserDefineLoansTask extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tasks = DB::table('tasks')
        ->where('module', 'Loan')
        ->get();

        foreach ($tasks as $task) {
            $this->removeTask($task);
        }
    }

    public function removeTask($task)
    {
        DB::table('permissions')->where('task_id', '=', $task->id)->delete();
        DB::table('tasks')->where('id', '=', $task->id)->delete();
    }
}
