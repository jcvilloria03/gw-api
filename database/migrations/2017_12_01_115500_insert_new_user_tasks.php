<?php

use Illuminate\Database\Migrations\Migration;
use Database\Migrations\TasksMigrationsTrait;

class InsertNewUserTasks extends Migration
{
    use TasksMigrationsTrait;

    protected $tasks = [
        [
            'name' => 'view.user',
            'display_name' => 'View Users',
            'description' => 'View Users',
            'module' => 'HRIS',
            'submodule' => 'User',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'delete.user',
            'display_name' => 'Delete Users',
            'description' => 'Delete Users',
            'module' => 'HRIS',
            'submodule' => 'User',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],
        [
            'name' => 'create.user',
            'display_name' => 'Create Users',
            'description' => 'Create Users',
            'module' => 'HRIS',
            'submodule' => 'User',
            'allowed_scopes' => [
                'Account'
            ],
            'ess' => false,
        ],

    ];
}
