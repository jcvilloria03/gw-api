<?php

namespace App\Bonus;

use App\Common\CommonAuthorizationService;

class BonusAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.bonuses';
    public $createTask = 'create.bonuses';
    public $updateTask = 'edit.bonuses';
    public $deleteTask = 'delete.bonuses';
}
