<?php

namespace App\Bonus;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineBonusRequestService extends RequestService
{
    /**
     * Call endpoint to assign bonuses
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/bonus/bulk_create",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update bonus.
     *
     * @param int $id Bonus ID
     * @param array $data Request data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, array $data, AuthzDataScope $dataScope = null)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/bonus/{$id}",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        ), $dataScope);
    }
}
