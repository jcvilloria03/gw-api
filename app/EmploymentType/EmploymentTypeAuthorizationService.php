<?php

namespace App\EmploymentType;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class EmploymentTypeAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.employment_type';
    const CREATE_TASK = 'create.employment_type';
    const UPDATE_TASK = 'edit.employment_type';
    const DELETE_TASK = 'delete.employment_type';


    /**
     * @param \stdClass $employmentType
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $employmentType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($employmentType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $employmentType
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyEmploymentTypes(\stdClass $employmentType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($employmentType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $employmentType
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $employmentType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($employmentType, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $employmentType
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $employmentType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($employmentType, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::UPDATE_TASK);
    }

      /**
       * @param int $targetAccountId
       * @param int $userId
       * @return bool
       */
    public function authorizeDelete(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::DELETE_TASK);
    }

    /**
     * Authorize employment type related tasks
     *
     * @param \stdClass $employmentType
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $employmentType,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for employment type's account
            return $accountScope->inScope($employmentType->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as employment type's account
                return $employmentType->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($employmentType->company_id);
        }

        return false;
    }
}
