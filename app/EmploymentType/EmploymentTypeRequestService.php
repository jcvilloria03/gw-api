<?php

namespace App\EmploymentType;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EmploymentTypeRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get employment type info
     *
     * @param int $id EmploymentType ID
     * @return \Illuminate\Http\JsonResponse EmploymentType Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/employment_type/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create employment type
     *
     * @param array $data employment type information
     * @return \Illuminate\Http\JsonResponse Created EmploymentType
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/employment_type",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to create multiple employment types
     *
     * @param array $data employment types information
     * @return \Illuminate\Http\JsonResponse Created EmploymentTypes ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/employment_type/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if employment type name is available
     *
     * @param int $companyId Company ID
     * @param array $data employment type information
     * @return \Illuminate\Http\JsonResponse Availability of employment type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/employment_type/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all employment types within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company employment types
     */
    public function getCompanyEmploymentTypes(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/employment_types"
        );
        return $this->send($request);
    }

      /**
     * Call endpoint to update employment type for given id
     *
     * @param array $data employment type informations
     * @param int $id employment type Id
     * @return \Illuminate\Http\JsonResponse Updated employment type
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/employment_type/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

      /**
       * Call endpoint to delete multiple employment types
       *
       * @param array $data employment types to delete informations
       * @return \Illuminate\Http\JsonResponse Deleted employment type id's
       */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/employment_type/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
