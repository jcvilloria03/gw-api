<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use App\Audit\AuditService;

class AuditServiceProvider extends ServiceProvider
{
    /**
     * Register Audit Service.
     *
     * @return void
     */
    public function register()
    {
        $auditLogUri = getenv('AUDIT_LOG_URI');

        $this->app->bind(AuditService::class, function () use ($auditLogUri) {
            return new AuditService(new Client([
                'base_uri' => $auditLogUri
            ]));
        });
    }
}
