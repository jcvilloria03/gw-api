<?php

namespace App\Providers;

use App\AnnualEarning\AnnualEarningUploadTask;
use App\Earning\EarningUploadTask;
use App\Attendance\AttendanceUploadTask;
use App\Employee\EmployeeUploadTask;
use App\Employee\EmployeeUpdateUploadTask;
use App\LeaveCredit\LeaveCreditDownloadTask;
use App\Payroll\PayrollAttendanceUploadTask;
use App\Payroll\PayrollAllowanceUploadTask;
use App\Payroll\PayrollBonusUploadTask;
use App\Payroll\PayrollCommissionUploadTask;
use App\Payroll\PayrollDeductionUploadTask;
use App\PayrollGroup\PayrollGroupUploadTask;
use App\Schedule\ScheduleUploadTask;
use App\PayrollLoan\PayrollLoanUploadTask;
use App\PayrollLoan\PayrollLoanDownloadTask;
use App\OtherIncome\OtherIncomeUploadTask;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\LeaveCredit\LeaveCreditUploadTask;
use App\LeaveRequest\LeaveRequestUploadTask;
use App\WorkflowEntitlement\WorkflowEntitlementUploadTask;
use App\User\UserProfileUploadTask;
use App\Shift\ShiftUploadTask;
use App\LeaveRequest\LeaveRequestDownloadTask;
use App\Shift\AssignShiftsTask;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class UploadTaskServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AttendanceUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new AttendanceUploadTask($s3);
        });

        $this->app->bind(EmployeeUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new EmployeeUploadTask($s3);
        });

        $this->app->bind(EmployeeUpdateUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new EmployeeUpdateUploadTask($s3);
        });

        $this->app->bind(PayrollAttendanceUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollAttendanceUploadTask($s3);
        });

        $this->app->bind(PayrollAllowanceUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollAllowanceUploadTask($s3);
        });

        $this->app->bind(PayrollBonusUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollBonusUploadTask($s3);
        });

        $this->app->bind(PayrollCommissionUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollCommissionUploadTask($s3);
        });

        $this->app->bind(PayrollDeductionUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollDeductionUploadTask($s3);
        });

        $this->app->bind(ScheduleUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new ScheduleUploadTask($s3);
        });

        $this->app->bind(PayrollLoanUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollLoanUploadTask($s3);
        });

        $this->app->bind(PayrollGroupUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollGroupUploadTask($s3);
        });

        $this->app->bind(LeaveCreditUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new LeaveCreditUploadTask($s3);
        });

        $this->app->bind(LeaveCreditDownloadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new LeaveCreditDownloadTask($s3);
        });

        $this->app->bind(OtherIncomeUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new OtherIncomeUploadTask($s3);
        });

        $this->app->bind(LeaveRequestUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new LeaveRequestUploadTask($s3);
        });

        $this->app->bind(WorkflowEntitlementUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new WorkflowEntitlementUploadTask($s3);
        });

        $this->app->bind(AnnualEarningUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new AnnualEarningUploadTask($s3);
        });

        $this->app->bind(EarningUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new EarningUploadTask($s3);
        });

        $this->app->bind(UserProfileUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new UserProfileUploadTask($s3);
        });

        $this->app->bind(ShiftUploadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new ShiftUploadTask($s3);
        });

        $this->app->bind(LeaveRequestDownloadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new LeaveRequestDownloadTask($s3);
        });

        $this->app->bind(AssignShiftsTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new AssignShiftsTask($s3);
        });

        $this->app->bind(PayrollLoanDownloadTask::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollLoanDownloadTask($s3);
        });
    }
}
