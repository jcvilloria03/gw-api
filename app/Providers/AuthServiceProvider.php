<?php

namespace App\Providers;

use App\Role\UserRoleService;
use Illuminate\Support\ServiceProvider;
use Salarium\Cache\FragmentedRedisCache;
use App\Authorization\AuthorizationService;
use App\Permission\AuthorizationPermissions;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthorizationService::class, function () {
            return new AuthorizationService(
                $this->app->make(UserRoleService::class),
                $this->app->make(AuthorizationPermissions::class),
                $this->app->make(FragmentedRedisCache::class)
            );
        });
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
    }
}
