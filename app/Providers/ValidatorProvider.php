<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidatorProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('name', function ($attribute, $value, $parameters, $validator) {
            // accepts Alpha, space, hyphen, period, apostrophe
            return preg_match('/^[\pL\s\-\.\']+$/u', $value);
        });

        Validator::extend('absolute_number', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^[0-9]*$/', $value);
        });

        Validator::extend('number_format', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^\d{1,3}(,\d{3})*(\.\d+)?$/', $value) || is_numeric($value);
        });

        Validator::extend('mobile_number', function ($attribute, $value, $parameters, $validator) {
            // accepts pattern of (12)1234567890
            return preg_match('/^\(\d{2}[)]\d{10}$/', $value);
        });

        Validator::extend('military_time', function ($attribute, $value, $parameters, $validator) {
            // accepts pattern of 8:55 | 13:59 | 24:00
            return preg_match('/^(([01]?\d|2[0-3]):([0-5]\d))|(24:00)$/', $value);
        });

        Validator::extend('date_multi_format', function ($attribute, $value, $formats) {
            // iterate through all formats
            foreach ($formats as $format) {
                // parse date with current format
                $parsed = date_parse_from_format($format, $value);

                // if value matches given format return true=validation succeeded
                if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                    return true;
                }
            }

            // value did not match any of the provided formats, so return false=validation failed
            return false;
        });

        Validator::extend('yes_no', function ($attribute, $value, $parameters, $validator) {
            return in_array(strtolower($value), [
                'yes', 'no'
            ]);
        });

        Validator::extend('allowed_array_values', function ($attribute, $value, $parameters) {
            return empty(array_diff(explode(',', $value), $parameters));
        });

        Validator::extend('alphanum_hyphen_underscore', function ($attribute, $value, $parameters, $validator) {
            // accepts Alpha, numbers, hyphen, underscore
            return preg_match('/^[[:alnum:]\-\_]+$/u', $value);
        });

        Validator::extend('employee_id_validate', function ($attribute, $value, $parameters, $validator) {
            // accepts Alpha, numbers, hyphen, underscore
            return preg_match('/^[[:alnum:]\-\_]+$/u', $value);
        });

        Validator::extend('array_contains', function ($attribute, $value, $parameters) {
            if (!is_array($value)) {
                return false;
            }

            return in_array($parameters[0], $value);
        });
    }
}
