<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Storage\UploadService;
use App\Storage\PayrollUploadService;
use App\Storage\OtherIncomeUploadService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UploadServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UploadService::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new UploadService($s3);
        });

        $this->app->bind(PayrollUploadService::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new PayrollUploadService($s3);
        });

        $this->app->bind(OtherIncomeUploadService::class, function () {
            $s3 = App::make('aws')->createClient('s3');
            return new OtherIncomeUploadService($s3);
        });
    }
}
