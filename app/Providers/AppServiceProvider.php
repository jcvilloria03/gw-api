<?php

namespace App\Providers;

use App\Http\Middleware\LogRequestResponse;
use App\Http\Middleware\AuditTrailMiddleware;
use App\Tracker\Tracker;
use App\Routing\Adapter\Lumen as LumenAdapter;
use FastRoute\DataGenerator\GroupCountBased as GcbDataGenerator;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteParser\Std as StdRouteParser;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;
use BaoPham\DynamoDb\DynamoDbServiceProvider;
use App\Storage\PictureService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Tracker::class, function ($app) {
            $configs = config('tracker', []);

            return new Tracker($app->make(LoggerInterface::class), $configs);
        });

        $this->app->singleton(LogRequestResponse::class, function ($app) {
            return new LogRequestResponse($app->make(Tracker::class), $app->make(LoggerInterface::class));
        });

        $this->app->extend('api.router.adapter', function () {
            return new LumenAdapter($this->app, new StdRouteParser, new GcbDataGenerator, GroupCountBased::class);
        });

        $this->app->singleton(AuditTrailMiddleware::class, function ($app) {
            return new AuditTrailMiddleware();
        });

        $this->app->singleton(DynamoDbServiceProvider::class, function ($app) {
            return new DynamoDbServiceProvider();
        });

        $this->app->bind(PictureService::class, function () {
            return new PictureService(app()->make('aws')->createClient('s3'));
        });
    }
}
