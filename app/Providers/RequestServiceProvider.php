<?php

namespace App\Providers;

use App\Payroll\PayrollFinalPayRequestService;
use App\Request\RequestService;
use App\Tracker\Tracker;
use GuzzleHttp\Client;
use App\Tag\TagRequestService;
use App\Tax\TaxRequestService;
use App\Bank\BankRequestService;
use App\Rank\RankRequestService;
use App\Team\TeamRequestService;
use App\Jobs\JobsRequestService;
use App\User\UserRequestService;
use App\ESS\EssClockStateService;
use App\Authz\AuthzRequestService;
use App\Shift\ShiftRequestService;
use App\Ess\EssPayrollRequestService;
use App\ESS\EssPayslipRequestService;
use App\Account\AccountRequestService;
use App\BasePay\BasePayRequestService;
use App\Company\CompanyRequestService;
use App\Country\CountryRequestService;
use App\Earning\EarningRequestService;
use App\ESS\EssApprovalRequestService;
use App\Ess\EssEmployeeRequestService;
use App\Holiday\HolidayRequestService;
use App\Payroll\PayrollRequestService;
use App\Payslip\PayslipRequestService;
use App\Project\ProjectRequestService;
use App\RestDay\RestDayRequestService;
use App\View\FilterViewRequestService;
use Illuminate\Support\ServiceProvider;
use App\Approval\ApprovalRequestService;
use App\Employee\EmployeeRequestService;
use App\ESS\EssAttachmentRequestService;
use App\FinalPay\FinalPayRequestService;
use App\Location\LocationRequestService;
use App\Position\PositionRequestService;
use App\Schedule\ScheduleRequestService;
use App\View\EmployeeViewRequestService;
use App\Workflow\WorkflowRequestService;
use App\Salpay\SalpayAuthorizationService;
use App\ESS\EssAnnouncementRequestService;
use App\ESS\EssNotificationRequestService;
use App\LeaveType\LeaveTypeRequestService;
use App\Attendance\AttendanceRequestService;
use App\Bonus\PhilippineBonusRequestService;
use App\CostCenter\CostCenterRequestService;
use App\Department\DepartmentRequestService;
use App\NightShift\NightShiftRequestService;
use App\TimeRecord\TimeRecordRequestService;
use App\ESS\EssEmployeeRequestRequestService;
use App\ESS\EssOvertimeRequestRequestService;
use App\DayHourRate\DayHourRateRequestService;
use App\ESS\EssUndertimeRequestRequestService;
use App\HoursWorked\HoursWorkedRequestService;
use App\LeaveCredit\LeaveCreditRequestService;
use App\OtherIncome\OtherIncomeRequestService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\ProductSeat\ProductSeatRequestService;
use App\Announcement\AnnouncementRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Contribution\ContributionRequestService;
use App\ESS\EssShiftChangeRequestRequestService;
use App\ESS\EssTimeDisputeRequestRequestService;
use App\LeaveRequest\LeaveRequestRequestService;
use App\Notification\NotificationRequestService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\AnnualEarning\AnnualEarningRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\TardinessRule\TardinessRuleRequestService;
use App\AdminDashboard\AdminDashboardRequestService;
use App\AffectedEntity\AffectedEntityRequestService;
use App\Allowance\PhilippineAllowanceRequestService;
use App\AttendanceView\AttendanceViewRequestService;
use App\Authentication\AuthenticationRequestService;
use App\BonusType\PhilippineBonusTypeRequestService;
use App\Deduction\PhilippineDeductionRequestService;
use App\EmploymentType\EmploymentTypeRequestService;
use App\GovernmentForm\GovernmentFormRequestService;
use App\Http\Controllers\SalpayIntegrationController;
use App\Adjustment\PhilippineAdjustmentRequestService;
use App\Commission\PhilippineCommissionRequestService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\Location\TimeAttendanceLocationRequestService;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use App\PayrollLoan\PayrollLoanDownloadRequestService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use App\AffectedEmployee\AffectedEmployeeRequestService;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use App\Termination\TerminationInformationRequestService;
use App\PayrollGroup\PhilippinePayrollGroupRequestService;
use App\AllowanceType\PhilippineAllowanceTypeRequestService;
use App\BasicPayAdjustment\BasicPayAdjustmentRequestService;
use App\DeductionType\PhilippineDeductionTypeRequestService;
use App\CommissionType\PhilippineCommissionTypeRequestService;
use App\WorkflowEntitlement\WorkflowEntitlementRequestService;
use App\EmployeeRequestModule\EmployeeRequestModuleRequestService;
use App\Role\RoleRequestService;
use App\DeviceManagement\DeviceManagementRequestService;
use App\Http\Controllers\SubscriptionPaymentController;
use App\Http\Controllers\AuditTrailController;
use App\MaxClockOut\MaxClockOutRequestService;
use App\AccountDeletion\AccountDeletionRequestService;
use App\DemoCompany\DemoCompanyRequestService;
use App\Authn\AuthnRequestService;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 *
 */
class RequestServiceProvider extends ServiceProvider
{
    /**
     * Register any Request Services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->afterResolving(RequestService::class, function (RequestService $instance) {
            $instance->setTracker($this->app->make(Tracker::class));
        });

        // this is a controller that is simply a pass through so I registered
        // it here just so it belongs to "requests" code
        $this->app->bind(SalpayIntegrationController::class, function () {
            return new SalpayIntegrationController(
                new Client([
                    'base_uri' => getenv('SALPAY_INTEGRATION_API_URI')
                ])
            );
        });

        $this->app->bind(SubscriptionPaymentController::class, function () {
            return new SubscriptionPaymentController(
                new Client([
                    'base_uri' => getenv('SUBSCRIPTIONS_PAYMENT_API_URI')
                ])
            );
        });

        $this->app->bind(AuditTrailController::class, function () {
            return new AuditTrailController(
                new Client([
                    'base_uri' => getenv('AUDIT_TRAIL_API_URI')
                ])
            );
        });

        $payrollUri = getenv('PAYROLL_API_URI');

        $this->app->bind(AnnualEarningRequestService::class, function () use ($payrollUri) {
            return new AnnualEarningRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PayrollRequestService::class, function () use ($payrollUri) {
            return new PayrollRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PayrollFinalPayRequestService::class, function () use ($payrollUri) {
            return new PayrollFinalPayRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });

        $this->app->bind(PayslipRequestService::class, function () use ($payrollUri) {
            return new PayslipRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(GovernmentFormRequestService::class, function () use ($payrollUri) {
            return new GovernmentFormRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(EssPayrollRequestService::class, function () use ($payrollUri) {
            return new EssPayrollRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(EssPayslipRequestService::class, function () use ($payrollUri) {
            return new EssPayslipRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineBonusTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineBonusTypeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAllowanceTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineAllowanceTypeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineCommissionTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineCommissionTypeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineDeductionTypeRequestService::class, function () use ($payrollUri) {
            return new PhilippineDeductionTypeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(OtherIncomeTypeRequestService::class, function () use ($payrollUri) {
            return new OtherIncomeTypeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(OtherIncomeRequestService::class, function () use ($payrollUri) {
            return new OtherIncomeRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineBonusRequestService::class, function () use ($payrollUri) {
            return new PhilippineBonusRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineCommissionRequestService::class, function () use ($payrollUri) {
            return new PhilippineCommissionRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAllowanceRequestService::class, function () use ($payrollUri) {
            return new PhilippineAllowanceRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineDeductionRequestService::class, function () use ($payrollUri) {
            return new PhilippineDeductionRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(PhilippineAdjustmentRequestService::class, function () use ($payrollUri) {
            return new PhilippineAdjustmentRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });
        $this->app->bind(FinalPayRequestService::class, function () use ($payrollUri) {
            return new FinalPayRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });

        $this->app->bind(BankRequestService::class, function () use ($payrollUri) {
            return new BankRequestService(new Client([
                'base_uri' => $payrollUri
            ]));
        });

        $authenticationUri = getenv('AUTHENTICATION_API_URI');

        $this->app->bind(AuthenticationRequestService::class, function () use ($authenticationUri) {
            return new AuthenticationRequestService(new Client([
                'base_uri' => $authenticationUri
            ]));
        });

        $jobsManagementUri = getenv('JOBS_MANAGEMENT_API_URI');

        $this->app->bind(JobsRequestService::class, function () use ($jobsManagementUri) {
            return new JobsRequestService(new Client([
                'base_uri' => $jobsManagementUri
            ]));
        });

        $companyUri = getenv('COMPANY_API_URI');

        $this->app->bind(AccountRequestService::class, function () use ($companyUri) {
            return new AccountRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CountryRequestService::class, function () use ($companyUri) {
            return new CountryRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PhilippineCompanyRequestService::class, function () use ($companyUri) {
            return new PhilippineCompanyRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CompanyRequestService::class, function () use ($companyUri) {
            return new CompanyRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(UserRequestService::class, function () use ($companyUri) {
            return new UserRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(LocationRequestService::class, function () use ($companyUri) {
            return new LocationRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TimeAttendanceLocationRequestService::class, function () use ($companyUri) {
            return new TimeAttendanceLocationRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PayrollGroupRequestService::class, function () use ($companyUri) {
            return new PayrollGroupRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PhilippinePayrollGroupRequestService::class, function () use ($companyUri) {
            return new PhilippinePayrollGroupRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EmployeeRequestService::class, function () use ($companyUri) {
            return new EmployeeRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EssEmployeeRequestService::class, function () use ($companyUri) {
            return new EssEmployeeRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(CostCenterRequestService::class, function () use ($companyUri) {
            return new CostCenterRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(DepartmentRequestService::class, function () use ($companyUri) {
            return new DepartmentRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(PositionRequestService::class, function () use ($companyUri) {
            return new PositionRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(RankRequestService::class, function () use ($companyUri) {
            return new RankRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(EmploymentTypeRequestService::class, function () use ($companyUri) {
            return new EmploymentTypeRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TeamRequestService::class, function () use ($companyUri) {
            return new TeamRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(BasePayRequestService::class, function () use ($companyUri) {
            return new BasePayRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(TaxRequestService::class, function () use ($companyUri) {
            return new TaxRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });
        $this->app->bind(ContributionRequestService::class, function () use ($companyUri) {
            return new ContributionRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(AffectedEmployeeRequestService::class, function () use ($companyUri) {
            return new AffectedEmployeeRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(AffectedEntityRequestService::class, function () use ($companyUri) {
            return new AffectedEntityRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(BasicPayAdjustmentRequestService::class, function () use ($companyUri) {
            return new BasicPayAdjustmentRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(EarningRequestService::class, function () use ($companyUri) {
            return new EarningRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(ProductSeatRequestService::class, function () use ($companyUri) {
            return new ProductSeatRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(TerminationInformationRequestService::class, function () use ($companyUri) {
            return new TerminationInformationRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(RoleRequestService::class, function () use ($companyUri) {
            return new RoleRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(MaxClockOutRequestService::class, function () use ($companyUri) {
            return new MaxClockOutRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(AccountDeletionRequestService::class, function () use ($companyUri) {
            return new AccountDeletionRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->app->bind(DemoCompanyRequestService::class, function () use ($companyUri) {
            return new DemoCompanyRequestService(new Client([
                'base_uri' => $companyUri
            ]));
        });

        $this->registerTaApi();
        $this->registerNotificationsApi();
        $this->registerRequestsApi();
        $this->registerPayrollLoanApi();
        $this->registerTimeclock();
        $this->registerViewsApi();
        $this->registerAttendanceApi();
        $this->registerSubscriptionsApi();
        $this->registerAuthzApi();
        $this->registerBundyApi();
        $this->registerAuthnApi();
    }

    protected function registerAuthzApi()
    {
        $authzUri = getenv('AUTHZ_API_URL');

        $this->app->bind(AuthzRequestService::class, function () use ($authzUri) {
            return new AuthzRequestService(new Client([
                'base_uri' => $authzUri
            ]));
        });
    }

    protected function registerBundyApi()
    {
        $bundyUri = getenv('BY_API_URI');

        $this->app->bind(DeviceManagementRequestService::class, function () use ($bundyUri) {
            return new DeviceManagementRequestService(new Client([
                'base_uri' => $bundyUri
            ]));
        });
    }

    protected function registerSubscriptionsApi()
    {
        $subscriptionsUri = getenv('SUBSCRIPTIONS_API_URI');
        $this->app->bind(SubscriptionsRequestService::class, function () use ($subscriptionsUri) {
            return new SubscriptionsRequestService(new Client([
                'base_uri' => $subscriptionsUri
            ]));
        });
    }

    protected function registerAttendanceApi()
    {
        $attendanceUri = getenv('ATTENDANCE_API_URI');
        $this->app->bind(AttendanceRequestService::class, function () use ($attendanceUri) {
            return new AttendanceRequestService(new Client([
                'base_uri' => $attendanceUri
            ]));
        });
    }

    protected function registerViewsApi()
    {
        $viewsUri = getenv('VIEWS_API_URI');
        $this->app->bind(EmployeeViewRequestService::class, function () use ($viewsUri) {
            return new EmployeeViewRequestService(new Client([
                'base_uri' => $viewsUri
            ]));
        });
        $this->app->bind(FilterViewRequestService::class, function () use ($viewsUri) {
            return new FilterViewRequestService(new Client([
                'base_uri' => $viewsUri
            ]));
        });
        $this->app->bind(AttendanceViewRequestService::class, function () use ($viewsUri) {
            return new AttendanceViewRequestService(new Client([
                'base_uri' => $viewsUri
            ]));
        });
    }

    protected function registerTaApi()
    {
        $taUri = getenv('TA_API_URI');
        $this->app->bind(AdminDashboardRequestService::class, function () use ($taUri) {
            return new AdminDashboardRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(DefaultScheduleRequestService::class, function () use ($taUri) {
            return new DefaultScheduleRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ProjectRequestService::class, function () use ($taUri) {
            return new ProjectRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });

        $this->app->bind(DayHourRateRequestService::class, function () use ($taUri) {
            return new DayHourRateRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveTypeRequestService::class, function () use ($taUri) {
            return new LeaveTypeRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveEntitlementRequestService::class, function () use ($taUri) {
            return new LeaveEntitlementRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(HolidayRequestService::class, function () use ($taUri) {
            return new HolidayRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(TardinessRuleRequestService::class, function () use ($taUri) {
            return new TardinessRuleRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(WorkflowRequestService::class, function () use ($taUri) {
            return new WorkflowRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(NightShiftRequestService::class, function () use ($taUri) {
            return new NightShiftRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(EmployeeRequestModuleRequestService::class, function () use ($taUri) {
            return new EmployeeRequestModuleRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(WorkflowEntitlementRequestService::class, function () use ($taUri) {
            return new WorkflowEntitlementRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ScheduleRequestService::class, function () use ($taUri) {
            return new ScheduleRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(ShiftRequestService::class, function () use ($taUri) {
            return new ShiftRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(TagRequestService::class, function () use ($taUri) {
            return new TagRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(RestDayRequestService::class, function () use ($taUri) {
            return new RestDayRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(LeaveCreditRequestService::class, function () use ($taUri) {
            return new LeaveCreditRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
        $this->app->bind(HoursWorkedRequestService::class, function () use ($taUri) {
            return new HoursWorkedRequestService(new Client([
                'base_uri' => $taUri
            ]));
        });
    }

    protected function registerNotificationsApi()
    {
        $notificationsApi = getenv('NOTIFICATIONS_API_URI');
        $this->app->bind(EssNotificationRequestService::class, function () use ($notificationsApi) {
            return new EssNotificationRequestService(new Client([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(EssAnnouncementRequestService::class, function () use ($notificationsApi) {
            return new EssAnnouncementRequestService(new Client([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(AnnouncementRequestService::class, function () use ($notificationsApi) {
            return new AnnouncementRequestService(new Client([
                'base_uri' => $notificationsApi
            ]));
        });
        $this->app->bind(NotificationRequestService::class, function () use ($notificationsApi) {
            return new NotificationRequestService(new Client([
                'base_uri' => $notificationsApi
            ]));
        });
    }

    protected function registerRequestsApi()
    {
        $requestsApi = getenv('REQUESTS_API_URI');
        $this->app->bind(EssEmployeeRequestRequestService::class, function () use ($requestsApi) {
            return new EssEmployeeRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssOvertimeRequestRequestService::class, function () use ($requestsApi) {
            return new EssOvertimeRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssAttachmentRequestService::class, function () use ($requestsApi) {
            return new EssAttachmentRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssApprovalRequestService::class, function () use ($requestsApi) {
            return new EssApprovalRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(ApprovalRequestService::class, function () use ($requestsApi) {
            return new ApprovalRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssTimeDisputeRequestRequestService::class, function () use ($requestsApi) {
            return new EssTimeDisputeRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssShiftChangeRequestRequestService::class, function () use ($requestsApi) {
            return new EssShiftChangeRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(EssUndertimeRequestRequestService::class, function () use ($requestsApi) {
            return new EssUndertimeRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
        $this->app->bind(LeaveRequestRequestService::class, function () use ($requestsApi) {
            return new LeaveRequestRequestService(new Client([
                'base_uri' => $requestsApi
            ]));
        });
    }

    protected function registerPayrollLoanApi()
    {
        $payrollLoanApi = getenv('PAYROLL_LOAN_API_URI');

        $this->app->bind(PayrollLoanRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanRequestService(new Client([
                'base_uri' => $payrollLoanApi
            ]));
        });

        $this->app->bind(PayrollLoanTypeRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanTypeRequestService(new Client([
                'base_uri' => $payrollLoanApi
            ]));
        });

        $this->app->bind(PayrollLoanDownloadRequestService::class, function () use ($payrollLoanApi) {
            return new PayrollLoanDownloadRequestService(new Client([
                'base_uri' => $payrollLoanApi
            ]));
        });
    }

    protected function registerTimeclock()
    {
        $timeclockUrl = getenv('TIMECLOCK_URL');

        $this->app->bind(EssClockStateService::class, function () use ($timeclockUrl) {
            return new EssClockStateService(new Client([
                'base_uri' => $timeclockUrl
            ]));
        });

        $this->app->bind(TimeRecordRequestService::class, function () use ($timeclockUrl) {
            return new TimeRecordRequestService(new Client([
                'base_uri' => $timeclockUrl
            ]));
        });
    }

    protected function registerAuthnApi()
    {
        $baseUri = getenv('AUTHN_API_URI');
        $this->app->bind(AuthnRequestService::class, function () use ($baseUri) {
            return new AuthnRequestService(new Client([
                'base_uri' => $baseUri
            ]));
        });
    }

    public function boot()
    {
    }
}
