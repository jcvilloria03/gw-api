<?php

namespace App\Providers;

use App\Permission\PermissionService;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PermissionService::class, function () {
            return new PermissionService();
        });
    }
}
