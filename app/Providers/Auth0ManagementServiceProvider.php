<?php

namespace App\Providers;

use App\Auth0\Auth0ManagementService;
use Auth0\SDK\API\Authentication;
use Auth0\SDK\API\Management;
use Illuminate\Support\ServiceProvider;

class Auth0ManagementServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Auth0ManagementService::class, function () {
            $management = new Management(null, getenv('AUTH0_DOMAIN'));
            return new Auth0ManagementService($management);
        });
    }
}
