<?php

namespace App\Validator;

use App\Model\Role;
use App\Rule\RoleRule;

class RoleValidator extends Validator
{
    const TYPE_OWNER = 'owner';
    const TYPE_SUPER_ADMIN = 'super admin';
    const TYPE_ADMIN = 'admin';
    const TYPE_EMPLOYEE = 'employee';

    private $isUpdating = false;

    public function validate(array &$attributes)
    {
        $errors = [];
        $rule = new RoleRule($attributes, $this->isUpdating);

        $validator = $this->getValidator(
            $attributes,
            $rule,
            $rule->customAttributes
        );

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
        }

        return $errors;
    }

    public function setForUpdating()
    {
        $this->isUpdating = true;
    }

    /**
     * Validate role roles.
     *
     * @param array $attributes
     * @param int $accountId
     *
     * @return array
     */
    public function validateAssignedRoles(array $attributes, int $accountId)
    {
        $roles = Role::where('account_id', $accountId)->get();
        $errors = [];

        foreach ($attributes['assigned_companies'] as $id => $assignedCompany) {
            $this->areRoleIdsValidForCompany($assignedCompany, $id, $roles, $errors);
            $this->areEmployeeIdsRequired($accountId, $assignedCompany, $id, $errors);
        }

        $this->checkRoleTypeExistBasedOnUserType(
            $roles,
            $attributes['user_type'],
            $attributes['assigned_companies'],
            $errors
        );

        return $errors;
    }

    /**
     * Validate role id belongs to company roles.
     *
     * @param array $assignedCompany
     * @param $id
     * @param $roles
     * @param $errors
     */
    private function areRoleIdsValidForCompany(array $assignedCompany, $id, $roles, &$errors)
    {
        $companyId = $assignedCompany['company_id'];
        $companyRolesIds = $roles->whereIn('company_id', [$companyId, 0])->pluck('id')->toArray();

        $this->getErrors($assignedCompany['roles_ids'], $companyRolesIds, $id, "doesn't belongs to company", $errors);
    }

    /**
     * If role type is employee, employee_id is required.
     *
     * @param int $accountId
     * @param array $assignedCompany
     * @param int $id
     * @param array $errors
     */
    private function areEmployeeIdsRequired(int $accountId, array $assignedCompany, int $id, array &$errors)
    {
        $isAtLeastOneRoleOfEmployeeType = Role::where('account_id', $accountId)
            ->whereIn('company_id', [0, $assignedCompany['company_id']])
            ->whereIn('id', $assignedCompany['roles_ids'])
            ->where('type', Role::EMPLOYEE)
            ->exists();

        if ($isAtLeastOneRoleOfEmployeeType && empty($assignedCompany['employee_id'])) {
            $errors = array_merge(
                $errors,
                ['assigned_companies.' . $id . '.employee_id' =>  ["The employee id field is required."]]
            );
        }
    }

    /**
     * @param $compare
     * @param $against
     * @param $id
     * @param $msg
     * @param $errors
     */
    private function getErrors($compare, $against, $id, $msg, &$errors)
    {
        $invalidRoleIds = implode(', ', array_diff($compare, $against));

        if ($invalidRoleIds) {
            $errors = array_merge(
                $errors,
                ['assigned_companies.' . $id . '.roles_ids' =>  ["Role id(s) {$invalidRoleIds} {$msg}!"]]
            );
        }
    }

    /**
     * Check does has at least one employee or admin role type based on, user type.
     *
     * @param $roles
     * @param string $userType
     * @param array $assignedCompany
     * @param array $errors
     */
    private function checkRoleTypeExistBasedOnUserType($roles, string $userType, array $assignedCompany, array &$errors)
    {
        $allRoleIds = collect($assignedCompany)->pluck('roles_ids')->flatten(1);
        $adminRoles = $roles->whereIn('id', $allRoleIds)->where('type', Role::ADMIN);

        switch ($userType) {
            case self::TYPE_OWNER:
            case self::TYPE_SUPER_ADMIN:
                $assignedCompaniesCount = count($assignedCompany);
                $adminRolesCount = count($adminRoles);

                if ($adminRolesCount < $assignedCompaniesCount) {
                    $errors = array_merge(
                        $errors,
                        [
                            'assigned_companies' => [
                                "Users of type Owner and Super Admin need to have admin role for every company."
                            ]
                        ]
                    );
                }

                break;
            case self::TYPE_ADMIN:
                $hasAdminType = count($adminRoles) > 0;

                if (!$hasAdminType) {
                    $errors = array_merge(
                        $errors,
                        [
                            'assigned_companies' => [
                                "Users of type Owner, Super Admin and Admin need to have at least one admin role."
                            ]
                        ]
                    );
                }

                break;
            case self::TYPE_EMPLOYEE :
                $hasEmployeeType = $roles->whereIn('id', $allRoleIds)->where('type', Role::EMPLOYEE)->first();

                if (!$hasEmployeeType) {
                    $errors = array_merge(
                        $errors,
                        [
                            'assigned_companies' => [
                                "Users of type Employee need to have at least one employee role."
                            ]
                        ]
                    );
                }

                break;

            default:
                return;
        }
    }
}
