<?php

namespace App\Validator;

use App\Model\User;

class UserValidator extends Validator
{
    const TYPE_OWNER = 'owner';
    const TYPE_SUPER_ADMIN = 'super admin';
    const TYPE_ADMIN = 'admin';
    const TYPE_EMPLOYEE = 'employee';
    const TYPES = [
        self::TYPE_OWNER,
        self::TYPE_SUPER_ADMIN,
        self::TYPE_ADMIN,
        self::TYPE_EMPLOYEE
    ];

    public function validate(array &$attributes)
    {
    }

    /**
     * If User wants to create user type owner or super admin then he needs to have owner type role
     *
     * @param array $inputs
     * @param string $userType
     * @param boolean $isCompanyLevel
     *
     * @return array
     */
    public function validateUserType(array $inputs, string $userType, $isCompanyLevel = false)
    {
        $errors = [];
        // is selected user type out of scope
        $isOutOfScope = !in_array($inputs['user_type'], [self::TYPE_ADMIN, self::TYPE_EMPLOYEE]);
        $isDisallowedUserType = false;
        if ($isCompanyLevel) {
            $isDisallowedUserType = $isOutOfScope &&
                in_array($userType, [self::TYPE_OWNER, self::TYPE_SUPER_ADMIN, self::TYPE_ADMIN]);
        } else {
            $isDisallowedUserType = $isOutOfScope && in_array($userType, [self::TYPE_SUPER_ADMIN, self::TYPE_ADMIN]);
        }
        $isDisallowedEmployee = $userType == self::TYPE_EMPLOYEE && $inputs['user_type'] != self::TYPE_EMPLOYEE;

        if ($isDisallowedUserType || $isDisallowedEmployee) {
            $errors = ['user_type' => 'Given user is unable to create selected user type.'];
        }

        return $errors;
    }
}
