<?php

namespace App\Validator;

use App\Rule\ResetPasswordRule;
use Illuminate\Support\Facades\App;

class ResetPasswordValidator extends Validator
{
    /**
     * Validate that input for changing password is correct.
     *
     * @param array &$attributes
     * @return array
     */
    public function validate(array &$attributes)
    {
        $validator = $this->getValidator($attributes, App::make(ResetPasswordRule::class));

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        return [];
    }
}
