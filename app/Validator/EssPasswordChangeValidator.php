<?php

namespace App\Validator;

use App\Rule\EssChangePasswordRule;

class EssPasswordChangeValidator extends Validator
{
    /**
     * Validate that input for changing password is correct.
     *
     * @param array &$attributes
     * @return array
     */
    public function validate(array &$attributes)
    {
        $validator = $this->getValidator($attributes, new EssChangePasswordRule());

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        return [];
    }
}
