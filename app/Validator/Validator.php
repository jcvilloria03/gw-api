<?php

namespace App\Validator;

use App\Rule\Rule;

abstract class Validator
{
    abstract public function validate(array &$attributeCollection);

    public function getValidator(array $attributes, Rule $rule, array $customAttributes = [])
    {
        return app('validator')->make(
            $attributes,
            $rule->getRules(),
            $rule->getMessages(),
            $customAttributes
        );
    }
}
