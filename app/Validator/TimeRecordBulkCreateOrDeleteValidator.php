<?php

namespace App\Validator;

use App\Rule\TimeRecordBulkCreateOrDeleteRule;
use App\Employee\EmployeeRequestService;

class TimeRecordBulkCreateOrDeleteValidator extends Validator
{
    /**
     * @var \App\Employee\EmployeeRequestService
     */
    private $employeeRequestService;

    /**
     * Time Record Bulk Create Or Delete Validator constructor
     *
     * @param EmployeeRequestService $employeeRequestService
     */
    public function __construct(
        EmployeeRequestService $employeeRequestService
    ) {
        $this->employeeRequestService = $employeeRequestService;
    }

    /**
     * Validate that input for bulk
     *
     * @param array &$attributes
     * @return array
     */
    public function validate(array &$attributes)
    {
        $rule = new TimeRecordBulkCreateOrDeleteRule();

        $validator = $this->getValidator(
            $attributes,
            $rule,
            $rule->customAttributes
        );

        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        return $this->validateEmployeesExists($attributes);
    }

    /**
     * Validate employees exists
     *
     * @param array $attributes
     * @return array $errors
     */
    protected function validateEmployeesExists(array $attributes)
    {
        $timeRecords = array_merge($attributes['create'] ?? [], $attributes['delete'] ?? []);
        $inputEmployeeIds = collect($timeRecords)->pluck('employee_id')
            ->unique()->all();
        $existingEmployeeIds = $this->getCompanyTaEmployees($attributes['company_id'])
            ->pluck('id')->all();
        $notExistingEmployeeIds = (collect($inputEmployeeIds)->diff($existingEmployeeIds))->sort()->all();

        return !empty($notExistingEmployeeIds)
            ? ['Employees with IDs [' . implode(', ', $notExistingEmployeeIds) . '] don\'t exist']
            : [];
    }

    /**
     * Get company employees using company ID
     *
     * @param int|string $companyId
     * @return \Illuminate\Support\Collection
     */
    public function getCompanyTaEmployees(int $companyId, array $query = [])
    {
        try {
            $response = $this->employeeRequestService->getCompanyTaEmployees($companyId, $query);
            $responseData = json_decode($response->getData(), true);
            $employees = $responseData['data'];
        } catch (HttpException $e) {
            $employees = [];
            Log::error($e->getMessage());
        }

        return collect($employees);
    }
}
