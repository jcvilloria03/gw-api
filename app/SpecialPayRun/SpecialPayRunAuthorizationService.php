<?php

namespace App\SpecialPayRun;

use App\Commission\CommissionAuthorizationService;

class SpecialPayRunAuthorizationService  extends CommissionAuthorizationService
{
    public $viewTask = 'view.special_pay_run';
    public $createTask = 'create.special_pay_run';
    public $updateTask = 'edit.special_pay_run';
    public $closeTask = 'close.special_pay_run';
    public $deleteTask = 'delete.special_pay_run';
    public $runTask = 'run.special_pay_run';
    public $sendTask = 'send.special_pay_run';
}
