<?php

namespace App\SubscriptionPayment;

use App\Common\CommonAuthorizationService;

class SubscriptionPaymentAuthorizationService extends CommonAuthorizationService
{
    public $viewTask   = 'view.subscriptions';
    public $createTask = 'create.subscriptions';
    public $updateTask = 'edit.subscriptions';
    public $deleteTask = 'delete.subscriptions';
}
