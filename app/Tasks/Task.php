<?php

namespace App\Tasks;

use Illuminate\Support\Facades\Redis;

abstract class Task
{
    const ERRORS_KEY = "errors:set";

    /**
     * Task Id
     *
     * @var string
     */
    protected $id;

    /**
     * Current values of Redis hash
     *
     * @var array
     */
    protected $currentValues;

    /**
     * Get Task id
     *
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    abstract protected function generateId();

    /**
     * Create task in Redis
     *
     */
    protected function createInRedis()
    {
        return Redis::hMSet($this->id, $this->currentValues);
    }

    /**
     * Fetch task in Redis
     *
     * @param array $fields Selected fields to fetch
     *
     */
    public function fetch(array $fields = null)
    {
        if (!empty($fields)) {
            return Redis::hMGet($this->id, $fields);
        }
        return Redis::hGetAll($this->id);
    }

    /**
     * Put expiry
     *
     * @param $key
     * @param int $ttl seconds
     *
     */
    public function expireInRedis($key, int $ttl = 60)
    {
        if (!empty($key)) {
            return Redis::expire($this->id, $ttl);
        }
    }

    /**
     * Helper function to set keys in Redis Hash
     *
     * @param string $key
     * @param mixed $val
     *
     */
    protected function setVal($key, $val)
    {
        $this->currentValues[$key] = $val;
        Redis::hSet($this->id, $key, $val);
    }

    /**
     * Set user id of the person who initiated the task
     *
     * @param int $userId
     *
     */
    public function setUserId($userId)
    {
        $this->setVal('user_id', $userId);
    }

    /**
     * Fetch user id of the person who initiated the task
     *
     * @return int $userId
     *
     */
    public function getUserId()
    {
        return Redis::hGet($this->id, 'user_id');
    }

    /**
     * Fetch the list of errors based from the passed job id
     *
     */
    public function getErrors()
    {
        $setKeys = Redis::sMembers($this->id . ":" . self::ERRORS_KEY);
        $errors = [];

        foreach ($setKeys as $key) {
            $error = Redis::hGetAll($key);

            if (!empty($error)) {
                $decoded = array_map(function ($item) {
                    return json_decode($item, true);
                }, $error);
                $errors[] = $decoded;
            }
        }
        
        return count($errors) == 1 ? $errors[0] : $errors;
    }
}
