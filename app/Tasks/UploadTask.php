<?php

namespace App\Tasks;

use Aws\S3\S3Client;
use Illuminate\Support\Facades\Redis;
/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class UploadTask extends Task
{
    const STATUS_VALIDATION_QUEUED = 'validation_queued';
    const STATUS_VALIDATING = 'validating';
    const STATUS_VALIDATED = 'validated';
    const STATUS_VALIDATION_FAILED = 'validation_failed';
    const STATUS_SAVE_QUEUED = 'save_queued';
    const STATUS_SAVING = 'saving';
    const STATUS_SAVED = 'saved';
    const STATUS_SAVE_FAILED = 'save_failed';

    const VALIDATION_STATUSES = [
        self::STATUS_VALIDATION_QUEUED,
        self::STATUS_VALIDATING,
        self::STATUS_VALIDATED,
        self::STATUS_VALIDATION_FAILED
    ];

    const SAVE_STATUSES = [
        self::STATUS_SAVE_QUEUED,
        self::STATUS_SAVING,
        self::STATUS_SAVED,
        self::STATUS_SAVE_FAILED
    ];

    /**
     * S3 client
     *
     * @var Aws\S3\S3Client
     */
    protected $s3Client;

    /**
     * Constructor
     *
     * @param Aws\S3\S3Client $s3Client
     */
    public function __construct(S3Client $s3Client)
    {
        $this->s3Client = $s3Client;
        $this->s3Bucket = env('UPLOADS_BUCKET');
    }

    /**
     * Get S3 Bucket
     *
     */
    public function getS3Bucket()
    {
        return $this->s3Bucket;
    }

    /**
     * Save File to S3
     *
     * @param string $s3Key S3 Key of file after upload
     * @param string $path Full path of file to upload
     *
     */
    protected function saveFileToS3(string $s3Key, string $path)
    {
        $this->s3Client->putObject([
            'Bucket' => $this->s3Bucket,
            'Key' => $s3Key,
            'SourceFile' => $path
        ]);
    }

    /**
     * Fetch Error File From S3
     *
     * @param string $s3Key S3 Key of error file
     *
     */
    public function fetchErrorFileFromS3(string $s3Key)
    {
        $result = $this->s3Client->getObject([
            'Bucket' => $this->s3Bucket,
            'Key' => $s3Key
        ]);
        if (empty($result) || empty($result['Body'])) {
            return null;
        }
        return json_decode($result['Body']->getContents(), true);
    }

    /**
     * Check if Validation Status can be updated
     *
     * @param string $newStatus
     * @param string $currentStatus
     * @return boolean
     *
     */
    protected function canChangeValidationStatus(string $currentStatus = null, string $newStatus)
    {
        $changeStatus = false;
        switch ($newStatus) {
            case self::STATUS_VALIDATION_QUEUED:
                $changeStatus = $this->canUpdateStatusToQueued($currentStatus);
                break;
            case self::STATUS_VALIDATING:
                $changeStatus = $this->canUpdateStatusToValidating($currentStatus);
                break;
            case self::STATUS_VALIDATED:
            case self::STATUS_VALIDATION_FAILED:
                $changeStatus = $this->canUpdateStatusToFinished($currentStatus);
                break;
        }
        return $changeStatus;
    }

    /**
     * Check if Save Status can be updated
     *
     * @param string $newStatus
     * @param string $currentStatus
     * @return boolean
     *
     */
    protected function canChangeSaveStatus(string $currentStatus = null, string $newStatus)
    {
        $changeStatus = false;
        switch ($newStatus) {
            case self::STATUS_SAVE_QUEUED:
                $changeStatus = $this->canUpdateStatusToQueued($currentStatus);
                break;
            case self::STATUS_SAVING:
                $changeStatus = $this->canUpdateStatusToValidating($currentStatus);
                break;
            case self::STATUS_SAVED:
            case self::STATUS_SAVE_FAILED:
                $changeStatus = $this->canUpdateStatusToFinished($currentStatus);
                break;
        }
        return $changeStatus;
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return (
            !in_array(
                $currentStatus,
                [
                    self::STATUS_VALIDATING,
                    self::STATUS_SAVING,
                ]
            )
        );
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATION_QUEUED || $currentStatus === self::STATUS_SAVE_QUEUED;
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATING || $currentStatus === self::STATUS_SAVING;
    }
}
