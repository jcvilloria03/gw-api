<?php

namespace App\Notification;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class EmployeeNotificationAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.notification';

    /**
     * @param \stdClass $authParams
     * @param array     $user
     * @return bool
     */
    public function authorizeView(\stdClass $authParams, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($authParams, $user, self::VIEW_TASK);
    }

    /**
     * Authorize user notificatons related tasks
     *
     * @param \stdClass $authParams
     * @param array     $user
     * @param string    $taskType
     *
     * @return bool
     */
    private function authorizeTask(
        \stdClass $authParams,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($authParams->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $authParams->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($authParams->company_id);
        }

        return false;
    }
}
