<?php

namespace App\Notification;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class NotificationRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Send email to notify user upon successful password change.
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetPasswordNotification($email, $token)
    {
        $request = new Request(
            'POST',
            "/notification/reset_password",
            ['Content-Type' => 'application/json'],
            json_encode([
                'email' => $email,
                'token' => $token
            ])
        );
        return $this->send($request);
    }

    public function sendAccountInactiveNotification($email)
    {
        $request = new Request(
            'POST',
            "/notification/account_inactive",
            ['Content-Type' => 'application/json'],
            json_encode([
                'email' => $email
            ])
        );
        return $this->send($request);
    }

    /**
     * Send the OTP.
     * @param string $email
     * @param int $otp
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendOtpEmailNotificationToUser($email, $otp)
    {
        $request = new Request(
            'POST',
            "/notification/otp",
            ['Content-Type' => 'application/json'],
            json_encode([
                'email' => $email,
                'otp' => $otp
            ])
        );
        return $this->send($request);
    }
}
