<?php

namespace App\Notification;

use App\Authorization\EssBaseAuthorizationService;

class EssNotificationAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.notification';

    /**
     * @param int $userId User ID
     * @return bool
     */
    public function authorizeView(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
