<?php

namespace App\ClockState;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Authorization\EssBaseAuthorizationService;
use \BaoPham\DynamoDb\Facades\DynamoDb;
use Illuminate\Support\Facades\Log;
use App\Transformer\TimeSheetTransformer;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection;
/**
 * @SuppressWarnings(PHPMD.UnusedPrivateField)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class ClockStateService extends EssBaseAuthorizationService
{
    /**
     * @var \App\Transformer\TimeSheetTransformer
     */
    private $transformer;

    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function __construct(
        TimeSheetTransformer $transformer,
        FractalManager $fractal
    ) {
        $this->transformer = $transformer;
        $this->fractal = $fractal;
    }

    /**
     * @param int $employeeId
     */
    public function lastTimeclock(int $employeeId)
    {
        try {
            $result = DynamoDb::table(env('TIMECLOCK_TABLE_NAME', 'timeclock'))
                ->setKeyConditionExpression('#employee_uid = :employee_uid')
                ->setProjectionExpression('employee_uid, #t, company_uid, max_clock_out, #s, tags')
                ->setExpressionAttributeName('#employee_uid', 'employee_uid', 'max_clock_out')
                ->setExpressionAttributeName('#t', 'timestamp')
                ->setExpressionAttributeName('#s', 'state')
                ->setExpressionAttributeValue(':employee_uid', DynamoDb::marshalValue($employeeId))
                ->setLimit(1)
                ->setScanIndexForward(false)
                ->prepare()
                ->query();
        } catch (\Throwable $e) {
            Log::error('DYNAMODB QUERY ERROR: ' . $e->getMessage());
            Log::error($e->getTraceAsString());
            throw new HttpException(500, $e->getMessage());
        }

        return $result;
    }

    public function getFormattedLastTimeClockEntry(int $employeeId)
    {
        $lastEntry = [];

        try {
            $lastTimeclock = $this->lastTimeclock($employeeId);

            if ($lastTimeclock && isset($lastTimeclock['Items']) && !empty($lastTimeclock['Items'])) {
                $lastEntry = DynamoDB::unmarshalItem($lastTimeclock['Items'][0]);

                $lastEntry['tags'] = !empty($lastEntry['tags']) ? $lastEntry['tags'] : [];
                $lastEntry['max_clock_out'] = !empty($lastEntry['max_clock_out'])
                        ? $lastEntry['max_clock_out']
                        : false;
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return $lastEntry;
        }

        return $lastEntry;
    }

    public function getFormattedTimesheet(int $employeeId, $start, $end)
    {
        $timesheets = [];

        $start = implode(", ", $start);
        $end = implode(", ", $end);
        $newStartDate =  date('Y-m-d H:i:s', strtotime($start . ' -1 day'));
        $newEndDate =  date('Y-m-d H:i:s', strtotime($end . ' +1 day'));

        $startDate = strtotime($newStartDate);
        $endDate = strtotime($newEndDate);

        try {
            $timeclock = $this->getTimesheet($employeeId, $startDate, $endDate);

            if ($timeclock && isset($timeclock['Items']) && !empty($timeclock['Items'])) {
                $timesheetData = $timeclock['Items'];
                $collection = new Collection($timesheetData, $this->transformer);
                $timesheets = $this->fractal->createData($collection)->toArray();
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            return $timesheets;
        }

        return $timesheets;
    }

    public function getTimesheet(int $employeeId, $startDate, $endDate)
    {
        try {
            $result = DynamoDb::table(env('TIMECLOCK_TABLE_NAME', 'timeclock'))
            ->setKeyConditionExpression('#employee_uid = :employee_uid AND #t BETWEEN :start AND :end')
            ->setProjectionExpression('employee_uid, #t, company_uid, max_clock_out, #s, tags')
            ->setExpressionAttributeName('#employee_uid', 'employee_uid', 'max_clock_out')
            ->setExpressionAttributeName('#t', 'timestamp')
            ->setExpressionAttributeName('#s', 'state')
            ->setExpressionAttributeValue(':employee_uid', DynamoDb::marshalValue($employeeId))
            ->setExpressionAttributeValue(':start', DynamoDb::marshalValue($startDate))
            ->setExpressionAttributeValue(':end', DynamoDb::marshalValue($endDate))
            ->setScanIndexForward(false)
            ->prepare()
            ->query();
        } catch (\Throwable $e) {
            Log::error('DYNAMODB QUERY ERROR: ' . $e->getMessage());
            Log::error($e->getTraceAsString());
            throw new HttpException(500, $e->getMessage());
        }

        return $result;
    }

}
