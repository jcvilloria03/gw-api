<?php

namespace App\ClockState;

use App\Role\UserRoleService;
use App\Employee\EmployeeRequestService;
use Salarium\Cache\FragmentedRedisCache;
use App\Permission\AuthorizationPermissions;
use App\Authorization\EssBaseAuthorizationService;
use App\Location\TimeAttendanceLocationRequestService;
use App\Shift\ShiftRequestService;
use Carbon\Carbon;

/**
 * @SuppressWarnings(PHPMD.UnusedPrivateField)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class EssClockStateAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TIME_RECORDS_TASK = 'ess.clock.view_time_records';
    const LOG_TASK = 'ess.clock.log';

    /**
     * App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * App\Permission\AuthorizationPermissions
     */
    private $permissions;

    /**
     * App\Employee\EmployeeRequestService
     */
    private $employeeRequestService;

    /**
     * App\Location\TimeAttendanceLocationRequestService
     */
    private $timeAttendanceLocationService;

    /**
     * App\Shift\ShiftRequestService
     */
    private $shiftRequestService;

    public function __construct(
        UserRoleService $userRoleService,
        AuthorizationPermissions $permissions,
        FragmentedRedisCache $cacheService,
        EmployeeRequestService $employeeRequestService,
        TimeAttendanceLocationRequestService $timeAttendanceLocationService,
        ShiftRequestService $shiftRequestService
    ) {
        $this->employeeRequestService = $employeeRequestService;
        $this->timeAttendanceLocationService = $timeAttendanceLocationService;
        $this->shiftRequestService = $shiftRequestService;
        parent::__construct(
            $userRoleService,
            $permissions,
            $cacheService
        );
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeViewTimeRecords(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TIME_RECORDS_TASK);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeLog(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::LOG_TASK);
    }


    /**
     * @description This method provides one of the essential data
     *              needed to prevent an employee from clocking-in
     *              and clocking-out from a location that's not
     *              authorized by the company. The location id,
     *              primary location id and secondary location id
     *              are retrieved from this method thus, the return
     *              of this method are the three properties.
     * @private
     * @param int $employeeId
     * @return array
     */
    private function getEmployeeInfo(int $employeeId)
    {
        $locationObject = array(
            "thirdLocation" => null,
            "secondaryLocation" => null,
            "primaryLocation" => null
        );

        $rawInfo = $this->employeeRequestService->getEmployee($employeeId);
        $info = json_decode($rawInfo->getData(), true);

        // Fill-in the $locationObject with its corresponding key identification
        // thirdLocation - location_id
        // secondaryLocation - time_attendance.secondary_location
        // primaryLocation - time_attendance.primary_location
        if ($info && isset($info["location_id"])) {
            if ($locationObject["thirdLocation"]) {
                $locationObject["thirdLocation"] = $info["location_id"];
            }
        }

        if ($info && isset($info["time_attendance"])) {
            $taDetails = $info["time_attendance"];

            if ($taDetails["primary_location_id"]) {
                $locationObject["primaryLocation"] =  $taDetails["primary_location_id"];
            }

            if ($taDetails["secondary_location_id"]) {
                $locationObject["secondaryLocation"] = $taDetails["secondary_location_id"];
            }
        }

        return $locationObject;
    }


    /**
     * @description This method provides one of the essential data.
     *              provides the list of IP addresses that's white listed
     *              to this employee.
     *
     * @private
     * @param int $locationId
     * @return array
     */
    private function getAllowedLocations(int $locationId)
    {
        $ipListRaw = array();
        $ipList = array();

        $rawInfo = $this->timeAttendanceLocationService->get($locationId);
        $info = json_decode($rawInfo->getData(), true);

        // If there's no location data, accept all requests
        // regardless of the IP.
        if (!$info || !isset($info)) {
            return $ipList;
        } elseif (isset($info["ip_addresses"])) {
            $ipListRaw = $info["ip_addresses"];
        }

        // Return only ip addresses. We don't need any info from attendance
        // location service.
        foreach ($ipListRaw as $value) {
            array_push($ipList, $value["ip_address"]);
        }

        return $ipList;
    }


    /**
     * @description This method is considered as a util method that creates regular expression
     *              from an IP addresses that contains a valid wildcard. Any passed
     *              IP addresses while be compiled  for regex matching.
     *
     *              Supported wildcards:
     *              1. * - Ex. 192.168.*.1
     *
     * @private
     * @param int $whiteIp
     * @param int $ipAddr
     * @return bool
     */
    private function isIPInRange(string $whiteIp, string $ipAddr)
    {
        $rawPattern = "([0-9]{0,3})";
        $pattern = "/".str_replace(
            "*",
            $rawPattern,
            $whiteIp
        )."/";

        return preg_match($pattern, $ipAddr);
    }


    /**
     * @description This method performs the necessary step to determine if
     *              a requested IP address is valid.
     *
     *              Steps:
     *              1. Get employee info for location ids.
     *              2. Get list of ip addresses
     *              3. validate requested ip if included in the list of whitelisted ips.
     *
     * @public
     * @param int $employeeId
     * @param int $ipAddr
     * @return array
     */
    public function isIPAllowed(int $employeeId, string $ipAddr)
    {
        $whitelist = array();
        $ipRange = array();

        $employeeInfo = $this->getEmployeeInfo($employeeId);

        // Primary location id
        $primaryLoc = $employeeInfo["primaryLocation"];

        // Third location id
        $thirdLoc = $employeeInfo["thirdLocation"];
        /**
          * Disabling this for now as a use case discrepancy
          * - Secondary location will now be disabled.
          *  Checking will be done on primary location only.
          *
        // Secondary location id
        // $secondaryLoc = $employeeInfo["secondaryLocation"];
        if ($primaryLoc && !$secondaryLoc) {
            $whitelist = $this->getAllowedLocations($primaryLoc);
        } elseif ($secondaryLoc && !$primaryLoc) {
            $whitelist = $this->getAllowedLocations($secondaryLoc);
        } elseif ($secondaryLoc && $primaryLoc) {
            // Both secondary and primary locations are present.
            // Prioritize primary location over secondary location on retreiving ip addresses
            $whitelistPrimary = $this->getAllowedLocations($primaryLoc);
            if (!count($whitelistPrimary)) {
                $whitelist = $this->getAllowedLocations($secondaryLoc);
            } else {
                // If both primary and secondary are not empty, merge the two
                // arrays to get the full list of IP addresses
                $whitelistSec = $this->getAllowedLocations($secondaryLoc);
                $whitelist = array_merge($whitelistPrimary, $whitelistSec);
            }
        } elseif ($thirdLoc) {
            $whitelist = $this->getAllowedLocations($primaryLoc);
        } else {
            // Should not happen or data is broken
            return false;
        }
        */
        if ($primaryLoc) {
            $whitelist = $this->getAllowedLocations($primaryLoc);
        } elseif ($thirdLoc) {
            // This is the location_id in user profile
            $whitelist = $this->getAllowedLocations($thirdLoc);
        }

        // At this point there's no configured ip white listing
        if (!count($whitelist)) {
            return true;
        }

        // Check if ip address is in array
        if (in_array($ipAddr, $whitelist)) {
            return true;
        }

        // Last line of checking is determining if the
        // white list has wild cards. ex, 192.*.168.0.1
        if ($whitelist) {
            foreach ($whitelist as $value) {
                if ($this->isIPInRange($value, $ipAddr)) {
                    array_push($ipRange, $value);
                }
            }
        }

        // 0 is not valid otherwise, valid.
        return (boolean)count($ipRange);
    }

    /**
     * Validate time methods for employee shift
     *
     * @param array $user
     * @param array $attributes
     * @return boolean
     */
    public function validateTimeMethods($user, $attributes)
    {
        $allow = true;
        // Get employee current date shift
        $shiftDate = Carbon::parse($attributes['timestamp'] ?? null)->format('Y-m-d');
        $queryData = [
            'company_id' => $user['employee_company_id'],
            'start_date' => $shiftDate,
            'end_date' => $shiftDate
        ];
        $employeeShiftsResponse = $this->shiftRequestService->getEmployeeShifts(
            $user['employee_id'],
            $queryData
        );
        $employeeShifts = json_decode($employeeShiftsResponse->getData(), true)['data'] ?? [];
        foreach ($employeeShifts as $shift) {
            if ($shift) {
                $shiftAllowedTimeMethods = $shift['schedule']['allowed_time_methods'] ?? [];
                $allow = in_array($attributes['origin'], $shiftAllowedTimeMethods);
                if ($allow == true) {
                    break;
                }
            }
        }
        return $allow;
    }

}
