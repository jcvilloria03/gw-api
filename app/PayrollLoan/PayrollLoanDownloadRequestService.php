<?php

namespace App\PayrollLoan;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

class PayrollLoanDownloadRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to download attachments
     *
     * @param int $id Employe Request ID
     * @return $reponse
     */
    public function download($id, $data)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/payroll_loans/generate_csv",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        try {
            $psrResponse = $this->client->send($request);
            $httpFoundationFactory = new HttpFoundationFactory();
            $response = $httpFoundationFactory->createResponse($psrResponse);

            return $response;
        } catch (BadResponseException $e) {
            // we catch 400 and 500 errors from the micro-service, then return as HttpExceptions
            $contents = json_decode($e->getResponse()->getBody()->getContents());
            if ($contents && isset($contents->status_code) && isset($contents->message)) {
                throw new HttpException($contents->status_code, $contents->message, $e);
            } elseif ($contents && $e->getResponse()->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
                return response()->json($contents, $e->getResponse()->getStatusCode());
            }
            $response = $e->getResponse();
            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }
}
