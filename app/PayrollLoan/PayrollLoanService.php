<?php

namespace App\PayrollLoan;

use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Payroll\PayrollRequestService;
use Illuminate\Support\Collection;

class PayrollLoanService
{
    const FREQUENCIES = [
        'MONTHLY',
        'SEMI_MONTHLY',
        'WEEKLY',
        'FORTNIGHTLY',
    ];

    const SUBTYPES = [
        'SALARY',
        'CALAMITY',
        'EDUCATIONAL',
        'EMERGENCY',
        'STOCK_INVESTMENT',
        'MULTI_PURPOSE',
        'HOUSING',
        'SHORT_TERM',
        'CALAMITY',
    ];

    public function checkPayrollLoanScope(
        AuthzDataScope $scope,
        int $companyId,
        array $loanIds
    ) {
        $employeeRequestService = app()->make(EmployeeRequestService::class);
        $payrollRequestService = app()->make(PayrollRequestService::class);
        $payrollLoanRequestService = app()->make(PayrollLoanRequestService::class);
        
        $loansRequest = $payrollLoanRequestService->getCompanyLoansByAttribute(
            $companyId,
            'id',
            ['values' => implode(',', $loanIds)]
        );

        $loansData = collect(json_decode($loansRequest->getData(), true)['data']);
        $payrollIds = $loansData->pluck('payroll_id')->filter()->all();
        $payrollsData = Collection::make([]);

        if ($payrollIds) {
            $payrollsData = Collection::make(json_decode(
                $payrollRequestService->getMultiple($payrollIds)->getData(),
                true
            ));
        }

        $payrollLoanData = $loansData->map(function ($loan) use (
            $employeeRequestService,
            $scope,
            $payrollsData
        ) {
            $employeeData = json_decode(
                $employeeRequestService->getEmployee($loan['employee_id'])->getData(),
                true
            );

            $companyIds = [data_get($employeeData, 'company_id')];
            $payrollGroupIds = [data_get($employeeData, 'payroll_group.id')];
            $payrollData = $payrollsData->where('id', $loan['payroll_id']);

            if ($loan['payroll_id'] && $payrollData->isEmpty()) {
                abort(406, "Error: Payroll id [{$loan['payroll_id']}] " .
                    "not found for Employee id [{$loan['employee_id']}]");
            }

            if ($loan['payroll_id']) {
                $companyIds[] = $payrollData->first()['company_id'];
                $payrollGroupIds[] = $payrollData->first()['payroll_group_id'];
            }

            return [
                'employee_id' => $loan['employee_id'],
                'is_authorized' => $scope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => $companyIds,
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                        AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id'),
                    ]
                )
            ];
        });

        return $payrollLoanData;
    }
}
