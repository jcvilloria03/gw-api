<?php

namespace App\PayrollLoan;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class PayrollLoanAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.payroll_loans';
    const CREATE_TASK = 'create.payroll_loans';
    const UPDATE_TASK = 'edit.payroll_loans';
    const DELETE_TASK = 'delete.payroll_loans';
    const REQUEST_TASK = 'request.payroll_loans';
    const APPROVE_TASK = 'approve.payroll_loans';

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeGetAll(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeRequest(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::REQUEST_TASK);
    }

    /**
     * @param \stdClass $loan
     * @param array $user
     * @return bool
     */
    public function authorizeApprove(\stdClass $loan, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loan, $user, self::APPROVE_TASK);
    }

    /**
     * Authorize loan related tasks
     *
     * @param \stdClass $loan
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $loan,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($loan->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $loan->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($loan->company_id);
        }

        return false;
    }
}
