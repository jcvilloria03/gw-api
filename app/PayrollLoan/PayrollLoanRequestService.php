<?php

namespace App\PayrollLoan;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */

class PayrollLoanRequestService extends RequestService
{
    /**
     * Constructor
     * @param \GuzzleHttp\Client $client Guzzle client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get payroll loan
     *
     * @param int $id Payroll Loan ID
     * @return \Illuminate\Http\JsonResponse Payroll Loan Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll_loan/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get payroll loan
     *
     * @param int $id Payroll Loan ID
     * @return array
     */
    public function getTemp(string $tempId)
    {
        $request = new Request(
            'GET',
            "/payroll_loan/temp/{$tempId}"
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Call endpoint to get months with at least one collected loan
     *
     * @param int $id Company ID
     * @param string type Loan type
     * @return \Illuminate\Http\JsonResponse Payroll Loan Info
     */
    public function monthsWithCollectedLoans(int $id, string $type)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/months_with_payroll_loans/{$type}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all loans within company
     *
     * @param int $companyId Company Id
     * @param AuthzDataScope|null $dataScope Authz Data Scope instance
     * @return \Illuminate\Http\JsonResponse List of company loans
     */
    public function getCompanyLoans(int $companyId, array $params, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/payroll_loans?" . http_build_query($params)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to get all loans within company
     *
     * @param int $id Company Id
     * @param string $attribute Loan attribute
     * @param array $attributeValues Attribute values
     * @return \Illuminate\Http\JsonResponse List of company loans
     */
    public function getCompanyLoansByAttribute(int $id, string $attribute, array $attributeValues)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/payroll_loans/{$attribute}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributeValues)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create initial preview payroll loan
     *
     * @param array $data payroll loan information
     * @return \Illuminate\Http\JsonResponse Created Payroll Loan
     */
    public function createInitialPreview(array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create payroll loan
     *
     * @param string $uid payroll loan unique id
     * @param array $data payroll loan information
     * @return \Illuminate\Http\JsonResponse Created Payroll Loan
     */
    public function create(string $uid, array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan/create/{$uid}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update payroll loan for given id
     *
     * @param int $id Payroll Loan Id
     * @param array $data Payroll Loan informations
     * @return \Illuminate\Http\JsonResponse Updated Payroll Loan
     */
    public function update(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to preview payroll loan update  for given id
     *
     * @param int $id Payroll Loan Id
     * @param array $data Payroll Loan details
     * @return \Illuminate\Http\JsonResponse Generated amortizations for updated Payroll Loan
     */
    public function updatePreview(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan/{$id}/update_loan_preview",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to edit amortization and save it as temporary for preview
     *
     * @param string $id Payroll Loan Id
     * @param array $data Payroll Loan details
     * @return \Illuminate\Http\JsonResponse Generated amortizations for updated Payroll Loan
     */
    public function updateAmortizationPreview(string $id, array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan/{$id}/update_amortization_preview",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Preview amortizations and payroll for edited payroll loan details
     *
     * @param int $id Payroll Loan Id
     * @param array $data Payroll Loan details
     * @return \Illuminate\Http\JsonResponse Generated amortizations for updated Payroll Loan
     */
    public function initialPreview(int $id, array $data)
    {
        $request = new Request(
            'Get',
            "/payroll_loan/{$id}/initial_preview",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple payroll loans
     *
     * @param array $data information on payroll loans to delete
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/payroll_loan/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Data to be saved in Payroll Loan upload job
     *
     * @param string $id Job ID
     * @return json Company information
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/payroll_loan/upload_preview?job_id={$jobId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Loan details
     *
     * @param int $id Payroll Loan ID
     * @return \Illuminate\Http\JsonResponse Loan details Info
     */
    public function getLoanDetails(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll_loan/{$id}/loan_details"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all amortizations of payroll loan
     *
     * @param int $id Payroll Loan ID
     * @return \Illuminate\Http\JsonResponse Amortizations Info
     */
    public function getAmortizationsDetailsOfLoan(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll_loan/{$id}/amortizations_details"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all amortizations of payroll loan
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Amortizations Info
     */
    public function activateGapLoans(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/activate_gap_loans",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get active loans for employee
     *
     * @param int $companyId Company ID
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse Active Payroll loans for employee
     */
    public function getEmployeeActiveLoans(int $companyId, int $employeeId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/employee/{$employeeId}/payroll_loans"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get unpaid loans for employee
     *
     * @param int $companyId Company ID
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse Unpaid Payroll loans for employee
     */
    public function getEmployeeUnpaidLoans(int $companyId, int $employeeId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/employee/{$employeeId}/unpaid_payroll_loans"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint for initiating file upload processing
     */
    public function processFileUpload($companyId, $bucket, $key, AuthzDataScope $authzDataScope = null)
    {
        $payload = [
            'fileKey' => $key,
            'fileBucket' => $bucket
        ];
        $request = new Request(
            'POST',
            "/company/{$companyId}/process_upload",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpont for getting file upload status
     */
    public function getFileUploadStatus($companyId, $jobId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/process_upload/status?job_id={$jobId}"
        );
        return $this->send($request);
    }
}
