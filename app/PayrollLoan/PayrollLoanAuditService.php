<?php

namespace App\PayrollLoan;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use Illuminate\Support\Facades\Log;

class PayrollLoanAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const ACTION_BATCH_CREATE = 'batch_create';
    const OBJECT_NAME = 'payroll_loan';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var App\PayrollLoan\PayrollLoanRequestService
     */
    private $requestService;

    public function __construct(
        PayrollLoanRequestService $requestService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
    }

    /**
     * Log Payroll Loan related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_BATCH_CREATE:
                $this->logBatchCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log Payroll Loan Batch Create
     *
     * @param array $cacheItem
     */
    public function logBatchCreate(array $cacheItem)
    {
        $item = new AuditItem([
            'company_id' => $cacheItem['company_id'],
            'user_id' => $cacheItem['user_id'],
            'account_id' => $cacheItem['account_id'],
            'action' => self::ACTION_BATCH_CREATE,
            'data' => $cacheItem['loans_ids'],
            'object_name' => self::OBJECT_NAME,
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Payroll Loan create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Payroll Loan update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);

        $this->auditService->log($item);
    }

     /**
     *
     * Log Payroll Loan Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $loan = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $loan->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $loan->id
            ]
        ]);

        $this->auditService->log($item);
    }
}
