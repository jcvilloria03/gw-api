<?php

namespace App\RestDay;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\Response;

class RestDayRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get rest day.
     *
     * @param int $id RestDay ID
     * @return \Illuminate\Http\JsonResponse RestDay Info
     */
    public function get(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/rest_day/{$id}"
        ));
    }

    /**
     * Call endpoint to get all rest days within company.
     *
     * @param int   $companyId      Company Id
     * @param array $employeeIds    List of employee IDs for filtering
     * @return \Illuminate\Http\JsonResponse List of company rest days
     */
    public function getCompanyRestDays(int $companyId, array $employeeIds = [])
    {
        $data = json_encode([
            'data' => [
                'employee_ids' => $employeeIds
            ]
        ]);

        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/rest_days",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to get all rest days within employee with given ID.
     *
     * @param int $employeeId Employee Id
     * @param array $params Additional params
     * @return \Illuminate\Http\JsonResponse List of employee rest days
     */
    public function getEmployeeRestDays(int $employeeId, array $params = [])
    {
        return $this->send(new Request(
            'GET',
            "/employee/{$employeeId}/rest_days?" . http_build_query($params)
        ));
    }

    /**
     * Call endpoint to get all rest days within employee with given ID.
     *
     * @param int $employeeId Employee Id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse List of employee rest days
     */
    public function getCompanyEmployeeRestDays(int $employeeId, int $companyId)
    {
        return $this->send(new Request(
            'GET',
            "/employee/{$employeeId}/rest_days?" . http_build_query(['company_id' => $companyId])
        ));
    }

    /**
     * Call endpoint to get all employee rest day on a given date
     *
     * @param int       $employeeId Employee Id
     * @param string    $date       Date
     * @return array|null
     */
    public function getEmployeeRestDayByDate(int $employeeId, string $date)
    {
        $response = $this->send(new Request(
            'GET',
            "/employee/{$employeeId}/rest_day_by_date?" . http_build_query(['date' => $date])
        ));

        if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
            return null;
        }

        return json_decode($response->getData(), true);
    }

    /**
     * Call endpoint to store rest day.
     *
     * @param array $data Rest Day data
     * @return \Illuminate\Http\JsonResponse Created rest day id
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            '/rest_day',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to update rest day.
     *
     * @param array $data Rest Day data
     * @return \Illuminate\Http\JsonResponse Created rest day id
     */
    public function update(int $id, array $data)
    {
        return $this->send(new Request(
            'PUT',
            "/rest_day/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to unassign rest day.
     *
     * @param int $id Rest Day ID
     * @param array $data Rest day attributes to unassign by.
     * @return \Illuminate\Http\JsonResponse Created rest day id
     */
    public function unassign(int $id, array $data)
    {
        return $this->send(new Request(
            'POST',
            "/rest_day/unassign/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to assign and overwrite employee rest day
     *
     * @param  array  $data Shift data
     * @return array
     */
    public function assignOverwrite(array $data)
    {
        $request = new Request(
            'POST',
            '/rest_day/assign_overwrite',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }
}
