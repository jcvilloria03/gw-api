<?php

namespace App\RestDay;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class RestDayAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.rest_day';
    const CREATE_TASK = 'create.rest_day';
    const UPDATE_TASK = 'edit.rest_day';
    const DELETE_TASK = 'delete.rest_day';

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $restDay, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($restDay, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyRestDays(\stdClass $restDay, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($restDay, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $restDay, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($restDay, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $restDay, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($restDay, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeUnassign(\stdClass $restDay, array $user)
    {
        return $this->authorizeUpdate($restDay, $user);
    }

    /**
     * @param \stdClass $restDay
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $restDay, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($restDay, $user, self::DELETE_TASK);
    }

    /**
     * Authorize rest day related tasks
     *
     * @param \stdClass $restDay
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $restDay,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($restDay->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $restDay->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($restDay->company_id);
        }

        return false;
    }
}
