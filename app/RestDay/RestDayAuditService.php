<?php

namespace App\RestDay;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use Illuminate\Support\Facades\Log;
use App\User\UserRequestService;
use Carbon\Carbon;

class RestDayAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_UNASSIGN = 'unassign';
    const OBJECT_NAME = 'rest_day';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var App\RestDay\RestDayRequestService
     */
    private $requestService;

    /**
     * @var \App\User\UserRequestService
     */
    private $userRequestService;

    public function __construct(
        RestDayRequestService $requestService,
        UserRequestService $userRequestService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Log RestDay related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_UNASSIGN:
                $this->logUnassign($cacheItem);
                break;
        }
    }

    /**
     * Log RestDay create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'user_name' => $userData->name,
                'employee_name' => $data['employee']['first_name'] . ' ' . $data['employee']['first_name'],
                'rest_day_start_date' => $data['start_date'],
                'rest_day_end_date' => $data['end_date']
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log RestDay update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'user_name' => $userData->name,
                'employee_name' => $new['employee']['first_name'] . ' ' . $new['employee']['first_name'],
                'old_rest_day_data' => $old,
                'new_rest_day_data' => $new
            ]
        ]);

        $this->auditService->log($item);
    }

     /**
     *
     * Log RestDay Delete
     *
     * @param array $cacheItem
     *
     */
    public function logUnassign(array $cacheItem)
    {
        $restDay = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $restDay->company_id,
            'account_id' => $userData->account_id,
            'user_id' => $userData->id,
            'action' => self::ACTION_UNASSIGN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $restDay->id,
                'user_name' => $userData->name,
                'employee_name' => $restDay->employee->first_name . ' ' . $restDay->employee->last_name,
                'rest_day_start_date' => $restDay->start_date,
                'rest_day_end_date' => $restDay->end_date
            ]
        ]);

        $this->auditService->log($item);
    }
}
