<?php

namespace App\AllowanceType;

use App\Common\CommonAuthorizationService;

class AllowanceTypeAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.allowance_types';
    public $createTask = 'create.allowance_types';
    public $updateTask = 'edit.allowance_types';
    public $deleteTask = 'delete.allowance_types';
}
