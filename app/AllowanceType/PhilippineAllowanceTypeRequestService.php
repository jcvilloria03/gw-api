<?php

namespace App\AllowanceType;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineAllowanceTypeRequestService extends RequestService
{
    /**
     * Call endpoint to create company allowance types
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/allowance_type/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check allowance type name is available
     *
     * @param int $companyId Company ID
     * @param array $data allowance type information
     * @return \Illuminate\Http\JsonResponse Availability of allowance type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$companyId}/other_income_type/allowance_type/is_name_available",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update allowance type.
     *
     * @param int $id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $data)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/allowance_type/{$id}",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }
}
