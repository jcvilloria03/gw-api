<?php

namespace App\FinalPay;

use App\Common\CommonAuthorizationService;

class FinalPayAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.final_pays';
    public $createTask = 'create.final_pays';
    public $updateTask = 'edit.final_pays';
    public $closeTask = 'close.final_pays';
    public $deleteTask = 'delete.final_pays';

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeClose(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->closeTask);
    }
}
