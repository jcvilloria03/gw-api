<?php

namespace App\FinalPay;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class FinalPayRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch undeducted bonuses for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @return json Bonuses
     *
     */
    public function getBonuses(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/bonuses"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch undeducted commissions for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @return json Commissions
     *
     */
    public function getCommissions(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/commissions"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch allowances for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @return json Allowances
     *
     */
    public function getAllowances(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/allowances"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch deductions for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @return json Deductions
     *
     */
    public function getDeductions(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/deductions"
        );

        return $this->send($request);
    }

     /**
     * Call endpoint to fetch adjustments for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @return json Adjustments
     *
     */
    public function getAdjustments(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/adjustments"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch contributions for given Employee ID
     *
     * @param int $id EmployeeId ID
     * @param int $terminationInformationId Termination Information ID
     * @return json Contributions
     *
     */
    public function getContributions(int $id, int $terminationInformationId)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/final_pay/contributions/$terminationInformationId"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get Unprocessed Final Pays
     *
     * @param int $companyId Company ID
     * @param array $query Release Dates
     * @param AuthzDataScope|null $authzDataScope
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUnprocessedFinalPays($companyId, array $query, $authzDataScope = null)
    {
        $queryString = empty($query) ? '' : '?' . http_build_query($query);

        return $this->send(
            new Request(
                'GET',
                "/company/{$companyId}/employees/final_pay" . $queryString
            ),
            $authzDataScope
        );
    }

    /**
     * Call endpoint to compute final pay for given attributes
     *
     * @param int $id EmployeeId ID
     * @param array $attributes Additional parameters
     * @return json Computed data
     *
     */
    public function compute(int $id, $attributes)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/final_pay/compute",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to save final pay for given attributes
     *
     * @param int $id EmployeeId ID
     * @param array $attributes Additional parameters
     * @return json Computed data
     *
     */
    public function save(int $id, $attributes)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/final_pay/save",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to save final pay
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveFinalPay(string $data)
    {
        $request = new Request(
            'POST',
            '/final_pay',
            ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            $data
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get employees available items for final pay
     *
     * @param int $id EmployeeId ID
     * @param array $attributes Additional parameters
     * @return json Computed data
     *
     */
    public function getAvailableItems(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/final_pay/available_items",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create projection job for final pay
     *
     * @param string $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectFinalPay(int $companyId, int $employeeId)
    {
        $request = new Request(
            'POST',
            "/final_pay/project",
            ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            json_encode(['company_id' => $companyId, 'employee_id' => $employeeId])
        );

        return $this->send($request);
    }

    /**
     * Call endpoint for patch final pay, this will trigger active to semi active
     */
    public function patchFinalPay(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            '/final_pay/' . $id,
            ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }
}
