<?php

namespace App\Ess;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class EssEmployeeRequestService extends RequestService
{
    /**
     * Call endpoint to get the Employee Profile for ESS
     *
     * @param int $id Employee Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfile(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/employee/{$id}/profile"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get teams where employee belongs to with memebers
     *
     * @param int $id Employee Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeTeams(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/teams"
        );

        return $this->send($request);
    }

    /**
     * Fetch employee for given internal employee id
     *
     * @param int $id Employee ID
     * @return \Illuminate\Http\JsonResponse Employee information
     *
     */
    public function getEmployeePersonalInfo(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/personal_info"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get entitled employees for given data
     *
     * @param array $data Affected entities
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEntitledEmployees(array $data, int $companyId)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/filtered_employees",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'filters' => $data
            ])
        );

        return $this->send($request);
    }
}
