<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssNotificationRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get the User Notifications for ESS
     *
     * @param int $id User Id
     * @param array $query query params
     * @param bool $isAdminUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotifications(int $id, array $query, bool $isAdminUser = false)
    {
        $query['is_admin_user'] = $isAdminUser;
        $request = new Request(
            'GET',
            "/user/{$id}/notifications",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get the User Notifications for T&A
     *
     * @param int $id User Id
     * @param bool $paginate use pagination
     * @param int $page for pagination
     * @param int $perPage items per page
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnnouncementNotifications(
        int $id,
        $paginate,
        $page = 1,
        $perPage = 10,
        $responseType = null,
        $activityType = null
    ) {
        $queryParams = http_build_query([
            "paginate" => $paginate,
            "page" => $page,
            "per_page" => $perPage,
            "response_type" => $responseType,
            "activity_type" => $activityType
        ]);

        $request = new Request(
            'GET',
            "/user/{$id}/announcement_notifications?{$queryParams}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get the User Notifications Status for ESS
     *
     * @param int $id User Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificationsStatus(int $id)
    {
        $request = new Request(
            'GET',
            "/user/{$id}/notifications_status"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update the User Notifications Status for ESS
     *
     * @param int $id User Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateNotificationsStatus(int $id)
    {
        $request = new Request(
            'PUT',
            "/user/{$id}/notifications_status"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update Notification clicked status
     *
     * @param int $id Notification ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function notificationClicked(int $id)
    {
        $request = new Request(
            'PUT',
            "/notification/{$id}/clicked"
        );

        return $this->send($request);
    }

    /**
     * Send email to notify user upon successful password change.
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmailNotificationToUser($data)
    {
        $request = new Request(
            'POST',
            "/notification/password_changed",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );
        return $this->send($request);
    }
}
