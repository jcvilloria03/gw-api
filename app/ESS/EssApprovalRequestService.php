<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * Class ApprovalRequestService
 *
 * @package App\Approval
 */
class EssApprovalRequestService extends RequestService
{
    /**
     * Call endpoint to get the Employee Requests by ID
     *
     * @param array $filters Filters
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeRequestsById(array $filters)
    {
        return $this->send(new Request(
            'GET',
            "/employee_request?" . http_build_query($filters),
            [
                'Content-Type' => 'application/json'
            ]
        ));
    }

    /**
     * Call endpoint to get the employee approvals for given ID and request data
     *
     * @param int   $employeeId Employee Id
     * @param array $data       Request data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeApprovals(int $employeeId, array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee/{$employeeId}/approvals",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to decline requests for given IDs in request data
     *
     * @param array $data Request data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDecline(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee_request/bulk_decline",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to approve requests for given IDs in request data
     *
     * @param array $data Request data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkApprove(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee_request/bulk_approve",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to cancel requests for given IDs in request data
     *
     * @param array $data Request data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCancel(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee_request/bulk_cancel",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }
}
