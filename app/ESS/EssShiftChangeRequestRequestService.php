<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssShiftChangeRequestRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get shift change request data
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request('GET', "/shift_change_request/{$id}"));
    }

    /**
     * Call endpoint to create shift change request
     *
     * @param array $data shift change request information
     * @return \Illuminate\Http\JsonResponse Created Shift Change Request
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            '/shift_change_request',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }
}
