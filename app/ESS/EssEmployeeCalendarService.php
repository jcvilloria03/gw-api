<?php

namespace App\ESS;

use App\Attendance\AttendanceRequestService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\RestDay\RestDayRequestService;
use App\Shift\ShiftRequestService;
use App\TimeRecord\TimeRecordService;
use Carbon\Carbon;

class EssEmployeeCalendarService
{
    const STATE_CLOCK_IN = true;

    const STATE_CLOCK_OUT = false;

    /** @var \App\Shift\ShiftRequestService */
    protected $shiftService;

    /** @var \App\RestDay\RestDayRequestService */
    protected $restDayService;

    /** @var \App\TimeRecord\TimeRecordService */
    protected $timeRecordService;

    /** @var \App\DefaultSchedule\DefaultScheduleRequestService */
    protected $defaultScheduleService;

    /** @var \App\Attendance\AttendanceRequestService */
    protected $attendanceService;

    /**
     * Constructs EssEmployeeCalendarService
     *
     * @param ShiftRequestService $shiftService
     * @param TimeRecordService   $timeRecordService
     */
    public function __construct(
        ShiftRequestService $shiftService,
        RestDayRequestService $restDayService,
        TimeRecordService $timeRecordService,
        DefaultScheduleRequestService $defaultScheduleService,
        AttendanceRequestService $attendanceService
    ) {
        $this->shiftService = $shiftService;

        $this->restDayService = $restDayService;

        $this->timeRecordService = $timeRecordService;

        $this->defaultScheduleService = $defaultScheduleService;

        $this->attendanceService = $attendanceService;
    }

    /**
     * Generator to get dates from start date to end date
     *
     * @param  Carbon $startDate Start date range
     * @param  Carbon $endDate   End date range
     * @return Generator|string[]
     */
    public function getDates(Carbon $startDate, Carbon $endDate)
    {
        for ($date = $startDate->copy(); $date->lte($endDate); $date->addDay()) {
            yield $date->format('Y-m-d');
        }
    }

    public function getDefaultSchedules(int $companyId)
    {
        $defaultScheduleRequest = $this->defaultScheduleService->index($companyId);
        $defaultScheduleData = json_decode($defaultScheduleRequest->getData(), true)['data'];

        $defaultSchedule = collect($defaultScheduleData)
            ->keyBy('day_of_week')
            ->map(function ($entry) {
                $name = $entry['day_type'] === 'rest_day' ? 'Rest Day' : 'Default Schedule';

                return [
                    'id' => 0,
                    'schedule_id' => 0,
                    'schedule' => [
                        'name' => $name,
                        'type' => 'fixed',
                        'default' => true,
                        'day_type' => $entry['day_type'],
                        'start_time' => $entry['work_start'],
                        'end_time' => $entry['work_end'],
                        'total_hours' => $entry['total_hours'],
                    ]
                ];
            });

        return $defaultSchedule;
    }

    /**
     * Get assigned shifts and its schedule by employee for a given date range
     *
     * @param  int    $employeeId Employee ID
     * @param  Carbon $startDate  Start date
     * @param  Carbon $endDate    End date
     * @return array
     */
    public function getShifts(int $employeeId, Carbon $startDate, Carbon $endDate)
    {
        $shiftsRequest = $this->shiftService->getEmployeeShifts($employeeId, [
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d')
        ]);

        $shiftsData = collect(json_decode($shiftsRequest->getData(), true)['data']);
        $shifts = collect();

        foreach ($this->getDates($startDate, $endDate) as $date) {
            $shifts[$date] = $shiftsData
                ->filter(function ($shift) use ($date) {
                    return in_array($date, $shift['dates']);
                })
                ->map(function ($shift) {
                    return [
                        'id' => $shift['id'],
                        'schedule_id' => $shift['schedule_id'],
                        'start_date' => $shift['start'],
                        'end_date' => $shift['end'],
                        'schedule' => [
                            'name' => $shift['schedule']['name'],
                            'type' => $shift['schedule']['type'],
                            'default' => false,
                            'day_type' => 'regular',
                            'start_time' => $shift['schedule']['start_time'],
                            'end_time' => $shift['schedule']['end_time'],
                            'total_hours' => $shift['schedule']['total_hours'],
                        ]
                    ];
                })
                ->values();
        }

        return $shifts;
    }

    /**
     * Get user-defined rest days per employee for the given date range
     *
     * @param  int    $employeeId Employee ID
     * @param  Carbon $startDate  Start date
     * @param  Carbon $endDate    End date
     * @return array
     */
    public function getRestDays(int $employeeId, Carbon $startDate, Carbon $endDate)
    {
        $restDaysRequest = $this->restDayService->getEmployeeRestDays($employeeId, [
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d')
        ]);

        $restDaysData = collect(json_decode($restDaysRequest->getData(), true)['data']);
        $restDays = collect();

        foreach ($this->getDates($startDate, $endDate) as $date) {
            $restDays[$date] = $restDaysData
                ->filter(function ($restDay) use ($date) {
                    return in_array($date, $restDay['dates']);
                })
                ->map(function ($restDay) {
                    return [
                        'id' => 0,
                        'schedule_id' => 0,
                        'start_date' => null,
                        'end_date' => null,
                        'schedule' => [
                            'name' => 'Rest Day',
                            'type' => 'fixed',
                            'default' => false,
                            'day_type' => 'rest_day',
                            'start_time' => null,
                            'end_time' => null,
                            'total_hours' => null,
                        ]
                    ];
                })
                ->values();
        }

        return $restDays;
    }

    /**
     * Get timesheet by employee for a given start date and end date
     *
     * @param  int    $employeeId Employee ID
     * @param  Carbon $startDate  Start date
     * @param  Carbon $endDate    End date
     * @return array
     */
    public function getTimesheets(int $employeeId, Carbon $startDate, Carbon $endDate)
    {
        $timesheets = collect($this->timeRecordService->getTimesheetForEmployee($employeeId))
            ->filter(function ($entry) use ($startDate, $endDate) {
                $timestamp = Carbon::createFromTimestamp($entry['timestamp']);

                // Extend end date search to next day for shifts that span to the next date
                $endDateTomorrow = $endDate->copy()->addDay();

                return $timestamp->between($startDate, $endDateTomorrow);
            })
            ->groupBy(function ($entry) {
                $timestamp = Carbon::createFromTimestamp($entry['timestamp']);

                return $timestamp->format('Y-m-d');
            });

        // Include the clock out on the next date to the current date when its matching clock in
        // occurs at the current date.
        $timesheets = $timesheets->map(function ($entries, $date) use ($timesheets) {
            $lastEntry = $entries->last();

            if ($lastEntry['state'] === self::STATE_CLOCK_IN) {
                // Get the time records that occurred on the next date
                $nextDateIndex = $timesheets->keys()->search($date) + 1;
                $nextDateEntries = $timesheets->slice($nextDateIndex, 1)->first();
                $nextDateFirstEntry = $nextDateEntries ? $nextDateEntries->first() : null;

                if ($nextDateFirstEntry && $nextDateFirstEntry['state'] === self::STATE_CLOCK_OUT) {
                    $entries->push($nextDateFirstEntry);
                }
            }

            return $entries;
        });

        return $timesheets;
    }

    /**
     * Get badges by employee for a given start date and end date
     *
     * @param  int    $employeeId Employee ID
     * @param  Carbon $startDate  Start date
     * @param  Carbon $endDate    End date
     * @return array
     */
    public function getBadges(int $employeeId, Carbon $startDate, Carbon $endDate)
    {
        $badges = $this->attendanceService->getBadges([
            'employee_ids' => [$employeeId],
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d')
        ]);

        return collect($badges)->keyBy('date')->map(function ($entry) {
            return $entry['badges'];
        });
    }
}
