<?php

namespace App\ESS;

use App\Attendance\AttendanceRequestService;
use App\Traits\DateTrait;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class EssApprovalService
{
    const REQUEST_PENDING_STATUS = 'Pending';
    const REQUEST_APPROVED_STATUS = 'Approved';

    use DateTrait;

    public function __construct(
        AttendanceRequestService $attendanceRequestService
    ) {
        $this->attendanceRequestService = $attendanceRequestService;
    }

    private function prepareEmployeeRequestDates(array $dateData)
    {
        $date = Arr::get($dateData, 'date');
        $startDate = Arr::get($dateData, 'start_date') ? Carbon::parse($dateData['start_date']) : null;
        $endDate = Arr::get($dateData, 'end_date') ? Carbon::parse($dateData['end_date']) : null;

        if ($startDate && $startDate->diff($endDate)->days > 0) {
            $startDate = $startDate->format('Y-m-d');
            $endDate = $endDate->subDay()->format('Y-m-d');
        }

        $date = $date ? [$date] : [];
        $dates = $startDate ? $this->generateDateRange($startDate, $endDate) : [];

        return array_merge($date, $dates);
    }

    public function getLockedDates(int $employeeId, array $dates)
    {
        $lockedDates = [];
        foreach ($dates as $date) {
            try {
                $response = $this->attendanceRequestService
                    ->getSingleAttendanceRecordLockStatus($employeeId, $date);
                $responseData = json_decode($response->getData(), true);

                if ($responseData['data']) {
                    array_push($lockedDates, $date);
                }
            } catch (\Exception $e) {
                // Do nothing
            }
        }

        return $lockedDates;
    }

    public function validateRequestDatesLockStatus(array $requests)
    {
        $formattedRequests = [
            "processible_requests" => [],
            "unprocessible_requests" => []
        ];

        foreach ($requests as $request) {
            // approved request with locked dates will not be processed
            $dates = $this->prepareEmployeeRequestDates($request['dates']);
            $lockedDates = $this->getLockedDates($request['employee_id'], $dates);

            if (!empty($lockedDates)) {
                $request['locked_dates'] = $lockedDates;
            }

            $arrayRequest = empty($lockedDates) ? 'processible_requests' : 'unprocessible_requests';
            array_push($formattedRequests[$arrayRequest], $request);
        }

        return $formattedRequests;
    }
}
