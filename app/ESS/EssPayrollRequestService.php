<?php

namespace App\Ess;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssPayrollRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get the Employee YTD data
     *
     * @param int $id Employee Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getYtdProfile(int $id)
    {
        $request = new Request(
            'GET',
            "/ess/philippine/employee/{$id}/ytd"
        );

        return $this->send($request);
    }
}
