<?php

namespace App\ESS;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class EssClockStateService
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Call endpoint to get Timeclock state for employee with given id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        try {
            $response = $this->client->request(
                'GET',
                "state/$id"
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' : Could not fetch Timeclock state for Employee with id = ' . $id);
            $response = $e->getResponse();

            if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
                return response()->json(json_encode(false));
            }

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Call endpoint to get last timeclock for employee with given id
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function lastTimeclock(int $id)
    {
        try {
            $response = $this->client->request(
                'GET',
                "last_timeclock/$id"
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            $response = $e->getResponse();

            if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
                return response()->json('');
            }

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Call endpoint to get Timeclock timesheet for employee with given id
     *
     * @param int $id
     * @param array $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTimesheet(int $id, array $query = [])
    {
        $queryString = http_build_query($query);

        try {
            $response = $this->client->request(
                'GET',
                "timesheet/{$id}?{$queryString}"
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage() .
                ' : Could not fetch Timesheet for Employee with id = ' .
                $id . '. Filters: ' . $queryString);

            $response = $e->getResponse();

            if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
                return response()->json(json_encode([]));
            }

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Call endpoint to log Timeclock entry for employee with given id
     *
     * @param int $id Employee ID
     * @param int $companyId Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function log(int $id, int $companyId, array $data)
    {
        $tz = $data['timezone'] ?? 'Asia/Manila';

        $timestamp = !empty($data['timestamp'])
            ? Carbon::createFromFormat('Y-m-d H:i:s', $data['timestamp'], $tz)->timestamp
            : Carbon::now()->timestamp;

        try {
            if ($data['state'] == '1') {
                $response = $this->client->request(
                    'PUT',
                    'log',
                    [
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ],
                        'json' => [
                            'employee_uid' => strval($id),
                            'company_uid' => strval($companyId),
                            'state' => strval($data['state']),
                            'tags' => $data['tags'] ?? [],
                            'timestamp' => strval($timestamp)
                        ]
                    ]
                );
            } else {
                $response = $this->client->request(
                    'PUT',
                    'log',
                    [
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ],
                        'json' => [
                            'employee_uid' => strval($id),
                            'company_uid' => strval($companyId),
                            'state' => strval($data['state']),
                            'tags' => $data['tags'] ?? [],
                            'timestamp' => strval($timestamp),
                            'hour_rule' => strval($data['max_clockout_rule']['hours'])
                        ]
                    ]
                );
            }

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' : Could not log Timeclock entry for Employee with id = ' . $id);
            $response = $e->getResponse();

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Get attendance of team members.
     *
     * @param array $teamMembersIds
     * @param int|string $employeeId
     * @return array
     */
    public function getTeamAttendance(array $teamMembersIds, $employeeId)
    {
        $teamAttendances = [];

        foreach ($teamMembersIds as $id) {
            // Skip current user
            if ($id === $employeeId) {
                continue;
            }

            try {
                $memberAttendance = json_decode($this->getTimesheet($id)->getData(), true);
            } catch (HttpException $e) {
                $memberAttendance = [];
            }

            $teamAttendances = array_merge($teamAttendances, $memberAttendance);
        }

        return $teamAttendances;
    }
}
