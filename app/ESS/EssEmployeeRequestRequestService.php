<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class EssEmployeeRequestRequestService extends RequestService
{
    /**
     * Call endpoint to get the employee Requests details for given ID
     *
     * @param int $id EmployeeRequest Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request('GET', "/employee_request/{$id}"));
    }

    /**
     * Call endpoint to get the employee Requests for ESS
     *
     * @param int $id Employee Id
     * @param string $queryParams from request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRequests(int $id, string $queryParams)
    {
        return $this->send(new Request('GET', "/employee/{$id}/requests?{$queryParams}"));
    }

    /**
     * Call endpoint to store request message
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessage(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/request_message",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to cancel request
     *
     * @param int $id Request Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(int $id)
    {
        return $this->send(new Request('POST', "/employee_request/{$id}/cancel"));
    }

    /**
     * Call endpoint to get Leaves for given data
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLeaves(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/leaves",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to get related pending employee requests to default schedule.
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRelatedRequestsToUpdatedDefaultScheduleDaysOfWeek(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee_request/related_requests_to_updated_days_of_week",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }
}
