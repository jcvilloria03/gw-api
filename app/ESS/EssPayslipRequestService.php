<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssPayslipRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Payslip information
     *
     * @param int $employeeId
     * @param int $year
     * @return \Illuminate\Http\JsonResponse Payslips
     */
    public function getPayslips(int $employeeId, int $year)
    {
        $request = new Request(
            'GET',
            "/ess/payslips?employee_id={$employeeId}&year={$year}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Payslip information
     *
     * @param int $employeeId
     * @return \Illuminate\Http\JsonResponse Payslips
     */
    public function getUnreadPayslips(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/ess/payslips/unread?employee_id={$employeeId}"
        );
        return $this->send($request);
    }
}
