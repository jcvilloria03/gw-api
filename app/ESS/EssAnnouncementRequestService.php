<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssAnnouncementRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to create announcement with given data
     *
     * @param array $data Request data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            '/announcement/',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get role of given user for announcement
     *
     * @param int $id Announcement ID
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRoleForAnnouncement($id, int $userId)
    {
        $request = new Request(
            'GET',
            "/announcement/{$id}/user_role/{$userId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get announcement for given id for given user role
     *
     * @param int $id Announcement ID
     * @param string $role logged user role on given announcement
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id, string $role, int $userId)
    {
        $request = new Request(
            'GET',
            "/announcement/{$id}?role={$role}&user_id={$userId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get announcement response for given id
     *
     * @param int $id Response ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReply($id)
    {
        $request = new Request(
            'GET',
            "/announcement/reply/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get user announcements filtered
     *
     * @param int $userId User ID
     * @param array $data filter params
     * @param string $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserAnnouncements(int $userId, array $data, string $query)
    {
        $request = new Request(
            'POST',
            "/user/{$userId}/announcements?{$query}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get user announcements filtered
     *
     * @param int $userId User ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserUnreadAnnouncements(int $userId)
    {
        $request = new Request(
            'GET',
            "/user/{$userId}/announcements/unread"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to send reply to Announcement with given id
     *
     * @param int $id Announcement ID
     * @param array $data request data
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/announcement/{$id}/reply",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update recipient when saw announcement
     *
     * @param int $id Announcement ID
     * @param int $recipientId Recipient User ID
     * @param string $ipAddress Recipient IP address
     * @param string $domain Domain from which the recipient has viewed the announcement
     * @return \Illuminate\Http\JsonResponse
     */
    public function recipientSawAnnouncement($id, int $recipientId, string $ipAddress, $domain = '')
    {
        $request = new Request(
            'PUT',
            "/announcement/{$id}/recipient/{$recipientId}/seen",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'ip_address' => $ipAddress,
                'domain' => $domain
            ])
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to mark reply as seen when announcement sender open it
     *
     * @param int $id Announcement Reply ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function markReplyAsSeen($id)
    {
        $request = new Request(
            'PUT',
            "/announcement/reply/{$id}/seen"
        );

        return $this->send($request);
    }
}
