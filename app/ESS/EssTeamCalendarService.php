<?php

namespace App\ESS;

use App\Attendance\AttendanceRequestService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\Shift\ShiftRequestService;
use App\TimeRecord\TimeRecordService;
use Carbon\Carbon;

class EssTeamCalendarService extends EssEmployeeCalendarService
{
    /**
     * Get assigned shifts and its schedule for each employee for a given date range
     *
     * @param  int    $companyId   Company ID
     * @param  array  $employeeIds List Employee IDs
     * @param  Carbon $startDate   Start date
     * @param  Carbon $endDate     End date
     * @return \Illuminate\Support\Collection
     */
    public function getMultipleShifts(int $companyId, array $employeeIds, Carbon $startDate, Carbon $endDate)
    {
        $shiftsResponse = $this->shiftService->getMultipleEmployeeShifts($companyId, $employeeIds, [
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d')
        ]);

        $shiftsData = collect($shiftsResponse)->pluck('shifts')->flatten(1);
        $shifts = [];

        foreach ($this->getDates($startDate, $endDate) as $date) {
            $shifts[$date] = $shiftsData
                ->filter(function ($shift) use ($date) {
                    return in_array($date, $shift['dates']);
                })
                ->map(function ($shift) {
                    return [
                        'id' => $shift['id'],
                        'employee_id' => $shift['employee_id'],
                        'schedule_id' => $shift['schedule_id'],
                        'schedule' => [
                            'name' => $shift['schedule']['name'],
                            'type' => $shift['schedule']['type'],
                            'default' => false,
                            'day_type' => 'regular',
                            'start_time' => $shift['schedule']['start_time'],
                            'end_time' => $shift['schedule']['end_time'],
                            'total_hours' => $shift['schedule']['total_hours'],
                        ]
                    ];
                })
                ->groupBy('employee_id');
        }

        return $shifts;
    }

    /**
     * Get user-defined rest days for each employee for a given date range
     *
     * @param  int    $companyId   Company ID
     * @param  array  $employeeIds List Employee IDs
     * @param  Carbon $startDate   Start date
     * @param  Carbon $endDate     End date
     * @return \Illuminate\Support\Collection
     */
    public function getMultipleRestDays(int $companyId, array $employeeIds, Carbon $startDate, Carbon $endDate)
    {
        $restDaysResponse = $this->restDayService->getCompanyRestDays($companyId, $employeeIds);

        $restDaysData = collect(json_decode($restDaysResponse->getData(), true)['data']);
        $restDays = [];

        foreach ($this->getDates($startDate, $endDate) as $date) {
            $restDays[$date] = $restDaysData
                ->filter(function ($restDay) use ($date) {
                    return in_array($date, $restDay['dates']);
                })
                ->map(function ($restDay) {
                    return [
                        'id' => 0,
                        'employee_id' => $restDay['employee_id'],
                        'schedule_id' => 0,
                        'schedule' => [
                            'name' => 'Rest Day',
                            'type' => 'fixed',
                            'default' => false,
                            'day_type' => 'rest_day',
                            'start_time' => null,
                            'end_time' => null,
                            'total_hours' => null,
                        ]
                    ];
                })
                ->groupBy('employee_id');
        }

        return $restDays;
    }

    /**
     * Get badges by employee for a given start date and end date
     *
     * @param  array  $employeeIds List of employee IDs
     * @param  Carbon $startDate   Start date
     * @param  Carbon $endDate     End date
     * @return \Illuminate\Support\Collection
     */
    public function getMultipleBadges(array $employeeIds, Carbon $startDate, Carbon $endDate)
    {
        $entries = $this->attendanceService->getBadges([
            'employee_ids' => $employeeIds,
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d')
        ]);

        $result = [];

        foreach ($entries as $entry) {
            $date = $entry['date'];
            $employeeId = $entry['employee_id'];

            $result[$date][$employeeId] = [
                'badges' => $entry['badges'],
                'summary' => $entry['summary']
            ];
        }

        return collect($result);
    }
}
