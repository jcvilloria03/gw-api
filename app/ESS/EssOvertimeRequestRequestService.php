<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssOvertimeRequestRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get overtime request data
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request('GET', "/overtime_request/{$id}"));
    }

    /**
     * Call endpoint to create overtime request
     *
     * @param array $data overtime request information
     * @return \Illuminate\Http\JsonResponse Created Overtime Request
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/overtime_request",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }
}
