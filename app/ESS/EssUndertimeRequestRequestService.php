<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssUndertimeRequestRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get undertime request data
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request('GET', "/undertime_request/{$id}"));
    }

    /**
     * Call endpoint to create undertime request
     *
     * @param array $data undertime request information
     * @return \Illuminate\Http\JsonResponse Created Undertime Request
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/undertime_request",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }
}
