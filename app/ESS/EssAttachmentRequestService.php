<?php

namespace App\ESS;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\MultipartStream;
use App\Request\DownloadRequestService;

class EssAttachmentRequestService extends DownloadRequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to upload Attachment
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(array $data)
    {
        $multipartContents = $this->getMultipartContents($data);
        $multipartStream = new MultipartStream($multipartContents);

        $request = new Request(
            'POST',
            '/employee_request/attachments',
            [],
            $multipartStream
        );

        return $this->send($request);
    }

     /**
     * Call endpoint to replace Attachment
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function replace(array $data)
    {
        $multipartContents = $this->getMultipartContents($data);
        $multipartStream = new MultipartStream($multipartContents);

        $request = new Request(
            'POST',
            '/employee_request/attachments/replace',
            [],
            $multipartStream
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete Attachment
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/employee_request/attachments/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to download attachments
     *
     * @param int $id Employe Request ID
     * @return $reponse
     */
    public function downloadAttachments($id)
    {
        $request = new Request(
            'GET',
            "/employee_request/{$id}/download_attachments"
        );

        return $this->download($request);
    }

    /**
     * Creates multipart contents for requests with attachment in body
     *
     * @param array $data
     * @return array $multipartContents Multipart Contents
     *
     */
    private function getMultipartContents(array $data)
    {
        $multipartContents = [];

        foreach ($data as $index => $value) {
            $multipartContents[] = [
                'name' => $index,
                'contents' => $index === 'attachment' ? fopen($value->getPathName(), 'r') : $value
            ];
        }

        return $multipartContents;
    }
}
