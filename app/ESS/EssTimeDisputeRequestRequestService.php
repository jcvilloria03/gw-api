<?php

namespace App\ESS;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EssTimeDisputeRequestRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get time dispute request data
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request('GET', "/time_dispute_request/{$id}"));
    }

    /**
     * Call endpoint to create time dispute request
     *
     * @param array $data time dispute request information
     * @return \Illuminate\Http\JsonResponse Created Time Dispute Request
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            '/time_dispute_request',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }
}
