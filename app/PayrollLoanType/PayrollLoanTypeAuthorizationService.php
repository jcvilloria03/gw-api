<?php

namespace App\PayrollLoanType;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class PayrollLoanTypeAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.payroll_loan_types';
    const CREATE_TASK = 'create.payroll_loan_types';
    const UPDATE_TASK = 'edit.payroll_loan_types';
    const DELETE_TASK = 'delete.payroll_loan_types';

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeGetAll(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $loanType
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $loanType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($loanType, $user, self::CREATE_TASK);
    }

    /**
     * Authorize loan type related tasks
     *
     * @param \stdClass $loanType
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $loanType,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for account
            return $accountScope->inScope($loanType->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as account
                return $loanType->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($loanType->company_id);
        }

        return false;
    }
}
