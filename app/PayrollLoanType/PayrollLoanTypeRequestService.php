<?php

namespace App\PayrollLoanType;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PayrollLoanTypeRequestService extends RequestService
{
    /**
     * Constructor
     * @param \GuzzleHttp\Client $client Guzzle client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get all loan types within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company loan types
     */
    public function getCompanyLoanTypes(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/payroll_loan_types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get loan type
     *
     * @param int $id Loan Type Id
     * @return \Illuminate\Http\JsonResponse Loan type details
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll_loan_type/{$id}/"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create payroll loan type
     *
     * @param array $data payroll loan information
     * @return \Illuminate\Http\JsonResponse Created Payroll Loan
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/payroll_loan_type",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Get loan subtypes for a given name
     *
     * @param string $loanTypeName Loan name
     * @return \Illuminate\Http\JsonResponse List of company loan types
     */
    public function getLoanSubtypesByName(string $loanTypeName)
    {
        $request = new Request(
            'GET',
            "payroll_loan_type/subtypes/{$loanTypeName}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update payroll loan type for given id
     *
     * @param array $data payroll loan type informations
     * @param int $id Payroll Loan Type Id
     * @return \Illuminate\Http\JsonResponse Updated payroll loan type
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PATCH',
            "/payroll_loan_type/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if payroll loan type name is available
     *
     * @param int $companyId Company ID
     * @param array $data payroll loan type information
     * @return \Illuminate\Http\JsonResponse Availability of payroll loan type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/payroll_loan_type/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple payroll loan types
     *
     * @param array $data information on payroll loan types to delete
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/payroll_loan_type/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }
}
