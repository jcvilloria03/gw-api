<?php

namespace App\Announcement;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class AnnouncementAuditService
{
    const OBJECT_NAME = 'announcement';
    const ACTION_CREATE = 'create';
    const ACTION_SEND_REPLY = 'send_reply';
    const ACTION_RECIPIENT_SEEN = 'recipient_seen';
    const ACTION_RESPONSE_SEEN = 'response_seen';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Announcement related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_SEND_REPLY:
                $this->logSendReply($cacheItem);
                break;
            case self::ACTION_RECIPIENT_SEEN:
                $this->logRecipientSawAnnouncement($cacheItem);
                break;
            case self::ACTION_RESPONSE_SEEN:
                $this->logSenderSawAnnouncementResponse($cacheItem);
                break;
        }
    }

    /**
     * Create Announcement
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['announcement'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'announcement' => $data
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Send reply to Announcement
     *
     * @param array $cacheItem
     */
    public function logSendReply(array $cacheItem)
    {
        $data = json_decode($cacheItem['reply'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_SEND_REPLY,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'reply' => $data
            ]
        ]);
        $this->auditService->log($item);
    }
    /**
     * Recipient Saw Announcement
     *
     * @param array $cacheItem
     */
    public function logRecipientSawAnnouncement(array $cacheItem)
    {
        $data = json_decode($cacheItem['announcement_recipient'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_RECIPIENT_SEEN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'announcement_recipient' => $data
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Sender Saw Announcement Response
     *
     * @param array $cacheItem
     */
    public function logSenderSawAnnouncementResponse(array $cacheItem)
    {
        $data = json_decode($cacheItem['announcement_response'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_RESPONSE_SEEN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'announcement_response' => $data
            ]
        ]);
        $this->auditService->log($item);
    }
}
