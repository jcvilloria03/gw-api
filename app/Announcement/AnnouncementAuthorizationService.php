<?php

namespace App\Announcement;

use App\Common\CommonAuthorizationService;

class AnnouncementAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.announcement';
    public $createTask = 'create.announcement';
    public $updateTask = 'edit.announcement';
    public $deleteTask = 'delete.announcement';
    public $exportTask = 'export.announcement';

    /**
     * Authorize sender view task.
     *
     * @param \stdClass $model
     * @param array $user
     * @param \stdClass $role
     * @return bool
     */
    public function authorizeSenderView(\stdClass $model, array $user, \stdClass $role)
    {
        return $this->authorizeGet($model, $user) && $role->sender;
    }

    public function authorizeDownloadAnnouncements(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->exportTask);
    }
}
