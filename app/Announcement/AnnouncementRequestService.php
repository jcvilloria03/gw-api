<?php

namespace App\Announcement;

use GuzzleHttp\Psr7\Request;
use App\Request\DownloadRequestService;

/**
 * Class AnnouncementRequestService
 *
 * @package App\Announcement
 */
class AnnouncementRequestService extends DownloadRequestService
{
    /**
     * Call endpoint to get company announcements
     *
     * @param int   $companyId Company ID
     * @param array $query     Query
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompanyAnnouncements($companyId, array $query)
    {
        $queryString = empty($query) ? '' : '?' . http_build_query($query);

        return $this->send(
            new Request(
                'GET',
                "/company/{$companyId}/announcements" . $queryString
            )
        );
    }

    /**
     * Call endpoint to get an announcement.
     *
     * @param int $id     Announcement ID
     * @param int $userId User ID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id, int $userId)
    {
        return $this->send(new Request('GET', "/announcement/{$id}?user_id={$userId}"));
    }

    /**
     * Call endpoint to create an announcement.
     *
     * @param array $data Announcement data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(array $data)
    {
        return $this->send(
            new Request(
                'POST',
                '/announcement',
                [
                    'Content-Type' => 'application/json'
                ],
                json_encode($data)
            )
        );
    }

    /**
     * Call endpoint to download company announcements csv.
     *
     * @param int   $companyId Company ID
     * @param array $data      from request
     *
     * @return \Illuminate\Http\JsonResponse Announcement
     */
    public function downloadCompanyAnnouncementsCsv(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/announcements/download",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->download($request);
    }

    /**
     * Call endpoint to get role of given user for announcement
     *
     * @param int $announcementId Announcement ID
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRoleForAnnouncement($announcementId, int $userId)
    {
        return $this->send(new Request(
            'GET',
            "/announcement/{$announcementId}/user_role/{$userId}"
        ));
    }

    /**
     * Call an endpoint to reply to an announcement.
     *
     * @param int $announcementId
     * @param array $replyData
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply($announcementId, array $replyData)
    {
        return $this->send(new Request(
            'POST',
            "/announcement/{$announcementId}/reply",
            ['Content-Type' => 'application/json'],
            json_encode($replyData)
        ));
    }

    /**
     * Call an endpoint to reply to an announcement.
     *
     * @param int $announcementId
     * @param int $recipientId
     * @param string $ipAddress
     * @param string $domain
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAnnouncementAsSeen($announcementId, $recipientId, string $ipAddress, $domain = '')
    {
        return $this->send(new Request(
            'PUT',
            "/announcement/{$announcementId}/recipient/{$recipientId}/seen",
            ['Content-Type' => 'application/json'],
            json_encode([
                'ip_address' => $ipAddress,
                'domain' => $domain,
            ])
        ));
    }
}
