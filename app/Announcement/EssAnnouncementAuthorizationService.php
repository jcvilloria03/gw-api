<?php

namespace App\Announcement;

use App\Authorization\EssBaseAuthorizationService;

class EssAnnouncementAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.announcement';
    const CREATE_TASK = 'ess.create.announcement';

    /**
     * @param int $userId
     * @param \stdClass $role
     * @return bool
     */
    public function authorizeView(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }

    /**
     * @param int $userId
     * @param \stdClass $role
     * @return bool
     */
    public function authorizeSingleView(int $userId, \stdClass $role)
    {
        return $this->authorizeView($userId) && ($role->sender || $role->recipient);
    }

    /**
     * @param int $userId
     * @param \stdClass $role
     * @return bool
     */
    public function authorizeSenderView(int $userId, \stdClass $role)
    {
        return $this->authorizeView($userId) && $role->sender;
    }

    /**
     * @param int $userId
     * @param \stdClass $role
     * @return bool
     */
    public function authorizeRecipientView(int $userId, \stdClass $role)
    {
        return $this->authorizeView($userId) && $role->recipient;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeCreate(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::CREATE_TASK);
    }
}
