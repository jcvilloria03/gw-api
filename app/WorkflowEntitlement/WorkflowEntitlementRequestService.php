<?php

namespace App\WorkflowEntitlement;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class WorkflowEntitlementRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get workflow entitlement info
     *
     * @param int $id WorkflowEntitlement ID
     *
     * @return \Illuminate\Http\JsonResponse WorkflowEntitlement Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/workflow_entitlement/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to bulk create or update workflow entitlements
     *
     * @param array $data workflow entitlement information
     *
     * @return \Illuminate\Http\JsonResponse Created or updated Workflow Entitlements
     */
    public function bulkCreateOrUpdate(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            '/workflow_entitlement/bulk_create_or_update',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to bulk create or update employee workflow entitlements by employee id.
     *
     * @param string|integer $id   employee id
     * @param array          $data employee workflow entitlement information
     *
     * @return \Illuminate\Http\JsonResponse Created or updated Employee Workflow Entitlements
     */
    public function createOrUpdateEmployeeWorkflowEntitlements($id, array $data)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/workflow_entitlement/create_or_update",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all workflow entitlements within company
     *
     * @param int   $companyId Company Id
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse List of company workflow entitlements
     */
    public function getCompanyWorkflowEntitlements(int $companyId, array $data, AuthzDataScope $authzDataScope = null)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/workflow_entitlements",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        ), $authzDataScope);
    }

    /**
     * Call endpoint to get all employee workflow entitlements for given employee id
     *
     * @param int $employeeId Employee Id
     *
     * @return \Illuminate\Http\JsonResponse List of employee workflow entitlements
     */
    public function getEmployeeWorkflowEntitlements(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/workflow_entitlements"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update workflow entitlement for given id
     *
     * @param array $data workflow entitlement informations
     * @param int   $id   workflow entitlement Id
     *
     * @return \Illuminate\Http\JsonResponse Updated workflow entitlement
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            '/workflow_entitlement/' . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple workflow entitlements
     *
     * @param array $data workflow entitlements to delete informations
     *
     * @return \Illuminate\Http\JsonResponse Deleted workflow entitlements id's
     */
    public function bulkDelete(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            '/workflow_entitlement/bulk_delete',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to fetch Data to be saved in Workflow Entitlement upload job
     *
     * @param string $jobId Job ID
     *
     * @return \Illuminate\Http\JsonResponse Workflow Entitlement upload preview
     *
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/workflow_entitlement/upload_preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if saving Workflow Entitlement upload will reset pending requests.
     *
     * @param array $data Request data
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function checkUploadHasPendingRequests(array $data)
    {
        $queryString = ($query = http_build_query($data)) ? '?' . $query : '';

        $request = new Request(
            'GET',
            '/workflow_entitlement/upload/has_pending_requests' . $queryString
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if there are pending requests of any type.
     *
     * @param array $data Workflow entitlement information
     *
     * @return \Illuminate\Http\JsonResponse has_pending_requests: Boolean
     */
    public function checkAffectedEmployeesHavePendingRequests(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            '/workflow_entitlement/has_pending_requests',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to check if there are pending requests of any type.
     *
     * @param mixed $id   Employee ID
     * @param array $data Workflow entitlement information
     *
     * @return \Illuminate\Http\JsonResponse has_pending_requests: Boolean
     */
    public function checkEmployeeHasPendingRequests($id, array $data)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/workflow_entitlement/has_pending_requests",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if there are pending requests for inline update of assigned workflow.
     *
     * @param int   $id   Workflow ID
     * @param array $data Workflow entitlement information
     *
     * @return \Illuminate\Http\JsonResponse has_pending_requests: Boolean
     */
    public function checkAssignWorkflowInlineUpdateHasPendingRequests(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/workflow_entitlement/{$id}/has_pending_requests",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
