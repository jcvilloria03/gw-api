<?php

namespace App\WorkflowEntitlement;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class WorkflowEntitlementAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.workflow_entitlement';
    const CREATE_TASK = 'create.workflow_entitlement';
    const UPDATE_TASK = 'edit.workflow_entitlement';
    const DELETE_TASK = 'delete.workflow_entitlement';

    /**
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyWorkflowEntitlements(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $workflowEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $workflowEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflowEntitlement, $user, self::DELETE_TASK);
    }

    /**
     * Authorize workflowEntitlement related tasks
     *
     * @param \stdClass $workflowEntitlement
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $workflowEntitlement,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for workflowEntitlement account
            return $accountScope->inScope($workflowEntitlement->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as workflowEntitlement's account
                return $workflowEntitlement->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($workflowEntitlement->company_id);
        }

        return false;
    }
}
