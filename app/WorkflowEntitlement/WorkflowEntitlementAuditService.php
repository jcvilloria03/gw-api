<?php

namespace App\WorkflowEntitlement;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class WorkflowEntitlementAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_UPDATE = 'update';
    const ACTION_BATCH_UPDATE = 'batch_update';
    const ACTION_DELETE = 'delete';
    const ACTION_BATCH_DELETE = 'batch_delete';

    const OBJECT_NAME = 'workflow_entitlement';

    const ACTIONS = [
        'created' => self::ACTION_CREATE,
        'updated' => self::ACTION_UPDATE,
        'deleted' => self::ACTION_DELETE
    ];

    const ACTIONS_CREATE_OR_UPDATE = [
        'created',
        'updated'
    ];

    const ACTIONS_CREATE_OR_UPDATE_OR_DELETE = [
        'created',
        'updated',
        'deleted'
    ];

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log WorkflowEntitlement related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log WorkflowEntitlement create
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch create
     * @return void
     */
    public function logCreate(array $cacheItem, bool $isBatch = false)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE : self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'new' => $new
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log WorkflowEntitlement update
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch update
     * @return void
     */
    public function logUpdate(array $cacheItem, bool $isBatch = false)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $data = [
            'id' => $new['id'],
            'new' => $new
        ];

        if (!$isBatch) {
            $data['old'] = json_decode($cacheItem['old'], true);
        }

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_UPDATE : self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log WorkflowEntitlement Delete
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch update
     * @return void
     */
    public function logDelete(array $cacheItem, bool $isBatch = false)
    {
        $data = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_DELETE : self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data->id
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log upload Workflow Entitlements
     *
     * @param array $data Workflow entitlements data
     * @param array $user
     * @return void
     */
    public function logUploadedWorkflowEntitlements(array $data, array $user)
    {
        $this->logBatchCreate($data['created'], $user);
        $this->logBatchUpdate($data['updated'], $user);
        $this->logBatchDelete($data['deleted'], $user);
    }

    /**
     * Log batch create workflow entitlements
     *
     * @param array $workflowEntitlements Workflow Entitlements
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $workflowEntitlements, array $user)
    {
        foreach ($workflowEntitlements as $workflowEntitlement) {
            try {
                $this->logCreate([
                    'new' => json_encode($workflowEntitlement),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                $message = $e->getMessage()
                    . ' : Could not audit Workflow Entitlement batch create with id = ' . $workflowEntitlement['id'];
                \Log::error($message);
            }
        }
    }

    /**
     * Log batch update workflow entitlements
     *
     * @param array $data workflow entitlements data
     * @param array $user
     * @return void
     */
    public function logBatchUpdate(array $workflowEntitlements, array $user)
    {
        foreach ($workflowEntitlements as $workflowEntitlement) {
            try {
                $this->logUpdate([
                    'new' => json_encode($workflowEntitlement),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                $message = $e->getMessage()
                    . ' : Could not audit Workflow Entitlement batch update with id = ' . $workflowEntitlement['id'];
                \Log::error($message);
            }
        }
    }

    /**
     * Log batch delete workflow entitlements
     *
     * @param array $workflowEntitlements Workflow Entitlements
     * @param array $user
     * @return void
     */
    public function logBatchDelete(array $workflowEntitlements, array $user)
    {
        foreach ($workflowEntitlements as $workflowEntitlement) {
            try {
                $this->logDelete([
                    'old' => json_encode($workflowEntitlement),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                $message = $e->getMessage()
                    . ' : Could not audit Workflow Entitlement batch delete with id = ' . $workflowEntitlement['id'];
                \Log::error($message);
            }
        }
    }
}
