<?php

namespace App\EventStream;

use Illuminate\Support\Facades\Log;
use Salarium\LumenEventStreamMQ\Event;
use Salarium\LumenEventStreamMQ\Contracts\EventHandlerContract;
use App\Subscriptions\SubscriptionsRequestService;
use App\Model\Role;
use App\Company\PhilippineCompanyRequestService;
use App\Company\CompanyRequestService;
use App\Holiday\HolidayRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Role\DefaultRoleService;
use App\DemoCompany\DemoCompanyRequestService;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */

class DemoCompanyCreatedEventHandler implements EventHandlerContract
{
    /**
     * @var \App\Company\PhilippineCompanyRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    private $subscriptionsRequestService;

    /**
     * @var \App\Role\DefaultRoleService
     */
    private $defaultRoleService;

    /**
     * @var \App\DemoCompany\DemoCompanyRequestService
     */
    protected $demoCompanyRequestService;

    public function __construct(
        PhilippineCompanyRequestService $requestService,
        SubscriptionsRequestService $subscriptionsRequestService,
        DefaultRoleService $defaultRoleService,
        DemoCompanyRequestService $demoCompanyRequestService
    ) {
        $this->requestService = $requestService;
        $this->subscriptionsRequestService = $subscriptionsRequestService;
        $this->defaultRoleService = $defaultRoleService;
        $this->demoCompanyRequestService = $demoCompanyRequestService;
    }

    /**
     * Event handling logic for democompany.created
     *
     * create demo account upon signup
     *
     * @param \Salarium\LumenEventStreamMQ\Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $eventData = $event->getData();

        Log::info(
            get_class($this) . '::handle called. trigger DemoCompanyCreatedEventHandler',
            $eventData
        );

        if (
            !empty($eventData['account_id'])
            && !empty($eventData['user_id'])
        ) {
            // Call subscription api before creating
            $customerResponse = $this->subscriptionsRequestService
                ->getCustomerByAccountId($eventData['account_id'], ['plan']);

            $customerData         = json_decode($customerResponse->getData(), true);
            $accountSubscription  = $customerData['data'][0]['subscriptions'][0] ?? null;

            $productCode = $accountSubscription['plan']['main_product']['code'];
            $accountId = $eventData['account_id'];

            // call microservice
            Log::info('[DemoCompanyCreatedEventHandler] createCompanyDemo account id: ' . $accountId);
            $response = $this->requestService->createCompanyDemo([
                'account_id' => $eventData['account_id'],
                'name'       => $eventData['name'],
                'type'       => $eventData['type'],
                'tin'        => $eventData['tin'],
                'rdo'        => $eventData['rdo'],
                'sss'        => $eventData['sss'],
                'hdmf'       => $eventData['hdmf'],
                'philhealth' => $eventData['philhealth']
            ]);

            // if there are validation errors in the create request, display these errors
            if ($response->getStatusCode() !== Response::HTTP_CREATED) {
                return $response;
            }

            $responseData = json_decode($response->getBody(), true);
            $demoCompanyId = $responseData['id'];

            // trigger demo company process
            try {
                Log::info('[DemoCompanyCreatedEventHandler]triggerCompanyDemo company id: ' . $demoCompanyId);
                $this->requestService->triggerCompanyDemo($demoCompanyId);
            } catch (\Exception $e) {
                Log::error('[DemoCompanyCreatedEventHandler]triggerCompanyDemo Error for companyId: ' . $demoCompanyId);
                Log::error($e->getMessage() . '|' . $e->getTraceAsString());
            }

            // create admin role for company
            try {
                Log::info('[DemoCompanyCreatedEventHandler] createAdminRole company id: ' . $demoCompanyId);
                $companyResponse = $this->requestService->get($responseData['id']);
                $companyData = json_decode($companyResponse->getData(), true);
                $this->defaultRoleService->createAdminRole(
                    $responseData['account_id'],
                    $companyData,
                    $this->buildModuleAccessFromSubscriptionProductCode($productCode)
                );
            } catch (\Exception $e) {
                Log::error('[DemoCompanyCreatedEventHandler] createAdminRole Error for company id: ' . $demoCompanyId);
                Log::error($e->getMessage() . '|' . $e->getTraceAsString());
            }

            // create employee role for company
            try {
                Log::info('[DemoCompanyCreatedEventHandler] createCompanyEmployeeRole company id: ' . $demoCompanyId);
                $this->defaultRoleService->createCompanyEmployeeRole(
                    $responseData['account_id'],
                    $companyData,
                    $this->buildModuleAccessFromSubscriptionProductCode($productCode)
                );
            } catch (\Exception $e) {
                Log::error('[DemoCompanyCreatedEventHandler] createCompanyEmployeeRole Error for company id: ' .
                    $demoCompanyId);
                Log::error($e->getMessage() . '|' . $e->getTraceAsString());
            }

            // create default holidays
            try {
                Log::info('[DemoCompanyCreatedEventHandler] createCompanyDefaultHolidays companyId: ' . $demoCompanyId);
                $holidayRequestService = app()->make(HolidayRequestService::class);
                $holidayRequestService->createCompanyDefaultHolidays($demoCompanyId);
            } catch (\Exception $e) {
                Log::error('[DemoCompanyCreatedEventHandler] createCompanyDefaultHolidays Error for company id: ' .
                    $demoCompanyId);
                Log::error($e->getMessage() . '|' . $e->getTraceAsString());
            }
        } else {
            Log::info(
                get_class($this) . '::handle called. DemoCompanyCreatedEventHandler Invalid Parameters',
                $eventData
            );
        }
    }

    /**
     * Convert subscription product code to list of module accesses
     * allowed for that product
     *
     * @param string $productCode
     * @return array
     */
    private function buildModuleAccessFromSubscriptionProductCode(string $productCode)
    {
        $mapping = [
            'time_and_attendance'              => [Role::ACCESS_MODULE_HRIS, Role::ACCESS_MODULE_TA],
            'payroll'                          => [Role::ACCESS_MODULE_HRIS, Role::ACCESS_MODULE_PAYROLL],
            'time_and_attendance_plus_payroll' => [
                Role::ACCESS_MODULE_HRIS,
                Role::ACCESS_MODULE_TA,
                Role::ACCESS_MODULE_PAYROLL
            ],
        ];

        $formattedProductName = str_ireplace(' ', '_', $productCode);

        return $mapping[$formattedProductName] ?? Role::DEFAULT_MODULE_ACCESSES;
    }
}
