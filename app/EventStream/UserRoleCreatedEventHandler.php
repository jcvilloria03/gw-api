<?php

namespace App\EventStream;

use App\Model\Role;
use App\Role\RoleService;
use App\Role\UserRoleService;
use App\Authn\AuthnService;
use App\Model\AuthnUser;
use App\Role\DefaultRoleService;
use Illuminate\Support\Facades\Log;
use Salarium\LumenEventStreamMQ\Event;
use App\Authorization\AuthorizationService;
use Salarium\LumenEventStreamMQ\Contracts\EventHandlerContract;
use Salarium\LumenEventStreamMQ\Exceptions\EventStreamException;
/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */

class UserRoleCreatedEventHandler implements EventHandlerContract
{
    /**
     * @var \App\Role\RoleService
     */
    protected $roleService;

    public function __construct(
        AuthorizationService $authorizationService,
        RoleService $roleService
    ) {
        $this->authorizationService = $authorizationService;
        $this->roleService = $roleService;
    }

    /**
     * Event handling logic for license.applied event type
     *
     * Update user_roles.module_access based on license's product name
     *
     * @param \Salarium\LumenEventStreamMQ\Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $eventData = $event->getData();

        Log::info(
            get_class($this).'::handle called. Update UserRoles By Account Id',
            $eventData
        );

        if (!empty($eventData['user_ids'])
            && !empty($eventData['owner_user_id'])
            && !empty($eventData['product_code'])
        ) {
            $authnService = app()->make(AuthnService::class);
            $defaultRoleService = app()->make(DefaultRoleService::class);
            foreach ($eventData['user_ids'] as $userId) {
                $authnUser = $authnService->getUser($userId);
                if (empty($authnUser)) {
                    $authnUser = (object) array('user_id' => $userId);
                }
                $employeeDefaultRole = $defaultRoleService->getEmployeeSystemRole(
                    $eventData['account_id'],
                    $eventData['company_id']
                );
                $this->assignRoleToUser(
                    $authnUser,
                    $employeeDefaultRole
                );
            }
        } else {
            Log::info(
                get_class($this).'::handle called. UserRoleCreatedEventHandler Invalid Parameters',
                $eventData
            );
        }
    }

    /**
     * Assigns given role to user if not yet assigned
     *
     * @param  \App\Model\AuthnUser $authnUser
     * @param  \App\Model\Role      $role
     * @return void
     */
    private function assignRoleToUser($authnUser, $role)
    {
        $userRoleService = app()->make(UserRoleService::class);
        // Check if employee default role is not yet set for user
        $userRoles = $userRoleService->getWithConditions([
            ['user_id', $authnUser->user_id],
            ['role_id', $role->id]
        ]);
        if ($userRoles->isEmpty()) {
            $userRoleService->create([
                'role_id' => $role->id,
                'user_id' => $authnUser->user_id,
            ]);
        }
    }


}
