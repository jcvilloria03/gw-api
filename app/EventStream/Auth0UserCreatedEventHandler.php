<?php

namespace App\EventStream;

use App\Model\Auth0User;
use App\Auth0\Auth0UserService;
use App\Auth0\Auth0ManagementService;
use App\User\UserRequestService;
use App\CompanyUser\CompanyUserService;
use App\Role\RoleRequestService;
use Illuminate\Support\Facades\Log;
use Salarium\LumenEventStreamMQ\Event;
use App\Authorization\AuthorizationService;
use Salarium\LumenEventStreamMQ\Contracts\EventHandlerContract;
use App\Http\Controllers\Concerns\ProcessesEmployeeActiveStatusUpdatesTrait;
use App\Http\Controllers\Concerns\ProcessesEmployeeEmailUpdatesTrait;
use App\Cache\FragmentedRedisCacheService;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */

class Auth0UserCreatedEventHandler implements EventHandlerContract
{
    use ProcessesEmployeeEmailUpdatesTrait,
        ProcessesEmployeeActiveStatusUpdatesTrait;

    const CACHE_BUCKET_PREFIX = 'user-status';
    const CACHE_FIELD_PREFIX = 'id';

    /**
     * @var \App\Auth0\AuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Auth0\Auth0UserService
     */
    protected $auth0Service;

    /**
     * @var App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * @var App\Role\RoleRequestService
     */
    protected $roleRequestService;

    /**
     * @var \App\Cache\FragmentedRedisCacheService
     */
    protected $cacheService;

    public function __construct(
        AuthorizationService $authorizationService,
        Auth0UserService $auth0Service,
        RoleRequestService $roleRequestService,
        UserRequestService $userRequestService,
        FragmentedRedisCacheService $cacheService
    ) {
        $this->authorizationService = $authorizationService;
        $this->auth0Service         = $auth0Service;
        $this->roleRequestService   = $roleRequestService;
        $this->userRequestService   = $userRequestService;
        $this->cacheService         = $cacheService;
    }

    /**
     * Event handling logic for license.applied event type
     *
     * Update user_roles.module_access based on license's product name
     *
     * @param \Salarium\LumenEventStreamMQ\Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $eventData = $event->getData();

        Log::info(
            get_class($this) . '::handle called. Update User Auth0',
            $eventData
        );

        if (
            !empty($eventData['employees'])
        ) {
            $companyUserService = app()->make(CompanyUserService::class);

            foreach ($eventData['employees'] as $employee) {
                $isEmailUpdate = $employee['update_email'] ?? false;
                $userId = $employee['user_id'];
                $auth0User = $this->auth0Service->getUser($userId);
                if ($employee['is_update'] === false) {
                    if (empty($auth0User)) {
                        // create preactive auth0 record
                        $auth0User = $this->auth0Service->create([
                            'auth0_user_id' => Auth0UserService::generatePreactiveId(
                                $employee['account_id'],
                                $userId
                            ),
                            'user_id'       => $userId,
                            'account_id'    => $employee['account_id'],
                            'status'        => $employee['status']
                        ]);
                    }

                    $companyUser = $companyUserService->getByUserAndCompanyId($userId, $employee['company_id']);
                    if (empty($companyUser)) {
                        $companyUser = $companyUserService->create([
                            'user_id'     => $userId,
                            'company_id'  => $employee['company_id'],
                            'employee_id' => $employee['id']
                        ]);
                    } else {
                        $companyUser = $companyUserService->update($userId, $employee['company_id'], [
                            'employee_id' => $employee['id']
                        ]);
                    }

                    if (isset($employee['status']) && in_array($employee['status'], ['active', "semi-active"])) {
                        $this->processEmployeeActiveStatus(
                            [
                                'id'              => $employee['id'],
                                'email'           => $employee['email'],
                                'first_name'      => $employee['first_name'],
                                'last_name'       => $employee['last_name'],
                                'middle_name'     => $employee['middle_name'],
                                'account_id'      => $employee['account_id'],
                                'company_id'      => $employee['company_id'],
                                'employee_id'     => $employee['id'],
                                'user_id'         => $userId
                            ],
                            true,
                            $auth0User
                        );
                    }
                } else {
                    if ($isEmailUpdate) {
                        $employeeData = [
                            'id'         => $employee['id'],
                            'email'      => $employee['email'],
                            'account_id' => $employee['account_id'],
                            'company_id' => $employee['company_id'],
                            'user_id'    => $userId
                        ];

                        if ($employee['status'] === 'active') {
                            $companyUserService = app()->make(CompanyUserService::class);
                            $companyUser = $companyUserService->getByUserAndCompanyId($userId, $employee['company_id']);
                            if (empty($companyUser)) {
                                $companyUser = $companyUserService->create([
                                    'user_id'     => $userId,
                                    'company_id'  => $employee['company_id'],
                                    'employee_id' => $employee['id']
                                ]);
                            } else {
                                $companyUser = $companyUserService->update($userId, $employee['company_id'], [
                                    'employee_id' => $employee['id']
                                ]);
                            }
                            $this->userRequestService->setStatus($userId, 'inactive');
                            $this->processEmployeeEmailUpdate($employeeData, ['email' => $employee['email']]);
                            $this->setUserStatus($userId, 'active', $auth0User, $employee['id']);
                            $this->userRequestService->setStatus($userId, 'active');
                        } else {
                            $this->processEmployeeEmailUpdate($employeeData, ['email' => $employee['email']]);
                        }
                    }
                }
            }
        } else {
            Log::info(
                get_class($this) . '::handle called. Auth0UserCreatedEventHandler Invalid Parameters',
                $eventData
            );
        }
    }

    /**
     * set user status
     *
     * @return void
     */
    public function setUserStatus(
        $id,
        $status,
        $auth0User,
        $employeeId
    ) {
        $inputs = [];

        $this->cacheService->setPrefix(static::CACHE_BUCKET_PREFIX);
        $this->cacheService->setHashFieldPrefix(static::CACHE_FIELD_PREFIX);

        if ($this->cacheService->exists($id)) {
            $this->cacheService->delete($id);
        }
        array_push($inputs, $status);
        // validate permissions related requests
        $user = $this->auth0Service->getUser($id);

        if (!$user) {
            Log::info(
                get_class($this) . '::handle called. User not found'
            );
        }

        // don't allow inactivating of first OWNER
        $userResponse = $this->userRequestService->get($id);
        $userData = json_decode($userResponse->getData(), true);

        $accountId = $this->auth0Service->getUser($id)->account_id;
        $activeUserIds = $this->auth0Service->getActiveUserIds($accountId);

        // call microservice to validate new status
        $isValidStatusResponse = $this->userRequestService->validateStatus(
            $id,
            array_merge(
                ['status' => $status],
                ['active_user_ids' => $activeUserIds]
            )
        );

        if (!$isValidStatusResponse->isSuccessful()) {
            return $isValidStatusResponse;
        }

        $userRole = $this->roleRequestService->getRoleByAccount(
            $userData['account_id'],
            $userData['role']['id'],
            true
        );

        $userRoleType = $this->roleRequestService->getRoleType($userRole['data']);

        $isEmployee = !empty(array_filter(array_column($userData['companies'], 'employee_id')));

        // update user status
        $response = $this->userRequestService->setStatus($id, $status);

        if (!$response->isSuccessful()) {
            return $response;
        }

        // If user type is employee, use ProcessesEmployeeActiveStatusUpdatesTrait
        if ($userRoleType === RoleRequestService::ROLE_EMPLOYEE || $isEmployee) {
            $this->processEmployeeActiveStatus(
                [
                    'id'              => $employeeId,
                    'email'           => $userData['email'],
                    'first_name'      => $userData['first_name'],
                    'last_name'       => $userData['last_name'],
                    'middle_name'     => $userData['middle_name'],
                    'account_id'      => $userData['account']['id'],
                    'company_id'      => $userData['companies'][0]['id'],
                    'employee_id'     => $employeeId
                ],
                ($status === Auth0User::STATUS_ACTIVE),
                $user
            );
        }

        $auth0ManagementService = app()->make(Auth0ManagementService::class);
        $userAuth0Profile = $auth0ManagementService->getAuth0UserProfile($user->auth0_user_id);

        if ($userAuth0Profile !== null && $status == 'active') {
            if (!$userAuth0Profile['email_verified']) {
                $auth0ManagementService->verifyEmail($auth0User->auth0_user_id);
            }
        }

        $userUpdated = $user->fresh()->toArray();

        return $userUpdated;
    }
}
