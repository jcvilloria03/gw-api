<?php

namespace App\EventStream;

use App\Model\Role;
use App\Role\UserRoleService;
use Illuminate\Support\Facades\Log;
use Salarium\LumenEventStreamMQ\Event;
use App\Authorization\AuthorizationService;
use Salarium\LumenEventStreamMQ\Contracts\EventHandlerContract;
use Salarium\LumenEventStreamMQ\Exceptions\EventStreamException;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */

class LicenseAppliedEventHandler implements EventHandlerContract
{

    const LICENSE_MODULE_MAPPING = [
        'hris'                             => [Role::ACCESS_MODULE_HRIS],
        'time_and_attendance'              => [Role::ACCESS_MODULE_TA],
        'payroll'                          => [Role::ACCESS_MODULE_PAYROLL],
        'time_and_attendance_plus_payroll' => [
            Role::ACCESS_MODULE_TA,
            Role::ACCESS_MODULE_PAYROLL
        ],
    ];

    /**
     * @var \App\Role\UserRoleService
     */
    protected $userRoleService;

    public function __construct(
        UserRoleService $userRoleService,
        AuthorizationService $authorizationService
    ) {
        $this->userRoleService = $userRoleService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * Event handling logic for license.applied event type
     *
     * Update user_roles.module_access based on license's product name
     *
     * @param \Salarium\LumenEventStreamMQ\Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $eventData = $event->getData();

        if (empty($eventData['user_ids']) || empty($eventData['product_name'])) {
            throw new EventStreamException(
                'Incomplete event data for '
                . $event->getName()
                .' event.'
            );
        }

        //Update module access by user id/s
        Log::info(get_class($this).'::handle called. Update Role By User Id/s', $eventData);
        if (isset($eventData['user_ids']) && is_array($eventData['user_ids']) && count($eventData['user_ids'])) {
            foreach ($eventData['user_ids'] as $userId) {
                if (!empty($userId)) {
                    $userId = (int) $userId;
                    $this->userRoleService->updateUserRolesModuleAccess(
                        $userId,
                        $this->getMappedModuleAccess($eventData['product_name'])
                    );
                    $this->authorizationService->invalidateCachedPermissions($userId);
                    $this->authorizationService->buildUserPermissions($userId);
                }
            }
        }


    }

    /**
     * Get mapped module access for given product name
     *
     * @param string $productName
     * @return array
     */
    protected function getMappedModuleAccess($productName) : array
    {
        $formattedProductName = str_ireplace(' ', '_', $productName);

        if (!array_key_exists($formattedProductName, self::LICENSE_MODULE_MAPPING)) {
            throw new EventStreamException(
                "Product to module access mapping not found for {$formattedProductName}."
            );
        }
        return self::LICENSE_MODULE_MAPPING[$formattedProductName];
    }
}
