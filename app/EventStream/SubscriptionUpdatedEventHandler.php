<?php

namespace App\EventStream;

use App\Model\Role;
use App\Role\UserRoleService;
use App\Role\RoleService;
use Illuminate\Support\Facades\Log;
use Salarium\LumenEventStreamMQ\Event;
use App\Authorization\AuthorizationService;
use Salarium\LumenEventStreamMQ\Contracts\EventHandlerContract;
use Salarium\LumenEventStreamMQ\Exceptions\EventStreamException;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */

class SubscriptionUpdatedEventHandler implements EventHandlerContract
{
    const LICENSE_MODULE_MAPPING = [
        'hris'                             => [Role::ACCESS_MODULE_HRIS],
        'time_and_attendance'              => [Role::ACCESS_MODULE_TA],
        'payroll'                          => [Role::ACCESS_MODULE_PAYROLL],
        'time_and_attendance_plus_payroll' => [
            Role::ACCESS_MODULE_TA,
            Role::ACCESS_MODULE_PAYROLL
        ],
    ];

    /**
     * @var \App\Role\UserRoleService
     */
    protected $userRoleService;

    /**
     * @var \App\Role\RoleService
     */
    protected $roleService;

    public function __construct(
        UserRoleService $userRoleService,
        AuthorizationService $authorizationService,
        RoleService $roleService
    ) {
        $this->userRoleService = $userRoleService;
        $this->authorizationService = $authorizationService;
        $this->roleService = $roleService;
    }

    /**
     * Event handling logic for license.applied event type
     *
     * Update user_roles.module_access based on license's product name
     *
     * @param \Salarium\LumenEventStreamMQ\Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $eventData = $event->getData();
        if (empty($eventData['account_id']) || empty($eventData['product_name'])) {
            throw new EventStreamException(
                'Incomplete event data for '
                . $event->getName()
                .' event.'
            );
        }

        //This updates all employees user roles under the account id
        if (!empty($eventData['update_employees'])) {
            //Update module access of all employees by account Id
            if (isset($eventData['account_id']) && !empty($eventData['product_name'])) {
                Log::info(
                    get_class($this).'::handle called. Update UserRoles By Account Id',
                    $eventData
                );
                $usersUpdated = $this->userRoleService->updateUserRolesByAccountId(
                    $eventData['account_id'],
                    $this->getMappedModuleAccess(
                        $eventData['product_name']
                    ),
                    $eventData['owner_id']
                );
                //Resync roles on cache
                Log::info(
                    get_class($this).'::handle called. Resync cache for all employees under account id',
                    $eventData
                );
                foreach ($usersUpdated as $userId) {
                    $this->authorizationService->invalidateCachedPermissions($userId);
                    $this->authorizationService->buildUserPermissions($userId);
                }
            }
        }

        //Update account's role
        if (!empty($eventData['include_roles']) && !empty($eventData['account_id'])) {
            Log::info(get_class($this).'::handle called. Update Roles By Account Id', $eventData);
            //Batch update roles
            $this->userRoleService->updateRolesByAccountId(
                $eventData['account_id'],
                $this->getMappedModuleAccess(
                    $eventData['product_name']
                )
            );
        }

        //Invalidate Cache
        $this->authorizationService->invalidateCachedPermissions($eventData['owner_id']);
        $this->authorizationService->invalidateUsersCachedPermissions(
            $this->userRoleService->getUsersByAccountId($eventData['account_id'])
        );

    }
 
    /**
     * Get mapped module access for given product name
     *
     * @param string $productName
     * @return array
     */
    protected function getMappedModuleAccess($productName) : array
    {
        $formattedProductName = str_ireplace(' ', '_', $productName);

        if (!array_key_exists($formattedProductName, self::LICENSE_MODULE_MAPPING)) {
            throw new EventStreamException(
                "Product to module access mapping not found for {$formattedProductName}."
            );
        }

        return self::LICENSE_MODULE_MAPPING[$formattedProductName];
    }
}
