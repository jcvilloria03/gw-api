<?php

namespace App\Helpers;

class ArrayHelper
{
    /**
     * Returns the difference on two multidimensional array
     *
     * @param $array1
     * @param $array2
     * @return mixed
     */
    public function arrayDiffAssocRecursive($array1, $array2)
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key])) {
                    $difference[$key] = $value;
                } else if (!is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $newDiff = $this->arrayDiffAssocRecursive($value, $array2[$key]);
                    if ($newDiff != false) {
                        $difference[$key] = $newDiff;
                    }
                }
            } else if (!array_key_exists($key, $array2) || $array2[$key] != $value) {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? 0 : $difference;
    }
}
