<?php

namespace App\Deduction;

use App\Common\CommonAuthorizationService;

class DeductionAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.deductions';
    public $createTask = 'create.deductions';
    public $updateTask = 'edit.deductions';
    public $deleteTask = 'delete.deductions';
}
