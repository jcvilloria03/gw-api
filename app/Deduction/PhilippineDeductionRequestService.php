<?php

namespace App\Deduction;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineDeductionRequestService extends RequestService
{
    /**
     * Call endpoint to assign deductions
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/deduction/bulk_create",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update deduction.
     *
     * @param int $id Deduction ID
     * @param array $data Request data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, array $data, AuthzDataScope $dataScope = null)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/deduction/{$id}",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        ), $dataScope);
    }
}
