<?php

namespace App\Request;

use App\Authz\AuthzDataScope;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class DownloadRequestService extends RequestService
{
    /**
     * GuzzleHttp\Client
     */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Add Authz Data Scope to query params of the request
     *
     * @param Request        $request        Request object
     * @param AuthzDataScope $authzDataScope AuthzDataScope object
     */
    protected function addAuthzDataScopeToRequest(Request $request, AuthzDataScope $authzDataScope)
    {
        $dataScope = $authzDataScope->getDataScope();

        $newUri = Uri::withQueryValue(
            $request->getUri(),
            'authz_data_scope',
            json_encode($dataScope)
        );

        return $request->withUri($newUri);
    }

    protected function download(Request $request, AuthzDataScope $authzDataScope = null)
    {

        if ($authzDataScope !== null) {
            $request = $this->addAuthzDataScopeToRequest($request, $authzDataScope);
        }

        try {
            $psrResponse = $this->client->send($request);
            $httpFoundationFactory = new HttpFoundationFactory();
            $response = $httpFoundationFactory->createResponse($psrResponse);

            return $response;
        } catch (BadResponseException $e) {
            // we catch 400 and 500 errors from the micro-service, then return as HttpExceptions
            $contents = json_decode($e->getResponse()->getBody()->getContents());
            if ($contents && isset($contents->status_code) && isset($contents->message)) {
                throw new HttpException($contents->status_code, $contents->message, $e);
            } elseif ($contents && $e->getResponse()->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
                return response()->json($contents, $e->getResponse()->getStatusCode());
            }
            $response = $e->getResponse();
            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }
}
