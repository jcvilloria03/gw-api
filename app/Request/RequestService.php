<?php

namespace App\Request;

use App\Authz\AuthzDataScope;
use App\Tracker\Tracker;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class RequestService
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var Tracker
     */
    protected $tracker;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function setTracker(Tracker $tracker): self
    {
        $this->tracker = $tracker;

        return $this;
    }

    protected function send(Request $baseRequest, AuthzDataScope $authzDataScope = null)
    {
        $request = $baseRequest->withAddedHeader('X-From-Gateway-Api', 1);

        if ($authzDataScope !== null) {
            $request = $this->addAuthzDataScopeToRequest($request, $authzDataScope);
        }

        try {
            $options = [];
            if ($this->tracker instanceof Tracker) {
                $options = [
                    'headers' => [
                        $this->tracker->getHeaderTrackingName() => $this->tracker->getTrackingId(),
                        $this->tracker->getHeaderTrackingSourceName() => $this->tracker->getTrackingSource(),
                        'X-Forwarded-For' => implode(', ', $this->tracker->getRequestIps()),
                    ]
                ];
            }

            $response = $this->client->send($request, $options);

            return response()->json($response->getBody()->getContents(), $response->getStatusCode());
        } catch (BadResponseException $e) {
            // we catch 400 and 500 errors from the micro-service, then return as HttpExceptions;
            $contents = json_decode($e->getResponse()->getBody()->getContents());
            if ($contents && (isset($contents->status_code) || isset($contents->status)) && isset($contents->message)) {
                $statusCode = isset($contents->status) ? $contents->status : $contents->status_code;
                throw new HttpException($statusCode, $contents->message, $e);
            } elseif (
                $contents &&
                collect([
                    Response::HTTP_BAD_REQUEST,
                    Response::HTTP_NOT_ACCEPTABLE
                ])->contains($e->getResponse()->getStatusCode())
            ) {
                return response()->json($contents, $e->getResponse()->getStatusCode());
            }
            $response = $e->getResponse();
            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Add Authz Data Scope to query params of the request
     *
     * @param Request        $request        Request object
     * @param AuthzDataScope $authzDataScope AuthzDataScope object
     */
    protected function addAuthzDataScopeToRequest(Request $request, AuthzDataScope $authzDataScope)
    {
        $dataScope = $authzDataScope->getDataScope();

        if (array_key_exists("STATUS", $authzDataScope->dataScopeFilter) ||
            array_key_exists("LOCATION", $authzDataScope->dataScopeFilter) ||
            array_key_exists("DEPARTMENT", $authzDataScope->dataScopeFilter) ||
            array_key_exists("PAYROLL_GROUP", $authzDataScope->dataScopeFilter)) {
            $dataScope = $authzDataScope->getDataScopeFilter();
        }

        $newUri = Uri::withQueryValue(
            $request->getUri(),
            'authz_data_scope',
            json_encode($dataScope)
        );

        return $request->withUri($newUri);
    }

    /**
      * The generic handler for exceptions
      *
      * @param BadResponseException $e
      * @return mixed
      */
    private function handleErrors(BadResponseException $e)
    {
        // we catch 400 and 500 errors from the micro-service, then return as HttpExceptions
        $contents = json_decode($e->getResponse()->getBody()->getContents());
        if ($contents && isset($contents->status_code) && isset($contents->message)) {
            throw new HttpException($contents->status_code, $contents->message, $e);
        } elseif (
            $contents &&
            collect([
                Response::HTTP_BAD_REQUEST,
                Response::HTTP_NOT_ACCEPTABLE
            ])->contains($e->getResponse()->getStatusCode())
        ) {
            return response()->json($contents, $e->getResponse()->getStatusCode());
        }
        $response = $e->getResponse();
        throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
    }

    /**
      * The send helper for multipart requests
      *
      * @param string $method The http method used
      * @param string $uri The http uri route.
      * @param array $company The multipart data to send
      * @return mixed
      */
    protected function sendMultipart(string $method, string $uri, array $company)
    {
        $params = [];
        foreach ($company as $key => $param) {
            if ($key == 'logo') {
                if (empty($param) || strtolower($param) == 'null') {
                    continue;
                }
                $params[] = [
                    "name"      => $key,
                    "contents"  => file_get_contents($company["logo"]->getRealPath()),
                    "filename"  => "temp_" . uniqid()
                ];
            } elseif ($key == 'picture') {
                if (empty($param) || strtolower($param) == 'null') {
                    continue;
                }
                $params[] = [
                    "name"      => $key,
                    "contents"  => file_get_contents($company["picture"]->getRealPath()),
                    "filename"  => "temp_" . uniqid()
                ];
            } else {
                $params[] = [
                    "name"     => $key,
                    "contents" => $param
                ];
            }
        }

        try {
            return $this->client->request(
                $method,
                $uri,
                [
                    'multipart' => $params,
                ]
            );
        } catch (BadResponseException $e) {
            return $this->handleErrors($e);
        }
    }
}
