<?php

namespace App\CompanyUser;

use App\Model\CompanyUser;
use App\User\UserRequestService;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CompanyUserService
{
    /**
     * @var \App\User\UserRequestService;
     */
    private $userRequestService;

    public function __construct(UserRequestService $userRequestService)
    {
        $this->userRequestService = $userRequestService;
    }

    /**
     *
     * Create CompanyUser in Gateway DB
     *
     * @param array $attributes
     * @return CompanyUser object
     */
    public function create(array $attributes)
    {
        return CompanyUser::create($attributes);
    }

    /**
     *
     * Update CompanyUser in Gateway DB
     *
     * @param array $attributes
     * @return CompanyUser object
     */
    public function update(int $userId, int $companyId, array $attributes)
    {
        CompanyUser::where([
            ['user_id', '=', $userId],
            ['company_id', '=', $companyId]
        ])->update($attributes);

        return CompanyUser::where('user_id', $userId)->where('company_id', $companyId)->first();
    }

    /**
     *
     * Get CompanyUser by employee id
     *
     * @param int $employeeId
     * @return CompanyUser object
     */
    public function getByEmployeeId(int $employeeId)
    {
        return CompanyUser::where('employee_id', $employeeId)->first();
    }

    /**
     *
     * Get CompanyUser by user id
     *
     * @param int $userId
     * @return CompanyUser object
     */
    public function getByUserId(int $userId)
    {
        return CompanyUser::where('user_id', $userId)->first();
    }

    /**
     *
     * Get all CompanyUser by user id
     *
     * @param int $userId
     * @return CompanyUser object
     */
    public function getAllByUserId(int $userId)
    {
        $companiesUsers = null;
        $cacheKey = $this->getCacheKey('*', $userId);
        if (!$cacheKey) {
            $companiesUsers = $this->getUserCompaniesRelations($userId);
        } else {
            $companiesUsers = $this->getFromCache($cacheKey);
            if ($companiesUsers->isEmpty()) {
                $this->sync($userId);
                $companiesUsers = $this->getFromCache($cacheKey);
            }
        }

        return collect($companiesUsers);
    }

    public function getUserFromTable(int $userId)
    {
        $companiesUsers = CompanyUser::where('user_id', $userId)->get();

        if ($companiesUsers) {
            $this->storeInCache(
                $companiesUsers,
                '*',
                $userId
            );
        }

        return $companiesUsers;
    }

    /**
     *
     * Sync company users
     *
     * @param int $userId
     * @return void
     */
    public function sync(int $userId)
    {
        $relations = array_map(function ($relation) {
            return [
                'user_id' => Arr::get($relation, 'user_id'),
                'company_id' => Arr::get($relation, 'company_id'),
                'employee_id' => Arr::get($relation, 'employee_id'),
            ];
        }, $this->getUserCompaniesRelations($userId));

        if (!empty($relations)) {
            CompanyUser::where('user_id', $userId)->delete();
            CompanyUser::insert($relations);

            $cacheKey = $this->getCacheKey('*', $userId);
            if ($cacheKey) {
                $this->deleteFromCache($cacheKey);
                $this->getAllByUserId($userId);
            }
        }
    }

    /**
     * Call an endpoint to get a user companies relations
     *
     * @param int $userId
     * @return array
     */
    public function getUserCompaniesRelations(int $userId)
    {
        try {
            $response = $this->userRequestService->getUserCompaniesRelations($userId);

            return json_decode($response->getData(), true);
        } catch (HttpException $e) {
            \Log::error($e->getMessage());

            return [];
        }
    }

    /** Get CompanyUser by user id and company
     *
     * @param int $userId
     * @param int $companyId
     *
     * @return CompanyUser object
     */
    public function getByUserAndCompanyId(int $userId, int $companyId)
    {
        return CompanyUser::where('company_id', $companyId)->where('user_id', $userId)->first();
    }

    /**
     * Build cache key based on auth0 user record
     *
     * @param string $companyId
     * @param string $userId
     * @return string
     */
    private function buildCacheKey($companyId = '*', $userId = '*')
    {
        if ($companyId == '*' && $userId == '*') {
            throw new HttpException(500, "Both identifiers cannot be wildcards.");
        }

        return env('APP_ENV')
            . ":companiesusers"
            . ":{$companyId}"
            . ":{$userId}";
    }

    /**
     * Get cache key based on given company and user ids
     *
     * @param string $companyId
     * @param string $userId
     * @return string
     */
    private function getCacheKey($companyId = '*', $userId = '*')
    {
        if ($companyId == '*' && $userId == '*') {
            throw new HttpException(500, "Both identifiers cannot be wildcards.");
        }

        if (Redis::exists($this->buildCacheKey($companyId, $userId))) {
            return $this->buildCacheKey($companyId, $userId);
        } elseif ($companyId != '*') {
            $keys = Redis::keys($this->buildCacheKey($companyId, '*'));
            return !empty($keys) ? $keys[0] : false;
        } else if ($userId != '*') {
            $keys = Redis::keys($this->buildCacheKey('*', $userId));
            return !empty($keys) ? $keys[0] : false;
        }

        return false;
    }

    /**
     * Cache record
     *
     * @param \Illuminate\Support\Collection $companiesUsers
     * @return void
     */
    private function storeInCache(
        Collection $companiesUsers,
        $companyId = '*',
        $userId = '*'
    ) {
        Redis::set(
            $this->buildCacheKey($companyId, $userId),
            serialize($companiesUsers),
            "ex",
            env("REDIS_CACHE_EXPIRY")
        );
    }

    /**
     * Get from cache
     *
     * @param string $key
     * @return Collection
     */
    private function getFromCache($key)
    {
        $data = Redis::get($key);

        return unserialize($data);
    }

    /**
     * Remove from cache
     *
     * @param Auth0User $user
     * @return void
     */
    private function deleteFromCache($key)
    {
        Redis::del($key);
    }
}
