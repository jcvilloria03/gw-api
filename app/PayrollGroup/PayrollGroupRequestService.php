<?php

namespace App\PayrollGroup;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PayrollGroupRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Philippine Payroll Group details
     *
     * @param int $id Payroll Group ID
     * @return json PayrollGroup information
     *
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/payroll_group/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Company's Payroll Groups
     *
     * @param int $id Company ID
     * @return json PayrollGroup information
     *
     */
    public function getCompanyPayrollGroups(int $id)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/payroll_groups"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch account payroll groups
     *
     * @param int $id account id
     * @return \Illuminate\Http\JsonResponse List of payroll groups
     */
    public function getAccountPayrollGroups(int $id)
    {
        $request = new Request(
            'GET',
            "/account/{$id}/payroll_groups"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if payroll group name is available
     *
     * @param int $id Company ID
     * @param array $data Payroll Group information
     * @return json Name is available for company
     *
     */
    public function isNameAvailable(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/payroll_group/is_name_available",
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete payroll group
     *
     * @param int $id Payroll Group ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/payroll_group/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple payroll groups
     *
     * @param array $data information on payroll groups to delete
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/payroll_group/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Fetch company payroll groups filtering by selected attribute
     *
     * @param int $companyId
     * @param string $attribute
     * @param array $attributeValues
     * @return \Illuminate\Http\JsonResponse List of payroll groups
     */
    public function getCompanyPayrollGroupByAttribute(int $companyId, string $attribute, array $attributeValues)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/payroll_groups/{$attribute}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributeValues)
        );
        return $this->send($request);
    }

    /**
     * Gets the employee payroll group.
     *
     * @param integer $employeeId
     *
     * @return \Illuminate\Http\JsonResponse  The employee payroll group.
     */
    public function getEmployeePayrollGroup(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/payroll_group/employee_payroll_group/{$employeeId}"
        );
        return $this->send($request);
    }

    /**
     * Gets the pay dates.
     *
     * @param \stdClass $group
     * @param int $employeeId
     *
     * @return array The pay dates.
     */
    public function getPayDates(\stdClass $group, int $employeeId)
    {
        $paydates = [];

        array_push($paydates, [
            'group_id' => $group->id,
            'employee_id' => $employeeId,
            'pay_date' => $group->pay_date,
            'name' => 'pay_date'
        ]);

        if ($group->pay_date_2) {
            array_push($paydates, [
                'group_id' => $group->id,
                'employee_id' => $employeeId,
                'pay_date_2' => $group->pay_date_2,
                'name' => 'pay_date_2'
            ]);
        }

        return $paydates;
    }

    /**
     * Call endpoint to fetch Data to be saved in Payroll Groups upload job
     *
     * @param string $id Job ID
     * @return json Company information
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/philippine/payroll_group/upload_preview?job_id={$jobId}"
        );
        return $this->send($request);
    }
}
