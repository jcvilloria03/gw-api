<?php

namespace App\PayrollGroup;

use App\ES\ESIndexQueueService;

class PayrollGroupEsIndexQueueService
{
    const TYPE = 'payroll_group';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created payroll groups for indexing
     * @param array $payrollGroupIds payroll group ids to be indexed
     */
    public function queue(array $payrollGroupIds)
    {
        $details = [
            'id' => $payrollGroupIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
