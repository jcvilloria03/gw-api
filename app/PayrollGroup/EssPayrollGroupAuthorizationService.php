<?php

namespace App\PayrollGroup;

use App\Authorization\EssBaseAuthorizationService;

class EssPayrollGroupAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.payroll_group';

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function authorizeView(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
