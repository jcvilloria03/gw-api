<?php

namespace App\PayrollGroup;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PhilippinePayrollGroupRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Philippine Payroll Group details
     *
     * @param int $id Payroll Group ID
     * @param AuthzDataScope|null
     * @return json PayrollGroup information
     *
     */
    public function get(int $id, $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/philippine/payroll_group/{$id}"
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to fetch all payroll groups in company
     *
     * @param int $id Company ID
     * @return json PayrollGroup(s) information
     *
     */
    public function getAll(int $id, $dataScope = null)
    {
        $request = new Request(
            'GET',
            "/philippine/company/{$id}/payroll_groups"
        );

        if ($dataScope instanceof AuthzDataScope) {
            return $this->send($request, $dataScope);
        }

        return $this->send($request);
    }


    /**
     * Call endpoint create a new payroll group
     *
     * @param array $id Payroll Group information
     * @return json PayrollGroup information
     *
     */
    public function create(array $payrollGroup, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/philippine/payroll_group/",
            [
               'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($payrollGroup)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to update Payroll group details
     *
     * @param array $payrollGroup Company information
     * @param int $id Payroll Group ID
     * @return json Payroll group information
     *
     */
    public function update(array $payrollGroup, $id)
    {
        $request = new Request(
            'PATCH',
            "/philippine/payroll_group/$id",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($payrollGroup)
        );
        return $this->send($request);
    }
}
