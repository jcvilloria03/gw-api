<?php

namespace App\PayrollGroup;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;

class PayrollGroupAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.payroll_group';
    const CREATE_TASK = 'create.payroll_group';
    const DELETE_TASK = 'delete.payroll_group';
    const UPDATE_TASK = 'edit.payroll_group';

    /**
     * Authorize view, update, delete PayrollGroup related tasks
     *
     * @param \stdClass $payrollGroup
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeNonCreateTask(
        \stdClass $payrollGroup,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payrollGroup's account
            return $accountScope->inScope($payrollGroup->account_id);
        }

        // verify company scope OR payroll group scope
        return $this->verifyCompanyScope($taskScopes, $payrollGroup, $user) ||
               $this->verifyPayrollGroupScope($taskScopes, $payrollGroup);
    }

    /**
     * Authorize Company Scope for view, update, delete PayrollGroup related tasks
     *
     * @param TaskScopes $taskScopes
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    protected function verifyCompanyScope(
        TaskScopes $taskScopes,
        \stdClass $payrollGroup,
        array $user
    ) {
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payrollGroups's account
                return $payrollGroup->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($payrollGroup->company_id);
        }
        return false;
    }


    /**
     * Authorize PayrolL Group Scope for view, update, delete PayrollGroup related tasks
     *
     * @param TaskScopes $taskScopes
     * @param \stdClass $payrollGroup
     * @return bool
     */
    protected function verifyPayrollGroupScope(
        TaskScopes $taskScopes,
        \stdClass $payrollGroup
    ) {
        $payrollGroupScope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        // check if user has payroll group level permissions for payroll group
        return $payrollGroupScope && $payrollGroupScope->inScope($payrollGroup->id);
    }

    /**
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $payrollGroup, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($payrollGroup, $user, self::VIEW_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $targetCompanyId
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyPayrollGroups(int $targetAccountId, int $targetCompanyId, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        // Check module access
        if (!$this->checkTaskModuleAccess(self::VIEW_TASK)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes(self::VIEW_TASK);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payrollGroup's account
            return $accountScope->inScope($targetAccountId);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payrollGroups's account
                return $targetAccountId == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($targetCompanyId);
        }
        return false;
    }

    /**
     * @param array $user
     * @return bool
     */
    public function authorizeGetAccountPayrollGroups(array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        // Check module access
        if (!$this->checkTaskModuleAccess(self::VIEW_TASK)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes(self::VIEW_TASK);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        // Check if user has account scope
        if ($accountScope) {
            return true;
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            // check if user's account has company target all permission
            return $companyScope->targetAll();
        }
        return false;
    }

    /**
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $payrollGroup, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeCreateTask($payrollGroup, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $payrollGroup, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeCreateTask($payrollGroup, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $payrollGroup, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($payrollGroup, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $payrollGroup
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $payrollGroup, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($payrollGroup, $user, self::UPDATE_TASK);
    }


    /**
     * Authorize create related tasks (create, is name available)
     *
     * @param \stdClass $payrollGroup
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeCreateTask(
        \stdClass $payrollGroup,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payrollGroup's account
            return $accountScope->inScope($payrollGroup->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payrollGroups's account
                return $payrollGroup->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($payrollGroup->company_id);
        }
        return false;
    }
}
