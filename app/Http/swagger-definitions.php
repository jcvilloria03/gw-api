<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     definition="JobLink",
 *     type="object",
 *     required={"self"},
 *     @SWG\Property(property="self", type="string", example="/jobs/bb88a596-56d4-491d-97b9-5cf216098d67")
 * )
 *
 * @SWG\Definition(
 *     definition="JobData",
 *     type="object",
 *     @SWG\Property(property="type", type="string" , example="jobs"),
 *     @SWG\Property(property="id", type="string", example="bb88a596-56d4-491d-97b9-5cf216098d67"),
 *     @SWG\Property(
 *         property="attributes",
 *         ref="#/definitions/JobDataAttributes"
 *     )
 * )
 *
 * @SWG\Definition(
 *     definition="JobDataAttributes",
 *     type="object",
 *     @SWG\Property(
 *          property="createdAt",
 *          type="integer",
 *          description="Timestamp of creation date and time",
 *          example="1578983210"
 *     ),
 *     @SWG\Property(
 *          property="expiresAt",
 *          type="integer",
 *          description="Timestamp of expires date and time",
 *          example="1586759210"
 *     ),
 *     @SWG\Property(property="payload"),
 *     @SWG\Property(property="status", type="string", description="Status of the job", example="PENDING"),
 *     @SWG\Property(property="tasksCount", type="integer", description="Total task", example="1"),
 *     @SWG\Property(property="tasksDone", type="integer", description="Total done task", example="0"),
 *     @SWG\Property(
 *          property="updatedAt",
 *          type="integer",
 *          description="Timestamp of updated date and time",
 *          example="null"
 *     ),
 *     @SWG\Property(property="version", type="integer", description="Version of the job", example="1")
 * )
 */

/**
 * Error Message
 *
 * @SWG\Definition(
 *     definition="ErrorMessage",
 *     type="object",
 *     description="Error Message",
 *     required={"message", "status_code"},
 *     @SWG\Property(property="message", type="string"),
 *     @SWG\Property(property="status_code", type="integer")
 * )
 */



/**
 * @SWG\Definition(
 *     definition="PayrollStatus",
 *     type="string",
 *     enum={
 *          "DRAFT",
 *          "CALCULATING",
 *          "RECALCULATING",
 *          "REOPENING",
 *          "OPEN",
 *          "CLOSING",
 *          "CLOSED",
 *          "DISBURSING",
 *          "DISBURSED"
 *     }
 * )
 *
 * @SWG\Definition(
 *     definition="Id",
 *     type="integer"
 * )
 * @SWG\Definition(
 *     definition="IdObject",
 *     type="object",
 *     @SWG\Property(property="id", ref="#/definitions/Id")
 * )
 *
 *
 * @SWG\Definition(
 *     definition="FinalPay",
 *     type="object",
 *     required={"id", "termination_information_id", "employee_id", "final_pay_items", "payroll", "projected"},
 *     @SWG\Property(property="id", type="integer", example=1),
 *     @SWG\Property(property="termination_information_id", type="integer", example=1),
 *     @SWG\Property(property="employee_id", type="integer", example=1),
 *     @SWG\Property(
 *          property="payroll",
 *          type="object",
 *          @SWG\Property(property="id", type="integer", example=1),
 *          @SWG\Property(property="status", ref="#/definitions/PayrollStatus", example="OPEN")
 *     ),
 *     @SWG\Property(
 *          property="final_pay_items",
 *          type="array",
 *          minItems=0,
 *          @SWG\Items(
 *              type="object",
 *              allOf={
 *                  @SWG\Schema(type="object", ref="#/definitions/IdObject"),
 *                  @SWG\Schema(type="object", ref="#/definitions/FinalPayItem"),
 *              }
 *          )
 *     ),
 *     @SWG\Property(property="projection_job_id", type="string"),
 *     @SWG\Property(property="projected", ref="#/definitions/FinalPayProjection"),
 *     @SWG\Property(property="calculated", ref="#/definitions/FinalPayProjection")
 * )
 *
 * @SWG\Definition(
 *     definition="FinalPayProjection",
 *     type="object",
 *     required={"gross_income", "non_taxable_income", "taxable_income", "withholding_tax_as_adjusted", "net_pay"},
 *     @SWG\Property(property="gross_income", type="number"),
 *     @SWG\Property(property="non_taxable_income", type="number"),
 *     @SWG\Property(property="taxable_income", type="number"),
 *     @SWG\Property(property="withholding_tax_as_adjusted", type="number"),
 *     @SWG\Property(property="net_pay", type="number"),
 *     @SWG\Property(
 *          property="taxable_incomes",
 *          type="array",
 *          @SWG\Items(type="object", ref="#/definitions/FinalPayIncome")
 *     ),
 *     @SWG\Property(
 *          property="non_taxable_incomes",
 *          type="array",
 *          @SWG\Items(type="object", ref="#/definitions/FinalPayIncome")
 *     ),
 *     @SWG\Property(
 *          property="deductions",
 *          type="array",
 *          @SWG\Items(type="object", ref="#/definitions/FinalPayIncome")
 *     ),
 * )
 *
 * @SWG\Definition(
 *     definition="FinalPayIncome",
 *     type="object",
 *     required={"label", "amount"},
 *     @SWG\Property(property="label", type="string"),
 *     @SWG\Property(property="amount", type="number")
 * )
 *
 * @SWG\Definition(
 *     definition="FinalPayItemTypes",
 *     type="string",
 *     enum={"ADJUSTMENT", "ALLOWANCE", "BONUS", "COMMISSION", "DEDUCTION", "CONTRIBUTION", "LOAN"}
 * )
 *
 * @SWG\Definition(
 *     definition="FinalPayItem",
 *     type="object",
 *     @SWG\Property(property="item_id", type="integer"),
 *     @SWG\Property(property="type", ref="#/definitions/FinalPayItemTypes"),
 *     @SWG\Property(
 *         property="meta",
 *         type="object"
 *     )
 * )
 *
 * @SWG\Definition(
 *     definition="CompanyBank",
 *     type="object",
 *     @SWG\Property(property="type", type="string"),
 *     @SWG\Property(property="id", type="integer"),
 *     @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="bankBranch", type="string"),
 *          @SWG\Property(property="accountNumber", type="string"),
 *          @SWG\Property(property="accountType", type="string", enum={"SAVINGS", "CURRENT"}, example="SAVINGS"),
 *          @SWG\Property(property="companyCode", type="string", example="123"),
 *          @SWG\Property(property="bank_details", type="object"),
 *          @SWG\Property(
 *               property="bank",
 *               type="object",
 *               @SWG\Property(property="id", type="integer"),
 *               @SWG\Property(property="name", type="string"),
 *               @SWG\Property(property="group", type="string")
 *          )
 *     )
 * )
 *
 * @SWG\Definition(
 *     definition="Bank",
 *     type="object",
 *     @SWG\Property(property="id", type="integer"),
 *     @SWG\Property(property="type", type="string"),
 *     @SWG\Property(
 *          property="attributes",
 *          type="object",
 *          @SWG\Property(property="name", type="string"),
 *          @SWG\Property(property="group", type="string")
 *     )
 * )
 *
 * @SWG\Definition(
 *     definition="AccountUser",
 *     type="object",
 *     @SWG\Property(property="id", type="integer", example=1),
 *     @SWG\Property(property="account_id", type="integer", example=1),
 *     @SWG\Property(property="email", type="string", example="user@salarium.com"),
 *     @SWG\Property(property="user_type", type="string", example="owner"),
 *     @SWG\Property(property="first_name", type="string", example=1),
 *     @SWG\Property(property="middle_name", type="string", example=1),
 *     @SWG\Property(property="last_name", type="string", example=1),
 *     @SWG\Property(property="full_name", type="string", example=1),
 *     @SWG\Property(property="last_active_company_id", type="integer", example=1),
 *     @SWG\Property(property="active", type="string", example=1),
 *     @SWG\Property(property="product_seats", type="array", @SWG\Items(ref="#/definitions/ProductSeat")),
 *     @SWG\Property(property="companies", type="array", @SWG\Items(ref="#/definitions/Company"))
 * )
 *
 * @SWG\Definition(
 *     definition="ProductSeat",
 *     type="object",
 *     @SWG\Property(property="id", type="integer", example=4),
 *     @SWG\Property(property="name", type="string", example="time and attendance plus payroll")
 * ),
 *
 * @SWG\Definition(
 *     definition="Country",
 *     type="object",
 *     @SWG\Property(property="id", type="integer", example=1),
 *     @SWG\Property(property="code", type="string", example="PH"),
 *     @SWG\Property(property="name", type="string", example="Philippines")
 * )
 *
 * @SWG\Definition(
 *     definition="Company",
 *     type="object",
 *     @SWG\Property(property="id", type="integer", example=1),
 *     @SWG\Property(property="name", type="string", example="Salarium"),
 *     @SWG\Property(property="country", ref="#/definitions/Country"),
 *     @SWG\Property(property="account_id", type="integer", example=1),
 *     @SWG\Property(property="logo", type="string", example="https://logo.url/logo.png"),
 *     @SWG\Property(property="email", type="string", example="company@email.com"),
 *     @SWG\Property(property="website", type="string", example="www.salarium.com"),
 *     @SWG\Property(property="mobile_number", type="string", example=""),
 *     @SWG\Property(property="telephone_number", type="string", example=""),
 *     @SWG\Property(property="telephone_extension", type="string", example=""),
 *     @SWG\Property(property="fax_number", type="string", example=""),
 *     @SWG\Property(property="employee_id", type="strinig", example=""),
 *     @SWG\Property(property="employee_uid", type="string", example="")
 * )
 *
 * @SWG\Definition(
 *     definition="PayrollGroup",
 *     type="object",
 *     @SWG\Property(property="id", type="integer"),
 *     @SWG\Property(property="account_id", type="integer"),
 *     @SWG\Property(property="company_id", type="integer"),
 *     @SWG\Property(property="name", type="string"),
 *     @SWG\Property(property="payroll_frequency", type="string"),
 *     @SWG\Property(property="day_factor", type="string", example="260.00"),
 *     @SWG\Property(property="hours_per_day", type="string", example="8.00"),
 *     @SWG\Property(property="non_working_day_option", type="string", example="BEFORE"),
 *     @SWG\Property(property="pay_date", type="string", example="2020-01-15", format="date"),
 *     @SWG\Property(property="fpd_end_of_month", type="boolean", example=false),
 *     @SWG\Property(property="cut_off_date", type="string", example="2020-01-10", format="date"),
 *     @SWG\Property(property="fcd_end_of_month", type="boolean", example=false),
 *     @SWG\Property(property="pay_run_posting", type="string", example="2020-01-10", format="date"),
 *     @SWG\Property(property="fpp_end_of_month", type="boolean", example=false),
 *     @SWG\Property(property="pay_date_2", type="string", example="2020-01-31", format="date"),
 *     @SWG\Property(property="spd_end_of_month", type="boolean", example=true),
 *     @SWG\Property(property="cut_off_date_2", type="string", example="2020-01-25", format="date"),
 *     @SWG\Property(property="scd_end_of_month", type="boolean", example=false),
 *     @SWG\Property(property="pay_run_posting_2", type="string", example="2020-01-25", format="date"),
 *     @SWG\Property(property="spp_end_of_month", type="boolean", example=false),
 *     @SWG\Property(property="sss_schedule", type="string", example="EVERY_PAY"),
 *     @SWG\Property(property="sss_method", type="string", example="EQUALITY_SPLIT"),
 *     @SWG\Property(property="philhealth_schedule", type="string", example="EVERY_PAY"),
 *     @SWG\Property(property="philhealth_method", type="string", example="EQUALITY_SPLIT"),
 *     @SWG\Property(property="hdmf_schedule", type="string", example="EVERY_PAY"),
 *     @SWG\Property(property="hdmf_method", type="string", example="EQUALITY_SPLIT"),
 *     @SWG\Property(property="wtax_schedule", type="string", example="EVERY_PAY"),
 *     @SWG\Property(property="wtax_method", type="string", example="BASED_ON_ACTUAL"),
 *     @SWG\Property(property="enforce_gap_loans", type="boolean", example=false),
 *     @SWG\Property(property="minimum_net_take_home_pay", type="string", example="2,000.00"),
 *     @SWG\Property(property="compute_overbreak", type="boolean", example=false),
 *     @SWG\Property(property="compute_undertime", type="boolean", example=false),
 *     @SWG\Property(property="compute_tardiness", type="boolean", example=false),
 * )
 */
