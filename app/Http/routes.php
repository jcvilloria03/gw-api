<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$middleware = [];

if (!$app->runningUnitTests()) {
    $middleware[] = 'cors';
    $middleware[] = 'authn';
    $middleware[] = 'user';
}

$middleware[] = 'authz';

if (!$app->runningUnitTests()) {
    $middleware[] = 'audit';
}

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) use ($app) {
    $api->get('/', function () use ($app) {
        return $app->version();
    });
});

$api->version('v1', ['middleware' => ['cors']], function ($api) use ($app) {
    $api->get('/bundy/auth_user/{id}/status', 'App\Http\Controllers\UserController@getBundyEmployeeStatus');

    // /api
    $api->group(['prefix' => 'api'], function ($api) {
        // /api/internal
        $api->group(['prefix' => 'internal'], function ($api) {
            // /api/internal/ra08t
            $api->group(['prefix' => 'ra08t'], function ($api) {
                $api->post('/devices', 'App\Http\Controllers\DeviceManagementController@store');
                $api->put('/devices/{id}', 'App\Http\Controllers\DeviceManagementController@updateDevice');
                $api->get('/devices', 'App\Http\Controllers\DeviceManagementController@searchDeviceByFilter');
                $api->delete('/devices/{id}', 'App\Http\Controllers\DeviceManagementController@unregisterDevice');
            });
        }); // end of /api/internal
    }); // end of /api
});

$api->version('v1', ['middleware' => ['auth0', 'cors', 'user']], function ($api) use ($app) {
    $api->post('/deprecated/account/sign_up', 'App\Http\Controllers\AccountController@signUpOld');
});

$api->version('v1', ['middleware' => ['cors', 'audit']], function ($api) use ($app) {
    $api->post('/auth/user/login/deprecated', 'App\Http\Controllers\UserController@login');
    $api->post('/account/sign_up', 'App\Http\Controllers\AccountController@signUp');

    $api->post('/auth/reset_password_request', 'App\Http\Controllers\UserController@resetPasswordRequest');
    $api->post('/auth/reset_password', 'App\Http\Controllers\UserController@resetPassword');
});

$api->version('v1', ['middleware' => ['cors']], function ($api) use ($app) {
    $api->group(['prefix' => 'subscriptions/tools'], function ($api) use ($app) {
        $api->get(
            '/customers/{owner_user_id}',
            'App\Http\Controllers\SubscriptionToolsController@getCustomerDetails'
        );

        $api->patch(
            '/invoices/{invoice_id}',
            'App\Http\Controllers\SubscriptionToolsController@updateInvoice'
        );
    });
});

$api->version('v1', ['middleware' => $middleware], function ($api) use ($app, $middleware) {

    /**
     * WHITELISTED FROM SUBSCRIPTION MIDDLEWARE
     */
    $api->group(['prefix' => 'account'], function ($api) use ($app) {
        // /account/
        $api->get('/', [
            'as' => 'account.get',
            'uses' => 'App\Http\Controllers\AccountController@get'
        ]);

        $api->get('/users', 'App\Http\Controllers\UserController@getAccountUsers');
        $api->get('/philippine/companies', [
            'as' => 'account.philippine.companies',
            'uses' => 'App\Http\Controllers\AccountController@getAccountCompanies'
        ]);
        $api->get('/allusers', 'App\Http\Controllers\UserController@getAllUsersFilter');
    });

    $api->get('/user/{userId}/initial-data', 'App\Http\Controllers\UserController@getUserInitialData');

    $api->group(['prefix' => 'subscriptions'], function ($api) use ($app) {
        $api->patch('/license/product', 'App\Http\Controllers\SubscriptionsController@updateOwnerLicenseProduct');
        $api->patch(
            '/{id}/plan/update',
            'App\Http\Controllers\SubscriptionsController@updateSubscriptionPlan'
        );
        $api->post('/transpose/license_units', 'App\Http\Controllers\SubscriptionsController@assign');
        $api->put('/transpose/license_units', 'App\Http\Controllers\SubscriptionsController@unassign');
        $api->get('/license_units', 'App\Http\Controllers\SubscriptionsController@getLicenseUnits');
        $api->get('/transpose/status/{jobId}', 'App\Http\Controllers\SubscriptionsController@getTransposeStatus');
        $api->get('/{subscriptionId}/stats', 'App\Http\Controllers\SubscriptionsController@getSubscriptionStats');
        $api->get('/{subscriptionId}/draft', 'App\Http\Controllers\SubscriptionDraftsController@show');
        $api->delete('/{subscriptionId}/draft', 'App\Http\Controllers\SubscriptionDraftsController@destroy');

        $api->get('/receipts', 'App\Http\Controllers\SubscriptionReceiptsController@search');

        $api->get('/invoices', 'App\Http\Controllers\SubscriptionInvoicesController@search');
        $api->get('/invoices/{id}', 'App\Http\Controllers\SubscriptionInvoicesController@get');
        $api->get(
            '/invoices/{id}/payments/generate_paynamics',
            'App\Http\Controllers\SubscriptionInvoicesController@getPaynamicsPaymentDetails'
        );
        $api->get(
            '/invoices/{id}/billed_users',
            'App\Http\Controllers\SubscriptionInvoicesController@getBilledUsers'
        );

        $api->post(
            '/invoices/{id}/payments/paypal/orders',
            'App\Http\Controllers\SubscriptionInvoicesController@createPayPalOrder'
        );

        $api->post(
            '/invoices/{id}/payments/paypal/capture/{orderId}',
            'App\Http\Controllers\SubscriptionInvoicesController@capturePayPalOrder'
        );

        $api->get('/billing_information', 'App\Http\Controllers\SubscriptionBillingInformationController@get');
        $api->patch('/billing_information', 'App\Http\Controllers\SubscriptionBillingInformationController@update');

        //account deletion request
        $api->get('/account_deletion',
            'App\Http\Controllers\AccountDeletionController@get');
        $api->post('/account_deletion', 'App\Http\Controllers\AccountDeletionController@create');
        $api->post('/account_deletion/{id}/confirm_otp', 'App\Http\Controllers\AccountDeletionController@confirmOtp');
        $api->post('/account_deletion/{id}/resend_otp', 'App\Http\Controllers\AccountDeletionController@resendOtp');
        $api->patch('/account_deletion/{id}/confirm', 'App\Http\Controllers\AccountDeletionController@confirm');
        $api->delete('/account_deletion/{id}/cancel', 'App\Http\Controllers\AccountDeletionController@cancelRequest');
    });

    $api->group(['prefix' => 'users'], function ($api) use ($app) {
        $api->get('/permissions', [
            'as' => 'users.permissions',
            'uses' => 'App\Http\Controllers\UserController@getUserPermissions'
        ]);
    });

    $api->group(['prefix' => 'subscription-payment'], function ($api) {
        $controller = 'App\Http\Controllers\SubscriptionPaymentController';

        $api->group(['prefix' => 'payment-method'], function ($api) use ($controller) {
            $api->post('/setup-intent', $controller . '@passThroughToApi');
            $api->get('/current', $controller . '@passThroughToApi');
            $api->delete('/current/remove', $controller . '@passThroughToApi');
        });

        $api->group(['prefix' => 'invoice'], function ($api) use ($controller) {
            $api->post('/{invoiceId:[0-9]+}/payment-intent', $controller . '@passThroughToApi');
            $api->get('/payment-status', $controller . '@passThroughToApi');
        });
    });

    $api->group(['prefix' => 'audit-trail'], function ($api) {
        $controller = 'App\Http\Controllers\AuditTrailController';

        // Audit Trail Modules
        $api->get('/modules', $controller . '@getModuleList');

        $api->group(['prefix' => 'logs'], function ($api) use ($controller) {
            $api->post('/', $controller . '@passThroughToApi');
            $api->post('/export', $controller . '@passThroughToApi');
        });
    });

    // /countries/
    $api->get('/countries/', 'App\Http\Controllers\CountryController@getAll');

    /**
     * WITH SUBSCRIPTION MIDDLEWARE
     */
    $additionalMiddleware = $app->runningUnitTests() ? [] : ['subscription'];

    $api->group(['middleware' => $additionalMiddleware], function ($api) use ($app) {
        $api->group(['prefix' => 'api'], function ($api) {
            /**
             * RA08T
             */
            $api->post(
                '/face_pass/ra08t/users/{user_id}/sync',
                'App\Http\Controllers\DeviceManagementController@syncFaceIdToDevices'
            );
        });

        $api->group(['prefix' => 'salpay'], function ($api) {
            $controller = 'App\Http\Controllers\SalpayIntegrationController';

            $api->get('/companies/{companyId}/integration-status', $controller . '@passThroughToApi');
            $api->get('/companies/salpay-settings/{userId}', $controller . '@passThroughToApi');

            $api->put(
                '/companies/{companyId}/payrolls/{payrollId}/disbursements/{disbursementId}',
                $controller . '@passThroughToApi'
            );
            $api->get('/companies/{companyId}/payrolls/{payrollId}/disbursements', $controller . '@passThroughToApi');
            $api->post('/companies/{companyId}/payrolls/{payrollId}/disbursements', $controller . '@passThroughToApi');
            $api->get('/companies/{companyId}/payrolls/{payrollId}/employees', $controller . '@passThroughToApi');

            $api->post(
                '/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/resend',
                $controller . '@passThroughToApi'
            );
            $api->post(
                '/companies/{companyId}/payrolls/{payrollId}/otp/disbursement/verify',
                $controller . '@passThroughToApi'
            );

            $api->get('/companies/{companyId}/card-designs', $controller . '@passThroughToApi');
            $api->get('/companies/{companyId}/employees', $controller . '@passThroughToApi');
            $api->get('/companies/{companyId}/invite-shipping-details', $controller . '@passThroughToApi');
            $api->post('/companies/{companyId}/employee-invitations', $controller . '@passThroughToApi');
            $api->put('/companies/{companyId}/employee-reinvitations', $controller . '@passThroughToApi');
            $api->delete('/companies/{companyId}/employee-invitations', $controller . '@passThroughToApi');
            $api->post('/companies/{companyId}/withdrawn-employee-invitations', $controller . '@passThroughToApi');
            $api->get('/companies/{companyId}/remaining-balance', $controller . '@passThroughToApi');
            $api->delete('/companies/{companyId}/employee-unlink-invitation', $controller . '@passThroughToApi');

            $api->get('/users/{userId}/companies', $controller . '@passThroughToApi');

            $api->post('/link/company-authorizations', $controller . '@passThroughToApi');
            $api->post('/link/company-integrations', $controller . '@passThroughToApi');
            $api->post('/link/company-otp-verifications', $controller . '@passThroughToApi');

            $api->get(
                '/companies/{companyId}/salpay-business/administrators',
                $controller . '@passThroughToApi'
            );
            $api->get(
                '/companies/{companyId}/salpay-account/administrators',
                $controller . '@passThroughToApi'
            );
            $api->post(
                '/companies/{companyId}/salpay-account/administrators',
                $controller . '@passThroughToApi'
            );
            $api->delete(
                '/companies/{companyId}/salpay-account/administrators/{adminId}',
                $controller . '@passThroughToApi'
            );
            $api->patch(
                '/companies/{companyId}/salpay-settings',
                $controller . '@passThroughToApi'
            );
        });

        $api->get('/accounts', 'App\Http\Controllers\AccountController@index');
        $api->post('/auth/user/verify_resend', 'App\Http\Controllers\UserController@verifyResend');
        $api->patch('/auth/user/change_password', [
            'as' => 'auth.user.change_password',
            'uses' => 'App\Http\Controllers\UserController@changePassword'
        ]);
        $api->get('/download/{type}/{uuid}', 'App\Http\Controllers\FileDownloadController@downloadFile');
        $api->delete('/payrolls', 'App\Http\Controllers\PayrollController@deleteMany');
        $api->delete('/payrolls/delete_payroll_job', 'App\Http\Controllers\PayrollController@deleteManyJob');
        $api->get(
            '/payrolls/delete_payroll_job/{job_id}',
            'App\Http\Controllers\PayrollController@deleteManyJobStatus'
        );
        $api->get('/payrolls/jobs/status', 'App\Http\Controllers\PayrollController@getPayrollsJobsStatus');

                // people
        $api->group(['prefix' => 'people'], function ($api) use ($app) {
            $api->post('/', 'App\Http\Controllers\EmployeeController@store');
        });

        $api->group(['prefix' => 'account'], function ($api) use ($app) {
            // /account/
            $api->get('/setup_progress', [
                'as' => 'account.setup_progress',
                'uses' => 'App\Http\Controllers\AccountController@setupProgress'
            ]);
            $api->patch('/progress', 'App\Http\Controllers\AccountController@progress');
            $api->post('/role/is_name_available', 'App\Http\Controllers\RoleController@isNameAvailable');
            $api->get('/roles/', 'App\Http\Controllers\RoleController@getAccountRoles');
            $api->get('/roles/tasks', 'App\Http\Controllers\RoleController@getTasks');
            $api->get('/roles/all', 'App\Http\Controllers\RoleController@getAllAccountRoles');
            $api->post('/role', 'App\Http\Controllers\RoleController@create');
            $api->get('/role/{id}', 'App\Http\Controllers\RoleController@getRole');
            $api->get('/role/{id}/assigned_users', 'App\Http\Controllers\RoleController@getAssignedUsers');
            $api->patch('/role/{id}', 'App\Http\Controllers\RoleController@update');
            $api->delete('/roles/bulk_delete', 'App\Http\Controllers\RoleController@bulkDelete');
            $api->post('/roles/check_in_use', 'App\Http\Controllers\RoleController@checkInUse');
            $api->get('/logs', 'App\Http\Controllers\AccountController@getAccountLogs');
            $api->get('/payroll_groups', 'App\Http\Controllers\PayrollGroupController@getAccountPayrollGroups');
        });

        $api->get('/role_templates', 'App\Http\Controllers\RoleTemplateController@getRoleTemplates');

        $api->group(['prefix' => 'user'], function ($api) use ($app) {
            // /user/
            $api->get('/{id}', 'App\Http\Controllers\UserController@get');
            // /user/
            $api->get('/details/{userId}', 'App\Http\Controllers\UserController@getUserDetails');
            $api->patch('/last_active_company', 'App\Http\Controllers\UserController@setLastActiveCompany');
            $api->patch('/{id}', 'App\Http\Controllers\UserController@update');
            $api->patch('/{id}/set_status', 'App\Http\Controllers\UserController@setStatus');
            $api->post('/', 'App\Http\Controllers\UserController@create');
            $api->post('/subscribe', 'App\Http\Controllers\UserController@subscribe');
            $api->delete('/bulk_delete', 'App\Http\Controllers\UserController@bulkDelete');
            $api->delete('/{id}', 'App\Http\Controllers\UserController@delete');
            $api->post('/is_email_available', 'App\Http\Controllers\UserController@isEmailAvailable');
            $api->post('/check_in_use', 'App\Http\Controllers\UserController@checkInUse');
            $api->post('/informations', [
                'as' => 'user.informations',
                'uses' => 'App\Http\Controllers\UserController@getUserInformations'
            ]);
            $api->post('/approvals', 'App\Http\Controllers\ApprovalController@getUserApprovals');
            $api->post('/upload', 'App\Http\Controllers\UserProfileController@upload');
            $api->get('/upload/status', 'App\Http\Controllers\UserProfileController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\UserProfileController@uploadPreview');
            $api->post('/upload/save', 'App\Http\Controllers\UserProfileController@uploadSave');
            $api->post('/{id}/resend_verification', 'App\Http\Controllers\UserController@resendVerificationEmail');
            $api->get('/{id}/companies', 'App\Http\Controllers\UserController@getUserCompanies');
        });

        $api->post(
            '/other_income_type/is_edit_available',
            'App\Http\Controllers\OtherIncomeTypeController@isEditAvailable'
        );

        $api->get(
            '/other_income_type/{id}',
            'App\Http\Controllers\OtherIncomeTypeController@get'
        );

        $api->get(
            '/other_income_type/tax_option_choices/{otherIncomeType}',
            'App\Http\Controllers\OtherIncomeTypeController@getTaxOptionsChoices'
        );

        $api->get(
            '/other_income/{id}',
            'App\Http\Controllers\OtherIncomeController@get'
        );

        $api->get('/undertime_request/{id}', 'App\Http\Controllers\UndertimeRequestController@get');
        $api->get('/notifications', 'App\Http\Controllers\EmployeeNotificationController@getNotifications');
        $api->get(
            '/announcement_notifications',
            'App\Http\Controllers\EmployeeNotificationController@getAnnouncementNotifications'
        );
        $api->get(
            '/notifications_status',
            'App\Http\Controllers\EmployeeNotificationController@getNotificationsStatus'
        );
        $api->put(
            '/notifications_status',
            'App\Http\Controllers\EmployeeNotificationController@updateNotificationsStatus'
        );
        $api->put(
            '/notifications/{id}/clicked',
            'App\Http\Controllers\EmployeeNotificationController@notificationClicked'
        );

        $api->group(['prefix' => 'philippine'], function ($api) use ($app) {
            // /philippine/company/
            $api->group(['prefix' => 'company'], function ($api) use ($app) {
                $api->get('/{id}', 'App\Http\Controllers\PhilippineCompanyController@get');
                $api->post('/', 'App\Http\Controllers\PhilippineCompanyController@create');
                $api->patch('/details', 'App\Http\Controllers\PhilippineCompanyController@updateDetails');
                $api->get('/{id}/payroll_groups', 'App\Http\Controllers\PhilippinePayrollGroupController@getAll');
                $api->get('/{id}/locations', 'App\Http\Controllers\LocationController@getCompanyLocations');
                $api->get(
                    '/{id}/bonus_types',
                    'App\Http\Controllers\PhilippineBonusTypeController@getCompanyBonusTypes'
                );

                //bonus
                $api->post('/{id}/bonus/bulk_create', 'App\Http\Controllers\PhilippineBonusController@bulkCreate');

                //commission
                $api->post(
                    '/{id}/commission/bulk_create',
                    'App\Http\Controllers\PhilippineCommissionController@bulkCreate'
                );

                //allowance
                $api->post(
                    '/{id}/allowance/bulk_create',
                    'App\Http\Controllers\PhilippineAllowanceController@bulkCreate'
                );

                //deduction
                $api->post(
                    '/{id}/deduction/bulk_create',
                    'App\Http\Controllers\PhilippineDeductionController@bulkCreate'
                );

                //adjustment
                $api->post(
                    '/{id}/adjustment/bulk_create',
                    'App\Http\Controllers\PhilippineAdjustmentController@bulkCreate'
                );

                // bonus_type
                $api->group(['prefix' => '{id}/bonus_type'], function ($api) {
                    $api->post(
                        '/bulk_create',
                        'App\Http\Controllers\PhilippineBonusTypeController@bulkCreate'
                    );
                    $api->post(
                        '/is_name_available',
                        'App\Http\Controllers\PhilippineBonusTypeController@isNameAvailable'
                    );
                });

                // allowance_type
                $api->group(['prefix' => '{id}/allowance_type'], function ($api) {
                    $api->post(
                        '/bulk_create',
                        'App\Http\Controllers\PhilippineAllowanceTypeController@bulkCreate'
                    );
                    $api->post(
                        '/is_name_available',
                        'App\Http\Controllers\PhilippineAllowanceTypeController@isNameAvailable'
                    );
                });

                // commission_type
                $api->group(['prefix' => '{id}/commission_type'], function ($api) {
                    $api->post(
                        '/bulk_create',
                        'App\Http\Controllers\PhilippineCommissionTypeController@bulkCreate'
                    );
                    $api->post(
                        '/is_name_available',
                        'App\Http\Controllers\PhilippineCommissionTypeController@isNameAvailable'
                    );
                });

                // deduction type
                $api->group(['prefix' => '{id}/deduction_type'], function ($api) {
                    $api->post(
                        '/bulk_create',
                        'App\Http\Controllers\PhilippineDeductionTypeController@bulkCreate'
                    );
                    $api->post(
                        '/is_name_available',
                        'App\Http\Controllers\PhilippineDeductionTypeController@isNameAvailable'
                    );
                });
            });

            $api->patch('/bonus_type/{id}', 'App\Http\Controllers\PhilippineBonusTypeController@update');
            $api->patch('/allowance_type/{id}', 'App\Http\Controllers\PhilippineAllowanceTypeController@update');
            $api->patch('/commission_type/{id}', 'App\Http\Controllers\PhilippineCommissionTypeController@update');
            $api->patch('/deduction_type/{id}', 'App\Http\Controllers\PhilippineDeductionTypeController@update');

            $api->patch('/bonus/{id}', 'App\Http\Controllers\PhilippineBonusController@update');
            $api->patch('/allowance/{allowanceId}', 'App\Http\Controllers\PhilippineAllowanceController@update');
            $api->patch('/commission/{commissionId}', 'App\Http\Controllers\PhilippineCommissionController@update');
            $api->patch('/deduction/{id}', 'App\Http\Controllers\PhilippineDeductionController@update');
            $api->patch('/adjustment/{id}', 'App\Http\Controllers\PhilippineAdjustmentController@update');

            $api->get('company_types', 'App\Http\Controllers\PhilippineCompanyController@getTypes');

            // demo company
            $api->get('company_demo/details/{id}',
                'App\Http\Controllers\PhilippineCompanyController@getDemoCompany');

            // /philippine/payroll_group/
            $api->group(['prefix' => 'payroll_group'], function ($api) use ($app) {
                $api->get('/{id}', 'App\Http\Controllers\PhilippinePayrollGroupController@get');
                $api->post('/', 'App\Http\Controllers\PhilippinePayrollGroupController@create');
                $api->patch('/{payroll_group_id}', 'App\Http\Controllers\PhilippinePayrollGroupController@update');
            });

            // /philippine/location/
            $api->group(['prefix' => 'location'], function ($api) use ($app) {
                $api->get('/{id}', 'App\Http\Controllers\LocationController@get');
                $api->post('/', 'App\Http\Controllers\LocationController@create');
                $api->post('/time_attendance', 'App\Http\Controllers\LocationController@createTimeAttendanceLocation');
                $api->post('/bulk_create', 'App\Http\Controllers\LocationController@bulkCreate');
                $api->put('/{id}', 'App\Http\Controllers\LocationController@update');
                $api->delete('/bulk_delete', 'App\Http\Controllers\LocationController@bulkDelete');
            });

            // /philippine/employee/
            $api->group(['prefix' => 'employee'], function ($api) use ($app) {
                $api->get('/form_options', 'App\Http\Controllers\PhilippineEmployeeController@getFormOptions');
                $api->get('/{id}', 'App\Http\Controllers\PhilippineEmployeeController@get');
                $api->patch('/{id}', 'App\Http\Controllers\PhilippineEmployeeController@update');
                $api->post('/', 'App\Http\Controllers\PhilippineEmployeeController@create');
            });
        });

        $api->group(['prefix' => 'time_attendance_locations'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\TimeAttendanceLocationController@get');
            $api->post('/', 'App\Http\Controllers\TimeAttendanceLocationController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\TimeAttendanceLocationController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\TimeAttendanceLocationController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\TimeAttendanceLocationController@bulkDelete');
        });

        $api->group(['prefix' => 'admin_dashboard'], function ($api) use ($app) {
            $api->post(
                '/calendar_data',
                'App\Http\Controllers\AdminDashboardController@getAllCalendarData'
            );
            $api->post(
                '/calendar_data_counts',
                'App\Http\Controllers\AdminDashboardController@getAllCalendarDataCounts'
            );
        });

        // Deprecated due to ticket 7702: Removal of company settings users and roles option
         $api->group(['prefix' => 'companies/{companyId}'], function ($api) {

             $api->group(['prefix' => 'roles'], function ($api) {
                 $api->get('/', 'App\Http\Controllers\CompanyRoleController@index');
             });

         });

        $api->group(['prefix' => 'accounts/{accountId}'], function ($api) {

            $api->group(['prefix' => 'roles'], function ($api) {
                $api->get('/', 'App\Http\Controllers\AccountRoleController@index');
                $api->post('/', 'App\Http\Controllers\AccountRoleController@create');
                $api->get('/{roleId}', 'App\Http\Controllers\AccountRoleController@get');
                $api->put('/{roleId}', 'App\Http\Controllers\AccountRoleController@update');
                $api->delete('/{roleId}', 'App\Http\Controllers\AccountRoleController@delete');
            });

            $api->get('/essential_data', 'App\Http\Controllers\EssentialDataController@getAccountEssentialData');

            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // $api->group(['prefix' => 'companies/{companyId}'], function ($api) {
            //     $api->get('/essential_data', 'App\Http\Controllers\EssentialDataController@getCompanyEssentialData');
            // });

        });

        // /company/{id}/
        $api->group(['prefix' => 'company'], function ($api) use ($app) {
            $api->get(
                '/{company_id}/active_employees_count',
                'App\Http\Controllers\AdminDashboardController@getActiveEmployeesCount'
            );
            $api->get(
                '/{companyId}/attendance_stats',
                'App\Http\Controllers\AdminDashboardController@getAttendanceStatistics'
            );
            $api->get(
                '/{company_id}/dashboard/attendance',
                'App\Http\Controllers\AdminDashboardController@getDashboardAttendance'
            );

            $api->post('/{company_id}/bank', 'App\Http\Controllers\CompanyBankController@store');
            $api->delete('/{company_id}/bank', 'App\Http\Controllers\CompanyBankController@deleteCompanyBanks');
            $api->get('/{id}/affected_employees/search', 'App\Http\Controllers\AffectedEmployeeController@search');
            $api->get('/{id}/affected_entities/search', 'App\Http\Controllers\AffectedEntityController@search');
            $api->get('/{id}/employee/events', 'App\Http\Controllers\EmployeeController@getEventsForEmployees');
            $api->post('/{company_id}', 'App\Http\Controllers\PhilippineCompanyController@update');
            // Deprecated due to ticket 7702: Removal of company settings users and roles option
            // $api->get('/{id}/users', 'App\Http\Controllers\UserController@getAllCompanyUsers');
            $api->delete('/bulk_delete', 'App\Http\Controllers\PhilippineCompanyController@bulkDelete');

            $api->get(
                '/{company_id}/banks',
                'App\Http\Controllers\CompanyBankController@get'
            );
            $api->get(
                '/{company_id}/government_forms/bir_2316',
                'App\Http\Controllers\GovernmentFormController@getGenerated2316s'
            );
            $api->post(
                '/{company_id}/government_forms/bir_2316/regenerate',
                'App\Http\Controllers\GovernmentFormController@regenerateBir2316'
            );
            $api->get(
                '/{company_id}/government_forms/bir_2316/employees',
                'App\Http\Controllers\GovernmentFormController@getCandidate2316Employees'
            );
            $api->post(
                '/{company_id}/government_forms/bir_2316/download',
                'App\Http\Controllers\GovernmentFormController@downloadEmployee2316'
            );
            $api->get(
                '/{id}/payroll/{type}/available_items',
                'App\Http\Controllers\PayrollController@getCompanyPayrollAvailableItems'
            );
            $api->post(
                '/{id}/payroll_group/is_name_available/',
                'App\Http\Controllers\PayrollGroupController@isNameAvailable'
            );

            $api->get(
                '/{companyId}/payslips/authorized_payroll_groups',
                'App\Http\Controllers\PayslipController@getAuthorizedCompanyPayrollGroups'
            );

            $api->post(
                '/{id}/location/is_name_available/',
                'App\Http\Controllers\LocationController@isNameAvailable'
            );
            $api->get(
                '/{id}/time_attendance_locations',
                'App\Http\Controllers\TimeAttendanceLocationController@getAll'
            );
            $api->get('/{id}/employees', 'App\Http\Controllers\EmployeeController@getCompanyEmployees');
            $api->get(
                '/{id}/employees/generate_masterfile/status',
                'App\Http\Controllers\EmployeeController@getGenerateEmployeeMasterfileStatus'
            );
            $api->get(
                '/{id}/employees/generate_masterfile/result',
                'App\Http\Controllers\EmployeeController@getGenerateEmployeeMasterfileResult'
            );
            $api->post(
                '/{id}/employees/generate_masterfile',
                'App\Http\Controllers\EmployeeController@generateEmployeeMasterfile'
            );
            $api->post(
                '/{id}/inactive_employees',
                'App\Http\Controllers\EmployeeController@getCompanyInactiveEmployees'
            );
            $api->get(
                '/{id}/employees/search',
                'App\Http\Controllers\EmployeeController@getCompanyEmployeesFilteredByNameOrId'
            );
            $api->get(
                '/{id}/employee/{employeeId}/shifts',
                'App\Http\Controllers\ShiftController@getEmployeeShifts'
            );
            $api->get(
                '{id}/sorted_employees',
                'App\Http\Controllers\EmployeeController@sortCompanyEmployees'
            );
            $api->get('/{id}/payrolls', 'App\Http\Controllers\PayrollController@getCompanyPayrolls');
            $api->get(
                '/{id}/payroll_data_counts',
                'App\Http\Controllers\PayrollController@getCompanyPayrollDataCounts'
            );
            $api->get('/{companyId}/ta_employees', 'App\Http\Controllers\EmployeeController@getCompanyTaEmployees');
            $api->post(
                '{id}/ta_employees/id',
                'App\Http\Controllers\EmployeeController@getCompanyTaEmployeesByIds'
            );
            $api->get('/{id}/ta_employees/search', 'App\Http\Controllers\EmployeeController@searchCompanyTaEmployees');
            $api->get(
                '/{id}/payroll_group_periods',
                'App\Http\Controllers\PayrollGroupController@getCompanyPayrollGroupsWithPeriods'
            );

            $api->post('/{type}/is_in_use', 'App\Http\Controllers\PhilippineCompanyController@isInUse');
            $api->get('/{id}/departments', 'App\Http\Controllers\DepartmentController@getCompanyDepartments');
            $api->post(
                '/{id}/department/is_name_available/',
                'App\Http\Controllers\DepartmentController@isNameAvailable'
            );

            $api->get('/{id}/positions', 'App\Http\Controllers\PositionController@getCompanyPositions');
            $api->post('/{id}/position/is_name_available/', 'App\Http\Controllers\PositionController@isNameAvailable');

            $api->get(
                '/{id}/employment_types',
                'App\Http\Controllers\EmploymentTypeController@getCompanyEmploymentTypes'
            );
            $api->post(
                '/{id}/employment_type/is_name_available/',
                'App\Http\Controllers\EmploymentTypeController@isNameAvailable'
            );

            $api->get('/{id}/teams', 'App\Http\Controllers\TeamController@getCompanyTeams');
            $api->post(
                '/{id}/team/is_name_available/',
                'App\Http\Controllers\TeamController@isNameAvailable'
            );

            $api->get('/{id}/ranks', 'App\Http\Controllers\RankController@getCompanyRanks');
            $api->post('/{id}/rank/is_name_available/', 'App\Http\Controllers\RankController@isNameAvailable');

            $api->get('/{id}/projects', 'App\Http\Controllers\ProjectController@getCompanyProjects');
            $api->post('/{id}/project/is_name_available/', 'App\Http\Controllers\ProjectController@isNameAvailable');

            $api->get('/{id}/holidays', 'App\Http\Controllers\HolidayController@getCompanyHolidays');
            $api->post('/{id}/holiday/is_name_available/', 'App\Http\Controllers\HolidayController@isNameAvailable');

            $api->get('/{id}/leave_types', 'App\Http\Controllers\LeaveTypeController@getCompanyLeaveTypes');
            $api->get(
                '/{id}/employee/{employeeId}/leave_types',
                'App\Http\Controllers\LeaveTypeController@getLeaveTypesWithinCompanyForEmployee'
            );
            $api->post(
                '/{id}/leave_type/is_name_available/',
                'App\Http\Controllers\LeaveTypeController@isNameAvailable'
            );

            $api->get(
                '/{id}/leave_entitlements',
                'App\Http\Controllers\LeaveEntitlementController@getCompanyLeaveEntitlements'
            );
            $api->post(
                '/{id}/leave_entitlement/is_name_available/',
                'App\Http\Controllers\LeaveEntitlementController@isNameAvailable'
            );

            $api->post(
                '/{company_id}/leave_credits',
                'App\Http\Controllers\LeaveCreditController@getCompanyLeaveCredits'
            );

            $api->post(
                '/{id}/leave_credits/download',
                'App\Http\Controllers\LeaveCreditController@downloadCompanyLeaveCredits'
            );

            $api->post(
                '/{id}/leave_credits/download/status',
                'App\Http\Controllers\LeaveCreditController@getLeaveCreditsDownloadStatus'
            );

            $api->post(
                '/{id}/leave_requests/download',
                'App\Http\Controllers\LeaveRequestController@download'
            );

            $api->post(
                '/{id}/leave_requests/download/status',
                'App\Http\Controllers\LeaveRequestController@getLeaveRequestsDownloadStatus'
            );

            $api->post(
                '/{id}/workflow_entitlements',
                'App\Http\Controllers\WorkflowEntitlementController@getCompanyWorkflowEntitlements'
            );
            $api->post(
                '/{id}/workflow_entitlement/is_name_available/',
                'App\Http\Controllers\WorkflowEntitlementController@isNameAvailable'
            );

            $api->get('/{id}/cost_centers', 'App\Http\Controllers\CostCenterController@getCompanyCostCenters');
            $api->post(
                '/{id}/cost_center/is_name_available/',
                'App\Http\Controllers\CostCenterController@isNameAvailable'
            );

            $api->get('/{companyId}/payslips', 'App\Http\Controllers\PayslipController@search');

            $api->get(
                '/{id}/day_hour_rates',
                'App\Http\Controllers\DayHourRateController@getCompanyDayHourRates'
            );

            $api->get(
                '/{id}/tardiness_rules',
                'App\Http\Controllers\TardinessRuleController@getCompanyTardinessRules'
            );
            $api->post(
                '/{id}/tardiness_rule/is_name_available',
                'App\Http\Controllers\TardinessRuleController@isNameAvailable'
            );

            $api->get(
                '/{id}/workflows',
                'App\Http\Controllers\WorkflowController@getCompanyWorkflows'
            );
            $api->post(
                '/{id}/workflow/is_name_available',
                'App\Http\Controllers\WorkflowController@isNameAvailable'
            );

            $api->get(
                '/{id}/night_shift',
                'App\Http\Controllers\NightShiftController@getCompanyNightShift'
            );

            $api->get(
                '/{id}/tags',
                'App\Http\Controllers\TagController@getCompanyTags'
            );

            $api->get(
                '/{id}/schedules',
                'App\Http\Controllers\ScheduleController@getCompanySchedules'
            );

            $api->get(
                '/{id}/schedule_ids',
                'App\Http\Controllers\ScheduleController@getCompanyScheduleIds'
            );

            $api->get(
                '/{companyId}/shifts',
                'App\Http\Controllers\ShiftController@getCompanyShifts'
            );
            $api->get(
                '/{companyId}/shifts_data',
                'App\Http\Controllers\ShiftController@getShiftsData'
            );

            $api->post(
                '/{companyId}/schedule/is_name_available',
                'App\Http\Controllers\ScheduleController@isNameAvailable'
            );

            $api->post(
                '/{companyId}/schedules/generate_csv',
                'App\Http\Controllers\ScheduleController@generateCsv'
            );
            $api->get(
                '/{companyId}/schedules/download/{fileName}',
                'App\Http\Controllers\ScheduleController@downloadCsv'
            );
            $api->post(
                '/{companyId}/shifts/generate_csv',
                'App\Http\Controllers\ShiftController@generateCsv'
            );
            $api->get(
                '/{companyId}/shifts/download/{fileName}',
                'App\Http\Controllers\ShiftController@downloadCsv'
            );

            $api->post(
                '/{companyId}/payroll_loans/generate_csv',
                'App\Http\Controllers\PayrollLoanController@generateCsv'
            );

            $api->post(
                '/{companyId}/payroll_loans/download',
                'App\Http\Controllers\PayrollLoanController@downloadPayrollLoans'
            );

            $api->post(
                '/{companyId}/payroll_loans/download/status',
                'App\Http\Controllers\PayrollLoanController@getPayrollLoansDownloadStatus'
            );

            $api->get(
                '/{companyId}/rest_days',
                'App\Http\Controllers\RestDayController@getCompanyRestDays'
            );

            $api->get(
                '/{id}/payroll_loans',
                'App\Http\Controllers\PayrollLoanController@getCompanyLoans'
            );

            $api->get(
                '/{id}/months_with_payroll_loans/{type}',
                'App\Http\Controllers\PayrollLoanController@monthsWithCollectedLoans'
            );

            $api->get(
                '/{id}/payroll_loan_types',
                'App\Http\Controllers\PayrollLoanTypeController@getCompanyLoanTypes'
            );

            $api->post(
                '/{id}/payroll_loan_types/is_name_available',
                'App\Http\Controllers\PayrollLoanTypeController@isNameAvailable'
            );

            $api->post('{id}/activate_gap_loans', 'App\Http\Controllers\PayrollLoanController@activateGapLoans');

            $api->get(
                '/{companyId}/employee/{employeeId}/payroll_loans',
                'App\Http\Controllers\PayrollLoanController@getEmployeeActiveLoans'
            );
            // /other_income_types/
            $api->get(
                '/{id}/other_income_types/{type}',
                'App\Http\Controllers\OtherIncomeTypeController@getAllCompanyIncomeTypes'
            );
            $api->delete(
                '/{id}/other_income_type',
                'App\Http\Controllers\OtherIncomeTypeController@delete'
            );
            $api->post(
                '/{id}/other_income_type/is_delete_available',
                'App\Http\Controllers\OtherIncomeTypeController@isDeleteAvailable'
            );

            // /role/
            $api->get('/{id}/roles', 'App\Http\Controllers\RoleController@getCompanyRoles');

            // /other_incomes/
            $api->get(
                '/other_income/{type}/upload/preview',
                'App\Http\Controllers\OtherIncomeController@uploadPreview'
            );
            $api->post(
                '/{id}/other_income/{type}/upload',
                'App\Http\Controllers\OtherIncomeController@upload'
            );
            $api->get(
                '/{id}/other_income/{type}/upload/status',
                'App\Http\Controllers\OtherIncomeController@uploadStatus'
            );
            $api->post(
                '/{id}/other_income/{type}/upload/save',
                'App\Http\Controllers\OtherIncomeController@uploadSave'
            );
            $api->get(
                '/{id}/other_incomes/{type}',
                'App\Http\Controllers\OtherIncomeController@getAllCompanyIncomes'
            );
            $api->get(
                '/{companyId}/employee/{employeeId}/other_incomes/{type}',
                'App\Http\Controllers\OtherIncomeController@getEmployeeOtherIncomes'
            );
            $api->post(
                '/{id}/other_incomes/{type}/download',
                'App\Http\Controllers\OtherIncomeController@downloadOtherIncomesCsv'
            );
            $api->delete(
                '/{id}/other_income',
                'App\Http\Controllers\OtherIncomeController@delete'
            );
            $api->post(
                '/{id}/other_income/is_delete_available',
                'App\Http\Controllers\OtherIncomeController@isDeleteAvailable'
            );
            $api->post(
                '/{id}/leave_requests',
                'App\Http\Controllers\LeaveRequestController@getCompanyLeaveRequests'
            );
            $api->get(
                '/{id}/leave_types/search',
                'App\Http\Controllers\LeaveTypeController@searchLeaveTypes'
            );

            // /annual_earning/
            $api->post(
                '/{id}/annual_earning/set',
                'App\Http\Controllers\AnnualEarningController@setAnnualEarning'
            );
            $api->post(
                '/{companyId}/annual_earning/bulk_delete',
                'App\Http\Controllers\AnnualEarningController@bulkDeleteAnnualEarning'
            );
            $api->get(
                '/{id}/annual_earning/group_by_year',
                'App\Http\Controllers\AnnualEarningController@groupByYear'
            );
            $api->delete(
                '/{companyId}/annual_earning/{id}',
                'App\Http\Controllers\AnnualEarningController@deleteAnnualEarning'
            );
            $api->post(
                '/{id}/annual_earning/download',
                'App\Http\Controllers\AnnualEarningController@downloadCsv'
            );
            $api->get(
                '/{companyId}/employee/{employeeId}/annual_earning',
                'App\Http\Controllers\AnnualEarningController@getAllByEmployee'
            );
            $api->get(
                '{id}/annual_earning/{annualEarningId}',
                'App\Http\Controllers\AnnualEarningController@getAnnualEarning'
            );

            // attendance
            $api->post(
                '/{id}/time_records/bulk_create_or_delete',
                'App\Http\Controllers\TimeRecordController@bulkCreateOrDelete'
            );

            // earning
            $api->get(
                '/{id}/earnings',
                'App\Http\Controllers\EarningController@getAll'
            );

            $api->get(
                '/{id}/hours_worked',
                'App\Http\Controllers\HoursWorkedController@getCompanyHoursWorked'
            );

            $api->post(
                '/{id}/hours_worked/bulk_create_or_update_or_delete',
                'App\Http\Controllers\HoursWorkedController@bulkCreateOrUpdateOrDelete'
            );

            $api->post(
                '/{id}/hours_worked/leaves/bulk_create_or_update_or_delete',
                'App\Http\Controllers\HoursWorkedController@leavesBulkCreateOrUpdateOrDelete'
            );

            $api->post(
                '/{companyId}/hours_worked/leaves',
                'App\Http\Controllers\HoursWorkedController@getLeaves'
            );

            // announcements
            $api->get(
                '/{companyId}/announcements',
                'App\Http\Controllers\AnnouncementController@getCompanyAnnouncements'
            );
            $api->post(
                '/{companyId}/announcements/download',
                'App\Http\Controllers\AnnouncementController@downloadCompanyAnnouncements'
            );

            // admin dashboard
            $api->post(
                '/{companyId}/dashboard_statistics',
                'App\Http\Controllers\AdminDashboardController@getDashboardStatistics'
            );

            $api->post(
                '/{companyId}/scorecard/download',
                'App\Http\Controllers\AdminDashboardController@download'
            );

            $api->post(
                '/{companyId}/batch_assign_shifts/validate',
                'App\Http\Controllers\ShiftController@batchAssignShiftsValidate'
            );

            $api->post(
                '/{companyId}/batch_assign_shifts/save',
                'App\Http\Controllers\ShiftController@batchAssignShiftsSave'
            );

            $api->get(
                '/{companyId}/batch_assign_shifts/status',
                'App\Http\Controllers\ShiftController@getBatchAssignShiftsStatus'
            );

            $api->get(
                '/{id}/employees/final_pay',
                'App\Http\Controllers\FinalPayController@getUnprocessedFinalPays'
            );

            // Employees export
            $api->post('/{id}/employees/export', 'App\Http\Controllers\EmployeeController@export201s');

            $api->post(
                '/{companyId}/employees_by_ids',
                'App\Http\Controllers\EmployeeController@getCompanyEmployeesByIds'
            );

            $api->get(
                '/{id}/max_clock_outs',
                'App\Http\Controllers\MaxClockOutController@getCompanyMaxClockOuts'
            );
        });

        //maximum clock-out
        $api->group(['prefix' => 'max_clock_out'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\MaxClockOutController@get');
            $api->post('/', 'App\Http\Controllers\MaxClockOutController@create');
            $api->patch('/{id}', 'App\Http\Controllers\MaxClockOutController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\MaxClockOutController@bulkDelete');
            $api->get('/get_rule/{emp_id}', 'App\Http\Controllers\MaxClockOutController@getRules');
        });

        // /payroll_group/
        $api->group(['prefix' => 'payroll_group'], function ($api) {
            $api->post('/upload', 'App\Http\Controllers\PayrollGroupController@upload');
            $api->post('/upload/save', 'App\Http\Controllers\PayrollGroupController@uploadSave');
            $api->get('/upload/status', 'App\Http\Controllers\PayrollGroupController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\PayrollGroupController@uploadPreview');
            $api->delete('/bulk_delete', 'App\Http\Controllers\PayrollGroupController@bulkDelete');
            $api->delete('/{id}', 'App\Http\Controllers\PayrollGroupController@delete');

            $api->get(
                '/{payrollGroupId}/periods',
                'App\Http\Controllers\PayrollGroupController@getPayrollGroupPeriods'
            );
        });

        // /annual_earning/
        $api->group(['prefix' => 'annual_earning'], function ($api) {
            $api->post('/upload', 'App\Http\Controllers\AnnualEarningController@upload');
            $api->post('/upload/save', 'App\Http\Controllers\AnnualEarningController@uploadSave');
            $api->get('/upload/status', 'App\Http\Controllers\AnnualEarningController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\AnnualEarningController@uploadPreview');
        });

        // /earning/
        $api->get('/employee/{id}/earning', 'App\Http\Controllers\EarningController@getByEmployeeId');

        $api->group(['prefix' => 'earning'], function ($api) {
            $api->get('/{id}', 'App\Http\Controllers\EarningController@get');
            $api->patch('/{id}/update', 'App\Http\Controllers\EarningController@update');
            $api->post('/upload', 'App\Http\Controllers\EarningController@upload');
            $api->post('/upload/save', 'App\Http\Controllers\EarningController@uploadSave');
            $api->get('/upload/status', 'App\Http\Controllers\EarningController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\EarningController@uploadPreview');
        });

        // /employee/
        $api->group(['prefix' => 'employee'], function ($api) use ($app) {
            $api->post(
                '/government_forms/bir_2316/bulk/generate',
                'App\Http\Controllers\GovernmentFormController@generateBir2316'
            );
            $api->post(
                '/employees_for_payroll_validation',
                'App\Http\Controllers\EmployeeController@getEmployeesForPayrollValidation'
            );
            $api->get(
                '/employees_for_payroll_validation/{job_id}',
                'App\Http\Controllers\EmployeeController@getEmployeesForPayrollValidationStatus'
            );
            $api->patch(
                '/{employee_id}/government_forms/bir_2316/{id}',
                'App\Http\Controllers\GovernmentFormController@updateBir2316'
            );
            $api->get(
                '/{employee_id}/government_forms/bir_2316/{id}',
                'App\Http\Controllers\GovernmentFormController@getBir2316'
            );
            $api->post(
                '/{employee_id}/bank',
                'App\Http\Controllers\EmployeeBankController@store'
            );
            $api->delete(
                '/payment_method/{employee_payment_method_id}',
                'App\Http\Controllers\EmployeeBankController@destroy'
            );
            $api->delete(
                '{employee_id}/payment_method',
                'App\Http\Controllers\EmployeeBankController@destroyMany'
            );
            $api->get(
                '/{employee_id}/payment_methods',
                'App\Http\Controllers\EmployeeBankController@index'
            );
            $api->patch(
                '/{employee_id}/payment_methods/{payment_method_id}',
                'App\Http\Controllers\EmployeeBankController@activatePaymentMethod'
            );
            $api->put(
                '/{id}',
                'App\Http\Controllers\EmployeeController@updateTAPayrollEmployee'
            );
            $api->post(
                '/',
                'App\Http\Controllers\PhilippineEmployeeController@createEmployee'
            );
            $api->post(
                '/has_usermatch',
                'App\Http\Controllers\PhilippineEmployeeController@hasMatchingUser'
            );
            $api->get(
                '/{employeeId}/rest_days',
                'App\Http\Controllers\RestDayController@getEmployeeRestDays'
            );
            $api->post(
                '/batch_add/personal_info',
                'App\Http\Controllers\EmployeeController@uploadPersonalInfo'
            );
            $api->post(
                '/batch_add/people',
                'App\Http\Controllers\EmployeeController@uploadPeople'
            );
            $api->post(
                '/batch_add/payroll_info',
                'App\Http\Controllers\EmployeeController@uploadPayrollInfo'
            );
            $api->post(
                '/batch_add/save',
                'App\Http\Controllers\EmployeeController@uploadSave'
            );
            $api->post(
                '/batch_add/time_attendance_info',
                'App\Http\Controllers\EmployeeController@uploadTimeAttendanceInfo'
            );
            $api->post(
                '/batch_add/ta_save',
                'App\Http\Controllers\EmployeeController@uploadTaSave'
            );
            $api->get(
                '/batch_add/preview',
                'App\Http\Controllers\EmployeeController@uploadPreview'
            );
            $api->get(
                '/batch_add/status',
                'App\Http\Controllers\EmployeeController@uploadStatus'
            );
            $api->get(
                '/batch_add/step_status_list',
                'App\Http\Controllers\EmployeeController@createUploadStatuses'
            );

            $api->post(
                '/batch_update/personal_info',
                'App\Http\Controllers\EmployeeController@updateUploadPersonalInfo'
            );
            $api->post(
                '/batch_update/people',
                'App\Http\Controllers\EmployeeController@updateUploadPeople'
            );

            $api->post(
                '/batch_update/payroll_info',
                'App\Http\Controllers\EmployeeController@updateUploadPayrollInfo'
            );
            $api->post(
                '/batch_update/time_attendance_info',
                'App\Http\Controllers\EmployeeController@updateUploadTimeAttendanceInfo'
            );
            $api->get(
                '/batch_update/status',
                'App\Http\Controllers\EmployeeController@updateUploadStatus'
            );
            $api->get(
                '/batch_update/status_list',
                'App\Http\Controllers\EmployeeController@updateUploadStatuses'
            );
            $api->get(
                '/{id}',
                'App\Http\Controllers\EmployeeController@getEmployee'
            );
            $api->patch(
                '/{id}',
                'App\Http\Controllers\EmployeeController@updateEmployee'
            );

            $api->get(
                '/{id}/leave_entitlement',
                'App\Http\Controllers\LeaveEntitlementController@getForEmployeeAndLeaveType'
            );

            $api->post(
                '/{id}/leave_requests',
                'App\Http\Controllers\LeaveRequestController@getEmployeeLeaveRequests'
            );

            $api->post(
                '/{id}/leave_requests/download',
                'App\Http\Controllers\LeaveRequestController@downloadEmployeeLeaveRequests'
            );

            $api->post(
                '/{id}/leave_credits/download',
                'App\Http\Controllers\LeaveCreditController@downloadEmployeeLeaveCredits'
            );

            $api->get(
                '/{id}/schedules/search',
                'App\Http\Controllers\ScheduleController@searchEmployeeSchedules'
            );

            $api->get(
                '/{id}/workflow_entitlements',
                'App\Http\Controllers\WorkflowEntitlementController@getEmployeeWorkflowEntitlements'
            );

            $api->post(
                '/{id}/workflow_entitlement/create_or_update',
                'App\Http\Controllers\WorkflowEntitlementController@createOrUpdateEmployeeWorkflowEntitlements'
            );

            $api->post(
                '/{id}/workflow_entitlement/has_pending_requests',
                'App\Http\Controllers\WorkflowEntitlementController@employeeHasPendingRequests'
            );

            $api->get(
                '/{id}/timesheet',
                'App\Http\Controllers\TimeRecordController@getTimesheetForEmployee'
            );

            // basic_pay_adjustment
            $api->get(
                '/{id}/basic_pay_adjustments',
                'App\Http\Controllers\BasicPayAdjustmentController@getByEmployeeId'
            );
            $api->post(
                '/{id}/basic_pay_adjustment',
                'App\Http\Controllers\BasicPayAdjustmentController@create'
            );
            $api->patch(
                '/{employeeId}/basic_pay_adjustment/{adjustmentId}/update',
                'App\Http\Controllers\BasicPayAdjustmentController@update'
            );
            $api->delete(
                '/{employeeId}/basic_pay_adjustment/delete',
                'App\Http\Controllers\BasicPayAdjustmentController@delete'
            );

            $api->get(
                '/{id}/termination_informations',
                'App\Http\Controllers\TerminationInformationController@getByEmployeeId'
            );
            $api->post(
                '/upload/picture/{id}',
                'App\Http\Controllers\EmployeeController@uploadEmployeePic'
            );
        });

        // basic_pay_adjustment
        $api->get(
            '/basic_pay_adjustment/{id}',
            'App\Http\Controllers\BasicPayAdjustmentController@get'
        );

        // /payroll/
        $api->group(['prefix' => 'payroll'], function ($api) use ($app) {
            $api->get(
                '/{payroll_id}/job/{job_name}',
                'App\Http\Controllers\PayrollController@getPayrollJobByName'
            );
            $api->post(
                '/upload/attendance',
                'App\Http\Controllers\PayrollController@uploadAttendance'
            );
            $api->post(
                '/{payroll_id}/as-api/attendance',
                'App\Http\Controllers\PayrollController@processAttendance'
            );
            $api->delete(
                '/upload/attendance',
                'App\Http\Controllers\PayrollController@deleteUploadedPayrollAttendance'
            );
            $api->get(
                '/upload/attendance/status',
                'App\Http\Controllers\PayrollController@uploadAttendanceStatus'
            );
            $api->post(
                '/upload/allowance',
                'App\Http\Controllers\PayrollController@uploadAllowance'
            );
            $api->delete(
                '/upload/allowance',
                'App\Http\Controllers\PayrollController@deleteUploadedPayrollAllowance'
            );
            $api->get(
                '/upload/allowance/status',
                'App\Http\Controllers\PayrollController@uploadAllowanceStatus'
            );

            $api->post(
                '/upload/bonus',
                'App\Http\Controllers\PayrollController@uploadBonus'
            );
            $api->delete(
                '/upload/bonus',
                'App\Http\Controllers\PayrollController@deleteUploadedPayrollBonus'
            );
            $api->get(
                '/upload/bonus/status',
                'App\Http\Controllers\PayrollController@uploadBonusStatus'
            );

            $api->post(
                '/upload/commission',
                'App\Http\Controllers\PayrollController@uploadCommission'
            );
            $api->delete(
                '/upload/commission',
                'App\Http\Controllers\PayrollController@deleteUploadedPayrollCommission'
            );
            $api->get(
                '/upload/commission/status',
                'App\Http\Controllers\PayrollController@uploadCommissionStatus'
            );

            $api->post(
                '/upload/deduction',
                'App\Http\Controllers\PayrollController@uploadDeduction'
            );
            $api->delete(
                '/upload/deduction',
                'App\Http\Controllers\PayrollController@deleteUploadedPayrollDeduction'
            );
            $api->get(
                '/upload/deduction/status',
                'App\Http\Controllers\PayrollController@uploadDeductionStatus'
            );

            $api->get(
                '/upload/status_list',
                'App\Http\Controllers\PayrollController@uploadStatuses'
            );

            $api->post('/', 'App\Http\Controllers\PayrollController@create');
            $api->post('/regular_payroll_job', 'App\Http\Controllers\PayrollController@regularPayrollJob');
            $api->get(
                '/regular_payroll_job/{job_id}',
                'App\Http\Controllers\PayrollController@regularPayrollJobStatus'
            );
            $api->post('/special', 'App\Http\Controllers\PayrollController@createSpecial');
            $api->post('/special_payroll_job', 'App\Http\Controllers\PayrollController@createSpecialJob');
            $api->get(
                '/special_payroll_job/{job_id}',
                'App\Http\Controllers\PayrollController@specialPayrollJobStatus'
            );
            $api->post('/final', 'App\Http\Controllers\PayrollFinalPayController@createFinalPay');

            $api->post('/{payroll_id}/bank_advise', 'App\Http\Controllers\PayrollController@getBankAdvise');
            $api->post('/{payroll_id}/bank_file', 'App\Http\Controllers\PayrollController@getBankFile');

            $api->get('/{id}', 'App\Http\Controllers\PayrollController@get');
            $api->patch('/{id}', 'App\Http\Controllers\PayrollController@edit');
            $api->post('/{id}/close', 'App\Http\Controllers\PayrollController@close');
            $api->post('/{id}/open', 'App\Http\Controllers\PayrollController@open');
            $api->delete('/{id}', 'App\Http\Controllers\PayrollController@delete');

            $api->post('/{payroll_id}/calculate', 'App\Http\Controllers\PayrollController@calculate');
            $api->post('/{payroll_id}/recalculate', 'App\Http\Controllers\PayrollController@recalculate');
            $api->get('/{payroll_id}/calculate/status', 'App\Http\Controllers\PayrollController@calculationStatus');

            $api->get('/{id}/payslips/generate', 'App\Http\Controllers\PayslipController@generate');
            $api->get('/{id}/payslips/can_generate', 'App\Http\Controllers\PayslipController@canGenerate');

            $api->get('/{id}/has_payslips', 'App\Http\Controllers\PayslipController@hasPayslips');

            $api->post('/{id}/send_payslips', 'App\Http\Controllers\PayrollController@sendPayslips');
            $api->post('/{payrollId}/send_single_payslip', 'App\Http\Controllers\PayrollController@sendSinglePayslip');
            $api->get(
                '/{id}/employee_disbursement_method',
                'App\Http\Controllers\PayrollController@getEmployeeDisbursementMethod'
            );
            $api->get(
                '/{id}/disbursement_summary',
                'App\Http\Controllers\PayrollController@getDisbursementSummary'
            );
            $api->get('/{id}/has_gap_loan_candidates', 'App\Http\Controllers\PayrollController@hasGapLoanCandidates');

            $api->get(
                '{id}/get_gap_loan_candidates',
                'App\Http\Controllers\PayrollController@getGapLoanCandidates'
            );
        });

        $api->post(
            'payroll_register/multiple',
            'App\Http\Controllers\PayrollController@createMultiplePayrollRegisters'
        );
        $api->get('payroll_register/{id}', 'App\Http\Controllers\PayrollController@getPayrollRegister');
        $api->post('payroll_register/{id}', 'App\Http\Controllers\PayrollController@createPayrollRegister');
        $api->get(
            'payroll_register/{id}/has_payroll_register',
            'App\Http\Controllers\PayrollController@hasPayrollRegister'
        );

        $api->group(['prefix' => 'department'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\DepartmentController@get');
            $api->post('/', 'App\Http\Controllers\DepartmentController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\DepartmentController@bulkCreate');
            $api->put('/bulk_update', 'App\Http\Controllers\DepartmentController@bulkUpdate');
            $api->put('/{id}', 'App\Http\Controllers\DepartmentController@update');
            $api->delete('/{id}', 'App\Http\Controllers\DepartmentController@delete');
        });

        $api->group(['prefix' => 'position'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\PositionController@get');
            $api->post('/', 'App\Http\Controllers\PositionController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\PositionController@bulkCreate');
            $api->put('/bulk_update', 'App\Http\Controllers\PositionController@bulkUpdate');
            $api->put('/{id}', 'App\Http\Controllers\PositionController@update');
            $api->delete('/{id}', 'App\Http\Controllers\PositionController@delete');
        });

        $api->group(['prefix' => 'employment_type'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\EmploymentTypeController@get');
            $api->post('/', 'App\Http\Controllers\EmploymentTypeController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\EmploymentTypeController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\EmploymentTypeController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\EmploymentTypeController@bulkDelete');
        });

        $api->group(['prefix' => 'team'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\TeamController@get');
            $api->post('/', 'App\Http\Controllers\TeamController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\TeamController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\TeamController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\TeamController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\TeamController@checkInUse');
        });

        $api->group(['prefix' => 'time_attendance_team'], function ($api) use ($app) {
            $api->post('/', 'App\Http\Controllers\TeamController@createTimeAttendanceTeam');
        });

        $api->group(['prefix' => 'rank'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\RankController@get');
            $api->post('/', 'App\Http\Controllers\RankController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\RankController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\RankController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\RankController@bulkDelete');
        });

        $api->group(['prefix' => 'project'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\ProjectController@get');
            $api->post('/', 'App\Http\Controllers\ProjectController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\ProjectController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\ProjectController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\ProjectController@bulkDelete');
        });

        $api->group(['prefix' => 'holiday'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\HolidayController@get');
            $api->post('/', 'App\Http\Controllers\HolidayController@create');
            $api->put('/{id}', 'App\Http\Controllers\HolidayController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\HolidayController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\HolidayController@checkInUse');
        });

        $api->group(['prefix' => 'leave_credit'], function ($api) use ($app) {
            $api->post('/', 'App\Http\Controllers\LeaveCreditController@create');
            $api->put('/{id}', 'App\Http\Controllers\LeaveCreditController@update');
            $api->get('/{id}', 'App\Http\Controllers\LeaveCreditController@get');
            $api->delete('/bulk_delete', 'App\Http\Controllers\LeaveCreditController@bulkDelete');
            $api->post('/upload', 'App\Http\Controllers\LeaveCreditController@upload');
            $api->get('/upload/status', 'App\Http\Controllers\LeaveCreditController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\LeaveCreditController@uploadPreview');
            $api->post('/upload/save', 'App\Http\Controllers\LeaveCreditController@uploadSave');
        });

        $api->group(['prefix' => 'leave_type'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\LeaveTypeController@get');
            $api->post('/', 'App\Http\Controllers\LeaveTypeController@create');
            $api->put('/{id}', 'App\Http\Controllers\LeaveTypeController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\LeaveTypeController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\LeaveTypeController@checkInUse');
        });

        $api->group(['prefix' => 'leave_request'], function ($api) use ($app) {
            $api->post('/upload', 'App\Http\Controllers\LeaveRequestController@upload');
            $api->get('/upload/status', 'App\Http\Controllers\LeaveRequestController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\LeaveRequestController@uploadPreview');
            $api->post('/upload/save', 'App\Http\Controllers\LeaveRequestController@uploadSave');
            $api->get('/{id}', 'App\Http\Controllers\LeaveRequestController@get');
            $api->post('/admin', 'App\Http\Controllers\LeaveRequestController@create');
            $api->delete('/bulk_delete', 'App\Http\Controllers\LeaveRequestController@bulkDelete');
            $api->put('/{id}/admin', 'App\Http\Controllers\LeaveRequestController@update');
            $api->post(
                '/admin/calculate_leaves_total_value',
                'App\Http\Controllers\LeaveRequestController@calculateLeavesTotalValue'
            );
        });

        $api->group(['prefix' => 'leave_entitlement'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\LeaveEntitlementController@get');
            $api->post('/', 'App\Http\Controllers\LeaveEntitlementController@create');
            $api->put('/{id}', 'App\Http\Controllers\LeaveEntitlementController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\LeaveEntitlementController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\LeaveEntitlementController@checkInUse');
        });

        $api->group(['prefix' => 'workflow_entitlement'], function ($api) use ($app) {
            $api->post('/upload', 'App\Http\Controllers\WorkflowEntitlementController@upload');
            $api->get('/upload/status', 'App\Http\Controllers\WorkflowEntitlementController@uploadStatus');
            $api->post(
                '/has_pending_requests',
                'App\Http\Controllers\WorkflowEntitlementController@hasPendingRequestsOfAnyType'
            );
            $api->post(
                '/{id}/has_pending_requests',
                'App\Http\Controllers\WorkflowEntitlementController@inlineUpdateHasPendingRequests'
            );
            $api->get(
                '/upload/has_pending_requests',
                'App\Http\Controllers\WorkflowEntitlementController@uploadHasPendingRequests'
            );
            $api->get('/upload/preview', 'App\Http\Controllers\WorkflowEntitlementController@uploadPreview');
            $api->post('/upload/save', 'App\Http\Controllers\WorkflowEntitlementController@uploadSave');
            $api->get('/{id}', 'App\Http\Controllers\WorkflowEntitlementController@get');
            $api->post(
                '/bulk_create_or_update',
                'App\Http\Controllers\WorkflowEntitlementController@bulkCreateOrUpdate'
            );
            $api->put('/{id}', 'App\Http\Controllers\WorkflowEntitlementController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\WorkflowEntitlementController@bulkDelete');
        });

        $api->group(['prefix' => 'schedule'], function ($api) {
            $api->delete('/bulk_delete', 'App\Http\Controllers\ScheduleController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\ScheduleController@checkInUse');
            $api->post('/', 'App\Http\Controllers\ScheduleController@create');
            $api->put('/{id}', 'App\Http\Controllers\ScheduleController@update');
            $api->get('/{id}', 'App\Http\Controllers\ScheduleController@get');
        });

        $api->group(['prefix' => 'shift'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\ShiftController@get');
            $api->post('/', 'App\Http\Controllers\ShiftController@create');
            $api->post('/bulk_create', 'App\Http\Controllers\ShiftController@bulkCreate');
            $api->put('/{id}', 'App\Http\Controllers\ShiftController@update');
            $api->put('/unassign/{id}', 'App\Http\Controllers\ShiftController@unassign');
            $api->post('/overlapping_requests', 'App\Http\Controllers\ShiftController@getShiftsOverlappingRequests');
            $api->post('/{id}/assign_shifts/status', 'App\Http\Controllers\ShiftController@getAssignShiftsStatus');
        });

        $api->group(['prefix' => 'cost_center'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\CostCenterController@get');
            $api->post('/', 'App\Http\Controllers\CostCenterController@create');
            $api->delete('/bulk_delete', 'App\Http\Controllers\CostCenterController@bulkDelete');
            $api->put('/{cost_center_id}', 'App\Http\Controllers\CostCenterController@update');
        });

        $api->group(['prefix' => 'payslip'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\PayslipController@get');
            $api->get('/zipped/{id}/url', 'App\Http\Controllers\PayslipController@getZippedPayslipsUrl');
            $api->post('/download_multiple', 'App\Http\Controllers\PayslipController@downloadMultiple');
        });

        // government forms
        $api->get(
            '/philippine/company/{id}/government_form_periods',
            'App\Http\Controllers\GovernmentFormController@getGovernmentFormPeriods'
        );

        $api->group(['prefix' => '/government_forms'], function ($api) use ($app) {
            $api->get('/', 'App\Http\Controllers\GovernmentFormController@index');
            $api->get('/{id}', 'App\Http\Controllers\GovernmentFormController@show');
            $api->post('/{id}/generate', 'App\Http\Controllers\GovernmentFormController@generateForm');
            $api->put('/{id}', 'App\Http\Controllers\GovernmentFormController@update');
            $api->post('/', 'App\Http\Controllers\GovernmentFormController@store');
            $api->get(
                '/1604c/available_years',
                'App\Http\Controllers\GovernmentFormController@getAvailableYearsForBir1604c'
            );
        });

        // SSS forms
        $api->group(['prefix' => '/philippine/government_forms/sss'], function ($api) use ($app) {
            $api->get(
                '/r5_methods',
                'App\Http\Controllers\GovernmentFormController@getR5PaymentMethods'
            );
            $api->get(
                '/r5_options',
                'App\Http\Controllers\GovernmentFormController@getR5FormOptions'
            );
            $api->get(
                '/r5_values',
                'App\Http\Controllers\GovernmentFormController@getR5FormValues'
            );
            $api->post(
                '/r5_generate',
                'App\Http\Controllers\GovernmentFormController@generateR5'
            );
            $api->post(
                '/loan_collection_generate',
                'App\Http\Controllers\GovernmentFormController@generateSssCollection'
            );
            $api->get(
                '/loan_collection_report_values',
                'App\Http\Controllers\GovernmentFormController@getSssLoanCollectionReportValue'
            );
            $api->get(
                '/loan_collection_report_options',
                'App\Http\Controllers\GovernmentFormController@getSssLoanCollectionOptions'
            );
        });

        // HDMF forms
        $api->group(['prefix' => '/philippine/government_forms/hdmf'], function ($api) use ($app) {
            $api->get(
                '/msrf_channels',
                'App\Http\Controllers\GovernmentFormController@getMsrfPaymentChannels'
            );
            $api->get(
                '/msrf_options',
                'App\Http\Controllers\GovernmentFormController@getMsrfFormOptions'
            );
            $api->get(
                '/msrf_values',
                'App\Http\Controllers\GovernmentFormController@getHdmfMsrfValues'
            );
            $api->post(
                '/msrf_generate',
                'App\Http\Controllers\GovernmentFormController@generateHdmfMsrf'
            );
        });

        // STLRF forms
        $api->group(['prefix' => '/philippine/government_forms/stlrf'], function ($api) use ($app) {
            $api->get(
                '/stlrf_values',
                'App\Http\Controllers\GovernmentFormController@getStlrfValues'
            );
            $api->post(
                '/stlrf_generate',
                'App\Http\Controllers\GovernmentFormController@generateStlrf'
            );
            $api->get(
                '/stlrf_options',
                'App\Http\Controllers\GovernmentFormController@getStlrfOptions'
            );
        });

        // Philhealth forms
        $api->group(['prefix' => '/philippine/government_forms/philhealth'], function ($api) use ($app) {
            $api->get(
                '/values',
                'App\Http\Controllers\GovernmentFormController@getPhilhealthValues'
            );
        });

        // BIR Forms
        $api->group(['prefix' => '/philippine/government_forms/bir'], function ($api) use ($app) {
            $api->get(
                '/1601c_options',
                'App\Http\Controllers\GovernmentFormController@getBir1601cFormOptions'
            );
            $api->post(
                '/1601c_generate',
                'App\Http\Controllers\GovernmentFormController@generate1601c'
            );
            $api->get(
                '/1601c_values',
                'App\Http\Controllers\GovernmentFormController@get1601cFormValues'
            );

            $api->get(
                '/1601e_options',
                'App\Http\Controllers\GovernmentFormController@getBir1601eFormOptions'
            );
            $api->get(
                '/1601e_values',
                'App\Http\Controllers\GovernmentFormController@get1601eFormValues'
            );
            $api->post(
                '/1601e_generate',
                'App\Http\Controllers\GovernmentFormController@generate1601e'
            );

            $api->get(
                '/1601f_options',
                'App\Http\Controllers\GovernmentFormController@getBir1601fFormOptions'
            );
            $api->get(
                '/1601f_values',
                'App\Http\Controllers\GovernmentFormController@get1601fFormValues'
            );
            $api->post(
                '/1601f_generate',
                'App\Http\Controllers\GovernmentFormController@generate1601f'
            );
        });

        $api->group(['prefix' => 'default_schedule'], function ($api) use ($app) {
            $api->get('/', 'App\Http\Controllers\DefaultScheduleController@index');
            $api->post('/bulk_create', 'App\Http\Controllers\DefaultScheduleController@bulkCreate');
            $api->post(
                '/related_requests_to_updated_days_of_week',
                'App\Http\Controllers\EmployeeRequestController@getRelatedRequestsToUpdatedDefaultScheduleDaysOfWeek'
            );
            $api->put('/bulk_update', 'App\Http\Controllers\DefaultScheduleController@bulkUpdate');
        });

        $api->group(['prefix' => 'day_hour_rate'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\DayHourRateController@get');
            $api->post('/', 'App\Http\Controllers\DayHourRateController@create');
            $api->patch('/{id}', 'App\Http\Controllers\DayHourRateController@update');
        });

        $api->group(['prefix' => 'tardiness_rule'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\TardinessRuleController@get');
            $api->post('/', 'App\Http\Controllers\TardinessRuleController@create');
            $api->delete('/bulk_delete', 'App\Http\Controllers\TardinessRuleController@bulkDelete');
            $api->put('/{id}', 'App\Http\Controllers\TardinessRuleController@update');
            $api->post('/check_in_use', 'App\Http\Controllers\TardinessRuleController@checkInUse');
        });

        $api->group(['prefix' => 'workflow'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\WorkflowController@get');
            $api->post('/', 'App\Http\Controllers\WorkflowController@create');
            $api->put('/{id}', 'App\Http\Controllers\WorkflowController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\WorkflowController@bulkDelete');
            $api->post('/check_in_use', 'App\Http\Controllers\WorkflowController@checkInUse');
        });

        $api->group(['prefix' => 'night_shift'], function ($api) {
            $api->get('/{id}', 'App\Http\Controllers\NightShiftController@get');
            $api->put('/{id}', 'App\Http\Controllers\NightShiftController@update');
        });

        $api->group(['prefix' => 'employee_request_module'], function ($api) {
            $api->get('/', 'App\Http\Controllers\EmployeeRequestModuleController@index');
        });

        // /schedule/
        $api->group(['prefix' => 'schedule'], function ($api) use ($app) {
            $api->post(
                '/upload',
                'App\Http\Controllers\ScheduleController@uploadSchedules'
            );
            $api->post(
                '/upload/save',
                'App\Http\Controllers\ScheduleController@uploadSave'
            );
            $api->get(
                '/upload/status',
                'App\Http\Controllers\ScheduleController@uploadStatus'
            );
            $api->get(
                '/upload/preview',
                'App\Http\Controllers\ScheduleController@uploadPreview'
            );
        });

        // /rest_day/
        $api->group(['prefix' => 'rest_day'], function ($api) use ($app) {
            $api->get('/{id}', 'App\Http\Controllers\RestDayController@get');
            $api->post('/', 'App\Http\Controllers\RestDayController@create');
            $api->put('/{id}', 'App\Http\Controllers\RestDayController@update');
            $api->post('/unassign/{id}', 'App\Http\Controllers\RestDayController@unassign');
        });

        // /view/
        $api->group(['prefix' => 'view'], function ($api) use ($app) {
            $api->post('/employee', 'App\Http\Controllers\EmployeeViewsController@createEmployeeView');
            $api->patch('/employee/{id}', 'App\Http\Controllers\EmployeeViewsController@updateEmployeeView');
            $api->get('/employee/form_options', 'App\Http\Controllers\EmployeeViewsController@getEmployeeViewFields');
            $api->get('/employee/user', 'App\Http\Controllers\EmployeeViewsController@getUserEmployeeViews');
            $api->get('/employee/{id}', 'App\Http\Controllers\EmployeeViewsController@getEmployeeView');
            $api->post('/employee/{id}/list', 'App\Http\Controllers\EmployeeViewsController@getEmployeeViewList');
            $api->get(
                '/attendance/form_options/{id}',
                'App\Http\Controllers\AttendanceViewController@getAttendanceViewColumns'
            );
            $api->get(
                '/{companyId}/attendance/user',
                'App\Http\Controllers\AttendanceViewController@getUserCustomView'
            );
            $api->get('/attendance/{attendanceId}', 'App\Http\Controllers\AttendanceViewController@getView');
            $api->post('/attendance/', 'App\Http\Controllers\AttendanceViewController@createCustomView');
            $api->patch('/attendance/{attendanceId}', 'App\Http\Controllers\AttendanceViewController@updateCustomView');
        });

        $api->group(['prefix' => 'attendance'], function ($api) use ($app) {
            // /attendance/
            $api->post('/records', 'App\Http\Controllers\AttendanceController@searchAttendanceRecords');
            $api->post('/records/export', 'App\Http\Controllers\AttendanceController@exportAttendanceRecords');
            $api->post('/records/export/job', 'App\Http\Controllers\AttendanceController@exportAttendanceRecordsJob');
            $api->get(
                '/records/export/job/{job_id}',
                'App\Http\Controllers\AttendanceController@exportAttendanceRecordsJobStatus'
            );
            $api->post('/records/upload', 'App\Http\Controllers\AttendanceController@uploadAttendanceRecords');
            $api->get('/job/{job_id}/errors', 'App\Http\Controllers\AttendanceController@getJobErrors');
            $api->get('/job/{job_id}', 'App\Http\Controllers\AttendanceController@getJobDetails');
            $api->get('/{employee_id}/{date}', 'App\Http\Controllers\AttendanceController@getSingleAttendanceRecord');
            $api->get(
                '/{employee_id}/{date}/calculate',
                'App\Http\Controllers\AttendanceController@calculateAttendanceRecord'
            );
            $api->get(
                '/{employee_id}/{date}/lock',
                'App\Http\Controllers\AttendanceController@lockAttendanceRecord'
            );
            $api->post(
                '/lock/bulk',
                'App\Http\Controllers\AttendanceController@bulkLockAttendanceRecord'
            );
            $api->put(
                '/{employee_id}/{date}/edit',
                'App\Http\Controllers\AttendanceController@editAttendanceRecord'
            );
            $api->get(
                '/{employee_id}/{date}/unlock',
                'App\Http\Controllers\AttendanceController@unlockAttendanceRecord'
            );
            $api->post(
                '/unlock/bulk',
                'App\Http\Controllers\AttendanceController@bulkUnlockAttendanceRecord'
            );
            $api->post(
                '/calculate/bulk',
                'App\Http\Controllers\AttendanceController@bulkAttendanceCalculateByEmployees'
            );
            $api->post(
                '/company/{company_id}/{attendance_date}/calculate',
                'App\Http\Controllers\AttendanceController@bulkAttendanceCalculateByCompany'
            );
            $api->get(
                '/{employee_id}/{date}/lock_status',
                'App\Http\Controllers\AttendanceController@getSingleAttendanceRecordLockStatus'
            );
        });

        // /payroll_loan/
        $api->group(['prefix' => 'payroll_loan'], function ($api) use ($app) {
            $api->post('/upload', 'App\Http\Controllers\PayrollLoanController@upload');
            $api->post('/upload/save', 'App\Http\Controllers\PayrollLoanController@uploadSave');
            $api->get('/upload/status', 'App\Http\Controllers\PayrollLoanController@uploadStatus');
            $api->get('/upload/preview', 'App\Http\Controllers\PayrollLoanController@uploadPreview');
            $api->delete('/bulk_delete', 'App\Http\Controllers\PayrollLoanController@bulkDelete');
            $api->post('/', 'App\Http\Controllers\PayrollLoanController@createInitialPreview');
            $api->post('/create/{uid}', 'App\Http\Controllers\PayrollLoanController@create');
            $api->post('/{id}', 'App\Http\Controllers\PayrollLoanController@update');
            $api->get('/{id}/initial_preview', 'App\Http\Controllers\PayrollLoanController@initialPreview');
            $api->post(
                '/{id}/update_amortization_preview',
                'App\Http\Controllers\PayrollLoanController@updateAmortizationPreview'
            );
            $api->post('/{id}/update_loan_preview', 'App\Http\Controllers\PayrollLoanController@updateLoanPreview');
            $api->get('/{id}/loan_details', 'App\Http\Controllers\PayrollLoanController@getLoanDetails');
            $api->get(
                '/{id}/amortizations_details',
                'App\Http\Controllers\PayrollLoanController@getAmortizationsDetailsOfLoan'
            );
        });

        // /payroll_loan_type/
        $api->group(['prefix' => 'payroll_loan_type'], function ($api) use ($app) {
            $api->get(
                '/subtypes/{loanTypeName}',
                'App\Http\Controllers\PayrollLoanTypeController@getLoanSubtypesByName'
            );
            $api->patch('/{id}', 'App\Http\Controllers\PayrollLoanTypeController@update');
            $api->delete('/bulk_delete', 'App\Http\Controllers\PayrollLoanTypeController@bulkDelete');
            $api->post('/', 'App\Http\Controllers\PayrollLoanTypeController@create');
        });

        // /broadcasting/
        $api->group(['prefix' => 'broadcasting'], function ($api) use ($app) {
            $api->post('/auth', 'App\Http\Controllers\BroadcastController@authenticate');
        });

        // final_pay
        $api->post('/final_pay', 'App\Http\Controllers\FinalPayController@saveFinalPay');
        $api->patch('/final_pay/{id}', 'App\Http\Controllers\FinalPayController@patchFinalPay');
        $api->get('/employee/{employeeId}/final_pay/loans', 'App\Http\Controllers\FinalPayController@getLoans');
        $api->get('/employee/{employeeId}/final_pay/bonuses', 'App\Http\Controllers\FinalPayController@getBonuses');
        $api->get(
            '/employee/{employeeId}/final_pay/commissions',
            'App\Http\Controllers\FinalPayController@getCommissions'
        );
        $api->get(
            '/employee/{employeeId}/final_pay/allowances',
            'App\Http\Controllers\FinalPayController@getAllowances'
        );
        $api->get(
            '/employee/{employeeId}/final_pay/deductions',
            'App\Http\Controllers\FinalPayController@getDeductions'
        );
        $api->get(
            '/employee/{employeeId}/final_pay/adjustments',
            'App\Http\Controllers\FinalPayController@getAdjustments'
        );
        $api->get(
            '/employee/{employeeId}/final_pay/contributions/{terminationInformationId}',
            'App\Http\Controllers\FinalPayController@getContributions'
        );
        $api->get(
            '/employee/{employeeId}/final_pay/available_items',
            'App\Http\Controllers\FinalPayController@getAvailableItems'
        );
        $api->post('/employee/{employeeId}/final_pay/compute', 'App\Http\Controllers\FinalPayController@compute');
        $api->post('/employee/{employeeId}/final_pay/save', 'App\Http\Controllers\FinalPayController@save');

        // termination_information
        $api->group(['prefix' => 'termination_informations'], function ($api) use ($app) {
            $api->post('/', 'App\Http\Controllers\TerminationInformationController@create');
            $api->delete('/{id}', 'App\Http\Controllers\TerminationInformationController@delete');
            $api->get('/{id}', 'App\Http\Controllers\TerminationInformationController@get');
            $api->patch('/{id}', 'App\Http\Controllers\TerminationInformationController@update');
        });

        $api->group(['prefix' => 'announcement'], function ($api) {
            $api->post('/{announcementId}/reply', 'App\Http\Controllers\AnnouncementController@reply');
            $api->get('/{announcementId}', 'App\Http\Controllers\AnnouncementController@get');
            $api->post('/', 'App\Http\Controllers\AnnouncementController@create');
            $api->put(
                '/{announcementId}/recipient_seen',
                'App\Http\Controllers\AnnouncementController@updateRecipientSeen'
            );
            $api->put(
                '/reply/{id}/seen',
                'App\Http\Controllers\AnnouncementController@markReplyAsSeen'
            );
        });

        $api->group(['prefix' => 'request'], function ($api) {
            $api->post('/send_message', 'App\Http\Controllers\EmployeeRequestController@sendMessage');
            $api->post('/bulk_approve', 'App\Http\Controllers\ApprovalController@bulkApprove');
            $api->post('/bulk_decline', 'App\Http\Controllers\ApprovalController@bulkDecline');
            $api->post('/bulk_cancel', 'App\Http\Controllers\ApprovalController@bulkCancel');
        });

        $api->get('/time_dispute_request/{id}', 'App\Http\Controllers\TimeDisputeRequestController@get');
        $api->get('/overtime_request/{id}', 'App\Http\Controllers\OvertimeRequestController@get');
        $api->get('/shift_change_request/{id}', 'App\Http\Controllers\ShiftChangeRequestController@get');

        // /product_seat/
        $api->get('/available_product_seats', 'App\Http\Controllers\ProductSeatController@getAllAvailable');
        $api->get(
            '/product-seat-details[/{companyId}]',
            'App\Http\Controllers\ProductSeatController@getProductSeatsDetails'
        );

        // /bank/
        $api->group(['prefix' => 'banks'], function ($api) use ($app) {
            $api->get('/', 'App\Http\Controllers\BankController@index');
        });

        // /api
        $api->group(['prefix' => 'api'], function ($api) {
            // /api/face_pass
            $api->group(['prefix' => 'face_pass'], function ($api) {
                // /api/face_pass/ra08t
                $api->group(['prefix' => 'ra08t'], function ($api) {
                    $api->get('/devices', 'App\Http\Controllers\DeviceManagementController@get');

                    // Users
                    $api->post('/users', 'App\Http\Controllers\DeviceManagementController@registerUser');

                    // User devices
                    $api->get('/users/{user_id}', 'App\Http\Controllers\DeviceManagementController@userDevices');
                    $api->delete(
                        '/users/{user_id}/devices',
                        'App\Http\Controllers\DeviceManagementController@unregisterUserDevices'
                    );

                    // Accounts
                    $api->get(
                        '/accounts/unlinked_device_users',
                        'App\Http\Controllers\DeviceManagementController@getUnlinkedDeviceUsers'
                    );

                    $api->delete(
                        '/accounts/unlinked_device_users',
                        'App\Http\Controllers\DeviceManagementController@deleteUnlinkedDeviceUsers'
                    );
                }); // end of /api/facepass/ra08t
            }); // end of /api/facepass
        }); // end of /api

        // /time
        $api->get('/time', 'App\Http\Controllers\ClockStateController@getServerTime');
    });
});

require 'ess_routes.php';
