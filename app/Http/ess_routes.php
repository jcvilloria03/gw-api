<?php

$essMiddleware = $middleware;

if (!$app->runningUnitTests()) {
    $essMiddleware[] = 'ess';
}

$api->version('v1', ['middleware' => $essMiddleware, 'prefix' => 'ess'], function ($api) use ($app) {
    $api->post('/change_password', [
        'as' => 'ess.auth.change_password',
        'uses' => 'App\Http\Controllers\EssEmployeeController@changePassword'
    ]);

    $api->get('/hours_worked', 'App\Http\Controllers\EssHoursWorkedController@getHoursWorked');

    $api->group(['prefix' => 'payslip'], function ($api) use ($app) {
        $api->get('/{id}', [
            'as' => 'ess.payslip.getById',
            'uses' => 'App\Http\Controllers\EssPayslipController@readPayslip'
        ]);
    });

    // /philippine/employee/
    $api->group(['prefix' => 'philippine/employee'], function ($api) use ($app) {
        $api->get('/profile', 'App\Http\Controllers\EssEmployeeController@getProfile');
    });

    // /user/
    $api->group(['prefix' => 'user'], function ($api) use ($app) {
        $api->get('/{id}', [
            'as' => 'ess.user.getById',
            'uses' => 'App\Http\Controllers\EssUserController@get'
        ]);
    });

    // /employee/
    $api->group(['prefix' => 'employee'], function ($api) use ($app) {
        $api->get('/requests', 'App\Http\Controllers\EssEmployeeRequestController@getRequests');
        $api->post('/approvals', 'App\Http\Controllers\EssApprovalController@getApprovals');
        $api->get('/user', [
            'as' => 'ess.employee.user',
            'uses' => 'App\Http\Controllers\EssEmployeeController@getUser'
        ]);
        $api->get('/calendar_data', 'App\Http\Controllers\EssEmployeeController@getCalendarData');
        $api->post('/announcements', 'App\Http\Controllers\EssAnnouncementController@getAnnouncements');
        $api->get(
            '/announcements/unread',
            'App\Http\Controllers\EssAnnouncementController@getUserUnreadAnnouncements'
        );
        $api->post(
            '/count',
            'App\Http\Controllers\EssEmployeeController@affectedEmployeesCount'
        );

        // schedules
        $api->get('/current-schedules', 'App\Http\Controllers\EssScheduleController@getEmployeeCurrentSchedules');
        $api->get('/schedules', 'App\Http\Controllers\EssScheduleController@getEmployeeSchedules');
        $api->get('/rest_days', 'App\Http\Controllers\EssRestDayController@getEmployeeRestDays');

        // leave types
        $api->get('/leave_types', 'App\Http\Controllers\EssLeaveTypeController@getEntitledLeaveTypes');
    });

    // payslips
    $api->get('/payslips', [
        'as' => 'ess.payslip.getByYear',
        'uses' => 'App\Http\Controllers\EssPayslipController@getPayslips'
    ]);
    $api->get('/payslips/unread', [
        'as' => 'ess.payslip.unread',
        'uses' => 'App\Http\Controllers\EssPayslipController@getUnreadPayslips'
    ]);

    // shifts
    $api->get('/shifts', 'App\Http\Controllers\EssEmployeeController@getShifts');

    //notifications
    $api->get('/notifications', 'App\Http\Controllers\EssNotificationController@getNotifications');
    $api->get('/notifications_status', 'App\Http\Controllers\EssNotificationController@getNotificationsStatus');
    $api->put('/notifications_status', 'App\Http\Controllers\EssNotificationController@updateNotificationsStatus');
    $api->put('/notifications/{id}/clicked', 'App\Http\Controllers\EssNotificationController@notificationClicked');

    $api->post('/leave_request', 'App\Http\Controllers\EssLeaveRequestController@create');
    $api->get('/leave_request/{id}', 'App\Http\Controllers\EssLeaveRequestController@get');

    $api->get('/leave_credits', 'App\Http\Controllers\EssLeaveCreditController@index');

    $api->post('/overtime_request', 'App\Http\Controllers\EssOvertimeRequestController@create');
    $api->get('/overtime_request/{id}', 'App\Http\Controllers\EssOvertimeRequestController@get');

    $api->post('/time_dispute_request', 'App\Http\Controllers\EssTimeDisputeRequestController@create');
    $api->get('/time_dispute_request/{id}', 'App\Http\Controllers\EssTimeDisputeRequestController@get');

    $api->post('/shift_change_request', 'App\Http\Controllers\EssShiftChangeRequestController@create');
    $api->get('/shift_change_request/{id}', 'App\Http\Controllers\EssShiftChangeRequestController@get');

    $api->post('/undertime_request', 'App\Http\Controllers\EssUndertimeRequestController@create');
    $api->get('/undertime_request/{id}', 'App\Http\Controllers\EssUndertimeRequestController@get');

    $api->post('/attachments/replace', 'App\Http\Controllers\EssAttachmentsController@replace');
    $api->post('/attachments', 'App\Http\Controllers\EssAttachmentsController@upload');
    $api->delete(
        'employee_request/{requestId}/attachments/{id}',
        'App\Http\Controllers\EssAttachmentsController@delete'
    );
    $api->get(
        'employee_request/{id}/download_attachments',
        'App\Http\Controllers\EssAttachmentsController@download'
    );

    $api->get(
        '/time_types',
        'App\Http\Controllers\EssDayHourRateController@getTimeTypes'
    );

    $api->get('/affected_employees/search', 'App\Http\Controllers\EssAffectedEmployeeController@search');

    $api->group(['prefix' => 'request'], function ($api) use ($app) {
        $api->post('/send_message', 'App\Http\Controllers\EssEmployeeRequestController@sendMessage');
        $api->post('/bulk_decline', 'App\Http\Controllers\EssApprovalController@bulkDecline');
        $api->post('/bulk_approve', 'App\Http\Controllers\EssApprovalController@bulkApprove');
        $api->post('/{id}/cancel', 'App\Http\Controllers\EssEmployeeRequestController@cancel');
        $api->post('/bulk_cancel', 'App\Http\Controllers\EssApprovalController@bulkCancel');
    });

    $api->group(['prefix' => 'shift'], function ($api) {
        $api->post('/overlapping_requests', 'App\Http\Controllers\EssShiftController@getShiftsOverlappingRequests');
    });


    $api->get('/time', 'App\Http\Controllers\ClockStateController@getServerTime');

    $api->group(['prefix' => 'clock_state'], function ($api) {
        $api->get('/', 'App\Http\Controllers\ClockStateController@get');
        $api->get('/timesheet', 'App\Http\Controllers\ClockStateController@getTimesheet');
        $api->get('/timesheet/last_entry', 'App\Http\Controllers\ClockStateController@getTimesheetLastEntry');
        $api->post('/log', 'App\Http\Controllers\ClockStateController@log');
    });

    $api->group(['prefix' => 'announcement'], function ($api) {
        $api->post('/', 'App\Http\Controllers\EssAnnouncementController@create');
        $api->get('/{id}', 'App\Http\Controllers\EssAnnouncementController@get');
        $api->post('/{id}/reply', 'App\Http\Controllers\EssAnnouncementController@reply');
        $api->put(
            '/{id}/recipient_seen',
            'App\Http\Controllers\EssAnnouncementController@updateRecipientSeen'
        );
        $api->get(
            '/reply/{id}',
            'App\Http\Controllers\EssAnnouncementController@getReply'
        );
        $api->put(
            '/reply/{id}/seen',
            'App\Http\Controllers\EssAnnouncementController@markReplyAsSeen'
        );
    });

    $api->get(
        '/company/{id}/default_schedule',
        'App\Http\Controllers\EssScheduleController@getCompaniesDefaultSchedule'
    );

    $api->get(
        '/attendance/{employee_id}/{date}',
        'App\Http\Controllers\EssAttendanceController@getSingleAttendanceRecord'
    );

    $api->group(['prefix' => 'teams'], function ($api) {
        $api->get('/', 'App\Http\Controllers\EssTeamController@index');

        $api->get('/{id:\d+}', 'App\Http\Controllers\EssTeamController@view');

        $api->get('/{id:\d+}/members', 'App\Http\Controllers\EssTeamController@getMembers');

        $api->get('/{id:\d+}/calendar', 'App\Http\Controllers\EssTeamController@getCalendar');

        $api->group(['prefix' => '/{teamId:\d+}/members/{memberId:\d+}'], function ($api) {

            $api->get('/', 'App\Http\Controllers\EssTeamMemberController@get');

            $api->get('/calendar', 'App\Http\Controllers\EssTeamMemberController@getCalendar');

            $api->get('/schedules', 'App\Http\Controllers\EssTeamMemberController@getSchedules');

            $api->get('/shifts', 'App\Http\Controllers\EssTeamMemberController@getShifts');

            $api->post('/shifts', 'App\Http\Controllers\EssTeamMemberController@assignShift');

            $api->get('/rest_day', 'App\Http\Controllers\EssTeamMemberController@getRestDay');

            $api->post('/rest_day', 'App\Http\Controllers\EssTeamMemberController@assignRestDay');

        });

        $api->group(['prefix' => '/{teamId:\d+}/attendance'], function ($api) {

            $api->get(
                '/regenerate/{jobId}/errors',
                'App\Http\Controllers\EssTeamAttendanceController@getErrors'
            );

            $api->get(
                '/regenerate/{jobId}/errors/download',
                'App\Http\Controllers\EssTeamAttendanceController@downloadErrors'
            );

            $api->get('/regenerate/{jobId}', 'App\Http\Controllers\EssTeamAttendanceController@getStatus');

            $api->post('/regenerate', 'App\Http\Controllers\EssTeamAttendanceController@regenerate');

        });
    });
});
