<?php

use Swagger\Annotations as SWG;

/**
 * @SWG\Response(
 *     response="JobResponse",
 *     description="Job Response",
 *     @SWG\Schema(
 *          type="object",
 *          required={"links", "data"},
 *          @SWG\Property(
 *              property="links",
 *              ref="#/definitions/JobLink"
 *          ),
 *          @SWG\Property(
 *              property="data",
 *              ref="#/definitions/JobData"
 *          )
 *     )
 * )
 *
 *
 * @SWG\Response(
 *     response="ErrorResponse",
 *     description="Errors Response",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", description="Message of the error"),
 *          @SWG\Property(
 *               property="errors",
 *               type="object"
 *          )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="ErrorResponseWithStatus",
 *     description="Error response with status_code",
 *     @SWG\Schema(ref="#/definitions/ErrorMessage")
 * )
 *
 * @SWG\Response(
 *     response="ErrorResponseMessage",
 *     description="Errors Response",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", description="Message of the error")
 *     )
 * )
 *
 * @SWG\Response(
 *     response="InvalidRequestResponse",
 *     description="Invalid request response",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", description="Message of the error"),
 *          @SWG\Property(property="status_code", type="integer", example=406)
 *     )
 * )
 *
 * @SWG\Response(
 *     response="InternalServerErrorResponse",
 *     description="Something went wrong",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", example="500 Internal Server Error."),
 *          @SWG\Property(property="status_code", type="integer", example=500)
 *     )
 * )
 *
 * @SWG\Response(
 *     response="NotFoundResponse",
 *     description="Not Found Response",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", example="Not found"),
 *          @SWG\Property(property="status_code", type="integer", example=404)
 *     )
 * )
 *
 * @SWG\Response(
 *     response="UnauthorizedResponse",
 *     description="Unauthorized Response",
 *     @SWG\Schema(
 *          @SWG\Property(property="message", type="string", example="Unauthorized"),
 *          @SWG\Property(property="status_code", type="integer", example=401)
 *     )
 * )
 */
