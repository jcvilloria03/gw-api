<?php

/**
 * @SWG\Swagger(
 *      schemes={"https"},
 *      basePath="/",
 *      @SWG\Info(
 *          version="1.0.0",
 *          title="Salarium API Gateway",
 *          description="Salarium API Gateway",
 *          @SWG\Contact(
 *              email="eng@salarium.com"
 *          )
 *     )
 * )
 */
