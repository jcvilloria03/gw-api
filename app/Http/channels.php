<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

$app[Illuminate\Contracts\Broadcasting\Factory::class]
    ->channel('user.{id}', function ($user, $id) {
        return (int) $user['user_id'] === (int) $id;
    });

$app[Illuminate\Contracts\Broadcasting\Factory::class]
    ->channel('ess-request.{id}', function ($user, $id) use ($app) {
        $channel = $app[App\Broadcast\EssRequestChannel::class];

        return $channel($user, $id);
    });

$app[Illuminate\Contracts\Broadcasting\Factory::class]
    ->channel('employee.{id}', function ($user, $id) {
        return (int) $user['employee_id'] === (int) $id;
    });
