<?php

namespace App\Http\Middleware;

use Closure;
use App\Response\CustomResponse as Response;
use App\User\UserService;
use App\Role\UserRoleService;
use App\Model\Auth0User;
use App\Model\Role;
use Dingo\Api\Facade\Route;

class AccessMiddleware
{
    /**
     * @var App\User\UserService
     */
    protected $userService;

    /**
     * @var App\Role\UserRoleService
     */
    protected $userRoleService;

    public function __construct(
        UserService $userService,
        UserRoleService $userRoleService
    ) {
        $this->userService = $userService;
        $this->userRoleService = $userRoleService;
    }

    /**
     * Handle an incoming request.
     * Check if user's current subscription is expired
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->get('user', []);

        if (empty($user)) {
            return $next($request);
        }

        $status = $this->userService->getStatus($user['user_id']);

        if ($status === Auth0User::STATUS_ACTIVE) {
            return $next($request);
        }

        $employeeRole = $this->getEmployeeRole($user['user_id']);

        $isEmployee = !$employeeRole->isEmpty();

        switch ($status) {
            case Auth0User::STATUS_SEMI_ACTIVE:
                if ($isEmployee) {
                    $isPayrollSubscribed = $this->isPayrollSubscribed($employeeRole);

                    if ($isPayrollSubscribed) {
                        $whiteListedRoutes = [
                            'account.get',
                            'account.philippine.companies',
                            'account.setup_progress',
                            'auth.user.change_password',
                            'ess.auth.change_password',
                            'ess.employee.user',
                            'ess.payslip.getByYear',
                            'ess.payslip.unread',
                            'ess.payslip.getById',
                            'ess.user.getById',
                            'user.informations',
                            'users.permissions'
                        ];

                        $isWhitelisted = in_array(Route::getCurrentRoute()->getName(), $whiteListedRoutes);

                        if ($isWhitelisted) {
                            return $next($request);
                        } else {
                            return $this->errorResponse(Response::ACTION_CODE_REDIRECT, Response::REDIRECT_ESS_MSG);
                        }
                    } else {
                        return $this->errorResponse(Response::ACTION_CODE_LOGOUT, Response::LOGOUT_SEMIACTIVE_MSG);
                    }
                } else {
                    return $this->errorResponse(Response::ACTION_CODE_LOGOUT, Response::ADMIN_LOGOUT_SEMIACTIVE_MSG);
                }
            default:
                if ($isEmployee) {
                    return $this->errorResponse(Response::ACTION_CODE_LOGOUT, Response::LOGOUT_INACTIVE_MSG);
                } else {
                    return $this->errorResponse(Response::ACTION_CODE_LOGOUT, Response::ADMIN_LOGOUT_INACTIVE_MSG);
                }
        }
    }

    private function getEmployeeRole($userId)
    {
        $constraints['role'] = function ($query) {
            $query->where('type', UserRoleService::EMPLOYEE);
        };

        $userRoles = $this->userRoleService->getUserRoles(
            $userId,
            ['role'],
            $constraints
        );

        return $userRoles;
    }

    private function isPayrollSubscribed($employeeRole)
    {
        $userRole = $employeeRole->first();

        if (!empty($userRole->module_access)) {
            $modules = $userRole->module_access;
        } else {
            $modules = $userRole->role->module_access;
        }

        return in_array(Role::ACCESS_MODULE_PAYROLL, $modules);
    }

    private function errorResponse($actionCode, $message)
    {
        $content = [
            'status_code' => Response::HTTP_ACCESS_REVOKE,
            'action_code' => $actionCode,
            'message' => $message
        ];

        return response()->json($content, Response::HTTP_ACCESS_REVOKE);
    }
}
