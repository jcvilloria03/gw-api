<?php

namespace App\Http\Middleware;

use App\Account\AccountRequestService;
use App\Authz\AuthzRequestService;
use App\Authz\AuthzDataScope;
use App\User\UserRequestService;
use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\UserEssentialData;
use App\Model\AccountEssentialData;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class AuthzMiddleware
{
    const AUTHZ_ENTITIES_HEADER = 'X-Authz-Entities';

    const AUTHZ_COMPANY_ID_HEADER = 'X-Authz-Company-Id';

    const DEFAULT_ACTION = 'IN_RESOURCE';

    const AUTHZ_HEADER_CHECK_EXCLUDES = [
        '/users/permissions',
        '/holiday/check_in_use',
        '/philippine/company_types',
        '/company/{id}/roles',
        '/user/bulk_delete',
        '/account/philippine/companies',
        '/account/payroll_groups',
        '/user/informations',
        '/banks',
        '/countries',
        '/philippine/employee/form_options',
        '/view/employee/user',
        '/payroll_loan_type/subtypes/SSS',
        '/payroll_loan_type/subtypes/Pag-ibig',
        '/company/other_income/{type}/upload/preview',
        '/company/{id}/other_income/{type}/upload/save',
        '/philippine/company/{id}/government_form_periods',
        '/company/{companyId}/shifts/download/{fileName}',
        '/view/{companyId}/attendance/user',
        '/payroll/upload/allowance/status',
        '/payroll/upload/bonus/status',
        '/payroll/upload/commission/status',
        '/payroll/upload/deduction/status',
        '/payroll/upload/attendance/status',
        '/payroll_group/upload/status',
        '/payroll_group/upload/preview',
        '/philippine/government_forms/bir/1601c_generate',
        '/government_forms',
        '/auth/user/change_password',
        '/auth/user/verify_resend'
    ];

    protected $modulesMap = [];

    protected $accountRequestService;

    protected $authzRequestService;


    public function __construct(AuthzRequestService $authzRequestService, AccountRequestService $accountRequestService)
    {
        $this->modulesMap = Config::get('modules_map');

        $this->authzRequestService = $authzRequestService;
        $this->accountRequestService = $accountRequestService;
    }

    public function handle(Request $request, Closure $next)
    {
        $routeUri = $request->route()[1]['uri'];
        $method = $request->method();

        $authzEntities = array_map(
            'trim',
            explode(',', $request->headers->get(self::AUTHZ_ENTITIES_HEADER))
        );

        if (!in_array($routeUri, self::AUTHZ_HEADER_CHECK_EXCLUDES)) {
            // Check if header is empty or has multiple modules on the same accordion
            $this->checkAuthzHeader($authzEntities);
        }

        $modules = $this->getModules($method, $routeUri);
        if (empty($modules)) {
            $request->attributes->set('authz_enabled', false);

            return $next($request);
        }

        $resource = $this->getResource($modules, $authzEntities);
        $subject = $this->getSubject($request);

        $clearance = $this->authzRequestService->checkSalariumClearance([
            'subject' => $subject,
            'action' => self::DEFAULT_ACTION,
            'resource' => $resource,
            'environment' => []
        ]);

        if (empty($clearance)) {
            abort(401, 'Unauthorized');
        }

        $request->attributes->set('authz_enabled', true);
        $request->attributes->set(
            'authz_data_scope',
            $this->makeAuthzDataScope($clearance, $subject['owner_account_id'], $subject)
        );

        return $next($request);
    }

    protected function makeAuthzDataScope(array $clearance, int $accountId, array $subject = [])
    {
        $accountEssentialData = $this->getAccountEssentialData($accountId);

        return AuthzDataScope::makeFromClearance($clearance, $accountEssentialData, $subject);
    }

    protected function getAccountEssentialData(int $accountId)
    {
        $essentialData = $this->getAccountEssentialDataFromTable($accountId);
        if ($essentialData === null) {
            $essentialDataResponse = $this->accountRequestService->getAccountEssentialData($accountId);
            $essentialData = json_decode($essentialDataResponse->getData(), true);

            try {
                $this->createAccountEssentialData($accountId, $essentialData);
            } catch (\Exception $e) {
                Log::error('[AuthzMiddleware][getAccountEssentialData] Error|' . $accountId . '|' . $e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }

        return [
            AuthzDataScope::SCOPE_COMPANY => array_keys($essentialData),
            AuthzDataScope::SCOPE_DEPARTMENT => array_merge(...array_column($essentialData, 'department')),
            AuthzDataScope::SCOPE_LOCATION => array_merge(...array_column($essentialData, 'location')),
            AuthzDataScope::SCOPE_POSITION => array_merge(...array_column($essentialData, 'position')),
            AuthzDataScope::SCOPE_PAYROLL_GROUP => array_merge(...array_column($essentialData, 'payroll_group')),
            AuthzDataScope::SCOPE_TEAM => array_merge(...array_column($essentialData, 'team')),
        ];
    }

    /**
     * Get subject
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getSubject(Request $request)
    {
        $companyId = intval($request->headers->get(self::AUTHZ_COMPANY_ID_HEADER));
        $user = $request->attributes->get('user');

        if (!$user || empty($user['user_id']) || empty($user['account_id'])) {
            return null;
        }

        $subject = $this->getUserEssentialData($user['user_id'], $user['account_id'], $companyId);

        if ($subject === null) {
            $userRequestService = app()->make(UserRequestService::class);
            $essentialData = $userRequestService->getEssentialData($user['user_id'], $companyId);

            try {
                $this->createUserEssentialData($essentialData);
                $subject = $essentialData['subject'];
            } catch (\Exception $e) {
                Log::error('[AuthzMiddleware][getSubject] Error|' . $user['user_id'] . '|' . $e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }

        if ($subject === null) {
            abort(401);
        }

        return array_merge($subject, [
            'owner_account_id' => $user['account_id']
        ]);
    }

    private function getUserEssentialData(int $userId, int $accountId, int $companyId = null)
    {
        try {
            $subject = UserEssentialData::select([
                    'user_id',
                    'account_id',
                    'role_id',
                    'company_id',
                    'employee_id',
                    'department_id',
                    'cost_center_id',
                    'position_id',
                    'location_id',
                    'payroll_group_id',
                    'team_id'
                ])
                ->where([
                    ['user_id', '=', $userId],
                    ['account_id', '=', $accountId]
                ])
                ->where('updated_at', '>=', Carbon::now()->subDay())
                ->when($companyId, function ($q) use ($companyId) {
                    $q->where('company_id', $companyId);
                })
                ->orderBy('company_id', 'asc')
                ->first();

            if ($subject !== null) {
                return $subject->toArray();
            }

            return $subject;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function createUserEssentialData(array $data)
    {
        try {
            if (!empty($data['subject'])) {
                DB::transaction(function () use ($data) {
                    $subject = $data['subject'];

                    UserEssentialData::where([
                        ['user_id', '=', $subject['user_id']],
                        ['account_id', '=', $subject['account_id']]
                    ])->delete();

                    foreach ($data['userData'] as $item) {
                        if (is_array($item)) {
                            UserEssentialData::create($item);
                        }
                    }
                });
            }
        } catch (\Exception $e) {
            Log::error('[createUserEssentialData] Error: ' . $e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return true;
    }

    private function getAccountEssentialDataFromTable(int $accountId)
    {
        try {
            $essentialResult = AccountEssentialData::select('data')
                ->where('account_id', $accountId)
                ->where('updated_at', '>=', Carbon::now()->subDay())
                ->first();

            if ($essentialResult !== null) {
                return $essentialResult->data;
            }

            return $essentialResult;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function createAccountEssentialData(int $accountId, array $attributes)
    {
        try {
            AccountEssentialData::where('account_id', $accountId)->delete();

            AccountEssentialData::create([
                'account_id' => $accountId,
                'data' => $attributes
            ]);
        } catch (\Exception $e) {
            Log::error('[createAccountEssentialData] Error: ' . $e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return true;
    }

    /**
     * Get modules for the given route URI and HTTP method
     *
     * @param  string $method   HTTP method name
     * @param  string $routeUri Route URI signature
     * @return array
     */
    protected function getModules(string $method, string $routeUri)
    {
        foreach ($this->modulesMap as $map) {
            if ($map['method'] == $method && $map['url'] === $routeUri) {
                return $map['modules'];
            }
        }

        return [];
    }

    /**
     * Get resource data for the given module list and authz entities
     *
     * @param  array  $modules          List of modules
     * @param  array  $authzEntities    List of Authz Entities
     * @return array
     */
    protected function getResource(array $modules, array $authzEntities)
    {
        return array_filter($modules, function ($moduleKey) use ($authzEntities) {
            return in_array($moduleKey, $authzEntities);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Find subject from user data list by company ID
     *
     * @param  array  $userData  List of user data/subject
     * @param  int    $companyId Company ID
     * @return array|null        Subject data for the given company ID
     */
    protected function findSubjectByCompanyId(array $userData, int $companyId)
    {
        $index = array_search($companyId, array_column($userData, 'company_id'));

        if ($index === false) {
            return null;
        }

        return $userData[$index];
    }

    protected function checkAuthzHeader(array $entities)
    {
        $entities = collect($entities)->filter();

        if ($entities->isEmpty()) {
            abort(412, 'Missing required authentication header.');
        }

        $uniqueAccordions = $entities
            ->map(function ($entity) {
                list($accordion) = explode('.', $entity);

                return $accordion;
            })
            ->unique();

        if ($uniqueAccordions->count() > 1) {
            abort(412, 'Module names must be from the same accordion.');
        }
    }
}
