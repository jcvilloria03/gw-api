<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpFoundation\Response;
use Closure;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * This Middleware assumes that UserMiddleware is called beforehand
 *
 */
class EmployeeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $companiesUsers = $request->attributes->get('companies_users');

        foreach ($companiesUsers as $companyUser) {
            if (!empty($companyUser['employee_id'])) {
                return $next($request);
            }
        }

        return response('You can only access ESS pages with Employee users.', Response::HTTP_UNAUTHORIZED);
    }
}
