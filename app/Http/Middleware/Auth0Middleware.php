<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpFoundation\Response;
use Auth0\SDK\JWTVerifier;
use Auth0\SDK\Helpers\Cache\FileSystemCacheHandler;
use Closure;
use Auth0\SDK\Exception\CoreException as Auth0CoreException;

class Auth0Middleware
{
    /**
     * Auth0\SDK\JWTVerifier
     */
    protected $verifier;

    public function __construct(JWTVerifier $verifier = null)
    {
        if ($verifier === null) {
            $cache = new FileSystemCacheHandler();
            $verifier = new JWTVerifier([
                'cache' => $cache,
                'supported_algs' => ['RS256'],
                'valid_audiences' => [getenv('PAYROLL_JWT_CLIENT_ID'), getenv('TA_JWT_CLIENT_ID')],
                'authorized_iss' => [getenv('JWT_TRUSTED_ISS')]
            ]);
        }
        $this->verifier = $verifier;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorizationHeader = $request->headers->get('Authorization');

        if (
            !$authorizationHeader ||
            strpos($authorizationHeader, 'Bearer ') !== 0
        ) {
            // no authorization header or not valid token format attached to header
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $token = str_replace('Bearer ', '', $authorizationHeader);

        if (empty($token)) {
            // no token after `Bearer` keyword
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        try {
            $request->attributes->add([
                'decodedToken' => $this->verifier->verifyAndDecode($token)
            ]);
        } catch (Auth0CoreException $e) {
            return response($e->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
