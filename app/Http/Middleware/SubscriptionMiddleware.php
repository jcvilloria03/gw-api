<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;
use App\Subscriptions\SubscriptionsRequestService;

class SubscriptionMiddleware
{

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $subscriptionRequestService;

    public function __construct(SubscriptionsRequestService $subscriptionRequestService)
    {
        $this->subscriptionRequestService = $subscriptionRequestService;
    }

    /**
     * Handle an incoming request.
     * Check if user's current subscription is expired
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userDetails = $request->get('user');

        $failedResponse = response(
            'No active subscription for user\'s account.',
            Response::HTTP_I_AM_A_TEAPOT
        );

        if (empty($userDetails['account_id'])) {
            return $failedResponse;
        }

        $response = $this->subscriptionRequestService
            ->getCustomerByAccountId($userDetails['account_id']);
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        // No subscription
        if (empty($customer['subscriptions'][0])) {
            return $failedResponse;
        }

        $subscription = $customer['subscriptions'][0];
        if ($subscription['is_expired']) {
            return $failedResponse;
        }

        return $next($request);
    }
}
