<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;
use App\CompanyUser\CompanyUserService;
use Illuminate\Support\Arr;

class UserMiddleware
{
    /**
     * App\CompanyUser\CompanyUserService
     */
    private $companyUserService;

    public function __construct(CompanyUserService $companyUserService)
    {
        $this->companyUserService = $companyUserService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->attributes->get('user_id');
        $accountId = $request->attributes->get('account_id');

        if (!$userId || !$accountId) {
            return response('Unauthorized.', Response::HTTP_UNAUTHORIZED);
        }

        $companyUsersData = $this->companyUserService->getAllByUserId($userId);

        $userData = $companyUsersData->map(function ($item) use ($userId, $accountId) {
            return [
                'user_id' => $userId,
                'employee_id' => Arr::get($item, 'employee_id'),
                'employee_company_id' => Arr::get($item, 'company_id'),
                'account_id' => $accountId,
            ];
        });

        if ($userData->isEmpty()) {
            $userData = collect([[
                'user_id'             => $userId,
                'employee_id'         => null,
                'employee_company_id' => null,
                'account_id'          => $accountId,
            ]]);
        }

        $request->attributes->set('user', $userData->first());
        $request->attributes->set('companies_users', $userData->toArray());

        return $next($request);
    }
}
