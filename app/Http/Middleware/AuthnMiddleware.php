<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpFoundation\Response;
use App\Authn\AuthnRequestService;
use App\Authn\AuthnService;
use Closure;

class AuthnMiddleware
{
    /**
     * @var \App\Authn\AuthnService
     */
    protected $authnService;

    public function __construct(AuthnService $authnService)
    {
        $this->authnService = $authnService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bearerToken = $request->headers->get('Authorization');

        if (!$bearerToken || strpos($bearerToken, 'Bearer ') !== 0) {
            // no authorization header or not valid token format attached to header
            return response(
                'Unauthenticated.',
                Response::HTTP_UNAUTHORIZED
            );
        }

        $accessToken = $this->authnService->validateToken($bearerToken);

        if (!$accessToken) {
            return response(
                'Unauthenticated.',
                Response::HTTP_UNAUTHORIZED
            );
        }

        $request->attributes->add([
                'user_id' => $accessToken['user_id'],
                'account_id' => $accessToken['account_id']
            ]);

        return $next($request);
    }
}
