<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Traits\AuditTrailTrait;

class AuditTrailMiddleware
{
    use AuditTrailTrait;

    const ACTION_VIEW = 'VIEW';

    public function handle(Request $request, Closure $next)
    {
        // process the actual request but do not return yet
        $response               = $next($request);
        $isRunningUnitTest      = app()->runningUnitTests();

        if ($isRunningUnitTest || !config('audit_trail.is_enable')) {
            return $response;
        }

        try {
            // All auditable GET endpoints will be automatically processed here
            // No need to manually add in controller
            if ($response->isSuccessful()) {
                $auditableEndpoint = $this->isInAuditEndpoints(
                    $request->route()[1]['uri'],
                    $request->headers->get('X-Authz-Entities')
                );

                if (!$auditableEndpoint) {
                    return $response;
                }

                $manualGetEndpoints = config('audit_trail.get_endpoints_manual');
                if (in_array($request->route()[1]['uri'], $manualGetEndpoints)) {
                    return $response;
                }

                $actionType = $this->getModuleActionType(
                    $request->method(),
                    $request->route()[1]['uri'],
                    $request->headers->get('X-Authz-Entities')
                );

                if ($request->method() == 'GET' || $actionType == self::ACTION_VIEW) {
                    $this->log($request, $response);
                }
            }
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailMiddleware][handle][ERROR]|[" . $request->route()[1]['uri'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());

            return $response;
        }

        return $response;
    }

    private function log(Request $request, $response)
    {
        try {
            $data = [];

            if ($response instanceof \Illuminate\Http\Response) {
                $data = $response->getOriginalContent();
            }

            if ($response instanceof \GuzzleHttp\Psr7\Response) {
                $data = $response->getBody()->getContents();
            }

            if ($response instanceof \Symfony\Component\HttpFoundation\Response) {
                $data = $response->getContent();
            }

            if ($response instanceof \Illuminate\Http\JsonResponse) {
                $data = $response->getData();
            }

            $data = is_string($data) ? json_decode($data, true) : $data;
            $data = is_object($data) ? (array) $data : $data;

            $this->audit($request, null, [], $data);
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailMiddleware][log][ERROR]|[" . $request->route()[1]['uri'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }

        return;
    }
}
