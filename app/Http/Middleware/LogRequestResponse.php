<?php

namespace App\Http\Middleware;

use App\Tracker\Tracker;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class LogRequestResponse
{
    /**
     * @var \App\Tracker\Tracker
     */
    private $tracker;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $handled;

    public function __construct(Tracker $tracker, LoggerInterface $logger)
    {
        $this->tracker = $tracker;
        $this->logger = $logger;
        $this->handled = false;
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$this->handled) {
            $this->handled = true;
            $this->tracker->initFromRequest($request);
            $trackingId = $this->tracker->getTrackingId();

            $this->logger->info('Request: ' . $trackingId, [
                'request' => [
                    'method' => $this->tracker->getRequest()->getMethod(),
                    'uri' => $this->tracker->getRequest()->path(),
                    'headers' => collect($this->tracker->getRequest()->headers->all())->except(['cookie'])->all(),
                    'query' => $this->tracker->getRequest()->query->all(),
                    'body' => $this->tracker->getRequest()->getContent(),
                ],
            ]);
        }


        return $next($request);
    }

    public function terminate($request, $response)
    {
        $trackingId = $this->tracker->getTrackingId();

        Log::info('Response: ' . $trackingId, [
            'response' => ['status_code' => $response->getStatusCode()]
        ]);
    }
}
