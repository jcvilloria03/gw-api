<?php

namespace App\Http\Middleware;

use App\Tracker\Tracker;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Log\LoggerInterface;

class Tracking
{
    /**
     * @var \App\Tracker\Tracker
     */
    private $tracker;

    private $handled;

    public function __construct(Tracker $tracker)
    {
        $this->tracker = $tracker;
        $this->handled = false;
    }

    public function handle(Request $request, Closure $next)
    {
        $this->tracker->initFromRequest($request);
        $response = $next($request);

        if (!$this->handled) {
            $this->handled = true;
            $response->headers
                ->set($this->tracker->getHeaderTrackingName(), $this->tracker->getTrackingId()->toString());
            $response->headers
                ->set($this->tracker->getHeaderTrackingSourceName(), $this->tracker->getTrackingSource());
        }

        return $response;
    }
}
