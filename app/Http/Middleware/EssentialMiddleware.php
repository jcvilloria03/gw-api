<?php

namespace App\Http\Middleware;

use App\Authz\AuthzService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EssentialMiddleware
{
    /**
     * @var AuthzService
     */
    private $authzService;

    public function __construct(AuthzService $authzService)
    {
        $this->authzService = $authzService;
    }

    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $method = $request->getMethod();
        $uri = $request->route()[1]['uri'];
        $statusCode = $response->getStatusCode();
        if ($this->authzService->isUriIncludesToClearEssential($method, $uri, $statusCode)) {
            $user = $request->attributes->get('user');
            $this->authzService->clearAccountCacheEssentialData($user['account_id']);
        }

        return $response;
    }
}
