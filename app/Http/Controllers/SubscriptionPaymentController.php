<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request as LaravelRequest;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Authz\AuthzDataScope;
use App\User\UserRequestService;
use App\SubscriptionPayment\SubscriptionPaymentAuthorizationService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class SubscriptionPaymentController extends Controller
{
    protected $client;

    /**
     * @var \App\SubscriptionPayment\SubscriptionPaymentAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->authorizationService = app(SubscriptionPaymentAuthorizationService::class);
        $this->userRequestService = app(UserRequestService::class);
    }

    public function passThroughToApi(LaravelRequest $request)
    {
        $requestContent = $request->getContent();
        $requestPath    = substr($request->path(), strlen('subscription-payment'));

        $jsonContent = null;
        if (!empty($requestContent)) {
            $jsonContent = json_decode($requestContent, true);
            // make sure content body is a valid json
            if (null === $jsonContent) {
                throw new HttpException(400, "Request content is an invalid json.");
            }
        }

        $requestQuery = $request->query();
        // authcheck
        $userReqAttribArr = $request->attributes->get('user');

        $userResponse   = $this->userRequestService->get($userReqAttribArr['user_id']);
        $userData       = json_decode($userResponse->getData());
        $requestQuery   = array_merge($requestQuery, ['account_id' => $userReqAttribArr['account_id']]);

        $isAuthzEnabled = $this->isAuthzEnabled($request);
        $requestMethod = $request->method();

        if (!$isAuthzEnabled) {
            $isAuthorized = true;
        } else {
            $authData = (object) [
                'account_id' => $userData->account_id
            ];

            $authMethods = [
                Request::METHOD_GET => 'authorizeGet',
                Request::METHOD_POST => 'authorizeCreate',
                Request::METHOD_PUT => 'authorizeUpdate',
                Request::METHOD_DELETE => 'authorizeDelete',
            ];

            $authMethod = $authMethods[$requestMethod];
            $isAuthorized = $this->authorizationService->$authMethod($authData, $userReqAttribArr);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        try {
            $options = [
                'headers' => [
                    'Authorization' => $request->header('Authorization'),
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'query' => $requestQuery,
            ];

            if (!empty($jsonContent)) {
                $options['json'] = $jsonContent;
            }

            $response = $this->client->request(
                $requestMethod,
                $requestPath,
                $options
            );

            return $response;
        } catch (BadResponseException $e) {
            // just throw the internal api reponse
            $response = $e->getResponse();

            $headers = $response->getHeaders();
            if (array_key_exists('SBPayment-Src', $headers)) {
                $baseUriArr = array_reverse(explode('.', $headers['SBPayment-Src'][0]));
                $headers['SBPayment-Src'] = $baseUriArr[min(count($baseUriArr)-1, 1)];
            }

            return new Response(
                $response->getBody()->getContents(),
                $response->getStatusCode(),
                $headers
            );
        }
    }
}
