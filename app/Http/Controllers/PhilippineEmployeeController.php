<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Model\Auth0User;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Role\UserRoleService;
use App\User\UserAuditService;
use App\Auth0\Auth0UserService;
use App\Role\DefaultRoleService;
use App\User\UserRequestService;
use App\Auth0\Auth0ManagementService;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeAuditService;
use App\CompanyUser\CompanyUserService;
use App\ProductSeat\ProductSeatService;
use App\Employee\EmployeeRequestService;
use App\Employee\EmployeeESIndexQueueService;
use App\FormOption\EmployeeFormOptionService;
use App\Employee\EmployeeAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Http\Controllers\Concerns\ProcessesEmployeeEmailUpdatesTrait;
use App\Http\Controllers\Concerns\ProcessesEmployeeActiveStatusUpdatesTrait;
use App\Traits\AuditTrailTrait;
use Carbon\Carbon;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class PhilippineEmployeeController extends Controller
{
    use ProcessesEmployeeEmailUpdatesTrait,
        ProcessesEmployeeActiveStatusUpdatesTrait,
        AuditTrailTrait;

    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const OBJECT_NAME = 'employee';

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $subscriptionsRequestService;


    /**
     * @var \App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $requestService;

    /**
     * @var \App\Employee\EmployeeAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeESIndexQueueService
     */
    protected $indexQueueService;

    /**
     * @var \App\ProductSeat\ProductSeatService
     */
    protected $productSeatService;

    /**
     * @var \App\Role\DefaultRoleService
     */
    protected $defaultRoleService;

    /**
     * @var \App\CompanyUser\CompanyUserService
     */
    protected $companyUserService;

    /**
     * @var \App\Role\UserRoleService
     */
    protected $userRoleService;

    public function __construct(
        PhilippineCompanyRequestService $companyRequestService,
        EmployeeRequestService $requestService,
        EmployeeAuthorizationService $authorizationService,
        AuditService $auditService,
        EmployeeESIndexQueueService $indexQueueService,
        ProductSeatService $productSeatService,
        DefaultRoleService $defaultRoleService,
        CompanyUserService $companyUserService,
        UserRoleService $userRoleService,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        $this->companyRequestService = $companyRequestService;
        $this->requestService        = $requestService;
        $this->authorizationService  = $authorizationService;
        $this->auditService          = $auditService;
        $this->indexQueueService     = $indexQueueService;
        $this->productSeatService    = $productSeatService;
        $this->defaultRoleService    = $defaultRoleService;
        $this->companyUserService    = $companyUserService;
        $this->userRoleService       = $userRoleService;
        $this->subscriptionsRequestService = $subscriptionsRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/employee/{id}",
     *     summary="Get Philippine Employee",
     *     description="Get philippine employee Details

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $isAuthorized = false;

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $response = $this->requestService->getEmployee($id);
            $employeeData = json_decode($response->getData());

            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $employeeData->company_id,
                    AuthzDataScope::SCOPE_DEPARTMENT => $employeeData->department_id,
                    AuthzDataScope::SCOPE_POSITION => $employeeData->position_id,
                    AuthzDataScope::SCOPE_LOCATION => $employeeData->location_id,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $employeeData->payroll_group->id,
                ]);
        } else {
            $response = $this->requestService->getEmployee($id);
            $employeeData = json_decode($response->getData());
            $companyId = $employeeData->company_id;
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());


            $isAuthorized = $this->authorizationService->authorizeGet(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/employee/{id}",
     *     summary="Update philippine employee",
     *     description="Update philippine employee details

Authorization Scope : **edit.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Philippine Employee Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "employee_id"={"type"="string"},
     *                 "payroll_group_name"={"type"="string"},
     *                 "work_location"={"type"="string"},
     *                 "department_name"={"type"="string"},
     *                 "rank_name"={"type"="string"},
     *                 "cost_center_name"={"type"="string"},
     *                 "position_name"={"type"="string"},
     *                 "first_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "email"={"type"="string"},
     *                 "gender"={"type"="string"},
     *                 "birth_date"={"type"="string"},
     *                 "consultant_tax_rate"={"type"="integer"},
     *                 "mobile_number"={"type"="string"},
     *                 "telephone_number"={"type"="integer"},
     *                 "email"={"type"="string"},
     *                 "address_line_1"={"type"="string"},
     *                 "address_line_2"={"type"="string"},
     *                 "city"={"type"="string"},
     *                 "country"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "tax_status"={"type"="string"},
     *                 "tax_type"={"type"="string"},
     *                 "base_pay"={"type"="number"},
     *                 "base_pay_unit"={"type"="string"},
     *                 "hours_per_day"={"type"="number"},
     *                 "date_hired"={"type"="string"},
     *                 "date_ended"={"type"="string"},
     *                 "sss_number"={"type"="string"},
     *                 "tin"={"type"="string"},
     *                 "rdo"={"type"="string"},
     *                 "hdmf_number"={"type"="string"},
     *                 "philhealth_number"={"type"="string"},
     *                 "sss_basis"={"type"="string"},
     *                 "sss_amount"={"type"="number"},
     *                 "hdmf_basis"={"type"="string"},
     *                 "hdmf_amount"={"type"="number"},
     *                 "philhealth_basis"={"type"="string"},
     *                 "philhealth_amount"={"type"="number"},
     *                 "active"={"type"="boolean"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        Auth0UserService $auth0UserService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = $request->all();
        $response = $this->requestService->getEmployee($id);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());

        if (!$this->authorizationService->authorizeUpdate($companyData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $attributes);

        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        // if email is changed, check if user exists in Auth0, then update email there too
        if (isset($attributes['email'])) {
            $this->processEmployeeEmailUpdate($responseData);
        }

        // process status
        if (isset($attributes['active'])) {
            $this->processEmployeeActiveStatus(
                $responseData,
                $attributes['active'],
                $auth0UserService->getUserByEmployeeId($responseData['id'])
            );
        }

        $this->indexQueueService->queue([$id]);

        // trigger audit trail
        $this->audit($request, $companyId, $responseData, $employeeData);

        return $response->setData($responseData);
    }

    /**
     * @SWG\Post(
     *     path="/philippine/employee",
     *     summary="Create philippine employee",
     *     description="Create philippine employee

Authorization Scope : **create.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create Philippine Employee",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "employee_id"={"type"="string"},
     *                 "first_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "email"={"type"="string"},
     *                 "mobile_number"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "country"={"type"="string"},
     *                 "birth_date"={"type"="string"},
     *                 "gender"={"type"="string"},
     *                 "address_line_1"={"type"="string"},
     *                 "address_line_2"={"type"="string"},
     *                 "city"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "status"={"type"="string"},
     *                 "date_hired"={"type"="string"},
     *                 "date_ended"={"type"="string"},
     *                 "employment_type_name"={"type"="string"},
     *                 "position_name"={"type"="string"},
     *                 "department_name"={"type"="string"},
     *                 "rank_name"={"type"="string"},
     *                 "team_name"={"type"="string"},
     *                 "team_role"={"type"="string"},
     *                 "primary_location_name"={"type"="string"},
     *                 "secondary_location_name"={"type"="string"},
     *                 "timesheet_required"={"type"="string"},
     *                 "overtime"={"type"="string"},
     *                 "differential"={"type"="string"},
     *                 "regular_holiday_pay"={"type"="string"},
     *                 "special_holiday_pay"={"type"="string"},
     *                 "holiday_premium_pay"={"type"="string"},
     *                 "rest_day_pay"={"type"="string"},
     *                 "hours_per_day"={"type"="integer"},
     *                 "product_seat_id"={"type"="integer"},
     *                 "active"={"type"="boolean"},
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(
        Request $request,
        Auth0UserService $auth0UserService
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = $request->all();

        $this->validate($request, [
            'company_id' => 'required|integer',
            'product_seat_id' => 'required|integer',
            'active' => 'boolean',
        ]);

        $companyResponse = $this->companyRequestService->get($attributes['company_id']);
        $companyData = json_decode($companyResponse->getData());

        $user = $request->attributes->get('user');
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $attributes['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->createPhilippineEmployee(
            $attributes,
            $this->getAuthzDataScope($request)
        );

        // if there are validation errors in the create request, display these errors
        if (!$response->isSuccessful()) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        $philippineEmployeeData = $responseData['data'];
        $companyUserData        = $philippineEmployeeData['user'];

        // create company user record
        $companyUser = $this->companyUserService->create([
            'user_id'     => $companyUserData['id'],
            'company_id'  => $philippineEmployeeData['company_id'],
            'employee_id' => $philippineEmployeeData['id']
        ]);

        $auth0User = $auth0UserService->create([
            'auth0_user_id' => Auth0UserService::generatePreactiveId(
                $philippineEmployeeData['account_id'],
                $companyUser->user_id
            ),
            'user_id'       => $companyUser->user_id,
            'account_id'    => $philippineEmployeeData['account_id'],
            'status'        => $attributes['active']
        ]);

        if (isset($attributes['active']) && $attributes['active'] === true) {
            $this->processEmployeeActiveStatus(
                $philippineEmployeeData,
                $attributes['active'],
                $auth0User
            );
        }

        $this->indexQueueService->queue([$philippineEmployeeData['id']]);

        // trigger audit trail
        $newData = array_merge($philippineEmployeeData['data'], $request->all());
        $this->audit($request, $philippineEmployeeData['company_id'], $newData);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employee",
     *     summary="Create philippine employee with payroll and/or time and attendance",
     *     description="Create philippine employee with payroll and/or time and attendance

Authorization Scope : **create.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create Philippine Employee with Payroll and Time and Attendance",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "employee_id"={"type"="string"},
     *                 "first_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "email"={"type"="string"},
     *                 "mobile_number"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "country"={"type"="string"},
     *                 "birth_date"={"type"="string"},
     *                 "gender"={"type"="string"},
     *                 "address_line_1"={"type"="string"},
     *                 "address_line_2"={"type"="string"},
     *                 "city"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "status"={"type"="string"},
     *                 "date_hired"={"type"="string"},
     *                 "date_ended"={"type"="string"},
     *                 "employment_type_name"={"type"="string"},
     *                 "position_name"={"type"="string"},
     *                 "department_name"={"type"="string"},
     *                 "rank_name"={"type"="string"},
     *                 "team_name"={"type"="string"},
     *                 "team_role"={"type"="string"},
     *                 "primary_location_name"={"type"="string"},
     *                 "secondary_location_name"={"type"="string"},
     *                 "timesheet_required"={"type"="string"},
     *                 "overtime"={"type"="string"},
     *                 "differential"={"type"="string"},
     *                 "regular_holiday_pay"={"type"="string"},
     *                 "special_holiday_pay"={"type"="string"},
     *                 "holiday_premium_pay"={"type"="string"},
     *                 "rest_day_pay"={"type"="string"},
     *                 "hours_per_day"={"type"="integer"},
     *                 "product_seat_id"={"type"="integer"},
     *                 "active"={"type"="boolean"},
     *                 "payroll_group_name"={"type"="string"},
     *                 "sss_number"={"type"="string"},
     *                 "tin"={"type"="string"},
     *                 "hdmf_number"={"type"="string"},
     *                 "philhealth_number"={"type"="string"},
     *                 "work_location"={"type"="string"},
     *                 "sss_basis"={"type"="string"},
     *                 "hdmf_basis"={"type"="string"},
     *                 "philhealth_basis"={"type"="string"},
     *                 "entitled_deminimis"={"type"="string"},
     *                 "payment_method"={"type"="string"},
     *                 "bank_name"={"type"="string"},
     *                 "bank_account_number"={"type"="string"},
     *                 "bank_type"={"type"="string"},
     *                 "userlink_confirmed"={"type"="boolean"},
     *                 "active"={"type"="string"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createEmployee(
        Request $request,
        Auth0UserService $auth0UserService,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = collect($request->all())
            ->map(function ($item) {
                return is_string($item)
                    ? trim($item)
                    : $item;
            })
            ->toArray();

        $this->validate($request, [
            'company_id' => 'required|integer',
            'product_seat_id' => 'required|integer'
        ]);

        if ($this->isAuthzEnabled($request)) {
            if (!$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $attributes['company_id']
            )) {
                $this->response->errorUnauthorized();
            }
        }

        $companyResponse = $this->companyRequestService->get($attributes['company_id']);
        $companyData = json_decode($companyResponse->getData());

        $user = $request->attributes->get('user');
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeCreate($companyData, $user)
        ) {
            $this->response()->errorUnauthorized();
        }

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId($companyData->account_id);
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (!empty($customer['subscriptions'][0]['is_expired'])) {
            $this->invalidRequestError('Company owner does not have an active subscription.');
        }

        //Get Product Seat Name
        $productSeatName = $this->productSeatService->getProductSeatName($attributes['product_seat_id']);
        $subscription = $customer['subscriptions'][0];

        // Check if param product seat matches subscription licenses
        $subscriptionLicenseFound = false;
        foreach ($subscription['subscription_licenses'] as $subscriptionLicense) {
            // Subscription license match with param product seat
            if ($productSeatName == $subscriptionLicense['product']['code']) {
                $subscriptionLicenseFound = true;
            }
        }

        if (!$subscriptionLicenseFound) {
            $this->invalidRequestError("Product seat id does not match with subscription");
        }

        // Get subscription stats
        $response = $subscriptionsRequestService->getCustomerSubscriptionStats($subscription['id']);
        $subscriptionStats = json_decode($response->getData(), true);
        $subscriptionStats = $subscriptionStats['data'] ?? null;

        if (empty($subscriptionStats)) {
            $this->invalidRequestError('Failed to get customer subscription usage statistics.');
        }

        // Check if there's license available for param product seat
        $subscriptionStatsProductFound = false;
        foreach ($subscriptionStats['products'] as $licenseProduct) {
            // Subscription license match with param product seat
            if ($productSeatName == $licenseProduct['product_code']) {
                $subscriptionStatsProductFound = true;
                if ($licenseProduct['licenses_available'] <= 0) {
                    $subscriptionStatsProductFound = false;
                }
            }
        }
        if (!$subscriptionStatsProductFound) {
            $this->invalidRequestError('Account does not have an available license.');
        }

        //Determine which flow will create the employee; Payroll or TimeAndAttendance
        if (!empty($attributes['date_hired'])) {
            $attributes['date_hired'] = Carbon::parse($attributes['date_hired'])->format('M-d-Y');
        }
        if (!empty($attributes['date_ended'])) {
            $attributes['date_ended'] =  Carbon::parse($attributes['date_ended'])->format('M-d-Y');
        }

        if (stripos($productSeatName, 'payroll') !== false) {
            //Payroll + Time and Attendance
            if (stripos($productSeatName, 'time and attendance') !== false) {
                //Send flag to create time And attendance data
                $attributes['should_create_time_and_attendance'] = true;
            }
            $response = $this->requestService->createTAPayrollEmployee(
                $attributes,
                $this->getAuthzDataScope($request)
            );
        } else {
            //Create TimeAndAttendance Employee
            $response = $this->requestService->createPhilippineEmployee(
                $attributes,
                $this->getAuthzDataScope($request)
            );
        }

        if (!$response->isSuccessful()) {
            // if there are validation errors in the create request, display these errors
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        $philippineEmployeeData = $responseData['data'];
        $companyUserData        = $philippineEmployeeData['user'];

        // create company user record
        $companyUser = $this->companyUserService->create([
            'user_id'     => $companyUserData['id'],
            'company_id'  => $philippineEmployeeData['company_id'],
            'employee_id' => $philippineEmployeeData['id']
        ]);

        $auth0User = null;
        if (!empty($attributes['userlink_confirmed'])) {
            $auth0User = $auth0UserService->getUser($companyUser->user_id);
        }

        if (empty($auth0User)) {
            // create preactive auth0 record
            $auth0User = $auth0UserService->create([
                'auth0_user_id' => Auth0UserService::generatePreactiveId(
                    $philippineEmployeeData['account_id'],
                    $companyUser->user_id
                ),
                'user_id'       => $companyUser->user_id,
                'account_id'    => $philippineEmployeeData['account_id'],
                'status'        => $attributes['active']
            ]);
        }

        if (isset($attributes['active']) && in_array($attributes['active'], ['active', "semi-active"])) {
            $this->processEmployeeActiveStatus(
                $philippineEmployeeData,
                true,
                $auth0User
            );
        } else {
            // Create user_roles
            $this->assignDefaultEmployeeRolesToUser(
                $auth0User,
                $philippineEmployeeData['account_id'],
                [
                    [
                        'company_id' => $philippineEmployeeData['company_id']
                    ]
                ]
            );
        }

        $this->indexQueueService->queue([$philippineEmployeeData['id']]);

        // Always use the owner id of the subscription for assigning license
        // regardless if the creator is admin
        // to avoid validation error from the assign license endpoint
        // since admins doesn't have subscriptions

        // Get product name mapping from SB-API
        $productResponse = json_decode($this->subscriptionsRequestService
        ->getSubscriptionProductId($productSeatName)->getData(), true);
        $productId = $productResponse['data'][0]['id'] ?? '';
        $jobId = uniqid();
        if (!empty($productId)) {
            // Create subscription license units and usage entry
            $this->subscriptionsRequestService->assign(
                $subscription['user_id'],
                [
                    [
                        "entity_id" => $companyUser->user_id,
                        "product_id" => $productId,
                    ]
                ],
                $jobId
            );
            // Apply licenses to CP-API and GW-API
            $this->subscriptionsRequestService->applyLicenses(
                [(string) $companyUser->user_id],
                $productSeatName
            );
        }

        $responseData = [
            'data' => [
                'employee_id' => $philippineEmployeeData['id'],
                'user_id' => !empty($companyUser->user_id) ? $companyUser->user_id : '',
                'job_id' => !empty($jobId) ? $jobId : ''
            ]
        ];

        // trigger audit trail
        $newData = array_merge($responseData['data'], $request->all());
        $this->audit($request, $philippineEmployeeData['company_id'], $newData);

        return response()->json($responseData);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/employee/form_options",
     *     summary="Get Employee form options",
     *     description="Get list of Employee form option values",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getFormOptions(EmployeeFormOptionService $employeeFormOptionService)
    {
        return response()->json([
            'data' => $employeeFormOptionService->getPhilippineEmployeeFormOptions()
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/employee/has_usermatch",
     *     summary="Check if employee details match existing user",
     *     description="Check if employee details match existing user

Authorization Scope : **create.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create Philippine Employee with Payroll and Time and Attendance",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "first_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "email"={"type"="string"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function hasMatchingUser(Request $request, UserRequestService $userService)
    {
        $this->validate($request, [
            'company_id'  => 'required|integer',
            'first_name'  => 'required',
            'last_name'   => 'required',
            'email'       => 'required|email'
        ]);

        $params = collect($request->only([
                'first_name',
                'middle_name',
                'last_name',
                'email'
            ]))
            ->map(function ($item) {
                return trim($item);
            })
            ->toArray();

        $isAuthorized = false;

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->input('company_id'));
        } else {
            $companyResponse = $this->companyRequestService->get($request->get('company_id'));
            $companyData     = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $matchedUserResponse = $userService->getUserByEmail($params['email']);
        $matchedUser         = json_decode($matchedUserResponse->getData(), true);

        return response()->json($matchedUser);
    }
}
