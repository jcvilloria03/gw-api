<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Commission\CommissionAuthorizationService;
use App\Commission\PhilippineCommissionRequestService;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\OtherIncome\OtherIncomeAuditService;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PhilippineCommissionController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Commission\PhilippineCommissionRequestService
     */
    protected $requestService;

    /**
     * @var \App\Commission\CommissionAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncome\OtherIncomeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineCommissionRequestService $requestService,
        CommissionAuthorizationService $authorizationService,
        OtherIncomeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/commission/bulk_create",
     *     summary="Assign Multiple Commissions",
     *     description="Assign multiple commissions,
Authorization Scope : **create.commissions**",
     *     tags={"commission"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="commissions",
     *         in="body",
     *         description="Create multiple commissions",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/commissionsRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="commissionsRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/commissionsRequestItem"),
     *     collectionFormat="multiple"
     * ),
     * @SWG\Definition(
     *     definition="commissionsRequestItem",
     *     @SWG\Property(
     *         property="type_id",
     *         type="integer",
     *         description="Commission type id",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number",
     *         description="Amount of commission if type basis is fixed",
     *         example="100"
     *     ),
     *     @SWG\Property(
     *         property="percentage",
     *         type="integer",
     *         description="Percentage of commission if type basis i salary based",
     *         example="10"
     *     ),
     *     @SWG\Property(
     *         property="recipients",
     *         type="object",
     *             @SWG\Property(
     *              property="employees",
     *              type="array",
     *              items={"type"="integer"},
     *              description="Array of employee ids this commission will be assigned",
     *              example="10"
     *             ),
     *             @SWG\Property(
     *              property="payroll_groups",
     *              type="array",
     *              items={"type"="integer"},
     *              description="Array of payroll group ids this commission will be assigned",
     *              example="10"
     *             ),
     *             @SWG\Property(
     *              property="departments",
     *              type="array",
     *              items={"type"="integer"},
     *              description="Array of department ids this commission will be assigned",
     *              example="10"
     *             ),
     *     ),
     *     @SWG\Property(
     *         property="release_details",
     *         type="array",
     *         items={"type"="object",
     *                  "properties"={
     *                      "id"={"type"="integer", "example"=1},
     *                      "date"={"type"="string", "example"="2019-02-22"},
     *                      "disburse_through_special_pay_run"={"type"="boolean", "example"="1"},
     *                      "status"={"type"="integer", "example"=1}
     *                  }
     *              }
     *     ),
     * )
     */
    public function bulkCreate(
        Request $request,
        EmployeeRequestService $employeeService,
        $companyId
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;
        $errorMesssage = 'Unauthorized';

        if ($this->isAuthzEnabled($request)) {
            $authorizedCompany = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            $employeeIds = array_flatten($request->json('*.recipients.employees'), 1);
            $payrollGroupIds = array_flatten($request->json('*.recipients.payroll_groups'), 1);

            $unAuthorizedEmployees = collect();
            if (!empty($employeeIds)) {
                $employeeResponse = $employeeService->getCompanyEmployeesById($companyId, $employeeIds);
                if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
                    return $employeeResponse;
                }

                $unAuthorizedEmployees = collect(json_decode($employeeResponse->getData(), true))->reject(
                    function ($employee) use ($request) {
                        return $this->getAuthzDataScope($request)->isAllAuthorized([
                            AuthzDataScope::SCOPE_COMPANY => [data_get($employee, 'company_id')],
                            AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group_id'),
                        ]);
                    }
                );
            }

            $authorizedPayrollGroups = true;
            if (!empty($payrollGroupIds)) {
                $authorizedPayrollGroups = $this->getAuthzDataScope($request)->isAllAuthorized([
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                ]);
            }

            if (!$unAuthorizedEmployees->isEmpty()) {
                $errorMesssage = 'Unauthorized: Unable to process employees:[' .
                    implode(',', $unAuthorizedEmployees->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = $authorizedCompany
                    && $unAuthorizedEmployees->isEmpty()
                    && $authorizedPayrollGroups;
            }
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMesssage);
        }

        // call microservice
        $response = $this->requestService->bulkCreate($companyId, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $commissionIds = json_decode($response->getData(), true);
        $commissions = collect($inputs)->map(function ($commission, $index) use ($commissionIds) {
            array_set($commission, 'id', array_get($commissionIds, $index));
            array_set($commission, 'type', 'App\Model\PhilippineCommission');
            return $commission;
        })->toArray();

        if ($response->isSuccessful()) {
            $this->audit($request, $companyId, $commissions);
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/commission/{commissionId}",
     *     summary="Update the commission",
     *     description="Update the commission
Authorization Scope : **edit.commissiones**",
     *     tags={"commission"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="commissionId",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="commission",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/commissionsRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeRequestService $otherIncomeRequestService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $commissionResponse = $otherIncomeRequestService->get($id);
        if (!$commissionResponse->isSuccessful()) {
            return $commissionResponse;
        }
        $commissionData = json_decode($commissionResponse->getData(), true);
        $companyId = array_get($commissionData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $authData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->update($id, $request->all(), $this->getAuthzDataScope($request));
        if ($response->isSuccessful()) {
            $updatedData = json_decode($response->getData(), true);
            $oldData = $commissionData;

            $this->audit($request, $companyId, $updatedData, $oldData, true);
        }
        return $response;
    }
}
