<?php

namespace App\Http\Controllers;

use App\Audit\AuditService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\ESS\EssOvertimeRequestRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\ClockState\ClockStateService;
use App\Transformer\TimeSheetTransformer;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection;

/**
 * Class OvertimeRequestController
 *
 * @package App\Http\Controllers
 */
class OvertimeRequestController extends Controller
{

    /**
     * @var \App\ESS\EssOvertimeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Transformer\TimeSheetTransformer
     */
    private $transformer;

    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    /**
     * OvertimeRequestController constructor.
     *
     * @param EssOvertimeRequestRequestService    $requestService       EssOvertimeRequestRequestService
     * @param EmployeeRequestAuthorizationService $authorizationService EmployeeRequestAuthorizationService
     * @param AuditService                        $auditService         AuditService
     * @param TimeSheetTransformer                $transformer          TimeSheetTransformer
     * @param FractalManager                      $fractal              FractalManager
     */
    public function __construct(
        EssOvertimeRequestRequestService $requestService,
        EmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService,
        TimeSheetTransformer $transformer,
        FractalManager $fractal
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
    }

    /**
     * @SWG\Get(
     *     path="/overtime_request/{id}",
     *     summary="Get Overtime Request",
     *     description="Get Overtime Request
     Authorization Scope : view.request",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Overtime Request ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id,
        ClockStateService $clockStateService
    ) {
        $user = $request->attributes->get('user');

        $response = $this->requestService->get($id);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $overtimeRequest = json_decode($response->getData());

        $authorized = true;
        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $companyId = $overtimeRequest->request->company_id;
            $accountId = $companyId ? $user['account_id'] : null;

            $overtimeRequest->account_id = (int)$accountId;
            $overtimeRequest->company_id = $companyId;

            $workflows = $overtimeRequest->workflow_levels;

            $authorized = $this->authorizationService->authorizeViewSingleRequest($user, $workflows, $overtimeRequest);

            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        // fetch timesheet
        try {
            $shiftStart = collect($overtimeRequest->shifts)->pluck('start')->unique()->all();
            $shiftEnd = collect($overtimeRequest->shifts)->pluck('end')->unique()->all();

            $timeSheets = $clockStateService->getFormattedTimesheet(
                $overtimeRequest->employee_id,
                $shiftStart,
                $shiftEnd
            );
            $overtimeRequest->timesheet = isset($timeSheets['data']) ? $timeSheets['data'] : [];
        } catch (\Throwable $e) {
            \Log::error($e->getMessage() . ' : No timesheet data = ' . $overtimeRequest->employee_id);
            \Log::error($e->getTraceAsString());
        }

        return $this
            ->response
            ->array((array) $overtimeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
