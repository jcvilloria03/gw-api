<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Approval\ApprovalAuditService;
use App\ESS\EssApprovalRequestService;
use App\Http\Controllers\EssBaseController;
use App\Approval\EssApprovalAuthorizationService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\ESS\EssApprovalService;
use App\User\UserService;

/**
 * Class EssApprovalController
 *
 * @package App\Http\Controllers
 */
class EssApprovalController extends EssBaseController
{
    /**
     * @var \App\ESS\EssApprovalRequestService
     */
    protected $requestService;

    /**
     * @var \App\Approval\EssApprovalAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $essEmployeeRequestAuthorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\ESS\EssApprovalService
     */
    private $essApprovalService;


    public function __construct(
        EssApprovalRequestService $requestService,
        EssApprovalAuthorizationService $authorizationService,
        EssEmployeeRequestAuthorizationService $essEmployeeRequestAuthorizationService,
        AuditService $auditService,
        EssApprovalService $essApprovalService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->essEmployeeRequestAuthorizationService = $essEmployeeRequestAuthorizationService;
        $this->auditService = $auditService;
        $this->essApprovalService = $essApprovalService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/employee/approvals",
     *     summary="Get employee Approvals",
     *     description="Get all Approvals for employee.",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ApprovalFilter"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="ApprovalFilter",
     *     required={"page"},
     *     @SWG\Property(
     *         property="page",
     *         type="integer",
     *         description="Page number. It should be >= 1 . Default is 1."
     *     ),
     *     @SWG\Property(
     *         property="types",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         description="List of valid request types.
 Possible values are: leaves, loans, overtime, shift_change, time_dispute, undertime."
     *     ),
     *     @SWG\Property(
     *         property="my_level",
     *         type="boolean",
     *         description="Show requests on exact approver level
 or if it's false, exact level and levels below."
     *     ),
     *     @SWG\Property(
     *         property="statuses",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         description="List of valid request statuses.
 Possible values are: pending, approved, declined, cancelled, rejected."
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         description="A inclusive lower date boundary for filtering YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         description="A inclusive upper date boundary for filtering YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/affected_employees"
     *         )
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="affected_employees",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Possible values: department, position, location, employee."
     *     )
     * )
     */
    public function getApprovals(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->essEmployeeRequestAuthorizationService->authorizeView($user)
        ) {
            $this->response()->errorUnauthorized();
        }

        $employeeId = $user['employee_id'];
        $attributes = $request->all();
        $attributes['account_id'] = $request->attributes->get('user')['account_id'];
        if (empty($attributes['company_id'])) {
            $attributes['company_id'] = $user['employee_company_id'];
        }
        $attributes['user_id'] = $user['user_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['user_id'],
            $user['employee_id']
        );

        if (!empty($attributes['employee_relations']['company_ids'])) {
            $attributes['company_ids'] = $attributes['employee_relations']['company_ids'];
            unset($attributes['employee_relations']['company_ids']);
        }

        return $this->requestService->getEmployeeApprovals($employeeId, $attributes);
    }

    /**
     * @SWG\Post(
     *     path="/ess/request/bulk_decline",
     *     summary="Decline multiple requests",
     *     description="Decline requests with given id's.",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests_ids[]",
     *         in="formData",
     *         description="Employee Requests Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDecline(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeApprove($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        $attributes = $request->all();

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        $attributes['company_id'] = $attributes['company_id'];
        $attributes['account_id'] = $request->attributes->get('user')['account_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        $this->requestService->bulkDecline($attributes);

        return $this->response->noContent();
    }

    /**
     * @SWG\Post(
     *     path="/ess/request/bulk_approve",
     *     summary="Approve multiple requests",
     *     description="Approve requests with given id's.",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests_ids[]",
     *         in="formData",
     *         description="Employee Requests Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkApprove(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeApprove($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        $attributes = $request->all();

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        $attributes['company_id'] = isset($attributes['company_id'])
            ? $attributes['company_id']
            : $user['employee_company_id'];
        $attributes['account_id'] = $user['account_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        $this->requestService->bulkApprove($attributes);

        return $this->response->noContent();
    }

    /**
     * @SWG\Post(
     *     path="/ess/request/bulk_cancel",
     *     summary="Cancel multiple requests",
     *     description="Cancel requests with given id's.",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests[]",
     *         in="formData",
     *         description="Employee Requests",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkCancel(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeApprove($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        $attributes = $request->all();
        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];

        if (!empty($attributes['employee_requests'])) {
            $attributes['employee_requests_ids'] = collect($attributes['employee_requests'])
                ->pluck('id')->values()->toArray();
        }

        $attributes['account_id'] = $user['account_id'];
        $attributes['company_id'] = $attributes['company_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        if (!empty($attributes['employee_relations']['company_ids'])) {
            $attributes['company_ids'] = $attributes['employee_relations']['company_ids'];
            unset($attributes['employee_relations']['company_ids']);
        }

        $this->requestService->bulkCancel($attributes);

        return $this->response->noContent();
    }
}
