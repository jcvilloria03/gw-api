<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Department\DepartmentAuditService;
use App\Department\DepartmentAuthorizationService;
use App\Department\DepartmentEsIndexQueueService;
use App\Department\DepartmentRequestService;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;

class DepartmentController extends Controller
{
    /**
     * @var \App\Department\DepartmentRequestService
     */
    private $requestService;

    /**
     * @var \App\Department\DepartmentAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\Department\DepartmentEsIndexQueueService
     */
    private $indexQueueService;

    public function __construct(
        DepartmentRequestService $requestService,
        DepartmentAuthorizationService $authorizationService,
        AuditService $auditService,
        DepartmentEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }


    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/department/{id}",
     *     summary="Get Department",
     *     description="Get Department Details
Authorization Scope : **view.department**",
     *     tags={"department"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Department ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "id"=0,
     *                 "name"="Human Resources",
     *                 "company_id"=0,
     *                 "account_id"=0,
     *                 "description": "Lorem ipsum dolor sit amet",
     *                 "parent_id": 0,
     *                 "parent": 0,
     *             }
     *         },
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *             "application/json": {
     *                 "message"="Department not found."
     *             }
     *         },
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $departmentData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($departmentData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     deprecated=true,
     *     path="/department/",
     *     summary="Create company department",
     *     description="Create department
Authorization Scope : **create.department**",
     *     tags={"department"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Department Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="Department Description",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="parent_id",
     *         in="formData",
     *         description="Parent Department Id - Refers to which department this department will be under",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "id"=0
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Department name already exists on the given company"
     *             }
     *         }
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $departmentData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($departmentData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        $this->indexQueueService->queue([$responseData['id']]);

        // audit log
        $departmentGetResponse = $this->requestService->get($responseData['id']);
        $departmentData = json_decode($departmentGetResponse->getData(), true);
        $details = [
            'new' => $departmentData,
        ];
        $item = new AuditCacheItem(
            DepartmentAuditService::class,
            DepartmentAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/department/is_name_available",
     *     summary="Is department name available",
     *     description="Check availability of department name with the given company",
     *     tags={"department"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Department Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="department_id",
     *         in="formData",
     *         description="Department Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $user = $request->attributes->get('user');

            $departmentData = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $departmentData,
                $user
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($companyId, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/departments",
     *     summary="Get all Departments",
     *     description="Get all Department within company.

Authorization Scope : **view.department**",
     *     tags={"department"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="mode",
     *         in="query",
     *         description="Response mode",
     *         required=false,
     *         type="string",
     *         enum={"FILTER_MODE","DEFAULT_MODE"},
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyDepartments(Request $request, int $companyId)
    {
        $params = $request->only([
            'mode',
            'keyword',
            'sort_by',
            'sort_order',
            'page',
            'per_page'
        ]);
        $authzEnabled = $request->attributes->get('authz_enabled');
        $dataScope = $request->attributes->get('authz_data_scope');

        if ($authzEnabled && !$dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyDepartments($companyId, $params, $dataScope);
        $departmentData = json_decode($response->getData(), true)['data'];

        if (empty($departmentData)) {
            return $response;
        }

        if (
            !$authzEnabled &&
            !$this->authorizationService->authorizeGetCompanyDepartments(
                current($departmentData),
                $request->attributes->get('user')
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/department/bulk_create",
     *     summary="Create company departments",
     *     description="Create company departments

Authorization Scope : **create.department**",
     *     tags={"department"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/NewDepartments"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="NewDepartments",
     *     required={"company_id", "data"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/NewDepartment"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="NewDepartment",
     *     required={"name"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="parent_id",
     *         type="integer",
     *     ),
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $departmentData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($departmentData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        $departmentIds = [];

        // audit log
        foreach ($responseData['data'] as $department) {
            $departmentGetResponse = $this->requestService->get($department['id']);
            $departmentData = json_decode($departmentGetResponse->getData(), true);
            $details = [
                'new' => $departmentData,
            ];
            $item = new AuditCacheItem(
                DepartmentAuditService::class,
                DepartmentAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);

            $departmentIds[] = $department['id'];
        }

        $this->indexQueueService->queue($departmentIds);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/department/bulk_update",
     *     summary="Update company departments",
     *     description="Update company departments

Authorization Scope : **edit.department**",
     *     tags={"department"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ExistingDepartments"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *              "id": 0,
     *              "name": "string",
     *              "company_id": 0,
     *              "account_id": 0,
     *              "description": null,
     *              "parent_id": 0,
     *              "parent": {}
    *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                  "message": "Department name already exists on the given company."
    *             }
     *         }
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ExistingDepartments",
     *     required={"data"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/ExistingDepartment"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ExistingDepartment",
     *     required={"name", "id", "company_id"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="parent_id",
     *         type="integer",
     *     ),
     * )
     */
    public function bulkUpdate(Request $request)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $departmentData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($departmentData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $oldDataResponse = $this->requestService->getCompanyDepartments($inputs['company_id']);
        $oldData = json_decode($oldDataResponse->getData(), true);
        $oldData = collect($oldData['data']);

        $response = $this->requestService->bulkUpdate($request->all());
        $responseData = json_decode($response->getData(), true);

        $departmentIds = [];

        // audit log
        foreach ($responseData['data'] as $department) {
            $old = $oldData->where('id', $department['id']);
            $details = [
                'new' => $department,
                'old' => $old->first()
            ];
            $item = new AuditCacheItem(
                DepartmentAuditService::class,
                DepartmentAuditService::ACTION_UPDATE,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                $details
            );
            $this->auditService->queue($item);

            $departmentIds[] = $department['id'];
        }

        $this->indexQueueService->queue($departmentIds);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/department/{id}",
     *     summary="Update Department",
     *     description="Update Department
     Authorization Scope : **edit.department**",
     *     tags={"department"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Department Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Department Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="parent_id",
     *         in="formData",
     *         description="Parent Department ID - Refers to which department this department will be under",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "id"=0,
     *                 "name"="name",
     *                 "company_id"=0,
     *                 "account_id"=0,
     *                 "description"="description",
     *                 "parent_id"=0,
     *                 "parent"=null,
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Department ID doesn't exists.",
     *         examples={
     *             "application/json": {
     *                 "message"="Department ID doesn't exists."
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="The selected company id is invalid."
     *             }
     *         }
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldDepartmentData = json_decode($response->getData());
        $oldDepartmentData->account_id = Company::getAccountId($inputs['company_id']);

        if (!$this->authorizationService->authorizeUpdate($oldDepartmentData, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }


        $this->indexQueueService->queue([$id]);

        $getResponse = $this->requestService->get($id);
        $oldDepartmentData = json_decode($response->getData(), true);
        $newDepartmentData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldDepartmentData,
            'new' => $newDepartmentData,
        ];
        $item = new AuditCacheItem(
            DepartmentAuditService::class,
            DepartmentAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/department/{id}",
     *     summary="Delete Department",
     *     description="Delete Department
     Authorization Scope : **delete.department**",
     *     tags={"department"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Department ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Department not found.",
     *         examples={
     *             "application/json": {
     *                 "message"="Department not found."
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Department ID should be numeric."
     *             }
     *         }
     *     )
     * )
     */
    public function delete(Request $request, PhilippineCompanyRequestService $companyRequestService, $id)
    {
        $deletedBy = $request->attributes->get('user');

        // get original department details
        $departmentResponse = $this->requestService->get($id);
        $departmentData = json_decode($departmentResponse->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $departmentData->company_id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete($departmentData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inUseResponse = $companyRequestService->isInUse('department', ['ids' => [$id]]);
        $inUseData = json_decode($inUseResponse->getData());

        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->delete($id);

        // audit log
        $item = new AuditCacheItem(
            DepartmentAuditService::class,
            DepartmentAuditService::ACTION_DELETE,
            new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
            [
                'old' => [
                    'id' => $id
                ]
            ]
        );
        $this->auditService->queue($item);

        return $response;
    }
}
