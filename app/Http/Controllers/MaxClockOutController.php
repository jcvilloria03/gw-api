<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Illuminate\Http\Request;
use App\MaxClockOut\MaximumClockOutAuthorizationService;
use App\MaxClockOut\MaxClockOutRequestService;
use Illuminate\Support\Arr;
use App\Employee\EmployeeRequestService;
use Illuminate\Support\Facades\App;
use App\Traits\AuditTrailTrait;
use Illuminate\Support\Facades\Log;

class MaxClockOutController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\MaxClockout\MaxClockOutRequestService
     */
    private $requestService;

    /**
     * @var \App\MaxClockOut\MaximumClockOutAuthorizationService
     */
    private $authorizationService;


    public function __construct(
        MaxClockOutRequestService $requestService,
        MaximumClockOutAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/max_clock_outs",
     *     summary="Get Company Max Clock Out Settings",
     *     description="Get Company Max Clock Out Settings",
     *     tags={"max_clock_out"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="keyword",
     *         in="query",
     *         description="Filter list based on keyword",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *          response=406,
     *          description="Not Acceptable",
     *          ref="$/responses/InvalidRequestResponse",
     *          examples={
     *              "Invalid Id": {
     *                  "message": "The id must be an integer.",
     *                  "status_code": 406,
     *              }
     *          }
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Not Found",
     *          examples={
     *              "Invalid Id": {
     *                  "message": "Account not found.",
     *                  "status_code": 404,
     *              }
     *          }
     *     )
     * )
     */
    public function getCompanyMaxClockOuts(Request $request, int $companyId)
    {
        $inputs = $request->only([
            "keyword",
            "page",
            "per_page"
        ]);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyMaxClockOuts(
                $authData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyMaxClockOuts($companyId, $inputs);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/max_clock_out",
     *     summary="Create max clock out",
     *     description="Create max clock out",
     *     tags={"max_clock_out"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create max clock out data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "required"={"company_id"},
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "name"={"type"="string"},
     *                 "hours"={"type"="integer"},
     *                 "affected_employees"={
     *                      "type"="array",
     *                      "items"={
     *                         "type"="object",
     *                         "properties"={
     *                             "id"={"type"="integer"},
     *                             "name"={"type"="string"},
     *                             "type"={"type"="string"},
     *                             "uid"={"type"="string"}
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="successful operation",
     *         examples={
     *             "application/json": {
     *                 "data"={
     *                    "id": 0
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Name already exists"
     *             }
     *         }
     *     )
     * )
     */
    public function create(Request $request)
    {
        $inputs = $request->only([
            'name',
            'hours',
            'company_id',
            'affected_employees'
        ]);

        $companyId = Arr::get($inputs, 'company_id');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->createCompanyMaxClockOut($inputs);

        if ($response->isSuccessful()) {
            try {
                // trigger audit trail
                $newData = json_decode($response->getData(), true);
                $this->audit($request, $companyId, $newData, [], true);
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/max_clock_out/{id}",
     *     summary="Update max clock out",
     *     description="Update max clock out",
     *     tags={"max_clock_out"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Max Clock Out Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create max clock out data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "required"={"company_id"},
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "name"={"type"="string"},
     *                 "hours"={"type"="integer"},
     *                 "affected_employees"={
     *                      "type"="array",
     *                      "items"={
     *                         "type"="object",
     *                         "properties"={
     *                             "id"={"type"="integer"},
     *                             "name"={"type"="string"},
     *                             "type"={"type"="string"},
     *                             "uid"={"type"="string"}
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="successful operation",
     *         examples={
     *             "application/json": {}
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Name already exists"
     *             }
     *         }
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        $inputs = $request->only([
            'name',
            'hours',
            'company_id',
            'affected_employees'
        ]);
        $companyId = Arr::get($inputs, 'company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        // For audit trail
        try {
            $oldDataResponse = $this->requestService->get($id);
            $oldData = json_decode($oldDataResponse->getData(), true);
        } catch (\Throwable $e) {
            $oldData = [];
        }

        $response = $this->requestService->updateCompanyMaxClockOut($id, $inputs);

        if ($response->isSuccessful()) {
            try {
                // trigger audit trail
                $newData = json_decode($response->getData(), true);
                $this->audit($request, $companyId, $newData, $oldData, true);
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }

        return $response;
    }


    /**
     * @SWG\Delete(
     *     path="/max_clock_out/bulk_delete",
     *     summary="Delete multiple max clock out",
     *     description="Delete multiple max clock out",
     *     tags={"max_clock_out"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_clock_out_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave entitlemens Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *    @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $maxClockOutData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete(
                $maxClockOutData,
                $deletedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // For audit trail
        try {
            $oldDataResponse = $this->requestService->get($inputs['id']);
            $deleteData = json_decode($oldDataResponse->getData(), true);
        } catch (\Exception $e) {
            $deleteData = [];
        }

        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            foreach ($deleteData as $oldData) {
                // trigger audit trail
                $this->audit($request, $inputs['company_id'], [], $oldData);
            }
        }

        return $response;
    }


    /**
     * @SWG\Get(
     *     path="/max_clock_out/{id}",
     *     summary="Get Single Max Clock Out Settings",
     *     description="Get Single Max Clock Out Settings",
     *     tags={"max_clock_out"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Max Clock Out Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *          response=406,
     *          description="Not Acceptable",
     *          ref="$/responses/InvalidRequestResponse",
     *          examples={
     *              "Invalid Id": {
     *                  "message": "The id must be an integer.",
     *                  "status_code": 406,
     *              }
     *          }
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Not Found",
     *          examples={
     *              "Invalid Id": {
     *                  "message": "Account not found.",
     *                  "status_code": 404,
     *              }
     *          }
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $responseData = Arr::get(json_decode($response->getData(), true), 'data', []);
        $companyId = Arr::get($responseData, 'company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        } else {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/max_clock_out/get_rule/{id}",
     *     summary="Get max clock out rules",
     *     description="Get max clock out rules",
     *     tags={"max_clock_out_rules"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function getRules(Request $request, $id)
    {
        $employeeRequestService = App::make(EmployeeRequestService::class);
        $employeeResponse = $employeeRequestService->getEmployee($id);
        $employeeData = json_decode($employeeResponse->getData(), true);
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData     = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];

            $authorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response =$this->requestService->getRules($id);
        return $response;
    }

}
