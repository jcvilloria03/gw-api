<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\User\UserAuthorizationService;

use App\Subscriptions\SubscriptionsRequestService;
use App\Subscriptions\SubscriptionsAuthorizationService;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;

class SubscriptionsController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $requestService;

    /**
     * @var \App\User\UserAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;


    /**
     * SubscriptionsController constructor.
     */
    public function __construct(
        SubscriptionsRequestService $subscriptionsRequestService,
        UserAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $subscriptionsRequestService;
        $this->userRequestService = $userRequestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\POST(
     *     path="/subscriptions/licenses",
     *     summary="Apply license to provided user_id/s",
     *     description="Apply license to provided user_id/s",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ApplyLicense"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ApplyLicense",
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/ApplyLicenseParam"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ApplyLicenseParam",
     *     required={"product_name","user_ids"},
     *     @SWG\Property(
     *         property="user_ids",
     *         type="array",
     *     ),
     *     @SWG\Property(
     *         property="product_name",
     *         type="string",
     *     )
     * )
     */

    public function applyLicenses(Request $request)
    {

        // validate permissions related requests

        $createdBy = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $inputs = isset($request->all()['data'][0]) ? $request->all()['data'][0] : null;

        if (!empty($inputs)) {
            $userIds = isset($inputs['user_ids']) ? $inputs['user_ids'] : [];
            $productName = isset($inputs['product_name']) ? $inputs['product_name'] : "";
            $subscriptionLicenseId =  isset($inputs['subscription_license_id']) ?
                        $inputs['subscription_license_id'] : "";
            return $this->requestService->applyLicenses($userIds, $productName, $subscriptionLicenseId);
        } else {
            $this->invalidRequestError("Invalid parameters");
        }
    }


    /**
     * @SWG\PATCH(
     *     path="/subscriptions/{id}/plan/update",
     *     summary="Update subscription plan",
     *     description="Update subscription plan

Authorization Scope : **edit.subscriptions**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/SubscriptionPlanUpdate"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="SubscriptionPlanUpdate",
     *     @SWG\Property(
     *         property="plan_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="convert_to_paid",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="licenses",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/SubscriptionPlanUpdateLicensesParam"
     *         )
     *     ),
     * ),

     * @SWG\Definition(
     *     definition="SubscriptionPlanUpdateLicensesParam",
     *     @SWG\Property(
     *         property="product_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="units",
     *         type="integer",
     *     ),
     * ),
     * )
     */

    public function updateSubscriptionPlan(
        Request $request,
        $id,
        SubscriptionsAuthorizationService $authorizationService
    ) {
        // validate permissions related requests
        $user = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($user['user_id']);
        $userData = json_decode($userResponse->getData());
        $companyId = $userData->companies[0]->id;
        $customerData = [];
        if (!$this->isAuthzEnabled($request)) {
            $customer     = $this->requestService->getCustomerByAccountId($user['account_id']);
            $customerData = json_decode($customer->getData(), true);

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            if (!$authorizationService->authorizeUpdate($authData, $user)) {
                $this->response()->errorUnauthorized();
            }
        }

        // Get user ids of sub owners
        $subOwners = $this->userRequestService->getAccountOwners($user['account_id']);
        $subOwners = json_decode($subOwners->getData());
        $subOwners = $subOwners->data ?? [];
        $subOwnersParam = [];

        foreach ($subOwners as $sub) {
            $subOwnersParam[] = $sub->id;
        }

        // Get user ids of super admins
        $superAdmins = $this->userRequestService->getAccountSuperAdmins($user['account_id']);
        $superAdmins = json_decode($superAdmins->getData());
        $superAdmins = $superAdmins->data ?? [];
        $superAdminsParam = [];

        foreach ($superAdmins as $sup) {
            $superAdminsParam[] = $sup->id;
        }

        $inputs = $request->all() ?? [];
        $inputs['sub_owners'] = count($subOwnersParam) > 0 ? $subOwnersParam : [];
        $inputs['super_admins'] = $superAdminsParam;
        $inputs['actor_id'] = $user['user_id'];

        if (!empty($inputs)) {
            try {
                $customer     = $this->requestService->getCustomerByAccountId($user['account_id']);
                $customerData = json_decode($customer->getData(), true);
                // trigger audit trail
                $companyId = $request->headers->get('X-Authz-Company-Id');
                $this->audit($request, $companyId, $inputs, $customerData, true);
                return $this->requestService->updateSubscriptionPlan($inputs, $id);
            } catch (\HttpException $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
                abort($e->getStatusCode(), $e->getMessage());
            }
        }
    }

    /**
     * @SWG\PATCH(
     *     path="/subscriptions/license/product",
     *     summary="Update license product scope of owner",
     *     description="Update license product scope of owner

Authorization Scope : **edit.subscriptions**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/OwnerLicenseUpdate"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="OwnerLicenseUpdate",
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/OwnerLicenseUpdateParam"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="OwnerLicenseUpdateParam",
     *     required={"product_code","new_units","subscription_license_id"},
     *     @SWG\Property(
     *         property="product_code",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="new_units",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="subscription_license_id",
     *         type="integer",
     *     ),
     * )
     */

    public function updateOwnerLicenseProduct(
        Request $request,
        SubscriptionsAuthorizationService $authorizationService
    ) {

         // validate permissions related requests

        $createdBy = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $inputs = isset($request->all()['data'][0]) ? $request->all()['data'][0] : null;
        if (!empty($inputs)) {
            $productCode = isset($inputs['product_code']) ? $inputs['product_code'] : [];
            $newUnits = isset($inputs['new_units']) ?
            $inputs['new_units'] : 0;
            $subscriptionLicenseId =  isset($inputs['subscription_license_id']) ?
                $inputs['subscription_license_id'] : "";
            return $this->requestService->updateOwnerLicenseProduct(
                $userData->id,
                $productCode,
                $subscriptionLicenseId,
                $newUnits
            );
        } else {
            $this->invalidRequestError("Invalid parameters");
        }
    }

    /**
     * @SWG\POST(
     *     path="/subscriptions/transpose/license_units",
     *     summary="Assign licenses to user(s)",
     *     description="Assign licenses to user(s) given the user id and product id",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AssignLicenseParam"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="AssignLicenseParam",
     *     required={"job_id", "assignees"},
     *     @SWG\Property(
     *         property="job_id",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="assignees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/AssigneesParam"),
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="AssigneesParam",
     *     @SWG\Property(
     *         property="entity_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="product_id",
     *         type="integer",
     *     )
     * ),
     */

    public function assign(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $jobId = $request->get("job_id");
        $assignees = $request->get("assignees");
        return $this->requestService->assign($createdBy['user_id'], $assignees, $jobId);
    }

    /**
     * @SWG\PUT(
     *     path="/subscriptions/transpose/license_units",
     *     summary="Unassign licenses to user(s)",
     *     description="Unassign licenses to user(s) given the user id and product id",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UnassignLicenseParam"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="UnassignLicenseParam",
     *     required={"job_id", "candidates"},
     *     @SWG\Property(
     *         property="job_id",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="candidates",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/UnassigneesParam"),
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="UnassigneesParam",
     *     @SWG\Property(
     *         property="entity_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="license_id",
     *         type="integer",
     *     )
     * ),
     */

    public function unassign(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $jobId = $request->get("job_id");
        $candidates = $request->get("candidates");
        return $this->requestService->unassign($createdBy['user_id'], $candidates, $jobId);
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/transpose/status/{jobId}",
     *     summary="Assign licenses to user(s)",
     *     description="Assign licenses to user(s) given the user id and product id",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="jobId",
     *         in="path",
     *         required=true,
     *         type="string"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */

    public function getTransposeStatus(Request $request, $jobId)
    {
        $createdBy = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getTransposeStatus($jobId);
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/{subscriptionId}/stats",
     *     summary="Get customer subscription statistics",
     *     description="Get customer subscription usage statistics",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="subscriptionId",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Subscription not found",
     *     )
     * )
     */
    public function getSubscriptionStats(Request $request, $subscriptionId)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->getCustomerSubscriptionStats($subscriptionId);
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/license_units",
     *     summary="Get customer subscription statistics",
     *     description="Get customer subscription usage statistics",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="filter[owner_user_id][]",
     *         type="array",
     *         in="query",
     *         description="ID's",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Subscription not found",
     *     )
     * )
     */
    public function getLicenseUnits(Request $request)
    {
        $createdBy   = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet(
            $authData,
            $createdBy
        )) {
            $this->response()->errorUnauthorized();
        }

        $inputs = $request->all();

        if (empty($inputs) || empty($inputs['filter'])) {
            $this->response()->errorBadRequest("The filter parameter is required.");
        }

        if (empty($inputs['filter']['owner_user_id'])) {
            $this->response()->errorBadRequest(
                "The owner user id filter list must not be empty."
            );
        }

        return $this->requestService->getLicenseUnits(
            $inputs['filter']['owner_user_id']
        );
    }
}
