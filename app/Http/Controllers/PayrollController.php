<?php

namespace App\Http\Controllers;

use App\FinalPay\FinalPayAuthorizationService;
use App\FinalPay\FinalPayRequestService;
use App\SpecialPayRun\SpecialPayRunAuthorizationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Tasks\UploadTask;
use App\Jobs\JobsRequestService;
use App\Payroll\PayrollUploadTask;
use Illuminate\Support\Facades\App;
use App\Payroll\PayrollRequestService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\Payroll\PayrollAuthorizationService;
use App\Payroll\PayrollAuditService;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use Aws\S3\Exception\S3Exception;
use App\Payroll\PayrollTaskException;
use App\Facades\Company;
use App\Storage\PayrollUploadService;
use Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\User\UserRequestService;
use App\Employee\EmployeeRequestService;
use App\Company\CompanyRequestService;
use Illuminate\Support\Arr;
use App\Payroll\PayrollAuthz;
use Illuminate\Support\Facades\Log;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PayrollController extends Controller
{
    use AuditTrailTrait;

    const TYPE_REGULAR = 'regular';
    const TYPE_SPECIAL = 'special';
    const PAYROLL_TYPES = [
        self::TYPE_REGULAR,
        self::TYPE_SPECIAL
    ];
    const UPLOAD_STATUSES = [
        "results",
        "errors",
    ];
    const UPLOAD_TYPE = [
        PayrollUploadService::ATTENDANCE_UPLOAD => 'ATTENDANCE',
        PayrollUploadService::ALLOWANCE_UPLOAD => 'ALLOWANCE',
        PayrollUploadService::BONUS_UPLOAD => 'BONUS',
        PayrollUploadService::COMMISSION_UPLOAD => 'COMMISSION',
        PayrollUploadService::DEDUCTION_UPLOAD => 'DEDUCTION',
    ];

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Payroll\PayrollRequestService
     */
    protected $requestService;

    /*
     * App\Payroll\PayrollGroupRequestService
     */
    protected $payrollGroupRequestService;

    /*
     * App\Payroll\PayrollAuthorizationService
     */
    protected $authorizationService;

    /*
     * App\Jobs\JobsRequestService
     */
    protected $jobsRequestService;

    /*
     * App\Storage\PayrollUploadService
     */
    protected $payrollUploadService;

    /**
     * @var \App\FinalPay\FinalPayAuthorizationService
     */
    protected $finalPayAuthorizationService;

    /**
     * @var \App\FinalPay\FinalPayRequestService
     */
    protected $finalPayRequestService;


    public function __construct(
        PayrollRequestService $requestService,
        PayrollGroupRequestService $payrollGroupRequestService,
        PayrollAuthorizationService $authorizationService,
        AuditService $auditService,
        JobsRequestService $jobsRequestService,
        PayrollUploadService $payrollUploadService,
        FinalPayAuthorizationService $finalPayAuthorizationService,
        FinalPayRequestService $finalPayRequestService
    ) {
        $this->requestService = $requestService;
        $this->payrollGroupRequestService = $payrollGroupRequestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->jobsRequestService = $jobsRequestService;
        $this->payrollUploadService = $payrollUploadService;
        $this->finalPayAuthorizationService = $finalPayAuthorizationService;
        $this->finalPayRequestService = $finalPayRequestService;
    }

    /**
     * Shared function to handle Payroll Upload requests
     *
     * @param Request $request
     * @param string $uploadType Job name related to upload
     * @return Response
     */
    protected function handleUploadRequest(Request $request, $uploadType)
    {
        $createdBy = $request->attributes->get('user');
        $payrollId = $request->input('payroll_id');
        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $authorized = $this->authorizationService->authorizeUpload($payrollData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $s3Key = $this->payrollUploadService->saveFile($file, $createdBy, 'public-read');
            $jobPayload = [
                'data' => [
                    'type' => 'jobs',
                    'attributes' => [
                        'payload' => [
                            'name' => $uploadType,
                            'fileBucket' => $this->payrollUploadService->getS3Bucket(),
                            'fileKey' => $s3Key,
                            'startDate' => $payrollData->start_date,
                            'endDate' => $payrollData->end_date,
                            'companyId' => $payrollData->company_id,
                            'payrollGroupId' => $payrollData->payroll_group_id,
                            'payrollType' => $payrollData->type,
                            'payrollId' => $payrollId,
                        ]
                    ]
                ]
            ];

            $jobResponse = $this->jobsRequestService->createJob($jobPayload);
            $jobData = json_decode($jobResponse->getData());

            $this->requestService->addUploadJobToPayroll([
                'payroll_id' => $payrollId,
                'job_id' => (string) $jobData->data->id,
                'type' => self::UPLOAD_TYPE[$uploadType],
                'uploaded_file' => $s3Key,
            ]);

            try {
                $this->audit($request, $payrollData->company_id, ['Trigger Upload Payroll Data']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json($jobData, Response::HTTP_CREATED);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }


    /**
     * Shared function to handle Payroll Process AS API Attendance requests
     *
     * @param Request $request
     * @param string $uploadType Job name related to upload
     * @return Response
     */
    protected function handleProcessAttendanceRequest($payrollId, Request $request)
    {
        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        $createdBy = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpload(
                $payrollData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $jobPayload = [
            'data' => [
                'type' => 'jobs',
                'attributes' => [
                    'payload' => [
                        'name' => 'validate-attendance-service-data',
                        'startDate' => $payrollData->start_date,
                        'endDate' => $payrollData->end_date,
                        'companyId' => $payrollData->company_id,
                        'payrollGroupId' => $payrollData->payroll_group_id,
                        'payrollId' => $payrollId,
                        'payrollType' => $payrollData->type,
                    ]
                ]
            ]
        ];

        $jobResponse = $this->jobsRequestService->createJob($jobPayload);
        $jobData = json_decode($jobResponse->getData());

        $this->requestService->addUploadJobToPayroll([
            'payroll_id' => $payrollId,
            'job_id' => (string) $jobData->data->id,
            'type' => self::UPLOAD_TYPE[PayrollUploadService::ATTENDANCE_UPLOAD],
            'uploaded_file' => "as-api-data",
        ]);

        // audit log
        $payrollData->type = 'validate-attendance-service-data';
        $payrollData->job_id = $jobData->data->id;
        $details = [
            'old' => json_decode(json_encode($payrollData), true),
        ];

        $item = new AuditCacheItem(
            PayrollAuditService::class,
            PayrollAuditService::ACTION_UPLOAD,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );

        $this->auditService->queue($item);

        return response()->json($jobData, Response::HTTP_CREATED);
    }

    /**
     *
     * Shared function to handle Payroll Upload Status requests
     *
     * @param Request $request
     * @param string $uploadTaskClass Task class related to upload
     * @return Response
     *
     */
    protected function handleUploadStatusRequest(
        Request $request,
        string $uploadTaskClass
    ) {

        //check payroll exists (will throw exception if payroll doesn't exist)
        $payrollId = $request->input('payroll_id');
        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeUpload(
                $payrollData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make($uploadTaskClass);
            $uploadTask->create($payrollId, $jobId);
        } catch (PayrollTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, PayrollUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' .
                    array_explode(",", PayrollUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === PayrollUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === PayrollUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }
        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     *
     * Shared function to handle Payroll Upload Deletion requests
     *
     * @param Request $request
     * @param string $uploadTaskClass Task class related to upload
     * @return Response
     *
     */
    protected function handleUploadDeleteRequest(
        Request $request,
        string $uploadType
    ) {
        $payrollId = $request->json('payroll_id');
        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
            ]);
        } else {
            $authorized = $this->authorizationService->authorizeEdit($payrollData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $payrollDeletionRequestResponse = $this->requestService->deleteUploadedJobToPayroll([
            'payroll_id' => $payrollId,
            'type' => self::UPLOAD_TYPE[$uploadType],
        ]);

        if ($payrollDeletionRequestResponse->getStatusCode() === Response::HTTP_OK) {
            try {
                $uploadType = str_replace('validate-', '', $uploadType);
                unset($payrollData->jobs);
                $this->audit($request, $payrollData->company_id, ['Remove Uploaded Payroll Data' => [
                    'type' => $uploadType,
                    'payroll_id' => $payrollData->id
                ]]);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }

        return $payrollDeletionRequestResponse;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/upload/attendance",
     *     summary="Upload Payroll Attendance",
     *     description="Uploads Attendance Information for Payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadAttendance(Request $request)
    {
        return $this->handleUploadRequest(
            $request,
            PayrollUploadService::ATTENDANCE_UPLOAD
        );
    }


    /**
     * @SWG\Post(
     *     path="/payroll/{payroll_id}/as-api/attendance",
     *     summary="Process attendance data from Attendance service",
     *     description="Process attendance data from Attendance service

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function processAttendance(Request $request, $payrollId)
    {
        $response = $this->handleProcessAttendanceRequest($payrollId, $request);

        try {
            $responseData = is_string($response->getData())
                ? json_decode($response->getData(), true)
                : $response->getData();
            $responseData = is_object($responseData) ? (array) $responseData : $responseData;
            $data = $responseData['data']->attributes->payload;
            $companyId = $data->companyId;

            $this->audit($request, $companyId, [], ['Trigger Payroll Get Attendance Data' => $payrollId]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/upload/attendance",
     *     summary="Delete Payroll Attendance",
     *     description="Deletes Uploaded Attendance Information Payroll Job

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deletePayrollJob"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="deletePayrollJob",
     *     @SWG\Property(
     *         property="payroll_id",
     *         description="Payroll ID",
     *         type="integer",
     *     )
     * )
     */
    public function deleteUploadedPayrollAttendance(Request $request)
    {
        return $this->handleUploadDeleteRequest(
            $request,
            PayrollUploadService::ATTENDANCE_UPLOAD
        );
    }

    /**
     * @SWG\Get(
     *     path="/payroll/upload/attendance/status",
     *     summary="Get Payroll Attendance Upload Status",
     *     description="Get Payroll Attendance Upload Status

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadAttendanceStatus(Request $request)
    {
        return $this->getUploadJobsStatus($request);
    }

    /**
     * @SWG\Post(
     *     path="/payroll/upload/allowance",
     *     summary="Upload Payroll Allowance",
     *     description="Uploads Allowance Information for Payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadAllowance(Request $request)
    {
        return $this->handleUploadRequest(
            $request,
            PayrollUploadService::ALLOWANCE_UPLOAD
        );
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/upload/allowance",
     *     summary="Deletes Uploaded Allowance Payroll Job",
     *     description="Deletes Allowance Information for Payroll

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deletePayrollJob"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteUploadedPayrollAllowance(Request $request)
    {
        return $this->handleUploadDeleteRequest(
            $request,
            PayrollUploadService::ALLOWANCE_UPLOAD
        );
    }

    /**
     * @SWG\Get(
     *     path="/payroll/upload/allowance/status",
     *     summary="Get Payroll Allowance Upload Status",
     *     description="Get Payroll Allowance Upload Status

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadAllowanceStatus(Request $request)
    {
        return $this->getUploadJobsStatus($request);
    }

    /**
     * @SWG\Post(
     *     path="/payroll/upload/bonus",
     *     summary="Upload Payroll Bonus",
     *     description="Uploads Bonus Information for Payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadBonus(Request $request)
    {
        return $this->handleUploadRequest(
            $request,
            PayrollUploadService::BONUS_UPLOAD
        );
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/upload/bonus",
     *     summary="Delete Uploaded Bonus Payroll Job",
     *     description="Deletes Bonus Information for Payroll

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deletePayrollJob"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteUploadedPayrollBonus(Request $request)
    {
        return $this->handleUploadDeleteRequest(
            $request,
            PayrollUploadService::BONUS_UPLOAD
        );
    }

    /**
     * @SWG\Get(
     *     path="/payroll/upload/bonus/status",
     *     summary="Get Payroll Bonus Upload Status",
     *     description="Get Payroll Bonus Upload Status

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadBonusStatus(Request $request)
    {
        return $this->getUploadJobsStatus($request);
    }

    /**
     * @SWG\Post(
     *     path="/payroll/upload/commission",
     *     summary="Upload Payroll Commission",
     *     description="Uploads Commission Information for Payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadCommission(Request $request)
    {
        return $this->handleUploadRequest(
            $request,
            PayrollUploadService::COMMISSION_UPLOAD
        );
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/upload/commission",
     *     summary="Delete Commission Payroll Job",
     *     description="Deletes Uploaded Commission Information Payroll Job

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deletePayrollJob"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteUploadedPayrollCommission(Request $request)
    {
        return $this->handleUploadDeleteRequest(
            $request,
            PayrollUploadService::COMMISSION_UPLOAD
        );
    }

    /**
     * @SWG\Get(
     *     path="/payroll/upload/commission/status",
     *     summary="Get Payroll Commission Upload Status",
     *     description="Get Payroll Commission Upload Status

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadCommissionStatus(Request $request)
    {
        return $this->getUploadJobsStatus($request);
    }

    /**
     * @SWG\Post(
     *     path="/payroll/upload/deduction",
     *     summary="Upload Payroll Deduction",
     *     description="Uploads Deduction Information for Payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadDeduction(Request $request)
    {
        return $this->handleUploadRequest(
            $request,
            PayrollUploadService::DEDUCTION_UPLOAD
        );
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/upload/deduction",
     *     summary="Upload Payroll Deduction",
     *     description="Uploads Deduction Information for Payroll

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deletePayrollJob"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteUploadedPayrollDeduction(Request $request)
    {
        return $this->handleUploadDeleteRequest(
            $request,
            PayrollUploadService::DEDUCTION_UPLOAD
        );
    }

    /**
     * @SWG\Get(
     *     path="/payroll/upload/deduction/status",
     *     summary="Get Payroll Deduction Upload Status",
     *     description="Get Payroll Deduction Upload Status

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadDeductionStatus(Request $request)
    {
        return $this->getUploadJobsStatus($request);
    }

    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/payroll/upload/status_list",
     *     summary="Get List of Step Statuses",
     *     description="List of Possible Payroll Upload Step Statuses",
     *     tags={"payroll"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     )
     * )
     */
    public function uploadStatuses()
    {
        return response()->json([
            'validation' => UploadTask::VALIDATION_STATUSES,
            'save' => UploadTask::SAVE_STATUSES
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payrolls",
     *     summary="Get company payrolls",
     *     description="Get list of payrolls within a certain company

Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[status]",
     *         in="query",
     *         description="Filter payroll status",
     *         required=false,
     *         type="array",
     *         uniqueItems=true,
     *         collectionFormat="csv",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="filter[start_date]",
     *         in="query",
     *         description="Filter start date",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[end_date]",
     *         in="query",
     *         description="Filter end date",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[payroll_date]",
     *         in="query",
     *         description="Filter payroll date",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[payroll_group_ids]",
     *         in="query",
     *         description="Filter by Payroll Group",
     *         required=false,
     *         type="array",
     *         uniqueItems=true,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="keyword",
     *         in="query",
     *         description="Search keyword",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="order_by",
     *         type="string",
     *         in="query",
     *         description="Sort By Field (sorts by creation date if it is empty)"
     *     ),
     *     @SWG\Parameter(
     *         name="order_dir",
     *         type="string",
     *         in="query",
     *         description="Sort By Field Order (sorts desc if it is empty)",
     *         enum={ "asc", "desc" }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyPayrolls(Request $request, $id)
    {
        $authzDataScope = $this->getAuthzDataScope($request);
        $isAuthorized = false;

        $queryData = $request->query();

        $response = $this->requestService->getCompanyPayrolls($id, $queryData, $authzDataScope);
        $responseData = json_decode($response->getData(), true);

        if (empty($responseData['data'])) {
            return $response;
        }

        $payrolls = collect($responseData['data']);
        $regularPayrolls = $payrolls->where('type', '=', 'regular')->values();

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $payrollData = current($responseData['data']);

            $companyData = new \stdClass();
            $companyData->id = $payrollData['company_id'];
            $companyData->account_id = $payrollData['account_id'];

            $viewedBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeViewCompanyPayrolls(
                $companyData,
                $viewedBy
            );
        }

        //authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $payrollGroupIds = $regularPayrolls->pluck('payroll_group_id')->unique()->implode(',');

        if (!$payrolls->isEmpty() && !empty($payrollGroupIds)) {
            $payrollGroupResponse = $this->payrollGroupRequestService->getCompanyPayrollGroupByAttribute(
                $id,
                'id',
                ['values' => $payrollGroupIds]
            );

            $payrollGroupData = json_decode($payrollGroupResponse->getData(), true);
            $payrollGroups = collect($payrollGroupData)->keyBy('id');

            $payrolls->transform(function ($item) use ($payrollGroups) {
                $item['payroll_group_name'] = $payrollGroups[$item['payroll_group_id']]['name'] ?? '';

                return $item;
            });

            $responseData['data'] = $payrolls->toArray();
            $response->setData($responseData);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll",
     *     summary="Create payroll and payroll employees",
     *     description="Create payroll and payroll employees

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="formData",
     *         description="Payroll group id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="formData",
     *         description="Start Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="formData",
     *         description="End Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_start_date",
     *         in="formData",
     *         description="Attandance Start date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_end_date",
     *         in="formData",
     *         description="Attandance End date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="search_term",
     *         in="formData",
     *         description="Search Term for employees",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="annualize",
     *         in="formData",
     *         description="Annualize Tax Computation",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $payrollGroupResponse = $this->payrollGroupRequestService->get($inputs['payroll_group_id']);
        $payrollGroupData = json_decode($payrollGroupResponse->getData(), true);

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollGroupData['company_id'],
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupData['id'],
            ]);
        } else {
            $payrollData = new \stdClass();
            $payrollData->account_id = $payrollGroupData['account_id'];
            $payrollData->company_id = $payrollGroupData['company_id'];
            $payrollData->payroll_group_id = $payrollGroupData['id'];
            $authorized = $this->authorizationService->authorizeCreate($payrollData, $createdBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($inputs);
        $responseData = json_decode($response->getData(), true);

        // audit log
        unset($responseData['employees']);
        $this->audit($request, $payrollGroupData['company_id'], $responseData);
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/regular_payroll_job",
     *     summary="Start job for creaing payroll",
     *     description="Start job for creaing payroll

Authorization Scope : **create.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="formData",
     *         description="Payroll group id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="formData",
     *         description="Start Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="formData",
     *         description="End Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_start_date",
     *         in="formData",
     *         description="Attandance Start date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_end_date",
     *         in="formData",
     *         description="Attandance End date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="annualize",
     *         in="formData",
     *         description="Annualize Tax Computation",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function regularPayrollJob(Request $request)
    {
        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $payrollGroupResponse = $this->payrollGroupRequestService->get($inputs['payroll_group_id']);
        $payrollGroupData = json_decode($payrollGroupResponse->getData(), true);

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollGroupData['company_id'],
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupData['id'],
            ]);
        } else {
            $payrollData = new \stdClass();
            $payrollData->account_id = $payrollGroupData['account_id'];
            $payrollData->company_id = $payrollGroupData['company_id'];
            $payrollData->payroll_group_id = $payrollGroupData['id'];
            $authorized = $this->authorizationService->authorizeCreate($payrollData, $createdBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->regularPayrollJob($inputs);
        $responseData = json_decode($response->getData(), true);

        // audit log
        unset($responseData['employees']);
        $details = [
            'new' => json_decode(json_encode($responseData), true),
        ];
        $item = new AuditCacheItem(
            PayrollAuditService::class,
            PayrollAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\GET(
     *     path="/regular_payroll_job/{job_id}",
     *     summary="Get status of payroll creation job",
     *     description="Get status of payroll creation job",
     *     tags={"payroll_job_status"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid job id",
     *     )
     * )
     */
    public function regularPayrollJobStatus($jobId)
    {
        $response =$this->requestService->regularPayrollJobStatus($jobId);
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/special",
     *     summary="Create special payroll and payroll employees",
     *     description="Create payroll and payroll employees

    Authorization Scope : **create.special_payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Special pay run Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "account_id"={"type"="integer"},
     *                 "included_items"={
     *                     "type"="array",
     *                     "items"={
     *                         "type"="object",
     *                         "properties"={
     *                              "release_type"={"type"="string"},
     *                              "release_id"={"type"="string"},
     *                              "employee_id"={"type"="integer"},
     *                              "employee_uid"={"type"="string"},
     *                              "employee_name"={"type"="string"},
     *                              "locations"={"type"="string"},
     *                              "department"={"type"="string"},
     *                              "position"={"type"="string"},
     *                              "release_date"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createSpecial(Request $request, SpecialPayRunAuthorizationService $authorizationService)
    {
        $inputs = $request->all();
        $companyId = $request->get('company_id');
        $this->validate($request, [
            'company_id' => 'required|integer',
            'account_id' => 'required|integer',
        ]);

        $authorized = false;
        $authDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $createdBy = $request->attributes->get('user');
            $payrollData = new \stdClass();
            $payrollData->account_id = $inputs['account_id'];
            $payrollData->company_id = $inputs['company_id'];
            $authorized = $authorizationService->authorizeCreate($payrollData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->createSpecial($inputs, $authDataScope);

        try {
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/special/special_payroll_job",
     *     summary="Start job for special payroll creation",
     *     description="Start job for special payroll creation

    Authorization Scope : **create.special_payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Special pay run Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "account_id"={"type"="integer"},
     *                 "included_items"={
     *                     "type"="array",
     *                     "items"={
     *                         "type"="object",
     *                         "properties"={
     *                              "release_type"={"type"="string"},
     *                              "release_id"={"type"="string"},
     *                              "employee_id"={"type"="integer"},
     *                              "employee_uid"={"type"="string"},
     *                              "employee_name"={"type"="string"},
     *                              "locations"={"type"="string"},
     *                              "department"={"type"="string"},
     *                              "position"={"type"="string"},
     *                              "release_date"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createSpecialJob(Request $request, SpecialPayRunAuthorizationService $authorizationService)
    {
        $inputs = $request->all();
        $companyId = $request->get('company_id');
        $this->validate($request, [
            'company_id' => 'required|integer',
            'account_id' => 'required|integer',
        ]);

        $authorized = false;
        $authDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $createdBy = $request->attributes->get('user');
            $payrollData = new \stdClass();
            $payrollData->account_id = $inputs['account_id'];
            $payrollData->company_id = $inputs['company_id'];
            $authorized = $authorizationService->authorizeCreate($payrollData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->createSpecialJob($inputs, $authDataScope);

        return $response;
    }

     /**
     * @SWG\GET(
     *     path="/special_payroll_job/{job_id}",
     *     summary="Get status of special payroll creation job",
     *     description="Get status of special payroll creation job",
     *     tags={"payroll_job_status"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid job id",
     *     )
     * )
     */
    public function specialPayrollJobStatus($jobId)
    {
        $response =$this->requestService->specialPayrollJobStatus($jobId);
        return $response;
    }
    /**
     * @SWG\Get(
     *     path="/payroll/{id}",
     *     summary="Get Payroll",
     *     description="Get Payroll Details

Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="with_employees",
     *         in="query",
     *         description="Whether or not to include payroll employees",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page of data to request",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Per Page of data to request",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="search_term",
     *         in="query",
     *         description="Search Term for employees",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $response = $this->requestService->get(
            $id,
            $request['with_employees'] ?? false,
            $request['page'] ?? null,
            $request['search_term'] ?? null,
            $request['per_page'] ?? 10
        );
        $payrollData = json_decode($response->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeGet(
                $payrollData,
                $user
            );
        }

        //authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }


        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{id}/close",
     *     summary="Close a generated Payroll",
     *     description="Close an OPEN Payroll for a given Payroll ID.
 Authorization Scope : **close.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="skipEnforceGapLoans",
     *         in="formData",
     *         description="Override enforcement of Gap loans",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully CLOSED an OPEN Payroll": {
     *                  "payroll": {
     *                      "id": 2623,
     *                      "payroll_group_id": 9117,
     *                      "company_id": 491,
     *                      "account_id": 366,
     *                      "start_date": "2019-09-27",
     *                      "end_date": "2019-10-10",
     *                      "posting_date": "2019-10-15",
     *                      "payroll_date": "2019-10-15",
     *                      "total_employees": 1,
     *                      "status": "CLOSED",
     *                      "gross": "25000.0000",
     *                      "net": "23756.2500",
     *                      "total_deductions": "1243.7500",
     *                      "total_contributions": "3317.5000",
     *                      "payslips_sent_at": null,
     *                      "created_at": "2019-12-06 19:39:14",
     *                      "updated_at": "2020-01-13 13:04:14",
     *                      "payroll_type": "regular",
     *                      "annualize": 1,
     *                      "statutory_minimum_wage_for_mwe": "0.0000",
     *                      "other_pays_for_mwe": "0.0000",
     *                      "13th_month_pay_and_other_benefits": "0.0000",
     *                      "de_minimis_benefits": "0.0000",
     *                      "employee_govt_contribution": "0.0000",
     *                      "other_non_taxable_compensation_amount": "0.0000",
     *                      "total_non_taxable_compensation": "0.0000",
     *                      "total_taxable_compensation": "0.0000",
     *                      "taxable_compensation_not_subject_to_wtax": "0.0000",
     *                      "net_taxable_compensation": "0.0000",
     *                      "total_taxes_withheld": "0.0000",
     *                      "taxes_withheld_for_remittance": "0.0000",
     *                      "sss_ee_er_contribution": "0.0000",
     *                      "sss_ec_contribution": "0.0000",
     *                      "hdmf_total_loan_amount": "0.0000",
     *                      "hdmf_total_ee_share": "0.0000",
     *                      "hdmf_total_er_share": "0.0000",
     *                      "hdmf_total_share": "0.0000"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Payroll ID does not exist": {
     *                  "message": "Payroll not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Payroll ID is required": {
     *                  "message": "Payroll ID should not be empty.",
     *                  "status_code": 406
     *              },
     *              "Non-numeric Payroll ID": {
     *                  "message": "Payroll ID should be numeric.",
     *                  "status_code": 406
     *              },
     *              "Invalid Payroll status": {
     *                  "message": "Only open payrolls can be closed.",
     *                  "status_code": 406
     *              },
     *              "Unfinished Gap Loan Transaction": {
     *                  "message": "You must finish gap loan transaction before closing this payroll.",
     *                  "status_code": 406
     *              },
     *              "Payroll Group errors": {
     *                  "message": "Payroll Group not found.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function close(Request $request, $payrollId)
    {
        //check payroll exists (will throw exception if payroll doesn't exist)
        $response = $this->requestService->get($payrollId);

        $payrollData = json_decode($response->getData());
        $closedBy = $request->attributes->get('user');
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $payrollType = data_get($payrollData, 'type');
            $authzScope = [
                AuthzDataScope::SCOPE_COMPANY => data_get($payrollData, 'company_id')
            ];
            // disable payroll group check if payroll type is not regular
            if ($payrollType === self::TYPE_REGULAR) {
                $authzScope[AuthzDataScope::SCOPE_PAYROLL_GROUP] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized($authzScope);
        } else {
            $isAuthorized = $this->authorizationService->authorizeClose($payrollData, $closedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $options = $request->all();

        $response = $this->requestService->close($payrollId, $options);

        try {
            $companyId = data_get($payrollData, 'company_id');
            $oldData = json_decode(json_encode($payrollData), true);
            unset($oldData['jobs']);
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData['payroll'], $oldData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{id}/open",
     *     summary="Open a generated Payroll",
     *     description="Open a DRAFT or CLOSED generated Payroll for a given Payroll ID.
 Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully opened a DRAFT or CLOSED Payroll": {
     *                  "payroll": {
     *                      "id": 2623,
     *                      "payroll_group_id": 9117,
     *                      "company_id": 491,
     *                      "account_id": 366,
     *                      "start_date": "2019-09-27",
     *                      "end_date": "2019-10-10",
     *                      "posting_date": "2019-10-15",
     *                      "payroll_date": "2019-10-15",
     *                      "total_employees": 1,
     *                      "status": "REOPENING",
     *                      "gross": "25000.0000",
     *                      "net": "23756.2500",
     *                      "total_deductions": "1243.7500",
     *                      "total_contributions": "3317.5000",
     *                      "payslips_sent_at": null,
     *                      "created_at": "2019-12-06 19:39:14",
     *                      "updated_at": "2020-01-13 11:43:46",
     *                      "payroll_type": "regular",
     *                      "annualize": 1,
     *                      "statutory_minimum_wage_for_mwe": "0.0000",
     *                      "other_pays_for_mwe": "0.0000",
     *                      "13th_month_pay_and_other_benefits": "0.0000",
     *                      "de_minimis_benefits": "0.0000",
     *                      "employee_govt_contribution": "0.0000",
     *                      "other_non_taxable_compensation_amount": "0.0000",
     *                      "total_non_taxable_compensation": "0.0000",
     *                      "total_taxable_compensation": "0.0000",
     *                      "taxable_compensation_not_subject_to_wtax": "0.0000",
     *                      "net_taxable_compensation": "0.0000",
     *                      "total_taxes_withheld": "0.0000",
     *                      "taxes_withheld_for_remittance": "0.0000",
     *                      "sss_ee_er_contribution": "0.0000",
     *                      "sss_ec_contribution": "0.0000",
     *                      "hdmf_total_loan_amount": "0.0000",
     *                      "hdmf_total_ee_share": "0.0000",
     *                      "hdmf_total_er_share": "0.0000",
     *                      "hdmf_total_share": "0.0000"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Payroll ID does not exist": {
     *                  "message": "Payroll not found.",
     *                  "status_code": 406
     *              },
     *              "Payroll ID is required": {
     *                  "message": "Payroll ID should not be empty.",
     *                  "status_code": 406
     *              },
     *              "Non-numeric Payroll ID": {
     *                  "message": "Payroll ID should be numeric.",
     *                  "status_code": 406
     *              },
     *              "Invalid Payroll status": {
     *                  "message": "Only draft or closed payrolls can be opened.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function open($id, Request $request)
    {
        $payrollData = json_decode($this->requestService->get($id)->getData());
        $user = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $authorized = $this->authorizationService->authorizeOpen($payrollData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->open($id);

        try {
            $companyId = data_get($payrollData, 'company_id');
            $oldData = json_decode(json_encode($payrollData), true);
            unset($oldData['jobs']);
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData['payroll'], $oldData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll/{id}/",
     *     summary="Delete Payroll",
     *     description="Delete Payroll for given id

Authorization Scope : **delete.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function delete($id, Request $request)
    {
        //check payroll exists (will throw exception if payroll doesn't exist)
        $payrollData = json_decode($this->requestService->get($id)->getData());
        $deletedBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $authorized = $this->authorizationService->authorizeDelete($payrollData, $deletedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->delete($id);

        // create payroll statistics data
        try {
            $this->requestService->createCompanyPayrollStatistics($payrollData->company_id);
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        // audit log
        $details = [
            'old' => json_decode(json_encode($payrollData), true)
        ];
        $item = new AuditCacheItem(
            PayrollAuditService::class,
            PayrollAuditService::ACTION_DELETE,
            new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/payroll/{id}",
     *     summary="Edit Payroll",
     *     description="Edit Payroll Summary Fields and Payroll Employee Fields

Authorization Scope : **edit.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Payroll Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "posting_date"={"type"="string"},
     *                 "net"={"type"="number"},
     *                 "gross"={"type"="number"},
     *                 "total_deductions"={"type"="number"},
     *                 "total_contributions"={"type"="number"},
     *                 "employees"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "id"={"type"="integer"},
     *                              "taxable_income"={"type"="number"},
     *                              "gross"={"type"="number"},
     *                              "withholding_tax"={"type"="number"},
     *                              "total_deduction"={"type"="number"},
     *                              "net_pay"={"type"="number"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function edit($id, Request $request)
    {
        // check payroll exists (will throw exception if payroll doesn't exist)
        $response = $this->requestService->get($id, true);
        $payrollData = json_decode($response->getData());
        $editedBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $authorized = $this->authorizationService->authorizeEdit($payrollData, $editedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->update($payrollData->id, $request->all());

        try {
            if (!empty($payrollData->payroll_group_id)) {
                // create payroll statistics data
                $this->requestService->createCompanyPayrollStatistics($payrollData->company_id);
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        // audit log
        $oldValue = json_decode(json_encode($payrollData), true);
        $newPayrollData = $request->all();
        $this->audit($request, $payrollData->company_id, $newPayrollData, $oldValue);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{payroll_id}/calculate",
     *     summary="Calculate a Payroll",
     *     description="Trigger Payroll Calculation.
 Calculate a DRAFT or an OPEN Payroll.

Authorization Scope : **run.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID. Set to 0 (zero) when projecting Final Pay calculations,
 as projectedCalculation MUST be set to TRUE, companyId and employeeId MUST also be set to
 their corresponding values.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters all OPTIONAL: projectedCalculation (bool), employeeId (int),
 companyId (int)",
     *         required=false,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="projectedCalculation", type="boolean"),
     *             @SWG\Property(property="employeeId", type="integer"),
     *             @SWG\Property(property="companyId", type="integer"),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="successful operation",
     *         examples={
     *              "Calculate Payroll": {
     *                  "links": {
     *                      "self": "/jobs/bb88a596-56d4-491d-97b9-5cf216098d67"
     *                  },
     *                  "data": {
     *                      "type": "jobs",
     *                      "id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                      "attributes": {
     *                      "createdAt": "1578983210",
     *                      "expiresAt": "1586759210",
     *                      "payload": {
     *                          "name": "compute-final-payroll",
     *                          "payrollId": 41,
     *                          "companyId": 22,
     *                          "attendanceJobId": "62eaffff-daf9-46a7-a2ab-e816585d81cd",
     *                          "jobId": "bb88a596-56d4-491d-97b9-5cf216098d67"
     *                      },
     *                      "status": "PENDING",
     *                      "tasksCount": "1",
     *                      "tasksDone": "0",
     *                      "updatedAt": "null",
     *                      "version": "1"
     *                      }
     *                  }
     *              },
     *              "Projected Final Pay Payroll calculation": {
     *                  "links": {
     *                      "self": "/jobs/bb88a596-56d4-491d-97b9-5cf216098d67"
     *                  },
     *                  "data": {
     *                      "type": "jobs",
     *                      "id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                      "attributes": {
     *                      "createdAt": "1578983210",
     *                      "expiresAt": "1586759210",
     *                      "payload": {
     *                          "name": "projected-final-payroll",
     *                          "companyId": 491,
     *                          "employeeId": 271,
     *                      },
     *                      "status": "PENDING",
     *                      "tasksCount": "1",
     *                      "tasksDone": "0",
     *                      "updatedAt": "null",
     *                      "version": "1"
     *                      }
     *                  }
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "No Attendance data": {
     *                  "message": "Attendance data is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid Payroll status": {
     *                  "message": "Only Draft Payrolls can be calculated.",
     *                  "status_code": 406
     *              },
     *              "Projected Calculation must be true": {
     *                  "message": "Projected Calculation must be true if payroll id is 0.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Payroll ID does not exist": {
     *                  "message": "Payroll not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              },
     *              "Invalid Payroll ID": {
     *                  "message": "Trying to get property of non-object",
     *                  "status_code": 500
     *              },
     *              "Non-existing Payroll ID": {
     *                  "message": "Payroll not found.",
     *                  "status_code": 500
     *              },
     *              "Invalid Payroll status to calculate": {
     *                  "message": "Only Draft payroll can be set to calculating.",
     *                  "status_code": 500
     *              },
     *              "Invalid Payroll status to re-calculate": {
     *                  "message": "Only Open payroll can be set to recalculating.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function calculate(Request $request, EmployeeRequestService $employeeRequestService, int $payrollId)
    {
        $createdBy = $request->attributes->get('user');
        $requestData = $request->all();
        $authorized = false;
        if ($payrollId === 0 && !($requestData['projectedCalculation'] ?? false)) {
            $this->invalidRequestError('Projected Calculation must be true if payroll id is 0.');
        } elseif ($payrollId === 0 && ($requestData['projectedCalculation'] ?? false)) {
            if ($this->isAuthzEnabled($request)) {
                $response     = $employeeRequestService->getEmployee($requestData['employeeId']);
                $employeeData = json_decode($response->getData(), true);
                $authorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                        AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                        AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                        AuthzDataScope::SCOPE_POSITION =>  Arr::get($employeeData, 'position_id'),
                        AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
                    ],
                    'employees.people.termination_information'
                );
            } else {
                $data = (object) [
                    'account_id' => (int) Company::getAccountId($requestData['companyId']),
                    'company_id' => $requestData['companyId']
                ];
                $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
            }

            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }

            return $this->finalPayRequestService
                ->projectFinalPay($requestData['companyId'], $requestData['employeeId']);
        }

        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            if ($payrollData->type === 'regular') {
                $authorized = $authzDataScope->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                ]);
            } else {
                $authorized = $authzDataScope->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id
                ]);
            }
        } else {
            $authorized = $this->authorizationService->authorizeCalculate($payrollData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        //TODO:  We should start moving this code to the Payroll-API as it is too domain specific to be here.
        if ($payrollData->status !== 'DRAFT') {
            $this->invalidRequestError('Only Draft Payrolls can be calculated.');
        }

        $calculateResponse = $this->requestService->calculate($payrollId, []);

        // create payroll statistics data
        try {
            $this->requestService->createCompanyPayrollStatistics($payrollData->company_id);
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        // audit log
        $oldData = json_decode(json_encode($payrollData), true);
        $this->audit($request, $payrollData->company_id, [], $oldData);

        return $calculateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{payroll_id}/recalculate",
     *     summary="Re-Calculate Payroll",
     *     description="Trigger Payroll Calculation

Authorization Scope : **run.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Parameters both optional: saveResultToParent (bool), employeeIds (array of ints)
     * **NOTE: Please remove the 0 in the sample employeeIds**",
     *         required=false,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="saveResultToParent", type="boolean"),
     *             @SWG\Property(property="employeeIds", type="array", @SWG\Items(type="integer")),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function recalculate(Request $request, $payrollId)
    {
        // Joe
        $this->validate($request, [
            'saveResultToParent' => 'boolean',
            'employeeIds' => 'array'
        ]);
        $createdBy = $request->attributes->get('user');
        //check payroll exists (will throw exception if payroll doesn't exist)
        $payrollResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollResponse->getData());
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            if ($payrollData->type === 'regular') {
                $authorized = $authzDataScope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                    ]
                );
            } else {
                $authorized = $authzDataScope
                    ->isAllAuthorized([AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id]);
            }
        } else {
            $authorized = $this->authorizationService->authorizeCalculate($payrollData, $createdBy);
        }

        $body = $request->all();
        // authorize
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        if ($payrollData->status != 'OPEN') {
            $this->invalidRequestError('Only Open Payrolls can be re-calculated.');
        }

        $calculateResponse = $this->requestService->calculate($payrollId, $body);

        // create payroll statistics data
        try {
            $this->requestService->createCompanyPayrollStatistics($payrollData->company_id);
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        // audit log
        $oldData = json_decode(json_encode($payrollData), true);
        $this->audit($request, $payrollData->company_id, [], $oldData);

        return $calculateResponse;
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{payroll_id}/calculate/status",
     *     summary="Get status of Payroll Calculation",
     *     description="Get status of Payroll Calculation

Authorization Scope : **run.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include results/errors data",
     *         required=false,
     *         type="array",
     *         collectionFormat="csv",
     *         @SWG\Items(type="string", enum={"results", "errors"})
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function calculationStatus(Request $request, int $payrollId)
    {
        $isProjection = $payrollId == 0;

        $jobStatusResponse = $this->getUploadJobsStatus($request);

        $payrollData = new \stdClass;
        if (!$isProjection) {
            $payrollResponse = $this->requestService->get($payrollId);
            $payrollData = json_decode($payrollResponse->getData());
        } else {
            $jobStatusResponseData = json_decode($jobStatusResponse->getData(), true);
            $jobPayload = Arr::get($jobStatusResponseData, 'data.attributes.payload', []);

            $employeeId = $jobPayload['employee_id'];
            $employeeRequestService = App::make(EmployeeRequestService::class);
            $employeeResponse = $employeeRequestService->getEmployeeInfo($employeeId);
            $employeeResponseData = json_decode($employeeResponse->getData(), true);
            $payrollGroupId = Arr::get($employeeResponseData, 'payroll_group.id', 0);

            $companyId = $jobPayload['companyId'];
            $companyRequestService = App::make(CompanyRequestService::class);
            $accountId = $companyRequestService->getAccountId($companyId);

            $payrollData = (object)[
                'id' => $payrollGroupId,
                'account_id' => $accountId,
                'company_id' => $companyId,
                'payroll_group_id' => $payrollGroupId,
            ];
        }

        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $dataScope = $this->getAuthzDataScope($request);
            $companyId = data_get($payrollData, 'company_id');
            $payrollGroupId = data_get($payrollData, 'payroll_group_id');

            $isAuthorized =
                $dataScope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => $companyId,
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupId
                    ]
                );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpload($payrollData, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $jobStatusResponse;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{id}/send_payslips",
     *     summary="Send Payslips",
     *     description="Sends Payslips to employees.
You can only send payslips when the payroll is CLOSED.
This will trigger payslip notifications to employees.

Authorization Scope : **send_payslip.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="integer", description="Payroll Id"),
     *             @SWG\Property(property="payroll_group_id", type="integer", description="Payroll Group Id"),
     *             @SWG\Property(property="payroll_group_name", type="string", description="Payroll Group Name"),
     *             @SWG\Property(property="company_id", type="integer", description="Company Id"),
     *             @SWG\Property(property="account_id", type="integer", description="Account Id"),
     *             @SWG\Property(property="start_date", type="date", description="Start Date"),
     *             @SWG\Property(property="end_date", type="date", description="End Date"),
     *             @SWG\Property(property="payroll_date", type="date", description="Payroll Date"),
     *             @SWG\Property(property="total_employees", type="integer", description="Number of Employees"),
     *             @SWG\Property(property="status", type="string", description="Payroll Status"),
     *             @SWG\Property(property="gross", type="number", description="Total Gross"),
     *             @SWG\Property(property="net", type="number", description="Total Net"),
     *             @SWG\Property(property="total_deductions", type="number", description="Total Deductions"),
     *             @SWG\Property(property="total_contributions", type="number", description="Total Contributions"),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": 24,
     *                  "payroll_group_id": 1,
     *                  "payroll_group_name": "PG1",
     *                  "company_id": 2,
     *                  "account_id": 2,
     *                  "start_date": "2017-09-11",
     *                  "end_date": "2017-10-10",
     *                  "payroll_date": "2017-10-31",
     *                  "total_employees": 52,
     *                  "status": "CLOSED",
     *                  "gross": 1518592.35,
     *                  "net": 1281681.24,
     *                  "total_deductions": -312334.31,
     *                  "total_contributions": -48197.2
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll is not CLOSED"
     *              }
     *         }
     *     )
     * )
     */
    public function sendPayslips($id, Request $request)
    {
        $payrollGetResponse = $this->requestService->get($id);
        $payrollData = json_decode($payrollGetResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $triggeredBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeSendPayslips($payrollData, $triggeredBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->sendPayslips($id);

        $this->audit($request, $payrollData->company_id, [], ['Trigger Send Payslips' => $payrollData]);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{payrollId}/send_single_payslip",
     *     summary="Send Individual Payslip",
     *     description="Sends individual payslip to employee.
You can only send payslips when the payroll is CLOSED.
This will trigger payslip notifications to employee.

Authorization Scope : **send_payslip.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payrollId",
     *         in="path",
     *         description="Payroll Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         type="integer",
     *         in="formData",
     *         description="Employee Id",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll not found"
     *              }
     *         }
     *     )
     * )
     */
    public function sendSinglePayslip(Request $request, $payrollId)
    {
        $payrollGetResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollGetResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeSendPayslips(
                $payrollData,
                $request->attributes->get('user')
            );
        }

        //authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $employeeId = (int) $request->input('employeeId');

        return $this->requestService->sendSinglePayslip($payrollId, $employeeId);
    }
    /**
     * @SWG\Get(
     *     path="/payroll/{id}/employee_disbursement_method",
     *     summary="Fetch list of employee id with active payment method details",
     *     description="Return active disbursement method for employees within payrun.

Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter",
     *         in="query",
     *         description="Filter results based on query string value. following the format
```
[fieldname]|[value] (e.g. attributes.type|Cash)
```",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page of data to request",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="perPage",
     *         in="query",
     *         description="Number of items per page requested. Default is 10.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "application/json": {
     *                   "data": {
     *                      {
     *                          "type": "employee-payment-method",
     *                          "id": 24,
     *                          "attributes": {
     *                              "type": "Cash",
     *                              "employee_id": 2411,
     *                              "netPay": 5433.5206154
     *                          }
     *                      },
     *                      {
     *                          "type": "employee-payment-method",
     *                          "id": 45,
     *                          "attributes": {
     *                              "type": "Bank Account",
     *                              "bankName": "UNION BANK OF THE PHILIPPINES",
     *                              "accountNumber": "789456 (Current)",
     *                              "employee_id": 2412,
     *                              "netPay": 3951.846154
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll employees not found"
     *              }
     *         }
     *     )
     * )
     */
    public function getEmployeeDisbursementMethod($id, Request $request)
    {
        $payrollGetResponse = $this->requestService->get($id);
        $payrollData = json_decode($payrollGetResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $user = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getEmployeeActiveDisbursementMethod($id, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/disbursement_summary",
     *     summary="Fetch disbursement summary of employee active payment method details",
     *     description="Return active disbursement method for employees within payrun.

Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "application/json": {
     *                   "data": {
     *                      {
     *                          "type": "disbursement-method",
     *                          "name": "Cash",
     *                          "attributes": {
     *                          "count": 11,
     *                          "employees": {
     *                              2411,
     *                              2424,
     *                              2410,
     *                              2413,
     *                              2414,
     *                              2415,
     *                              2417,
     *                              2418,
     *                              2419,
     *                              2421,
     *                              2422
     *                          },
     *                          "amount": 1754375.87
     *                          }
     *                      },
     *                      {
     *                          "type": "disbursement-method",
     *                          "name": "Cheque",
     *                          "attributes": {
     *                          "count": 2,
     *                          "employees": {
     *                              2420,
     *                              2423
     *                          },
     *                          "amount": 2512264.34
     *                          }
     *                      },
     *                      {
     *                          "type": "disbursement-method",
     *                          "name": "UNION BANK OF THE PHILIPPINES",
     *                          "attributes": {
     *                             "count": 1,
     *                             "employees": {
     *                                 2412
     *                             },
     *                             "amount": 3951.85,
     *                             "bankFile": {
     *                                 "supported" : true,
     *                                 "bankGroup" : "UNIONBANK"
     *                             }
     *                          }
     *                      },
     *                      {
     *                          "type": "disbursement-method",
     *                          "name": "BANK OF CHINA LIMITED",
     *                          "attributes": {
     *                              "count": 1,
     *                              "employees": {
     *                                  2416
     *                              },
     *                              "amount": 2506.43,
     *                              "bankFile":{
     *                                  "supported" : false
     *                              }
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll employees not found"
     *              }
     *         }
     *     )
     * )
     */
    public function getDisbursementSummary(Request $request, int $id)
    {
        $payrollGetResponse = $this->requestService->get($id);
        $payrollData = json_decode($payrollGetResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeGet(
                $payrollData,
                $user
            );
        }

        //authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getDisbursementSummary($id);
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/has_gap_loan_candidates",
     *     summary="Check if one payroll has gap loan candidates",
     *     description="Check if one payroll has employees
     *     with net pay lower than company's defined minimum takeaway amount
    Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function hasGapLoanCandidates(Request $request, $id)
    {
        $payrollGetResponse = $this->requestService->get($id);
        $payrollData = json_decode($payrollGetResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->hasGapLoanCandidates($id);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/get_gap_loan_candidates",
     *     summary="Get gap loan candidates for payroll.",
     *     description="Get payroll employees who has net pay less then minimum
     *     net take home pay for given payroll id
    Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getGapLoanCandidates(Request $request, $id)
    {
        $payrollGetResponse = $this->requestService->get($id);
        $payrollData = json_decode($payrollGetResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getGapLoanCandidates($id);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payroll/{type}/available_items",
     *     summary="Get Company Payroll available items",
     *     description="Get Company Payroll available items
     Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Payroll type",
     *         required=true,
     *         enum=App\Http\Controllers\PayrollController::PAYROLL_TYPES,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function getCompanyPayrollAvailableItems(Request $request, $id, $type)
    {
        $authorized = false;
        $authzDataScope = null;
        $inputs = $request->all();

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $user = $request->attributes->get('user');

            $authorizationData = (object)[
                'company_id' => $id,
                'account_id' => empty($id) ? null : Company::getAccountId($id),
            ];

            $authorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyPayrollAvailableItems(
            $id,
            $type,
            $authzDataScope,
            $inputs
        );
    }

    /**
     * Get the Job status of Upload tasks by job_id
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\JsonResponse
     */
    private function getUploadJobsStatus(Request $request)
    {
        $inputs = $request->all();
        $validatorAllowedStatus = implode(',', self::UPLOAD_STATUSES);

        $validator = Validator::make($inputs, [
            'job_id' => 'required',
            'include' => 'allowed_array_values:' . $validatorAllowedStatus,
        ], [
            'allowed_array_values' => 'The selected :attribute values are invalid'
        ]);

        if ($validator->fails()) {
            return response($validator->errors()->getMessages())
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        $response = $this->jobsRequestService->getJobStatus($inputs['job_id'], $inputs['include'] ?? false);

        try {
            $responseData = json_decode($response->getData(), true);
            $jobStatus = Arr::get($responseData, 'data.attributes.status', null);
            $errors = Arr::get($responseData, 'data.relationships.errors', null);
            $payload = Arr::get($responseData, 'data.attributes.payload', null);
            $companyId = data_get($payload, 'company_id');
            $uploadType = str_replace('validate-', '', $payload['name']);

            if ($jobStatus == 'FINISHED') {
                $this->audit($request, $companyId, ['Upload Payroll Data Status' => [
                    'type' => $uploadType,
                    'status' => $jobStatus
                ]]);
            }

            if ($errors && count($errors) > 0) {
                $errors = Arr::get($responseData, 'included', null);
                $errorDetails = [];
                foreach ($errors as $error) {
                    $description = Arr::get($error, 'attributes.description.errors', null);
                    if ($error['type'] == 'job-errors' && $description) {
                        array_push($errorDetails, $description);
                    }
                }

                if (count($errorDetails) > 0) {
                    $this->audit($request, $companyId, ['Upload Payroll Data Status' => [
                        'type' => $uploadType,
                        'status' => 'FAILED',
                        'errors' => $errorDetails
                    ]]);
                }
            }
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/payrolls",
     *     summary="Delete Payroll",
     *     description="Delete Multiple Payroll for given ids

Authorization Scope : **delete.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll IDs",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteMany(Request $request)
    {
        $companyId = $request->input('company_id');
        if (!$this->isAuthzEnabled($request)) {
            $authorized = $this->checkAuthorizationMultipleItems($request, $request->input('id'), 'delete');
            if (!$authorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        try {
            $response = $this->requestService->deleteMany($request->all(), $this->getAuthzDataScope($request));
            $payrolls = json_decode($response->getOriginalContent(), true);

            // audit log
            try {
                foreach ($payrolls['data'] as $payroll) {
                    $this->audit($request, $companyId, [], $payroll);
                }
            } catch (\Throwable $e) {
                // Do Nothing
            }

            return $response;
        } catch (Exception $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    /**
     * @SWG\DELETE(
     *     path="/payrolls/delete_payroll_job",
     *     summary="Start Job To Delete Multiple Payrolls",
     *     description="Start Job To Delete Multiple Payrolls for given ids",

     *     tags={"payroll"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll IDs",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function deleteManyJob(Request $request)
    {
        if (!$this->isAuthzEnabled($request)) {
            $authorized = $this->checkAuthorizationMultipleItems($request, $request->input('id'), 'delete');
            if (!$authorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        try {
            $response = $this->requestService->deleteManyJob($request->all(), $this->getAuthzDataScope($request));

            try {
                $this->audit($request, null, [], ['Trigger Payroll Delete' => $request->all()]);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return $response;
        } catch (Exception $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    public function deleteManyJobStatus($jobId)
    {
        return $this->requestService->deleteManyJobStatus($jobId);
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{payroll_id}/job/{job_name}",
     *     summary="Get Payroll Job Status --  with results and/or errors",
     *     description="Get Payroll Job Status --  with results and/or errors

Authorization Scope : **view.payroll**",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="job_name",
     *         in="path",
     *         description="Job Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getPayrollJobByName(Request $request, $payrollId, $jobName)
    {
        $payrollGetResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollGetResponse->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getPayrollJobDetail($payrollId, $jobName);
    }

    /**
     * @todo add endpoint to get multiple payrolls
     * get payroll data and run authorization tasks
     */
    private function checkAuthorizationMultipleItems(Request $request, array $payrollIds, $authorizationTask)
    {
        $payrolls = [];

        $authUser = $request->attributes->get('user');

        $task = camel_case("authorize_{$authorizationTask}");

        $methods = get_class_methods(PayrollAuthorizationService::class);

        if (!in_array($task, $methods)) {
            return false;
        }

        foreach ($payrollIds as $id) {
            $response = $this->requestService->get($id);
            $payrollData = $payrolls[] = json_decode($response->getData());

            // authorize
            if (
                !$this->authorizationService->{$task}(
                    $payrollData,
                    $authUser
                )
            ) {
                return false;
            }
        }

        return $payrolls;
    }

    /**
     * @SWG\Get(
     *     path="/payroll_register/{payroll_id}",
     *     summary="Generate Payroll register",
     *     description="Get a payroll register based on the given payroll id

Authorization Scope : **get.payroll**",
     *     tags={"payroll register"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getPayrollRegister(Request $request, $payrollId)
    {
        $payrollGetResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollGetResponse->getData());
        $viewedBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeGet(
                $payrollData,
                $viewedBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getPayrollRegister($payrollId);
    }

    /**
     * @SWG\Post(
     *     path="/payroll_register/{payroll_id}",
     *     summary="Generate Payroll register",
     *     description="Generates a payroll register based on the given payroll id

Authorization Scope : **get.payroll**",
     *     tags={"payroll register"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createPayrollRegister(Request $request, $payrollId)
    {
        $payrollGetResponse = $this->requestService->get($payrollId);
        $payrollData = json_decode($payrollGetResponse->getData());
        $viewedBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeGet(
                $payrollData,
                $viewedBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->createPayrollRegister($payrollId);
    }

    /**
     * @SWG\Post(
     *     path="/payroll_register/multiple",
     *     summary="Generate Payroll register",
     *     description="Generate payroll register of multiple payrolls

Authorization Scope : **get.payroll**",
     *     tags={"payroll register"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll IDs",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createMultiplePayrollRegisters(Request $request, PayrollAuthz $payrollAuthz)
    {
        if (empty($request->input('id'))) {
            return $this->response()->errorBadRequest("Payroll id is required.");
        }
        $payrollIds = $request->input('id');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $payrollAuthz->isAllAuthorized($payrollIds, $this->getAuthzDataScope($request));
        } else {
            $authorized = $this->checkAuthorizationMultipleItems($request, $request->input('id'), 'get');
        }

        if (!$authorized) {
            return $this->response()->errorUnauthorized();
        }

        $userId = $request->attributes->get('user')['user_id'];

        if (empty($userId)) {
            return $this->response()->errorBadRequest("User details is required.");
        }

        $userRequestService = App::make(UserRequestService::class);

        $response = $userRequestService->getUserInformations($userId);
        $responseData = json_decode($response->getData(), true);

        $response = $this->requestService->createMultiplePayrollRegisters(
            $request->only('id'),
            array_only($responseData, ['id', 'first_name', 'email'])
        );

        try {
            $this->audit($request, null, [], ['Trigger Generate Payroll Register' => $request->only('id')]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/payroll_register/{id}/has_payroll_register",
     *     summary="Check if Payroll has Payroll Register",
     *     description="Check if Payroll has Payroll Register already generated",
     *     tags={"payroll register"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function hasPayrollRegister($id, Request $request)
    {
        //check payroll exists (will throw exception if payroll doesn't exist)
        $response = $this->requestService->get($id);
        $payrollData = json_decode($response->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)
                ->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }
        $hasPayrollRegister = ['has_payroll_register' => false];

        try {
            $payrollRegisterResponse = $this->requestService->getPayrollRegister($id);
            if ($payrollRegisterResponse->getStatusCode() === Response::HTTP_OK) {
                $hasPayrollRegister['has_payroll_register'] = true;
            }
        } catch (HttpException $e) {
            $e;
        }

        return $this->response()->array($hasPayrollRegister);
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{id}/bank_advise",
     *     summary="Generate Bank's Payroll Summary and Transmittal document",
     *     description="Create Bank Advise for the supported banks

      Currently supporting the following bank groups:
      - BDO
      - BPI
      - Metrobank
      - Unionbank",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="bank: Name of company bank found in company disbursement settings",
     *         required=false,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="data",
     *                  type="object",
     *                  @SWG\Property(property="bank", type="string", example="BANCO DE ORO ELITE SAVINGS BANK")
     *             ),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Payroll exists but the specified bank is not supported.",
     *         examples={
     *             "Payroll exists but requested bank is not available": {
     *                  "message" : "No disbursement record available with this bank."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Problem with payload",
     *         examples={
     *             "Payroll is open": {
     *                  "message" : "Only closed payroll can access the bank advise."
     *              },
     *             "JSON is malformed": {
     *                  "message" : "Data not found"
     *              },
     *             "bank data is missing": {
     *                  "message" : "Bank attribute is missing"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getBankAdvise(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $payrollData = json_decode($response->getData());
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $responseBankAdvise = $this->requestService->getBankAdvise($id, $request->json('data.bank'));

        try {
            $responseBankAdviseData = json_decode($responseBankAdvise->getData());
            $this->audit($request, $payrollData->company_id, [], ['Bank Advise' => $responseBankAdviseData]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $responseBankAdvise;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/{id}/bank_file",
     *     summary="Generate Bank File document",
     *     description="Create Bank File for the supported banks

      Currently supporting the following bank groups:
      - BDO
      - BPI
      - Metrobank
      - Unionbank",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="bank: Name of company bank found in company disbursement settings",
     *         required=false,
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="data",
     *                  type="object",
     *                  @SWG\Property(property="bank", type="string", example="BANCO DE ORO ELITE SAVINGS BANK")
     *             ),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Server understand the request, but not processable",
     *         examples={
     *             "Payroll exists but requested bank is not available": {
     *                  "message": "No disbursement record available with this bank."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Problem with payload",
     *         examples={
     *             "Payroll is open": {
     *                  "message" : "Only closed payroll can access the bank file."
     *              },
     *             "JSON is malformed": {
     *                  "message" : "Data not found"
     *              },
     *             "bank data is missing": {
     *                  "message" : "Bank attribute is missing"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */

    public function getBankFile(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $payrollData = json_decode($response->getData());
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $responseBankFile = $this->requestService->getBankFile($id, $request->json('data.bank'));

        try {
            $responseBankFileData = json_decode($responseBankFile->getData());
            $this->audit($request, $payrollData->company_id, [], ['Bank File' => $responseBankFileData]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $responseBankFile;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payroll_data_counts",
     *     summary="Get company payroll statistics data",
     *     description="Get company payroll statistics data.",
     *     tags={"admin_dashboard"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Items(type="integer")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyPayrollDataCounts(Request $request, $companyId)
    {
        $user = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        try {
            // Fetch new data from microservice
            $payrollData = $this->requestService->getCompanyPayrollDataCounts(
                $companyId
            );

            return $payrollData;
        } catch (Exception $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/payrolls/jobs/status",
     *     summary="Get the latest generate and regenerate jobs status for multiple jobs",
     *     description="Get the latest generate and regenerate jobs status for multiple jobs",
     *     tags={"payroll"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(
     *            type="object",
     *            @SWG\Property(
     *               property="payroll_ids",
     *               type="array",
     *               @SWG\Items(type="number")
     *             )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     )
     * )
     */
    public function getPayrollsJobsStatus(Request $request)
    {
        $payrollIds = array_unique($request->input('payroll_ids', []));
        return $this->requestService->getPayrollsJobsStatus($payrollIds);
    }
}
