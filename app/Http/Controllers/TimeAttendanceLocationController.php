<?php

namespace App\Http\Controllers;

use App\Facades\Company;
use App\Location\LocationAuthorizationService;
use Illuminate\Http\Request;
use App\Location\TimeAttendanceLocationRequestService;
use App\Audit\AuditService;
use App\Audit\AuditItem;
use App\Authz\AuthzDataScope;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;

class TimeAttendanceLocationController extends Controller
{
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';
    const ACTION_UPDATE = 'update';

    const OBJECT_NAME = 'location';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /*
     * App\Location\TimeAttendanceLocationRequestService
     */
    protected $requestService;

    /**
     * @var \App\Location\LocationAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        TimeAttendanceLocationRequestService $requestService,
        LocationAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/time_attendance_locations/{id}",
     *     summary="Get TimeAttendance Location",
     *     description="Get TimeAttendance Location Details

Authorization Scope : **view.location**",
     *     tags={"time_attendance_locations"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Location ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        // authorize
        $response = $this->requestService->get($id);
        $locationData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($locationData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        // call microservice
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/time_attendance_locations",
     *     summary="Get TimeAttendance Company Locations",
     *     description="Get TimeAttendance Company Locations

Authorization Scope : **view.location**",
     *     tags={"time_attendance_locations"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="mode",
     *         in="query",
     *         description="Response mode",
     *         required=false,
     *         type="string",
     *         enum={"FILTER_MODE","DEFAULT_MODE"},
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAll(Request $request, int $companyId)
    {
        $params = $request->only(['name', 'limit', 'is_simplified', 'mode']);
        $dataScope = $request->attributes->get('authz_data_scope');
        $response = $this->requestService->getCompanyLocations($companyId, $params, $dataScope);
        $locationData = json_decode($response->getData())->data;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $location = $locationData[0];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyLocations(
                $location,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (empty($locationData)) {
            return $response;
        }

        // call microservice
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/time_attendance_locations/bulk_create",
     *     summary="Create multiple TimeAttendance locations",
     *     description="Create multiple TimeAttendance locations

Authorization Scope : **create.location**",
     *     tags={"time_attendance_locations"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newLocations"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newLocations",
     *     required={"company_id", "locations"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="locations",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/newLocation"
     *         )
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="newLocation",
     *     required={"name", "company_id", "timezone", "country", "region",
     *               "city", "zip_code", "is_headquarters", "address_bar",
     *               "location_pin", "first_address_line", "second_address_line"},
     *     @SWG\Property(
     *         property="name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="timezone",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="country",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="region",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="city",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="zip_code",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="is_headquarters",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="address_bar",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="location_pin",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="first_address_line",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="second_address_line",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="ip_addresses",
     *         type="array",
     *         @SWG\Items(
     *            type="string"
     *         )
     *     ),
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $locationData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($locationData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        foreach ($responseData['ids'] as $id) {
            $locationResponse = $this->requestService->get($id);
            $locationData = json_decode($locationResponse->getData(), true);
            $item = new AuditItem([
                'company_id' => $inputs['company_id'],
                'account_id' => $createdBy['account_id'],
                'user_id' => $createdBy['user_id'],
                'action' => self::ACTION_CREATE,
                'object_name' => self::OBJECT_NAME,
                'data' => $locationData
            ]);
            $this->auditService->log($item);
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/time_attendance_locations/{id}",
     *     summary="Update TimeAttendance Location",
     *     description="Update TimeAttendance Location
Authorization Scope : **edit.location**",
     *     tags={"time_attendance_locations"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Location Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         type="string",
     *         in="formData",
     *         description="Name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="timezone",
     *         type="string",
     *         in="formData",
     *         description="Timezone",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="country",
     *         type="string",
     *         in="formData",
     *         description="Country",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="region",
     *         type="string",
     *         in="formData",
     *         description="Region",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         type="string",
     *         in="formData",
     *         description="City",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="zip_code",
     *         type="string",
     *         in="formData",
     *         description="Zip code",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="is_headquarters",
     *         type="boolean",
     *         in="formData",
     *         description="Is headquarters",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="address_bar",
     *         type="string",
     *         in="formData",
     *         description="Address bar",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="location_pin",
     *         type="string",
     *         in="formData",
     *         description="Location pin",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="first_address_line",
     *         type="string",
     *         in="formData",
     *         description="First address line",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="second_address_line",
     *         type="string",
     *         in="formData",
     *         description="Second address line",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="ip_addresses[]",
     *         type="array",
     *         in="formData",
     *         description="IP addresses",
     *         required=false,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldLocationData = json_decode($response->getData());
        $oldLocationData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $dataScope = $request->attributes->get('authz_data_scope');

        // check if new company_id and old company_id is in company scope
        if (
            $authzEnabled &&
            (
                !$dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']) ||
                !$dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $oldLocationData->company_id)
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        if (!$authzEnabled && !$this->authorizationService->authorizeUpdate($oldLocationData, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldLocationData = json_decode($response->getData(), true);
        $newLocationData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldLocationData,
            'new' => $newLocationData,
        ];
        $item = new AuditItem([
            'company_id' => $inputs['company_id'],
            'account_id' => $updatedBy['account_id'],
            'user_id' => $updatedBy['user_id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $details
        ]);
        $this->auditService->log($item);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/time_attendance_locations/bulk_delete",
     *     summary="Delete multiple TimeAttendance Locations",
     *     description="Delete multiple TimeAttendance Locations
Authorization Scope : **delete.location**",
     *     tags={"time_attendance_locations"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="location_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Location Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request, PhilippineCompanyRequestService $companyRequestService)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        $companyId = $inputs['company_id'];
        $isAuthorized = false;
        if ($this->isAuthzEnabled($request)) {
            $dataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $dataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $locationData = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($locationData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inUseResponse = $companyRequestService->isInUse('location', ['ids' => $inputs['location_ids']]);
        $inUseData = json_decode($inUseResponse->getData());

        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('location_ids', []) as $id) {
                $data = [
                    'old' => [
                        'id' => $id
                    ]
                ];
                $item = new AuditItem([
                    'company_id' => $inputs['company_id'],
                    'account_id' => $deletedBy['account_id'],
                    'user_id' => $deletedBy['user_id'],
                    'action' => self::ACTION_DELETE,
                    'object_name' => self::OBJECT_NAME,
                    'data' => $data
                ]);
                $this->auditService->log($item);
            }
        }

        return $response;
    }

    /**
    * @SWG\Post(
    *     path="/time_attendance_locations/",
    *     summary="Create time and attendance location",
    *     description="Create time and attendance location
Authorization Scope : **create.location**",
    *     tags={"time_attendance_locations"},
    *     consumes={"application/x-www-form-urlencoded"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true,
    *         description="Salarium Module Map"
    *     ),
    *     @SWG\Parameter(
    *         name="company_id",
    *         in="formData",
    *         description="Company Id",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="name",
    *         type="string",
    *         in="formData",
    *         description="Name",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="timezone",
    *         type="string",
    *         in="formData",
    *         description="Timezone",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="country",
    *         type="string",
    *         in="formData",
    *         description="Country",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="region",
    *         type="string",
    *         in="formData",
    *         description="Region",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="city",
    *         type="string",
    *         in="formData",
    *         description="City",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="zip_code",
    *         type="string",
    *         in="formData",
    *         description="Zip code",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="is_headquarters",
    *         type="boolean",
    *         in="formData",
    *         description="Is headquarters",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="address_bar",
    *         type="string",
    *         in="formData",
    *         description="Address bar",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="location_pin",
    *         type="string",
    *         in="formData",
    *         description="Location pin",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="first_address_line",
    *         type="string",
    *         in="formData",
    *         description="First address line",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="second_address_line",
    *         type="string",
    *         in="formData",
    *         description="Second address line",
    *         required=true,
    *     ),
    *     @SWG\Parameter(
    *         name="ip_addresses[]",
    *         type="array",
    *         in="formData",
    *         description="IP addresses",
    *         required=false,
    *         @SWG\Items(type="string"),
    *         collectionFormat="multi"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
    *         description="Successful operation",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
    *         description="Unauthorized",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $locationData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($locationData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $locationResponse = $this->requestService->get($responseData['id']);
        $locationData = json_decode($locationResponse->getData(), true);
        $item = new AuditItem([
            'company_id' => $inputs['company_id'],
            'account_id' => $createdBy['account_id'],
            'user_id' => $createdBy['user_id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $locationData
        ]);
        $this->auditService->log($item);

        return $response;
    }
}
