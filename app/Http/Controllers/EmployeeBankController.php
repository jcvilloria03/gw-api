<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\Bank\BankRequestService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeRequestService;
use App\Bank\EmployeeBankAuthorizationService;
use App\Company\PhilippineCompanyRequestService;
use App\Traits\AuditTrailTrait;

class EmployeeBankController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Bank\EmployeeBankAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        EmployeeBankAuthorizationService $authorizationService,
        EmployeeRequestService $employeeRequestService,
        PhilippineCompanyRequestService $companyRequestService
    ) {
        $this->authorizationService   = $authorizationService;
        $this->employeeRequestService = $employeeRequestService;
        $this->companyRequestService  = $companyRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employee_id}/payment_methods",
     *     summary="Get Employee's Payment Methods",
     *     description="Get Employee's Payment Methods

Authorization Scope : **view.payment_method**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function index(Request $request, $employeeId)
    {
        $employeeResponse = $this->employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($employeeResponse->getData(), true);
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData     = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];

            $authorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }
        $employeePaymentMethods = [];
        try {
            $bankRequestService = App::make(BankRequestService::class);
            $employeePaymentMethods = $bankRequestService->getEmployeePaymentMethods($employeeId);
            return $employeePaymentMethods;
        } catch (\Throwable $th) {
            return response(json_encode(['data'=>[]]), 200);
        }
        
    }

    /**
     * @SWG\Post(
     *     path="/employee/{employee_id}/bank",
     *     summary="Add Employee Bank Account",
     *     description="Add an Employee's Bank Account to disburse to

Authorization Scope : **create.payment_method**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Property(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(property="type",type="string"),
     *                 @SWG\Property(
     *                     property="attributes",
     *                     type="object",
     *                     @SWG\Property(property="bank_id", type="integer"),
     *                     @SWG\Property(property="company_id", type="integer"),
     *                     @SWG\Property(property="account_number", type="string"),
     *                     @SWG\Property(property="account_type", type="string"),
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function store(Request $request, $employeeId)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $authorized = false;
        $response     = $this->employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $authorized = $this->authorizationService
                ->authorizeCreate($companyData, $request->attributes->get('user'));
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $bankRequestService = App::make(BankRequestService::class);

        $response = $bankRequestService->addEmployeeBankAccount($employeeId, $request->all());

        if ($response->isSuccessful()) {
            $newData = json_decode($response->getData(), true);
            // trigger audit trail
            $this->audit($request, $employeeData['company_id'], $newData);
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/employee/payment_method/{employee_payment_method_id}",
     *     summary="Delete Employee Payment Method",
     *     description="Delete an Employee's Payment Method to disburse to",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_payment_method_id",
     *         in="path",
     *         description="Employee Payment Method ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function destroy($employeePaymentMethodId)
    {
        $bankRequestService = App::make(BankRequestService::class);

        return $bankRequestService->deleteEmployeePaymentMethod($employeePaymentMethodId);
    }


    /**
     * @SWG\Patch(
     *     path="/employee/{employee_id}/payment_methods/{payment_method_id}",
     *     summary="Set current payment method active status",
     *     description="Set current payment method active status. Limited to setting to true only.

Authorization Scope : **edit.payment_method**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_method_id",
     *         in="path",
     *         description="Payment method ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/activatePaymentMethodPatch")
      *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="activatePaymentMethodPatch",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="type", type="string", enum={"employee-payment-method"}),
     *         @SWG\Property(property="attributes", type="object",
     *             @SWG\Property(property="active", type="boolean", enum={true})
     *         )
     *     ),
     * )
     */
    public function activatePaymentMethod(Request $request, $employeeId, $paymentMethodId)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $response     = $this->employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                AuthzDataScope::SCOPE_POSITION =>  Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $authorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $bankRequestService = App::make(BankRequestService::class);

        $result = $bankRequestService->activatePaymentMethod($employeeId, $paymentMethodId, $request->all());
        $resultData = json_decode($result->getData(), true);

        if (!empty($resultData['data'])) {
            $bankAccountAndType = explode(' ', $resultData['data']['attributes']['accountNumber'] ?? '');
            $bankType = count($bankAccountAndType) >= 2 ?
                strtr($bankAccountAndType[1], array('(' => '', ')' => '')) : '';
            $bankAccount =count($bankAccountAndType) >= 2 ? $bankAccountAndType[0] : '';

            $this->employeeRequestService->update($employeeId, [
                'payment_method' => $resultData['data']['attributes']['disbursementMethod'] ?? '',
                'bank_name' => $resultData['data']['attributes']['details'] ?? '',
                'bank_account_number' => $bankAccount,
                'bank_type' => $bankType
            ]);
        }

        if ($response->getStatusCode() == 200) {
            // trigger audit trail
            $this->audit($request, $employeeData['company_id'], $resultData);
        }

        return $result;
    }

    /**
     * @SWG\Delete(
     *     path="/employee/{employee_id}/payment_method",
     *     summary="Delete Multilple Employee Payment Method",
     *     description="Delete an Multiple Employee's Payment Method to disburse to

Authorization Scope : **delete.payment_method**",
     *     tags={"employee", "bank"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Property(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(property="type", type="string", enum={"employee-payment-method"}),
     *                 @SWG\Property(
     *                     type="array",
     *                     property="id",
     *                     example={1,2,3}
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function destroyMany(Request $request, $employeeId)
    {
        $employeeResponse     = $this->employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($employeeResponse->getData(), true);
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'team_id'),
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];

            $authorized = $this->authorizationService->authorizeDelete(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }


        $bankRequestService = App::make(BankRequestService::class);
        $requestData = $request->getContent();

        $response = $bankRequestService->deleteEmployeePaymentMethods($requestData, $employeeId);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            foreach ($responseData as $oldData) {
                // trigger audit trail
                $this->audit($request, $employeeData['company_id'], [], $oldData);
            }
        }

        return $response;
    }
}
