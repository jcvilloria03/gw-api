<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\PayrollGroup\PayrollGroupEsIndexQueueService;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\PayrollGroup\PhilippinePayrollGroupRequestService;
use App\PayrollGroup\PayrollGroupAuthorizationService;
use App\PayrollGroup\PayrollGroupAuditService;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Facades\Company;
use Swagger\Annotations as SWG;

class PhilippinePayrollGroupController extends Controller
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\PayrollGroup\PayrollGroupAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\PayrollGroup\PhilippinePayrollGroupRequestService
     */
    protected $requestService;

    /**
     * @var \App\PayrollGroup\PayrollGroupEsIndexQueueService
     */
    private $indexQueueService;

    public function __construct(
        PhilippinePayrollGroupRequestService $requestService,
        PayrollGroupAuthorizationService $authorizationService,
        AuditService $auditService,
        PayrollGroupEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/payroll_group/{id}",
     *     summary="Get Philippine Payroll Group",
     *     description="Get Philippine Payroll Group Details

Authorization Scope : **view.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Group ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful",
     *         @SWG\Schema(ref="#/definitions/PayrollGroup")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Payroll Group not found": {
     *                  "message": "Payroll Group not found.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Payroll Group ID should be numeric" : {
     *                  "message": "Payroll Group ID should not be empty.",
     *                  "status_code": 406
     *              }
     *         }
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $authzDataScope = $this->getAuthzDataScope($request);

        $response = $this->requestService->get($id, $authzDataScope);
        $payrollGroupData = json_decode($response->getData());

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollGroupData->company_id,
            ]);
        } else {
            $isAuthorized = $this
                ->authorizationService
                ->authorizeGet($payrollGroupData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/payroll_group",
     *     summary="Create Philippine PayrollGroup",
     *     description="Create New Philippine PayrollGroup

Authorization Scope : **create.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Payroll Group Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="account_id",
     *         in="formData",
     *         description="Account Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_frequency",
     *         in="formData",
     *         description="Payroll Frequency",
     *         required=true,
     *         type="string",
     *         enum={"MONTHLY", "SEMI_MONTHLY", "FORTNIGHTLY", "WEEKLY"}
     *     ),
     *     @SWG\Parameter(
     *         name="day_factor",
     *         in="formData",
     *         description="Day Factor",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="hours_per_day",
     *         in="formData",
     *         description="Hours per day",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="non_working_day_option",
     *         in="formData",
     *         description="What happens when payroll date falls on non-working day",
     *         required=true,
     *         type="string",
     *         enum={"BEFORE", "AFTER", "ON"}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_date",
     *         in="formData",
     *         description="Pay Date (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fpd_end_of_month",
     *         in="formData",
     *         description="First pay date end of month (0/1)",
     *         required=true,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="cut_off_date",
     *         in="formData",
     *         description="Cut Off Date (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fcd_end_of_month",
     *         in="formData",
     *         description="First cut off date end of month (0/1)",
     *         required=true,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_run_posting",
     *         in="formData",
     *         description="First Pay Run Posting Date (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="fpp_end_of_month",
     *         in="formData",
     *         description="First pay run posting end of month (0/1)",
     *         required=true,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_date_2",
     *         in="formData",
     *         description="Second Pay Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="spd_end_of_month",
     *         in="formData",
     *         description="Second pay date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="cut_off_date_2",
     *         in="formData",
     *         description="Second Cut Off Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="scd_end_of_month",
     *         in="formData",
     *         description="Second cut off date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_run_posting_2",
     *         in="formData",
     *         description="Second Pay Run Posting Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="spp_end_of_month",
     *         in="formData",
     *         description="Second pay run posting end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="sss_schedule",
     *         in="formData",
     *         description="SSS Schedule",
     *         required=true,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="sss_method",
     *         in="formData",
     *         description="SSS Method",
     *         required=true,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth_schedule",
     *         in="formData",
     *         description="PhilHealth Schedule",
     *         required=true,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth_method",
     *         in="formData",
     *         description="PhilHealth Method",
     *         required=true,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf_schedule",
     *         in="formData",
     *         description="HDMF Schedule",
     *         required=true,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf_method",
     *         in="formData",
     *         description="HDMF Method",
     *         required=true,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="wtax_schedule",
     *         in="formData",
     *         description="WTax Schedule",
     *         required=true,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="wtax_method",
     *         in="formData",
     *         description="WTax Method",
     *         required=true,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *      ),
     *     @SWG\Parameter(
     *         name="enforce_gap_loans",
     *         in="formData",
     *         description="Enforce Gap Loans",
     *         required=true,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="minimum_net_take_home_pay",
     *         in="formData",
     *         description="Minimum Net Take Home Pay Value",
     *         required=false,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="compute_overbreak",
     *         in="formData",
     *         description="Compute Overbreak Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="compute_undertime",
     *         in="formData",
     *         description="Compute Undertime Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="compute_tardiness",
     *         in="formData",
     *         description="Compute Tardiness Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'account_id' => 'required|integer',
        ]);

        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzDataScope = $request->attributes->get(AuthzDataScope::REQUEST_AUTHZ_DATA_SCOPE);
        $isAuthorized = false;

        if ($request->attributes->get(AuthzDataScope::REQUEST_AUTHZ_ENABLED)) {
            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $payrollGroupData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($payrollGroupData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->create($inputs, $authzDataScope);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        return $response;
    }
    /**
     * @SWG\PATCH(
     *     path="/philippine/payroll_group/{payroll_group_id}",
     *     summary="Update Philippine PayrollGroup",
     *     description="Update New Philippine PayrollGroup
     Authorization Scope : **edit.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="path",
     *         description="Payroll group Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Payroll Group Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_frequency",
     *         in="formData",
     *         description="Payroll Frequency",
     *         required=false,
     *         type="string",
     *         enum={"MONTHLY", "SEMI_MONTHLY", "FORTNIGHTLY", "WEEKLY"}
     *     ),
     *     @SWG\Parameter(
     *         name="day_factor",
     *         in="formData",
     *         description="Day Factor",
     *         required=false,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="hours_per_day",
     *         in="formData",
     *         description="Hours per day",
     *         required=false,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="non_working_day_option",
     *         in="formData",
     *         description="What happens when payroll date falls on non-working day",
     *         required=false,
     *         type="string",
     *         enum={"BEFORE", "AFTER", "ON"}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_date",
     *         in="formData",
     *         description="Pay Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fpd_end_of_month",
     *         in="formData",
     *         description="First pay date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="cut_off_date",
     *         in="formData",
     *         description="Cut Off Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fcd_end_of_month",
     *         in="formData",
     *         description="First cut off date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_run_posting",
     *         in="formData",
     *         description="First Pay Run Posting Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         name="fpp_end_of_month",
     *         in="formData",
     *         description="First pay run posting end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_date_2",
     *         in="formData",
     *         description="Second Pay Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="spd_end_of_month",
     *         in="formData",
     *         description="Second pay date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="cut_off_date_2",
     *         in="formData",
     *         description="Second Cut Off Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="scd_end_of_month",
     *         in="formData",
     *         description="Second cut off date end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="pay_run_posting_2",
     *         in="formData",
     *         description="Second Pay Run Posting Date (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="spp_end_of_month",
     *         in="formData",
     *         description="Second pay run posting end of month (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="sss_schedule",
     *         in="formData",
     *         description="SSS Schedule",
     *         required=false,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="sss_method",
     *         in="formData",
     *         description="SSS Method",
     *         required=false,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth_schedule",
     *         in="formData",
     *         description="PhilHealth Schedule",
     *         required=false,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth_method",
     *         in="formData",
     *         description="PhilHealth Method",
     *         required=false,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf_schedule",
     *         in="formData",
     *         description="HDMF Schedule",
     *         required=false,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf_method",
     *         in="formData",
     *         description="HDMF Method",
     *         required=false,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *     ),
     *     @SWG\Parameter(
     *         name="wtax_schedule",
     *         in="formData",
     *         description="WTax Schedule",
     *         required=false,
     *         type="string",
     *         enum={"EVERY_PAY", "FIRST_PAY", "LAST_PAY"}
     *     ),
     *     @SWG\Parameter(
     *         name="wtax_method",
     *         in="formData",
     *         description="WTax Method",
     *         required=false,
     *         type="string",
     *         enum={"BASED_ON_ACTUAL", "EQUALLY_SPLIT"}
     *      ),
     *     @SWG\Parameter(
     *         name="enforce_gap_loans",
     *         in="formData",
     *         description="Enforce Gap Loans",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="minimum_net_take_home_pay",
     *         in="formData",
     *         description="Minimum Net Take Home Pay Value",
     *         required=false,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="compute_overbreak",
     *         in="formData",
     *         description="Compute Overbreak Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="compute_undertime",
     *         in="formData",
     *         description="Compute Undertime Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Parameter(
     *         name="compute_tardiness",
     *         in="formData",
     *         description="Compute Tardiness Flag (0/1)",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="int"),
     *         ),
     *         examples={
     *             "application/json": {
     *                 "id": 3243,
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $payrollGroupId)
    {
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($payrollGroupId);
        $oldCompanyData = json_decode($response->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this
                ->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $oldCompanyData->company_id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldCompanyData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $updateResponse = $this->requestService->update($inputs, $payrollGroupId);

        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($payrollGroupId);
        $oldCompanyData = json_decode($response->getData(), true);
        $newCompanyData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldCompanyData,
            'new' => $newCompanyData,
        ];
        $item = new AuditCacheItem(
            PayrollGroupAuditService::class,
            PayrollGroupAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company/{id}/payroll_groups",
     *     summary="Get Payroll Groups",
     *     description="Get list of Payroll Group within the given company Id

Authorization Scope : **view.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAll($id, Request $request)
    {
        if (!is_numeric($id)) {
            $this->invalidRequestError('Company ID should be numeric.');
        }

        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request) && !$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getAll($id, $authzDataScope);
        if (!$this->isAuthzEnabled($request)) {
            $payrollGroupData = json_decode($response->getData(), true);
            if (empty($payrollGroupData['data'])) {
                return $response;
            }
            $payrollGroup = $payrollGroupData['data'][0];
            if (
            !$this->authorizationService->authorizeGetCompanyPayrollGroups(
                $payrollGroup['account_id'],
                $payrollGroup['company_id'],
                $request->attributes->get('user')
            )
            ) {
                $this->response()->errorUnauthorized();
            }
        }

        return $response;
    }
}
