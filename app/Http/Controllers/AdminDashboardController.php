<?php

namespace App\Http\Controllers;

use App\AdminDashboard\AdminDashboardAuthorizationService;
use App\AdminDashboard\AdminDashboardRequestService;
use App\Employee\EmployeeRequestService;
use App\Attendance\AttendanceRequestService;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

/**
 * Class AdminDashboardController
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 *
 * @package App\Http\Controllers
 */
class AdminDashboardController extends Controller
{
    const ATTENDANCE_PRESENT = "present";
    const ATTENDANCE_TARDY = "tardy";
    const ATTENDANCE_ON_LEAVE = "on_leave";
    const ATTENDANCE_ABSENT = "absent";
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Account\AccountRequestService
     */
    protected $requestService;

    /**
     * @var \App\AdminDashboard\AdminDashboardAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /**
     * @var \App\Attendance\AttendanceRequestService
     */
    protected $attendanceRequestService;

    /**
     * AdminDashboardController constructor.
     *
     * @param AdminDashboardRequestService       $requestService       AdminDashboardRequestService
     * @param AdminDashboardAuthorizationService $authorizationService AdminDashboardAuthorizationService
     * @param EmployeeRequestService $employeeRequestService EmployeeRequestService
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     */
    public function __construct(
        AdminDashboardRequestService $requestService,
        AdminDashboardAuthorizationService $authorizationService,
        EmployeeRequestService $employeeRequestService,
        AttendanceRequestService $attendanceRequestService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->employeeRequestService = $employeeRequestService;
        $this->attendanceRequestService = $attendanceRequestService;
    }

    /**
     * @SWG\Post(
     *     path="/admin_dashboard/calendar_data",
     *     summary="Get calendar data",
     *     description="Get calendar data of employees.",
     *     tags={"admin_dashboard"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Items(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="A inclusive lower date boundary for filtering calendar data in format YYYY-MM-DD",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="formData",
     *         description="A inclusive upper date boundary for filtering calendar data in format YYYY-MM-DD",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="group[]",
     *         in="formData",
     *         description="Application group (payroll || time_attendance)",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="string",enum={"payroll","time_attendance"}),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAllCalendarData(Request $request)
    {
        $inputs = $request->all();
        $user = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAllCalendarData($inputs, $this->getAuthzDataScope($request));

    }

    /**
     * @SWG\Post(
     *     path="/admin_dashboard/calendar_data_counts",
     *     summary="Get calendar data counts",
     *     description="Get calendar data counts of employees.",
     *     tags={"admin_dashboard"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Items(type="integer")
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="A inclusive lower date boundary for filtering calendar data in format YYYY-MM-DD",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="formData",
     *         description="A inclusive upper date boundary for filtering calendar data in format YYYY-MM-DD",
     *         required=true,
     *         type="string",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="group[]",
     *         in="formData",
     *         description="Application group (payroll || time_attendance)",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="string",enum={"payroll","time_attendance"}),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAllCalendarDataCounts(Request $request)
    {
        $inputs = $request->all();
        $user = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Compute for cache expiry every 1st or 16th of month
        $currentDay = Carbon::now()->format('d');
        $expiry = (int)$currentDay > 16 ?
            Carbon::now()->addMonth()->firstOfMonth() :
            Carbon::now()->firstOfMonth()->addDays(15);
        // Set Key; formula = 'calendar-data:company_id:' + $companyId
        $cacheKey = 'calendar-data:company_id:' . $companyId;
        $dateToday = Carbon::now()->endOfDay()->toDateString();
        $useCache = $dateToday == $request->input('start_date') || $dateToday == $request->input('end_date');

        // Get leaves from cache, if none, fetch new data and set to cache
        if (Redis::hExists($cacheKey, $user['user_id']) && $useCache) {
            $cache = json_decode(Redis::hGet($cacheKey, $user['user_id']));

            if (Carbon::parse($cache->expires) <= Carbon::now()) {
                Redis::hDel($cacheKey, $user['user_id']);
                // Fetch new data from microservice
                $calendarData = $this->requestService->getAllCalendarDataCounts(
                    $inputs,
                    $this->getAuthzDataScope($request)
                );
                $status = $calendarData->status();
                $calendarData = $calendarData->getContent();
                // Save data to cache if status code is 200
                if ($status == 200) {
                    Redis::hMSet(
                        $cacheKey,
                        [$user['user_id']=>json_encode(['data'=>$calendarData, 'expires'=>$expiry->toDateString()])]
                    );
                    // Set life of cache to maximum of 15 days
                    Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
                }
            } else {
                $calendarData = $cache->data;
            }
        } else {
            // Fetch new data from microservice
            $calendarData = $this->requestService->getAllCalendarDataCounts(
                $inputs,
                $this->getAuthzDataScope($request)
            );
            $status = $calendarData->status();
            $calendarData = $calendarData->getContent();
            // Save data to cache if status code is 200
            if ($status == 200 && $useCache) {
                Redis::hMSet(
                    $cacheKey,
                    [$user['user_id']=>json_encode(['data'=>$calendarData, 'expires'=>$expiry->toDateString()])]
                );
                // Set life of cache to maximum of 15 days
                Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
            }
        }
        return response()->json(json_decode($calendarData));
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/scorecard/download",
     *     summary="Download employees data",
     *     description="Download generated employees CSV data
    Authorization Scope : **create.scorecard.export**",
     *     tags={"admin_dashboard"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="export_type",
     *         in="formData",
     *         description="CSV Export Type. Possible values: absent, on_leave, late, present",
     *         required=true,
     *         type="string",
     *         enum={"absent","on_leave","late","present"},
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Employee IDs",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function download(Request $request, int $id)
    {
        $inputs = $request->all();
        $user = $request->attributes->get('user');

        $authorizationData = (object)[
            'company_id' => $id,
            'account_id' => empty($id) ? null : Company::getAccountId($id)
        ];

        if (!$this->authorizationService->authorizeExportData($authorizationData, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->downloadCsv($id, $inputs);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/dashboard_statistics",
     *     summary="Get dashboard statistics",
     *     description="Get dashboard statistics",
     *     deprecated=true,
     *     tags={"admin_dashboard"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getDashboardStatistics(Request $request, int $id)
    {
        $user = $request->attributes->get('user');

        $authorizationData = (object)[
            'company_id' => $id,
            'account_id' => empty($id) ? null : Company::getAccountId($id),
        ];

        if (!$this->authorizationService->authorizeGet($authorizationData, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getDashboardStatistics(['company_id' => $id]);
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/active_employees_count",
     *     summary="Get the number of Active Employees, per Company",
     *     description="Get the number of Active Employees, per Company.
     Authorization Scope : **view.admin_dashboard**",
     *     tags={"admin_dashboard"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully fetched number of Active Employees in a Company": {
     *                  "data": {
     *                      "count": 2
     *                  }
     *              },
     *              "No Active Employees in a Company": {
     *                  "data": {
     *                      "count": 0
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Missing company_id": {
     *                  "message": "The company id field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid or non existing company_id": {
     *                  "message": "The selected company id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function getActiveEmployeesCount(Request $request, int $companyId)
    {
        $authorized = false;
        $authzDataScope = null;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $user = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $resultData = $this->employeeRequestService->getActiveEmployeesCount($companyId, $authzDataScope);
        $result = json_decode($resultData->getData(), true)['data'];

        return [
            'data' => [
                'count' => $result['count'] ?? 0,
            ],
        ];
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/attendance_stats",
     *     summary="Get Attendance Statistics for Admin Dashboard, per Company",
     *     description="Get Attendance Statistics for Admin Dashboard, per Company.
This endpoint uses the current date as the reference date of the Attendance Statistics
     Authorization Scope : **view.admin_dashboard**",
     *     tags={"admin_dashboard"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully fetched Attendance Statistics per Company": {
     *                  "data": {
     *                      "present": 23,
     *                      "tardy": 24,
     *                      "absent": 34,
     *                      "on_leave": 21
     *                  }
     *              },
     *              "No results": {
     *                  "data": {
     *                      "present": 0,
     *                      "tardy": 0,
     *                      "absent": 0,
     *                      "on_leave": 0
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Missing company_id": {
     *                  "message": "The company id field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid or non existing company_id": {
     *                  "message": "The selected company id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function getAttendanceStatistics(Request $request, int $companyId)
    {
        $user = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->attendanceRequestService->getAttendanceStatistics($companyId, $authzDataScope);
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/dashboard/attendance",
     *     summary="Get Attendance for Admin Dashboard, per Company, per type, per page",
     *     description="Get Attendance for Admin Dashboard, per Company, per type, per page
     Authorization Scope : **view.admin_dashboard**",
     *     tags={"admin_dashboard"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="Attendance Type",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\Http\Controllers\AdminDashboardController::ATTENDANCE_PRESENT,
     *              App\Http\Controllers\AdminDashboardController::ATTENDANCE_TARDY,
     *              App\Http\Controllers\AdminDashboardController::ATTENDANCE_ON_LEAVE,
     *              App\Http\Controllers\AdminDashboardController::ATTENDANCE_ABSENT
     *         }
     *     ),
     *     @SWG\Parameter(
     *         name="item_count",
     *         in="query",
     *         description="Number of items per page",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target page",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                property="data",
     *                type="object",
     *                @SWG\Property(
     *                  property="employees",
     *                  type="array",
     *                  @SWG\Items(
     *                    type="object",
     *                    @SWG\Property(property="employee_uid", type="integer"),
     *                    @SWG\Property(property="employee_name", type="string"),
     *                    @SWG\Property(property="first_clock_in", type="integer"),
     *                    @SWG\Property(property="remarks", type="string")
     *                  )
     *                ),
     *                @SWG\Property(property="item_count", type="integer"),
     *                @SWG\Property(property="page", type="integer"),
     *                @SWG\Property(property="total_pages", type="integer"),
     *                @SWG\Property(property="total_records", type="integer")
     *             )
     *         ),
     *         examples={
     *              "Successfully fetched Attendance": {
     *                  "data": {
     *                      "employees": {
     *                           {
     *                              "employee_uid": 6754,
     *                              "employee_name": "Alexandra Daddario",
     *                              "first_clock_in": 1589799671,
     *                              "remarks": "Present"
     *                           },
     *                           {
     *                              "employee_uid": 6755,
     *                              "employee_name": "Kobe Bryant",
     *                              "first_clock_in": 1589799971,
     *                              "remarks": "Present"
     *                           }
     *                      },
     *                      "item_count": 10,
     *                      "page": 2,
     *                      "total_pages": 10,
     *                      "total_records": 100
     *                  }
     *              },
     *              "No results": {
     *                  "data": {
     *                      "employees": {},
     *                      "item_count": 10,
     *                      "page": 0,
     *                      "total_pages": 0,
     *                      "total_records": 0
     *                  }
     *              }
    *          }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Missing company_id": {
     *                  "message": "The company id field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid or non existing company_id": {
     *                  "message": "The selected company id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function getDashboardAttendance(Request $request, int $companyId)
    {
        $authorized = false;
        $authzDataScope = null;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId),
            ];

            $user = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($authorizationData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $queryParams = $request->only(['type', 'item_count', 'page', 'search_terms', 'filters' ]);

        return $this->attendanceRequestService->getDashboardAttendance($companyId, $queryParams, $authzDataScope);
    }
}
