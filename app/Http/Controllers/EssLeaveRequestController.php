<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Workflow\WorkflowRequestService;
use App\Http\Controllers\EssBaseController;
use App\LeaveRequest\LeaveRequestAuditService;
use Symfony\Component\HttpFoundation\Response;
use App\LeaveRequest\LeaveRequestRequestService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;

class EssLeaveRequestController extends EssBaseController
{
    /**
     * @var \App\LeaveRequest\LeaveRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        LeaveRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/leave_request",
     *     summary="Create Leave Request",
     *     description="Create Leave Request
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newLeaveRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="newLeaveRequest",
     *     required={"leave_type_id", "start_date", "end_date",
     *     "leaves", "unit"},
     *     @SWG\Property(
     *         property="leave_type_id",
     *         type="integer",
     *         description="Leave Type ID"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Leave request start date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="attachments_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi",
     *         description="Attachments IDs."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         default="2018-12-29",
     *         description="Leave request end date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="messages",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *         description="Initial messages."
     *     ),
     *     @SWG\Property(
     *         property="unit",
     *         type="string",
     *         description="Leave Unit days | hours",
     *         enum={"days", "hours"}
     *     ),
     *     @SWG\Property(
     *         property="leaves",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/leave_request_leave")
     *     )
     * ),
     * @SWG\Definition(
     *     definition="leave_request_leave",
     *     required={"date", "schedule_id", "start_time", "end_time"},
     *     @SWG\Property(
     *         property="schedule_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-01-29",
     *         description="Leave on date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="start_time",
     *         type="string",
     *         default="09:00",
     *         description="Leave start time in format HH:mm."
     *     ),
     *     @SWG\Property(
     *         property="end_time",
     *         type="string",
     *         default="18:00",
     *         description="Leave end time in format HH:mm."
     *     )
     * )
     */
    public function create(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();
        $data['company_id'] = $user['employee_company_id'];
        $data['employee_id'] = $user['employee_id'];
        $data['user_id'] = $user['user_id'];
        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $leaveRequest = $this->requestService->get($responseData['id']);
            $leaveRequestData = json_decode($leaveRequest->getData(), true);

            $details = [
                'new' => [
                    'employee_company_id' => $user['employee_company_id'],
                    'id' => $leaveRequestData['id'],
                ],
            ];

            $item = new AuditCacheItem(
                LeaveRequestAuditService::class,
                LeaveRequestAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                $details
            );

            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/leave_request/{id}",
     *     summary="Get Leave Requests",
     *     description="Get Leave Requests",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Leave request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->get($id);
        $leaveRequest = json_decode($response->getData());
        $leaveRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = ApprovalService::isEitherRequestorOrApproverNew(
                $user['employee_id'],
                $leaveRequest,
                $user
            );
        } else {
            $workflows = $leaveRequest->workflow_levels;
            $isAuthorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $leaveRequest
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }
}
