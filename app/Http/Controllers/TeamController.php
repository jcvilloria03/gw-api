<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\Team\TeamAuditService;
use App\Team\TeamAuthorizationService;
use Illuminate\Http\Request;
use App\Team\TeamRequestService;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TeamController extends Controller
{
    /**
     * @var \App\Team\TeamRequestService
     */
    private $requestService;

    /**
     * @var \App\Team\TeamAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditServiceAuditService
     */
    private $auditService;

    public function __construct(
        TeamRequestService $requestService,
        TeamAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/team/{id}",
     *     summary="Get Team",
     *     description="Get Employment Type Details

Authorization Scope : **view.team**",
     *     tags={"team"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Team ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $teamData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $teamData->company_id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($teamData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/team/",
     *     summary="Create company team",
     *     description="Create company team

Authorization Scope : **create.team**",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Team Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $teamData = (object) [
            'account_id' => $companyId ? Company::getAccountId($companyId) : null,
            'company_id' => $companyId,
        ];
        if (!$this->authorizationService->authorizeCreate($teamData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $teamGetResponse = $this->requestService->get($responseData['id']);
        $teamData = json_decode($teamGetResponse->getData(), true);
        $details = [
            'new' => $teamData,
        ];
        $item = new AuditCacheItem(
            TeamAuditService::class,
            TeamAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/time_attendance_team/",
     *     summary="Create company time attendance team",
     *     description="Create company time attendance team

Authorization Scope : **create.team**",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Team Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="leaders_ids",
     *         in="formData",
     *         description="Employee Id (Leader)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="members_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Employee Ids (Members)",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="edit_team_schedule",
     *         type="boolean",
     *         in="formData",
     *         description="Can edit team schedule?",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="regenerate_team_attendance",
     *         type="boolean",
     *         in="formData",
     *         description="Can regenerate team attendance?",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createTimeAttendanceTeam(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $teamData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($teamData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $payload = $request->all();
        $payload['leaders_ids'] = $payload['leaders_ids'];

        $response = $this->requestService->createTimeAttendanceTeam($payload);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);

            // audit log
            $teamGetResponse = $this->requestService->get($responseData['id']);
            $teamData = json_decode($teamGetResponse->getData(), true);
            $details = [
                'new' => $teamData,
            ];
            $item = new AuditCacheItem(
                TeamAuditService::class,
                TeamAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/team/bulk_create",
     *     summary="Create multiple company teams",
     *     description="Create multiple company teams

Authorization Scope : **create.team**",
     *     tags={"team"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newTeams"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newTeams",
     *     required={"company_id", "names"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="names",
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             description="Array of team names"
     *         )
     *     )
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $teamData = (object) [
            'account_id' => $companyId ? Company::getAccountId($companyId) : null,
            'company_id' => $companyId,
        ];
        if (!$this->authorizationService->authorizeCreate($teamData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        foreach ($responseData['data'] as $team) {
            $teamGetResponse = $this->requestService->get($team['id']);
            $teamData = json_decode($teamGetResponse->getData(), true);
            $details = [
                'new' => $teamData,
            ];
            $item = new AuditCacheItem(
                TeamAuditService::class,
                TeamAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/team/{id}",
     *     summary="Update Team",
     *     description="Update Team
Authorization Scope : **edit.team**",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Team Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Team Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="leaders_ids",
     *         in="formData",
     *         description="Employee Id (Leader)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="members_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Employee Ids (Members)",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="edit_team_schedule",
     *         type="boolean",
     *         in="formData",
     *         description="Can edit team schedule?",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="regenerate_team_attendance",
     *         type="boolean",
     *         in="formData",
     *         description="Can regenerate team attendance?",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(int $id, Request $request)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldTeamData = json_decode($response->getData());
        $oldTeamData->account_id = Company::getAccountId($inputs['company_id']);
        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldTeamData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);
        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldTeamData = json_decode($response->getData(), true);
        $newTeamData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldTeamData,
            'new' => $newTeamData,
        ];
        $item = new AuditCacheItem(
            TeamAuditService::class,
            TeamAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/team/is_name_available",
     *     summary="Is team name available",
     *     description="Check availability of team name with the given company",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Team Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="team_id",
     *         in="formData",
     *         description="Team Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $teamData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($teamData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/teams",
     *     summary="Get all Teams",
     *     description="Get all Teams within company.

Authorization Scope : **view.team**",
     *     tags={"team"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyTeams($id, Request $request)
    {
        $response = $this->requestService->getCompanyTeams($id);
        $teamData = json_decode($response->getData())->data;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanyTeams(
                $teamData[0],
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (empty($teamData)) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/team/bulk_delete",
     *     summary="Delete multiple Teams",
     *     description="Delete multiple Teams
     Authorization Scope : **delete.team**",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="team_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Team Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $teamData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($teamData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        foreach ($request->input('team_ids') as $id) {
            $item = new AuditCacheItem(
                TeamAuditService::class,
                TeamAuditService::ACTION_DELETE,
                new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                [
                    'old' => [
                        'id' => $id,
                        'company_id' => $request->get('company_id')
                    ]
                ]
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/team/check_in_use",
     *     summary="Are Teams in use",
     *     description="Check are Teams in use
Authorization Scope : **view.team**",
     *     tags={"team"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="team_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Teams Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $teamData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $isAuthorized = $$this->authorizationService->authorizeGetCompanyTeams(
                $teamData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }
}
