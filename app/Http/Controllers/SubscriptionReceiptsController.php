<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Subscriptions\SubscriptionsAuthorizationService;

class SubscriptionReceiptsController extends Controller
{
    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionsAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;


    /**
     * SubscriptionsController constructor.
     */
    public function __construct(
        SubscriptionsRequestService $subscriptionsRequestService,
        SubscriptionsAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $subscriptionsRequestService;
        $this->userRequestService = $userRequestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/receipts",
     *     summary="Get list of receipts",
     *     description="Get list of receipts by invoice id or by account id if invoice id is not provided.
If both are provided, search by invoice id will be used.
Authorization Scope : **view.subscriptions**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="filter[invoice_id][]",
     *         type="array",
     *         in="query",
     *         description="ID's",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */

    public function search(Request $request)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        $filter = $request->get('filter');

        return $this->requestService->searchReceipts(
            [$user['account_id']],
            $filter['invoice_id'] ?? []
        );
    }
}
