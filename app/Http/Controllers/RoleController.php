<?php

namespace App\Http\Controllers;

use App\Facades\Company;
use App\Role\RoleService;
use App\Task\TaskService;
use App\Permission\PermissionService;
use App\Role\RoleAuditService;
use App\Audit\AuditUser;
use App\Account\AccountRequestService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Authn\AuthnService;
use App\Role\RoleAuthorizationService;
use App\Role\UserRoleService;
use App\Transformer\RoleTransformer;
use App\User\UserRequestService;
use App\Validator\RoleValidator;
use Illuminate\Http\Request;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RoleController extends Controller
{
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';

    const OBJECT_NAME = 'role';

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Authn\AuthnService
     */
    protected $authnService;

    /**
     * @var \App\Role\RoleAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * @var \App\Role\RoleService
     */
    private $roleService;

    /**
     * @var \App\Task\TaskService
     */
    private $taskService;

    /**
     * @var \App\Permission\PermissionService
     */
    private $permissionService;

    /**
     * @var \App\Transformer\RoleTransformer
     */
    private $transformer;

    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    /**
     * @var \App\User\UserRequestService
     */
    private $userRequestService;

    /**
     * @var \App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * @var \App\Validator\RoleValidator
     */
    private $roleValidator;

    /**
     * @var PhilippineCompanyRequestService
     */
    private $companyRequestService;

    public function __construct(
        RoleService $roleService,
        TaskService $taskService,
        PermissionService $permissionService,
        AccountRequestService $accountRequestService,
        RoleAuthorizationService $authorizationService,
        AuditService $auditService,
        RoleTransformer $transformer,
        FractalManager $fractal,
        AuthnService $authnService,
        UserRequestService $userRequestService,
        UserRoleService $userRoleService,
        RoleValidator $roleValidator,
        PhilippineCompanyRequestService $companyRequestService
    ) {
        $this->roleService = $roleService;
        $this->taskService = $taskService;
        $this->permissionService = $permissionService;
        $this->accountRequestService = $accountRequestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
        $this->authnService = $authnService;
        $this->userRequestService = $userRequestService;
        $this->userRoleService = $userRoleService;
        $this->roleValidator = $roleValidator;
        $this->companyRequestService = $companyRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/account/roles",
     *     summary="Get List of Account Roles",
     *     description="Get List of Account Roles for given account id

Authorization Scope : **view.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="exclude_system_defined",
     *         in="query",
     *         description="Exclude system defined",
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAccountRoles(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        // authorize
        $user = $request->attributes->get('user');
        $role = (object) [
            'account_id' => $accountId,
            'company_id' => 0
        ];

        if (!$this->authorizationService->authorizeGetAccountRoles($role, $user)) {
            $this->response()->errorUnauthorized();
        }

        $excludeSystemDefined = array_get($request->all(), 'exclude_system_defined', false);

        $this->validate($request, [
            'exclude_system_defined' => 'in:true,false,1,0'
        ]);

        // fetch account roles
        $roles = $this->roleService->getRoles(
            $accountId,
            filter_var($excludeSystemDefined, FILTER_VALIDATE_BOOLEAN)
        );
        $collection = new Collection($roles, $this->transformer);
        $result = $this->fractal->createData($collection)->toArray();

        // add powerless role for local / dev testing of permissions
        if (getenv('APP_ENV') === 'local' || getenv('APP_ENV') === 'dev') {
            $result['data'][] = [
                'id' => 0,
                'account_id' => $accountId,
                'name' => 'Permissionless',
            ];
        }
        return $result;
    }

    /**
     * @SWG\Get(
     *     path="/account/roles/all",
     *     summary="Get List of All Account Roles",
     *     description="Get List of All Roles for Account

Authorization Scope : **view.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "account_id": 1,
     *                          "name": "Owner",
     *                          "users": {
     *                              "Robert Vaughn",
     *                              "Jessie Park",
     *                              "Van Miles",
     *                              "Randall Hawkins"
     *                          },
     *                          "custom_role": false,
     *                          "users_count": 4
     *                      },
     *                      {
     *                          "id": 2,
     *                          "account_id": 1,
     *                          "name": "Super Admin",
     *                          "users": {
     *                              "Carolyn Howard",
     *                              "Dianne Walsh",
     *                              "Kristi Gonzalez",
     *                              "Kim Santiago"
     *                          },
     *                          "custom_role": false,
     *                          "users_count": 4
     *                      },
     *                      {
     *                          "id": 3,
     *                          "account_id": 1,
     *                          "name": "Company ABC Admin",
     *                          "users": {
     *                              "Gabriel Barnes"
     *                          },
     *                          "custom_role": false,
     *                          "users_count": 1
     *                      },
     *                      {
     *                          "id": 4,
     *                          "account_id": 1,
     *                          "name": "Company ABC Employee",
     *                          "users": {
     *                              "Harry Hines"
     *                          },
     *                          "custom_role": false,
     *                          "users_count": 1
     *                      },
     *                      {
     *                          "id": 5,
     *                          "account_id": 1,
     *                          "name": "CustomRole A",
     *                          "users": {
     *                              "Julia Briggs",
     *                              "Charlotte Norman"
     *                          },
     *                          "custom_role": true,
     *                          "users_count": 2
     *                      },
     *                      {
     *                          "id": 6,
     *                          "account_id": 1,
     *                          "name": "CustomRole B",
     *                          "users": {
     *                              "Mamie Mills",
     *                              "Lynn Barton"
     *                          },
     *                          "custom_role": true,
     *                          "users_count": 2
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAllAccountRoles(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        // authorize
        $user = $request->attributes->get('user');
        $role = (object) [
            'account_id' => $accountId,
            'company_id' => 0
        ];
        if (!$this->authorizationService->authorizeGetAccountRoles($role, $user)) {
            $this->response()->errorUnauthorized();
        }

        // fetch account roles
        $roles = $this->roleService->getAllAccountRoles($accountId);

        // fetch account companies
        $companies = $this->accountRequestService->getAccountCompanies($accountId);
        $companiesData = json_decode($companies->getData(), true);
        $accountCompanies = collect($companiesData['data'])->mapWithKeys(function ($company) {
            return [
                $company['id'] => [
                    'company_id' => $company['id'],
                    'company_name' => $company['name']
                ]
            ];
        })->toArray();

        $roles->transform(function ($role) use ($accountId, $accountCompanies) {
            $userRoles = $this->userRoleService->getWithConditions(['role_id' => $role['id']]);
            if ($userRoles->isNotEmpty()) {
                $formData['values'] = $userRoles->pluck('user_id')->implode(',');
                $usersResponse = $this->userRequestService->getAccountUsersByAttribute(
                    $accountId,
                    'id',
                    $formData
                );
                $userData = json_decode($usersResponse->getData(), true);
                $userNames = collect($userData)->pluck('first_name')->toArray();

                $role['users'] = $userNames;
            }

            $transformedRole = $this->transformer->transform($role);

            // if the role is system defined, return all companies for an account, or just company which belongs role
            $transformedRole['companies'] =
                $role['company_id'] === 0 ?
                    array_values($accountCompanies) : ([$accountCompanies[$role['company_id']] ?? []]);

            return $transformedRole;
        });

        return ['data' => $roles];
    }

    /**
     * @SWG\Post(
     *     path="/account/role",
     *     summary="Create role",
     *     description="Create role

Authorization Scope : **create.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Role details",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "name"={"type"="string"},
     *                  "company_id"={"type"="integer"},
     *                  "selected_modules"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "tasks"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="integer"
     *                      }
     *                  },
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $accountId = $createdBy['account_id'];

        $role = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeCreate($role, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $inputs['account_id'] = $accountId;
        $inputs['module_access'] = $inputs['selected_modules'];

        $errors = $this->roleValidator->validate($inputs);

        if ($errors) {
            return $this
                ->response
                ->array($errors)
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        $role = $this->roleService->createCustomRole($inputs);

        // audit log
        $details = [
            'new' => $role->toArray()
        ];

        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );

        $this->auditService->queue($item);

        return [
            'id' => $role->id
        ];
    }

    /**
     * @SWG\Patch(
     *     path="/account/role/{id}",
     *     summary="Update Role",
     *     description="Update Role
Authorization Scope : **edit.roles**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Role details",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "company_id"={"type"="integer"},
     *                  "selected_modules"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "tasks"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="integer"
     *                      }
     *                  },
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(int $id, Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();

        if (empty($id)) {
            $this->invalidRequestError('Role ID should not be empty.');
        }

        if (!is_numeric($id)) {
            $this->invalidRequestError('Role should be numeric.');
        }

        // authorize
        $updatedBy = $request->attributes->get('user');
        $oldRoleData = $this->roleService->get($id);

        if ($oldRoleData->isDefault()) {
            $this->invalidRequestError('Cannot update a Default Role.');
        }

        if (empty($oldRoleData)) {
            $this->notFoundError('Role not found.');
        }

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $accountId = $updatedBy['account_id'];

        $inputs = $request->all();
        $role = (object) [
            'account_id' => $oldRoleData->account_id,
            'company_id' => $oldRoleData->company_id
        ];

        if (!$this->authorizationService->authorizeUpdate($role, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        $inputs['account_id'] = $accountId;
        $inputs['id'] = $id;
        $inputs['module_access'] = $inputs['selected_modules'];

        $this->roleValidator->setForUpdating();
        $errors = $this->roleValidator->validate($inputs);

        if ($errors) {
            return $this
                ->response
                ->array($errors)
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        $updatedRoleData = $this->roleService->update($id, $inputs);

        // audit log
        $details = [
            'old' => $oldRoleData->toArray(),
            'new' => $updatedRoleData->toArray()
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updatedRoleData;
    }

    /**
     * @SWG\Delete(
     *     path="/account/roles/bulk_delete",
     *     summary="Delete multiple Roles",
     *     description="Delete multiple Roles
     Authorization Scope : **delete.role**",
     *     tags={"role"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="role_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Role Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $deletedBy = $request->attributes->get('user');
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        $this->validate(
            $request,
            $this->roleService->getCommonValidationRulesForRoleIds($accountId),
            RoleService::COMMON_VALIDATION_MESSAGES,
            RoleService::COMMON_VALIDATION_ATTRIBUTES
        );

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        // authorize
        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];
        if (!$this->authorizationService->authorizeDeleteAccountRoles($data, $deletedBy)) {
            $this->response()->errorUnauthorized();
        }

        // check are roles in use
        $ids = $request->get('role_ids');
        $inUse = $this->roleService->checkInUse($accountId, $ids, $this->authnService);
        if ($inUse > 0) {
            $this->invalidRequestError(
                'Selected records are currently in use and cannot be deleted.'
            );
        }

        // delete account roles
        $this->roleService->bulkDelete($accountId, $ids);

        return $this->response->noContent();
    }

    /**
     * @SWG\Get(
     *     path="/account/role/{id}",
     *     summary="Get Role",
     *     description="Get Role details and permissions

Authorization Scope : **view.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                          "id": 1,
     *                          "account_id": 1,
     *                          "name": "Owner",
     *                          "custom_role": false,
     *                          "module": {
     *                              {
     *                                  "name": "HRIS",
     *                                  "ess": false,
     *                                  "submodules": {
     *                                      {
     *                                          "name": "Location",
     *                                          "tasks": {
     *                                              {
     *                                                  "id": 564,
     *                                                  "name": "View Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 565,
     *                                                  "name": "Create Location",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 566,
     *                                                  "name": "Edit Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 567,
     *                                                  "name": "Delete Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "name": "Cost Center",
     *                                          "tasks": {
     *                                              {
     *                                                  "id": 574,
     *                                                  "name": "View Cost Centers",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 575,
     *                                                  "name": "Create Cost Centers",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                              },
     *                              {
     *                                  "name": "Payroll",
     *                                  "ess": false,
     *                                  "submodules": {
     *                                      "name": "Payroll",
     *                                      "tasks": {
     *                                          {
     *                                              "id": 584,
     *                                              "name": "Close Payroll",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 585,
     *                                              "name": "Create Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 586,
     *                                              "name": "Delete Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 587,
     *                                              "name": "Edit Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 588,
     *                                              "name": "Run Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 589,
     *                                              "name": "Send Payslips",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 589,
     *                                              "name": "View Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                              }
     *                          }
     *                      }
     *                  }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getRole($id, Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        $role = $this->roleService->get($id);

        $data = (object) [
            'company_id' => $role->company_id,
            'account_id' => $role->account_id
        ];

        // authorize
        $user = $request->attributes->get('user');

        if (!$this->authorizationService->authorizeGetAccountRoles($data, $user)) {
            $this->response()->errorUnauthorized();
        }
        $roleResponse = $role = $this->roleService->get($id)->toArray();
        $roleResponse['permissions'] = $this->permissionService->getRolePermissions($id);
        return response()->json($roleResponse);
    }

    /**
     * @SWG\Get(
     *     path="/account/roles/tasks",
     *     summary="Get List of Tasks available for Roles",
     *     description="Get List of Tasks available for Roles, organized by Module and Submodule
Authorization Scope : **create.role** | **edit.role**
           ",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="array",
     *         name="module_accesses",
     *         in="query",
     *         required=true,
     *         items="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                          "module": {
     *                              {
     *                                  "name": "HRIS",
     *                                  "ess": false,
     *                                  "submodules": {
     *                                      {
     *                                          "name": "Location",
     *                                          "tasks": {
     *                                              {
     *                                                  "id": 564,
     *                                                  "name": "View Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 565,
     *                                                  "name": "Create Location",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 566,
     *                                                  "name": "Edit Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 567,
     *                                                  "name": "Delete Locations",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "name": "Cost Center",
     *                                          "tasks": {
     *                                              {
     *                                                  "id": 574,
     *                                                  "name": "View Cost Centers",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "id": 575,
     *                                                  "name": "Create Cost Centers",
     *                                                  "scopes": {
     *                                                      "Account",
     *                                                      "Company"
     *                                                  }
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                              },
     *                              {
     *                                  "name": "Payroll",
     *                                  "ess": false,
     *                                  "submodules": {
     *                                      "name": "Payroll",
     *                                      "tasks": {
     *                                          {
     *                                              "id": 584,
     *                                              "name": "Close Payroll",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 585,
     *                                              "name": "Create Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 586,
     *                                              "name": "Delete Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 587,
     *                                              "name": "Edit Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 588,
     *                                              "name": "Run Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 589,
     *                                              "name": "Send Payslips",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": 589,
     *                                              "name": "View Payrolls",
     *                                              "scopes": {
     *                                                  "Account",
     *                                                  "Company",
     *                                                  "PayrollGroup"
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                              }
     *                          }
     *                      }
     *                  }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getTasks(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        $moduleAccesses = explode(',', $request->get('module_accesses'));

        // fetch account roles
        $tasks = $this->taskService->getTasksArrangedByModules($moduleAccesses);
        return response()->json($tasks);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/roles",
     *     summary="Get Company Roles",
     *     description="Get List of Company roles
    Authorization Scope : **view.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         description="Company ID",
     *         in="path",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyRoles(Request $request, $companyId)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        if (!is_numeric($companyId)) {
            $this->invalidRequestError('Company ID should be numeric.');
        }

        // authorize
        $data = (object) [
            'account_id' => Company::getAccountId($companyId),
            'company_id' => $companyId,
        ];
        $user = $request->attributes->get('user');
        if (!$this->authorizationService->authorizeGetAccountRoles($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        // fetch company roles
        $roles = $this->roleService->getCompanyRoles((int) $accountId, (int) $companyId);
        $collection = new Collection($roles, $this->transformer);

        return $this
            ->response
            ->array($this->fractal->createData($collection)->toArray())
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @SWG\Get(
     *     path="/account/role/{id}/assigned_users",
     *     summary="Get Assigned Users",
     *     description="Get List of Assigned users",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         description="Role ID",
     *         in="path",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAssignedUsers(Request $request, $id)
    {
        $viewer = $request->attributes->get('user');

        $roleResponse = $this->roleService->get($id);

        $role = (object) [
            'account_id' => $roleResponse->account_id,
            'company_id' => $roleResponse->company_id
        ];

        if (!$this->authorizationService->authorizeGetAccountRoles($role, $viewer)) {
            $this->response()->errorUnauthorized();
        }

        $userIds = $this->userRoleService->getWithConditions(['role_id' => $id])->pluck('user_id');

        if (!$userIds->isEmpty()) {
            $formData['values'] = $userIds->implode(',');
            $usersResponse = $this->userRequestService->getAccountUsersByAttribute(
                $viewer['account_id'],
                'id',
                $formData
            );

            $usersData = json_decode($usersResponse->getData(), true);

            $authnUsers = $this->authnService
                ->getUsersByIds($userIds->toArray())
                ->mapWithKeys(function ($user) {
                    return [$user['user_id'] => $user['status']];
                })->toArray();

            return collect($usersData)->map(function ($user) use ($authnUsers) {
                $user['status'] = $authnUsers[$user['id']];

                return $user;
            })->toArray();
        }

        return $userIds;
    }

    /**
     * @SWG\Post(
     *     path="/account/role/is_name_available",
     *     summary="Is name available",
     *     description="Check availability of role name on account or company level, with the given account id,
     *     and company id",
     *     tags={"role"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Role name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function isNameAvailable(Request $request)
    {
        $user = $request->attributes->get('user');
        $companyId = $request->get('company_id');

        $authData = (object)[
            'account_id' => $user['account_id'],
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeIsNameAvailable($authData, $user)) {
            $this->response()->errorUnauthorized();
        }

        $inputs = $request->all();
        $inputs['account_id'] = $user['account_id'];

        return [
            'available' => !$this->roleService->isNameAvailable($inputs)
        ];
    }
}
