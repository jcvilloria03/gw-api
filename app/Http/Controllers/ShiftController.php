<?php

namespace App\Http\Controllers;

use App\AffectedEntity\AffectedEntityRequestService;
use App\Employee\EmployeeAuthz;
use App\Facades\Company;
use App\Shift\ShiftAuthorizationService;
use Illuminate\Http\Request;
use App\Shift\ShiftRequestService;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use App\Schedule\ScheduleRequestService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Shift\ShiftAuditService;
use App\Employee\EmployeeRequestService;
use App\CSV\CsvValidatorException;
use Aws\S3\Exception\S3Exception;
use App\CSV\CsvValidator;
use App\Shift\ShiftUploadTask;
use App\Shift\AssignShiftsTask;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Arr;
use Validator;
use Illuminate\Support\Facades\Redis;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class ShiftController extends Controller
{
    use AuditTrailTrait;
    /**
     * App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\Shift\ShiftRequestService
     */
    private $requestService;

    /**
     * @var \App\Shift\ShiftAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        ShiftRequestService $requestService,
        ShiftAuthorizationService $authorizationService,
        AuditService $auditService,
        CsvValidator $csvValidator
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->csvValidator = $csvValidator;
    }

    /**
     * @SWG\Get(
     *     path="/shift/{id}",
     *     summary="Get Shift",
     *     description="Get Shift Details
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Shift ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $shiftData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $shiftData->company_id
            );
        } else {
            $shiftData->account_id = Company::getAccountId($shiftData->company_id);
            $isAuthorized = $this->authorizationService->authorizeGet(
                $shiftData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/shifts",
     *     summary="Get all Shifts",
     *     description="Get all Shifts within company.
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyShifts(Request $request, $id)
    {

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
            $response = $this->requestService->getCompanyShifts($id, $authzDataScope, $request->all());
        } else {
            $response = $this->requestService->getCompanyShifts($id, null, $request->all());
            $shiftData = json_decode($response->getData())->data;
            if (empty($shiftData)) {
                return $response;
            }
            $shift = (object)[
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyShifts(
                $shift,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employee/{employeeId}/shifts",
     *     summary="Get employee shifts within date range",
     *     description="Get employee shifts within date range.
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         description="A inclusive lower date boundary for filtering in format YYYY-MM-DD",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         description="A inclusive upper date boundary for filtering in format YYYY-MM-DD",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeShifts(
        Request $request,
        $id,
        $employeeId,
        ShiftRequestService $shiftRequestService,
        EmployeeRequestService $employeeRequestService
    ) {

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($employeeId);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [$id, Arr::get($employee, 'company_id')],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $authData = (object)[
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyShifts(
                $authData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $shiftRequestService->getEmployeeShifts(
            $employeeId,
            array_merge(['company_id' => $id, "include_default" => true], $request->only(['start_date', 'end_date']))
        );
    }

    /**
     * @SWG\Post(
     *     path="/shift/",
     *     summary="Create company shift",
     *     description="Create company shift
    Authorization Scope : **create.shift**",
     *     tags={"shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Employee ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="schedule_id",
     *         in="formData",
     *         description="Schedule Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="Effective Start Date in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="Effective End Date in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(
        Request $request,
        ScheduleRequestService $scheduleRequestService,
        EmployeeRequestService $employeeRequestService
    ) {
        $scheduleId = $request->input('schedule_id');
        if (empty($scheduleId)) {
            $this->invalidRequestError('Schedule ID should not be empty.');
        }
        if (empty($request->input('employee_id'))) {
            $this->invalidRequestError('Employee ID should not be empty.');
        }
        $scheduleData = $scheduleRequestService->get($scheduleId);

        $schedule = json_decode($scheduleData->getData(), true);

        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $schedule['company_id'];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($request->input('employee_id'));
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [$companyId, Arr::get($employee, 'company_id')],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $shiftData = (object)[
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($shiftData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $shiftGetResponse = $this->requestService->get($responseData['id']);
        $shiftData = json_decode($shiftGetResponse->getData(), true);

        $details = [
            'new' => $shiftData
        ];

        $item = new AuditCacheItem(
            ShiftAuditService::class,
            ShiftAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);
        $this->audit($request, $companyId, $shiftData, []);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/shift/bulk_create",
     *     summary="Create company shifts",
     *     description="Assign shifts to multiple employees
    Authorization Scope : **create.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newShift"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="newShift",
     *     required={"schedule_id", "start", "affected_employees"},
     *     @SWG\Property(property="schedule_id", type="integer"),
     *     @SWG\Property(
     *         property="start",
     *         type="string",
     *         default="2017-10-01",
     *         description="Effective Start Date in format YYYY-MM-DD",
     *     ),
     *     @SWG\Property(
     *         property="end",
     *         type="string",
     *         default="2017-10-29",
     *         description="Effective End Date in format YYYY-MM-DD",
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/shifts_affected_employees"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="shifts_affected_employees",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Relation type. Possible values: department, position, location, employee"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string"
     *     )
     * )
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function bulkCreate(
        Request $request,
        ScheduleRequestService $scheduleRequestService,
        AffectedEntityRequestService $entityService,
        EmployeeAuthz $employeeAuthz
    ) {
        $scheduleId = $request->input('schedule_id');
        if (empty($scheduleId)) {
            $this->invalidRequestError('Schedule ID should not be empty.');
        }
        $endDate = $request->input('end');
        if (empty($endDate) || $endDate === 'Invalid date' ) {
            $this->invalidRequestError('End Date should not be empty.');
        }
        $startDate = $request->input('start');
        if (empty($startDate) || $startDate === 'Invalid date') {
            $this->invalidRequestError('Start Date should not be empty.');
        }

        $scheduleData = $scheduleRequestService->get($scheduleId);
        $schedule = json_decode($scheduleData->getData(), true);
        $createdBy = $request->attributes->get('user');
        $companyId = $schedule['company_id'];
        $affectedEmployeeIds = [];
        $totalAffectedEmployees = 0;
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);

            $entityRequest = $entityService->getAllActiveAffectedEmployees(
                $companyId, $request->get('affected_employees', [])
            );

            if ($entityRequest->isSuccessful()) {
                $entityData = collect(json_decode($entityRequest->getData(), true));

                if ($entityData->isEmpty()) {
                    abort(409, 'No employees found.');
                }

                $affectedEmployeeIds = $entityData
                    ->pluck('id')
                    ->flatten(1)
                    ->reject(function ($id) {
                        return empty($id);
                    })->all();

                $data = empty(json_decode($entityRequest->getData(), true)) ? [] :
                    json_decode($entityRequest->getData(), true);
                $authorized = $employeeAuthz
                    ->isAllAuthorizedByIdsAndCompanyId($companyId, $data, $authzDataScope);
            } else {
                return $entityRequest;
            }
        } else {
            $shiftData = (object)[
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeCreate($shiftData, $createdBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $attributes = $request->all();
        $attributes['affected_employee_ids'] = $affectedEmployeeIds;
        $totalAffectedEmployees = count($affectedEmployeeIds);

        // Create status cache Check job exists (will throw exception if job doesn't exist)
        try {
            $uploadTask = App::make(AssignShiftsTask::class);
            $uploadTask->create($companyId, $totalAffectedEmployees);
            $jobId = $uploadTask->getId();


            // Drop to queue
            $details =             [
                'id'        => $jobId,
                'task'      => 'save',
                'authz_data_scope' => $this->getAuthzDataScope($request)->getDataScope(),
                'request'   => array_merge(['id' => $companyId], $attributes)
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.assign_shifts_write_queue'), $message);

            try {
                $this->audit($request, $companyId, ['Batch Assign Shifts create.']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);
            return response()->json([
                'id' => $jobId
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    /**
     * @SWG\Put(
     *     path="/shift/{id}",
     *     summary="Update Shift",
     *     description="Update Shift for given attributes
    Authorization Scope : **edit.shift**",
     *     tags={"shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Shift Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="Effective Start Date in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="Effective End Date in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, EmployeeRequestService $employeeRequestService, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);

        $oldShiftData = json_decode($response->getData());
        $oldShiftData->account_id = Company::getAccountId($oldShiftData->company_id);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($oldShiftData->employee_id)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldShiftData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $oldShiftData = json_decode($response->getData(), true);
        $newShiftData = json_decode($updateResponse->getData(), true);
        $companyId    = $oldShiftData['employee']['company_id'];

        try {
            $oldShiftData['employee_name'] = $oldShiftData['employee']['full_name'];
            $oldShiftData['employee_id'] = $oldShiftData['employee']['employee_id'];
            $oldShiftData['schedule'] = $oldShiftData['schedule']['name'];

            $newShiftData['employee_name'] = $newShiftData['employee']['full_name'];
            $newShiftData['employee_id'] = $newShiftData['employee']['employee_id'];
            $newShiftData['schedule'] = $inputs['schedule']['name'];

            unset($newShiftData['employee']);
            unset($newShiftData['schedule_id']);
            unset($oldShiftData['employee']);
            unset($oldShiftData['schedule_id']);

            $this->audit($request, $companyId, $newShiftData, $oldShiftData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);

        return $updateResponse;
    }

    /**
     * @SWG\Put(
     *     path="/shift/unassign/{id}",
     *     summary="Unassign Shift",
     *     description="Unassign Shift for given attributes
    Authorization Scope : **edit.shift**",
     *     tags={"shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Shift Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="A inclusive lower date boundary for unassigning in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="A inclusive upper date boundary for unassigning in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function unassign(Request $request, EmployeeAuthz $employeeAuthz, $id)
    {
        // authorize
        $inputs = $request->all();
        $unassignedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);

        $oldShiftData = json_decode($response->getData());
        $companyId = isset($oldShiftData->company_id) ?
            $oldShiftData->company_id : $oldShiftData->employee->company_id;

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $employeeAuthz->isAuthorized(
                (array) $oldShiftData->employee,
                $this->getAuthzDataScope($request)
            );
        } else {
            $oldShiftData->account_id = Company::getAccountId($companyId);
            $authorized = $this->authorizationService->authorizeUpdate($oldShiftData, $unassignedBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $unassignResponse = $this->requestService->unassign($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($unassignResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $unassignResponse;
        }

        $oldShiftData = json_decode($response->getData(), true);
        $newShiftData = json_decode($unassignResponse->getData(), true);

        try {
            $oldShiftData['employee_name'] = $oldShiftData['employee']['full_name'];
            $oldShiftData['employee_id'] = $oldShiftData['employee']['employee_id'];
            $oldShiftData['schedule'] = $oldShiftData['schedule']['name'];

            $newShiftData['employee_name'] = $newShiftData['employee']['full_name'];
            $newShiftData['employee_id'] = $newShiftData['employee']['employee_id'];
            $newShiftData['schedule'] = $newShiftData['schedule']['name'];

            unset($newShiftData['employee']);
            unset($newShiftData['schedule_id']);
            unset($oldShiftData['employee']);
            unset($oldShiftData['schedule_id']);

            $this->audit($request, $companyId, $newShiftData, $oldShiftData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);

        return $newShiftData;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/shifts/generate_csv",
     *     summary="Generate CSV with shifts",
     *     description="Generate CSV with shifts within company for given Shift Ids and additional filters.
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="shifts_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Shifts Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="location_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Location Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="department_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Department Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="position_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Position Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="schedule_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Schedule Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="date_from",
     *         in="formData",
     *         description="Effective Start Date From (YYYY-MM-DD)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_to",
     *         in="formData",
     *         description="Effective Start Date To (YYYY-MM-DD)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully generated CSV": {
     *                  "file_name": "shifts.csv"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Company ID does not exist": {
     *                  "message": "Company not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Date Format": {
     *                  "message": "The date to is not a valid date.",
     *                  "status_code": 400
     *              },
     *              "Non-numeric IDs": {
     *                  "message": "The location id's must be an integer.",
     *                  "status_code": 400
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function generateCsv(Request $request, $id)
    {
        $shift = (object)[
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanyShifts(
                $shift,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->generateCsv($id, $request->all(), $dataScope);

        try {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $id, [], $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/shifts/download/{fileName}",
     *     summary="Download Shift CSV",
     *     description="Download generated CSV with shifts
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="fileName",
     *         in="path",
     *         description="File name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully downloaded CSV": {
     *                  "body": "blob:http://ta-api/28b81a6e-5856-4b93-966d-9b181cec61aa"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "File not found": {
     *                  "message": "File not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Company ID": {
     *                  "message": "The id must be an integer.",
     *                  "status_code": 400
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function downloadCsv($companyId, $fileName, Request $request)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $shift = (object)[
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyShifts(
                $shift,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->downloadCsv($companyId, $fileName);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/shifts_data",
     *     summary="Get Shifts page data",
     *     description="Get Employees, Shifts and Rest Days within company.
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getShiftsData(Request $request, $id)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $authzDataScope = $request->attributes->get('authz_data_scope');
        $inputs = $request->query();
        $response = $this->requestService->getShiftsData($id, $authzDataScope, $inputs);

        $shiftData = json_decode($response->getData())->data;
        if (empty($shiftData)) {
            return $response;
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $shift = (object)[
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyShifts(
                $shift,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/shift/overlapping_requests",
     *     summary="Get Overlaping requests with Shifts",
     *     description="Get Overlaping requests with Shifts.
    Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="shift_ids[]",
     *         in="formData",
     *         description="Shift IDs",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_ids[]",
     *         in="formData",
     *         description="Employee IDs",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="Shift Start Date in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="Shift End Date in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getShiftsOverlappingRequests(
        Request $request,
        EmployeeRequestService $employeeService,
        EmployeeAuthz $employeeAuthz
    ) {
        $employeeIds = $request->get('employee_ids', []);
        if (
            $this->isAuthzEnabled($request)
            && !$employeeAuthz->isAllAuthorizedByIds($employeeIds, $this->getAuthzDataScope($request))
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->getShiftsOverlappingRequests($request->all());
        $shiftData = json_decode($response->getData());
        if (empty($shiftData) || empty($shiftData->data)) {
            return $response;
        }
        if (!$this->isAuthzEnabled($request)) {
            $employeeResponse = $employeeService->getEmployee($shiftData->data[0]->employee_id);
            $employee = json_decode($employeeResponse->getData());
            $shift = (object)[
                'account_id' => Company::getAccountId($employee->company_id),
                'company_id' => $employee->company_id
            ];
            if (!$this->authorizationService->authorizeGet($shift, $request->attributes->get('user'))) {
                $this->response()->errorUnauthorized();
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{companyId}/batch_assign_shifts/validate",
     *     summary="Validate batch assign shifts CSV.",
     *     description="This validates the uploaded CSV. The csv must pass the batch assign shift template requirement.
    Authorization Scope : **create.shift**",
     *     tags={"batch assign shift"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="The company id of the employees to assign shifts",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function batchAssignShiftsValidate($companyId, Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $authzDataScope = $this->getAuthzDataScope($request);
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $shiftData = (object)[
                'account_id' => $companyId ? Company::getAccountId((int) $companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeCreate($shiftData, $createdBy);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(ShiftUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveShiftInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => ShiftUploadTask::PROCESS_SHIFT,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $this->isAuthzEnabled($request) ? $authzDataScope->getDataScope() : null
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_batch_assign_shifts_queue'), $message);

            try {
                $this->audit($request, $companyId, ['Batch Assign Shifts Validate.']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/company/{companyId}/batch_assign_shifts/save",
     *     summary="Assign Shifts to multiple employees",
     *     description="This endpoint assigns shifts to multiple employees.
    Authorization Scope : **create.shift**",
     *     tags={"batch assign shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="The company id of the employees to assign shifts",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="The job id to get the draft items and execute those for saving.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function batchAssignShiftsSave($companyId, Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $shiftData = (object)[
                'account_id' => $companyId ? Company::getAccountId((int) $companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeCreate($shiftData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $jobId = $request->input('job_id');

        $validator = Validator::make($request->all(), [
            "job_id"        => "required"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $uploadTask = App::make(ShiftUploadTask::class);
        $uploadTask->setTasksPool($jobId);
        $tasks = $uploadTask->fetch();

        if (empty($tasks)) {
            return response()->json([
                "NotFoundError" => "Job ID not found or job id does not exist."
            ], 404);
        }

        $progress = array_keys($tasks);
        $lastStatus = end($progress);
        $statusValue = $tasks[$lastStatus];

        if ($lastStatus == ShiftUploadTask::VALIDATED_STATUS_KEY) {
            if ($statusValue != ShiftUploadTask::STATUS_VALIDATED) {
                return response()->json([
                    "NotValidated" => "The current job is not validated or there are unresolved "
                        . "errors. Please try running through validation again."
                ], 406);
            }
        } else {
            return response()->json([
                "SaveError" => "This task is most likely queued for saving or is already finished saving."
            ], 406);
        }

        $uploadTask->updateSaveStatus(ShiftUploadTask::STATUS_SAVE_QUEUED);

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            'task' => ShiftUploadTask::PROCESS_SAVE,
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.employee_batch_assign_shifts_queue'), $message);

        try {
            $this->audit($request, $companyId, ['Batch Assign Shifts Save.' => $uploadTask->getId()]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/batch_assign_shifts/status",
     *     summary="Gets the status of a current batch assign shift job",
     *     description="This endpoint gets the status of a current batch assign shift job.
    Authorization Scope : **create.shift**",
     *     tags={"batch assign shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="The company id of the employees to assign shifts",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="The job id to get the draft items and execute those for saving.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getBatchAssignShiftsStatus($companyId, Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $shiftData = (object)[
                'account_id' => $companyId ? Company::getAccountId((int) $companyId) : null,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($shiftData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $jobId = $request->input('job_id');
        Validator::make($request->all(), [
            "job_id"        => "required"
        ]);

        $uploadTask = App::make(ShiftUploadTask::class);
        $uploadTask->setTasksPool($jobId);
        $tasks = $uploadTask->fetch();
        $errors = $uploadTask->getErrors();

        if (empty($tasks)) {
            return response()->json([
                "NotFoundError" => "Job ID not found or job id does not exist."
            ], 404);
        }

        $progress = array_keys($tasks);
        $lastStatus = end($progress);
        $statusValue = $tasks[$lastStatus];

        if ($statusValue == 'saved') {
            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);
        }

        return response()->json([
            'status' => $statusValue,
            'errors' => $errors
        ]);
    }

    /**
     * @SWG\POST(
     *     path="/{id}/assign_shifts/status",
     *     summary="Get Assign Shifts Status",
     *     description="Get Assign Shifts Status
Authorization Scope : **view.shift**",
     *     tags={"shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="x-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function getAssignShiftsStatus(Request $request, $id)
    {
        $attributes = $request->only(['job_id']);
        $jobId = Arr::get($attributes, 'job_id', null);
        $downloadTask = App::make(AssignShiftsTask::class);
        $cache = $downloadTask->getRedisKey($id, $jobId);
        return $cache;
    }
}
