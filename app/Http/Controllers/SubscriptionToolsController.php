<?php

namespace App\Http\Controllers;

use App\Model\Role;
use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\Http\Controllers\Controller;
use App\Subscriptions\SubscriptionsRequestService;

class SubscriptionToolsController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/subscriptions/tools/customers/{owner_user_id}",
     *     summary="Get customer details by providing the owner user id",
     *     description="Get customer details",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="owner_user_id",
     *         in="path",
     *         description="Owner user ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCustomerDetails(
        UserRequestService $userRequestService,
        SubscriptionsRequestService $subscriptionsRequestService,
        $ownerUserId
    ) {
        $userData = $userRequestService->get($ownerUserId);
        $userData = json_decode($userData->getData(), true);

        if (empty($userData['user_type']) || stripos($userData['user_type'], Role::OWNER_NAME) !== 0) {
            $this->response()->errorNotFound('Customer user record not found.');
        }

        $customerData = $subscriptionsRequestService->searchCustomerDetailsByUserId($ownerUserId);
        $customerData = json_decode($customerData->getData(), true);
        $customerData = $customerData['data'][0] ?? null;

        if (empty($customerData) || empty($customerData['subscriptions'][0])) {
            $this->response()->errorNotFound('Customer subscription data not found.');
        }

        $subscriptionData    = current($customerData['subscriptions']);
        $subscriptionLicenses = $subscriptionData['subscription_licenses'];

        // TODO: Update once PR is done with salpay integration dev
        $payrollRequestData = [];

        $response = $subscriptionsRequestService->getCustomerSubscriptionDiscounts($subscriptionData['id']);
        $statsResponse = $subscriptionsRequestService->getCustomerSubscriptionStats(
            $subscriptionData['id']
        );

        $statsData = json_decode($statsResponse->getData(), true);

        $subscriptionDiscountsData = json_decode($response->getData(), true);

        $customer = [];

        $companiesList = collect(
            $userData['account']['companies']
            ?? $userData['companies']
            ?? []
        );

        $customer['account_id'] = $userData['account_id'] ?? null;
        $customer['companies']  = $companiesList->map(function ($company) {
            return [
                'id'   => $company['id'] ?? null,
                'name' => $company['name'] ?? null
            ];
        });

        $customer['salpay_integrated'] = $payrollRequestData['salpay_integration'] ?? null;

        $customer['customer'] = [
            'billing_information' => [
                'account_name' => $customerData['account_name'],
                'first_name'   => $customerData['first_name'] ?? null,
                'middle_name'  => $customerData['middle_name'] ?? null,
                'last_name'    => $customerData['last_name'] ?? null,
                'email'        => $customerData['email']
            ],
            'customer_credit'     => $customerData['credits'] ?? null
        ];

        $customer['subscription'] = [
            'start_date'            => $subscriptionData['start_date'] ?? null,
            'end_date'              => $subscriptionData['end_date'] ?? null,
            'next_billing_date'     => (
                empty($subscriptionData['is_trial'])
                                        && !empty($subscriptionData['next_billing_date'])
                                       )
                                        ? $subscriptionData['next_billing_date']
                                        : null,
            'is_trial'              => $subscriptionData['is_trial'],
            'subscription_licenses'  => $subscriptionLicenses,
            'days_left'             => (!empty($subscriptionData['is_trial']))
                                         ? $subscriptionData['days_left'] ?? null
                                         : null,
            'product_price'         => $subscriptionLicenses['price']
                                         ?? $subscriptionLicenses['product']['price']
                                         ?? null,
            'product_currency'      => $subscriptionLicenses['currency']
                                         ?? $subscriptionLicenses['product']['currency']
                                         ?? null,
            'is_expired'            => $subscriptionData['is_expired'] ?? null,
            'subscription_discounts' => $subscriptionDiscountsData['data'] ?? null,
            'subscription_stats'    => $statsData['data'] ?? []
        ];

        return response()->json($customer);
    }

    /**
     * @SWG\Patch(
     *     path="/subscriptions/tools/invoices/{invoice_id}",
     *     summary="Update an invoice status given an invoice id",
     *     description="Update an invoice status given an invoice id",
     *     tags={"Subscriptions Tools"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="invoice_id",
     *         in="path",
     *         description="Invoice id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         in="query",
     *         description="The invoice status to update",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateInvoice(
        Request $request,
        SubscriptionsRequestService $subscriptionsRequestService,
        $invoiceId
    ) {
        $inputs = $request->all();
        return $subscriptionsRequestService->updateInvoice(
            $invoiceId,
            $inputs["status"]
        );
    }
}
