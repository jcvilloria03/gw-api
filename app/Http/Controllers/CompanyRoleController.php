<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Role\RoleRequestService;
use Illuminate\Http\Request;

class CompanyRoleController extends Controller
{
    /**
     * @var \App\Role\RoleRequestService
     */
    protected $roleRequestService;

    /**
     * CompanyRolesController constructor
     *
     * @param RoleRequestService $roleRequestService
     *
     * @SWG\Definition(
     *     definition="CompanyRoleRequest",
     *     required={"name", "is_employee", "permissions"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         example="Role Name"
     *     ),
     *     @SWG\Property(
     *         property="is_employee",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="permissions",
     *         type="object",
     *         example={
     *             "control_panel.subscriptions": {
     *                 "scopes": {
     *                     "COMPANY": {"__ALL__"},
     *                     "DEPARTMENT": {1, 2, 3},
     *                 },
     *                 "actions": {"CREATE", "READ", "UPDATE", "DELETE"}
     *             }
     *         }
     *     )
     * )
     */
    public function __construct(RoleRequestService $roleRequestService)
    {
        $this->roleRequestService = $roleRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/companies/{companyId}/roles",
     *     summary="Get company roles",
     *     description="Get list of roles by company",
     *     tags={"roles"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="List of Roles",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     {
     *                         "id": 1,
     *                         "owner_account_id": 1,
     *                         "name": "Role Name",
     *                         "level": "ONE",
     *                         "is_employee": false,
     *                         "is_system_defined": true,
     *                         "company_ids": { 1 },
     *                         "policy": {
     *                             "subject": {
     *                                 "role_id": 1,
     *                                 "user_id": 1,
     *                                 "account_id": 1,
     *                                 "owner_account_id": 1
     *                             },
     *                             "action": "IN_RESOURCE",
     *                             "resource": {
     *                                 "module_name": {
     *                                     "scopes": {
     *                                         "COMPANY": { 1, 2, 3 }
     *                                     },
     *                                     "actions": { "CREATE", "READ", "UPDATE", "DELETE" }
     *                                 }
     *                             },
     *                             "environment": {}
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Company ID",
     *     )
     * )
     */
    public function index(Request $request, int $companyId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $this->validate($request, [
            'filter.role_ids' => 'nullable|array',
            'filter.role_ids.*' => 'required|integer|min:1',
            'page' => 'nullable|integer|min:1',
            'per_page' => 'nullable|integer|min:1',
        ]);

        return $this->roleRequestService->getRolesByCompany(
            $companyId,
            $request->only(['filter', 'page', 'per_page'])
        );
    }

    /**
     * @SWG\Get(
     *     path="/companies/{companyId}/roles/{roleId}",
     *     summary="Get company role",
     *     description="Get role by company",
     *     tags={"roles"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Company Role",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     "id": 1,
     *                     "owner_account_id": 1,
     *                     "name": "Role Name",
     *                     "level": "ONE",
     *                     "is_employee": false,
     *                     "is_system_defined": false,
     *                     "company_ids": { 1 },
     *                     "policy": {
     *                         "subject": {
     *                             "role_id": 1,
     *                             "user_id": 1,
     *                             "account_id": 1,
     *                             "owner_account_id": 1
     *                         },
     *                         "action": "IN_RESOURCE",
     *                         "resource": {
     *                             "module_name": {
     *                                 "scopes": {
     *                                     "COMPANY": { 1, 2, 3 }
     *                                 },
     *                                 "actions": { "CREATE", "READ", "UPDATE", "DELETE" }
     *                             }
     *                         },
     *                         "environment": {}
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Company ID",
     *     )
     * )
     */
    public function get(Request $request, int $companyId, int $roleId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->roleRequestService->getRoleByCompany($companyId, $roleId);
    }

    /**
     * @SWG\Post(
     *     path="/companies/{companyId}/roles",
     *     summary="Create company role",
     *     description="Create role by company",
     *     tags={"roles"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/CompanyRoleRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Company Role",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     "id": 1,
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Wrong or Missing Data Input (JSON or Param)",
     *     )
     * ),
     */
    public function create(Request $request, int $companyId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $data = $request->input();

        $result = $this->roleRequestService->createRoleByCompany($companyId, $data);

        return response()->json($result, 201);
    }

    /**
     * @SWG\Put(
     *     path="/companies/{companyId}/roles/{roleId}",
     *     summary="Update company role",
     *     description="Update role by company",
     *     tags={"roles"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/CompanyRoleRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful Response",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Company ID",
     *     )
     * ),
     */
    public function update(Request $request, int $companyId, int $roleId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $data = $request->input();

        return $this->roleRequestService->updateRoleByCompany($companyId, $roleId, $data);
    }

    /**
     * @SWG\Delete(
     *     path="/companies/{companyId}/roles/{roleId}",
     *     summary="Delete company role",
     *     description="Delete role by company",
     *     tags={"roles"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful Response",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     */
    public function delete(Request $request, int $companyId, int $roleId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $this->roleRequestService->deleteRoleByCompany($companyId, $roleId);

        return response(null, 204);
    }
}
