<?php

namespace App\Http\Controllers;

use App\AffectedEntity\AffectedEntityAuthorizationService;
use App\AffectedEntity\AffectedEntityRequestService;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AffectedEntityController extends Controller
{
    /**
     * @var \App\AffectedEntity\AffectedEntityRequestService
     */
    private $requestService;

    /**
     * @var \App\AffectedEntity\AffectedEntityAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        AffectedEntityRequestService $requestService,
        AffectedEntityAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/affected_entities/search",
     *     summary="Get affected entities by term",
     *     description="Get affected entities by term
Authorization Scope : [**view.department**, **view.position**, **view.team**, **view.employee**, **view.users]",
     *     tags={"affected_entities"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Search limit",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include_admin_users",
     *         in="query",
     *         description="If admin (non-employee) users should be included in results",
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function search(Request $request, $id)
    {
        $term = $request->input('term', '');
        $limit = $request->input('limit');
        $includeAdminUsers = filter_var($request->input('include_admin_users', false), FILTER_VALIDATE_BOOLEAN);
        $authzDataScope = $request->attributes->get('authz_data_scope');

        $authorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
                'id' => $id
            ];
            $authorized = $this->authorizationService->authorizeGet($data, $request->attributes->get('user'));
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->search($id, $term, $limit, $includeAdminUsers, $authzDataScope);

        return $response;
    }
}
