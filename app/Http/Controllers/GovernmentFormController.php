<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Authz\AuthzDataScope;
use App\User\UserRequestService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use App\Company\PhilippineCompanyRequestService;
use App\GovernmentForm\GovernmentFormAuditService;
use App\GovernmentForm\GovernmentFormRequestService;
use App\GovernmentForm\GovernmentFormAuthorizationService;
use App\Employee\EmployeeRequestService;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class GovernmentFormController extends Controller
{
    use AuditTrailTrait;

    /*
     * App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /*
     * App\GovernmentForm\GovernmentFormRequestService
     */
    protected $requestService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /*
     * App\GovernmentForm\GovernmentFormAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        PhilippineCompanyRequestService $companyRequestService,
        EmployeeRequestService $employeeRequestService,
        GovernmentFormRequestService $requestService,
        GovernmentFormAuthorizationService $authorizationService
    ) {
        $this->companyRequestService = $companyRequestService;
        $this->employeeRequestService = $employeeRequestService;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/government_forms",
     *     summary="Get generated Government Forms",
     *     description="Get list of generated Government Forms

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="filter[company_id]",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[kind]",
     *         in="query",
     *         description="Government form type/name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function index(Request $request)
    {
        return $this->requestService->getAll($request->all());
    }

    /**
     * @SWG\Get(
     *     path="/government_forms/{id}",
     *     summary="Get generated Government Form",
     *     description="Get a generated Government Form

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Government Form ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function show(Request $request, $id)
    {
        $govtFormResponse = $this->requestService->get($id);
        $govtFormData = json_decode($govtFormResponse->getData());

        $companyId = data_get($govtFormData, 'data.attributes.companyId')
            ?? data_get($govtFormData, 'data.attributes.company_id');

        if (
            !$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            )
        ) {
            $this->response->errorUnauthorized();
        }

        return $govtFormResponse;
    }

    /**
     * @SWG\Put(
     *     path="/government_forms/{id}",
     *     summary="Edit a generated Government Form",
     *     description="Edit a generated Government Form

Authorization Scope : **edit.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Government Form ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/govtFormEditItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="govtFormEditItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="company_id", type="integer"),
     *         @SWG\Property(property="kind", type="string"),
     *         @SWG\Property(property="attributes", type="object"),
     *     ),
     * )
     */
    public function update(Request $request, int $id)
    {
        $govtFormResponse = $this->requestService->get($id);
        $govtFormData = json_decode($govtFormResponse->getData(), true);

        $companyId = data_get($govtFormData, 'data.attributes.companyId')
            ?? data_get($govtFormData, 'data.attributes.company_id');

        if (
            $this->isAuthzEnabled($request) &&
            !$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            )
        ) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $request->all());

        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            if (empty($newData) || $newData === null) {
                $newData = [];
            }
            $this->audit($request, $companyId, $newData, $govtFormData);
        }

        return $response;
    }

     /**
     * @SWG\Post(
     *     path="/government_forms/{id}/generate",
     *     summary="Export Government Form file",
     *     description="Export Government Form File

Authorization Scope : **export.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Government Form ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generateForm(int $id)
    {
        return $this->requestService->generateForm($id);
    }

    /**
     * @SWG\Post(
     *     path="/government_forms",
     *     summary="Generate a Government Form",
     *     description="Generate a Government Form

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/generatedGovtFormItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="generatedGovtFormItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="kind", type="string"),
     *         @SWG\Property(property="company_id", type="integer"),
     *         @SWG\Property(property="month_from", type="string"),
     *         @SWG\Property(property="year_from", type="string"),
     *         @SWG\Property(property="month_to", type="string"),
     *         @SWG\Property(property="year_to", type="string"),
     *     ),
     * )
     */
    public function store(Request $request)
    {
        if (
            $this->isAuthzEnabled($request) &&
            !$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $request->json('data.company_id')
            )
        ) {
            $this->response->errorUnauthorized();
        }

        return $this->requestService->store($request->all());
    }

    /**
     * @SWG\Post(
     *     path="/employee/government_forms/bir_2316/bulk/generate",
     *     summary="Generate BIR 2316 for selected Employees",
     *     description="Generate BIR 2316 for selected Employees

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/generatedBir2316FormItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="generatedBir2316FormItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="type", type="string"),
     *         @SWG\Property(
     *             property="attributes",
     *             type="object",
     *             @SWG\Property(property="year", type="string"),
     *             @SWG\Property(property="company_id", type="integer"),
     *             @SWG\Property(
     *                 property="employees_id",
     *                 type="array",
     *                 @SWG\Items(type="integer"),
     *                 collectionFormat="multi"
     *             ),
     *         ),
     *     ),
     * )
     */
    public function generateBir2316(Request $request)
    {
        $this->validate(
            $request,
            [
                'data.attributes.company_id' => 'required|integer',
            ]
        );

        $companyId = $request->json('data.attributes.company_id', 0);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized("You are not authorized to generate Employee Government Form.");
        }

        $response = $this->requestService->generateBir2316($request->all(), $this->getAuthzDataScope($request));

        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{companyId}/government_forms/bir_2316/regenerate",
     *     tags={"government_forms"},
     *     description="Regenerate BIR 2316
     *     Authorization Scope : **create.government_form**",
     *     summary="Regenerate BIR 2316",
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         type="object",
     *         @SWG\Schema(
     *             @SWG\Property(property="data", type="array", @SWG\Items(type="integer"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     )
     * )
     */
    public function regenerateBir2316(Request $request, int $companyId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized("You are not authorized to generate Employee Government Form.");
        }

        $response = $this->requestService->regenerateBir2316(
            $companyId,
            $request->all(),
            $this->getAuthzDataScope($request)
        );

        if ($response->isSuccessful()) {
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData);
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/employee/{employee_id}/government_forms/bir_2316/{id}",
     *     summary="Update Employee BIR 2316",
     *     description="Update Employee BIR 2316

Authorization Scope : **edit.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Government Form 2316 ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/generatedBir2316EditFormItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="generatedBir2316EditFormItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="type", type="string"),
     *         @SWG\Property(
     *             property="attributes",
     *             type="object",
     *             @SWG\Property(property="company_id", type="integer"),
     *         ),
     *     ),
     * )
     */
    public function updateBir2316(Request $request, int $id, int $employeeId)
    {
        $this->validate(
            $request,
            [
                'data.attributes.company_id' => 'required|integer',
            ]
        );

        $attributes = $request->all();
        $companyId = Arr::get($attributes, 'data.attributes.company_id', 0);

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());

        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $employeeResponse = $this->employeeRequestService->getEmployee($employeeId);
            $employeeData = json_decode($employeeResponse->getData());

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employeeData, 'payroll_group.id'),
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id'),
                    AuthzDataScope::SCOPE_COMPANY => [
                        data_get($companyData, 'id'),
                        data_get($employeeData, 'company_id')
                    ],
                ]
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized(
                "You are not authorized to make any changes on the Employee Government Form."
            );
        }

        try {
            // For Audit Trail
            $oldResponse = $this->requestService->getBir2316($id, $employeeId);
            $oldData = json_decode($oldResponse->getData(), true);
        } catch (\Exception $e) {
            $oldData = [];
        }

        try {
            $response = $this->requestService->updateBir2316($id, $employeeId, $attributes);

            if ($response->isSuccessful()) {
                // trigger audit trail
                $newData = json_decode($response->getData(), true);
                $this->audit($request, $companyId, $newData, $oldData);
            }
            return $response;
        } catch (HttpException $exception) {
            $exception->getPrevious()->getResponse()->getBody()->rewind();
            $content = json_decode($exception->getPrevious()->getResponse()->getBody()->getContents(), true);
            if (isset($content['errors'])) {
                return $this->response->array([
                    'status_code' => $exception->getStatusCode(),
                    'message' => $exception->getMessage(),
                    'errors' => $content['errors'],
                ])->setStatusCode($exception->getStatusCode());
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employee_id}/government_forms/bir_2316/{id}",
     *     summary="Get generated Government Form",
     *     description="Get a generated Government Form

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Government Form ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getBir2316(Request $request, $id, $employeeId)
    {
        $response = $this->employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData());
        $companyId = $employeeData->company_id;
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employeeData, 'payroll_group.id'),
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id'),
                    AuthzDataScope::SCOPE_COMPANY => [
                        data_get($companyData, 'id'),
                        data_get($employeeData, 'company_id')
                    ],
                ]
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized("You are not authorized to access the Employee Government Forms.");
        }

        return $this->requestService->getBir2316($id, $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/government_forms/bir_2316",
     *     summary="Get generated Government Forms",
     *     description="Get list of generated Government Forms

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="coverage_start_date",
     *         in="query",
     *         description="Coverage Start Date (YYYY-MM-DD)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="coverage_end_date",
     *         in="query",
     *         description="Coverage End Date (YYYY-MM-DD)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="query",
     *         description="Employee ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="query",
     *         description="Payroll Group ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="department_id",
     *         in="query",
     *         description="Department ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="location_id",
     *         in="query",
     *         description="Location ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getGenerated2316s(Request $request, $companyId)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeView(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getGenerated2316s(
            $companyId,
            $request->all(),
            $this->getAuthzDataScope($request)
        );
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/government_forms/bir_2316/employees",
     *     summary="Get generated Government Forms",
     *     description="Get list of generated Government Forms

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month_from",
     *         in="query",
     *         description="Month From",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year_from",
     *         in="query",
     *         description="Year From",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month_to",
     *         in="query",
     *         description="Month To",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year_to",
     *         in="query",
     *         description="Year To",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getCandidate2316Employees(Request $request, $companyId)
    {

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeView(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized("You are not authorized to access the Employee Government Forms.");
        }

        return $this->requestService->getCandidate2316Employees(
            $companyId,
            $request->all(),
            $this->getAuthzDataScope($request)
        );
    }

    /**
     * @SWG\Post(
     *     path="/company/{company_id}/government_forms/bir_2316/download",
     *     summary="Download single/multiple Employee 2316",
     *     description="Download single/multiple Employee 2316

Authorization Scope : **view.government_form**",
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/generatedBir2316DownloadItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="generatedBir2316DownloadItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(
     *             property="ids",
     *             type="array",
     *             @SWG\Items(type="integer"),
     *             collectionFormat="multi"
     *         ),
     *     ),
     * )
     */
    public function downloadEmployee2316(Request $request, $companyId)
    {
        $this->validate(
            $request,
            [
                'data.ids' => 'required|array',
            ]
        );

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeView(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized("You are not authorized to access Employee Government Form.");
        }

        return $this->requestService->downloadEmployee2316(
            $companyId,
            $request->all(),
            $this->getAuthzDataScope($request)
        );
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company/{id}/government_form_periods",
     *     deprecated=true,
     *     summary="Get Government Form Periods",
     *     description="Get list of month/year combinations where company can generate gov. forms

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getGovernmentFormPeriods(Request $request, $id)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $companyResponse = $this->companyRequestService->get($id);
            $companyData = json_decode($companyResponse->getData());
            $createdBy = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getGovernmentFormPeriods($id);
    }

    /**
     * @SWG\Get(
     *     path="/government_forms/1604c/available_years",
     *     description="Get available years for BIR 1604C",
     *     summary="Get available years for BIR 1604C",
     *     tags={"government_forms"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     )
     * )
     */
    public function getAvailableYearsForBir1604c(Request $request)
    {
        $companyId = $request->get('company_id', null);
        if ($companyId === null) {
            $this->invalidRequestError('Company ID should not be empty.');
        } elseif (!is_numeric($companyId)) {
            $this->invalidRequestError('Company ID should be numeric.');
        }

        $companyResponse = $this->companyRequestService->get($companyId);
        if (!$companyResponse) {
            $this->notFoundError('Company not found.');
        }

        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                data_get($companyData, 'id')
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAvailableYearsForBir1604c($companyId);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/sss/r5_methods",
     *     deprecated=true,
     *     summary="Get SSS R5 payment methods",
     *     description="Get list of allowed SSS R5 payment methods and their allowed channels

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getR5PaymentMethods(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getSssR5Methods();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/sss/r5_options",
     *     deprecated=true,
     *     summary="Get SSS R5 form options",
     *     description="Get list of form option values",
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getR5FormOptions()
    {
        return $this->requestService->getSssR5FormOptions();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/sss/r5_values",
     *     deprecated=true,
     *     summary="Get SSS R5 form values",
     *     description="Get list of form values

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getR5FormValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getSssR5FormValues($inputs);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/sss/loan_collection_report_values",
     *     summary="Get SSS loan collection values",
     *     description="Get list of form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getSssLoanCollectionReportValue(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $companyData,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getSssLoanCollectionValues($inputs);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/sss/loan_collection_report_options",
     *     summary="Get SSS loan collection form options",
     *     description="Get list of SSS loan collection form option values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getSssLoanCollectionOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getSssLoanCollectionFormOptions();
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/sss/r5_generate",
     *     summary="Generate SSS R5 forms",
     *     description="Saves input form information, generates R3 and R5 files, zips then gives access url

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="R5 Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "payment_method"={"type"="string"},
     *                 "payment_channel"={"type"="string"},
     *                 "employer_name"={"type"="string"},
     *                 "document_number"={"type"="string"},
     *                 "document_date"={"type"="string"},
     *                 "sbr_number"={"type"="string"},
     *                 "sbr_date"={"type"="string"},
     *                 "sbr_amount"={"type"="number"},
     *                 "employer_zip_code"={"type"="string"},
     *                 "employer_telephone_number"={"type"="string"},
     *                 "employer_mobile_number"={"type"="string"},
     *                 "employer_email"={"type"="string"},
     *                 "employer_website"={"type"="string"},
     *                 "employer_locator_code"={"type"="string"},
     *                 "payor_type"={"type"="string"},
     *                 "payment_type"={"type"="string"},
     *                 "check_number"={"type"="string"},
     *                 "check_name"={"type"="string"},
     *                 "bank_branch_name"={"type"="string"},
     *                 "certified_correct"={"type"="string"},
     *                 "position_title"={"type"="string"},
     *                 "employees"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "employee_id"={"type"="integer"},
     *                              "date_hired_separated"={"type"="string"},
     *                              "employment_status"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generateR5(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateSssR5($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_SSS_R5;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/sss/loan_collection_generate",
     *     summary="Generate SSS loan collection forms",
     *     description="Saves input form information, generates Sss files, zips then gives access url

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Sss collection data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "er_name"={"type"="string"},
     *                 "er_id_number"={"type"="string"},
     *                 "total_amount_paid"={"type"="number"},
     *                 "total_amount_due"={"type"="number"},
     *                 "loans"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "sss_number"={"type"="string"},
     *                              "last_name"={"type"="string"},
     *                              "first_name"={"type"="string"},
     *                              "mi"={"type"="string"},
     *                              "lt"={"type"="string"},
     *                              "penalty"={"type"="number"},
     *                              "ln_date"={"type"="string"},
     *                              "ln_amount"={"type"="number"},
     *                              "amount_paid"={"type"="number"},
     *                              "amount_due"={"type"="number"},
     *                              "effectivity_date"={"type"="string"},
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generateSssCollection(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateSssCollection($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_SSS_LOAN_COLLECTION;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/hdmf/msrf_channels",
     *     summary="Get HDMF MSRF payment channels",
     *     description="Get list of allowed HDMF MSRF payment channels

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getMsrfPaymentChannels(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getHdmfMsrfChannels();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/hdmf/msrf_options",
     *     summary="Get HDMF MSRF form options",
     *     description="Get list of form option values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     deprecated=true,
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getMsrfFormOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getHdmfMsrfFormOptions();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/hdmf/msrf_values",
     *     summary="Get HDMF MSRF form values",
     *     description="Get list of form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getHdmfMsrfValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getHdmfMsrfFormValues($inputs);
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/hdmf/msrf_generate",
     *     summary="Generate HDMF MSRF forms",
     *     description="Saves input form information, generates pdf and .dat files, zips then gives access url

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="MSRF Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "payment_channel"={"type"="string"},
     *                 "employer_name"={"type"="string"},
     *                 "address"={"type"="string"},
     *                 "employer_zip_code"={"type"="string"},
     *                 "contact_number"={"type"="string"},
     *                 "branch_code"={"type"="string"},
     *                 "employer_type"={"type"="string"},
     *                 "payment_type"={"type"="string"},
     *                 "employees"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "employee_id"={"type"="integer"},
     *                              "pagibig_company_id"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generateHdmfMsrf(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateHdmfMsrf($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_HDMF_MSRF;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/stlrf/stlrf_values",
     *     summary="Get STLRF form values",
     *     description="Get list of form values

    Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getStlrfValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $companyData,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getStlrfFormValues($inputs);
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/stlrf/stlrf_generate",
     *     summary="Generate Stlrf forms",
     *     description="Saves input form information, generates pdf file, zips then gives access url
    Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="STLRF Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "pag_ibig_employer_id"={"type"="string"},
     *                 "employer_name"={"type"="string"},
     *                 "employer_address_line_1"={"type"="string"},
     *                 "employer_address_line_2"={"type"="string"},
     *                 "employer_city"={"type"="string"},
     *                 "employer_country"={"type"="string"},
     *                 "employer_zip_code"={"type"="string"},
     *                 "contact_number"={"type"="string"},
     *                 "loans"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "pagibig_id"={"type"="string"},
     *                              "application_number"={"type"="string"},
     *                              "employee_full_name"={"type"="string"},
     *                              "loan_type"={"type"="string"},
     *                              "amount"={"type"="string"},
     *                              "employer_remarks"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generateStlrf(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $companyData,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateStlrf($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_STLRF;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/stlrf/stlrf_options",
     *     summary="Get STLRF form options",
     *     description="Get list of STLRF form option values",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getStlrfOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getStlrfFormOptions();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/philhealth/values",
     *     summary="Get Philhealth form values",
     *     description="Get list of form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getPhilhealthValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getPhilhealthFormValues($inputs);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601c_options",
     *     summary="Get BIR 1601c form options",
     *     description="Get list of form option values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getBir1601cFormOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getBir1601cFormOptions();
    }


    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/bir/1601c_generate",
     *     summary="Generate BIR 1601C form",
     *     description="Generates 1601C PDF file then gives access url

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="1601 Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "amendment_return"={"type"="integer"},
     *                 "number_of_sheets_attached"={"type"="integer"},
     *                 "any_taxes_withheld"={"type"="integer"},
     *                 "rdo_code"={"type"="string"},
     *                 "line_of_business_occupation"={"type"="string"},
     *                 "company_name"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "registered_address"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "category_of_withholding_agent"={"type"="string"},
     *                 "tax_treaty"={"type"="integer"},
     *                 "tax_treaty_law_name"={"type"="string"},
     *                 "total_amount_of_compensation"={"type"="number"},
     *                 "statutory_mwe_income"={"type"="number"},
     *                 "mwe_other_income"={"type"="number"},
     *                 "other_non_taxable_compensation"={"type"="number"},
     *                 "tax_required_to_be_withheld"={"type"="number"},
     *                 "tax_remitted_in_return_previously_field"={"type"="number"},
     *                 "other_payments_made"={"type"="number"},
     *                 "surcharge"={"type"="number"},
     *                 "interest"={"type"="number"},
     *                 "compromise"={"type"="number"},
     *                 "section_a"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "frequency"={"type"="string"},
     *                              "previous_month"={"type"="string"},
     *                              "date_paid"={"type"="string"},
     *                              "bank_validation"={"type"="string"},
     *                              "bank_code"={"type"="string"},
     *                              "tax_paid"={"type"="number"},
     *                              "should_be_tax_due"={"type"="number"}
     *                          }
     *                      }
     *                  },
     *                 "signatory_name"={"type"="string"},
     *                 "signatory_position"={"type"="string"},
     *                 "signatory_tin"={"type"="string"},
     *                 "treasurer_name"={"type"="string"},
     *                 "treasurer_position"={"type"="string"},
     *                 "treasurer_tin"={"type"="string"},
     *                 "tax_agent_account_number"={"type"="string"},
     *                 "date_of_issuance"={"type"="string"},
     *                 "date_of_expiry"={"type"="string"},
     *                 "cash_drawee_bank"={"type"="string"},
     *                 "cash_number"={"type"="string"},
     *                 "cash_date"={"type"="string"},
     *                 "cash_amount"={"type"="number"},
     *                 "check_drawee_bank"={"type"="string"},
     *                 "check_number"={"type"="string"},
     *                 "check_date"={"type"="string"},
     *                 "check_amount"={"type"="number"},
     *                 "others_drawee_bank"={"type"="string"},
     *                 "others_number"={"type"="string"},
     *                 "others_date"={"type"="string"},
     *                 "others_amount"={"type"="number"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generate1601c(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $companyData,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateBir1601C($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_BIR_1601C;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601c_values",
     *     summary="Get BIR form values",
     *     description="Get list of form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Values not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get1601cFormValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getBir1601CFormValues($inputs);
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601e_options",
     *     summary="Get BIR 1601E form options",
     *     description="Get list of BIR 1601E form option values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getBir1601eFormOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getBir1601EFormOptions();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601e_values",
     *     summary="Get BIR form 1601E values",
     *     description="Get list of BIR 1601E form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Values not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get1601eFormValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getBir1601EFormValues($inputs);
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/bir/1601e_generate",
     *     summary="Generate BIR 1601E form",
     *     description="Generates 1601E PDF file then gives access url

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="1601E Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "amendment_return"={"type"="integer"},
     *                 "number_of_sheets_attached"={"type"="integer"},
     *                 "any_taxes_withheld"={"type"="integer"},
     *                 "rdo_code"={"type"="string"},
     *                 "line_of_business_occupation"={"type"="string"},
     *                 "company_name"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "registered_address"={"type"="string"},
     *                 "zip_code"={"type"="number"},
     *                 "category_of_withholding_agent"={"type"="string"},
     *                 "tax_treaty"={"type"="integer"},
     *                 "tax_treaty_law_name"={"type"="integer"},
     *                 "computation_of_tax"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "nature_of_income_payment"={"type"="string"},
     *                              "atc"={"type"="string"},
     *                              "tax_base"={"type"="number"},
     *                              "tax_rate"={"type"="number"},
     *                              "tax_required_to_be_withheld"={"type"="number"}
     *                          }
     *                      }
     *                  },
     *                 "advance_payments_made"={"type"="number"},
     *                 "tax_remitted_in_return_previously_filed"={"type"="number"},
     *                 "surcharge"={"type"="number"},
     *                 "interest"={"type"="number"},
     *                 "compromise"={"type"="number"},
     *                 "signatory_name"={"type"="string"},
     *                 "signatory_position"={"type"="string"},
     *                 "signatory_tin"={"type"="string"},
     *                 "treasurer_name"={"type"="string"},
     *                 "treasurer_position"={"type"="string"},
     *                 "treasurer_tin"={"type"="string"},
     *                 "tax_agent_account_number"={"type"="string"},
     *                 "date_of_issuance"={"type"="string"},
     *                 "date_of_expiry"={"type"="string"},
     *                 "cash_drawee_bank"={"type"="string"},
     *                 "cash_number"={"type"="string"},
     *                 "cash_date"={"type"="string"},
     *                 "cash_amount"={"type"="number"},
     *                 "check_drawee_bank"={"type"="string"},
     *                 "check_number"={"type"="string"},
     *                 "check_date"={"type"="string"},
     *                 "check_amount"={"type"="number"},
     *                 "others_drawee_bank"={"type"="string"},
     *                 "others_number"={"type"="string"},
     *                 "others_date"={"type"="string"},
     *                 "others_amount"={"type"="number"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generate1601e(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateBir1601E($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_BIR_1601E;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601f_options",
     *     summary="Get BIR 1601F form options",
     *     description="Get list of BIR 1601F form option values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getBir1601fFormOptions(Request $request)
    {
        $this->authorizeRequestor($request);
        return $this->requestService->getBir1601fFormOptions();
    }

    /**
     * @SWG\Get(
     *     path="/philippine/government_forms/bir/1601f_values",
     *     summary="Get BIR form 1601 F values",
     *     description="Get list of BIR 1601 F form values

Authorization Scope : **create.government_form**",
     *     deprecated=true,
     *     tags={"government_forms"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Month",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Values not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get1601fFormValues(Request $request)
    {
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getBir1601FFormValues($inputs);
    }

    /**
     * @SWG\Post(
     *     path="/philippine/government_forms/bir/1601f_generate",
     *     summary="Generate BIR 1601F form",
     *     description="Generates 1601F PDF file then gives access url

Authorization Scope : **create.government_form**",
     *     tags={"government_forms"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="1601F Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "company_id"={"type"="integer"},
     *                 "month"={"type"="integer"},
     *                 "year"={"type"="integer"},
     *                 "amendment_return"={"type"="integer"},
     *                 "number_of_sheets_attached"={"type"="integer"},
     *                 "any_taxes_withheld"={"type"="integer"},
     *                 "rdo_code"={"type"="string"},
     *                 "line_of_business_occupation"={"type"="string"},
     *                 "company_name"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "registered_address"={"type"="string"},
     *                 "zip_code"={"type"="number"},
     *                 "category_of_withholding_agent"={"type"="string"},
     *                 "tax_treaty"={"type"="integer"},
     *                 "tax_treaty_law_name"={"type"="string"},
     *                 "total_treaty_tax_required"={"type"="number"},
     *                 "tax_remitted_in_return_previously_filed"={"type"="number"},
     *                 "surcharge"={"type"="number"},
     *                 "interest"={"type"="number"},
     *                 "compromise"={"type"="number"},
     *                 "total_amount_still_due"={"type"="number"},
     *                 "computation_of_tax"={"type"="array",
     *                      "items"={"type"="object",
     *                          "properties"={
     *                              "nature_of_income_payment"={"type"="string"},
     *                              "atc"={"type"="string"},
     *                              "tax_base"={"type"="number"},
     *                              "tax_rate"={"type"="number"},
     *                              "tax_required_to_be_withheld"={"type"="number"}
     *                          }
     *                      }
     *                  },
     *                 "signatory_name"={"type"="string"},
     *                 "signatory_position"={"type"="string"},
     *                 "signatory_tin"={"type"="string"},
     *                 "treasurer_name"={"type"="string"},
     *                 "treasurer_position"={"type"="string"},
     *                 "treasurer_tin"={"type"="string"},
     *                 "tax_agent_account_number"={"type"="string"},
     *                 "date_of_issuance"={"type"="string"},
     *                 "date_of_expiry"={"type"="string"},
     *                 "cash_drawee_bank"={"type"="string"},
     *                 "cash_number"={"type"="string"},
     *                 "cash_date"={"type"="string"},
     *                 "cash_amount"={"type"="number"},
     *                 "check_drawee_bank"={"type"="string"},
     *                 "check_number"={"type"="string"},
     *                 "check_date"={"type"="string"},
     *                 "check_amount"={"type"="number"},
     *                 "others_drawee_bank"={"type"="string"},
     *                 "others_number"={"type"="string"},
     *                 "others_date"={"type"="string"},
     *                 "others_amount"={"type"="number"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function generate1601f(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }
        $inputs = $request->all();
        $companyResponse = $this->companyRequestService->get($inputs['company_id']);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->generateBir1601F($inputs);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $inputs['form_type'] = GovernmentFormAuditService::TYPE_BIR_1601F;
            $this->audit($request, $inputs['company_id'], $inputs);
        }

        return $response;
    }

    /**
     * Authorize request using user data of requestor
     *
     * @param Request $request
     * @return void
     */
    protected function authorizeRequestor(Request $request)
    {
        $userRequestService = App::make(UserRequestService::class);

        $createdBy = $request->attributes->get('user');
        $userResponse = $userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        // TODO support array of company ids, because user can have more then one company
        // for now, it will take only first company id from array
        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (!$this->authorizationService->authorizeCreate($authData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }
    }
}
