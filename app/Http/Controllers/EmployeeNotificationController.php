<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ESS\EssNotificationRequestService;
use App\Notification\EmployeeNotificationAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use App\Storage\PictureService;

class EmployeeNotificationController extends Controller
{
    /**
     * @var \App\ESS\EssNotificationRequestService
     */
    protected $requestService;

    /**
     * @var \App\Notification\EmployeeNotificationAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        EssNotificationRequestService $requestService,
        EmployeeNotificationAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/notifications",
     *     summary="Get User Notifications",
     *     description="Get all Notifications for user.
     Authorization Scope : **view.notification**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *         description="Items to display per one page. Default: 10",
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1. Default is 1.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getNotifications(Request $request)
    {
        $user = $request->attributes->get('user');

        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $authParams = (object) [
                'company_id' => $user['employee_company_id'],
                'account_id' => $user['account_id']
            ];

            $authorized = $this->authorizationService->authorizeView($authParams, $user);

            // authorize
            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->getNotifications($user['user_id'], $request->all(), true);
    }

    /**
     * @SWG\Get(
     *     path="/announcement_notifications",
     *     summary="Get User Announcement Notifications",
     *     description="Get all Notifications for user.
     Authorization Scope : **view.notification**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *      @SWG\Parameter(
     *          name="paginate",
     *          in="query",
     *          description="Use pagination",
     *          required=true,
     *          type="boolean"
     *      ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Items to display per one page. Example: 10",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1. Default is 1.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAnnouncementNotifications(Request $request)
    {
        $user = $request->attributes->get('user');

        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $authParams = (object) [
                'company_id' => $user['employee_company_id'],
                'account_id' => $user['account_id']
            ];

            $authorized = $this->authorizationService->authorizeView($authParams, $user);

            // authorize
            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->getAnnouncementNotifications(
            $user['user_id'],
            $request->input('paginate'),
            $request->input('page'),
            $request->input('per_page'),
            $request->input('response_type'),
            $request->input('activity_type')
        );

        if ($response->isSuccessful() && !empty($request->input('response_type'))) {
            $result = json_decode($response->getData(), true);
            $data = array_get($result, 'data', []);
            $service = app()->make(PictureService::class);

            $resultData = collect($data)->map(function ($item) use ($service) {
                if (empty($item['sender_picture']) && !empty($item['sender_user_id'])) {
                    $item['sender_picture'] = $service->getUserPicture($item['sender_user_id']);
                }
                return $item;
            })->toArray();

            $result['data'] = $resultData;

            return $result;
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/notifications_status",
     *     summary="Get Notifications Status",
     *     description="Get Notifications Status for user.
     Authorization Scope : **view.notification**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getNotificationsStatus(Request $request)
    {
        $user = $request->attributes->get('user');
        $authParams = (object) [
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id']
        ];

        // authorize
        if (!$this->authorizationService->authorizeView($authParams, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getNotificationsStatus($user['user_id']);
    }


    /**
     * @SWG\Put(
     *     path="/notifications_status",
     *     summary="Update Notifications Status",
     *     description="Update Notifications Status for user.
     Authorization Scope : **view.notification**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateNotificationsStatus(Request $request)
    {
        $user = $request->attributes->get('user');
        $authParams = (object) [
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id']
        ];

        // authorize
        if (!$this->authorizationService->authorizeView($authParams, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->updateNotificationsStatus($user['user_id']);
    }

    /**
     * @SWG\Put(
     *     path="/notifications/{id}/clicked",
     *     summary="Update Notification clicked status",
     *     description="Update Notification clicked status
     Authorization Scope : **view.notification**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Notification ID (It is an ID from notifications table on nt-api database)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function notificationClicked($id, Request $request)
    {
        if (!$this->isAuthzEnabled($request)) {
            $user = $request->attributes->get('user');
            $authParams = (object) [
                'company_id' => $user['employee_company_id'],
                'account_id' => $user['account_id']
            ];

            // authorize
            if (!$this->authorizationService->authorizeView($authParams, $user)) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->notificationClicked($id);
    }
}
