<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\RestDay\RestDayAuditService;
use App\RestDay\RestDayAuthorizationService;
use App\RestDay\RestDayRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class RestDayController extends Controller
{
    /**
     * @var \App\RestDay\RestDayRequestService
     */
    protected $requestService;

    /**
     * @var \App\RestDay\RestDayAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        RestDayRequestService $requestService,
        RestDayAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/rest_day/{id}",
     *     summary="Get Rest Day",
     *     description="Get Rest Day Details
Authorization Scope : **view.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Rest Day ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $restDayData = json_decode($response->getData());
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeRequestService = App::make(EmployeeRequestService::class);
            $employeeFullDataResponse = $employeeRequestService->getEmployee($restDayData->employee_id)->getData();
            $employee = json_decode($employeeFullDataResponse, true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $restDayData->company_id,
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll_group.payroll_group_id'),
            ]);
        } else {
            $restDayData->account_id = Company::getAccountId($restDayData->company_id);
            $isAuthorized = $this->authorizationService->authorizeGet(
                $restDayData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/rest_days",
     *     summary="Get all company Rest Days",
     *     description="Get all Rest Days within company.
    Authorization Scope : **view.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyRestDays($companyId, Request $request)
    {
        $restDay = (object) [
            'account_id' => $companyId ? Company::getAccountId($companyId) : null,
            'company_id' => $companyId
        ];

        $authorized = $this->authorizationService->authorizeGetCompanyRestDays(
            $restDay,
            $request->attributes->get('user')
        );

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // Set Key; formula = 'rest_days:company_id:' + $companyId
        $cacheKey = 'rest_days:company_id:' . $companyId;

        // Get leaves from cache, if none, fetch new data and set to cache
        if (Redis::exists($cacheKey)) {
            $cache = Redis::hGetAll($cacheKey);
            $response = $cache['data'];
            return $this->response->array($response);
        } else {
            // Fetch new data from microservice
            $response = $this->requestService->getCompanyRestDays($companyId);

            // Save data to cache
            Redis::hMSet(
                $cacheKey,
                [
                    'data'=>$response->getData(),
                    'status_code'=> $response->getStatusCode()
                ]
            );
            // Set life of cache at midnight
            Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
            return $response;
        }
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employee_id}/rest_days",
     *     summary="Get employee rest days",
     *     description="Get all rest days for employee with given ID.
Authorization Scope : **view.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeRestDays(Request $request, $employeeId, EmployeeRequestService $employeeRequestService)
    {
        $user = $request->attributes->get('user');
        $companyId = (int)$request->input('company_id');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($employeeId);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [Arr::get($employee, 'company_id'), $companyId],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $restDayData = (object)[
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($restDayData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Set Key; formula = 'rest_days:company_id:' + $companyId
        $cacheKey = 'rest_days:company_id:' . $companyId . ':employee_id:' . $employeeId;

        // Get leaves from cache, if none, fetch new data and set to cache
        if (Redis::exists($cacheKey)) {
            $cache = Redis::hGetAll($cacheKey);
            $response = $cache['data'];
            return $this->response->array($response);
        } else {
            // Fetch new data from microservice
            $response = $this->requestService->getCompanyEmployeeRestDays($employeeId, $companyId);

            // Save data to cache
            Redis::hMSet(
                $cacheKey,
                [
                    'data'=>$response->getData(),
                    'status_code'=> $response->getStatusCode()
                ]
            );
            // Set life of cache at midnight
            Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
            return $response;
        }
    }

    /**
     * @SWG\Post(
     *     path="/rest_day",
     *     summary="Create Rest Day for the company employee.",
     *     description="Create Rest Day for the company employee.
Authorization Scope : **create.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/rest_day"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     *  @SWG\Definition(
     *     definition="rest_day",
     *     required={"company_id", "employee_id", "start_date"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer",
     *         description="Employee ID"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         description="Rest Day start date in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         description="Rest Day end date in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="repeat",
     *         ref="#/definitions/rest_day_repeat"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="rest_day_repeat",
     *     required={"rest_days", "repeat_every", "end_never"},
     *     @SWG\Property(
     *         property="rest_days",
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"}
     *         ),
     *         description="Repeat on week days. Possible values: monday, tuesday, wednesday,
 thursday, friday, saturday, sunday"
     *     ),
     *     @SWG\Property(
     *         property="repeat_every",
     *         type="integer",
     *         description="Repeat Every (No. of unit)"
     *     ),
     *     @SWG\Property(
     *         property="end_never",
     *         type="boolean",
     *         default=false,
     *         description="Indication is rest day repeat never ended"
     *     ),
     *     @SWG\Property(
     *         property="end_after",
     *         type="integer",
     *         description="Rest Day end after (No. of occurrences)"
     *     )
     * )
     */
    public function create(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $restDayData = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $isAuthorized = $$this->authorizationService->authorizeCreate($restDayData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->create($request->all());

        $responseData = json_decode($response->getData(), true);

        $restDayGetResponse = $this->requestService->get($responseData['id']);
        $restDayGetResponseData = json_decode($restDayGetResponse->getData(), true);

        if ($response->isSuccessful()) {
            // audit log
            $item = new AuditCacheItem(
                RestDayAuditService::class,
                RestDayAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                [
                    'new' => $restDayGetResponseData,
                ]
            );

            $this->auditService->queue($item);
            // Delete Cache on restday create
            Redis::del('rest_days:company_id:' . $companyId . ':employee_id:' . $request->input('employee_id'));
            Redis::del('rest_days:company_id:' . $companyId);
            Redis::del('rest_days:employee_id:' . $request->input('employee_id'));
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/rest_day/{id}",
     *     summary="Update Rest Day.",
     *     description="Update Rest Day details.
Authorization Scope : **edit.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         required=true,
     *         description="Rest Day ID"
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/rest_day"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $oldRestDayResponse = $this->requestService->get($id);
        $oldRestDayData = json_decode($oldRestDayResponse->getData());
        $oldRestDayData->account_id = $accountId;

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($oldRestDayData->employee_id);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldRestDayData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($id, $request->all());

        if ($updateResponse->isSuccessful()) {
            $getResponse = $this->requestService->get($id);

            $oldRestDayData = json_decode($oldRestDayResponse->getData(), true);
            $newRestDayData = json_decode($getResponse->getData(), true);
            $item = new AuditCacheItem(
                RestDayAuditService::class,
                RestDayAuditService::ACTION_UPDATE,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                [
                    'old' => $oldRestDayData,
                    'new' => $newRestDayData,
                ]
            );

            $this->auditService->queue($item);
            // Delete Cache on restday create
            Redis::del('rest_days:company_id:' . $companyId . ':employee_id:' . $request->input('employee_id'));
            Redis::del('rest_days:company_id:' . $companyId);
            Redis::del('rest_days:employee_id:' . $request->input('employee_id'));
        }

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/rest_day/unassign/{id}",
     *     summary="Unassign Rest Day",
     *     description="Unassign Rest Day for given attributes
Authorization Scope : **edit.rest_day**",
     *     tags={"rest_day"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Rest Day ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="A inclusive lower date boundary for unassigning in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="A inclusive upper date boundary for unassigning in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function unassign($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        // authorize
        $oldRestDayResponse = $this->requestService->get($id);
        $oldRestDayData = json_decode($oldRestDayResponse->getData());
        $companyId = $oldRestDayData->company_id;
        $updatedBy = $request->attributes->get('user');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;
        $oldRestDayData->account_id = $accountId;

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($oldRestDayData->employee_id);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUnassign($oldRestDayData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $usassignResponse = $this->requestService->unassign($id, $request->all());

        if ($usassignResponse->isSuccessful()) {
            $oldRestDayData = json_decode($oldRestDayResponse->getData(), true);
            $item = new AuditCacheItem(
                RestDayAuditService::class,
                RestDayAuditService::ACTION_UNASSIGN,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                [
                    'old' => $oldRestDayData
                ]
            );

            $this->auditService->queue($item);
            // Delete Cache on restday create
            Redis::del('rest_days:company_id:' . $companyId . ':employee_id:' . $request->input('employee_id'));
            Redis::del('rest_days:company_id:' . $companyId);
            Redis::del('rest_days:employee_id:' . $request->input('employee_id'));
        }

        return $usassignResponse;
    }
}
