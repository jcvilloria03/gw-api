<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EssBaseController;
use App\LeaveCredit\LeaveCreditRequestService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;

class EssLeaveCreditController extends EssBaseController
{
    /**
     * @var \App\LeaveCredit\LeaveCreditRequestService
     */
    protected $leaveCreditRequestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $employeeRequestAuthorizationService;

    public function __construct(
        LeaveCreditRequestService $leaveCreditRequestService,
        EssEmployeeRequestAuthorizationService $employeeRequestAuthorizationService
    ) {
        $this->leaveCreditRequestService = $leaveCreditRequestService;
        $this->employeeRequestAuthorizationService = $employeeRequestAuthorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/leave_credits",
     *     summary="Get Employee Leave Credits",
     *     description="Get Company Leave Credits
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function index(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->employeeRequestAuthorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = ['employees_ids' => [$user['employee_id']]];
        $queryString = $request->getQueryString() ?? 'per_page=1000';

        return $this->leaveCreditRequestService->getCompanyLeaveCredits(
            $user['employee_company_id'],
            $data,
            $queryString
        );
    }
}
