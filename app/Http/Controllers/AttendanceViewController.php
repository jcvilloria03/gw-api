<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facades\Company;
use Symfony\Component\HttpFoundation\Response;
use App\AttendanceView\AttendanceViewRequestService;
use App\AttendanceView\AttendanceViewAuthorizationService;
use App\AttendanceView\AttendanceViewAuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;

class AttendanceViewController extends Controller
{

    /**
     * @var \App\AttendanceView\AttendanceViewRequestService
     */
    protected $requestService;

    /**
     * @var \App\LeaveRequest\LeaveRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * AttendanceViewController constructor.
     *
     * @param AttendanceViewRequestService       $requestService       AttendanceViewRequestService
     * @param AttendanceViewAuthorizationService $authorizationService AttendanceViewAuthorizationService
     * @param AuditService                       $auditService         AuditService
     */
    public function __construct(
        AttendanceViewRequestService $requestService,
        AttendanceViewAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/view/{companyId}/attendance/user",
     *     summary="Get User's Custom Views",
     *     description="Get User's Custom Views
    Authorization Scope : **view.attendance_view**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getUserCustomView($companyId, Request $request): \Illuminate\Http\JsonResponse
    {
        $userData = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $userCustomViewAuthData = (object)[
                'account_id' => Company::getAccountId((int) $companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($userCustomViewAuthData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getUserCustomView((int) $companyId, (int) $userData['user_id']);
    }

    /**
     * @SWG\Get(
     *     path="/view/attendance/form_options/{id}",
     *     summary="Get Attendance Custom View Columns",
     *     description="Get Attendance Custom View Columns
    Authorization Scope : **view.attendance_view**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getAttendanceViewColumns($id, Request $request): \Illuminate\Http\JsonResponse
    {
        $attendanceViewData = (object)[
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];
        if (!$this->authorizationService->authorizeGet($attendanceViewData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAttendanceViewColumns((int)$id);
    }

    /**
     * @SWG\Get(
     *     path="/view/attendance/{attendanceId}",
     *     summary="Get Single View",
     *     description="Get Single View
    Authorization Scope : **view.attendance_view**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="attendanceId",
     *         in="path",
     *         description="Attendance ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getView($attendanceId, Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->requestService->getView((int) $attendanceId);
        $responseData = json_decode($response->getData(), true);
        $companyId = $responseData['data']['company_id'];

        $attendanceViewData = (object)[
            'account_id' => Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet($attendanceViewData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/view/attendance",
     *     summary="Create Attendance View",
     *     description="Create Attendance View
    Authorization Scope : **create.attendance_view**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="# Required Columns & Possible filters:
    employee_id
    employee_name
    date
    expected_shift
    regular
    ---------------
    location_id
    department_id
    position_id
    start_date
    end_date",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "required"={"employee_id", "employee_name", "date", "expected_shift", "regular"},
     *             "properties"={
     *                  "company_id"={"type"="integer"},
     *                  "name"={"type"="string"},
     *                  "mode"={"type"="string"},
     *                  "employee_id"={"type"="integer"},
     *                  "employee_name"={"type"="string"},
     *                  "date"={"type"="string"},
     *                  "expected_shift"={"type"="string"},
     *                  "regular"={"type"="string"},
     *                  "columns"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "filters"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="object",
     *                          "properties"={
     *                              "field"={"type"="string"},
     *                              "condition"={"type"="string"},
     *                              "value"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function createCustomView(Request $request): \Illuminate\Http\JsonResponse
    {
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $companyId = $inputs['company_id'];

        $customViewData = (object)[
            'account_id' => Company::getAccountId($companyId),
            'company_id' => $companyId
        ];
        if (!$this->authorizationService->authorizeCreate($customViewData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $inputs['user_id'] = $createdBy['user_id'];
        $response = $this->requestService->createCustomView($inputs);
        $responseData = json_decode($response->getData(), true);
        $customViewId = array_get($responseData['data'], 'id');

        // audit log
        $userCustomViewResponse = $this->requestService->getView($customViewId);
        $userCustomViewData = json_decode($userCustomViewResponse->getData(), true);

        $details = [
            'new' => $userCustomViewData['data']
        ];

        // creates auditCacheItem
        $item = $this->getAuditCacheItemInstance($createdBy, $details);
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/view/attendance/{attendanceId}",
     *     summary="Update Attendance View",
     *     description="Update Attendance View
    Authorization Scope : **update.attendance_view**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="attendanceId",
     *         in="path",
     *         description="Attendance ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="# Required Columns & Possible filters:
    employee_id
    employee_name
    date
    expected_shift
    regular
    ---------------
    location_id
    department_id
    position_id
    start_date
    end_date",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "name"={"type"="string"},
     *                  "mode"={"type"="string"},
     *                  "columns"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "filters"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="object",
     *                          "properties"={
     *                              "field"={"type"="string"},
     *                              "condition"={"type"="string"},
     *                              "value"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function updateCustomView($attendanceId, Request $request): \Illuminate\Http\JsonResponse
    {
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $oldDataResponse = $this->requestService->getView($attendanceId);
        $oldData = json_decode($oldDataResponse->getData(), true);

        $companyId = array_get($oldData['data'], 'company_id');

        $customViewData = (object)[
            'account_id' => Company::getAccountId($companyId),
            'company_id' => $companyId
        ];
        if (!$this->authorizationService->authorizeUpdate($customViewData, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        $updateResponse = $this->requestService->updateCustomView($inputs, $attendanceId);

        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $newData = json_decode($updateResponse->getData(), true);

        $details = [
            'new' => $newData['data'],
            'old' => $oldData['data'],
            'company_id' => $companyId
        ];

        // creates auditCashItem
        $item = $this->getAuditCacheItemInstance($updatedBy, $details, true);
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * Gets AuditCacheItem instance
     *
     * @param array $userData
     * @param array $details
     * @param bool  $isUpdating false by default
     *
     * @return \App\Audit\AuditCacheItem $AuditCacheItem
     */
    private function getAuditCacheItemInstance(
        array $userData,
        array $details,
        bool $isUpdating = false
    ): AuditCacheItem {
        $action = $isUpdating
            ? AttendanceViewAuditService::ACTION_UPDATE
            : AttendanceViewAuditService::ACTION_CREATE;

        return new AuditCacheItem(
            AttendanceViewAuditService::class,
            $action,
            new AuditUser($userData['user_id'], $userData['account_id']),
            $details
        );
    }
}
