<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EssBaseController;
use App\HoursWorked\HoursWorkedRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\HoursWorked\EssHoursWorkedAuthorizationService;

class EssHoursWorkedController extends EssBaseController
{
    /**
     * @var \App\HoursWorked\HoursWorkedRequestService
     */
    private $requestService;

    /**
     * @var \App\HoursWorked\EssHoursWorkedAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        HoursWorkedRequestService $requestService,
        EssHoursWorkedAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/hours_worked",
     *     summary="Get employee's hours worked",
     *     description="Get employee's hours worked
Authorization Scope : **ess.view.hours_worked**",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         type="string",
     *         description="A inclusive lower date boundary for filtering in format YYYY-MM-DD",
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         type="string",
     *         description="A upper lower date boundary for filtering in format YYYY-MM-DD",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getHoursWorked(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeGetHoursWorked($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        $data = [
            'company_id' => $user['employee_company_id'],
            'start_date' => $request->input('start_date', ''),
            'end_date' => $request->input('end_date', ''),
            'employees_ids' => [$user['employee_id']]
        ];

        return $this->requestService->getEmployeeHoursWorked($data);
    }
}
