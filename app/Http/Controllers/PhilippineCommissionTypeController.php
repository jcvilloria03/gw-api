<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\CommissionType\CommissionTypeAuthorizationService;
use App\CommissionType\PhilippineCommissionTypeRequestService;
use App\Facades\Company;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

class PhilippineCommissionTypeController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\CommissionType\PhilippineCommissionTypeRequestService
     */
    protected $requestService;

    /**
     * @var \App\CommissionType\CommissionTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncomeType\OtherIncomeTypeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineCommissionTypeRequestService $requestService,
        CommissionTypeAuthorizationService $authorizationService,
        OtherIncomeTypeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/commission_type/bulk_create",
     *     summary="Create Multiple Commission Types",
     *     description="Create multiple commission types,
Authorization Scope : **create.commission_types**",
     *     tags={"commission_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="commission_types",
     *         in="body",
     *         description="Create multiple commission types",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/commissionTypesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "name": "Commission Type Name",
     *                          "company_id": 1,
     *                          "fully_taxable": false,
     *                          "basis": "FIXED",
     *                          "max_non_taxable": false,
     *                          "type_name": "App\Model\PhilippineCommissionType",
     *                          "deleted_at": null
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="commissionTypesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/commissionTypesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     *
     * @SWG\Definition(
     *     definition="commissionTypesRequestItem",
     *     required={"name", "basis", "fully_taxable"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Commission type name",
     *         example="Placement Fees"
     *     ),
     *     @SWG\Property(
     *         property="basis",
     *         type="string",
     *         enum=App\OtherIncomeType\OtherIncomeTypeService::BASIS,
     *         description="Commission type basis",
     *         example="FIXED"
     *     ),
     *     @SWG\Property(
     *         property="fully_taxable",
     *         type="boolean",
     *         enum={1,0},
     *         description="Is commission with this commission type fully taxable",
     *         example="1"
     *     ),
     *      @SWG\Property(
     *         property="max_non_taxable",
     *         type="number",
     *         description="If not fully taxable specify max non taxable amount",
     *         example="100"
     *     )
     * )
     */
    public function bulkCreate(Request $request, $id)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];
            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($id, $inputs);
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        if ($response->isSuccessful()) {
            foreach ($inputs as $input) {
                $this->audit($request, $id, $input);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/commission_type/is_name_available",
     *     summary="Is philippine commission type name available",
     *     description="Check availability of philippine commission type name with the given company,
Authorization Scope : **create.commission_types**",
     *     tags={"commission_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Type name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="other_income_type_id",
     *         in="formData",
     *         description="Other Income Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="object", @SWG\Property(property="available", type="boolean", example=true))
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Id required": {"message": "The id field is required.", "status_code": 406},
     *              "Other income type id must be an integer": {
     *                  "message": "The other income type id must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Name is required": {"message": "The name field is required.", "status_code": 406},
     *         }
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $createdBy = $request->attributes->get('user');
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];

            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->isNameAvailable($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/commission_type/{id}",
     *     summary="Update the commission type",
     *     description="Update the commission type
Authorization Scope : **edit.commission_types**",
     *     tags={"commission_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         description="Commission type id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         type="string",
     *         required=true,
     *         description="Commission type name"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="basis",
     *         type="string",
     *         enum=App\OtherIncomeType\OtherIncomeTypeService::BASIS,
     *         required=true,
     *         description="Commission type basis"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="fully_taxable",
     *         type="integer",
     *         enum={1,0},
     *         required=true,
     *         description="Is commission with this commission type fully taxable"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="max_non_taxable",
     *         type="number",
     *         description="If not fully taxable specify max non taxable amount"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      "id": 1,
     *                      "name": "Bonus Test",
     *                      "company_id": 1,
     *                      "fully_taxable": true,
     *                      "max_non_taxable": null,
     *                      "basis": "FIXED",
     *                      "frequency": "ONE_TIME",
     *                      "type_name": "App\\Model\\PhilippineBonusType",
     *                      "deleted_at": null
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *         examples={
     *              "Default": {"message": "Other Income Type not found.", "status_code": 404}
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *              "Default": {
     *                  "id": "The id field is required.",
     *                  "name": "The name field is required.",
     *                  "fully_taxable": "The fully taxable field is required.",
     *                  "basis": "The basis field is required.",
     *              }
     *         }
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeTypeRequestService $otherIncomeTypeRequestService,
        $id
    ) {
        $commissionTypeResponse = $otherIncomeTypeRequestService->get($id);
        if (!$commissionTypeResponse->isSuccessful()) {
            return $commissionTypeResponse;
        }
        $commissionTypeData = json_decode($commissionTypeResponse->getData(), true);
        $companyId = array_get($commissionTypeData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeUpdate($authData, $updatedBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $request->all());
        if ($response->isSuccessful()) {
            $this->auditService->mapAndQueueAuditItems(
                [json_decode($response->getData(), true)],
                OtherIncomeTypeAuditService::ACTION_UPDATE,
                $updatedBy,
                [$commissionTypeData]
            );
        }

        return $response;
    }
}
