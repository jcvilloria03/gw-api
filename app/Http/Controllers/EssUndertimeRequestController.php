<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\ESS\EssClockStateService;
use App\Workflow\WorkflowRequestService;
use App\Http\Controllers\EssBaseController;
use App\ESS\EssUndertimeRequestRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\UndertimeRequest\UndertimeRequestAuditService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Authz\AuthzDataScope;
use Illuminate\Support\Facades\App;
use App\ClockState\ClockStateService;
use App\Transformer\TimeSheetTransformer;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection;

class EssUndertimeRequestController extends EssBaseController
{
    /**
     * @var \App\ESS\EssUndertimeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Transformer\TimeSheetTransformer
     */
    private $transformer;

    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function __construct(
        EssUndertimeRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService,
        TimeSheetTransformer $transformer,
        FractalManager $fractal
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
    }

    /**
     * @SWG\Post(
     *     path="/ess/undertime_request",
     *     summary="Create Undertime Request",
     *     description="Create Undertime Request
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newUndertimeRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="newUndertimeRequest",
     *     required={"date"},
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Date with undertime in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="messages",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *         description="Initial messages."
     *     ),
     *     @SWG\Property(
     *         property="attachments_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi",
     *         description="Attachments IDs."
     *     ),
     *     @SWG\Property(
     *         property="shifts",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/undertimeShifts")
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="undertimeShifts",
     *     required={"shift_id,hours,start_time,end_time"},
     *     @SWG\Property(
     *         property="shift_id",
     *         description="Shift ID. Null for Default Schedule."
     *     ),
     *     @SWG\Property(
     *         property="hours",
     *         type="string",
     *         description="Undertime hours in format HH:mm."
     *     ),
     *     @SWG\Property(
     *         property="start_time",
     *         type="string",
     *         description="Undertime request start time in format HH:mm."
     *     ),
     *     @SWG\Property(
     *         property="end_time",
     *         type="string",
     *         description="Undertime request end time in format HH:mm."
     *     )
     * )
     */
    public function create(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();
        $data['company_id'] = $user['employee_company_id'];
        $data['employee_id'] = $user['employee_id'];
        $data['user_id'] = $user['user_id'];
        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $undertimeRequest = $this->requestService->get($responseData['id']);
            $undertimeRequestData = json_decode($undertimeRequest->getData(), true);

            $details = [
                'new' => [
                    'employee_company_id' => $user['employee_company_id'],
                    'id' => $undertimeRequestData['id'],
                ],
            ];

            $item = new AuditCacheItem(
                UndertimeRequestAuditService::class,
                UndertimeRequestAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                $details
            );

            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/undertime_request/{id}",
     *     summary="Get Undertime Requests",
     *     description="Get Undertime Requests",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Undertime request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        ClockStateService $clockStateService,
        $id
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->get($id);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $undertimeRequest = json_decode($response->getData());
        $undertimeRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $undertimeRequest->company_id
            );

            $isAuthorized = ApprovalService::isEitherRequestorOrApproverNew(
                $user['employee_id'],
                $undertimeRequest,
                $user
            );
        } else {
            $workflows = $undertimeRequest->workflow_levels;
            $isAuthorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $undertimeRequest
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // fetch timesheet
        try {
            $shiftStart = collect($undertimeRequest->shifts)->pluck('start')->unique()->all();
            $shiftEnd = collect($undertimeRequest->shifts)->pluck('end')->unique()->all();

            $timeSheets = $clockStateService->getFormattedTimesheet(
                $undertimeRequest->employee_id,
                $shiftStart,
                $shiftEnd
            );
            $undertimeRequest->timesheet = isset($timeSheets['data']) ? $timeSheets['data'] : [];
        } catch (\Throwable $e) {
            \Log::error($e->getMessage() . ' : No timesheet data = ' . $undertimeRequest->employee_id);
            \Log::error($e->getTraceAsString());
        }

        return $this
            ->response
            ->array((array) $undertimeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
