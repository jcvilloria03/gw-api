<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\User\UserRequestService;
use Illuminate\Support\Facades\App;
use App\Account\AccountRequestService;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\DeviceManagement\DeviceManagementAuditService;
use App\DeviceManagement\DeviceManagementRequestService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class DeviceManagementController extends Controller
{
    /**
     * @var \App\DeviceManagement\DeviceManagementRequestService
     */
    private $requestService;

    /*
     * App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        DeviceManagementRequestService $requestService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/api/face_pass/ra08t/devices",
     *     summary="Get Account Devices",
     *     description="Get Account Devices",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[keyword]",
     *         in="query",
     *         description="Filter by device name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page (Defaults to 1)",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page (Defaults to 10)",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "current_page": 1,
     *                "data": {
     *                    {
     *                        "id": 1,
     *                        "device_name": "Test device",
     *                        "face_recognition_distance": 0,
     *                        "face_verify_threshold_seconds": 10,
     *                        "enable_facemask_check": true,
     *                        "enable_facemask_voice_prompt": true,
     *                        "facemask_voice_prompt_msg": "Show face",
     *                        "enable_temp_check": true,
     *                        "temp_threshold_degrees": 37.9,
     *                        "successful_entry_msg": "Good Day!",
     *                        "unauthorized_entry_msg": "Calling 911"
     *                    }
     *                },
     *                "from": 1,
     *                "last_page": 5,
     *                "per_page": 10,
     *                "to": 10,
     *                "total": 50
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid page": {
     *                  "message": "The Page must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum page": {
     *                  "message": "The Page must be at least 1.",
     *                  "status_code": 406
     *              },
     *              "Invalid per_page": {
     *                  "message": "The Per page must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum per_page": {
     *                  "message": "The per_page must be at least 1.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     * )
     */
    public function get(Request $request)
    {
        $this->validate(
            $request,
            [
                "filter.keyword" => "nullable|string",
                "page" => "nullable|integer|min:1",
                "per_page" => "nullable|integer|min:1"
            ]
        );

        $params = $request->only(['filter', 'page', 'per_page']);
        $accountId = $request->attributes->get('user')['account_id'];

        if (!$params['page']) {
            $params['page'] = 1;
        }

        if (!$params['per_page']) {
            $params['per_page'] = 10;
        }

        $deviceRequestService = App::make(DeviceManagementRequestService::class);

        return $deviceRequestService->getAccountDevice($accountId, $params);
    }

    /**
     * @SWG\Post(
     *     path="/api/internal/ra08t/devices",
     *     summary="Register device",
     *     description="Register device",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/RegisterDeviceRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "id": 1
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Account not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Device already registered": {
     *                  "Code": "NotAcceptableError",
     *                  "Message": "Device with serial number 'test' is already registered to an account."
     *              }
     *         }
     *     ),
     * ),
     *
     * @SWG\Definition(
     *     definition="RegisterDeviceRequest",
     *     required={
     *         "email",
     *          "device_serial_number",
     *          "device_password",
     *          "device_password_confirm",
     *          "cloud_domain",
     *          "device_name"
     *     },
     *     @SWG\Property(
     *         property="email",
     *         type="string",
     *         description="Primary owner email address"
     *     ),
     *     @SWG\Property(
     *         property="device_serial_number",
     *         type="string",
     *         description="Device serial number."
     *     ),
     *     @SWG\Property(
     *         property="device_password",
     *         type="string",
     *         description="Device password."
     *     ),
     *     @SWG\Property(
     *         property="device_password_confirmation",
     *         type="string",
     *         description="Device password confirmation."
     *     ),
     *     @SWG\Property(
     *         property="cloud_domain",
     *         type="string",
     *         description="Cloud domain URL"
     *     ),
     *     @SWG\Property(
     *         property="device_name",
     *         type="string",
     *         description="Account device name"
     *     )
     * )
     */
    public function store(
        Request $request,
        AccountRequestService $accountRequestService,
        DeviceManagementRequestService $deviceRequestService
    ) {
        $this->validate($request, [
            'email'                        => 'required|email',
            'device_serial_number'         => 'required',
            'device_password'              => 'required|confirmed',
            'device_password_confirmation' => 'required',
            'cloud_domain'                 => 'required|url',
            'device_name'                  => 'required'
        ]);

        // Get account
        $accountResponse = $accountRequestService->index([
            'primary_owner_email' => $request->get('email'),
            'includes'            => 'companies'
        ]);

        $account = json_decode($accountResponse->getData(), true);
        $account = $account['data'][0] ?? [];

        if (empty($account)) {
            throw new HttpException(500, 'Account data empty.');
        }

        $company = $account['companies'][0] ?? [];

        if (empty($company)) {
            throw new HttpException(500, 'Company data empty.');
        }

        $params = array_merge(
            [
                'primary_company_name' => $company['name']
            ],
            $request->only([
                'device_serial_number',
                'device_password',
                'cloud_domain',
                'device_name'
            ])
        );

        return $deviceRequestService->registerDevice($account['id'], $params);
    }

    /**
     * @SWG\Get(
     *     path="/api/face_pass/ra08t/users/{user_id}",
     *     summary="Get User Devices",
     *     description="Get User Devices",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "data": {
     *                    {
     *                        "user_id": 2,
     *                        "bundy_user_id": 3,
     *                        "device_id": 4,
     *                        "status": "PHOTO TAKEN"
     *                    }
     *                }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid ID": {
     *                  "message": "The ID must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum page": {
     *                  "message": "The ID must be at least 1.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     * )
     */
    public function userDevices($userId)
    {
        $validator = Validator::make(["user_id" => $userId], [
            "user_id" => "required|integer|min:1"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        $deviceRequestService = App::make(DeviceManagementRequestService::class);
        return $deviceRequestService->getUserDevice($userId);
    }

    /**
     * @SWG\Get(
     *     path="/api/internal/ra08t/devices",
     *     summary="Get Device",
     *     description="Get Devices",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="filter[serial_number]",
     *         in="query",
     *         description="Filter by serial number",
     *         type="string",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "current_page": 1,
     *                "data": {
     *                    {
     *                        "id": 1,
     *                        "device_name": "Test device",
     *                        "face_recognition_distance": 0,
     *                        "face_verify_threshold_seconds": 10,
     *                        "enable_facemask_check": true,
     *                        "enable_facemask_voice_prompt": true,
     *                        "facemask_voice_prompt_msg": "Show face",
     *                        "enable_temp_check": true,
     *                        "temp_threshold_degrees": 37.9,
     *                        "successful_entry_msg": "Good Day!",
     *                        "unauthorized_entry_msg": "Calling 911"
     *                    }
     *                },
     *                "from": 1,
     *                "last_page": 1,
     *                "per_page": 10,
     *                "to": 1,
     *                "total": 1
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid ID": {
     *                  "message": "The ID must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum page": {
     *                  "message": "The ID must be at least 1.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     * )
     */
    public function searchDeviceByFilter(Request $request, DeviceManagementRequestService $deviceRequestService)
    {
        $params = $request->only('filter');
        $filter = [];

        if ($params) {
            $filter = array_get($params, 'filter', []);
            if (is_null($filter) ) {
                $filter = [];
            }
        }

        $validator = Validator::make($filter, [
            "serial_number" => "required|string"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        return $deviceRequestService->searchDeviceByFilter($filter);
    }

    /**
     * @SWG\Put(
     *     path="/api/internal/ra08t/devices/{id}",
     *     summary="Update device",
     *     description="Update device",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Device Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UpdateDeviceRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "device_name": "Main Door",
     *                 "face_recognition_distance": 4,
     *                 "face_verify_threshold_seconds": 60,
     *                 "enable_facemask_check": 1,
     *                 "enable_facemask_voice_prompt": 1,
     *                 "facemask_voice_prompt_msg": "Please wear a mask",
     *                 "enable_temp_check": 1,
     *                 "temp_threshold_degrees": 37.9,
     *                 "successful_entry_msg": "Welcome",
     *                 "unauthorized_entry_msg": "Face not recognized"
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Device not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request."
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="UpdateDeviceRequest",
     *     required={
     *          "device_name",
     *          "face_recognition_distance",
     *          "face_verify_threshold_seconds",
     *          "enable_facemask_check",
     *          "enable_facemask_voice_prompt",
     *          "enable_temp_check",
     *          "temp_threshold_degrees",
     *          "successful_entry_msg",
     *          "unauthorized_entry_msg"
     *     },
     *     @SWG\Property(
     *         property="device_name",
     *         type="string",
     *         description="Account device name. AlphaNumeric and Max 50 characters."
     *     ),
     *     @SWG\Property(
     *         property="face_recognition_distance",
     *         type="integer",
     *         description="Face Recognition Distance. Possible Values: 0 - 4"
     *     ),
     *     @SWG\Property(
     *         property="face_verify_threshold_seconds",
     *         type="integer",
     *         description="Face Verification Threshold (seconds). Possible Values: 0 - 60"
     *     ),
     *     @SWG\Property(
     *         property="enable_facemask_check",
     *         type="boolean",
     *         description="Enable Face Mask Check"
     *     ),
     *     @SWG\Property(
     *         property="enable_facemask_voice_prompt",
     *         type="boolean",
     *         description="Enable Face Mask Voice Prompt"
     *     ),
     *     @SWG\Property(
     *         property="facemask_voice_prompt_msg",
     *         type="string",
     *         description="Face mask voice prompt message"
     *     ),
     *     @SWG\Property(
     *         property="enable_temp_check",
     *         type="boolean",
     *         description="Enable Temperature Check"
     *     ),
     *     @SWG\Property(
     *         property="temp_threshold_degrees",
     *         type="number",
     *         description="Temperature Threshold"
     *     ),
     *     @SWG\Property(
     *         property="successful_entry_msg",
     *         type="string",
     *         description="Display Message for Successful Entry"
     *     ),
     *     @SWG\Property(
     *         property="unauthorized_entry_msg",
     *         type="string",
     *         description="Warning message for unauthorized access"
     *     )
     * )
     */
    public function updateDevice(
        $id,
        Request $request,
        DeviceManagementRequestService $deviceRequestService
    ) {

        $validator = Validator::make(
            $request->all(),
            [
                'device_name'                   => 'required|max:50',
                'face_recognition_distance'     => 'required|integer|min:0|max:4',
                'face_verify_threshold_seconds' => 'required|integer|min:1|max:60',
                'enable_facemask_check'         => 'required|boolean',
                'enable_facemask_voice_prompt'  => 'required|boolean',
                'enable_temp_check'             => 'required|boolean',
                'temp_threshold_degrees'        => 'required|numeric|min:1',
                'successful_entry_msg'          => 'required',
                'unauthorized_entry_msg'        => 'required'
            ]
        );

        # Face-Mask-Voice-Prompt is required when Face-Mask-Voice-Prompt is enabled
        $validator->sometimes('facemask_voice_prompt_msg', 'required', function ($request) {
            return $request->get('enable_facemask_voice_prompt', false);
        });

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        $params = $request->only([
            "device_name",
            "face_recognition_distance",
            "face_verify_threshold_seconds",
            "enable_facemask_check",
            "enable_facemask_voice_prompt",
            "facemask_voice_prompt_msg",
            "enable_temp_check",
            "temp_threshold_degrees",
            "successful_entry_msg",
            "unauthorized_entry_msg"
        ]);

        $response = $deviceRequestService->updateDevice($id, $params);
        
        # Convert the response to Dingo format
        if ($response->getStatusCode() != Response::HTTP_OK) {
            $response->setData((object) [
                'status_code' => $response->getStatusCode(),
                'message' => $response->getData()->errors,
            ]);
        }
        
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/api/face_pass/ra08t/users/{user_id}/sync",
     *     summary="Sync Registered User to Other RA08T Devices",
     *     description="Sync existing User data and FaceID to other selected RA08T devices within the Account",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Device IDs",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "device_ids"={"type"="array",
     *                      "items"={"type"="integer"}
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation."
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid request params": {
     *                  "device_ids": {
     *                      "Device ids is required"
     *                  }
     *              },
     *              "Invalid User ID": {
     *                  "message": "User not found.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function syncFaceIdToDevices(Request $request, UserRequestService $userRequestService, $userId)
    {
        $userRequestService->get($userId);

        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['actor_id'] = $createdBy['user_id'];

        $response = $this->requestService->syncFaceIdToDevices($userId, $inputs);

        $details = [
            'new' => [
                'user_id' => $userId,
                'device_ids' => $inputs['device_ids'],
            ],
        ];

        $item = new AuditCacheItem(
            DeviceManagementAuditService::class,
            DeviceManagementAuditService::ACTION_SYNC,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );

        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/api/face_pass/ra08t/users",
     *     summary="Register user",
     *     description="Register device",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/RegisterUserRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "bundy_user_id": 1,
     *                "device_user_id": 1
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Unlinked device user not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Device already registered": {
     *                  "Code": "NotAcceptableError",
     *                  "Message": "User is already registered in the device."
     *              }
     *         }
     *     ),
     * ),
     *
     * @SWG\Definition(
     *     definition="RegisterUserRequest",
     *     required={
     *         "user_id",
     *         "unlinked_device_user_id"
     *     },
     *     @SWG\Property(
     *         property="user_id",
     *         type="integer",
     *         description="ID of user to be linked"
     *     ),
     *     @SWG\Property(
     *         property="unlinked_device_user_id",
     *         type="integer",
     *         description="ID of unlinked device user to be linked"
     *     )
     * )
     */
    public function registerUser(
        Request $request,
        UserRequestService $userRequestService,
        DeviceManagementRequestService $deviceRequestService
    ) {
        $this->validate(
            $request,
            [
                'user_id'                 => 'required|integer|min:1',
                'unlinked_device_user_id' => 'required|integer|min:1'
            ],
            [
                'integer' => 'Invalid :attribute.',
                'min'     => 'Invalid :attribute.',
            ]
        );

        // Get user details
        $userDataRes = $userRequestService->get($request->get('user_id'));
        $userData = json_decode($userDataRes->getData(), true);
        $employeeData = $userData['employee'] ?? [];

        // Check if same account id
        $accountId     = $request->attributes->get('user')['account_id'];
        $userAccountId = $userData['account_id'] ?? null;

        if ($accountId !== $userAccountId) {
            // Requestor and user does not belong to the same account
            throw new NotAcceptableHttpException('Invalid user id given.');
        }

        $name       = $employeeData['first_name'] ?? $userData['first_name'];
        $middleName = $employeeData['middle_name'] ?? $userData['middle_name'];
        $lastName   = $employeeData['last_name'] ?? $userData['last_name'];
        $name       .= (!empty($middleName)) ? " {$middleName}" : '';
        $name       .= " {$lastName}";

        return $deviceRequestService->registerUser(
            $request->get('user_id'),
            $request->get('unlinked_device_user_id'),
            $name
        );
    }

    /**
     * @SWG\Get(
     *     path="/api/face_pass/ra08t/accounts/unlinked_device_users",
     *     summary="Get Account unlinked device users",
     *     description="Get Account unlinked device users",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[query]",
     *         in="query",
     *         description="Device name or person name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page (Defaults to 1)",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page (Defaults to 10)",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                "first_page": 1,
     *                "current_page": 1,
     *                "last_page": 1,
     *                "from": 1,
     *                "to": 1,
     *                "total": 1,
     *                "data": {
     *                    {
     *                        "id": 1,
     *                        "device_name": "Test device",
     *                        "user_name": "Test Tester",
     *                        "s3_uri": "http://download.me",
     *                        "person_id": "Test",
     *                    }
     *                },
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid page": {
     *                  "message": "The Page must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum page": {
     *                  "message": "The Page must be at least 1.",
     *                  "status_code": 406
     *              },
     *              "Invalid per_page": {
     *                  "message": "The Per page must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Minimum per_page": {
     *                  "message": "The per_page must be at least 1.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     * )
     */
    public function getUnlinkedDeviceUsers(
        Request $request,
        DeviceManagementRequestService $requestService
    ) {
        $this->validate(
            $request,
            [
                'page'     => 'integer|min:1',
                'per_page' => 'integer|min:1'
            ]
        );

        $params = $request->only(['page', 'per_page']);
        $params = array_filter($params, function ($item) {
            return !empty($item);
        });

        $filters = $request->get('filter');
        if (is_array($filters) && !is_null($filters['query']) && trim($filters['query']) != '') {
            $params['filter']['query'] = $filters['query'];
        }

        $accountId = $request->attributes->get('user')['account_id'];

        return $requestService->getUnlinkedDeviceUsers($accountId, $params);
    }

    /**
     * @SWG\Delete(
     *     path="/api/face_pass/ra08t/users/{user_id}/devices",
     *     summary="Unregister user from the device",
     *     description="Unregister user from the device",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="user_id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UnregisterUserRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="User not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "device_ids": {
     *                  "Code": "NotAcceptableError",
     *                  "Message": "The device ids field is required."
     *              }
     *         }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="UnregisterUserRequest",
     *     required={
     *         "device_ids"
     *     },
     *     @SWG\Property(
     *         property="device_ids",
     *         type="array",
     *         description="Device IDs",
     *         items={"type"="integer"}
     *     )
     * )
     */
    public function unregisterUserDevices(
        Request $request,
        $userId
    ) {
        $params = $request->only(['device_ids']);

        $validator = Validator::make(
            array_merge(
                $params,
                [
                    'user_id' => $userId
                ]
            ),
            [
                'user_id'      => 'required|integer|min:1',
                'device_ids'   => 'required|array',
                'device_ids.*' => 'required|integer|min:1'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        $userRequestService = App::make(UserRequestService::class);

        // Get user details
        $userDataRes = $userRequestService->get($userId);
        $userData = json_decode($userDataRes->getData(), true);

        // Check if same account id
        $accountId     = $request->attributes->get('user')['account_id'];
        $userAccountId = $userData['account_id'] ?? null;

        if ($accountId !== $userAccountId) {
            // Requestor and user does not belong to the same account
            throw new NotAcceptableHttpException('Invalid user id given.');
        }

        $params['actor_id'] = $request->attributes->get('user')['user_id'];

        $deviceRequestService = App::make(DeviceManagementRequestService::class);

        return $deviceRequestService->unregisterUserDevice($userId, $params);
    }

    /**
     * @SWG\Delete(
     *     path="/api/face_pass/ra08t/accounts/unlinked_device_users",
     *     summary="Delete an unlinked User",
     *     description="Deletes an **Unlinked User** from the list of Unlinked Users in
Salarium V3 (by-api) and from the device it was originally registered in. An **Unlinked User** is
a **Face Record** in the **Face Database** of a registered RA08T device, but not yet linked to
a **User** record in Salarium V3.
**NOTE:** allows for only 1 Unlinked User to be deleted per request.",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/DeleteUnlinkedUserRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Unlinked Users": {
     *                  "message": "The user you are trying to delete doesn't exist from the list of unlinked users.",
     *                  "status_code": 406
     *              },
     *              "Unlinked Users do not exist in the Account": {
     *                  "message": "The user you are trying to delete doesn't exist within this account.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_TOO_MANY_REQUESTS,
     *         description="Invalid request.",
     *         examples={
     *              "You cannot delete more than 1 Unlinked User": {
     *                  "message": "您不能删除1个以上的用户。虽然这不应该发生。",
     *                  "status_code": 429
     *              }
     *         }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="DeleteUnlinkedUserRequest",
     *     required={
     *         "unlinked_user_ids"
     *     },
     *     @SWG\Property(
     *         property="unlinked_user_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         description="ID of Unlinked User"
     *     )
     * )
     */
    public function deleteUnlinkedDeviceUsers(
        Request $request,
        DeviceManagementRequestService $requestService
    ) {
        $this->validate(
            $request,
            [
                'unlinked_user_ids' => 'required|array',
                'unlinked_user_ids.*' => 'integer'
            ]
        );

        $accountId = $request->attributes->get('user')['account_id'];

        return $requestService->deleteUnlinkedDeviceUsers($accountId, $request->all());
    }

    /**
     * @SWG\Delete(
     *     path="/api/internal/ra08t/devices/{id}",
     *     summary="Unregister device",
     *     description="Unregister the device",
     *     tags={"device_management"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Device Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Device not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request."
     *     )
     * )
     */
    public function unregisterDevice($deviceId)
    {
        $validator = Validator::make(
            [
                'device_id' => $deviceId
            ],
            [
                'device_id' => 'required|integer|min:1'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        $deviceRequestService = App::make(DeviceManagementRequestService::class);

        return $deviceRequestService->unregisterDevice($deviceId);
    }

}
