<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\View\EmployeeViewRequestService;
use App\View\FilterViewRequestService;

class EmployeeViewsController extends Controller
{
    /*
     * \App\View\EmployeeViewRequestService
     */
    protected $employeeViewRequestService;

    /*
     * \App\View\FilterViewRequestService
     */
    protected $filterViewRequestService;

    public function __construct(
        EmployeeViewRequestService $employeeViewRequestService,
        FilterViewRequestService $filterViewRequestService
    ) {
        $this->employeeViewRequestService = $employeeViewRequestService;
        $this->filterViewRequestService = $filterViewRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/view/employee/{id}",
     *     summary="Get Employee View Settings",
     *     description="Get Employee View Settings",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee View ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "id": 1,
     *                  "name": "8 Hours Per Day, Hired since 2005",
     *                  "mode": "AND",
     *                  "columns": {
     *                      "employee_number",
     *                      "first_name",
     *                      "middle_name",
     *                      "last_name"
     *                  },
     *                  "filters": {
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "7",
     *                          "condition": ">"
     *                      },
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "9",
     *                          "condition": "<"
     *                      },
     *                      {
     *                          "field": "date_hired",
     *                          "value": "2005-12-31",
     *                          "condition": ">="
     *                      }
     *                  },
     *                  "sort": {
     *                      {
     *                          "field": "last_name",
     *                          "order": "asc"
     *                      }
     *                  },
     *                  "pagination": {
     *                       "page": 1,
     *                       "per_page": 20
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getEmployeeView($id)
    {
        $response = $this->employeeViewRequestService->get($id);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/view/employee/{id}/list",
     *     summary="List Employee View Items",
     *     description="List Employee View Items.
Enables setting of sorting and pagination options.
Option to save sorting and pagination options
",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee View ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="View Options",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "save"={"type"="boolean"},
     *                  "sort"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="object",
     *                          "properties"={
     *                              "field"={"type"="string"},
     *                              "order"={"type"="string"}
     *                          }
     *                      }
     *                  },
     *                  "pagination"={
     *                      "type"="object",
     *                      "properties"={
     *                          "page"={"type"="integer"},
     *                          "per_page"={"type"="integer"}
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                   "current_page": 4,
     *                   "data": {
     *                       {
     *                           "date_ended": null,
     *                           "hours_per_day": "8.00",
     *                           "employee_number": "20171108003110",
     *                           "last_name": "Kenneth",
     *                           "active": false,
     *                           "middle_name": "Jones",
     *                           "date_hired": "2016-12-29",
     *                           "first_name": "Brad"
     *                       },
     *                       {
     *                           "date_ended": null,
     *                           "hours_per_day": "8.00",
     *                           "employee_number": "20171108125330",
     *                           "last_name": "Kenneth",
     *                           "active": false,
     *                           "middle_name": "Jones",
     *                           "date_hired": "2016-12-29",
     *                           "first_name": "Brad"
     *                       }
     *                   },
     *                   "from": 76,
     *                   "last_page": 6,
     *                   "per_page": 25,
     *                   "to": 100,
     *                   "total": 139
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getEmployeeViewList($id, Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = $request->all();

        $response = $this->employeeViewRequestService->getItems($id, $attributes);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/view/employee/",
     *     summary="Create Employee View",
     *     description="Create Employee View",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="View Options",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "company_id"={"type"="integer"},
     *                  "name"={"type"="string"},
     *                  "mode"={"type"="string"},
     *                  "columns"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "filters"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="object",
     *                          "properties"={
     *                              "field"={"type"="string"},
     *                              "condition"={"type"="string"},
     *                              "value"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "id": 1,
     *                  "name": "8 Hours Per Day, Hired since 2005",
     *                  "mode": "AND",
     *                  "columns": {
     *                      "employee_number",
     *                      "first_name",
     *                      "middle_name",
     *                      "last_name"
     *                  },
     *                  "filters": {
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "7",
     *                          "condition": ">"
     *                      },
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "9",
     *                          "condition": "<"
     *                      },
     *                      {
     *                          "field": "date_hired",
     *                          "value": "2005-12-31",
     *                          "condition": ">="
     *                      }
     *                  },
     *                  "sort": {
     *                      {
     *                          "field": "last_name",
     *                          "order": "asc"
     *                      }
     *                  },
     *                  "pagination": {
     *                       "page": 1,
     *                       "per_page": 20
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function createEmployeeView(Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = $request->all();
        $attributes['user_id'] = $request->attributes->get('user')['user_id'];

        $response = $this->employeeViewRequestService->create($attributes);

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/view/employee/{id}",
     *     summary="Update Employee View",
     *     description="Update Employee View",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee View ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="View Options",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                  "name"={"type"="string"},
     *                  "mode"={"type"="string"},
     *                  "columns"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="string"
     *                      }
     *                  },
     *                  "filters"={
     *                      "type"="array",
     *                      "items"={
     *                          "type"="object",
     *                          "properties"={
     *                              "field"={"type"="string"},
     *                              "condition"={"type"="string"},
     *                              "value"={"type"="string"}
     *                          }
     *                      }
     *                  }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "id": 1,
     *                  "name": "8 Hours Per Day, Hired since 2005",
     *                  "mode": "AND",
     *                  "columns": {
     *                      "employee_number",
     *                      "first_name",
     *                      "middle_name",
     *                      "last_name"
     *                  },
     *                  "filters": {
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "7",
     *                          "condition": ">"
     *                      },
     *                      {
     *                          "field": "hours_per_day",
     *                          "value": "9",
     *                          "condition": "<"
     *                      },
     *                      {
     *                          "field": "date_hired",
     *                          "value": "2005-12-31",
     *                          "condition": ">="
     *                      }
     *                  },
     *                  "sort": {
     *                      {
     *                          "field": "last_name",
     *                          "order": "asc"
     *                      }
     *                  },
     *                  "pagination": {
     *                       "page": 1,
     *                       "per_page": 20
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function updateEmployeeView($id, Request $request)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $response = $this->employeeViewRequestService->update($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/view/employee/user",
     *     summary="Get User's Employee Views",
     *     description="Get User's Employee Views",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "name": "8 Hours Per Day, Hired since 2005",
     *                          "mode": "AND",
     *                          "columns": {
     *                              "employee_number",
     *                              "first_name",
     *                              "middle_name",
     *                              "last_name"
     *                          },
     *                          "filters": {
     *                              {
     *                                  "field": "hours_per_day",
     *                                  "value": "7",
     *                                  "condition": ">"
     *                              },
     *                              {
     *                                  "field": "hours_per_day",
     *                                  "value": "9",
     *                                  "condition": "<"
     *                              },
     *                              {
     *                                  "field": "date_hired",
     *                                  "value": "2005-12-31",
     *                                  "condition": ">="
     *                              }
     *                          },
     *                          "sort": {
     *                              {
     *                                  "field": "last_name",
     *                                  "order": "desc"
     *                              }
     *                          },
     *                          "pagination": {
     *                              "page": 1,
     *                              "per_page": 20
     *                          }
     *                      },
     *                      {
     *                          "id": 1,
     *                          "name": "At Least 5 Hours Per Day",
     *                          "mode": "AND",
     *                          "columns": {
     *                              "employee_number",
     *                              "first_name",
     *                              "middle_name",
     *                              "last_name"
     *                          },
     *                          "filters": {
     *                              {
     *                                  "field": "hours_per_day",
     *                                  "value": "4",
     *                                  "condition": ">"
     *                              }
     *                          },
     *                          "sort": {
     *                              {
     *                                  "field": "last_name",
     *                                  "order": "asc"
     *                              }
     *                          },
     *                          "pagination": {
     *                              "page": 1,
     *                              "per_page": 20
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="View not found",
     *     ),
     * )
     */
    public function getUserEmployeeViews(Request $request)
    {
        $userId = $request->attributes->get('user')['user_id'];

        $response = $this->employeeViewRequestService->getUserEmployeeViews($userId);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/view/employee/form_options",
     *     summary="Get Employee View form options",
     *     description="List of available filter columns and display columns in Employee View",
     *     tags={"view"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      "filters": {
     *                          {
     *                              "label": "Employee number",
     *                              "value": "employee_number",
     *                              "type": "keyword",
     *                          },
     *                          {
     *                              "label": "First name",
     *                              "value": "first_name",
     *                              "type": "keyword",
     *                          },
     *                          {
     *                              "label": "Middle name",
     *                              "value": "middle_name",
     *                              "type": "keyword",
     *                          },
     *                          {
     *                              "label": "Last name",
     *                              "value": "last_name",
     *                              "type": "keyword",
     *                          },
     *                          {
     *                              "label": "Date hired",
     *                              "value": "date_hired",
     *                              "type": "date",
     *                          },
     *                          {
     *                              "label": "Date ended",
     *                              "value": "date_ended",
     *                              "type": "date",
     *                          },
     *                          {
     *                              "label": "Hours per day",
     *                              "value": "hours_per_day",
     *                              "type": "number",
     *                          },
     *                          {
     *                              "label": "Active",
     *                              "value": "active",
     *                              "type": "boolean",
     *                          }
     *                      },
     *                      "filter_types": {
     *                          "keyword": {
     *                              {
     *                                   "label": "=",
     *                                   "value": "="
     *                              },
     *                              {
     *                                   "label": "Starts with",
     *                                   "value": "prefix"
     *                              }
     *                          },
     *                          "date": {
     *                              {
     *                                   "label": "=",
     *                                   "value": "="
     *                              },
     *                              {
     *                                   "label": ">",
     *                                   "value": ">"
     *                              },
     *                              {
     *                                   "label": ">=",
     *                                   "value": ">="
     *                              },
     *                              {
     *                                   "label": "<",
     *                                   "value": "<"
     *                              },
     *                              {
     *                                   "label": "<=",
     *                                   "value": "<="
     *                              }
     *                          },
     *                          "number": {
     *                              {
     *                                   "label": "=",
     *                                   "value": "="
     *                              },
     *                              {
     *                                   "label": ">",
     *                                   "value": ">"
     *                              },
     *                              {
     *                                   "label": ">=",
     *                                   "value": ">="
     *                              },
     *                              {
     *                                   "label": "<",
     *                                   "value": "<"
     *                              },
     *                              {
     *                                   "label": "<=",
     *                                   "value": "<="
     *                              }
     *                          },
     *                          "boolean": {
     *                              {
     *                                   "label": "True",
     *                                   "value": "true"
     *                              },
     *                              {
     *                                   "label": "False",
     *                                   "value": "false"
     *                              }
     *                          }
     *                      },
     *                      "columns": {
     *                          {
     *                               "label": "First name",
     *                               "value": "first_name",
     *                               "required": true
     *                          },
     *                          {
     *                               "label": "Last name",
     *                               "value": "last_name",
     *                               "required": true
     *                          },
     *                          {
     *                               "label": "Employee number",
     *                               "value": "employee_number",
     *                               "required": true
     *                          },
     *                          {
     *                               "label": "Middle name",
     *                               "value": "middle_name",
     *                               "required": false
     *                          },
     *                          {
     *                               "label": "Date hired",
     *                               "value": "date_hired",
     *                               "required": false
     *                          },
     *                          {
     *                               "label": "Date ended",
     *                               "value": "date_ended",
     *                               "required": false
     *                          },
     *                          {
     *                               "label": "Hours per day",
     *                               "value": "hours_per_day",
     *                               "required": false
     *                          },
     *                          {
     *                               "label": "Active",
     *                               "value": "active",
     *                               "required": false
     *                          }
     *                      },
     *                      "modes": {
     *                          {
     *                               "label": "Show results that matches ANY filter",
     *                               "value": "OR"
     *                          },
     *                          {
     *                               "label": "Show results that matches ALL filter",
     *                               "value": "AND"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getEmployeeViewFields()
    {
        return response()->json([
            'data' => [
               'filters' => json_decode($this->employeeViewRequestService->getFields()->getData(), true),
               'filter_types' => json_decode($this->filterViewRequestService->getTypes()->getData(), true),
               'columns' => json_decode($this->employeeViewRequestService->getColumns()->getData(), true),
               'modes' => json_decode($this->filterViewRequestService->getModes()->getData(), true),
            ]
        ]);
    }
}
