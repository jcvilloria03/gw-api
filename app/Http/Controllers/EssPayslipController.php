<?php

namespace App\Http\Controllers;

use App\Audit\AuditItem;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Payroll\PayslipAuditService;
use App\ESS\EssPayslipRequestService;
use App\Payslip\PayslipRequestService;
use App\Http\Controllers\EssBaseController;
use App\Payslip\EssPayslipAuthorizationService;

class EssPayslipController extends EssBaseController
{
    const ACTION_VIEW = 'view';

    const OBJECT_NAME = 'payslip';

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\ESS\EssPayslipRequestService
     */
    private $requestService;

    /**
     * @var \App\Payslip\PayslipRequestService
     */
    protected $payslipRequestService;

    /**
     * @var \App\Payslip\EssPayslipAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        EssPayslipRequestService $requestService,
        PayslipRequestService $payslipRequestService,
        AuditService $auditService,
        EssPayslipAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->payslipRequestService = $payslipRequestService;
        $this->auditService = $auditService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/payslip/{id}",
     *     summary="View Payslip",
     *     description="View payslip as logged in employee.
Marks the payslip as READ.
Provides a URI that automatically downloads the payslip.
Authorization Scope : **ess.view.payslip**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payslip Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="uri", type="string", description="Download URI string"),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "uri": "https://qwe-payslips-qwe.qwe-us-west-2.qwe.com/company_99/payslip_99.pdf?qwe=qwe"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function readPayslip(Request $request, int $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $responseData = $this->payslipRequestService->get($id);
        $payslip = json_decode($responseData->getData(), true);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $user['employee_id'] === $payslip['employee_id'];
        } else {
            $isAuthorized = $this->authorizationService->authorizeView($user, $payslip);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $responseData = $this->payslipRequestService->download($id, array_merge($request->all(), ['read' => true]));
        $responseData = json_decode($responseData->getData(), true);

        $details = [
            'new' => [
                'employee_company_id' => $user['employee_company_id'],
                'id' => $id,
            ],
        ];

        $item = new AuditCacheItem(
            PayslipAuditService::class,
            PayslipAuditService::ACTION_VIEW,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );

        $this->auditService->queue($item);

        return $responseData;
    }

    /**
     * @SWG\Get(
     *     path="/ess/payslips",
     *     summary="Get employee payslips",
     *     description="List all payslips for logged in employee.
The list of payslips is filtered by Year.
The list of payslips is sorted by the most recent payroll date.
Authorization Scope : **ess.view.payslip**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="year",
     *         in="query",
     *         description="Year",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="integer", description="Payroll Id"),
     *             @SWG\Property(property="payroll_date", type="date", description="Payroll Date")
     *         ),
     *         examples={
     *              "application/json": {
     *                  {
     *                      "id": 24,
     *                      "payroll_date": "2017-10-31"
     *                  },
     *                  {
     *                      "id": 25,
     *                      "payroll_date": "2017-10-30"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getPayslips(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeList($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getPayslips($user['employee_id'], $request->input('year'));
    }

    /**
     * @SWG\Get(
     *     path="/ess/payslips/unread",
     *     summary="Get unread payslips",
     *     description="Get all unread payslips for logged in employee.
    The list of payslips is sorted by the most recent payroll date.

    Authorization Scope : **ess.view.payslip**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="integer", description="Payroll Id"),
     *             @SWG\Property(property="payroll_date", type="date", description="Payroll Date"),
     *         ),
     *         examples={
     *              "application/json": {
     *                  {
     *                      "id": 24,
     *                      "payroll_date": "2017-10-31"
     *                  },
     *                  {
     *                      "id": 25,
     *                      "payroll_date": "2017-10-30"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getUnreadPayslips(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        // authorize
        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeList($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getUnreadPayslips($user['employee_id']);
    }
}
