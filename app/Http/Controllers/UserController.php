<?php

namespace App\Http\Controllers;

use App\Model\Role;
use Illuminate\Support\Arr;
use App\Audit\AuditUser;
use App\Model\Auth0User;
use App\Model\AuthnUser;
use App\Task\TaskService;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Role\UserRoleService;
use App\User\UserAuditService;
use App\Auth0\Auth0UserService;
use Illuminate\Validation\Rule;
use App\Role\DefaultRoleService;
use App\User\UserRequestService;
use App\Validator\RoleValidator;
use App\Validator\UserValidator;
use App\Model\ResetPasswordToken;
use App\Company\CompanyRequestService;
use App\User\UserAuthorizationService;
use App\CompanyUser\CompanyUserService;
use App\ProductSeat\ProductSeatService;
use App\Validator\ResetPasswordValidator;
use App\Validator\EssPasswordChangeValidator;
use Symfony\Component\HttpFoundation\Response;
use App\Notification\NotificationRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Authentication\AuthenticationRequestService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Auth0\Auth0ManagementService;
use App\Account\AccountRequestService;
use App\Http\Controllers\Concerns\ProcessesEmployeeActiveStatusUpdatesTrait;
use App\Response\CustomResponse;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\Role\RoleRequestService;
use App\Cache\FragmentedRedisCacheService;
use Illuminate\Support\Facades\Log;
use App\Traits\AuditTrailTrait;
use App\User\UserService;
use Illuminate\Support\Facades\App;
use App\ClockState\ClockStateService;
use App\AccountDeletion\AccountDeletionRequestService;
use App\Authn\AuthnService;
use Carbon\Carbon;


/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class UserController extends Controller
{
    use AuditTrailTrait;

    use ProcessesEmployeeActiveStatusUpdatesTrait;

    const CACHE_BUCKET_PREFIX = 'user-status';
    const CACHE_FIELD_PREFIX = 'id';

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\ProductSeat\ProductSeatService
     */
    protected $productSeatService;

    /**
     * @var \App\User\UserAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Auth0\Auth0UserService
     */
    private $auth0UserService;

    /**
     * @var \App\Authn\AuthnService
     */
    private $authnService;

    /**
     * @var \App\Authentication\AuthenticationRequestService
     */
    protected $authenticationRequestService;

    /**
     * @var \App\Auth0\Auth0ManagementService
     */
    private $auth0ManagementService;

    /**
     * @var \App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * @var \App\Role\UserRoleService
     */
    private $defaultRoleService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $subscriptionService;

    /**
     * @var \App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * @var \App\Validator\UserValidator
     */
    protected $userValidator;

    /**
     * @var \App\Cache\FragmentedRedisCacheService
     */
    protected $cacheService;

    /**
     * @var App\Role\RoleRequestService
     */
    protected $roleRequestService;

    /**
     * @var App\AccountDeletion\AccountDeletionRequestService
     */
    protected $accountDeletionRequestService;

    public function __construct(
        UserRequestService $requestService,
        UserRoleService $userRoleService,
        DefaultRoleService $defaultRoleService,
        UserAuthorizationService $authorizationService,
        Auth0UserService $auth0UserService,
        AuthenticationRequestService $authenticationRequestService,
        AuditService $auditService,
        Auth0ManagementService $auth0ManagementService,
        AccountRequestService $accountRequestService,
        SubscriptionsRequestService $subscriptionsService,
        UserValidator $userValidator,
        ProductSeatService $productSeatService,
        FragmentedRedisCacheService $cacheService,
        RoleRequestService $roleRequestService,
        AccountDeletionRequestService $accountDeletionRequestService,
        AuthnService $authnService
    ) {
        $this->requestService = $requestService;
        $this->userRoleService = $userRoleService;
        $this->defaultRoleService = $defaultRoleService;
        $this->authorizationService = $authorizationService;
        $this->auth0UserService = $auth0UserService;
        $this->authenticationRequestService = $authenticationRequestService;
        $this->auditService = $auditService;
        $this->auth0ManagementService = $auth0ManagementService;
        $this->accountRequestService = $accountRequestService;
        $this->subscriptionsService = $subscriptionsService;
        $this->userValidator = $userValidator;
        $this->productSeatService = $productSeatService;
        $this->cacheService = $cacheService;
        $this->roleRequestService = $roleRequestService;
        $this->accountDeletionRequestService = $accountDeletionRequestService;
        $this->authnService = $authnService;
    }

    /**
     * @SWG\Get(
     *     path="/user/{id}",
     *     summary="Get",
     *     description="Get user Details

Authorization Scope : **view.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        // call microservice
        $response = $this->requestService->get($id);
        $userData = json_decode($response->getData());

        // authorize
        $viewer = $request->attributes->get('user');

        $userResponse = $this->requestService->get($viewer['user_id']);
        $userResponseData = json_decode($userResponse->getData());

        $userCompanies = array_column($userData->companies, 'id');
        $viewerCompanies = array_column($userResponseData->companies, 'id');

        $isUserCompanyValid = array_intersect($viewerCompanies, $userCompanies);
        // Validate inclusive company of viewer vs the user
        if (!$isUserCompanyValid) {
            $this->response()->errorUnauthorized();
        }

        if (!$this->isAuthzEnabled($request)) {
            $authData = (object) [
                'account_id' => $userResponseData->account_id,
                'company_id' => $userResponseData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authData, $viewer);

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        // Use authz_role for CRBAC roles for now. This will be changed to "role" once
        // the old RBAC is completely removed.
        $userData->authz_role = $userData->role;

        $authnUser = $this->authnService->getUser($userData->id);

        $userData->email_verified = $authnUser ? $authnUser->isActive() : null;
        $userData->assigned_licenses_id = $userData->product_seats;

        $userRoles = $this->userRoleService->getUserRoles($userData->id, ['role']);

        $roleCollection = $userRoles->map(function ($role) {
            return $role['role'];
        });
        $userRoles = $roleCollection->toArray();

        $userData->role = [];
        $userData->admin_of = [];
        $userData->employee_of = [];

        if ($userRoles) {
            $userData->role = $userRoles;
            $userData->admin_of = $this->userRoleService->getCompanies(
                $userData,
                $userRoles,
                UserRoleService::ADMIN
            );
            $userData->employee_of = $this->userRoleService->getCompanies(
                $userData,
                $userRoles,
                UserRoleService::EMPLOYEE
            );
        }

        $response->setData($userData);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/auth/user/login/deprecated",
     *     summary="Login User",
     *     description="Login a registered User",
     *     tags={"user"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/userLogInItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(
     *                     property="user",
     *                     type="object",
     *                     @SWG\Property(property="id_token", type="string"),
     *                     @SWG\Property(property="sub", type="string"),
     *                     @SWG\Property(property="nickname", type="string"),
     *                     @SWG\Property(property="name", type="string"),
     *                     @SWG\Property(property="picture", type="string"),
     *                     @SWG\Property(property="updated_at", type="string"),
     *                     @SWG\Property(property="email", type="string"),
     *                     @SWG\Property(property="email_verified", type="boolean"),
     *                     @SWG\Property(property="https://salarium.com/account_name", type="string"),
     *                     @SWG\Property(property="https://salarium.com/fresh_account", type="string"),
     *                     @SWG\Property(property="https://salarium.com/initial_password", type="string"),
     *                     @SWG\Property(property="https://salarium.com/email_verified", type="boolean"),
     *                 ),
     *             ),
     *             @SWG\Property(
     *                 property="links",
     *                 type="string",
     *             ),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      "user": {
     *                          "id_token": "Bearer eyJz93a...k4laUWw",
     *                          "sub": "auth0|sadasd9das9das9dsa0d9",
     *                          "nickname": "test+testnickname",
     *                          "name": "test+testnickname@salarium.com",
     *                          "picture": "https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/16001568782/logo/salarium_logo.jpg",
     *                          "updated_at": "2019-01-14T10:19:55.493Z",
     *                          "email": "test+testnickname@salarium.com",
     *                          "email_verified": true,
     *                          "https://salarium.com/account_name": "Test Nickname",
     *                          "https://salarium.com/fresh_account": "false",
     *                          "https://salarium.com/initial_password": "",
     *                          "https://salarium.com/email_verified": true
     *                      }
     *                  },
     *                  "links": "/auth/user/login?None"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="BadRequestError",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="InternalServerError",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="userLogInItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(
     *             property="user",
     *             type="object",
     *             @SWG\Property(property="email", type="string"),
     *             @SWG\Property(property="password", type="string"),
     *         ),
     *     ),
     * )
     */
    public function login(Request $request)
    {
        $email = $request->input('data.user.email');

        if (!$email) {
            throw new HttpException(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                'The user email field is required.'
            );
        }

        // Check if user is active
        try {
            $user = $this->requestService->getUserByEmail($email, ['account_deletion']);
        } catch (\Throwable $e) {
            $errorMsg = $e->getMessage();

            if ($e->getStatusCode() === Response::HTTP_NOT_FOUND) {
                $errorMsg = $this->authenticationRequestService::INVALID_CREDENTIAL_MSG;
            }

            throw new HttpException($e->getStatusCode(), $errorMsg, $e);
        }

        $user = json_decode($user->getData(), true);

        if (!empty($user['account_deletion'])) {
            $accountDeletion = $user['account_deletion'];
            if ($accountDeletion['delete_at'] <= Carbon::now()->toDateTimeString() ||
                ($accountDeletion['status'] === 'deleting' || $accountDeletion['status'] === 'deleted')) {
                return response()
                ->json(
                    ['error_description' => CustomResponse::ACCOUNT_DELETION_RESTRICT_MSG],
                    Response::HTTP_NOT_ACCEPTABLE
                );
            }
        };

        $suspendedUserType = $this->isAccountSuspended($user);
        if ($suspendedUserType && $suspendedUserType === $this->roleRequestService::ROLE_EMPLOYEE) {
            return response()->json(
                ['error_description' => 'Your account is not currently active please contact your administrator.'],
                Response::HTTP_NOT_ACCEPTABLE
            );
        }

        if (empty($user['status']) || $user['status'] == Auth0User::STATUS_INACTIVE) {
            return response()
                ->json(
                    ['error_description' => ResetPasswordToken::INACTIVE_USER_REQUESTED],
                    Response::HTTP_NOT_ACCEPTABLE
                );
        }

        if ($user['status'] === Auth0User::STATUS_SEMI_ACTIVE) {
            $constraints['role'] = function ($query) {
                $query->where('type', UserRoleService::EMPLOYEE);
            };

            $employeeRole = $this->userRoleService->getUserRoles(
                $user['id'],
                ['role'],
                $constraints
            );

            $isEmployee = !$employeeRole->isEmpty();

            if ($isEmployee) {
                $userRole = $employeeRole->first();

                if (!empty($userRole->module_access)) {
                    $modules = $userRole->module_access;
                } else {
                    $modules = $userRole->role->module_access;
                }

                $isPayrollSubscribed = in_array(Role::ACCESS_MODULE_PAYROLL, $modules);

                if (!$isPayrollSubscribed) {
                    $errorResponse = response()->json([
                        'status_code' => CustomResponse::HTTP_ACCESS_REVOKE,
                        'action_code' => CustomResponse::ACTION_CODE_RESTRICT,
                        'message' => CustomResponse::SEMI_ACTIVE_RESTRICT_MSG
                    ], CustomResponse::HTTP_ACCESS_REVOKE);

                    return $errorResponse;
                }
            } else {
                $errorResponse = response()->json([
                    'status_code' => CustomResponse::HTTP_ACCESS_REVOKE,
                    'action_code' => CustomResponse::ACTION_CODE_RESTRICT,
                    'message' => CustomResponse::ADMIN_SEMI_ACTIVE_RESTRICT_MSG
                ], CustomResponse::HTTP_ACCESS_REVOKE);

                return $errorResponse;
            }
        }

        $response = $this->authenticationRequestService->login($request->all());

        if ($response->isSuccessful()) {
            $loginResponse = json_decode(json_encode($response->getData()), true);
            $userAdditionalInfo = null;

            try {
                $userRequestResult = $this->requestService->getUserDetails($user['id']);
                $userAdditionalInfo = json_decode($userRequestResult->getData(), true);
                $userAdditionalInfo['is_account_suspended'] = $suspendedUserType ? true : false;

                $loginResponse['data']['details'] = $userAdditionalInfo;
            } catch (\Throwable $e) {
                Log::error('LOGIN ERROR: ' . $e->getMessage());
                Log::error($e->getTraceAsString());
                throw new HttpException(500, 'Unexpected error encountered.');
            }

            try {
                if (!empty($userAdditionalInfo["employee_id"])) {
                    $clockStateService = App::make(ClockStateService::class);
                    $timeclockLastEntry = $clockStateService
                        ->getFormattedLastTimeClockEntry($userAdditionalInfo["employee_id"]);

                    if ($timeclockLastEntry) {
                        $loginResponse['data']['details']['timeclock_last_entry'] = $timeclockLastEntry;
                    }
                }
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }

            try {
                // trigger audit trail
                if ($userAdditionalInfo) {
                    $companyId = $userAdditionalInfo['last_active_company_id'] ?? $userAdditionalInfo['company_id'];

                    $request->attributes->add(['user' => [
                        'account_id' => $userAdditionalInfo['account_id'],
                        'employee_company_id' => $companyId,
                        'user_id' => $userAdditionalInfo['id']
                    ]]);

                    $this->audit($request, $companyId);
                }
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }

            return $loginResponse;
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/auth/user/verify_resend",
     *     summary="Send a 'verify email address' email",
     *     description="Send an email to the specified user that asks them to click a link to
verify their email address.",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(property="sent", type="boolean"),
     *             ),
     *             @SWG\Property(
     *                 property="links",
     *                 type="string",
     *             ),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      "sent": true,
     *                  },
     *                  "links": "/auth/user/verify_resend"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="BadRequestError",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="InternalServerError",
     *     )
     * )
     */
    public function verifyResend(Request $request)
    {
        $userId = $request->attributes->get('user')['user_id'];
        $auth0UserId = $this->auth0UserService->getAuth0UserId($userId);

        return $this->authenticationRequestService->verifyResend([
            'data' => [
                'user' => [
                    'auth0_user_id' => $auth0UserId,
                ]
            ]
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/account/users",
     *     summary="Get All Account Users",
     *     description="Get Details of Users in an Account
Authorization Scope : **view.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="filter[company_ids][]",
     *         in="query",
     *         description="Company IDs",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[user_status]",
     *         in="query",
     *         description="User Status",
     *         required=false,
     *         type="string",
     *         enum={"active","semi-active", "inactive"}
     *     ),
     *     @SWG\Parameter(
     *         name="filter[has_faceid]",
     *         in="query",
     *         description="User has face ID?",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[user_name_keyword]",
     *         in="query",
     *         description="Filter by name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target page. If not provided, return all rows",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page. Default to 50.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         examples={
     *             "paginated": {
     *                "current_page": 1,
     *                "data": {
     *                    {
     *                        "id": 1,
     *                        "account_id": 1,
     *                        "email": "user@salarium.com",
     *                        "user_type": "owner",
     *                        "first_name": "User First Name",
     *                        "middle_name": "User Second Name",
     *                        "last_name": "User Last Name",
     *                        "full_name": "User Full Name",
     *                        "last_active_company_id": 1,
     *                        "active": "active",
     *                        "with_registered_face": true
     *                    }
     *                },
     *                "from": 1,
     *                "last_page": 1,
     *                "per_page": 10,
     *                "to": 10,
     *                "total": 1
     *             },
     *             "all": {
     *                "data": {
     *                    {
     *                        "id": 1,
     *                        "account_id": 1,
     *                        "email": "user@salarium.com",
     *                        "user_type": "owner",
     *                        "first_name": "User First Name",
     *                        "middle_name": "User Second Name",
     *                        "last_name": "User Last Name",
     *                        "full_name": "User Full Name",
     *                        "last_active_company_id": 1,
     *                        "active": "active",
     *                        "with_registered_face": true
     *                    }
     *                }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string"),
     *              @SWG\Property(property="status_code", type="integer", example=406)
     *         ),
     *         examples={
     *              "Account id is required": {"message": "The account id field is required.", "status_code": 406},
     *              "Account id is invalid": {"message": "The selected account id is invalid.", "status_code": 406},
     *              "Company ids must be an integer": {
     *                  "message": "The company_ids.0 must be an integer",
     *                  "status_code": 406
     *              },
     *              "Company id is invalid": {
     *                  "message": "The selected company_ids.0 is invalid.",
     *                  "status_code": 406
     *              },
     *              "filter[status] is invalid": {
     *                  "message": "The selected filter[user status] is invalid.",
     *                  "status_code": 406
     *              },
     *              "filter[has_faceid] is invalid": {
     *                  "message": "The filter[has faceid] field must be true or false.",
     *                  "status_code": 406
     *              },
     *              "filter[user_name_keyword] is invalid": {
     *                   "message": "The filter[user name keyword] must be a string.",
     *                   "status_code": 406
     *              },
     *              "page is invalid": {
     *                   "message": "The page must be at least 1.",
     *                   "status_code": 406
     *              },
     *              "per_page is invalid": {
     *                   "message": "The per page must be at least 1.",
     *                   "status_code": 406
     *              }
     *         }
     *     )
     * )
     */
    public function getAccountUsers(Request $request)
    {
        $this->validate($request, [
            'filter.company_ids' => 'nullable|array',
            'filter.company_ids.*' => 'required|integer|min:1',
            'filter[user_status]' => 'nullable|string|in:' . implode(',', \App\Model\Auth0User::STATUSES),
            'filter.has_faceid' => 'nullable|in:true,false',
            'filter[user_name_keyword]' => 'nullable|string',
            'page' => 'nullable|integer|min:1',
            'per_page' => 'nullable|integer|min:1',
            'keyword' => 'nullable|string'
        ]);

        // call microservice
        $viewer = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->requestService->get($viewer['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authData, $viewer) ||
                (
                    $userData->user_type === UserRoleService::TYPE_OWNER ||
                    $userData->user_type === UserRoleService::TYPE_SUPER_ADMIN
                );

            if (!$isAuthorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        $params = $request->only([
            'filter.company_ids',
            'filter.user_status',
            'filter.user_name_keyword',
            'filter.has_faceid',
            'page',
            'per_page',
            'keyword'
        ]);

        # Remove parameters with null values
        $params = array_filter($params, function ($param) {
            return !is_null($param);
        });

        $companyIds = null;

        if (!empty($params['filter']['company_ids'])) {
            $companyIds = $params['filter']['company_ids'];

            # Remove the company IDS from the filter to avoid confusion
            unset($params['filter']['company_ids']);
        }

        if (!empty($params['filter']['user_status'])) {
            $userStatus = $params['filter']['user_status'];
            $params['filter[user_status]'] = $userStatus;
        }

        if (!is_null($params['filter']['user_name_keyword'])) {
            $userName = $params['filter']['user_name_keyword'];
            $params['filter[user_name_keyword]'] = $userName;
        }

        if (!empty($params['filter']['has_faceid'])) {
            $params['filter[has_faceid]'] = filter_var(
                $params['filter']['has_faceid'],
                FILTER_VALIDATE_BOOLEAN
            );
        }

        if (empty($params['per_page'])) {
            $params['per_page'] = 50;
        }

        unset($params['filter']);

        $response = $this->requestService->getAccountOrCompanyUsers(
            $viewer['account_id'],
            $companyIds,
            $params
        );

        $responseData = json_decode($response->getData());
        $accountUsers = $responseData->data;

        foreach ($accountUsers as &$user) {
            $userRoles = $this->userRoleService->getUserRoles($user->id, ['role']);

            $roleCollection = collect($userRoles)->map(function ($role) {
                return $role['role'];
            });
            $userRoles = $roleCollection->toArray();

            if (property_exists($user, 'role')) {
                $user->crbac_role = $user->role;
            }

            $user->role = [];
            $user->admin_of = [];
            $user->employee_of = [];

            if ($userRoles) {
                $user->role = $userRoles;
                $user->admin_of = $this->userRoleService->getCompanies(
                    $user,
                    $userRoles,
                    UserRoleService::ADMIN
                );
                $user->employee_of = $this->userRoleService->getCompanies(
                    $user,
                    $userRoles,
                    UserRoleService::EMPLOYEE
                );
            }

            $user->status = $user->active ? AuthnUser::STATUS_ACTIVE : AuthnUser::STATUS_INACTIVE;
            $user->assigned_licenses_id = $user->product_seats;
        }

        // add user roles (for now assume 1 role per user)
        $responseData->data = $accountUsers;
        $response->setData($responseData);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/user",
     *     summary="Create User",
     *     description="Create New User
    Authorization Scope : **create.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="user",
     *         in="body",
     *         description="User data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/createUserRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="createUserRequestItem",
     *     required={
     *         "user_type", "account_id", "first_name", "last_name", "email",
     *         "assigned_product_seats_ids", "role_id"
     *     },
     *     @SWG\Property(
     *         property="user_type",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="account_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="first_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="middle_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="last_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="email",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="assigned_product_seats_ids",
     *         type="array",
     *         items={"type"="integer"}
     *     ),
     *     @SWG\Property(
     *         property="is_company_level",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="role_id",
     *         type="integer"
     *     ),
     * )
     */
    public function create(
        Request $request,
        CompanyUserService $companyUserService,
        SubscriptionsRequestService $subscriptionsRequestService,
        ProductSeatService $productSeatService
    ) {
        $bearerToken = $request->headers->get('Authorization');
        $inputs = $request->all();
        $inputs['bearer'] = $bearerToken;

        // validate permissions related requests
        $createdBy = $request->attributes->get('user');

        $userResponse = $this->requestService->get($createdBy['user_id']);
        $userData = json_decode($userResponse->getData());

        // TODO: differentiate between company level and account level user creation
        // To be fixed in https://code.salarium.com/salarium/development/t-and-a-manila/issues/5872

        if (!$this->isAuthzEnabled($request)) {
            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        // Remove HRIS assigned product seats
        $hrisProductSeat = array_search(1, $inputs['assigned_product_seats_ids']);
        if (!empty($hrisProductSeat)) {
            unset($inputs['assigned_product_seats_ids'][$hrisProductSeat]);
        }

        $errors = [];

        if ($errors) {
            return $this->invalidRequestError(current($errors));
        }

        // call microservice
        $response = $this->requestService->create($inputs);

        if (!$response->isSuccessful()) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        $inputs['account_id'];

        $userId = $responseData['id'];

        // Sync company users
        $companyUserService->sync($userId);

        $userResponse = $this->requestService->get($responseData['id']);
        $userData = json_decode($userResponse->getData(), true);
        $response->setData(
            array_merge(
                $responseData,
                [
                    'status' => $responseData['active'],
                    'assigned_licenses_id' => $userData['product_seats']
                ]
            )
        );

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId($userData['account_id']);
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);
        $subscription = $customer['subscriptions'][0];

        // Subscription License Assigning
        if (!empty($subscription)) {
            $productSeatName = $productSeatService
            ->getProductSeatName($userData['product_seats'][0]['id']);
            // Get product name mapping from SB-API
            $productResponse = json_decode($subscriptionsRequestService
            ->getSubscriptionProductId($productSeatName)->getData(), true);
            $productId = $productResponse['data'][0]['id'] ?? '';
            $jobId = uniqid();
            if (!empty($productId)) {
                // Create subscription license entry
                $subscriptionsRequestService->assign(
                    $subscription['user_id'],
                    [
                        [
                            "entity_id" => $userId,
                            "product_id" => $productId,
                        ]
                    ],
                    $jobId
                );
                // Apply licenses to CP-API and GW-API
                $subscriptionsRequestService->applyLicenses(
                    [(string) $userId],
                    $productSeatName
                );
            }
        }

        $authnService = app()->make(AuthnService::class);
        $authnService->createAuthnUser([
            'user_id' => $responseData['id'],
            'account_id' => $responseData['account_id'],
            'authn_user_id' => $responseData['authn_user_id'],
            'status' => AuthnUser::STATUS_UNVERIFIED
        ]);

        // audit log
        $this->audit($request, $userData['companies'][0]['id'], $userData);

        unset($userData['account']);
        return $userData;
    }

    /**
     * @SWG\Post(
     *     path="/user/subscribe",
     *     summary="Subscribe User to Product Seat",
     *     description="Subscribe User to Product Seat
Authorization Scope : **create.user**, **edit.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Subscribe User to Product Seat",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "user_id"={"type"="integer"},
     *                 "product_seat_id"={"type"="integer"},
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function subscribe(Request $request)
    {
        $inputs = $request->all();

        // validate permissions related requests
        $createdBy = $request->attributes->get('user');
        $userId = $createdBy['user_id'];

        $userResponse = $this->requestService->get($userId);
        $userData = json_decode($userResponse->getData());

        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            )
            &&
            !$this->authorizationService->authorizeUpdate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->subscribe($inputs);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/user/{id}",
     *     summary="Delete user",
     *     description="Delete user
Authorization Scope : **delete.user**",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="User not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function delete($id, Request $request)
    {
        $deletedBy = $request->attributes->get('user');

        // get original user details
        $userResponse = $this->requestService->get($id);
        $userData = json_decode($userResponse->getData(), true);

        // don't allow deleting self
        if ($deletedBy['user_id'] === $userData['id']) {
            return $this->invalidRequestError('You cannot delete yourself.');
        }

        $user = $this->auth0UserService->getUser($id);

        // don't allow deleting active user
        if ($user->status === Auth0User::STATUS_ACTIVE) {
            return $this->invalidRequestError('You cannot delete active user.');
        }

        // don't allow deleting of first OWNER
        $accountResponse = $this->accountRequestService->get($deletedBy['account_id']);
        $account = json_decode($accountResponse->getData(), true);

        if (
            $userData['email'] === $account['email']
        ) {
            return $this->invalidRequestError('You cannot delete first owner.');
        }

        $auth0userId = $this->auth0UserService->getAuth0UserId($id);

        // call microservice
        $response = $this->requestService->delete($id);

        // delete user's roles
        $this->auth0UserService->deleteUser($userData['id']);
        $this->userRoleService->deleteUserRoles($userData['id']);

        // delete user from auth0
        try {
            $this->auth0ManagementService->deleteUser($auth0userId);
        } catch (\Throwable $e) {
            \Log::info('Could not delete user from auth0 : ' . $e->getMessage());
        }

        // audit log
        // log user deletion
        $details = [
            'old' => $userData,
        ];
        $item = new AuditCacheItem(
            UserAuditService::class,
            UserAuditService::ACTION_DELETE,
            new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/user/bulk_delete",
     *     summary="Delete multiple users",
     *     description="Delete multiple users,
    Authorization Scope : **delete.user**",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="user_ids[]",
     *         type="array",
     *         in="formData",
     *         description="User ids",
     *         required=true,
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request, AccountRequestService $accountRequestService)
    {
        $inputs = $request->all();

        $deletedBy = $request->attributes->get('user');
        $authorizedIds = [];

        // authorize
        $accountUsersResponse = $this->requestService->getAccountOrCompanyUsers($deletedBy['account_id']);
        $accountUsers = json_decode($accountUsersResponse->getData());

        // get only users which need to be deleted
        $allUsersForDelete = collect($accountUsers->data)->whereIn('id', $inputs['user_ids'])->all();

        foreach ($allUsersForDelete as $userForDelete) {
            $user = (object) [
                'id' => $userForDelete->id,
                'account_id' => $userForDelete->account_id
            ];

            if ($this->authorizationService->authorizeDelete($user, $deletedBy)) {
                $authorizedIds[] = $user->id;
            }
        }

        $unauthorizedUserIds = array_diff(
            array_values($request->get('user_ids')),
            array_values($authorizedIds)
        );

        if (!empty($unauthorizedUserIds)) {
            $this->response()->errorUnauthorized('Not authorized to delete all specified users');
        }

        // get original user details
        $userResponse = $this->requestService->get($deletedBy['user_id']);
        $deletedByUserData = json_decode($userResponse->getData(), true);

        $auth0Users = $this->auth0UserService->getUsersByIds(array_pluck($allUsersForDelete, 'id'));

        foreach ($allUsersForDelete as $userForDelete) {
            // don't allow deleting self
            if ($deletedBy['user_id'] === $userForDelete->id) {
                return $this->invalidRequestError('You cannot delete yourself.');
            }

            $auth0User = $auth0Users->where('user_id', $userForDelete->id)->first();

            // don't allow deleting active user
            if ($auth0User->status === Auth0User::STATUS_ACTIVE) {
                return $this->invalidRequestError('You cannot delete active user.');
            }

            // don't allow deleting owner if you are not owner
            if (
                $userForDelete->user_type === UserRoleService::TYPE_OWNER &&
                UserRoleService::TYPE_OWNER !== $deletedByUserData['user_type']
            ) {
                return $this->invalidRequestError('You do not have permission to delete owner.');
            }

            // don't allow deleting of first OWNER
            $accountResponse = $accountRequestService->get($deletedBy['account_id']);
            $account = json_decode($accountResponse->getData(), true);

            if (
                $userForDelete->email === $account['email']
            ) {
                return $this->invalidRequestError('You cannot delete first owner.');
            }
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            foreach ($allUsersForDelete as $userForDelete) {
                $auth0user = $auth0Users->where('user_id', $userForDelete->id)->first();

                // delete user's roles
                $this->auth0UserService->deleteUser($userForDelete->id);
                $this->userRoleService->deleteUserRoles($userForDelete->id);

                // delete user from auth0
                if ($auth0user) {
                    try {
                        $this->auth0ManagementService->deleteUser($auth0user->auth0_user_id);
                    } catch (\Throwable $e) {
                        \Log::info('Could not delete user from auth0 : ' . $e->getMessage());
                    }
                }
            }

            // audit log
            foreach ($request->input('user_ids', []) as $id) {
                $item = new AuditCacheItem(
                    UserAuditService::class,
                    UserAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'account_id' => $request->get('account_id'),
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/user/is_email_available",
     *     summary="Is email available",
     *     description="Checks availability of user email",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isEmailAvailable(Request $request)
    {
        return $this->requestService->isEmailAvailable($request->all());
    }

    /**
    * @SWG\Post(
    *     path="/user/check_in_use",
    *     summary="Are Users in use",
    *     description="Check are Users in use for given ids
Authorization Scope : **view.user**",
    *     tags={"user"},
    *     consumes={"application/x-www-form-urlencoded"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="user_ids[]",
    *         type="array",
    *         in="formData",
    *         description="User Ids",
    *         required=true,
    *         @SWG\Items(type="integer"),
    *         collectionFormat="multi"
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Successful operation",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized",
    *     ),
    *     @SWG\Response(
    *         response=404,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=406,
    *         description="Invalid request",
    *     )
    * )
    */
    public function checkInUse(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];

        $this->validate($request, [
            'user_ids.*' => [
                'required',
                'exists:auth0_users,user_id',
            ],
            'user_ids' => 'required|array'
        ], [
            'user_ids.*.exists' => 'Given User IDs contain invalid ID.'
        ], [
            'user_ids.*' => 'Role Ids'
        ]);

        // authorize
        $userId = $request->attributes->get('user')['user_id'];
        if (!$this->authorizationService->authorizeGet($accountId, $userId)) {
            $this->response()->errorUnauthorized();
        }

        $usersProfiles = $this->authnService->getActiveUserIds($accountId, $request->get('user_ids'));

        $countInUse = collect($usersProfiles)->filter(function ($profile) {
            return !empty($profile['status']) && $profile['status'] == AuthnUser::STATUS_ACTIVE;
        })->count();

        return [
            'in_use' => $countInUse
        ];
    }

    /**
     * @SWG\Post(
     *     path="/user/informations",
     *     summary="Get Current user informations",
     *     description="Get informations for user that sent request
Authorization Scope : **view.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getUserInformations(Request $request)
    {
        $userId = $request->attributes->get('user')['user_id'];

        $response = $this->requestService->getUserInformations($userId);
        $responseData = json_decode($response->getData(), true);

        $response->setData($responseData);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/users",
     *     summary="Get All Company Users",
     *     description="Get Details of Users
    Authorization Scope : **view.user**",
     *     tags={"user"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              ref="#/definitions/AccountUser"
     *         )
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string"),
     *              @SWG\Property(property="status_code", type="integer", example=406)
     *         ),
     *         examples={
     *              "Account id is required": {"message": "The account id field is required.", "status_code": 406},
     *              "Account id is invalid": {"message": "The selected account id is invalid.", "status_code": 406},
     *              "Company id must be an integer": {
     *                  "message": "The company_ids.0 must be an integer",
     *                  "status_code": 406
     *              },
     *              "Company id is invalid": {"message": "The selected company_ids.0 is invalid.", "status_code": 406},
     *              "Type is invalid": {"message": "The selected types is invalid.", "status_code": 406},
     *         }
     *     )
     * )
     */
    public function getAllCompanyUsers(Request $request, $id)
    {
        $this->validate($request, [
            'page' => 'nullable|integer|min:1',
            'per_page' => 'nullable|integer|min:1',
            'keyword' => 'nullable|string'
        ]);

        // call microservice
        $viewer = $request->attributes->get('user');

        $userResponse = $this->requestService->get($viewer['user_id']);
        $userResponse = json_decode($userResponse->getData());
        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            // TODO support array of company ids, because user can have more then one company
            // for now, it will take only first company id from array
            $authData = (object) [
                'account_id' => $userResponse->account_id,
                'company_id' => $userResponse->companies[0]->id,
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($authData, $viewer) ||
            $this->authorizationService->authorizeUserType($userResponse, $id);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // copying the code from getAccountUsers to avoid complications
        $params = $request->only([
            'page',
            'per_page',
            'keyword'
        ]);

        // Remove parameters with null values
        $params = array_filter($params, function ($param) {
            return !is_null($param);
        });

        if (empty($params['per_page'])) {
            $params['per_page'] = 50;
        }

        $response = $this->requestService->getAccountOrCompanyUsers(
            $viewer['account_id'],
            [$id],
            $params
        );

        $responseData = json_decode($response->getData(), true);
        $userIds = array_column($responseData['data'], 'id');
        if (!empty($userIds)) {
            $licensesResponse = $this->subscriptionsService->getAllLicensesByUsers($userIds);
            $licensesData = json_decode($licensesResponse->getData(), true)['data'];

            $responseData['data'] = array_map(function ($user) use ($licensesData) {
                $user['assigned_licenses_id'] = $user['product_seats'];
                $user['license'] = $licensesData[$user['id']] ?? null;
                $user['status'] = $user['active'] ? Auth0User::STATUS_ACTIVE : Auth0User::STATUS_INACTIVE;

                return $user;
            }, $responseData['data']);
        }

        return $responseData;
    }

    /**
     * @SWG\Patch(
     *     path="/user/{id}",
     *     summary="Update User",
     *     description="Update User
    Authorization Scope : **edit.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="user",
     *         in="body",
     *         description="User data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/updateUserRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="updateUserRequestItem",
     *     required={
     *         "user_type", "account_id", "first_name", "last_name", "email",
     *         "assigned_product_seats_ids"
     *     },
     *     @SWG\Property(
     *         property="user_type",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="account_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="first_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="middle_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="last_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="email",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="assigned_product_seats_ids",
     *         type="array",
     *         items={"type"="integer"}
     *     ),
     *     @SWG\Property(
     *         property="license_unit_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="subscription_product_id",
     *         type="integer"
     *     )
     * )
     */
    public function update(
        CompanyUserService $companyUserService,
        Request $request,
        $id
    ) {
        $bearerToken = $request->headers->get('Authorization');
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['bearer'] = $bearerToken;

        // get old user
        $userResponse = $this->requestService->get($id);
        $user = json_decode($userResponse->getData(), true);

        $isAuthorized = false;
        $isEmailUpdate = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                array_column($user['companies'], 'id')
            );
        } else {
            $userResponse = $this->requestService->get($updatedBy['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Remove HRIS assigned product seats
        $hrisProductSeat = array_search(1, $inputs['assigned_product_seats_ids']);
        if (!empty($hrisProductSeat)) {
            unset($inputs['assigned_product_seats_ids'][$hrisProductSeat]);
        }

        // update user
        $response = $this->requestService->update($id, $inputs);
        $responseData = json_decode($response->getData(), true);

        if (!$response->isSuccessful()) {
            return $response;
        }

        // Allow updating email of first OWNER
        $accountResponse = $this->accountRequestService->get($updatedBy['account_id']);
        $account = json_decode($accountResponse->getData(), true);

        if ($user['email'] === $account['email'] && $account['email'] !== $inputs['email']) {
            $this->accountRequestService->updateAccount($updatedBy['account_id'], $responseData);

            $subscriptionsRequestService = app()->make(SubscriptionsRequestService::class);
            $customerData = $subscriptionsRequestService->searchCustomerDetailsByUserId($updatedBy['user_id']);
            $customerData = json_decode($customerData->getData(), true);
            $customerData = $customerData['data'][0] ?? null;

            if (!empty($customerData)) {
                $subscriptionsRequestService->updateCustomer($customerData['id'], $responseData);
            }
            $isEmailUpdate = true;
        }

        $userUpdated = $responseData;

        $userUpdated['assigned_licenses_id'] = $userUpdated['product_seats'];
        $userUpdated['update_email'] = $isEmailUpdate;

        // Invalidate user's cached permissions
        // but let rebuild happend on updated user's next request
        $this->authorizationService->invalidateCachedPermissions($id);

        // Sync company users
        $companyUserService->sync($id);

        return $userUpdated;
    }

    /**
     * @SWG\Patch(
     *     path="/user/{id}/set_status",
     *     summary="Update User's status",
     *     description="Update User's status
    Authorization Scope : **edit.user**",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="status",
     *         in="formData",
     *         description="Status of user",
     *         type="string",
     *         enum={"active","semi-active","inactive"},
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function setStatus(
        $id,
        Request $request,
        AccountRequestService $accountRequestService,
        RoleRequestService $roleRequestService
    ) {
        // validate permissions related requests
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $user = $this->authnService->getUser($id);

        if (!$user) {
            $this->notFoundError('User not found.');
        }

        // don't allow inactivating of first OWNER
        $userResponse = $this->requestService->get($id);
        $userData = json_decode($userResponse->getData(), true);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        $companyId = Arr::get(current($userData['companies']), 'id', null);

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $accountResponse = $accountRequestService->get($updatedBy['account_id']);
        $account = json_decode($accountResponse->getData(), true);

        if (
            $userData['email'] === $account['email']
        ) {
            return $this->invalidRequestError('You cannot deactivate first owner.');
        }

        $accountId = $user['account_id'];
        $activeUserIds = $this->authnService->getActiveUserIds($accountId);

        // call microservice to validate new status
        $isValidStatusResponse = $this->requestService->validateStatus(
            $id,
            array_merge(
                $inputs,
                ['active_user_ids' => $activeUserIds]
            )
        );

        if (!$isValidStatusResponse->isSuccessful()) {
            return $isValidStatusResponse;
        }

        $updatedByResponse = $this->requestService->get($updatedBy['user_id']);
        $updatedByUser = json_decode($updatedByResponse->getData(), true);

        $userRole = $roleRequestService->getRoleByAccount(
            $userData['account_id'],
            $userData['role']['id'],
            true
        );

        $updatedByUserRole = $roleRequestService->getRoleByAccount(
            $updatedByUser['account_id'],
            $updatedByUser['role']['id'],
            true
        );

        $userRoleType = $roleRequestService->getRoleType($userRole['data']);
        $updatedByUserRoleType = $roleRequestService->getRoleType($updatedByUserRole['data']);

        if ($userRoleType === RoleRequestService::ROLE_OWNER
            && $updatedByUserRoleType !== RoleRequestService::ROLE_OWNER) {
            return $this->invalidRequestError('You cannot update status of owner.');
        }

        if ($userRoleType === RoleRequestService::ROLE_SUPER_ADMIN
            && ($updatedByUserRoleType !== RoleRequestService::ROLE_OWNER
                && $updatedByUserRoleType !== RoleRequestService::ROLE_SUPER_ADMIN)) {
            return $this->invalidRequestError('You cannot update status of super admin.');
        }

        if ($userRoleType === RoleRequestService::ROLE_ADMIN
            && ($updatedByUserRoleType !== RoleRequestService::ROLE_OWNER
                && $updatedByUserRoleType !== RoleRequestService::ROLE_SUPER_ADMIN
                && $updatedByUserRoleType !== RoleRequestService::ROLE_ADMIN)) {
            return $this->invalidRequestError('You cannot update status of admin.');
        }

        // update user status
        $response = $this->requestService->setStatus($id, $inputs['status']);

        if (!$response->isSuccessful()) {
            return $response;
        }
        $userUpdated = $user->fresh()->toArray();

        return $userUpdated;
    }

    /**
     * @SWG\Patch(
     *     path="/user/last_active_company",
     *     summary="Update Last Active Company Id",
     *     description="Update last active company id

Authorization Scope : **edit.user**",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="company_id",
     *         description="Company Id",
     *         in="formData",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function setLastActiveCompany(Request $request)
    {
        $userId = $request->attributes->get('user')['user_id'];

        if (!$this->isAuthzEnabled($request)) {
            $roles = $this->userRoleService->getUserRoles($userId, ['role']);

            $roleNames = collect($roles)->map(function ($role) {
                return $role['role'];
            });

            $roleNames = $roleNames->toArray();
            $isOwnerOrSuperAdmin = $this->userRoleService
                ->isOwnerOrSuperAdmin($roleNames);
            $isAdmin = $this->userRoleService->isUserAdmin($roleNames);

            if (empty($isOwnerOrSuperAdmin) && empty($isAdmin)) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->setLastActiveCompany(
            $userId,
            $request->input('company_id')
        );
    }

    /**
     * @SWG\Post(
     *     path="/user/{id}/resend_verification",
     *     summary="Resend user's verification code.",
     *     description="Resend user's verification code

Authorization Scope : **create.user** or **edit.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
     *         description="User's account ID does not match requester's.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="User not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function resendVerificationEmail(Request $request, $id)
    {
        $requester = $request->attributes->get('user');
        $authnService = app()->make(AuthnService::class);
        $user = $authnService->getUser($id);

        // Validate request
        if (empty($user)) {
            $this->response()->errorNotFound();
        }

        // Authorize request
        $userResponse = $this->requestService->get($user->user_id);
        $userData = json_decode($userResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                array_column($userData->companies, 'id')
            );
        } else {
            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($authData, $requester)
            || $this->authorizationService->authorizeUpdate($authData, $requester);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (strtolower($user->status) == 'inactive') {
            return response()
            ->json(
                [
                    'message' => "Unable to resend verification. User is inactive."
                ],
                Response::HTTP_NOT_ACCEPTABLE
            );
        }

            // Send request to Authn API
        return $authnService->resendVerification(
            $user->authn_user_id,
            $request->headers->get('Authorization')
        );
    }

    /**
     * @SWG\Get(
     *     path="/bundy/auth_user/{id}/status",
     *     summary="Get an employee's auth0 user status.",
     *     description="Get an employee's auth0 user status. This is only used by the bundy device to determine

Authorization Scope : **view.user**",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
     *         description="User's account ID does not match requester's.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="User not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getBundyEmployeeStatus($id, AuthnService $authnService)
    {
        $authnUser = $authnService->getUserByEmployeeId($id);
        $isActive = !empty($authnUser) && $authnUser->isActive() ? true : false;

        return [
            "active" => $isActive
        ];
    }

    /**
     * @SWG\Post(
     *     path="/auth/reset_password_request",
     *     summary="Generate reset password request.",
     *     description="Request password reset",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="user",
     *         in="body",
     *         description="User data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/resetPasswordRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="User with given email doesn't exist",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Validation failed",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="resetPasswordRequestItem",
     *     required={
     *        "email"
     *     },
     *     @SWG\Property(
     *         property="email",
     *         type="string"
     *     )
     * )
     */
    public function resetPasswordRequest(
        Request $request,
        NotificationRequestService $notificationService
    ) {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = $this->requestService->getUserByEmail($request->get('email'));
        $user = json_decode($user->getData(), true);

        if (empty($user['status']) || $user['status'] != 'active') {
            $this->invalidRequestError(ResetPasswordToken::INACTIVE_USER_REQUESTED);
        }

        $auth0User = $this->auth0UserService->getUser($user['id']);

        if ($auth0User && !Auth0UserService::isPreactive($auth0User)) {
            $this->auth0UserService->deleteExistingResetPasswordTokens($auth0User->user_id);
            $resetPasswordToken = $this->auth0UserService->generateResetPasswordToken($auth0User->user_id);

            $notificationService->sendResetPasswordNotification(
                $request->get('email'),
                $resetPasswordToken->token
            );
        }

        return $this->response->array([
            'message' => ResetPasswordToken::SUCCESS_MESSAGE
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/auth/reset_password",
     *     summary="Change password from reset password request.",
     *     description="Change password",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="reset_request",
     *         in="body",
     *         description="Reset password data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/resetPasswordItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="resetPasswordItem",
     *     required={
     *        "token","new_password","new_password_confirmation"
     *     },
     *     @SWG\Property(
     *         property="token",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="new_password",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="new_password_confirmation",
     *         type="string"
     *     )
     * )
     */
    public function resetPassword(
        Request $request,
        ResetPasswordValidator $validator
    ) {
        $data = $request->only([
            'token',
            'new_password',
            'new_password_confirmation'
        ]);

        $errors = $validator->validate($data);

        if (!empty($errors)) {
            return $this
                ->response
                ->array($errors)
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        $tokenObj = $this->auth0UserService->getResetPasswordTokenObjByToken($request->get('token'));

        if ($tokenObj->isExpired()) {
            return $this
                ->response
                ->array([
                    'token' => ['The given token is expired.']
                ])
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        // Sends a request to UA-API change password endpoint
        $response = $this->auth0UserService->updateUserPassword(
            $tokenObj->auth0User,
            $request->get('new_password')
        );

        $data = json_decode($response->getData(), true);

        // Failsafe, but shouldn't be reached
        // since Auth0UserService::updateUserPassword
        // should bubble up an HttpException
        // from AuthenticationRequestService::changePassword if it failed
        if (empty($data['data']['updated'])) {
            $this->response()->error(500, 'Updating password failed.');
        }

        $this->auth0UserService->deleteExistingResetPasswordTokens($tokenObj->user_id);

        return $this->response->array([
            'password_changed' => true,
        ]);
    }

    /**
     * @SWG\Patch(
     *     path="/auth/user/change_password",
     *     summary="Update password.",
     *     description="Update password",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="change_request",
     *         in="body",
     *         description="Change password data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/changePasswordItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Service processing failed",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Request validation failed",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="changePasswordItem",
     *     required={
     *        "new_password","new_password_confirmation"
     *     },
     *     @SWG\Property(
     *         property="new_password",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="new_password_confirmation",
     *         type="string"
     *     )
     * )
     */
    public function changePassword(
        Request $request,
        EssPasswordChangeValidator $validator
    ) {
        $data = $request->only([
            'new_password',
            'new_password_confirmation'
        ]);

        $errors = $validator->validate($data);

        if (!empty($errors)) {
            return $this
                ->response
                ->array($errors)
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        $requestData = $request->attributes->get('decodedToken');
        $auth0User   = $this->auth0UserService->getUserByAuth0UserId($requestData->sub);

        $response = $this->auth0UserService->updateUserPassword(
            $auth0User,
            $request->get('new_password'),
            // update user metadata in auth0
            // fresh account flag is used by client to
            // redirect to change password screen when set to true
            [
                'verify_password' => false,
                'user_metadata'   => [
                    'fresh_account' => false
                ]
            ]
        );

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/user/{id}/companies",
     *     summary="Get list of companies that user has access to.",
     *     description="Get list of companies that user has access to. Can be filtered by roles",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="user id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roles",
     *         in="query",
     *         description="Comma-separated list of roles to check",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="includes",
     *         in="query",
     *         description="Response extras",
     *         required=false,
     *         type="string",
     *         enum={"full_company_details"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Service processing failed",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Request validation failed",
     *     )
     * )
     */
    public function getUserCompanies(
        Request $request,
        TaskService $taskService,
        CompanyRequestService $companyRequestService,
        $id
    ) {
        $constraints = [];
        $rolesFilter = $request->get('roles');
        $includes    = $request->get('includes');

        if (!empty($rolesFilter)) {
            $rolesFilter = collect(explode(',', $rolesFilter))
                ->map(function ($item) {
                    return ucwords(trim($item));
                })
                ->toArray();

            $validRoles = [
                Role::OWNER_NAME,
                Role::SUPERADMIN_NAME,
                Role::EMPLOYEE,
                Role::ADMIN
            ];

            $invalidRoles = array_diff($rolesFilter, array_intersect($rolesFilter, $validRoles));

            if (!empty($invalidRoles)) {
                return response()
                    ->json(
                        [
                            'message' => "Invalid roles found: "
                                       . implode(', ', $invalidRoles)
                        ],
                        Response::HTTP_NOT_ACCEPTABLE
                    );
            }

            $constraints['role'] = function ($query) use ($rolesFilter) {
                $query->whereIn('name', $rolesFilter);
            };
        }

        // GET ALL ROLES OF A USER
        $userRoles = $this->userRoleService->getUserRoles(
            $id,
            ['role'], // eager load relationships
            $constraints // add relationship constraints
        );

        // Convert to assoc array with role names as keys
        $grouped = $userRoles->groupBy('role.name');
        $keys = $grouped->keys()->toArray();
        if (in_array(Role::OWNER_NAME, $keys)
            || in_array(Role::SUPERADMIN_NAME, $keys)
        ) {
            // Get list of companies under the account the user belongs to
            try {
                $response    = $this->accountRequestService->getAccountDetailsByUserId($id);
                $accountData = json_decode($response->getData(), true);
                $companies   = $accountData['data']['companies'] ?? [];
            } catch (HttpException $e) {
                // If response is 404, no account for user was found
                if ($e->getStatusCode == Response::HTTP_NOT_FOUND) {
                    $companies = [];
                } else {
                    throw $e;
                }
            }

            if (!empty($companies) && empty($includes)) {
                $companies = array_map(function ($item) {
                    return ['id' => $item['id']];
                }, $companies);
            }
        } elseif (in_array(Role::ADMIN, $keys)) {
            // Get list of company ids via companies_users where user_id = id
            $roles = $grouped->get(Role::ADMIN)->map(function ($userRole) {
                return $userRole->role;
            });

            $companyIds = $this->userRoleService->getCompanyIdForAdminRole($roles->all(), $taskService, true);

            if (empty($includes)) {
                $companies = array_map(function ($item) {
                    return (object)['id' => $item];
                }, $companyIds);
            } else {
                // Search companies
                $response      = $companyRequestService->searchByIds($companyIds);
                $companiesData = json_decode($response->getData(), true);
                $companies     = $companiesData['data'] ?? [];
            }
        }

        return response()->json([
            'data' => $companies
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/users/permissions",
     *     summary="Get list permissions for the current user.",
     *     description="Get list permissions for the current user.",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     {
     *                         "module_name": {
     *                              "scopes": {
     *                                   "COMPANY": {
     *                                        "__ALL__"
     *                                   }
     *                              },
     *                              "actions": {
     *                                   "CREATE",
     *                                   "READ"
     *                              }
     *                         },
     *                         "another_module_name": {
     *                              "scopes": {
     *                                   "COMPANY": {
     *                                        "__ALL__"
     *                                   }
     *                              },
     *                              "actions": {
     *                                   "CREATE",
     *                                   "READ"
     *                              }
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Service processing failed",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Request validation failed",
     *     )
     * )
     */
    public function getUserPermissions(Request $request, RoleRequestService $roleRequestService)
    {
        $currentUser = $request->attributes->get('user');

        $response = $this->requestService->getUserBydId($currentUser['user_id']);
        $user = json_decode($response->getData(), true);

        $accountId = $user['account_id'];
        $roleId = $user['role_id'];

        if (empty($roleId)) {
            abort(406, 'No role is currently set for the user.');
        }

        $role = $roleRequestService->getRoleByAccount($accountId, $roleId, true);

        return [
            'data' => $role['data']['policy']['resource']
        ];
    }

    public function setUserStatus(
        $id,
        $status
    ) {
        $inputs = [];

        $this->cacheService->setPrefix(static::CACHE_BUCKET_PREFIX);
        $this->cacheService->setHashFieldPrefix(static::CACHE_FIELD_PREFIX);

        if ($this->cacheService->exists($id)) {
            $this->cacheService->delete($id);
        }
        array_push($inputs, $status);

        $user = $this->authnService->getUser($id);

        if (!$user) {
            $this->notFoundError('User not found.');
        }

        // don't allow inactivating of first OWNER
        $userResponse = $this->requestService->get($id);
        $userData = json_decode($userResponse->getData(), true);
        $activeUserIds = $this->authnService->getActiveUserIds($user->account_id);

        // call microservice to validate new status
        $isValidStatusResponse = $this->requestService->validateStatus(
            $id,
            array_merge(
                ['status' => $status],
                ['active_user_ids' => $activeUserIds]
            )
        );

        if (!$isValidStatusResponse->isSuccessful()) {
            return $isValidStatusResponse;
        }

        $userRole = $this->roleRequestService->getRoleByAccount(
            $userData['account_id'],
            $userData['role']['id'],
            true
        );

        $userRoleType = $this->roleRequestService->getRoleType($userRole['data']);

        $isEmployee = !empty(
            array_filter(
                array_column($userData['companies'], 'employee_id')
            )
        );

        // update user status
        $response = $this->requestService->setStatus($id, $status);

        if (!$response->isSuccessful()) {
            return $response;
        }

        // If user type is employee, use ProcessesEmployeeActiveStatusUpdatesTrait
        if ($userRoleType === RoleRequestService::ROLE_EMPLOYEE || $isEmployee) {
            $this->processEmployeeActiveStatus(
                [
                    'id'              => $userData['companies'][0]['employee_id'],
                    'email'           => $userData['email'],
                    'first_name'      => $userData['first_name'],
                    'last_name'       => $userData['last_name'],
                    'middle_name'     => $userData['middle_name'],
                    'account_id'      => $userData['account']['id'],
                    'company_id'      => $userData['companies'][0]['id'],
                    'employee_id'     => $userData['companies'][0]['employee_id']
                ],
                ($status === Auth0User::STATUS_ACTIVE),
                $user
            );
        }

        $userUpdated = $user->fresh()->toArray();

        return $userUpdated;
    }

    /**
     * @SWG\Get(
     *     path="/user/details/{userId}",
     *     summary="Get logged-in user data",
     *     description="Get data for logged-in user",
     *     tags={"user"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getUserDetails(Request $request, int $id)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $authData = (object) [
                'account_id' => $user['account_id'],
                'company_id' => $user['employee_company_id'],
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($authData, $user);

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $employeeResponse = $this->requestService->getUserDetails($id);
        $user = json_decode($employeeResponse->getData(), true);

        return $user;
    }

    /**
     * @SWG\Get(
     *     path="/account/allusers/{$attribute}",
     *     summary="Get account users.",
     *     description="Get account users.",
     *     tags={"user"},
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     {
     *                         "id": 53351,
     *                         "employee_id": 34388,
     *                         "company_id": 12074,
     *                         "account_id": 7815,
     *                         "name": "Luigi Tobias"
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Service processing failed",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Request validation failed",
     *     )
     * )
     */
    public function getAllUsersFilter(Request $request)
    {
        $user = $request->attributes->get('user');
        $inputs = $request->all();
        $isAuthorized = false;
        $authzDataScope = null;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $user["employee_company_id"]
            ]);
        } else {
            // check company exists (will throw exception if company doesn't exist)
            $createdBy = $request->attributes->get('user');

            $data = (object) [
                'account_id' => $user['account_id'],
                'company_id' => $user["employee_company_id"]
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $userService = app()->make(UserService::class);

        try {
            $accountId = $user['account_id'];
            $keyword = isset($inputs['keyword']) && $inputs['keyword'] ? $inputs['keyword'] : null;
            $companyId = isset($inputs['company_id']) && $inputs['company_id'] ? $inputs['company_id'] : null;

            return $userService->getUsersFromEssentialData($accountId, $companyId, $keyword);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            return [];
        }
    }

    /**
     * @SWG\Get(
     *     path="/user/{id}/initial-data",
     *     summary="Get user initial data.",
     *     description="Get user data.",
     *     tags={"user"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getUserInitialData(Request $request, int $id)
    {
        $attributes = $request->attributes->get('user');

        try {
            $employeeResponse = $this->requestService->getUserInitialData($id);
            $user = json_decode($employeeResponse->getData(), true);

            $employeeId = Arr::get($user, 'employee_id', null);
            $timesheetRequired = Arr::get($user, 'employee.timesheet_required', false);

            // TIMECLOCK
            if ($employeeId && $timesheetRequired) {
                $clockStateService = App::make(ClockStateService::class);
                $timeclockLastEntry = $clockStateService->getFormattedLastTimeClockEntry($employeeId);

                if ($timeclockLastEntry) {
                    $user['timeclock_last_entry'] = $timeclockLastEntry;
                }
            }

            // SUBSCRIPTION
            $subscriptionRequestService = App::make(SubscriptionsRequestService::class);
            $response = $subscriptionRequestService->getCustomerByAccountId($user['account_id']);
            $customer = json_decode($response->getData(), true);
            $user['subscription'] = Arr::get($customer, 'data.0.subscriptions.0');

            $this->audit($request, $attributes['employee_company_id']);

            return ['data' => $user];
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());

            throw $e;
        }
    }
}
