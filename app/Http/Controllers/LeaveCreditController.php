<?php

namespace App\Http\Controllers;

use App\Facades\Company;
use App\LeaveCredit\LeaveCreditAuthorizationService;
use App\LeaveCredit\LeaveCreditRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\LeaveCredit\LeaveCreditUploadTask;
use Aws\S3\Exception\S3Exception;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;
use App\LeaveCredit\LeaveCreditUploadTaskException;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\LeaveCredit\LeaveCreditDownloadTask;
use Illuminate\Support\Arr;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class LeaveCreditController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\LeaveCredit\LeaveCreditRequestService
     */
    private $requestService;

    /**
     * @var \App\LeaveCredit\LeaveCreditAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        CsvValidator $csvValidator,
        LeaveCreditRequestService $requestService,
        LeaveCreditAuthorizationService $authorizationService
    ) {
        $this->csvValidator = $csvValidator;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/leave_credit/{id}",
     *     summary="Get Leave Credit",
     *     description="Get Leave Credit Details
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="x-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Leave Credit ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function get($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        $response = $this->requestService->get($id);
        $leaveCreditData = json_decode($response->getData());
        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee(json_decode(
                $leaveCreditData->employee->id
            ));
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $leaveCreditData->company_id,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveCreditData->account_id = Company::getAccountId($leaveCreditData->company_id);
            $this->authorizationService->authorizeGet($leaveCreditData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\POST(
     *     path="/{id}/leave_credits/download/status",
     *     summary="Get Leave Credit Download Status",
     *     description="Get Leave Credit Download Status
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="x-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function getLeaveCreditsDownloadStatus(Request $request, $id)
    {
        $attributes = $request->only(['job_id']);
        $jobId = Arr::get($attributes, 'job_id', null);
        $downloadTask = App::make(LeaveCreditDownloadTask::class);
        $cache = $downloadTask->getRedisKey($id, $jobId);
        return $cache;
    }

    /**
     * @SWG\Post(
     *     path="/leave_credit",
     *     summary="Create new leave credit",
     *     description="Store new leave credit
Authorization Scope : **create.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_type_id",
     *         in="formData",
     *         description="Leave type ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="value",
     *         in="formData",
     *         description="Number of leave credits",
     *         required=false,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="unit",
     *         in="formData",
     *         description="Unit of leave credits [hours | days]. If none selected, default will be 'hours'",
     *         type="string",
     *         enum={
     *              "hours","days"
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request, EmployeeRequestService $employeeRequestService)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($inputs['employee_id']);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveCreditData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveCreditData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['user_id'] = $createdBy['user_id'];

        $response = $this->requestService->create($inputs);
        $responseData = json_decode($response->getData(), true);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $leaveCreditResponse = $this->requestService->get($responseData['id']);
            $leaveCreditData = json_decode($leaveCreditResponse->getData(), true);

            $this->audit($request, $inputs['company_id'], $leaveCreditData);
        }



        return $response;
    }

    /**
    * @SWG\Put(
    *     path="/leave_credit/{id}",
    *     summary="Update leave credit",
    *     description="Update existing leave credit hours
Authorization Scope : **edit.leave_credit**",
    *     tags={"leave_credit"},
    *     consumes={"application/x-www-form-urlencoded"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Leave Credit ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="company_id",
    *         in="formData",
    *         description="Company ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="value",
    *         in="formData",
    *         description="Number of leave credits",
    *         required=true,
    *         type="number"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="Successful operation",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
    *         description="Unauthorized",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function update(int $id, Request $request)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldLeaveCreditData = json_decode($response->getData());
        $oldLeaveCreditData->account_id = Company::getAccountId($inputs['company_id']);
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            // Get employee data for authz_data_scope
            $employeeRequestService = app()->make(EmployeeRequestService::class);
            $employeeDataResponse = $employeeRequestService->getEmployee($oldLeaveCreditData->employee->id);
            $employee = json_decode($employeeDataResponse->getData(), true);

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                AuthzDataScope::SCOPE_COMPANY => $oldLeaveCreditData->company_id,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldLeaveCreditData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $inputs['user_id'] = $updatedBy['user_id'];

        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldLeaveCreditData = json_decode($response->getData(), true);

        if ($updateResponse->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($getResponse->getData(), true);
            $this->audit($request, $inputs['company_id'], $newData, $oldLeaveCreditData);
        }

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/leave_credits",
     *     summary="Get Company Leave Credits",
     *     description="Get Company Leave Credits
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID) and leave type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1 . Default is 1",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results to return per page. Default is 10",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="formData",
     *         description="Attribute to sort by.
 Possible values: employee_name, employee_id, leave_type_name, remaining, used, earned, updated_by, updated_at",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="formData",
     *         description="Sort order: asc, desc.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="locations_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="departments_ids[]",
     *         in="formData",
     *         description="Filters departments",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="positions_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Filters employees",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_types_ids[]",
     *         in="formData",
     *         description="Filters leave types",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLeaveCredits(Request $request, $id)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        $authzDataScope = null;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $leaveCredit = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($leaveCredit, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $queryString = $request->getQueryString() ?? '';
        $response = $this->requestService->getCompanyLeaveCredits($id, $request->all(), $queryString, $authzDataScope);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/leave_credit/bulk_delete",
     *     summary="Delete multiple Leave Credits",
     *     description="Delete multiple Leave Credits
Authorization Scope : **delete.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_credits_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Credits Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $authzDataScope = $this->getAuthzDataScope($request);

        $isAuthorized = false;

        if ($authzEnabled) {
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id']
            ]);
        } else {
            $leaveCreditData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($leaveCreditData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete(
            $inputs,
            $authzDataScope
        );
        if ($response->isSuccessful()) {
            // trigger audit trail
            foreach ($request->input('leave_credits_ids', []) as $id) {
                $oldData = [
                    'id' => $id,
                    'company_id' => $request->get('company_id')
                ];
                $this->audit($request, $inputs['company_id'], [], $oldData);
            }
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_credit/upload",
     *     summary="Upload Leave Credits",
     *     description="Uploads Leave Credits
Authorization Scope : **create.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "leave_credit_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
            ]);
        } else {
            // authorize
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $leaveCreditData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveCreditData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(LeaveCreditUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => LeaveCreditUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => !empty($authzDataScope) ? $authzDataScope->getDataScope() : []
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            Amqp::publish(env('LEAVE_CREDIT_VALIDATION_QUEUE'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/leave_credit/upload/status",
     *     summary="Get Job Status",
     *     description="Get Leave Credit Upload Status
Authorization Scope : **create.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\LeaveCredit\LeaveCreditUploadTask::PROCESS_VALIDATION,
     *              App\LeaveCredit\LeaveCreditUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Name is invalid"
     *                          },
     *                          "4": {
     *                              "Email is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $leaveCreditData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $leaveCreditData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveCreditUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (LeaveCreditUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, LeaveCreditUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(',', LeaveCreditUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === LeaveCreditUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === LeaveCreditUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }

        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/leave_credit/upload/preview",
     *     summary="Get Leave Credit Preview",
     *     description="Get Leave Credit Upload Preview
Authorization Scope : **create.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $leaveCreditData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $leaveCreditData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveCreditUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (LeaveCreditUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        return $this->requestService->getUploadPreview($jobId);
    }

    /**
     * @SWG\Post(
     *     path="/leave_credit/upload/save",
     *     summary="Save uploaded leave credits",
     *     description="Saves Leave Credits from Previously Uploaded CSV
Authorization Scope : **create.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "leave_credit_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Leave credits needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $leaveCreditData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveCreditData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveCreditUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(LeaveCreditUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (LeaveCreditUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            'user_id' => $uploadTask->getUserId(),
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(env('LEAVE_CREDIT_WRITE_QUEUE'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/leave_credits/download",
     *     summary="Download Leave Credits CSV",
     *     description="Download generated CSV with leave credits
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_credits_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Credits Ids",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         in="formData",
     *         description="Download all indicator",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="locations_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="departments_ids[]",
     *         in="formData",
     *         description="Filters departments",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="positions_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Filters employees",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_types_ids[]",
     *         in="formData",
     *         description="Filters leave types",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadCompanyLeaveCredits($id, Request $request)
    {

        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $leaveCredit = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($leaveCredit, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Create status cache Check job exists (will throw exception if job doesn't exist)
        try {
            $uploadTask = App::make(LeaveCreditDownloadTask::class);
            $uploadTask->create($id);
            $jobId = $uploadTask->getId();
        } catch (LeaveCreditUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // Drop to queue
        $details =             [
            'id'        => $jobId,
            'task'      => 'download',
            'authz_data_scope' => $this->getAuthzDataScope($request)->getDataScope(),
            'request'   => array_merge(['id'=>$id], $request->all())
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.leave_credits_download_queue'), $message);

        return ['data'=>['job_id' => $jobId]];
    }

    /**
     * @SWG\Post(
     *     path="/employee/{id}/leave_credits/download",
     *     summary="Download Employee  Leave Credits CSV",
     *     description="Download generated CSV with leave credits for employee
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Employee ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         type="integer",
     *         in="formData",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="leave_credits_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Credits Ids",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         in="formData",
     *         description="Download all indicator",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadEmployeeLeaveCredits($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($id);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveCredit = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($leaveCredit, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $queryString = $request->getQueryString() ?? '';
        return $this->requestService->downloadEmployeeLeaveCredits($id, $inputs, $queryString);
    }
}
