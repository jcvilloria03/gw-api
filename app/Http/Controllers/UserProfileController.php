<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\User\UserAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\User\UserProfileUploadTask;
use Aws\S3\Exception\S3Exception;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\App;
use App\User\UserProfileUploadTaskException;
use App\Role\RoleService;
use App\Company\PhilippineCompanyRequestService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UserProfileController extends Controller
{
    /**
     * @var \App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\User\UserAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $requestService;

    /**
     * @var \App\Role\RoleService
     */
    protected $roleService;

    /**
     * @var PhilippineCompanyRequestService
     */
    private $companyRequestService;

    public function __construct(
        CsvValidator $csvValidator,
        UserRequestService $requestService,
        UserAuthorizationService $authorizationService,
        RoleService $roleService,
        PhilippineCompanyRequestService $companyRequestService
    ) {
        $this->csvValidator = $csvValidator;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->roleService = $roleService;
        $this->companyRequestService = $companyRequestService;
    }

    /**
     * @SWG\Post(
     *     path="/user/upload",
     *     summary="Upload User Profiles",
     *     description="Uploads User Profiles
Authorization Scope : **create.user**",
     *     tags={"user"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "user_profile_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $accountId = $createdBy['account_id'];
        $userId = $createdBy['user_id'];
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $authData = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(UserProfileUploadTask::class);
            $uploadTask->create($accountId, $companyId);
            $uploadTask->cacheDataSetInRedis($this->getPreparedDataSet($accountId, $companyId));
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'account_id' => $accountId,
                'company_id' => $companyId,
                'uploader_user_id' => $userId,
                'task' => UserProfileUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            Amqp::publish(config('queues.user_profile_validation_queue'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * Get prepared data set for upload
     *
     * @return array
     */
    private function getPreparedDataSet($accountId, $companyId)
    {
        $roles = $this->roleService->getRoles($accountId, false, false, (int)$companyId);

        return [
            'roles' => json_encode($roles->toArray())
        ];
    }

    /**
     * @SWG\Get(
     *     path="/user/upload/status",
     *     summary="Get Job Status",
     *     description="Get User Profiles Upload Status
Authorization Scope : **create.user**",
     *     tags={"user"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\User\UserProfileUploadTask::PROCESS_VALIDATION,
     *              App\User\UserProfileUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "First name is invalid"
     *                          },
     *                          "4": {
     *                              "Last name is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $accountId = $createdBy['account_id'];
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $authData = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(UserProfileUploadTask::class);
            $uploadTask->create($accountId, $companyId, $jobId);
        } catch (UserProfileUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, UserProfileUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(',', UserProfileUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === UserProfileUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === UserProfileUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }

        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/user/upload/preview",
     *     summary="Get User Profiles Preview",
     *     description="Get User Profiles Upload Preview
Authorization Scope : **create.user**",
     *     tags={"user"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $accountId = $createdBy['account_id'];
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $authData = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(UserProfileUploadTask::class);
            $uploadTask->create($accountId, $companyId, $jobId);
        } catch (UserProfileUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        return $this->requestService->getUploadPreview($jobId);
    }

    /**
     * @SWG\Post(
     *     path="/user/upload/save",
     *     summary="Save uploaded user profiles",
     *     description="Saves User Profiles from Previously Uploaded CSV
Authorization Scope : **create.user**",
     *     tags={"user"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "user_profile_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "User profiles needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $accountId = $createdBy['account_id'];
        $userId = $createdBy['user_id'];
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if (is_numeric($companyId)) {
            $this->companyRequestService->get($companyId);
        }

        $authData = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $authData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(UserProfileUploadTask::class);
            $uploadTask->create($accountId, $companyId, $jobId);
            $uploadTask->updateSaveStatus(UserProfileUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (UserProfileUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'account_id' => $accountId,
            'company_id' => $companyId,
            'uploader_user_id' => $userId,
            'user_id' => $uploadTask->getUserId(),
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.user_profile_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }
}
