<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\DefaultSchedule\DefaultScheduleAuditService;
use App\DefaultSchedule\DefaultScheduleAuthorizationService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use App\Facades\Company;
use Illuminate\Http\Request;
use App\DefaultSchedule\EssDefaultScheduleAuthorizationService;
use App\Authz\AuthzDataScope;

class DefaultScheduleController extends Controller
{
    /**
     * @var \App\DefaultSchedule\DefaultScheduleRequestService
     */
    private $requestService;

    /**
     * @var \App\DefaultSchedule\DefaultScheduleAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        DefaultScheduleRequestService $requestService,
        DefaultScheduleAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }


    /**
     * @SWG\Get(
     *     path="/default_schedule",
     *     summary="Get default schedules",
     *     description="Get default schedules details

Authorization Scope : **view.default_schedule**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function index(Request $request)
    {
        $companyId = (int) $request->input('company_id');

        $isAuthzEnabled = $this->isAuthzEnabled($request);

        if ($isAuthzEnabled) {
            $dataScope = $this->getAuthzDataScope($request);

            if (!$dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->index($companyId);

        if (!$isAuthzEnabled) {
            $defaultScheduleData = json_decode($response->getData());

            foreach ($defaultScheduleData->data as $defaultSchedule) {
                $defaultSchedule->account_id = Company::getAccountId($defaultSchedule->company_id);

                if (!$this->authorizationService->authorizeGet($defaultSchedule, $request->attributes->get('user'))) {
                    $this->response()->errorUnauthorized();
                }
            }
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/company/{companyId}/default_schedule",
     *     summary="Get default schedules",
     *     description="Get default schedules details
Authorization Scope : **ess.view.default_schedule**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompaniesDefaultSchedule(
        $id,
        Request $request,
        EssDefaultScheduleAuthorizationService $essAuthorizationService
    ) {
        $user = $request->attributes->get('user');

        if (!$essAuthorizationService->authorizeView($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        if ($id != $user['employee_company_id']) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->index($id);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/default_schedule/bulk_create",
     *     summary="Create company default schedule",
     *     description="Create company default schedule

Authorization Scope : **create.default_schedule**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *
     *     @SWG\Parameter(
     *         name="default_schedules",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/NewDefaultSchedules"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="NewDefaultSchedules",
     *     required={"company_id", "default_schedules"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="default_schedules",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/NewDefaultSchedule"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="NewDefaultSchedule",
     *     required={"day_of_week", "day_type", "work_start", "work_break_start", "work_break_end", "work_end"},
     *     @SWG\Property(
     *         property="day_of_week",
     *         type="integer",
     *         description="Day of week. Possible values: 1, 2, 3, 4, 5, 6, 7"
     *     ),
     *     @SWG\Property(
     *         property="day_type",
     *         type="string",
     *         description="Day type value. Possible values: regular, rest_day"
     *     ),
     *     @SWG\Property(
     *         property="work_start",
     *         type="string",
     *         description="Start time of the default schedule in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_break_start",
     *         type="string",
     *         description="Start time of the break in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_break_end",
     *         type="string",
     *         description="End time of the break in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_end",
     *         type="string",
     *         description="End time of the default schedule in HH:mm format"
     *     ),
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $defaultScheduleData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $defaultScheduleData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        foreach ($responseData['data'] as $item) {
            $details = [
                'new' => $item,
            ];
            $item = new AuditCacheItem(
                DefaultScheduleAuditService::class,
                DefaultScheduleAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/default_schedule/bulk_update",
     *     summary="Update company default schedules",
     *     description="Update company default schedules
Authorization Scope : **edit.default_schedule**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/ExistingDefaultSchedules"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ExistingDefaultSchedules",
     *     required={"company_id", "default_schedules"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="default_schedules",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/ExistingDefaultSchedule"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="ExistingDefaultSchedule",
     *     required={"id","day_of_week", "day_type", "work_start", "work_break_start", "work_break_end", "work_end"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *         description="Default Schedule ID"
     *     ),
     *     @SWG\Property(
     *         property="day_of_week",
     *         type="integer",
     *         description="Day of week. Possible values: 1, 2, 3, 4, 5, 6, 7"
     *     ),
     *     @SWG\Property(
     *         property="day_type",
     *         type="string",
     *         description="Day type value. Possible values: regular, rest_day"
     *     ),
     *     @SWG\Property(
     *         property="work_start",
     *         type="string",
     *         description="Start time of the default schedule in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_break_start",
     *         type="string",
     *         description="Start time of the break in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_break_end",
     *         type="string",
     *         description="End time of the break in HH:mm format"
     *     ),
     *     @SWG\Property(
     *         property="work_end",
     *         type="string",
     *         description="End time of the default schedule in HH:mm format"
     *     ),
     * )
     */
    public function bulkUpdate(Request $request)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $isAuthzEnabled = $this->isAuthzEnabled($request);
        $isAuthorized = false;

        if ($isAuthzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $defaultScheduleData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $defaultScheduleData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $oldDataResponse = $this->requestService->index($inputs['company_id']);
        $oldData = json_decode($oldDataResponse->getData(), true);
        $oldData = collect($oldData['data']);

        $response = $this->requestService->bulkUpdate($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        foreach ($responseData['data'] as $item) {
            $old = $oldData->where('id', $item['id']);
            $details = [
                'new' => $item,
                'old' => $old->all()
            ];
            $logItem = new AuditCacheItem(
                DefaultScheduleAuditService::class,
                DefaultScheduleAuditService::ACTION_UPDATE,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                $details
            );
            $this->auditService->queue($logItem);
        }

        return $response;
    }
}
