<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Workflow\WorkflowRequestService;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\ESS\EssShiftChangeRequestRequestService;
use App\ShiftChangeRequest\ShiftChangeRequestAuditService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Authz\AuthzDataScope;

class EssShiftChangeRequestController extends EssBaseController
{
    /**
     * @var \App\ESS\EssShiftChangeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EssShiftChangeRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/shift_change_request",
     *     summary="Create Shift Change Request",
     *     description="Create Shift Change Request
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newShiftChangeRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newShiftChangeRequest",
     *     required={"start_date", "end_date", "shifts"},
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Shift change request start date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         default="2017-10-30",
     *         description="Shift change request end date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="messages",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *         description="Initial messages."
     *     ),
     *     @SWG\Property(
     *         property="shift_change_dates",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/shiftStateRecord")
     *     )
     * ),
     *
      * @SWG\Definition(
     *     definition="shiftStateRecord",
     *     required={"shift_id", "date"},
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-01-30",
     *         description="Date with shift changes in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="old_state",
     *         type="object",
     *         @SWG\Property(property="shifts_ids", type="array",  @SWG\Items(type="integer")),
     *         @SWG\Property(property="rest_days_ids", type="array",  @SWG\Items(type="integer")),
     *         description="Old shifts data on current date."
     *     ),
     *     @SWG\Property(
     *         property="new_state",
     *         type="object",
     *         @SWG\Property(property="shifts_ids", type="array",  @SWG\Items(type="integer")),
     *         @SWG\Property(property="schedules_ids", type="array",  @SWG\Items(type="integer")),
     *         @SWG\Property(property="rest_day", type="boolean"),
     *         description="New shifts data on current date."
     *     )
     * )
     */
    public function create(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();
        $data['company_id'] = $user['employee_company_id'];
        $data['employee_id'] = $user['employee_id'];
        $data['user_id'] = $user['user_id'];

        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $shiftChangeRequest = $this->requestService->get($responseData['id']);
            $shiftChangeRequestData = json_decode($shiftChangeRequest->getData(), true);

            $details = [
                'new' => [
                    'employee_company_id' => $user['employee_company_id'],
                    'id' => $shiftChangeRequestData['id'],
                ],
            ];

            $item = new AuditCacheItem(
                ShiftChangeRequestAuditService::class,
                ShiftChangeRequestAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                $details
            );

            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/shift_change_request/{id}",
     *     summary="Get Shift Change Request",
     *     description="Get Shift Change Request",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Shift Change Request ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->get($id);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $shiftChangeRequest = json_decode($response->getData());
        $shiftChangeRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $shiftChangeRequest->request->company_id
            );

            $isAuthorized = ApprovalService::isEitherRequestorOrApproverNew(
                $user['employee_id'],
                $shiftChangeRequest,
                $user
            );
        } else {
            $workflows = $shiftChangeRequest->workflow_levels;

            $isAuthorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $shiftChangeRequest
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this
            ->response
            ->array((array) $shiftChangeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
