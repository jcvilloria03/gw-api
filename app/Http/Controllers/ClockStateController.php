<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use InvalidArgumentException;
use App\ESS\EssClockStateService;
use App\Events\ClockStateChanged;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\ClockState\EssClockStateAuthorizationService;
use App\Attendance\ClockStateAttendanceTriggerService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\MaxClockOut\MaxClockOutRequestService;
use Illuminate\Support\Facades\App;
use App\ClockState\ClockStateService;

/**
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class ClockStateController extends EssBaseController
{
    const DEFAULT_ERROR = 'You logged-in via an IP that is not part of the approved IP range. <br>
    <br> This Clock in/Clock out is invalid.';

    /**
     * @var \App\ESS\EssClockStateService
     */
    private $essClockStateService;

    /**
     * @var \App\ClockState\EssClockStateAuthorizationService
     */
    private $authorizationService;

    /**
     * ClockStateController constructor
     *
     * @param \App\ESS\EssClockStateService                     $essClockStateService
     * @param \App\ClockState\EssClockStateAuthorizationService $authorizationService
     */
    public function __construct(
        EssClockStateService $essClockStateService,
        EssClockStateAuthorizationService $authorizationService
    ) {
        $this->essClockStateService = $essClockStateService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/clock_state",
     *     summary="Get Clock state",
     *     description="Get the current clock state (clocked-in or clocked-out) of an employee.
    Authorization Scope : **ess.clock.view_time_records**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeViewTimeRecords($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->essClockStateService->get($user['employee_id']);
    }

    /**
     * @SWG\Get(
     *     path="/ess/clock_state/timesheet",
     *     summary="Get Clock timesheet",
     *     description="Get the timesheet (all clock-ins and -outs) of an employee.
    Authorization Scope : **ess.clock.view_time_records**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="start_timestamp",
     *         in="query",
     *         description="Start time of records in unix timestamp format.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="end_timestamp",
     *         in="query",
     *         description="End time of records in unix timestamp format.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getTimesheet(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeViewTimeRecords($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        $inputs = $request->all();

        return $this->essClockStateService->getTimesheet($user['employee_id'], $inputs);
    }

    /**
     * @SWG\Get(
     *     path="/ess/clock_state/timesheet/last_entry",
     *     summary="Get Clock timesheet last entry",
     *     description="Get the timesheet (last entry) of an employee.
    Authorization Scope : **ess.clock.view_time_records**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getTimesheetLastEntry(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeViewTimeRecords($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        $clockStateService = App::make(ClockStateService::class);
        return $clockStateService->getFormattedLastTimeClockEntry($user['employee_id']);
    }

    /**
     * @SWG\Post(
     *     path="/ess/clock_state/log",
     *     summary="Add timeclock record log",
     *     description="Adds a timeclock record for a supplied employee id
    Authorization Scope : **ess.clock.log**",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="state",
     *         in="formData",
     *         required=true,
     *         enum={
     *              "1","0"
     *         }
     *     ),
     *     @SWG\Parameter(
     *         name="tags[]",
     *         type="array",
     *         in="formData",
     *         description="Tags",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="timestamp",
     *         in="formData",
     *         description="Timestamp in format: Y-m-d H:i:s",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="timezone",
     *         in="formData",
     *         description="Timezone (Asia/Manila timezone by default)",
     *         required=false,
     *         type="string",
     *         enum={"Asia/Manila", "UTC"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function log(Request $request, ClockStateAttendanceTriggerService $attendanceTriggerService)
    {
        $user = $this->getFirstEmployeeUser($request);
        $attributes = $request->all();
        // Validate allowed time methods
        $validTimeMethod = (!empty($attributes['origin'])
            ? $this->authorizationService->validateTimeMethods($user, $attributes) : true);
        if (!$validTimeMethod) {
            $this->response()->errorForbidden(
                'You are not authorized to clock-in or out via ESS.
                Please contact your administrator for more information.'
            );
        }

        $ipAddress = $request->server('HTTP_X_ORIGINAL_FORWARDED_FOR') ?? '';
        // For safety only. The above code should suffice.
        if (!$ipAddress) {
            $ipAddress = $request->server('HTTP_X_REAL_IP');
        }

        // For safety only. The above code should suffice.
        if (!$ipAddress) {
            $ipAddress = $request->server('HTTP_X_FORWARDED_FOR');
        }

        // If the above code fails, return na error forbidden. This shouldn't happen
        if (!$ipAddress) {
            $this->response()->errorForbidden(self::DEFAULT_ERROR);
        }

        $this->validate($request, [
            'timestamp' => 'date_format:"Y-m-d H:i:s"',
            'timezone' => 'timezone',
        ], [], []);

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeLog($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        // IP Restriction
        if (!$this->authorizationService->isIPAllowed($user["employee_id"], $ipAddress)) {
            $this->response()->errorForbidden(self::DEFAULT_ERROR);
        }

        if (empty($attributes['max_clockout_rule'])) {
            $attributes['max_clockout_rule']['name'] = null;
            $attributes['max_clockout_rule']['hours'] = null;
        }

        $this->validate($request, [
            'max_clockout_rule.hours' => 'nullable|regex:/^\d*(\.\d{2})?$/'
        ]);

        $message = $this->essClockStateService->log(
            $user['employee_id'],
            $user['employee_company_id'],
            $attributes
        );
        $messageData = $message->getData();

        // Disable temporarily as this causes spam in calculation
        // $formatedDate = Carbon::parse($attributes['timestamp'] ?? null)->format('Y-m-d');
        // $triggerServiceResponseData = $attendanceTriggerService->calculateAttendance(
        //     (int)$user['employee_id'],
        //     $formatedDate
        // );

        // attach job_id from response to $messageData after it's decoded from json
        if (is_string($messageData)) {
            $messageData = json_decode($messageData, true);
        } else {
            $messageData = (array) $messageData;
        }

        $messageData['job_id'] = '';

        return $messageData;
    }

    /**
     * @SWG\Get(
     *     path="/ess/time",
     *     summary="Get current server time",
     *     description="Get current server time",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="timezone",
     *         in="query",
     *         required=false
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     )
     * )
     */
    public function getServerTime(Request $request)
    {
        $now = Carbon::now();

        if ($request->get('timezone', false)) {
            try {
                $now->setTimezone($request->get('timezone'));
            } catch (InvalidArgumentException $e) {
                throw new HttpException(Response::HTTP_NOT_ACCEPTABLE, $e->getMessage());
            }
        }

        return response()->json(['data' => [
            'server_timestamp' => $now->timestamp,
            'conversion'       => [
                'datetime'   => $now->format('Y-m-d H:i:s'),
                'timezone'   => $now->timezoneName,
                'utc_offset' => $now->getOffset()
            ]
        ]]);
    }
}
