<?php

namespace App\Http\Controllers;

use App\CSV\CsvValidator;
use App\Employee\EmployeeAuthz;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Aws\S3\Exception\S3Exception;
use App\CSV\CsvValidatorException;
use Illuminate\Support\Facades\App;
use App\Attendance\AttendanceUploadTask;
use App\Employee\EmployeeRequestService;
use App\Attendance\AttendanceRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Attendance\AttendanceAuthorizationService;
use App\Authz\AuthzDataScope;
use Illuminate\Support\Arr;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class AttendanceController extends Controller
{
    use AuditTrailTrait;

    /**
     * App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /**
     * @var \App\Attendance\AttendanceRequestService
     */
    protected $requestService;

    /**
     * @var \App\Attendance\AttendanceAuthorizationService
     */
    protected $authorizationService;

    /**
     * AttendanceController constructor.
     *
     * @param AttendanceRequestService       $requestService       AttendanceRequestService
     */
    public function __construct(
        CsvValidator $csvValidator,
        PhilippineCompanyRequestService $companyRequestService,
        AttendanceRequestService $requestService,
        AttendanceAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->companyRequestService = $companyRequestService;
        $this->csvValidator = $csvValidator;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/attendance/records",
     *     summary="Get Attendance Records",
     *     description="Retrieve a list of attendance records. Supports filtering and pagination.
search, filter, sorting & pagination of attendance records.

Authorization Scope : **view.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/searchAttendanceRecordsItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="page", type="integer"),
     *             @SWG\Property(property="page_size", type="integer"),
     *             @SWG\Property(property="count", type="integer"),
     *             @SWG\Property(property="total", type="integer"),
     *             @SWG\Property(
     *                  property="attendance_records",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/attendanceRecordsRequestCollection"),
     *                  collectionFormat="multi"
     *             ),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "page" : 1,
     *                  "page_size" : 10,
     *                  "count": 3,
     *                  "total": 498,
     *                  "attendance_records": {
     *                      {
     *                          "employee": {
     *                              "uid": "1",
     *                              "company_employee_id": "qwerty-12",
     *                              "full_name": "John Doe",
     *                              "department_id": 42,
     *                              "location_id": 32,
     *                              "position_id": 256
     *                          },
     *                          "company_id": "1",
     *                          "date": "2018-06-01",
     *                          "locked": false,
     *                          "attendance": {
     *                              "rh": 480,
     *                              "uh": 480
     *                          },
     *                          "scheduled_shifts": {
     *                              {
     *                                  "schedule_id": "1",
     *                                  "name": "morning"
     *                              },
     *                              {
     *                                  "schedule_id": "2",
     *                                  "name": "afternoon"
     *                              }
     *                          },
     *                          "time_records": {
     *                              "first_clock_in": "2018-09-03 08:31",
     *                              "last_clock_out": "2018-09-03 17:04"
     *                          }
     *                      },
     *                      {
     *                          "employee": {
     *                              "uid": 2,
     *                              "company_employee_id": "qwerty-12",
     *                              "full_name": "John Smith",
     *                              "department_id": 31,
     *                              "location_id": 44,
     *                              "position_id": 1
     *                          },
     *                          "company_id": "1",
     *                          "date": "2018-06-01",
     *                          "locked": false,
     *                          "attendance": {
     *                              "rh": 480,
     *                              "uh": 480
     *                          },
     *                          "scheduled_shifts": {
     *                              {
     *                                  "schedule_id": "1",
     *                                  "name": "morning"
     *                              },
     *                              {
     *                                  "schedule_id": "2",
     *                                  "name": "afternoon"
     *                              }
     *                          },
     *                          "time_records": {
     *                              "first_clock_in": "2018-09-03 08:31",
     *                              "last_clock_out": "2018-09-03 17:04"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Invalid Employee ID/s": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: attendance record must have at least one valid employee uid."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "No records found": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: No attendance records matching supplied parameters found."
     *              },
     *         }
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="searchAttendanceRecordsItem",
     *     required={"company_id"},
     *     @SWG\Property(
     *         property="ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="employee_status",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="page",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="page_size",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="search_terms",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="integer"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *     ),
     *     @SWG\Property(
     *         property="filters",
     *         type="object",
     *         @SWG\Property(
     *              property="location",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="department",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="position",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *     ),
     *     @SWG\Property(
     *         property="sort_by",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="string"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="date", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *         @SWG\Property(property="first_clock_in", type="string"),
     *         @SWG\Property(property="last_clock_out", type="string"),
     *         @SWG\Property(property="computed_attendance", type="string"),
     *     )
     * ),
     * @SWG\Definition(
     *     definition="attendanceRecordsRequestCollection",
     *     @SWG\Property(
     *         property="employee",
     *         type="object",
     *         @SWG\Property(property="uid", type="integer"),
     *         @SWG\Property(property="company_employee_id", type="string"),
     *         @SWG\Property(property="full_name", type="string"),
     *         @SWG\Property(property="department_id", type="integer"),
     *         @SWG\Property(property="location_id", type="integer"),
     *         @SWG\Property(property="position_id", type="integer"),
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="locked",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="attendance",
     *         type="object",
     *         @SWG\Property(property="attendance_type", type="integer"),
     *         example={
     *              "UH": 480,
     *              "RH": 480,
     *         },
     *     ),
     *     @SWG\Property(
     *         property="scheduled_shifts",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/scheduledShiftsRequestCollection"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="time_records",
     *         description="YYYY-MM-DD hh:mm:ss",
     *         type="object",
     *         @SWG\Property(property="first_clock_in", type="string"),
     *         @SWG\Property(property="last_clock_out", type="string"),
     *         example={
     *              "first_clock_in": "2018-09-03 08:31",
     *              "last_clock_out": "2018-09-03 17:04",
     *         },
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="scheduledShiftsRequestCollection",
     *     @SWG\Property(
     *         property="schedule_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string"
     *     ),
     * ),
     */
    public function searchAttendanceRecords(Request $request): \Illuminate\Http\JsonResponse
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $companyResponse = $this->companyRequestService->get($request->get('company_id'));
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
            $dataScope = $this->getAuthzDataScope($request);
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->searchAttendanceRecords($request->all(), $dataScope);
    }

    /**
     * @SWG\Post(
     *     path="/attendance/records/export",
     *     summary="Download Attendance Records",
     *     description="Download attendance records. Supports searching, filtering, & sorting of attendance records.

Authorization Scope : **export.attendance_computation**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/downloadAttendanceRecordsItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(property="link", type="string"),
     *             ),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      {
     *                          "link": "https://v3-uploads-local.s3.amazonaws.com/a6e90c2c-781e-584f-bda3-b3aacbdcc209.csv?AWSAccessKeyId=AKIAIM6V6OBDMAB3656A&Signature=2nWugRICIY82oO88SupIz2Zg1oo%3D&Expires=1547725044"
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Invalid Employee ID/s": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: attendance record must have at least one valid employee uid."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "No records found": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: No attendance records matching supplied parameters found."
     *              },
     *         }
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="downloadAttendanceRecordsItem",
     *     @SWG\Property(
     *         property="ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="page",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="page_size",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="search_terms",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="integer"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *     ),
     *     @SWG\Property(
     *         property="filters",
     *         type="object",
     *         @SWG\Property(
     *              property="location",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="department",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="position",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *     ),
     *     @SWG\Property(
     *         property="sort_by",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="string"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="date", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *         @SWG\Property(property="first_clock_in", type="string"),
     *         @SWG\Property(property="last_clock_out", type="string"),
     *         @SWG\Property(property="computed_attendance", type="string"),
     *     )
     * ),
     */
    public function exportAttendanceRecords(Request $request): \Illuminate\Http\JsonResponse
    {
        $inputs = $request->all();
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $companyResponse = $this->companyRequestService->get($request->get('company_id'));
        $companyData = json_decode($companyResponse->getData());
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $request->get('company_id')
            );
            $filterScope = $authzDataScope->getDataScope();
            unset($filterScope[AuthzDataScope::SCOPE_COMPANY]);
            $inputs['filters'] = array_merge_recursive(
                !empty($inputs['filters']) ? $inputs['filters'] : [],
                array_change_key_case($filterScope, CASE_LOWER)
            );
        } else {
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $isAuthorized = $this->authorizationService->authorizeExportComputedAttendance(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response =  $this->requestService->exportAttendanceRecords($inputs);

        try {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyData->id, [], $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/attendance/{employee_id}/{date}",
     *     summary="Retrieve a Single Attendance Record",
     *     description="Retrieves a single attendance record for a given employee on a given date.

Authorization Scope : **view.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *     ),
     * )
     */
    public function getSingleAttendanceRecord(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $response     = $employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeGet(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getSingleAttendanceRecord($employeeId, $attendanceDate);
    }

    /**
     * @SWG\Get(
     *     path="/attendance/job/{job_id}",
     *     summary="Get job details",
     *     description="Get job details",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="path",
     *         description="Job Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *     ),
     * )
     */
    public function getJobDetails($jobId): \Illuminate\Http\JsonResponse
    {
        return $this->requestService->getJobDetails($jobId);
    }

    /**
     * @SWG\Get(
     *     path="/attendance/job/{job_id}/errors",
     *     summary="Get job errors",
     *     description="Get job errors",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="path",
     *         description="Job Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *     ),
     * )
     */
    public function getJobErrors($jobId): \Illuminate\Http\JsonResponse
    {
        return $this->requestService->getJobErrors($jobId);
    }

    /**
     * @SWG\Get(
     *     path="/attendance/{employee_id}/{date}/lock_status",
     *     summary="Get Lock Status",
     *     description="Get the LOCKED status of a single attendance record.
Returns True if locked, False if unlocked.

Authorization Scope : **view.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *     ),
     * )
     */
    public function getSingleAttendanceRecordLockStatus(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $response     = $employeeRequestService->getEmployee($request->get('employee_id'));
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeGet(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }
        return $this->requestService->getSingleAttendanceRecordLockStatus($employeeId, $attendanceDate);
    }

    /**
     * @SWG\Get(
     *     path="/attendance/{employee_id}/{date}/calculate",
     *     summary="Calculates a single attendance record for a given employee on a given date",
     *     description="Calculates a single attendance record for a given employee on a given date.
This endpoint is lock protected, and cannot operate upon a record with “locked” status set to **True**.
You must first **unlock the record** before performing this action.

Authorization Scope : **create.attendance_computation**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
     *         description="Accepted",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="NotFoundError: No entitled employee with matching ID found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
     *         description="PermissionDeniedError: The specified attendance record is locked.",
     *     ),
     * )
     */
    public function calculateAttendanceRecord(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $response     = $employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeGenerateCalculation(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->calculateAttendanceRecord($employeeId, $attendanceDate);
    }

    /**
     * @SWG\Get(
     *     path="/attendance/{employee_id}/{date}/lock",
     *     summary="Lock an employee attendance record",
     *     description="Set the **LOCKED** property of a supplied record to the value **True**,
locking that record and preventing automatic computation of that record.

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: no attendance record for employee id/date found."
     *              }
     *         }
     *     ),
     * )
     */
    public function lockAttendanceRecord(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $response     = $employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeUpdate(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->lockAttendanceRecord($employeeId, $attendanceDate);

        try {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/attendance/lock/bulk",
     *     summary="Bulk Lock Attendance Records",
     *     description="Set the **LOCKED** property of supplied records to the value **True**,
locking those records will prevent automatic computation of those records.

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bulkAttendanceLockRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="rows_affected", type="integer"),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "rows_affected": 0
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Invalid JSON body": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: JSON body is invalid."
     *              },
     *             "Company ID is required": {
     *                  "Code": 406,
     *                  "Message": "Company ID should not be empty."
     *              },
     *              "Invalid company_id": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: company_id must be a valid id and integer."
     *              },
     *              "Attendance Ids is required": {
     *                  "Code": 406,
     *                  "Message": "The attendance record ids field is required."
     *               },
     *              "Invalid list of Ids": {
     *                  "Code": "406",
     *                  "Message": "The attendance_record_ids.0 must be an integer."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="bulkAttendanceLockRequestItemCollection",
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="attendance_record_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         description="Attendance record ID"
     *     )
     * )
     */
    public function bulkLockAttendanceRecord(
        Request $request
    ): \Illuminate\Http\JsonResponse {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $this->validate($request, [
            'company_id' => 'required|integer|not_in:0',
            'attendance_record_ids' => 'required|array',
            'attendance_record_ids.*' => 'integer'
        ]);

        $companyId = $request->get('company_id');
        $attendanceRecordIds = $request->get('attendance_record_ids');

        $isAuthorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if ($authzDataScope !== null) {
            $this->checkAttendanceRecordAuthorization($authzDataScope, $companyId, $attendanceRecordIds);
        }

        $response =  $this->requestService->bulkLockAttendanceRecord(
            $request->only('company_id', 'attendance_record_ids')
        );

        try {
            $responseData = json_decode($response->getData(), true);
            $data = array_merge($request->only('company_id', 'attendance_record_ids'), $responseData);
            $this->audit($request, $companyId, $data);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/attendance/{employee_id}/{date}/unlock",
     *     summary="Lock an employee attendance record",
     *     description="Set the **LOCKED** property of a supplied record to the value **False**,
unlocking that record and allowing automatic computation of that record.

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: no attendance record for employee id/date found."
     *              }
     *         }
     *     ),
     * )
     */
    public function unlockAttendanceRecord(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $response     = $employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeUpdate(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->unlockAttendanceRecord($employeeId, $attendanceDate);

        try {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/attendance/unlock/bulk",
     *     summary="Bulk Unlock Attendance Records",
     *     description="Set the **LOCKED** property of supplied records to the value **False**,
unlocking those records will allow automatic computation of those records.

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bulkAttendanceUnlockRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="rows_affected", type="integer"),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "rows_affected": 0
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Invalid JSON body": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: JSON body is invalid."
     *              },
     *             "Company ID is required": {
     *                  "Code": 406,
     *                  "Message": "Company ID should not be empty."
     *              },
     *              "Invalid company_id": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: company_id must be a valid id and integer."
     *              },
     *              "Attendance Ids is required": {
     *                  "Code": 406,
     *                  "Message": "The attendance record ids field is required."
     *               },
     *              "Invalid list of Ids": {
     *                  "Code": "406",
     *                  "Message": "The attendance_record_ids.0 must be an integer."
     *              }
     *         }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="bulkAttendanceUnlockRequestItemCollection",
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="attendance_record_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         description="Attendance record ID"
     *     )
     * )
     */
    public function bulkUnlockAttendanceRecord(
        Request $request
    ): \Illuminate\Http\JsonResponse {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $this->validate($request, [
            'company_id' => 'required|integer|not_in:0',
            'attendance_record_ids' => 'required|array',
            'attendance_record_ids.*' => 'integer'
        ]);

        $companyId = $request->get('company_id');
        $attendanceRecordIds = $request->get('attendance_record_ids');

        $isAuthorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if ($authzDataScope !== null) {
            $this->checkAttendanceRecordAuthorization($authzDataScope, $companyId, $attendanceRecordIds);
        }

        $response =  $this->requestService->bulkUnlockAttendanceRecord(
            $request->only('company_id', 'attendance_record_ids')
        );

        try {
            $responseData = json_decode($response->getData(), true);
            $data = array_merge($request->only('company_id', 'attendance_record_ids'), $responseData);
            $this->audit($request, $companyId, $data);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/attendance/{employee_id}/{attendance_date}/edit",
     *     summary="Edit an Employee Attendance Record",
     *     description="Calling this endpoint sets the values of the attendance record to match the supplied data.
The supplied data must be valid.

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/attendanceEditItem"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *         ),
     *         examples={
     *              "Success response": {
     *                  "employee": {
     *                      "uid": 1,
     *                      "company_employee_id": "qwerty-12",
     *                      "full_name": "John Doe"
     *                  },
     *                  "date": "2018-06-01",
     *                  "locked": False,
     *                  "attendance": {
     *                      "rh": 480,
     *                      "uh": 480
     *                  },
     *                  "scheduled_shifts": {
     *                      {
     *                          "schedule_id": 1,
     *                          "name": "morning"
     *                      },
     *                      {
     *                          "schedule_id": 2,
     *                          "name": "afternoon"
     *                      }
     *                  },
     *                  "time_records": {
     *                      "first_clock_in": "2018-09-03 08:31",
     *                      "last_clock_out": "2018-09-03 17:04"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Invalid JSON body": {
     *                  "Code": "BadRequestError",
     *                  "Message": "BadRequestError: JSON body is invalid."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "Data not found": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: No attendance records matching supplied parameters found."
     *              },
     *              "Incorrect parameters supplied on URI": {
     *                  "Code": "NotFoundError",
     *                  "Message": "NotFoundError: No attendance records matching supplied parameters found."
     *              },
     *         }
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="attendanceEditItem",
     *     @SWG\Property(
     *         property="attendance_type",
     *         type="integer"
     *     ),
     *     example={
     *         "RH": 480,
     *         "UH": 480
     *     }
     * ),
     */
    public function editAttendanceRecord(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $response     = $employeeRequestService->getEmployee($employeeId);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll_group.id'),
            ]);
        } else {
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }


        $response = $this->requestService->editAttendanceRecord($employeeId, $attendanceDate, $request->all());

        try {
            $responseData = json_decode($response->getData(), true);
            unset($responseData['scheduled_shifts']);
            $this->audit($request, $companyId, $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/attendance/calculate/bulk",
     *     summary="Calculates attendance for all Employee IDs across a list of dates",
     *     description="Calculates attendance for all Employee IDs across a list of dates.
If there is any existing data for a given employee on a given date,
it is overwritten with the newly calculated result.

Authorization Scope : **create.attendance_computation**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/attendanceCalculateBulkRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
     *         description="HTTP/1.1 202 ACCEPTED",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "BadRequestError",
     *                  "Message": "Insufficient required request parameters."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "NotFoundError",
     *                  "Message": "The request could not be understood by the server due to malformed syntax. "
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_METHOD_NOT_ALLOWED,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "MethodNotAllowed",
     *                  "Message": "The method received in the request-line is known by the origin server but not
 supported by the target resource."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "InternalServerError",
     *                  "Message": "The server encountered an unexpected condition which prevented it from
 fulfilling the request."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="attendanceCalculateBulkRequestItemCollection",
     *     required={"company_id"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="employee_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="dates",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     * ),
     */
    public function bulkAttendanceCalculateByEmployees(
        Request $request,
        EmployeeAuthz $employeeAuthz
    ): JsonResponse {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $this->validate($request, [
            'company_id' => 'required|integer',
            'employee_ids' => 'array',
        ]);

        $companyId = $request->get('company_id');
        $employeeIds = $request->get('employee_ids');

        $authorized = false;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);

            if (empty($employeeIds)) {
                $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
            } else {
                $authorized = $employeeAuthz->isAllAuthorizedByIdAndCompanyId(
                    $companyId,
                    $employeeIds,
                    $authzDataScope
                );
            }
        } else {
            $companyResponse = $this->companyRequestService->get($request->get('company_id'));
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $authorized = $this->authorizationService->authorizeGenerateCalculation(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkAttendanceCalculateByEmployees($request->all());

        try {
            $this->audit($request, $companyId, ['Attendance Mass Calculate Trigger' => $request->all()]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response ;
    }

    /**
     * @SWG\Post(
     *     path="/attendance/company/{company_id}/{attendance_date}/calculate",
     *     summary="Calculates attendance for all employees of a given company on a given date.",
     *     description="Calculates attendance for all employees of a given company on a given date.
If there is any existing data for the given employee on the given date, it is overwritten with the
newly calculated results.

Authorization Scope : **create.attendance_computation**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="attendance_date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
     *         description="HTTP/1.1 202 ACCEPTED",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "BadRequestError",
     *                  "Message": "Date must be a valid YYYY-MM-DD value."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "NotFoundError",
     *                  "Message": "No company found for supplied company_id."
     *              },
     *              "": {
     *                  "Code": "NotFoundError",
     *                  "Message": "The request could not be understood by the server due to malformed syntax."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_METHOD_NOT_ALLOWED,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "MethodNotAllowed",
     *                  "Message": "The method received in the request-line is known by the origin server but not
 supported by the target resource."
     *              },
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Error messages",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "": {
     *                  "Code": "InternalServerError",
     *                  "Message": "The server encountered an unexpected condition which prevented it from
 fulfilling the request."
     *              },
     *         }
     *     ),
     * ),
     */
    public function bulkAttendanceCalculateByCompany(
        Request $request,
        $companyId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $companyData = (object)[
            'account_id' => $companyData->account_id,
            'company_id' => $companyData->id
        ];

        if (!$this->authorizationService->authorizeGenerateCalculation(
            $companyData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->bulkAttendanceCalculateByCompany($companyId, $attendanceDate);
    }

    /**
     * @SWG\Post(
     *     path="/attendance/records/upload",
     *     summary="Upload Attendance Records",
     *     description="Uploads Employee's Attendance Records data

Authorization Scope : **edit.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Schema(ref="#/definitions/uploadResponseItem")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      "job_id": "a1733799-c44a-4086-97e0-5e6c4f9603cd"
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Company not found",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "Code": "BadRequestError",
     *                  "Message": "Insufficient required request parameters: company_id."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_METHOD_NOT_ALLOWED,
     *         description="Company not found",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="Code", type="string"),
     *             @SWG\Property(property="Message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "Code": "MethodNotAllowed",
     *                  "Message": "The method received in the request-line is known by the origin
server but not supported by the target resource."
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="uploadResponseItem",
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(property="job_id", type="string")
     *     )
     * )
     */
    public function uploadAttendanceRecords(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $inputs = $request->all();

        $this->validate($request, [
            'company_id' => 'required|integer',
            'file' => 'required|file',
        ], [
            'file.required' => 'File is required.'
        ]);

        $authorized = false;
        $companyId = $inputs['company_id'];

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $authorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(AttendanceUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveAttendanceInfo($file->getPathName());

            $response = $this->requestService->uploadAttendanceInfo([
                'company_id' => $companyId,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $this->isAuthzEnabled($request)
                    ? $this->getAuthzDataScope($request)->getDataScope()
                    : null
            ]);

            $attendanceResponse = json_decode($response->getData());

            try {
                $this->audit($request, $companyId, ['Upload Attendance Records Trigger']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json($attendanceResponse);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * Check attendance records related employees authorization
     *
     * @param  AuthzDataScope $authzDataScope      Authz Data Scope
     * @param  int            $companyId           Company ID
     * @param  array          $attendanceRecordIds List of attendance record IDs
     */
    protected function checkAttendanceRecordAuthorization(
        AuthzDataScope $authzDataScope,
        int $companyId,
        array $attendanceRecordIds
    ) {
        $employeeRequestService = app()->make(EmployeeRequestService::class);

        $attendanceRecordEmployees = $this->requestService->getEmployeeIdsByAttendanceRecords(
            $companyId,
            $attendanceRecordIds
        );

        $employeeIds = array_column($attendanceRecordEmployees, 'employee_id');

        $employeesResponse = $employeeRequestService->getCompanyEmployeesById($companyId, $employeeIds);
        $employees = json_decode($employeesResponse->getData(), true);

        $unauthorizedEmployees = [];

        foreach ($employees as $employee) {
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_DEPARTMENT => $employee['department_id'],
                AuthzDataScope::SCOPE_POSITION => $employee['position_id'],
                AuthzDataScope::SCOPE_LOCATION => $employee['location_id'],
                AuthzDataScope::SCOPE_TEAM => $employee['team_id'],
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $employee['payroll_group_id']
            ]);

            if (!$isAuthorized) {
                $unauthorizedEmployees[] = sprintf(
                    '%s %s - %s',
                    $employee['first_name'],
                    $employee['last_name'],
                    $employee['employee_id']
                );
            }
        }

        if (!empty($unauthorizedEmployees)) {
            $employeeNames = implode(', ', $unauthorizedEmployees);

            abort(401, sprintf('No access to employees: %s', $employeeNames));
        }
    }

    /**
     * @SWG\Post(
     *     path="/attendance/records/export/job",
     *     summary="Start Job For Download Attendance Records",
     *     description="Download attendance records job. Supports searching, filtering, & sorting of attendance records.

Authorization Scope : **export.attendance_computation**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/downloadAttendanceRecordsItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="HTTP/1.1 200 OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 @SWG\Property(property="link", type="string"),
     *             ),
     *         ),
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      {
     *                          "job_id": "a4c6a79d-b6da-4da4-96aa-7ff056dcff97"
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     * ),
     * @SWG\Definition(
     *     definition="downloadAttendanceRecordsJob",
     *     @SWG\Property(
     *         property="ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="page",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="page_size",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="search_terms",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="integer"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *     ),
     *     @SWG\Property(
     *         property="filters",
     *         type="object",
     *         @SWG\Property(
     *              property="location",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="department",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *         @SWG\Property(
     *              property="position",
     *              type="array",
     *              @SWG\Items(type="integer"),
     *              collectionFormat="multi"
     *         ),
     *     ),
     *     @SWG\Property(
     *         property="sort_by",
     *         type="object",
     *         @SWG\Property(property="employee_uid", type="string"),
     *         @SWG\Property(property="employee_name", type="string"),
     *         @SWG\Property(property="date", type="string"),
     *         @SWG\Property(property="expected_shift", type="string"),
     *         @SWG\Property(property="first_clock_in", type="string"),
     *         @SWG\Property(property="last_clock_out", type="string"),
     *         @SWG\Property(property="computed_attendance", type="string"),
     *     )
     * ),
     */
    public function exportAttendanceRecordsJob(Request $request): \Illuminate\Http\JsonResponse
    {
        $inputs = $request->all();
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $companyResponse = $this->companyRequestService->get($request->get('company_id'));
        $companyData = json_decode($companyResponse->getData());
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $request->get('company_id')
            );
            $filterScope = $authzDataScope->getDataScope();
            unset($filterScope[AuthzDataScope::SCOPE_COMPANY]);
            $inputs['filters'] = array_merge_recursive(
                !empty($inputs['filters']) ? $inputs['filters'] : [],
                array_change_key_case($filterScope, CASE_LOWER)
            );
        } else {
            $companyData = (object)[
                'account_id' => $companyData->account_id,
                'company_id' => $companyData->id
            ];
            $isAuthorized = $this->authorizationService->authorizeExportComputedAttendance(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response =  $this->requestService->exportAttendanceRecordsJob($inputs);

        try {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyData->id, [], $responseData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    public function exportAttendanceRecordsJobStatus($jobId)
    {
        return $this->requestService->exportAttendanceRecordsJobStatus($jobId);
    }
}
