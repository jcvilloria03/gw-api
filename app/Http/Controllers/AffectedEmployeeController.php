<?php

namespace App\Http\Controllers;

use App\AffectedEmployee\AffectedEmployeeAuthorizationService;
use App\AffectedEmployee\AffectedEmployeeRequestService;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AffectedEmployeeController extends Controller
{
    /**
     * @var \App\AffectedEmployee\AffectedEmployeeRequestService
     */
    private $requestService;

    /**
     * @var \App\AffectedEmployee\AffectedEmployeeAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        AffectedEmployeeRequestService $requestService,
        AffectedEmployeeAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{company_id}/affected_employees/search",
     *     summary="Get affected employees by term",
     *     description="Get affected employees by term
Authorization Scope : [**view.department**, **view.position**, **view.location**, **view.employee**]",
     *     tags={"affected_employees"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Search limit",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="without_users",
     *         in="query",
     *         description="Do not include users",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="admin_users",
     *         in="query",
     *         description="Include only admin users",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function search($id, Request $request)
    {
        $authorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
                'id' => $id
            ];
            $authorized = $this->authorizationService->authorizeGet($data, $request->attributes->get('user'));
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->search(
            $id,
            $request->input('term', ''),
            $request->input('limit'),
            $request->input('without_users', false),
            $request->input('admin_users', false),
            $authzDataScope
        );
    }
}
