<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\BasePay\BasicPayAuditService;
use App\BasicPayAdjustment\BasicPayAdjustmentRequestService;
use App\BasicPayAdjustment\BasicPayAdjustmentAuthorizationService;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;
/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BasicPayAdjustmentController extends Controller
{
    use AuditTrailTrait;

    const PER_HOUR = 'per_hour';
    const PER_DAY = 'per_day';
    const PER_MONTH = 'per_month';
    const PER_YEAR = 'per_year';

    const UNITS = [
        self::PER_HOUR,
        self::PER_DAY,
        self::PER_MONTH,
        self::PER_YEAR
    ];

    private $requestService;

    private $authorizationService;

    private $employeeService;

    /*
     * App\Audit\AuditService
    */
    protected $auditService;

    public function __construct(
        BasicPayAdjustmentRequestService $requestService,
        BasicPayAdjustmentAuthorizationService $authorizationService,
        EmployeeRequestService $employeeService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->employeeService = $employeeService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/employee/{employeeId}/basic_pay_adjustment",
     *     summary="Create Basic Pay Adjustment",
     *     description="Create Basic Pay Adjustment
    Authorization Scope : **create.basic_pay**",
     *     tags={"basic_pay_adjustment"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="amount",
     *         in="formData",
     *         description="Basic Pay Adjustment Amount",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="unit",
     *         in="formData",
     *         description="Basic Pay Adjustment Unit",
     *         required=true,
     *         type="string",
     *         enum=App\Http\Controllers\BasicPayAdjustmentController::UNITS,
     *     ),
     *     @SWG\Parameter(
     *         name="effective_date",
     *         description="Basic Pay Adjustment Effective Date",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="adjustment_date",
     *         description="Basic Pay Adjustment Date",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="reason",
     *         description="Basic Pay Adjustment Reason",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function create(Request $request, $employeeId)
    {
        $employeeResponse = $this->employeeService->getEmployee($employeeId);
        if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
            return $employeeResponse;
        }

        $employee = json_decode($employeeResponse->getData(), true);
        $companyId = array_get($employee, 'company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create(
            (int) $employeeId,
            $request->all()
        );

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $responseData);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/basic_pay_adjustment/{id}",
     *     summary="Get Basic Pay Adjustment",
     *     description="Get Basic Pay Adjustments
    Authorization Scope : **view.basic_pay**",
     *     tags={"basic_pay_adjustment"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Basic Pay Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */

    public function get(Request $request, $id)
    {
        // authorize
        $response = $this->requestService->get($id);
        $adjustment = json_decode($response->getData());

        if (empty($adjustment)) {
            // no contents anyway, so don't check authorization
            return $response;
        }

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $this->employeeService->getEmployee($adjustment->employee_id)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $adjustment->company_id,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($adjustment->company_id),
                'company_id' => $adjustment->company_id
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $data,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/basic_pay_adjustments",
     *     summary="Get Basic Pay Adjustments",
     *     description="Get Basic Pay Adjustments for one employee
    Authorization Scope : **view.basic_pay**",
     *     tags={"basic_pay_adjustment"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */

    public function getByEmployeeId(Request $request, $employeeId)
    {
        // authorize
        $response = $this->requestService->getByEmployeeId($employeeId);
        $data = json_decode($response->getData())->data;

        if (empty($data)) {
            // no contents anyway, so don't check authorization
            return $response;
        }

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $this->employeeService->getEmployee($employeeId)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $adjustment = (object) [
                'account_id' => Company::getAccountId($data[0]->company_id),
                'company_id' => $data[0]->company_id
            ];

            $isAuthorized = $this->authorizationService->authorizeGetAll(
                $adjustment,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/employee/{employeeId}/basic_pay_adjustment/{adjustmentId}/update",
     *     summary="Update Basic Pay Adjustment",
     *     description="Update Basic Pay Adjustment
     Authorization Scope : **edit.basic_pay**",
     *     tags={"basic_pay_adjustment"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="adjustmentId",
     *         in="path",
     *         description="Basic Pay Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="amount",
     *         in="formData",
     *         description="Basic Pay Adjustment Amount",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="unit",
     *         in="formData",
     *         description="Basic Pay Adjustment Unit",
     *         required=true,
     *         type="string",
     *         enum=App\Http\Controllers\BasicPayAdjustmentController::UNITS,
     *     ),
     *     @SWG\Parameter(
     *         name="effective_date",
     *         description="Basic Pay Adjustment Effective Date",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="adjustment_date",
     *         description="Basic Pay Adjustment Date",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="reason",
     *         description="Basic Pay Adjustment Reason",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function update(Request $request, $employeeId, $id)
    {
        $employeeResponse = $this->employeeService->getEmployee($employeeId);
        if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
            return $employeeResponse;
        }

        $updatedBy = $request->attributes->get('user');
        $oldResponse = $this->requestService->get($id);

        $employee = json_decode($employeeResponse->getData(), true);
        $companyId = array_get($employee, 'company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $data,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $updateResponse = $this->requestService->update(
            (int) $employeeId,
            (int) $id,
            $request->all()
        );

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        if ($updateResponse->isSuccessful()) {
            $oldData = json_decode($oldResponse->getData(), true);
            $newData = json_decode($updateResponse->getData(), true);
            // trigger audit trail
            $this->audit($request, $companyId, $newData, $oldData, true);
        }

        return $updateResponse;
    }

     /**
     * @SWG\Delete(
     *     path="/employee/{employeeId}/basic_pay_adjustment/delete",
     *     summary="Delete Basic Pay Adjustment",
     *     description="Delete Basic Pay Adjustment
     Authorization Scope : **delete.basic_pay**",
     *     tags={"basic_pay_adjustment"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="query",
     *         description="Basic Pay Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function delete(Request $request, $employeeId)
    {
        $employeeResponse = $this->employeeService->getEmployee($employeeId);
        if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
            return $employeeResponse;
        }

        $deletedBy = $request->attributes->get('user');

        $employee = json_decode($employeeResponse->getData(), true);
        $companyId = array_get($employee, 'company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete(
                $data,
                $deletedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $employeeBasicPayAdjustmentsResponse = $this->requestService->getByEmployeeId($employeeId);
        $oldEmployeeBasicPayAdjustments = collect(
            array_get(json_decode($employeeBasicPayAdjustmentsResponse->getData(), true), 'data', [])
        )->filter(function ($basicPayAdjustment) use ($request) {
            return in_array($basicPayAdjustment['id'], array_get($request->all(), 'ids', []));
        });

        $deleteResponse = $this->requestService->delete(
            (int) $employeeId,
            $request->all()
        );

        if ($deleteResponse->getStatusCode() !== Response::HTTP_NO_CONTENT) {
            return $deleteResponse;
        }

        if ($deleteResponse->isSuccessful()) {
            foreach ($oldEmployeeBasicPayAdjustments as $oldEmployeeBasicPayAdjustment) {
                // trigger audit trail
                $this->audit($request, $companyId, [], $oldEmployeeBasicPayAdjustment);
            }
        }

        return $deleteResponse;
    }
}
