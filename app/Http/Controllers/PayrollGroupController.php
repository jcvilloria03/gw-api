<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\Facades\Company;
use App\Payroll\PayrollRequestService;
use App\PayrollGroup\PayrollGroupAuthorizationService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\PayrollGroup\PayrollGroupAuditService;
use App\PayrollGroup\PayrollGroupUploadTask;
use App\PayrollGroup\PayrollGroupUploadTaskException;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class PayrollGroupController extends Controller
{
    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /*
     * App\PayrollGroup\PayrollGroupRequestService
     */
    protected $requestService;

    /*
     * App\PayrollGroup\PayrollGroupAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Payroll\PayrollRequestService
     */
    private $payrollRequestService;

    /**
     * @var App\CSV\CsvValidator
     */
    protected $csvValidator;

    public function __construct(
        PayrollGroupAuthorizationService $authorizationService,
        PayrollGroupRequestService $requestService,
        AuditService $auditService,
        PayrollRequestService $payrollRequestService,
        CsvValidator $csvValidator
    ) {
        $this->authorizationService = $authorizationService;
        $this->requestService = $requestService;
        $this->auditService = $auditService;
        $this->payrollRequestService = $payrollRequestService;
        $this->csvValidator = $csvValidator;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/payroll_group/is_name_available",
     *     summary="Is name available",
     *     description="Checks availability of payroll group name",
     *     tags={"payroll_group"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Payroll Group Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="formData",
     *         description="Payroll Group Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=\Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="available", type="boolean", example=true)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=\Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=\Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Id must be a number": { "message": "The id must be a number.", "status_code": 406 },
     *              "Id is invalid": { "message": "The selected id is invalid.", "status_code": 406 },
     *              "Name field is required": { "message": "The name field is required.", "status_code": 406 },
     *              "Payroll Group Id is invalid": {
     *                  "message": "The selected payroll group id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        if ($this->isAuthzEnabled($request)
            && !$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');
        $payrollGroupData = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeIsNameAvailable($payrollGroupData, $user)
        ) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/account/payroll_groups",
     *     summary="Get account payroll groups",
     *     description="Get list of Payroll Group within the Account

Authorization Scope : **view.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *         examples={
     *              "application/json": {
     *                  "data": {
     *                      {
     *                          "id": 559,
     *                          "account_id": 269,
     *                          "company_id": 205,
     *                          "name": "Payroll Group A",
     *                          "payroll_frequency": "MONTHLY",
     *                          "day_factor": "260.00",
     *                          "hours_per_day": "8.00",
     *                          "non_working_day_option": "BEFORE",
     *                          "pay_date": "2017-09-30",
     *                          "cut_off_date": "2017-09-25",
     *                          "pay_date_2": "",
     *                          "cut_off_date_2": "",
     *                          "end_of_month": true
     *                      },
     *                      {
     *                          "id": 561,
     *                          "account_id": 269,
     *                          "company_id": 206,
     *                          "name": "Payroll Group B",
     *                          "payroll_frequency": "MONTHLY",
     *                          "day_factor": "260.00",
     *                          "hours_per_day": "8.00",
     *                          "non_working_day_option": "AFTER",
     *                          "pay_date": "2017-09-30",
     *                          "cut_off_date": "2017-09-25",
     *                          "pay_date_2": "",
     *                          "cut_off_date_2": "",
     *                          "end_of_month": false
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAccountPayrollGroups(Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->requestService->getAccountPayrollGroups($user['account_id']);
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $payrollGroupData = json_decode($response->getData());
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => data_get($payrollGroupData, 'data.*.company_id'),
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($payrollGroupData, 'data.*.id')
                ]
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetAccountPayrollGroups(
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll_group/{id}",
     *     summary="Delete payroll group",
     *     description="Delete payroll group

Authorization Scope : **delete.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Group ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll Group not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function delete($id, Request $request)
    {
        $deletedBy = $request->attributes->get('user');

        // get original payroll group details
        $payrollGroupResponse = $this->requestService->get($id);
        $payrollGroupData = json_decode($payrollGroupResponse->getData());

        // authorize
        if (
            !$this->authorizationService->authorizeDelete(
                $payrollGroupData,
                $deletedBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->delete($id);

        // audit log payroll group deletion
        $payrollGroupData = json_decode($payrollGroupResponse->getData(), true);
        $details = [
            'old' => $payrollGroupData,
        ];
        $item = new AuditCacheItem(
            PayrollGroupAuditService::class,
            PayrollGroupAuditService::ACTION_DELETE,
            new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll_group/bulk_delete",
     *     summary="Delete multiple payroll groups",
     *     description="Delete multiple payroll groups
Authorization Scope : **delete.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll Group IDs",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "The company id must be an integer.": {
     *                  "message": "The company id must be an integer.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "The company id must be at least 1.": {
     *                  "message": "The company id must be at least 1.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "The Payroll Group IDs field is required.": {
     *                  "message": "The Payroll Group IDs field is required.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "The Payroll Group IDs must be an array.": {
     *                  "message": "The Payroll Group IDs must be an array.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "The Payroll Group IDs must be an integer.": {
     *                  "message": "The Payroll Group IDs must be an integer.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "The selected Payroll Group IDs is invalid.": {
     *                  "message": "The selected Payroll Group IDs is invalid.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *              "Deletion of payroll group assigned to employee is not permitted.": {
     *                  "message": "Deletion of payroll group assigned to employee is not permitted.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              },
     *         }
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $deletedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $payrollGroupIds = $request->input('payroll_group_ids', []);

        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)
            && !$authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds
            ])
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkDelete($request->all());

        if ($response->isSuccessful()) {
            foreach ($request->input('payroll_group_ids', []) as $id) {
                $item = new AuditCacheItem(
                    PayrollGroupAuditService::class,
                    PayrollGroupAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $companyId
                        ]
                    ]
                );

                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payroll_group_periods",
     *     summary="Get Company Payroll Groups with payroll period",
     *     description="Get list of company payroll group with generated payroll period",
     *     tags={"payroll_group"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getCompanyPayrollGroupsWithPeriods(Request $request, $id)
    {
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)
            && !$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->payrollRequestService->getCompanyPayrollGroupsWithPeriods($id, $authzDataScope);
    }

    /**
     * @SWG\Post(
     *     path="/payroll_group/upload",
     *     summary="Upload Payroll Groups",
     *     description="Upload Payroll Groups
Authorization Scope : **create.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "payroll_groups_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = \App::make(PayrollGroupUploadTask::class);

            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => PayrollGroupUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            \Amqp::publish(config('queues.payroll_group_validation_queue'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/payroll_group/upload/status",
     *     summary="Get Job Status",
     *     description="Get Payroll Group Upload Status
Authorization Scope : **create.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\PayrollGroup\PayrollGroupUploadTask::PROCESS_VALIDATION,
     *              App\PayrollGroup\PayrollGroupUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Payroll Group is invalid"
     *                          },
     *                          "4": {
     *                              "Pay Frequency is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(PayrollGroupUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (PayrollGroupUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, PayrollGroupUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(",", PayrollGroupUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === PayrollGroupUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === PayrollGroupUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }
        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/payroll_group/upload/save",
     *     summary="Save Payroll Groups",
     *     description="Saves Payroll Groups from Previously Uploaded CSV
Authorization Scope : **create.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "payroll_groups_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll groups needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $request->input('company_id')
            );
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(PayrollGroupUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(PayrollGroupUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (PayrollGroupUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        \Amqp::publish(config('queues.payroll_group_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/payroll_group/upload/preview",
     *     summary="Get Payroll Groups Preview",
     *     description="Get Payroll Groups Upload Preview
Authorization Scope : **create.payroll_group**",
     *     tags={"payroll_group"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(PayrollGroupUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (PayrollGroupUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="payroll_group/{payrollGroupId}/periods",
     *     summary="Get Payroll Group periods",
     *     description="Get the list of the payroll group's period",
     *     tags={"payroll_group"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="payrollGroupId",
     *         in="path",
     *         description="Payroll Group Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getPayrollGroupPeriods(Request $request, $payrollGroupId)
    {
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)
            && !$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $payrollGroupId)
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->payrollRequestService->getPayrollGroupPeriods($payrollGroupId, $authzDataScope);
    }
}
