<?php

namespace App\Http\Controllers;

use App\AllowanceType\AllowanceTypeAuthorizationService;
use App\AllowanceType\PhilippineAllowanceTypeRequestService;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class PhilippineAllowanceTypeController extends Controller
{
    /**
     * @var \App\AllowanceType\PhilippineAllowanceTypeRequestService
     */
    protected $requestService;

    /**
     * @var \App\AllowanceType\AllowanceTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncomeType\OtherIncomeTypeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineAllowanceTypeRequestService $requestService,
        AllowanceTypeAuthorizationService $allowanceTypeAuthorizationService,
        OtherIncomeTypeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $allowanceTypeAuthorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/allowance_type/bulk_create",
     *     summary="Create Multiple Allowance Types",
     *     description="Create multiple allowance types,
Authorization Scope : **create.allowance_types**",
     *     tags={"allowance_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="allowance_types",
     *         in="body",
     *         description="Create multiple allowance types",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/allowanceTypesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "name": "Commission Type Name",
     *                          "company_id": 1,
     *                          "fully_taxable": false,
     *                          "basis": "FIXED",
     *                          "max_non_taxable": false,
     *                          "type_name": "App\Model\PhilippineCommissionType",
     *                          "deleted_at": null
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="allowanceTypesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/allowanceTypesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     *
     * @SWG\Definition(
     *     definition="allowanceTypesRequestItem",
     *     required={"name", "fully_taxable"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Name of allowance type",
     *         example="Fuel Allowance"
     *     ),
     *     @SWG\Property(
     *         property="fully_taxable",
     *         type="boolean",
     *         enum={1,0},
     *         description="Is allowance fully taxable",
     *         example="0"
     *     ),
     *      @SWG\Property(
     *         property="max_non_taxable",
     *         type="number",
     *         description="If allowance is not fully taxable, specify max non taxable amount",
     *         example="100"
     *     )
     * )
     */
    public function bulkCreate(Request $request, $id)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];
            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($id, $inputs);

        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $otherIncomeTypes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if ($response->isSuccessful()) {
            $this->auditService->mapAndQueueAuditItems(
                $otherIncomeTypes,
                OtherIncomeTypeAuditService::ACTION_CREATE,
                $createdBy
            );
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/allowance_type/is_name_available",
     *     summary="Is philippine allowance type name available",
     *     description="Check availability of philippine allowance type name with the given company,
Authorization Scope : **create.allowance_types**",
     *     tags={"allowance_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Type name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="other_income_type_id",
     *         in="formData",
     *         description="Other Income Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="object", @SWG\Property(property="available", type="boolean", example=true))
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Id required": {"message": "The id field is required.", "status_code": 406},
     *              "Other income type id must be an integer": {
     *                  "message": "The other income type id must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Name is required": {"message": "The name field is required.", "status_code": 406},
     *         }
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $createdBy = $request->attributes->get('user');
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];

            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->isNameAvailable($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/allowance_type/{id}",
     *     summary="Update the allowance type",
     *     description="Update the allowance type
Authorization Scope : **edit.allowance_types**",
     *     tags={"allowance_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         type="string",
     *         required=true,
     *         description="Name of allowance type"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="fully_taxable",
     *         type="integer",
     *         enum={1,0},
     *         required=true,
     *         description="Is allowance fully taxable"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="max_non_taxable",
     *         type="number",
     *         description="If allowance is not fully taxable, specify max non taxable amount"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", example="Other Income Type not found."),
     *              @SWG\Property(property="status_code", type="integer", example=404),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *              "Default": {
     *                  "id": "The id field is required",
     *                  "name": "The name field is required",
     *                  "fully_taxable": "The fully taxable field is required",
     *              }
     *         }
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeTypeRequestService $otherIncomeTypeRequestService,
        $id
    ) {
        $allowanceTypeResponse = $otherIncomeTypeRequestService->get($id);
        if (!$allowanceTypeResponse->isSuccessful()) {
            return $allowanceTypeResponse;
        }

        $allowanceTypeData = json_decode($allowanceTypeResponse->getData(), true);
        $companyId = array_get($allowanceTypeData, 'company_id');
        $updatedBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeUpdate($authData, $updatedBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $request->all());
        if ($response->isSuccessful()) {
            $this->auditService->mapAndQueueAuditItems(
                [json_decode($response->getData(), true)],
                OtherIncomeTypeAuditService::ACTION_UPDATE,
                $updatedBy,
                [$allowanceTypeData]
            );
        }

        return $response;
    }
}
