<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shift\ShiftRequestService;
use App\Http\Controllers\EssBaseController;
use App\Shift\EssShiftsAuthorizationService;

class EssShiftController extends EssBaseController
{
    /**
     * @var \App\Shift\ShiftRequestService
     */
    protected $scheduleRequestService;

    public function __construct(
        ShiftRequestService $requestService,
        EssShiftsAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/shift/overlapping_requests",
     *     summary="Get Overlaping requests with Shifts",
     *     description="Get Overlaping requests with Shifts.
Authorization Scope : **ess.view.shift**",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="shift_ids[]",
     *         in="formData",
     *         description="Shift IDs",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_ids[]",
     *         in="formData",
     *         description="Employee IDs",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         in="formData",
     *         description="Shift Start Date in format YYYY-MM-DD",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         in="formData",
     *         description="Shift End Date in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getShiftsOverlappingRequests(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeGet($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getShiftsOverlappingRequests($request->all());
    }
}
