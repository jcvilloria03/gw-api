<?php

namespace App\Http\Controllers\Concerns;

use App\Auth0\Auth0UserService;
use App\Model\Auth0User;

trait ProcessesEmployeeEmailUpdatesTrait
{
    /**
     * Process update of employee email in auth0
     *
     * @param array $emloyeeData
     *
     * @return void
     */
    public function processEmployeeEmailUpdate($employeeData, array $attributes = [])
    {
        $email = $attributes['email'] ?? $employeeData['email'];
        $auth0UserService = app()->make(Auth0UserService::class);
        $auth0User = $auth0UserService->getUserByEmployeeId($employeeData['id']);
        $targetEmailAuth0Data = $auth0UserService->getAuth0UserProfileByEmail($email);

        if ($auth0User && !empty($targetEmailAuth0Data)) {
            if ($auth0User->auth0_user_id != $targetEmailAuth0Data['user_id']) {
                # Use the auth0_user_id from the target email
                $auth0User->auth0_user_id = $targetEmailAuth0Data['user_id'];
            }
        }

        if ($auth0User && !Auth0UserService::isPreactive($auth0User)) {
            if (empty($targetEmailAuth0Data)) {
                # Auth0 Email doesn't exist yet.
                $auth0UserService->updateEmployeeUser(
                    $auth0User,
                    ['email' => $employeeData['email']]
                );
            }
        }

        if ($auth0User->status === 'active') {
            $auth0UserService->setStatus($auth0User->user_id, Auth0User::STATUS_INACTIVE);
            $auth0UserService->updateEmployeeAuth0UserId(
                $auth0User,
                ['auth0_user_id' => "preactive|{$employeeData['account_id']}-{$employeeData['user_id']}"]
            );
        }
    }
}
