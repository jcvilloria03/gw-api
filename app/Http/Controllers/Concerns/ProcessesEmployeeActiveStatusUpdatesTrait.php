<?php

namespace App\Http\Controllers\Concerns;

use App\Model\Auth0User;
use App\Auth0\Auth0UserService;
use Illuminate\Support\Facades\Log;
use App\Auth0\Auth0ManagementService;
use App\CompanyUser\CompanyUserService;
use App\Role\DefaultRoleService;
use App\Role\UserRoleService;

trait ProcessesEmployeeActiveStatusUpdatesTrait
{
    /**
     * Handle update of employee status
     *
     * @param array                   $employeeData
     * @param bool                    $activeStatus
     * @param int                     $editedBy
     *
     * @return void
     */
    private function processEmployeeActiveStatus(
        array $employeeData,
        bool $activeStatus,
        Auth0User $auth0User
    ) {
        $auth0UserService = app()->make(Auth0UserService::class);
        $companyUserService = $this->loadCompanyUserService();
        // employee activation
        if ($activeStatus) {
            // auth0User doesn't exist
            if (Auth0UserService::isPreactive($auth0User)) {
                $companyUser = $companyUserService->getByEmployeeId($employeeData['id']);

                // create user in Auth0
                $auth0User = $auth0UserService->createEmployeeUserInManagement(
                    [
                        'email'           => $employeeData['email'],
                        'first_name'      => $employeeData['first_name'],
                        'last_name'       => $employeeData['last_name'],
                        'middle_name'     => $employeeData['middle_name'],
                        'account_id'      => $employeeData['account_id'] ?? $employeeData['company']['account_id'],
                        'company_user_id' => $employeeData['user_id'] ?? $companyUser->user_id,
                        'active'          => $activeStatus,
                    ],
                    $auth0User
                );
            } elseif (!$auth0User->isActive()) {
                // auth0User exists and it is not active
                $auth0UserService->setStatus($auth0User->user_id, Auth0User::STATUS_ACTIVE);
                $auth0ManagementService = app()->make(Auth0ManagementService::class);
                try {
                    $userAuth0Profile = $auth0ManagementService->getAuth0UserProfile($auth0User->auth0_user_id);
                    if (!$userAuth0Profile['email_verified']) {
                        $auth0ManagementService->verifyEmail($auth0User->auth0_user_id);
                    } else {
                        $auth0UserService->setStatus($auth0User->user_id, Auth0User::STATUS_ACTIVE);
                    }
                } catch (\Exception $e) {
                    Log::info('Could not send verification email to auth0 user : ' . $e->getMessage());
                }
            }
            $defaultRoleService = app()->make(DefaultRoleService::class);
            $employeeDefaultRole = $defaultRoleService->getEmployeeSystemRole(
                $employeeData['account_id'],
                $employeeData['company_id']
            );

            $assignedCompanies = [[
                'company_id'  => $employeeData['company_id'],
                'employee_id' => $employeeData['employee_id'],
                'roles_ids'   => [$employeeDefaultRole->id]
            ]];

            $this->assignDefaultEmployeeRolesToUser(
                $auth0User,
                $employeeData['account_id'],
                $assignedCompanies
            );
        }
    }

    /**
     * Assigns default employee roles to given auth0User
     *
     * @param  \App\Model\Auth0User $auth0User
     * @param  int                  $accountId
     * @param  array                $assignedCompanies
     * @return void
     */
    private function assignDefaultEmployeeRolesToUser(Auth0User $auth0User, $accountId, array $assignedCompanies)
    {
        // Assign defualt roles per assigned company
        $defaultRoleService = app()->make(DefaultRoleService::class);
        foreach ($assignedCompanies as $assignedCompany) {
            // get default system role for employee
            $defaultRole = $defaultRoleService->getEmployeeSystemRole(
                $accountId,
                $assignedCompany['company_id']
            );

            $this->assignRoleToUser($auth0User, $defaultRole);
        }
    }

    /**
     * Assigns given role to user if not yet assigned
     *
     * @param  \App\Model\Auth0User $auth0User
     * @param  \App\Model\Role      $role
     * @return void
     */
    private function assignRoleToUser($auth0User, $role)
    {
        // Check if employee default role is not yet set for user
        $userRoleService = app()->make(UserRoleService::class);
        $userRoles = $userRoleService->getWithConditions([
            ['user_id', $auth0User->user_id],
            ['role_id', $role->id]
        ]);

        if ($userRoles->isEmpty()) {
            $userRoleService->create([
                'role_id' => $role->id,
                'user_id' => $auth0User->user_id,
            ]);
        }
    }

    /**
     * Load trati's class dependency
     *
     * @return companyUserService
     */
    private function loadCompanyUserService()
    {
        if (isset($this->companyUserService)) {
            return $this->companyUserService;
        } else {
            return app()->make(CompanyUserService::class);
        }
    }
}
