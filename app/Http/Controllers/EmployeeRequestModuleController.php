<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\EmployeeRequestModule\EmployeeRequestModuleRequestService;
use App\EmployeeRequestModule\EmployeeRequestModuleAuthorizationService;
use Symfony\Component\HttpFoundation\Response;

class EmployeeRequestModuleController extends Controller
{
    /**
     * @var \App\EmployeeRequstModule\EmployeeRequestModuleRequestService
     */
    private $requestService;

    /**
     * @var \App\EmployeeRequstModule\EmployeeRequestModuleAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        EmployeeRequestModuleRequestService $requestService,
        EmployeeRequestModuleAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/employee_request_module",
     *     summary="Index EmployeeRequestModule",
     *     description="Index EmployeeRequestModule Details

Authorization Scope : **view.employee_request_module**",
     *     tags={"employee_request_module"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function index(Request $request)
    {

        $response = $this->requestService->get();
        $data = json_decode($response->getData());
        $data->account_id = $request->attributes->get('user')['account_id'];
        $data->company_id = $request->attributes->get('user')['employee_company_id'];
  
        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled && !empty($data->company_id)) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $data->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($data, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }
}
