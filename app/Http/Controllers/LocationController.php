<?php

namespace App\Http\Controllers;

use App\Facades\Company;
use App\Location\LocationAuthorizationService;
use App\Location\LocationEsIndexQueueService;
use Illuminate\Http\Request;
use App\Location\LocationRequestService;
use App\Audit\AuditService;
use App\Audit\AuditItem;
use App\Authz\AuthzDataScope;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;

class LocationController extends Controller
{
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';
    const ACTION_UPDATE = 'update';

    const OBJECT_NAME = 'location';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /*
     * App\Location\LocationRequestService
     */
    protected $requestService;

    /**
     * @var \App\Location\LocationAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Location\LocationEsIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        LocationRequestService $requestService,
        LocationAuthorizationService $authorizationService,
        AuditService $auditService,
        LocationEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/location/{id}",
     *     summary="Get Philippine Location",
     *     description="Get Philippine Location Details

Authorization Scope : **view.location**",
     *     tags={"location"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Location ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        // authorize
        $response = $this->requestService->get($id);
        $locationData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($locationData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        // call microservice
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company/{id}/locations",
     *     summary="Get Company Locations",
     *     description="Get Company Locations

Authorization Scope : **view.location**",
     *     tags={"location"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLocations(Request $request, $companyId)
    {
        $isAuthorized = false;
        
        // authorize
        if ($this->isAuthzEnabled($request)) {
            $response = $this->requestService->getCompanyLocations(
                $companyId,
                $request->attributes->get('authz_data_scope')
            );
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $response = $this->requestService->getCompanyLocations($companyId);
            $locationData = json_decode($response->getData())->data;
            $location = $locationData[0];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyLocations(
                $location,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/location/",
     *     summary="Create company location",
     *     description="Get philippine company location

Authorization Scope : **create.location**",
     *     tags={"location"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Location Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_1",
     *         in="formData",
     *         description="Address line 1",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_2",
     *         in="formData",
     *         description="Address line 2",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="City",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="region",
     *         in="formData",
     *         description="Region",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="zip_code",
     *         in="formData",
     *         description="Zip Code",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $locationData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($locationData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->create($inputs);
        $responseData = json_decode($response->getData(), true);

        $this->indexQueueService->queue([$responseData['id']]);

        // audit log
        $locationResponse = $this->requestService->get($responseData['id']);
        $locationData = json_decode($locationResponse->getData(), true);
        $locationData['headquarters'] = empty($locationData['headquarters']) ? 'false' : 'true';
        $item = new AuditItem([
            'company_id' => $inputs['company_id'],
            'account_id' => $createdBy['account_id'],
            'user_id' => $createdBy['user_id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $locationData
        ]);
        $this->auditService->log($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/location/is_name_available",
     *     summary="Is name available",
     *     description="Checks availability of location name",
     *     tags={"location"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Location Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="location_id",
     *         in="formData",
     *         description="Location Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, $companyId)
    {
        $response = $this->requestService->isNameAvailable($companyId, $request->all());

        $user = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $locationData = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($locationData, $user);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
        
        return $response;
    }
}
