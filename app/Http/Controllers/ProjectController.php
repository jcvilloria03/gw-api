<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\Project\ProjectAuditService;
use App\Project\ProjectAuthorizationService;
use Illuminate\Http\Request;
use App\Project\ProjectRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

class ProjectController extends Controller
{
    /**
     * @var \App\Project\ProjectRequestService
     */
    private $requestService;

    /**
     * @var \App\Project\ProjectAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        ProjectRequestService $requestService,
        ProjectAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/project/{id}",
     *     summary="Get Project",
     *     description="Get Project Details

Authorization Scope : **view.project**",
     *     tags={"project"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Project ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $projectData = json_decode($response->getData());
        $projectData->account_id = Company::getAccountId($projectData->company_id);
        if (!$this->authorizationService->authorizeGet($projectData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     deprecated=true,
     *     path="/project/",
     *     summary="Create company project type",
     *     description="Create company project

Authorization Scope : **create.project**",
     *     tags={"project"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Project Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $projectData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($projectData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $projectGetResponse = $this->requestService->get($responseData['id']);
        $projectData = json_decode($projectGetResponse->getData(), true);
        $details = [
            'new' => $projectData,
        ];
        $item = new AuditCacheItem(
            ProjectAuditService::class,
            ProjectAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/project/bulk_create",
     *     summary="Create multiple company projects",
     *     description="Create multiple company projects

Authorization Scope : **create.project**",
     *     tags={"project"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newProjects"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newProjects",
     *     required={"company_id", "names"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="names",
     *         type="array",
     *         @SWG\Items(
     *             type="string"
     *         )
     *     )
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $projectData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
    
            $isAuthorized = $this->authorizationService->authorizeCreate($projectData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        foreach ($responseData['ids'] as $id) {
            $projectGetResponse = $this->requestService->get($id);
            $projectData = json_decode($projectGetResponse->getData(), true);
            $details = [
                'new' => $projectData,
            ];
            $item = new AuditCacheItem(
                ProjectAuditService::class,
                ProjectAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/project/is_name_available",
     *     summary="Is project name available",
     *     description="Check availability of project name with the given company",
     *     tags={"project"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Project Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="project_id",
     *         in="formData",
     *         description="Project Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $projectData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($projectData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/projects",
     *     summary="Get all Projects",
     *     description="Get all Projects within company.
Authorization Scope : **view.project**",
     *     tags={"project"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyProjects($id, Request $request)
    {
        $response = $this->requestService->getCompanyProjects($id);
        $projectData = json_decode($response->getData())->data;
        if (empty($projectData)) {
            return $response;
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $project = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyProjects(
                $project,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/project/{id}",
     *     summary="Update Project",
     *     description="Update Project
     Authorization Scope : **edit.project**",
     *     tags={"project"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Project Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Project Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldProjectData = json_decode($response->getData());
        $oldProjectData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldProjectData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldProjectData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldProjectData = json_decode($response->getData(), true);
        $newProjectData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldProjectData,
            'new' => $newProjectData,
        ];
        $item = new AuditCacheItem(
            ProjectAuditService::class,
            ProjectAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
    * @SWG\Delete(
    *     path="/project/bulk_delete",
    *     summary="Delete multiple Projects",
    *     description="Delete multiple Projects
    Authorization Scope : **delete.project**",
    *     tags={"project"},
    *     consumes={"application/x-www-form-urlencoded"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="company_id",
    *         in="formData",
    *         description="Company ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="project_ids[]",
    *         type="array",
    *         in="formData",
    *         description="Project Ids",
    *         required=true,
    *         @SWG\Items(type="integer"),
    *         collectionFormat="multi"
    *     ),
    *     @SWG\Response(
    *         response=204,
    *         description="Successful operation",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Company not found.",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $projectData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($projectData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        if ($response->isSuccessful()) {
            foreach ($request->input('project_ids', []) as $id) {
                $item = new AuditCacheItem(
                    ProjectAuditService::class,
                    ProjectAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $request->get('company_id')
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }
}
