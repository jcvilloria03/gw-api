<?php

namespace App\Http\Controllers;

use Swagger\Annotations as SWG;
use Validator;
use App\Bank\BankRequestService;
use App\Bank\CompanyBankAuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Bank\CompanyBankAuthorizationService;
use App\Company\PhilippineCompanyRequestService;

class CompanyBankController extends Controller
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Bank\CompanyBankAuthorizationService;
     */
    protected $authorizationService;

    /**
     * @var App\Company\PhilippineCompanyRequestService;
     */
    protected $companyService;

    public function __construct()
    {
        $this->auditService = App::make(AuditService::class);
        $this->companyService = App::make(PhilippineCompanyRequestService::class);
        $this->authorizationService = App::make(CompanyBankAuthorizationService::class);
    }


    /**
     * @SWG\Get(
     *     path="/company/{company_id}/banks",
     *     summary="Get Company Bank Accounts",
     *     description="Get Company Bank Accounts

Authorization Scope : **view.disbursement**",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($companyId)
    {
        $data = ['company_id' => $companyId];
        $validator = Validator::make($data, [
            'company_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $this->invalidRequestError('Company ID is required and should be numeric.');
        }

        $user = app('request')->attributes->get('user');
        $authzEnabled = app('request')->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = app('request')->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $company = $this->companyService->get($companyId);
            $companyData = CompanyBankAuthorizationService::generateCompanyModel($company);
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $bankRequestService = App::make(BankRequestService::class);

        return $bankRequestService->getCompanyBankAccounts($companyId);
    }

    /**
     * @SWG\Post(
     *     path="/company/{company_id}/bank",
     *     security={ {"api_key": {}} },
     *     summary="Add Company Bank Account",
     *     description="Add a Bank Account to a given Company

Authorization Scope : **create.disbursement**",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"company"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newCompanyBankAccountsData"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="data", ref="#/definitions/CompanyBank")
     *         ),
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      "type": "company-bank-account",
     *                      "id": 1,
     *                      "attributes": {
     *                          "bankBranch": "Branch Location",
     *                          "accountNumber": "123456789",
     *                          "accountType": "SAVINGS",
     *                          "companyCode": "CMP-CODE",
     *                          "bank_details": {},
     *                          "bank": {
     *                              "id": 12,
     *                              "name": "BANCO DE ORO UNIVERSAL BANK",
     *                              "group": "BDO"
     *                          }
     *                      }
     *                  }
     *              },
     *              "With Account Source and Destination": {
     *                  "data": {
     *                      "type": "company-bank-account",
     *                      "id": 2,
     *                      "attributes": {
     *                          "bankBranch": "Branch Location",
     *                          "accountNumber": "123456789",
     *                          "accountType": "SAVINGS",
     *                          "companyCode": "CMP-CODE",
     *                          "bank_details": {"account_source": "ABC", "account_destination": "ABC"},
     *                          "bank": {
     *                              "id": 97,
     *                              "name": "SECURITY BANKING CORPORATION",
     *                              "group": "SECURITYBANK"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Default": { "message": "Company not found.", "status_code": 404 }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Default": { "message": "The account destination field is required.", "status_code": 406 }
     *         }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="newCompanyBankAccountsData",
     *     required={"data"},
     *     @SWG\Property(
     *         property="data",
     *         type="object",
     *         @SWG\Property(
     *             property="type",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="attributes",
     *             type="object",
     *             @SWG\Property(property="bank_id", type="integer"),
     *             @SWG\Property(property="bank_branch", type="string"),
     *             @SWG\Property(property="account_number", type="string"),
     *             @SWG\Property(property="account_type", type="string"),
     *             @SWG\Property(property="company_code", type="string"),
     *             @SWG\Property(
     *                  property="bank_details",
     *                  type="object",
     *                  @SWG\Property(property="account_source", type="string"),
     *                  @SWG\Property(property="account_destination", type="string")
     *             )
     *         )
     *     )
     * )
     */
    public function store(Request $request, $companyId)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);

            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $company = $this->companyService->get($companyId);
            $companyData = CompanyBankAuthorizationService::generateCompanyModel($company);
    
            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeCreate($companyData, $user);
        }

        if (!$isAuthorized) {
            return $this->response()->errorUnauthorized();
        }

        $bankRequestService = App::make(BankRequestService::class);
        return $bankRequestService->addCompanyBankAccount($companyId, $request->all());
    }


    /**
     * @SWG\Delete(
     *     path="/company/{company_id}/bank",
     *     summary="Delete Company Bank Account(s)",
     *     description="Delete company bank accounts whose uids are specified in request body.

Authorization Scope : **delete.disbursement**",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/CompanyBankAccountsResourceIdList"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
        * @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Cannot Delete Company Banks",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company bank not found",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="CompanyBankAccountsResourceIdList",
     *     required={"data"},
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             type="object",
     *              @SWG\Property(property="type", type="string", enum={"company-bank-account"}),
     *              @SWG\Property(property="id", type="integer")
     *         )
     *     )
     * )
     */
    public function deleteCompanyBanks(Request $request, $companyId)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $user = $request->attributes->get('user');
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $company = $this->companyService->get($companyId);
            $companyData = CompanyBankAuthorizationService::generateCompanyModel($company);

            $isAuthorized = $this->authorizationService->authorizeDelete($companyData, $user);
        }

        if (!$isAuthorized) {
            return $this->response()->errorUnauthorized();
        }

        $bankRequestService = App::make(BankRequestService::class);
        $bankRequestServiceResponse = $bankRequestService->deleteCompanyBanks($companyId, $request->all());

        if ($bankRequestServiceResponse->getStatusCode() === Response::HTTP_OK) {
            $companyBanks = json_decode($bankRequestServiceResponse->getData(), true);

            foreach ($companyBanks['data'] as $bank) {
                $item = new AuditCacheItem(
                    CompanyBankAuditService::class,
                    CompanyBankAuditService::ACTION_DELETE,
                    new AuditUser($user['user_id'], $user['account_id']),
                    [
                        'old' => [
                            'id' => $bank['id'],
                            'company_id' => $request->get('company_id')
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $bankRequestServiceResponse;
    }
}
