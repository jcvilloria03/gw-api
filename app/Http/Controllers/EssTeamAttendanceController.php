<?php

namespace App\Http\Controllers;

use App\Attendance\AttendanceJobErrorCsvGenerator;
use App\Attendance\AttendanceRequestService;
use App\Employee\EmployeeRequestService;
use App\Http\Controllers\EssBaseController;
use App\Storage\UploadService;
use App\Team\TeamRequestService;
use App\Transformer\EssTeamAttendanceJobDetailsTransformer;
use App\Transformer\EssTeamAttendanceJobErrorsTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Resource\Item;
use League\Fractal\Manager as FractalManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssTeamAttendanceController extends EssBaseController
{
    const ERRORS_CSV_FILENAME = '%s - Attendance Generation Error.csv';

    /** @var \League\Fractal\Manager */
    protected $fractal;

    /** @var \App\Team\TeamRequestService */
    protected $teamService;

    /** @var \App\Attendance\AttendanceRequestService */
    protected $attendanceService;

    /** @var \App\Employee\EmployeeRequestService */
    protected $employeeService;

    /** @var \App\Storage\UploadService */
    protected $uploadService;

    public function __construct(
        FractalManager $fractal,
        TeamRequestService $teamService,
        AttendanceRequestService $attendanceService,
        EmployeeRequestService $employeeService,
        UploadService $uploadService
    ) {
        $this->fractal = $fractal;

        $this->teamService = $teamService;

        $this->attendanceService = $attendanceService;

        $this->employeeService = $employeeService;

        $this->uploadService = $uploadService;
    }

    /**
    * @SWG\Post(
    *     path="/ess/teams/{id}/attendance/regenerate",
    *     summary="Regenerate team attendance",
    *     description="Regenerate team attendance",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="date",
     *                 type="string"
     *             )
     *         )
     *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "job_id": "abf0f69c-3463-4b62-a975-d3c5051a8343"
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function regenerate(Request $request, int $teamId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        if (!$team['data']['regenerate_team_attendance']) {
            abort(406, 'You are not allowed to regenerate team attendance.');
        }

        $this->validate($request, [
            'date' => 'required|date_format:Y-m-d',
        ]);

        $companyId = $team['data']['company_id'];
        $employeeIds = $this->teamService->getMemberIds($teamId);
        $date = $request->input('date');

        $data = [
            'company_id' => $companyId,
            'employee_ids' => $employeeIds,
            'dates' => [$date]
        ];

        $jobResponse = $this->attendanceService->bulkAttendanceCalculateByEmployees($data);

        return json_decode($jobResponse->getData(), true);
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/attendance/regenerate/{jobId}",
    *     summary="Get attendance regeneration status",
    *     description="Get attendance regeneration status",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="jobId",
    *         in="path",
    *         description="Job ID",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "job_id": "abf0f69c-3463-4b62-a975-d3c5051a8343",
    *                    "status": "PENDING|FINISHED|FAILED",
    *                    "count": 25,
    *                    "done": 9,
    *                    "error": 0,
    *                    "warning": 0
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getStatus(Request $request, int $teamId, string $jobId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        if (!$team['data']['regenerate_team_attendance']) {
            abort(406, 'You are not allowed to regenerate team attendance.');
        }

        $jobResponse = $this->attendanceService->getJobDetails($jobId);
        $job = json_decode($jobResponse->getData(), true)['data'];

        $result = new Item($job, new EssTeamAttendanceJobDetailsTransformer());

        return $this->fractal->createData($result)->toArray();
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/attendance/regenerate/{jobId}/errors",
    *     summary="Get attendance regeneration job errors",
    *     description="Get attendance regeneration job errors",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="jobId",
    *         in="path",
    *         description="Job ID",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "job_id": "abf0f69c-3463-4b62-a975-d3c5051a8343",
    *                    "errors": {
    *                        {
    *                            "employee_id": 1,
    *                            "date": "2020-01-01",
    *                            "message": "Error message here"
    *                        }
    *                    },
    *                    "warnings": {
    *                        {
    *                            "employee_id": 1,
    *                            "date": "2020-01-01",
    *                            "message": "Warning message here"
    *                        }
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getErrors(Request $request, int $teamId, string $jobId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        if (!$team['data']['regenerate_team_attendance']) {
            abort(406, 'You are not allowed to regenerate team attendance.');
        }

        $errors = $this->attendanceService->getJobErrorsData($jobId);

        $result = new Item($errors, new EssTeamAttendanceJobErrorsTransformer($jobId));

        return $this->fractal->createData($result)->toArray();
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/attendance/regenerate/{jobId}/errors/download",
    *     summary="Download attendance regeneration job errors",
    *     description="Download attendance regeneration job errors",
    *     tags={"ess", "sprint43"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="jobId",
    *         in="path",
    *         description="Job ID",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "url": "https://v3-uploads-local.s3.amazonaws.com/file.csv"
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function downloadErrors(Request $request, int $teamId, string $jobId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        if (!$team['data']['regenerate_team_attendance']) {
            abort(406, 'You are not allowed to regenerate team attendance.');
        }

        $errors = $this->attendanceService->getJobErrorsData($jobId);
        $employeeIds = array_column($errors, 'employee_uid');
        $employees = [];

        if (!empty($employeeIds)) {
            $employeesResponse = $this->employeeService->getCompanyEmployeesByIds(
                $team['data']['company_id'],
                $employeeIds,
                'TA_BASIC',
                []
            );

            $employees = json_decode($employeesResponse->getData(), true)['data'];
        }

        $employeesMap = [];

        foreach ($employees as $employee) {
            $employeesMap[$employee['id']] = sprintf(
                '%s - %s',
                $employee['employee_id'],
                $employee['full_name']
            );
        }

        $fileName = sprintf(self::ERRORS_CSV_FILENAME, $team['data']['name']);

        $csvGenerator = new AttendanceJobErrorCsvGenerator($errors, $employeesMap);
        $csvFileName = $csvGenerator->getCsvFile();

        $this->uploadService->saveFileToS3($fileName, $csvFileName);

        $url = $this->uploadService->createSignedlUrl($fileName);

        return [
            'data' => [
                'url' => $url
            ]
        ];
    }
}
