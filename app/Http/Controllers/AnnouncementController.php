<?php

namespace App\Http\Controllers;

use App;
use App\Audit\AuditService;
use App\Announcement\AnnouncementAuditService;
use App\Announcement\AnnouncementAuthorizationService;
use App\Announcement\AnnouncementRequestService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Illuminate\Http\Request;
use App\ESS\EssAnnouncementRequestService;
use App\Storage\PictureService;

/**
 * Class AnnouncementController
 *
 * @package App\Http\Controllers
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class AnnouncementController extends Controller
{
    /**
     * @var \App\Announcement\AnnouncementRequestService
     */
    protected $requestService;

    /**
     * @var \App\Announcement\AnnouncementAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * AnnouncementController constructor.
     *
     * @param \App\Announcement\AnnouncementRequestService       $requestService       AnnouncementRequestService
     * @param \App\Announcement\AnnouncementAuthorizationService $authorizationService AnnouncementAuthorizationService
     * @param \App\Audit\AuditService $auditService
     */
    public function __construct(
        AnnouncementRequestService $requestService,
        AnnouncementAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/announcements",
     *     summary="Get Company Announcement",
     *     description="Get all announcements within company
    Authorization Scope : **view.announcement**",
     *     tags={"announcement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Items to display per one page. F.e. 15. Default: 5",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1. Default is 1. F.e. 1",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="role",
     *         in="query",
     *         description="Get Announcement based on role: [sender|recipient]",
     *         type="string",
     *         enum={"sender", "recipient"}
     *     ),
     *     @SWG\Parameter(
     *         name="after",
     *         in="query",
     *         description="Show announcements after this date. F.e. 2018-08-11",
     *         type="string",
     *         format="date"
     *     ),
     *     @SWG\Parameter(
     *         name="before",
     *         in="query",
     *         description="Show announcements before this date. F.e. 2018-09-20",
     *         type="string",
     *         format="date"
     *     ),
     *     @SWG\Parameter(
     *         name="unread_only",
     *         in="query",
     *         description="Show only unread announcements",
     *         type="integer",
     *         enum={1, 0}
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="query",
     *         description="Attribute to sort by",
     *         type="string",
     *         enum={"id", "subject", "sender", "recipient", "created_at"}
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="query",
     *         description="Sort order",
     *         type="string",
     *         enum={"asc", "desc"}
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyAnnouncements($companyId, Request $request)
    {
        $user = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId)
            ];
            $authorized = $this->authorizationService->authorizeGetAll($authorizationData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $query = array_merge($request->query(), ['user_id' => $user['user_id']]);

        return $this->requestService->getCompanyAnnouncements($companyId, $query);
    }

    /**
     * @SWG\Get(
     *     path="/announcement/{announcementId}",
     *     summary="Get Announcement",
     *     description="Get Announcement Details
    Authorization Scope : **view.announcement**",
     *     tags={"announcement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="announcementId",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($announcementId, Request $request)
    {
        // authorize
        $user = $request->attributes->get('user');
        $response = $this->requestService->get($announcementId, $user['user_id']);

        if (!$response->isSuccessful()) {
            return $response;
        }
        $announcement = json_decode($response->getData());
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this
                ->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $announcement->company_id);
        } else {
            $announcement->account_id = !empty($announcement->company_id)
                ? Company::getAccountId($announcement->company_id)
                : null;

            $authorized = $this->authorizationService->authorizeGet($announcement, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $data = $response;
        if ($response->isSuccessful()) {
            $service = App::make(PictureService::class);
            $data = json_decode($response->getData(), true);
            if (!empty($data['sender']) && !empty($data['sender']['id'])) {
                $data['sender']['picture'] = $service->getUserPicture($data['sender']['id']);
            }

            if (!empty($data['replies'])) {
                foreach ($data['replies'] as $key => $value) {
                    $data['replies'][$key]['sender']['picture'] = $service->getUserPicture($value['sender']['id']);
                }
            }
        }

        return $data;
    }

    /**
     * @SWG\Post(
     *     path="/announcement",
     *     summary="Create Announcement",
     *     description="Create new Announcement
    Authorization Scope : **create.announcement**",
     *     tags={"announcement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *     @SWG\Schema(ref="#/definitions/newAdminAnnouncement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="newAdminAnnouncement",
     *     required={"affected_employees", "company_id", "subject", "message",
     *     "read_receipt", "allow_response"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="subject",
     *         type="string",
     *         description="Subject of announcement"
     *     ),
     *     @SWG\Property(
     *         property="message",
     *         type="string",
     *         description="Content of announcement message"
     *     ),
     *     @SWG\Property(
     *         property="read_receipt",
     *         type="boolean",
     *         description="See the list of announcement recipients who read the announcement"
     *     ),
     *     @SWG\Property(
     *         property="allow_response",
     *         type="boolean",
     *         description="Allow reply on anncouncement"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *     @SWG\Items(ref="#/definitions/affected_employees"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function create(Request $request)
    {
        $user = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authorizationData = (object)[
                'company_id' => $companyId,
                'account_id' => empty($companyId) ? null : Company::getAccountId($companyId)
            ];

            $authorized = $this->authorizationService->authorizeCreate($authorizationData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $data = array_merge($request->all(), ['sender_id' => $user['user_id']]);

        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $this->auditService->queue(new AuditCacheItem(
                AnnouncementAuditService::class,
                AnnouncementAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                [
                    'announcement' => json_decode($response->getData(), true),
                ]
            ));
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/announcement/{announcementId}/reply",
     *     summary="Send reply to Announcement",
     *     description="Send reply message to Announcement with given id",
     *     tags={"announcement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="announcementId",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="message",
     *         in="formData",
     *         description="Reply content",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function reply($announcementId, Request $request)
    {
        $user = $request->attributes->get('user');
        $attributes = $request->all();
        $attributes['sender_id'] = $user['user_id'];
        if ($this->isAuthzEnabled($request)) {
            $response = $this->requestService->get($announcementId, $user['user_id']);
            $announcement = json_decode($response->getData(), true);
            if (!$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $announcement['company_id']
            )) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->getUserRoleForAnnouncement($announcementId, $user['user_id']);
        $role = json_decode($response->getData());

        if (!$role->recipient) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->reply($announcementId, $attributes);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_SEND_REPLY,
            new AuditUser($user['user_id'], $user['account_id']),
            [
                'reply' => $responseData,
            ]
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{companyId}/announcements/download",
     *     summary="Download Company Announcements CSV",
     *     description="Download generated CSV with announcements
    Authorization Scope : **view.announcements**",
     *     tags={"announcement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="announcements_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Announcements Ids",
     *     @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         type="string",
     *         in="formData",
     *         description="Filter Announcements by start date 'YYYY-MM-DD'",
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         type="string",
     *         in="formData",
     *         description="Filter Announcements by end date 'YYYY-MM-DD'",
     *     ),
     *     @SWG\Parameter(
     *         name="role",
     *         in="formData",
     *         required=true,
     *         description="Announcements by User role: recipient|sender",
     *         type="string",
     *         enum={"recipient","sender"}
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         in="formData",
     *         description="Download all indicator",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="formData",
     *         description="Sort by attribute",
     *         type="string",
     *         enum={"sender", "subject"}
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="formData",
     *         description="Sort order",
     *         type="string",
     *         enum={"asc", "desc"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadCompanyAnnouncements($companyId, Request $request)
    {
        $user = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $announcement = (object)[
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $authorized = $this->authorizationService->authorizeDownloadAnnouncements($announcement, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();
        $data['user_id'] = $user['user_id'];

        return $this->requestService->downloadCompanyAnnouncementsCsv($companyId, $data);
    }

    /**
     * @SWG\Put(
     *     path="/announcement/{announcementId}/recipient_seen",
     *     summary="Update announcement as seen",
     *     description="Update announcement as seen
    Authorization Scope : **view.announcements**",
     *     tags={"announcement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="announcementId",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function updateRecipientSeen($announcementId, Request $request)
    {
        $user = $request->attributes->get('user');
        $userId = $user['user_id'];
        if ($this->isAuthzEnabled($request)) {
            $announcementResponse = $this->requestService->get($announcementId, $userId);
            $announcement = json_decode($announcementResponse->getData(), true);
            if (!$this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $announcement['company_id']
            )) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->getUserRoleForAnnouncement($announcementId, $userId);
        $role = json_decode($response->getData());
        if (!$role->recipient) {
            $this->response()->errorUnauthorized();
        }

        $ipAddress = $request->ip();
        $domain = $request->headers->get('origin');
        $response = $this->requestService->updateAnnouncementAsSeen($announcementId, $userId, $ipAddress, $domain);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'announcement_recipient' => $responseData
        ];

        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_RECIPIENT_SEEN,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }


    /**
     * @SWG\Put(
     *     path="/announcement/reply/{id}/seen",
     *     summary="Update announcement reply as seen",
     *     description="Mark announcement response as seen when announcement sender open it
Authorization Scope : **view.announcement**",
     *     tags={"announcement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Response ID (It is and ID from announcement_replies table on nt-api database)",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function markReplyAsSeen(
        $id,
        Request $request,
        EssAnnouncementRequestService $essAnnouncementRequestService
    ) {
        $user = $request->attributes->get('user');
        $userId = $user['user_id'];

        if (!$this->isAuthzEnabled($request)) {
            $response = $essAnnouncementRequestService->getReply($id);
            $reply = (object) json_decode($response->getData(), true);

            $roleResponse = $this->requestService->getUserRoleForAnnouncement($reply->announcement_id, $userId);
            $role = (object) json_decode($roleResponse->getData());

            $reply->account_id = $user['account_id'];
            // authorize
            if (!$this->authorizationService->authorizeSenderView($reply, $user, $role)) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $essAnnouncementRequestService->markReplyAsSeen($id);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'announcement_response' => $responseData,
        ];
        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_RESPONSE_SEEN,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }
}
