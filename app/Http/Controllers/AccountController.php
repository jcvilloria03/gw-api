<?php

namespace App\Http\Controllers;

use App\Account\AccountAuthorizationService;
use App\Account\AccountAuditService;
use App\Authentication\AuthenticationRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Company\CompanyRequestService;
use App\Company\CompanyAuthorizationService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Role\DefaultRoleService;
use App\Role\UserRoleService;
use App\Role\RoleAuditService;
use App\Model\Auth0User;
use Illuminate\Http\Request;
use App\Account\AccountRequestService;
use App\User\UserAuditService;
use App\User\UserRequestService;
use App\Auth0\Auth0UserService;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Holiday\HolidayRequestService;
use App\Task\TaskService;
use Symfony\Component\HttpFoundation\Response;
use Bschmitt\Amqp\Facades\Amqp;
use Validator;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class AccountController extends Controller
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Account\AccountRequestService
     */
    protected $requestService;

    /**
     * @var \App\Authentication\AuthenticationRequestService
     */
    protected $authenticationRequestService;

    /**
     * @var \App\User\UserRequestService
     */
    private $userRequestService;

    /**
     * @var \App\Auth0\Auth0UserService
     */
    private $auth0UserService;

    /**
     * @var \App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * @var \App\Role\DefaultRoleService
     */
    private $defaultRoleService;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    private $subscriptionsService;

    /**
     * @var \App\Account\AccountAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        AccountRequestService $requestService,
        UserRequestService $userRequestService,
        AuditService $auditService,
        DefaultRoleService $defaultRoleService,
        UserRoleService $userRoleService,
        Auth0UserService $auth0UserService,
        AuthenticationRequestService $authenticationRequestService,
        AccountAuthorizationService $authorizationService,
        SubscriptionsRequestService $subscriptionsService
    ) {
        $this->requestService = $requestService;
        $this->userRequestService = $userRequestService;
        $this->auditService = $auditService;
        $this->defaultRoleService = $defaultRoleService;
        $this->userRoleService = $userRoleService;
        $this->auth0UserService = $auth0UserService;
        $this->authenticationRequestService = $authenticationRequestService;
        $this->authorizationService = $authorizationService;
        $this->subscriptionsService = $subscriptionsService;
    }

    /**
     * @SWG\Get(
     *     path="/account/",
     *     summary="Get Account",
     *     description="Get Account Details

Authorization Scope : **view.account**",
     *     tags={"account"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request)
    {
        $accountId = $request->attributes->get('user')['account_id'];
        $account     = $this->requestService->get($accountId);
        $accountData = json_decode($account->getData(), true);

        $customerResponse = $this->subscriptionsService->getCustomerByAccountId($accountId);
        $customerData     = json_decode($customerResponse->getData(), true);

        $customerData['data'][0]['subscriptions'][0]['subscription_settings'] =
            $customerData['data'][0]['subscription_settings'] ?? null;
        $accountData['subscriptions'] = $customerData['data'][0]['subscriptions'] ?? [];

        return $accountData;
    }

    /**
     * @SWG\Post(
     *     deprecated=true,
     *     path="/account/sign_up/deprecated",
     *     summary="Sign Up",
     *     description="Create new account and new user",
     *     tags={"account"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Account Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="First name",
     *         required=true,
     *         in="formData",
     *         name="first_name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Last name",
     *         name="last_name",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Company name",
     *         name="company",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Email Address",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="Password",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="plan_id",
     *         in="formData",
     *         description="The selected plan id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function signUp(
        Request $request,
        CompanyRequestService $companyRequestService,
        HolidayRequestService $holidayRequestService
    ) {
        $validator = Validator::make($request->all(), [
            "email"         => "required|email",
            "password"      => "required|string|min:8",
            "name"          => "required",
            "company"       => "required",
            "last_name"     => "required|name",
            "first_name"    => "required|name",
            "plan_id"       => "required|integer"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // get user id from token
        $inputs = $request->all();

        // Get the module access value
        $moduleAccess = $this->subscriptionsService
            ->getModuleAccessByPlanId($request->input("plan_id"));

        if (empty($moduleAccess)) {
            return response()->json([
                "BadRequestError" => "Invalid subscription plan id."
            ], 400);
        }

        // call microservice
        $response = $this->requestService->signUp($inputs);

        // create default Owner and Super Admin roles
        $accountData = json_decode($response->getData(), true);

        // Create Owner's auth user
        $authResponse = $this->authenticationRequestService->signUp([
            'data' => [
                'user' => [
                    "email"             => $request->input("email"),
                    "password"          => $request->input("password"),
                    "user_metadata"     => [
                        "name"              => $request->input("name"),
                        "fresh_account"     => "false",
                        "initial_password"  => "",
                        "company"           => $request->input("company"),
                        "first_name"        => $request->input("first_name"),
                        "last_name"         => $request->input("last_name"),
                    ]
                ]
            ]
        ]);

        $authData = json_decode($authResponse->getData(), true);
        $authUser = $authData['data']['user'];

        // create auth0_user entry
        $this->auth0UserService->create([
            'auth0_user_id' => $authUser['auth0_user_id'],
            'user_id' => $accountData['user_id'],
            'account_id' => $accountData['id'],
            'status' => Auth0User::STATUS_ACTIVE
        ]);
        // CRBAC create and auto assign system defined roles
        $companyRequestService->createSystemDefinedRoles($accountData['id'], true);

        // create owner role and assign this role to first user
        $ownerRole = $this->defaultRoleService->createOwnerRole(
            $accountData['id'],
            $moduleAccess
        );
        $superAdminRole = $this->defaultRoleService->createSuperAdminRole(
            $accountData['id'],
            $moduleAccess
        );
        $this->userRoleService->create([
            'role_id' => $ownerRole->id,
            'user_id' => $accountData['user_id'],
        ]);

        // create admin role for company
        $company['id'] = $accountData['company_id'];
        $this->defaultRoleService->createAdminRole(
            $accountData['id'],
            $company,
            $moduleAccess
        );

        // create employee role for company
        $this->defaultRoleService->createCompanyEmployeeRole(
            $accountData['id'],
            $company,
            $moduleAccess
        );

        // Register selected subscription
        $this->subscriptionsService->createCustomer([
            "plan_id"       => $request->input("plan_id"),
            "user_id"       => $accountData['user_id'],
            "first_name"    => $request->input("first_name"),
            "last_name"     => $request->input("last_name"),
            "account_name"  => $request->input("name"),
            "is_trial"      => true,
            "account_id"    => $accountData['id'],
            "email"         => $request->input("email")
        ]);

        // create default holidays
        $holidayRequestService->createCompanyDefaultHolidays($company['id']);

        // Log for audit log usage
        $this->logSignupActions($accountData, $ownerRole, $superAdminRole);

        // drop queue to create company demo account
        $details = [
            'name' => 'democompany.created',
            'data' => [
                'account_id' => $accountData['id'],
                'user_id'    => $accountData['user_id'],
                'name'       => 'Demo Company',
                'type'       => 'Private',
                'tin'        => '000-000-000-000',
                'rdo'        => '048',
                'sss'        => '00-0000000-0',
                'hdmf'       => '0000-0000-0000',
                'philhealth' => '00-000000000-0'
            ]
        ];
        $this->dropQueuetoEventExchange($details);

        return [
            'data' => [
                'user' => [
                    'id_token' => $authUser['id_token'],
                    'access_token' => $authUser['access_token']
                ]
            ]
        ];
    }

    /**
     * This method performs the logging of the signup activities. This usually
     * queues into the auditlog service.
     * Possible queues:
     * 1. Account audit service
     * 2. User audit service
     * 3. Role audit service
     * @param $accountData array
     * @param $ownerRole App\Role\DefaultRoleService;
     */
    private function logSignupActions($accountData, $ownerRole, $superAdminRole)
    {
        // audit log
        // log account creation
        $details = [
            'new' => $accountData,
        ];
        $item = new AuditCacheItem(
            AccountAuditService::class,
            AccountAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log user creation
        $userResponse = $this->userRequestService->get($accountData['user_id']);
        $userData = json_decode($userResponse->getData(), true);
        $details = [
            'new' => $userData,
        ];
        $item = new AuditCacheItem(
            UserAuditService::class,
            UserAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log owner role creation
        $details = [
            'new' => $ownerRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log super admin role creation
        $details = [
            'new' => $superAdminRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);
    }

    /**
     * @SWG\Post(
     *     deprecated=true,
     *     path="/deprecated/account/sign_up",
     *     summary="Sign Up",
     *     description="Create new account and new user",
     *     tags={"account"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Account Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="First name",
     *         required=true,
     *         in="formData",
     *         name="first_name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Last name",
     *         name="last_name",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Company name",
     *         name="company",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Email Address",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function signUpOld(Request $request)
    {
        // get user id from token
        $inputs = $request->all();
        $decodedToken = $request->attributes->get('decodedToken');

        // call microservice
        $response = $this->requestService->signUp($inputs);

        // create default Owner and Super Admin roles
        $accountData = json_decode($response->getData(), true);

        // create auth0_user entry
        $this->auth0UserService->create([
            'auth0_user_id' => $decodedToken->sub,
            'user_id' => $accountData['user_id'],
            'account_id' => $accountData['id'],
            'status' => Auth0User::STATUS_ACTIVE
        ]);

        // create owner role and assign this role to first user
        $ownerRole = $this->defaultRoleService->createOwnerRole($accountData['id'], $accountData['module_access']);
        $superAdminRole = $this->defaultRoleService->createSuperAdminRole(
            $accountData['id'],
            $accountData['module_access']
        );
        $this->userRoleService->create([
            'role_id' => $ownerRole->id,
            'user_id' => $accountData['user_id'],
        ]);

        // create admin role for company
        $company['id'] = $accountData['company_id'];
        $this->defaultRoleService->createAdminRole(
            $accountData['id'],
            $company,
            $accountData['module_access']
        );

        // create employee role for company
        $this->defaultRoleService->createCompanyEmployeeRole(
            $accountData['id'],
            $company,
            $accountData['module_access']
        );

        // audit log
        // log account creation
        $details = [
            'new' => $accountData,
        ];
        $item = new AuditCacheItem(
            AccountAuditService::class,
            AccountAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log user creation
        $userResponse = $this->userRequestService->get($accountData['user_id']);
        $userData = json_decode($userResponse->getData(), true);
        $details = [
            'new' => $userData,
        ];
        $item = new AuditCacheItem(
            UserAuditService::class,
            UserAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log owner role creation
        $details = [
            'new' => $ownerRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        // log super admin role creation
        $details = [
            'new' => $superAdminRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($accountData['user_id'], $accountData['id']),
            $details
        );
        $this->auditService->queue($item);

        return $this
            ->response
            ->array([
                'id' => $accountData['id'],
                'user_id' => $accountData['user_id'],
                'company_id' => $accountData['company_id']
            ])
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     path="/account/setup_progress",
     *     summary="Get setup progress of a account",
     *     description="Shows which parts of account setup have been finished",
     *     tags={"account"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function setupProgress(Request $request)
    {
        // call microservice
        $accountId = $request->attributes->get('user')['account_id'];
        return $this->requestService->setupProgress($accountId);
    }

    /**
     * @SWG\Patch(
     *     path="/account/progress",
     *     summary="Set specific part of setup to be done",
     *     description="Set specific part of setup to be done.

Authorization Scope : **edit.account**",
     *     tags={"account"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Progress Type",
     *         required=true,
     *         type="string",
     *         enum={"company", "user", "location", "payroll_group", "employee", "department", "rank", "cost_center"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function progress(Request $request)
    {
        $accountId = $request->attributes->get('user')['account_id'];
        $userId = $request->attributes->get('user')['user_id'];

        if (
            !$this->isAuthzEnabled($request)
            && !$this->authorizationService->progress($accountId, $userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->progress($accountId, $request->all());

        // Audit log
        $details = [
            'new' => $request->all(),
        ];
        $item = new AuditCacheItem(
            AccountAuditService::class,
            AccountAuditService::ACTION_SETUP_PROGRESS,
            new AuditUser($userId, $accountId),
            $details
        );
        $this->auditService->queue($item);
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/account/philippine/companies",
     *     summary="Get Account Companies",
     *     description="Get Account Companies
Authorization Scope : **view.company**",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAccountCompanies(
        Request $request,
        PhilippineCompanyRequestService $companyRequestService,
        CompanyAuthorizationService $companyAuthorizationService,
        TaskService $taskService
    ) {
        // call microservice
        $accountId = $request->attributes->get('user')['account_id'];
        $user = $request->attributes->get('user');

        // get all roles for user id
        $userRoles = $this->userRoleService->getUserRoles($user['user_id']);

        $roles = $userRoles->map(function ($userRole) {
            return $userRole->role;
        })
            ->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $authzDataScope = $request->attributes->get('authz_data_scope');

        $customerResponse     = $this->subscriptionsService->getCustomerByAccountId($accountId);
        $customerData         = json_decode($customerResponse->getData(), true);
        $accountSubscriptions = $customerData['data'][0]['subscriptions'] ?? [];

        if ($authzEnabled) {
            $accountCompaniesResponse = $this->requestService->getAccountCompanies(
                $accountId,
                $authzDataScope
            );

            $accountCompaniesData = json_decode($accountCompaniesResponse->getData(), true);

            if (empty($accountCompaniesData['data'])) {
                $this->response()->errorUnauthorized();
            }

            return array_merge($accountCompaniesData, [
                'subscriptions' => $accountSubscriptions
            ]);
        }

        if (!$authzEnabled) {
            $isTypeAdmin = $this->userRoleService->isTypeAdmin($roles);
            $companyIds = $this->userRoleService->getCompanyIdForAdminRole($roles, $taskService, true);

            if (!$isTypeAdmin || !$companyIds) {
                $this->response()->errorUnauthorized();
            }

            $companies = [];

            foreach ($companyIds as $companyId) {
                $companyResponse = $companyRequestService->get($companyId);
                $companyData = json_decode($companyResponse->getData());

                if (!$companyAuthorizationService->authorizeGet($companyData, $user)) {
                    $this->response()->errorUnauthorized();
                }

                $companies[] = $companyData;
            }
        }

        return [
            'data' => $companies,
            'subscriptions' => $accountSubscriptions
        ];
    }

    /**
     * @SWG\Get(
     *     path="/account/logs/",
     *     summary="Get Account Logs",
     *     description="Get Account Logs

Authorization Scope : **view.account_logs**",
     *     tags={"account"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAccountLogs(Request $request)
    {
        // call microservice
        $accountId = $request->attributes->get('user')['account_id'];

        // authorize
        $userId = $request->attributes->get('user')['user_id'];
        if (!$this->authorizationService->authorizeGetAccountLogs($accountId, $userId)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->auditService->view($accountId);
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/accounts",
     *     summary="Get All Accounts",
     *     description="Get All Accounts in system",
     *     tags={"account"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="keyphrase",
     *         in="query",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully fetched Accounts": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "name": "BarAccount",
     *                          "email": "test@example.test",
     *                          "user_id": "",
     *                          "subscriptions": {
     *                              "id": 2,
     *                              "name": "time and attendance"
     *                          },
     *                          "companies": {
     *                              {
     *                                  "id": 1,
     *                                  "name": "FooCompany",
     *                                  "country": {
     *                                      "id": 1,
     *                                      "code": "PH",
     *                                      "name": "Philippines"
     *                                  },
     *                                  "account_id": 1,
     *                                  "logo": "...",
     *                                  "email": null,
     *                                  "website": null,
     *                                  "mobile_number": null,
     *                                  "telephone_number": null,
     *                                  "telephone_extension": null,
     *                                  "fax_number": null
     *                              }
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Account not found.",
     *         examples={
     *              "Non existing Account": {
     *                  "message": "Account not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     )
     * )
     */
    public function index(Request $request)
    {
        if ($request->get('keyphrase', '') !== getenv('ACCOUNT_MANAGEMENT_KEY_KEYPHRASE')) {
            $this->response()->errorUnauthorized();
        }

        $accountsResponse = $this->requestService->index();
        $accountsData = json_decode($accountsResponse->getData(), true);

        $userIds = array_column($accountsData['data'], 'user_id');

        $subscriptionsResponse = $this->subscriptionsService->getSubscriptionsByUserIds($userIds);
        $subscriptionsData = json_decode($subscriptionsResponse->getData(), true)['data'];

        $accountsData['data'] = array_map(function ($account) use ($subscriptionsData) {
            $account['subscriptions'] = $subscriptionsData[$account['user_id']] ?? [];

            return $account;
        }, $accountsData['data']);

        return $accountsData;
    }

    /**
     * Send message to event exchange
     *
     * @param array $details
     * @return void
     */
    private function dropQueuetoEventExchange($details)
    {
        $message = new \Bschmitt\Amqp\Message(
            json_encode($details),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish('', $message, [
            'exchange_type' => 'fanout',
            'exchange' => env('EVENT_STREAM_EXCHANGE')
        ]);
    }
}
