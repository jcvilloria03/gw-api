<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\ESS\EssEmployeeRequestRequestService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\EmployeeRequest\EmployeeRequestAuditService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use App\Workflow\WorkflowRequestService;
use App\Authz\AuthzDataScope;

class EmployeeRequestController extends Controller
{
    /**
     * @var \App\Ess\EssEmployeeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        EssEmployeeRequestRequestService $requestService,
        EmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/request/send_message",
     *     summary="Send request message.",
     *     description="Send request message.
    Authorization Scope : **edit.request**",
     *     tags={"request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="request_id",
     *         in="formData",
     *         description="Employee Request ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="content",
     *         in="formData",
     *         description="Message content.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function sendMessage(Request $request, WorkflowRequestService $workflowRequestService)
    {
        // authorize
        $user = $request->attributes->get('user');
        $inputs = $request->all();

        // Get User Workflows for Authorization
        $responseData = $workflowRequestService->getUserWorkflows($user['user_id'], true);
        $workflows = json_decode($responseData->getData(), true);

        // Get Employee Request for Authorization
        $employeeRequestData = $this->requestService->get($request['request_id']);
        $employeeRequest = json_decode($employeeRequestData->getData());

        $employeeRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $employeeRequest->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeSendMessage($user, $workflows, $employeeRequest);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['sender_id'] = $user['user_id'];

        $response = $this->requestService->sendMessage($inputs);
        $requestMessage = json_decode($response->getData(), true);

        // audit log
        $details = [
            'new' => $requestMessage
        ];
        $item = new AuditCacheItem(
            EmployeeRequestAuditService::class,
            EmployeeRequestAuditService::ACTION_SEND_MESSAGE,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/default_schedule/related_requests_to_updated_days_of_week",
     *     summary="Gets Related Pending Employee Requests To Changed Days Of Week",
     *     description="Gets Related Pending Employee Requests To Changed Days Of Week
Authorization Scope : **view.request**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UpdatedDefaultSchedules"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="UpdatedDefaultSchedules",
     *     required={"company_id", "updated_default_schedules"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="updated_default_schedule",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/UpdatedDefaultSchedule"
     *         )
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="UpdatedDefaultSchedule",
     *     required={"id","day_of_week", "day_type", "work_start", "work_break_start", "work_break_end", "work_end"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="day_of_week",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="day_type",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="work_start",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="work_break_start",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="work_break_end",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="work_end",
     *         type="string",
     *     ),
     * )
     */
    public function getRelatedRequestsToUpdatedDefaultScheduleDaysOfWeek(
        Request $request,
        DefaultScheduleRequestService $defaultScheduleService
    ) {
        $user = $request->attributes->get('user');
        $inputs = $request->all();
        $companyId = $inputs['company_id'];

        if ($this->isAuthzEnabled($request) && !$this->getAuthzDataScope($request)->isAuthorized(
            AuthzDataScope::SCOPE_COMPANY,
            (int) $companyId
        )) {
            $this->response()->errorUnauthorized();
        }

        $oldDataResponse = $defaultScheduleService->index((int)$companyId);
        $oldData = json_decode($oldDataResponse->getData(), true);
        $oldData = collect($oldData['data']);

        $payload['changed_days_of_week'] = $this->mapChangedDaysOfDefaultSchedule(
            $oldData,
            $inputs['updated_default_schedule']
        );
        $payload['company_id'] = $companyId;

        $employeeRequestsData = $this->requestService->getRelatedRequestsToUpdatedDefaultScheduleDaysOfWeek($payload);
        $employeeRequests = json_decode($employeeRequestsData->getData(), true)['data'];

        if (!$this->isAuthzEnabled($request)) {
            $isUnauthorized = collect($employeeRequests)->contains(function ($employeeRequest) use ($user) {
                $employeeRequest['account_id'] = $user['account_id'];

                return !$this->authorizationService->authorizeView((object)$employeeRequest, $user);
            });

            if ($isUnauthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $employeeRequestsData;
    }

    /**
     * Maps changed days of week of default schedule.
     *
     * @param \Illuminate\Support\Collection $oldData
     * @param array $newData
     * @return array array of day of week number
     */
    private function mapChangedDaysOfDefaultSchedule(Collection $oldData, array $newData)
    {
        return collect($newData)->map(function ($new) use ($oldData) {
            return !in_array($new, $oldData->all()) ? $new['day_of_week'] : null;
        })->filter()->all();
    }
}
