<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\Schedule\ScheduleAuthorizationService;
use Illuminate\Http\Request;
use App\Tag\TagRequestService;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    /**
     * @var \App\Tag\TagRequestService
     */
    private $requestService;

    /**
     * @var \App\Schedule\ScheduleAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        TagRequestService $requestService,
        ScheduleAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/tags",
     *     summary="Get all Tags",
     *     description="Get all Tags within company.

Authorization Scope : **view.tag**",
     *     tags={"tag"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyTags($id, Request $request)
    {
        $response = $this->requestService->getCompanyTags($id);
        $tagsData = json_decode($response->getData())->data;
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $tag = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($tag, $user)
            || $this->authorizationService->authorizeUpdate($tag, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (empty($tagsData)) {
            return $response;
        }

        return $response;
    }
}
