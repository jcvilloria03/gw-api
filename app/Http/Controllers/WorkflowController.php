<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\Workflow\WorkflowAuditService;
use App\Workflow\WorkflowAuthorizationService;
use Illuminate\Http\Request;
use App\Workflow\WorkflowRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

class WorkflowController extends Controller
{
    /**
     * @var \App\Workflow\WorkflowRequestService
     */
    private $requestService;

    /**
     * @var \App\Workflow\WorkflowAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        WorkflowRequestService $requestService,
        WorkflowAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

      /**
     * @SWG\Get(
     *     path="/workflow/{id}",
     *     summary="Get Workflow",
     *     description="Get Workflow Details
Authorization Scope : **view.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Workflow ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, int $id)
    {
        $userData = $request->attributes->get('user');
        $response = $this->requestService->get($id, $userData['account_id']);

        $workflowData = json_decode($response->getData());
        $workflowData->account_id = Company::getAccountId($workflowData->company_id);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $workflowData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet(
                $workflowData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/workflows",
     *     summary="Get all Workflows",
     *     description="Get all Workflows within company.
Authorization Scope : **view.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search by workflow name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyWorkflows(Request $request, int $companyId)
    {
        $isAuthorized = false;
        $accountId = Company::getAccountId($companyId);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $workflow = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyWorkflows(
                $workflow,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $query = $request->all() ?? '';
        $query['account_id'] = $accountId;
        $queryString = http_build_query($query);

        return $this->requestService->getCompanyWorkflows($companyId, $queryString);
    }

     /**
     * @SWG\Post(
     *     path="/workflow",
     *     summary="Create Workflow",
     *     description="Create Workflow.
Authorization Scope : **create.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newWorkflow"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newWorkflow",
     *     required={"company_id", "name"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Workflow name"
     *     ),
     *     @SWG\Property(
     *         property="levels",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/level"),
     *         collectionFormat="multi"
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="level",
     *     required={"position"},
     *     @SWG\Property(
     *         property="position",
     *         type="integer",
     *         description="Workflow level position"
     *     ),
     *     @SWG\Property(
     *         property="approvers",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/approver"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="approver",
     *     required={"approver_type", "approver_id"},
     *     @SWG\Property(
     *         property="approver_type",
     *         type="string",
     *         description="Approver type. Possible values: employee, user, position, department, team"
     *     ),
     *     @SWG\Property(
     *         property="approver_id",
     *         type="integer"
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $accountId = Company::getAccountId($inputs['company_id']);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $workflowData = (object) [
                'account_id' => $accountId,
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($workflowData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['account_id'] = $accountId;
        $response = $this->requestService->create($inputs);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $workflowGetResponse = $this->requestService->get($responseData['id'], $createdBy['account_id']);
        $workflow = json_decode($workflowGetResponse->getData(), true);
        $details = [
            'new' => $workflow
        ];
        $item = new AuditCacheItem(
            WorkflowAuditService::class,
            WorkflowAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/workflow/{id}",
     *     summary="Update Workflow.",
     *     description="Update existing Workflow.
Authorization Scope : **edit.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newWorkflow"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, int $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id, $updatedBy['account_id']);
        $oldWorkflowData = json_decode($response->getData());
        $oldWorkflowData->account_id = Company::getAccountId($inputs['company_id']);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldWorkflowData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldWorkflowData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $inputs['account_id'] = Company::getAccountId($inputs['company_id']);
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id, $updatedBy['account_id']);
        $oldWorkflowData = json_decode($response->getData(), true);
        $newWorkflowData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldWorkflowData,
            'new' => $newWorkflowData,
        ];
        $item = new AuditCacheItem(
            WorkflowAuditService::class,
            WorkflowAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

     /**
     * @SWG\Post(
     *     path="/company/{id}/workflow/is_name_available",
     *     summary="Is Workflow name available",
     *     description="Check availability of Workflow name with the given company",
     *     tags={"workflow"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Workflow Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="workflow_id",
     *         in="formData",
     *         description="Workflow Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $workflowData = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $workflowData,
                $user
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($companyId, $request->all());
    }

    /**
     * @SWG\Delete(
     *     path="/workflow/bulk_delete",
     *     summary="Delete multiple Workflows",
     *     description="Delete multiple Workflows
Authorization Scope : **delete.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="workflows_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Workflows Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $deletedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($authData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($request->all());

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('workflows_ids', []) as $id) {
                $item = new AuditCacheItem(
                    WorkflowAuditService::class,
                    WorkflowAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $companyId
                        ]
                    ]
                );

                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/workflow/check_in_use",
     *     summary="Are Workflows in use",
     *     description="Check if Workflows are in use
Authorization Scope : **view.workflow**",
     *     tags={"workflow"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="workflow_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Workflow Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();

        if (empty($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $authorized = false;

        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $workflowData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $authorized = $this->authorizationService->authorizeGetCompanyWorkflows(
                $workflowData,
                $request->attributes->get('user')
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }
}
