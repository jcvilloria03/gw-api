<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Payroll\PayslipGenerationTask;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;
use App\Jobs\JobsRequestService;
use App\Payroll\PayrollRequestService;
use App\Payslip\PayslipRequestService;
use App\PayrollGroup\PayrollGroupRequestService;
use App\Payroll\PayslipAuthorizationService;
use App\Payroll\PayrollTaskException;
use App\Payroll\PayslipAuditService;
use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use Swagger\Annotations as SWG;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PayslipController extends Controller
{
    use AuditTrailTrait;

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /*
     * App\Payslip\PayslipRequestService
     */
    protected $requestService;

    /*
     * App\Payroll\PayrollRequestService
     */
    protected $payrollRequestService;

    /*
     * App\Payroll\PayrollGroupRequestService
     */
    protected $payrollGroupRequestService;

    /*
     * App\Payroll\PayslipAuthorizationService
     */
    protected $authorizationService;

    /*
     * App\Jobs\JobsRequestService
     */
    protected $jobsRequestService;


    public function __construct(
        PayslipRequestService $requestService,
        PayrollRequestService $payrollRequestService,
        PayrollGroupRequestService $payrollGroupRequestService,
        PayslipAuthorizationService $authorizationService,
        AuditService $auditService,
        JobsRequestService $jobsRequestService
    ) {
        $this->requestService = $requestService;
        $this->payrollRequestService = $payrollRequestService;
        $this->payrollGroupRequestService = $payrollGroupRequestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->jobsRequestService = $jobsRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/payslips/generate",
     *     summary="Generate Payslips",
     *     description="Trigger Payslip Generation for Given Payroll ID

Authorization Scope : **generate.payslip**",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function generate(Request $request, $id)
    {
        //TODO: move this code to PR-API
        //check payroll exists (will throw exception if payroll doesn't exist)
        $payrollResponse = $this->payrollRequestService->get($id);
        $payrollData = json_decode($payrollResponse->getData());
        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $dataScope = $this->getAuthzDataScope($request);
            $companyId = $payrollData->company_id;
            $payrollGroupId = $payrollData->payroll_group_id;

            $isAuthorized =
                $dataScope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => $companyId,
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupId
                    ]
                );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($payrollData, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if ($payrollData->status == 'DRAFT') {
            $this->invalidRequestError("Can't Generate Payslips for a Draft Payroll.");
        }

        //TODO:: get payrollData and see if there is a payslips job
        //see if it's safe to generate payslips

        $jobPayload = [
            'data' => [
                'type' => 'jobs',
                'attributes' => [
                    'payload' => [
                        'name' => 'generate-payroll-payslips',
                        'startDate' => $payrollData->start_date,
                        'endDate' => $payrollData->end_date,
                        'companyId' => $payrollData->company_id,
                        'payrollGroupId' => $payrollData->payroll_group_id,
                        'payrollId' => $id,
                        'createdBy' => $createdBy['user_id'],
                    ]
                ]
            ]
        ];

        $jobResponse = $this->jobsRequestService->createJob($jobPayload);
        $jobData = json_decode($jobResponse->getData());

        $this->payrollRequestService->addUploadJobToPayroll([
            'payroll_id' => $payrollData->id,
            'job_id' => (string) $jobData->data->id,
            'type' => 'PAYSLIP',
            'uploaded_file' => 'not-applicable'
        ]);

        try {
            $data = json_decode(json_encode($payrollData), true);
            unset($data['jobs']);
            $this->audit($request, $payrollData->company_id, [], ['Trigger Payslip Generation' => $data]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return response()->json($jobData, Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/payslips/can_generate",
     *     summary="Payroll Payslip Generation Check",
     *     description="Check payslips can be generated for a given payroll

Authorization Scope : **generate.payslip**",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function canGenerate($id, Request $request)
    {

        //check payroll exists (will throw exception if payroll doesn't exist)
        $payrollResponse = $this->payrollRequestService->get($id);
        $payrollData = json_decode($payrollResponse->getData());
        $createdBy = $request->attributes->get('user');

        if ($payrollData->status == 'DRAFT') {
            return response()->json([
                'can_generate' => false,
            ]);
        }

        // authorize
        if (
            !$this->authorizationService->authorizeCreate(
                $payrollData,
                $createdBy
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $task = App::make(PayslipGenerationTask::class);

        return response()->json([
            'can_generate' => $task->canGenerate($id),
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/payslip/{id}",
     *     summary="Get Payslip",
     *     description="Get payslip using payslip id",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payslip Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="download",
     *         in="query",
     *         description="Download url instead of inline",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="uri", type="string", example="https://uri")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payslip not found",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", example="Payslip not found."),
     *              @SWG\Property(property="status_code", type="integer", example=404)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Payslip not found",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", example="The id must be an integer."),
     *              @SWG\Property(property="status_code", type="integer", example=406)
     *         )
     *     )
     * )
     */
    public function get(Request $request, int $id)
    {
        $payslipResponse = $this->requestService->get($id);
        $payslipData = json_decode($payslipResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $payslipData->company_id,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payslipData->payroll_group_id
                ]
            );
        } else {
            $payroll = (object) [
                'id' => $payslipData->payroll_id,
                'payroll_group_id' => $payslipData->payroll_group_id,
                'company_id' => $payslipData->company_id,
                'account_id' => $payslipData->account_id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $payroll,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->download($id, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/payslips",
     *     summary="Get Payslips",
     *     description="Get Payslips by Payroll Group, Pay Dates, as specified",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_ids",
     *         in="query",
     *         description="Payroll Group IDs, comma separated",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_from",
     *         in="query",
     *         description="Pay Date From (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date_to",
     *         in="query",
     *         description="Pay Date To (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="include_special",
     *         in="query",
     *         description="Include special payrun payslips within date range.",
     *         required=true,
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function search(Request $request, int $companyId)
    {
        $inputPayrollGroups = array_unique(explode(",", $request->input('payroll_group_ids')));

        $payrollGroupResponse = $this->payrollGroupRequestService->getCompanyPayrollGroups($companyId);
        $payrollGroups = json_decode($payrollGroupResponse->getData());

        $authorizedPayrollGroups = collect($payrollGroups->data)
            ->filter(function ($payrollGroup) use ($request, $companyId) {
                if ($this->isAuthzEnabled($request)) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized(
                        [
                            AuthzDataScope::SCOPE_COMPANY => $companyId,
                            AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroup->id
                        ]
                    );
                } else {
                    $payroll = (object) [
                        'payroll_group_id' => $payrollGroup->id,
                        'account_id' => $payrollGroup->account_id,
                        'company_id' => $payrollGroup->company_id
                    ];

                    return $this->authorizationService->authorizeGet(
                        $payroll,
                        $request->attributes->get('user')
                    );
                }
            })->transform(function ($payrollGroup) {
                return $payrollGroup->id;
            })->all();

        $unauthorizedPayrollGroups = array_diff(
            array_values($inputPayrollGroups),
            array_values($authorizedPayrollGroups)
        );

        if (!empty($unauthorizedPayrollGroups)) {
            $this->response()->errorUnauthorized('Not authorized to view all specified payroll groups');
        }

        return $this->requestService->search(
            array_merge(["company_id" => $companyId], $request->all())
        );
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/payslips/authorized_payroll_groups",
     *     summary="Get Authorized Payroll Groups For Payslip viewing ",
     *     description="Get list of company payroll groups that current user is authorized to view payslips for",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getAuthorizedCompanyPayrollGroups(Request $request, $companyId)
    {
        $payrollGroupResponse = $this->payrollGroupRequestService->getCompanyPayrollGroups($companyId);
        $payrollGroups = json_decode($payrollGroupResponse->getData());

        $filteredPayrollGroups = collect($payrollGroups->data)
            ->filter(function ($payrollGroup) use ($request, $companyId) {
                if ($this->isAuthzEnabled($request)) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized(
                        [
                            AuthzDataScope::SCOPE_COMPANY => $companyId,
                            AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroup->id
                        ]
                    );
                } else {
                    $payroll = (object) [
                        'payroll_group_id' => $payrollGroup->id,
                        'account_id' => $payrollGroup->account_id,
                        'company_id' => $payrollGroup->company_id
                    ];

                    return $this->authorizationService->authorizeGet(
                        $payroll,
                        $request->attributes->get('user')
                    );
                }
            })->transform(function ($payrollGroup) {
                return (array) $payrollGroup;
            })->all();

        return response()->json(array_column($filteredPayrollGroups, 'name', 'id'));
    }

    /**
     * @SWG\Get(
     *     path="/payroll/{id}/has_payslips",
     *     summary="Check if Payroll has Payslips",
     *     description="Check if Payroll has Payslips already generated",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="has_payslips", type="boolean", example=true)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function hasPayslips($id, Request $request)
    {
        $payrollData = json_decode($this->payrollRequestService->get($id)->getData());
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id
            ]);
        } else {
            $viewedBy = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeGet($payrollData, $viewedBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->payrollRequestService->hasPayslips($id);
    }/**
     * @SWG\Post(
     *     path="/payslip/download_multiple",
     *     summary="Download Multiple Payslips",
     *     description="Triggers creation of zip file containing multiple payslips.
Will return whether user will be emailed of Zip URI or not.",
     *     tags={"payslip"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids",
     *         in="formData",
     *         description="Payslip Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer", minimum=1),
     *         collectionFormat="csv"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payslip not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadMultiple(Request $request)
    {
        $user = $request->attributes->get('user');
        $input = $request->all();

        $this->checkPayslipsAuthorization($user, $input);

        $input['user_id'] = $user['user_id'];
        return $this->requestService->downloadMultiple($input);
    }

    protected function checkPayslipsAuthorization(array $user, array $payslipIds)
    {
        $payslipsResponse = $this->requestService->getMultiple($payslipIds);
        $payslipsData = json_decode($payslipsResponse->getData());
        $request = app('request');
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $payrollGroupIds = collect(data_get($payslipsData, 'data.*.payroll_group_id'))->filter(function ($value) {
                return !empty($value);
            })->toArray();
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => data_get($payslipsData, 'data.*.company_id'),
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => !empty($payrollGroupIds) ? $payrollGroupIds : null
                ]
            );
        } else {
            $processed = [];
            foreach ($payslipsData->data as $payslipData) {
                if (isset($processed[$payslipData->payroll_id])) {
                    continue;
                }

                // verify if user can view payslips from a specific payroll
                $payroll = (object) [
                    'id' => $payslipData->payroll_id,
                    'payroll_group_id' => $payslipData->payroll_group_id,
                    'company_id' => $payslipData->company_id,
                    'account_id' => $payslipData->account_id,
                ];
                if (
                    !empty($payslipData->payroll_group_id) &&
                    !$this->authorizationService->authorizeGet($payroll, $user)
                ) {
                    // if at least one payslip is unauthorized, return 401
                    $this->response()->errorUnauthorized('Not authorized to view all specified payslips');
                }

                // mark payroll so we don't check authorization for payslips from same payroll
                $processed[$payslipData->payroll_id] = true;
            }
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
    }

    /**
     * @SWG\Get(
     *     path="/payslip/zipped/{id}/url",
     *     summary="Get Zipped Payslip URL",
     *     description="Get zipped payslip using the given id",
     *     tags={"payslip"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Zipped Payslip ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payslip not found",
     *     )
     * )
     */
    public function getZippedPayslipsUrl(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $zippedPayslipsResponse = $this->requestService->getZippedPayslipsUrl($id);
        $zippedPayslipsData = json_decode($zippedPayslipsResponse->getData());

        $payslipIds = [
            'ids' => $zippedPayslipsData->payslip_ids
        ];
        $this->checkPayslipsAuthorization($user, $payslipIds);

        return $zippedPayslipsResponse;
    }
}
