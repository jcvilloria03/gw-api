<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\AccountDeletion\AccountDeletionAuthorizationService;
use App\AccountDeletion\AccountDeletionRequestService;
use App\User\UserRequestService;
use App\Notification\NotificationRequestService;
use Illuminate\Support\Facades\App;
use App\Traits\AuditTrailTrait;
use Symfony\Component\HttpFoundation\Response;

class AccountDeletionController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\AccountDeletion\AccountDeletionRequestService
     */
    private $requestService;

    /**
     * @var \App\AccountDeletion\AccountDeletionAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;


    public function __construct(
        AccountDeletionRequestService $requestService,
        AccountDeletionAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/subscriptions/account_deletion",
     *     summary="Get Account Deletion Request",
     *     description="Get Account Details

Authorization Scope : **view.subscriptions**",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->getAccountDeletion($user['account_id']);
    }


    /**
     * @SWG\Post(
     *     path="/subscriptions/account_deletion",
     *     summary="Create account deletion request",
     *     description="Create account deletion request",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Create account deletion request data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "required"={"user_id"},
     *             "properties"={
     *                 "user_id"={"type"="integer"},
     *                 "email"={"type"="string"},
     *                 "otp"={"type"="integer"},
     *                 "otp_expires_at"={"type"="string"},
     *                 "last_otp_at"={"type"="string"},
     *                 "delete_at"={"type"="string"},
     *                 "status"={"type"="string"},
     *                 "selected_reason"={"type"="string"},
     *                 "request_reasons"={"type"="string"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="successful operation",
     *         examples={
     *             "application/json": {
     *                 "data"={
     *                    "id": 0
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Name already exists"
     *             }
     *         }
     *     )
     * )
     */
    public function create(
        Request $request,
        NotificationRequestService $notificationService
    ) {
        $user = $request->attributes->get('user');

        $userResponse = $this->userRequestService->get($user['user_id']);
        $userData = json_decode($userResponse->getData());
        $companyId = $userData->companies[0]->id;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->createAcountDeletionRequest([
            "user_id"          => $user['user_id'],
            "email"            => $userData->email,
            "account_id"       => $user['account_id'],
            "selected_reasons" => $request->input("selected_reasons"),
            "request_reasons"  => $request->input("request_reasons")
        ]);

        $responseData = json_decode($response->getData(), true);
        $notificationService->sendOtpEmailNotificationToUser($responseData['email'], $responseData['otp']);

        return
            $this
            ->response
            ->array([
                'id' => $responseData['id'],
                'email' => $responseData['email'],
                'otp_expires_at' => $responseData['otp_expires_at']
            ])
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @SWG\Post(
     *     path="/subscriptions/account_deletion/{id}/confirm_otp",
     *     summary="Get confirm otp code",
     *     description="Get confirm otp code",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function confirmOtp($id, Request $request)
    {
        $inputs = $request->only([
            'otp'
        ]);
        // call microservice
        return $this->requestService->confirmOtp($id, $inputs);
    }

    /**
     * @SWG\Post(
     *     path="/subscriptions/account_deletion/{id}/resend_otp",
     *     summary="Regenerate otp code",
     *     description="Regenerate otp code",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function resendOtp(
        $id,
        NotificationRequestService $notificationService
    ) {
        $response = $this->requestService->resendOtp($id);
        $responseData = json_decode($response->getData(), true);
        $notificationService->sendOtpEmailNotificationToUser($responseData['email'], $responseData['otp']);

        return $this
            ->response
            ->array([
                'id' => $responseData['id'],
                'email' => $responseData['email'],
                'otp_expires_at' => $responseData['otp_expires_at'],
                'remaining_attempt' => $responseData['remaining_attempt']
            ])
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @SWG\Patch(
     *     path="/subscriptions/account_deletion/{id}/confirm",
     *     summary="Update account deletion request to confirmed status",
     *     description="Update account deletion request to confirmed status",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function confirm(Request $request, $id)
    {
        $inputs = $request->only([
            'confirm_text'
        ]);
        // call microservice
        return $this->requestService->confirmRequest($id, $inputs);
    }

    /**
     * @SWG\Delete(
     *     path="/subscriptions/account_deletion/{id}/cancel",
     *     summary="Delete account deletion request",
     *     description="Delete account deletion request",
     *     tags={"account_deletion"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Subscription not found or draft not found",
     *     )
     * ),
     */
    public function cancelRequest(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($user['user_id']);
        $userData = json_decode($userResponse->getData());
        $companyId = $userData->companies[0]->id;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->deleteRequest($id);

        // pass request to cp-api
        return $response;
    }
}
