<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\TardinessRule\TardinessRuleAuditService;
use App\TardinessRule\TardinessRuleAuthorizationService;
use App\TardinessRule\TardinessRuleRequestService;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TardinessRuleController extends Controller
{
    /**
     * @var \App\TardinessRule\TardinessRuleRequestService
     */
    private $requestService;

    /**
     * @var \App\TardinessRule\TardinessRuleAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditServiceAuditService
     */
    private $auditService;

    public function __construct(
        TardinessRuleRequestService $requestService,
        TardinessRuleAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/tardiness_rule/{id}",
     *     summary="Get Tardiness Rule",
     *     description="Get Tardiness Rule Details
Authorization Scope : **view.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Tardiness Rule ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $responseData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $responseData->company_id);
        } else {
            $authData = (object) [
                'account_id' => Company::getAccountId($responseData->company_id),
                'company_id' => $responseData->company_id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
    * @SWG\Get(
    *     path="/company/{id}/tardiness_rules",
    *     summary="Get all Tardiness rules for company.",
    *     description="Get all Tardiness rules for company.
    Authorization Scope : **view.tardiness_rule**",
    *     tags={"tardiness_rule"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="companyId",
    *         in="path",
    *         description="Company ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="Successful operation",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
    *         description="Unauthorized",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getCompanyTardinessRules(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyTardinessRules(
                $authData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyTardinessRules($companyId);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/tardiness_rule/",
     *     summary="Create company tardiness rules",
     *     description="Create company tardiness rules
Authorization Scope : **create.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newTardinessRule"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     *
     * @SWG\Definition(
     *     definition="newTardinessRule",
     *     required={"company_id", "minutes_tardy", "name"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Tardiness rule name"
     *     ),
     *     @SWG\Property(
     *         property="minutes_tardy",
     *         type="integer",
     *         description="No. of Minutes Tardy"
     *     ),
     *     @SWG\Property(
     *         property="minutes_to_deduct",
     *         type="integer",
     *         description="No. of Minutes to Deduct"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_tardiness_rule"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="affected_employees_tardiness_rule",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Relation type. Possible values: location, department, position, employee"
     *     )
     * )
     */
    public function create(Request $request)
    {
        //Validate request parameters
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($authData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $tardinessRuleGetResponse = $this->requestService->get($responseData['id']);
        $tardinessRuleData = json_decode($tardinessRuleGetResponse->getData(), true);

        $item = new AuditCacheItem(
            TardinessRuleAuditService::class,
            TardinessRuleAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            [
                'new' => $tardinessRuleData,
            ]
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/tardiness_rule/bulk_delete",
     *     summary="Delete multiple Tardiness Rules",
     *     description="Delete multiple Tardiness Rules
Authorization Scope : **delete.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="tardiness_rules_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Tardiness rules Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $deletedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($authData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $tardinessRulesIds = $request->input('tardiness_rules_ids', []);

        // call microservice
        $response = $this->requestService->bulkDelete($request->all());

        if ($response->isSuccessful()) {
            // audit log
            foreach ($tardinessRulesIds as $id) {
                $item = new AuditCacheItem(
                    TardinessRuleAuditService::class,
                    TardinessRuleAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $companyId
                        ]
                    ]
                );

                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/tardiness_rule/{id}",
     *     summary="Update Tardiness Rule",
     *     description="Update Tardiness Rule
Authorization Scope : **edit.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *    ),
     *    @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *    ),
     *    @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/existingTardinessRule"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="existingTardinessRule",
     *     required={"company_id", "name", "minutes_tardy", "affected_employees"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Tardiness rule name"
     *     ),
     *     @SWG\Property(
     *         property="minutes_tardy",
     *         type="integer",
     *         description="No. of Minutes Tardy"
     *     ),
     *     @SWG\Property(
     *         property="minutes_to_deduct",
     *         type="integer",
     *         description="No. of Minutes to Deduct"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_tardiness_rule"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function update(Request $request, int $tardinessRuleId)
    {
        $updatedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $oldTardinessRule = $this->requestService->get($tardinessRuleId);
        $oldTardinessRuleData = json_decode($oldTardinessRule->getData());
        $oldTardinessRuleData->account_id = $accountId;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldTardinessRuleData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldTardinessRuleData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($tardinessRuleId, $request->all());

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->isSuccessful()) {
            $getResponse = $this->requestService->get($tardinessRuleId);

            $oldTardinessRuleData = json_decode($oldTardinessRule->getData(), true);
            $newTardinessRuleData = json_decode($getResponse->getData(), true);

            $item = new AuditCacheItem(
                TardinessRuleAuditService::class,
                TardinessRuleAuditService::ACTION_UPDATE,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                [
                    'old' => $oldTardinessRuleData,
                    'new' => $newTardinessRuleData,
                ]
            );

            $this->auditService->queue($item);
        }

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/tardiness_rule/check_in_use",
     *     summary="Are Tardiness Rules in use",
     *     description="Check are Tardiness Rules in use
Authorization Scope : **view.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="tardiness_rule_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Tardiness Rules Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $tardinessRuleData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanyTardinessRules(
                $tardinessRuleData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/tardiness_rule/is_name_available",
     *     summary="Is Tardiness Rule name available",
     *     description="Check availability of Tardiness Rule name with the given company
Authorization Scope : **create.tardiness_rule**",
     *     tags={"tardiness_rule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Tardiness Rule Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="tardiness_rule_id",
     *         in="formData",
     *         description="Tardiness Rule Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId
            ];

            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $authData,
                $user
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($companyId, $request->all());
    }
}
