<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Http\Controllers\EssBaseController;
use App\ESS\EssOvertimeRequestRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\OvertimeRequest\OvertimeRequestAuditService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Authz\AuthzDataScope;
use App\ClockState\ClockStateService;

class EssOvertimeRequestController extends EssBaseController
{
    /**
     * @var \App\ESS\EssOvertimeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EssOvertimeRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/overtime_request",
     *     summary="Create Overtime Request",
     *     description="Create Overtime Request
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newOvertimeRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="newOvertimeRequest",
     *     required={"date"},
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="messages",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *         description="Initial messages."
     *     ),
     *     @SWG\Property(
     *         property="shifts",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/overtimeShifts")
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="overtimeShifts",
     *     required={"shift_id,hours,start_time"},
     *     @SWG\Property(
     *         property="shift_id",
     *         description="Shift ID. Null value for Default Schedule."
     *     ),
     *     @SWG\Property(
     *         property="hours",
     *         type="string",
     *         description="Overtime hours in format HH:mm."
     *     ),
     *     @SWG\Property(
     *         property="start_time",
     *         type="string",
     *         description="Overtime start time in format HH:mm."
     *     )
     * )
     */
    public function create(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();
        $data['company_id'] = $user['employee_company_id'];
        $data['employee_id'] = $user['employee_id'];
        $data['user_id'] = $user['user_id'];
        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $overtimeRequest = $this->requestService->get($responseData['id']);
            $overtimeRequestData = json_decode($overtimeRequest->getData(), true);

            $details = [
                'new' => [
                    'employee_company_id' => $user['employee_company_id'],
                    'id' => $overtimeRequestData['id'],
                ],
            ];

            $item = new AuditCacheItem(
                OvertimeRequestAuditService::class,
                OvertimeRequestAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                $details
            );

            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/overtime_request/{id}",
     *     summary="Get Overtime Requests",
     *     description="Get Overtime Requests",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Overtime request ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        ClockStateService $clockStateService,
        $id
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->get($id);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $overtimeRequest = json_decode($response->getData());
        $overtimeRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $overtimeRequest->request->company_id
            );

            $isAuthorized = ApprovalService::isEitherRequestorOrApproverNew(
                $user['employee_id'],
                $overtimeRequest,
                $user
            );
        } else {
            $workflows = $overtimeRequest->workflow_levels;
            $isAuthorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $overtimeRequest
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // fetch timesheet
        try {
            $shiftStart = collect($overtimeRequest->shifts)->pluck('start')->unique()->all();
            $shiftEnd = collect($overtimeRequest->shifts)->pluck('end')->unique()->all();

            $timeSheets = $clockStateService->getFormattedTimesheet(
                $overtimeRequest->employee_id,
                $shiftStart,
                $shiftEnd
            );
            $overtimeRequest->timesheet = isset($timeSheets['data']) ? $timeSheets['data'] : [];
        } catch (\Throwable $e) {
            \Log::error($e->getMessage() . ' : No timesheet data = ' . $overtimeRequest->employee_id);
            \Log::error($e->getTraceAsString());
        }

        return $this
            ->response
            ->array((array) $overtimeRequest)
            ->setStatusCode(Response::HTTP_CREATED);
    }
}
