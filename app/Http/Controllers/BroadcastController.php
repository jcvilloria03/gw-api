<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broadcast\BroadcastAuthRequest;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/broadcasting/auth",
     *     summary="Authenticate user",
     *     description="Authenticate the request for channel access.",
     *     tags={"broadcasting"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="channel_name",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
     *         description="forbidden operation",
     *     ),
     * )
     */
    public function authenticate(Request $request)
    {
        $request = new BroadcastAuthRequest($request);
        return Broadcast::auth($request);
    }
}
