<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Company\PhilippineCompanyRequestService;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\Facades\Company;
use App\Schedule\ScheduleAuditService;
use App\Schedule\ScheduleAuthorizationService;
use App\Schedule\ScheduleRequestService;
use App\Schedule\ScheduleUploadTask;
use App\Schedule\ScheduleUploadTaskException;
use App\Employee\EmployeeRequestService;
use Aws\S3\Exception\S3Exception;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class ScheduleController extends Controller
{
    use AuditTrailTrait;
    /**
     * @var \App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /**
     * @var \App\Schedule\ScheduleRequestService
     */
    protected $requestService;

    /**
     * @var \App\Schedule\ScheduleAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /**
     * ScheduleController constructor.
     *
     * @param CsvValidator                    $csvValidator          CsvValidator
     * @param PhilippineCompanyRequestService $companyRequestService PhilippineCompanyRequestService
     * @param ScheduleRequestService          $requestService        ScheduleRequestService
     * @param ScheduleAuthorizationService    $authorizationService  ScheduleAuthorizationService
     * @param AuditService                    $auditService          AuditService
     */
    public function __construct(
        CsvValidator $csvValidator,
        PhilippineCompanyRequestService $companyRequestService,
        ScheduleRequestService $requestService,
        ScheduleAuthorizationService $authorizationService,
        AuditService $auditService,
        EmployeeRequestService $employeeRequestService
    ) {
        $this->csvValidator = $csvValidator;
        $this->companyRequestService = $companyRequestService;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->employeeRequestService = $employeeRequestService;
    }

    /**
     * @SWG\Post(
     *     path="/schedule/upload",
     *     summary="Upload Schedules",
     *     description="Uploads Schedules

    Authorization Scope : **create.schedule**",
     *     tags={"schedule"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         description="Action type",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\Schedule\ScheduleUploadTask::PROCESS_TYPE_CREATE,
     *              App\Schedule\ScheduleUploadTask::PROCESS_TYPE_UPDATE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "schedule_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSchedules(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;
        $type = $request->input('type');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $scheduleData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $scheduleData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(ScheduleUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => ScheduleUploadTask::PROCESS_VALIDATION,
                'type' => $type,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $this->getAuthzDataScope($request)->getDataScope()
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            Amqp::publish(config('queues.schedule_validation_queue'), $message);

            // trigger audit trail
            $this->audit($request, $companyId, $details);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/schedule/upload/save",
     *     summary="Save Schedules",
     *     description="Saves Schedules from Previously Uploaded CSV

    Authorization Scope : **create.schedule**",
     *     tags={"schedule"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "schedule_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Schedules needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;
        $scheduleData = (object)[
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $scheduleData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(ScheduleUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(ScheduleUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (ScheduleUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.schedule_write_queue'), $message);

        // trigger audit trail
        $this->audit($request, $companyId, $details);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/schedule/upload/status",
     *     summary="Get Job Status",
     *     description="Get Schedule Upload Status

    Authorization Scope : **create.schedule**",
     *     tags={"schedule"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\Schedule\ScheduleUploadTask::PROCESS_VALIDATION,
     *              App\Schedule\ScheduleUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Name is invalid"
     *                          },
     *                          "4": {
     *                              "Email is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $scheduleData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $scheduleData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(ScheduleUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (ScheduleUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, ScheduleUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(",", ScheduleUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;

        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === ScheduleUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === ScheduleUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }
        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/schedule/check_in_use",
     *     summary="Are Schedule in use",
     *     description="Check are Schedule in use",
     *     tags={"schedule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="schedules_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Schedules Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }
        $scheduleData = (object)[
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        $userData = $request->attributes->get('user');

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanySchedules($scheduleData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->checkInUse($inputs);

        if ($response->isSuccessful()) {
            $data = json_decode($response, true);
            // trigger audit trail
            $this->audit($request, $inputs['company_id'], [], $data ?? []);
        }
        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/schedule/bulk_delete",
     *     summary="Delete multiple Schedules",
     *     description="Delete multiple Schedules,
    Authorization Scope : **delete.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="schedules_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Schedules Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        $companyId = $request->get('company_id', 0);

        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $scheduleData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($scheduleData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $scheduleIds = $request->input('schedules_ids', []);
        $deletedScheduleData = $this->requestService->getMultiple($companyId, $scheduleIds);
        
        // call microservice
        $response = $this->requestService->bulkDelete($inputs, $authzDataScope);

        if ($response->isSuccessful()) {
            foreach ($deletedScheduleData as $oldData) {
                // trigger audit trail
                $this->audit($request, $companyId, [], $oldData);
            }
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/schedule",
     *     summary="Create Schedule for the company.",
     *     description="Create Schedule for the company.
    Authorization Scope : **create.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/schedule"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="schedule",
     *     required={
     *         "company_id", "type", "name", "start_date", "start_time",
     *         "allowed_time_methods", "auto_assign", "on_holidays", "on_rest_day"
     *     },
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         enum={"fixed", "flexi"},
     *         description="Schedule type. Possible values: fixed, flexi"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Schedule Name"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         description="Schedule validity start date in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="start_time",
     *         type="string",
     *         description="Start time of the schedule in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="end_time",
     *         type="string",
     *         description="End time of the schedule in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="total_hours",
     *         type="string",
     *         description="Schedule total hours in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="allowed_time_methods",
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={"Bundy App", "Time Sheet", "Web Bundy", "Biometric"},
     *         ),
     *         collectionFormat="multi",
     *         description="Time methods allowed to log time against schedule"
     *     ),
     *     @SWG\Property(
     *         property="auto_assign",
     *         type="boolean",
     *         description="Schedule can be auto-assigned as a shift"
     *     ),
     *     @SWG\Property(
     *         property="on_holidays",
     *         type="boolean",
     *         description="Schedule can be applicable to holidays"
     *     ),
     *     @SWG\Property(
     *         property="on_rest_day",
     *         type="boolean",
     *         description="Schedule can be assigned as shifts on rest days"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_schedule"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="breaks",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/schedule_breaks"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="core_times",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/schedule_core_times"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="tags",
     *         type="array",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Property(
     *         property="repeat",
     *         ref="#/definitions/schedule_repeat"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="affected_employees_schedule",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Relation type. Possible values: location, department, position, employee"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="schedule_breaks",
     *     required={"type"},
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         enum={"fixed", "flexi", "floating"},
     *         description="Break type. Possible values: fixed, flexi, floating"
     *     ),
     *     @SWG\Property(
     *         property="start",
     *         type="string",
     *         description="Start time of the break in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="end",
     *         type="string",
     *         description="End time of the break in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="break_hours",
     *         type="string",
     *         description="Break total hours in format HH:mm"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="schedule_core_times",
     *     @SWG\Property(
     *         property="start",
     *         type="string",
     *         description="Start time of the core time in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="end",
     *         type="string",
     *         description="End time of the core time in format HH:mm"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="schedule_repeat",
     *     required={"type"},
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         enum={"daily", "weekly", "monthly", "yearly", "every_weekday",
     *             "every_monday_wednesday_friday", "every_tuesday_thursday"},
     *         description="Repeat type. Possible values: 'daily', 'weekly', 'monthly', 'yearly',
 'every_weekday', 'every_monday_wednesday_friday','every_tuesday_thursday'"
     *     ),
     *     @SWG\Property(
     *         property="repeat_by",
     *         type="string",
     *         enum={"week", "month"},
     *         description="Repeat by month or week."
     *     ),
     *     @SWG\Property(
     *         property="repeat_on",
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             enum={"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"},
     *         description="Repeat on exactly days. Possible values: 'monday', 'tuesday', 'wednesday',
 'thursday', 'friday', 'saturday', 'sunday'"
     *         )
     *     ),
     *     @SWG\Property(
     *         property="end_never",
     *         type="boolean",
     *         default=false,
     *         description="Indication is schedule repeat never ended"
     *     ),
     *     @SWG\Property(
     *         property="end_after",
     *         type="integer",
     *         description="Schedule repeat ends after certain number of occurrences"
     *     ),
     *     @SWG\Property(
     *         property="end_on",
     *         type="string",
     *         description="Schedule repeat ends on date value."
     *     )
     * )
     */
    public function create(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        $createParam = [];
        if ($this->isAuthzEnabled($request)) {
            // Authorize with AuthZ
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
            // add the scope as additional param for the microservice call for
            // further filtering of data (ie employee department)
        } else {
            $scheduleData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($scheduleData, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $createParam = array_merge($createParam, $request->all());
        $response = $this->requestService->create($createParam, $this->getAuthzDataScope($request));

        $responseData = $response->getData();
        if (\is_array($responseData)) {
            return $responseData;
        }

        $responseData = json_decode($responseData, true);

        $scheduleGetResponse = $this->requestService->get($responseData['id']);
        $scheduleRuleData = json_decode($scheduleGetResponse->getData(), true);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $this->audit($request, $companyId, $scheduleRuleData, []);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/schedule/{id}",
     *     summary="Get Schedule",
     *     description="Get Schedule Details
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Schedule ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $response = $this->requestService->get($id, $dataScope->getDataScope());
        } else {
            $response = $this->requestService->get($id);
            $scheduleData = json_decode($response->getData());
            $scheduleData->account_id = Company::getAccountId($scheduleData->company_id);
            if (!$this->authorizationService->authorizeGet($scheduleData, $request->attributes->get('user'))) {
                $this->response()->errorUnauthorized();
            }
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/schedules",
     *     summary="Get all Schedules",
     *     description="Get all Schedules within company.
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanySchedules($id, Request $request)
    {
        $queryData = $request->query();
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        $pageType = null;
        $page = null;
        $perPage = '';

        if (isset($queryData['type'])) {
            $pageType = $queryData['type'];
        }
        if (isset($queryData['page'])) {
            $page = $queryData['page'];
        }
        if (isset($queryData['per_page'])) {
            $perPage = $queryData['per_page'];
        }

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $schedule = (object)[
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $this->authorizationService->authorizeGetCompanySchedules(
                $schedule,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }



        $response = $this->requestService->getCompanySchedules(
            $id,
            $authzDataScope,
            $pageType,
            $page,
            $perPage
        );

        $scheduleData = json_decode($response->getData())->data;

        if (empty($scheduleData)) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/schedule_ids",
     *     summary="Get all Schedule IDs",
     *     description="Get all Schedule ids within company.
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyScheduleIds($id, Request $request)
    {
        $response = $this->requestService->getCompanyScheduleIds($id);
        $scheduleData = json_decode($response->getData())->data;
        if (empty($scheduleData)) {
            return $response;
        }

        $schedule = (object)[
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];
        if (
        !$this->authorizationService->authorizeGetCompanySchedules(
            $schedule,
            $request->attributes->get('user')
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/schedule/{id}",
     *     summary="Update Schedule for the company.",
     *     description="Create Schedule for the company.
    Authorization Scope : **edit.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Schedule ID",
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/schedule"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update($id, Request $request)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $oldSchedule = $this->requestService->get($id);
        $oldScheduleData = json_decode($oldSchedule->getData());
        $oldScheduleData->account_id = $accountId;

        // authorize
        $authzEnabled = $this->isAuthzEnabled($request);
        $dataScope = $this->getAuthzDataScope($request);

        $isAuthorized = false;

        if ($authzEnabled) {
            $isAuthorized = $dataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldScheduleData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($id, $request->all(), $dataScope);

        if ($updateResponse->isSuccessful()) {
            $getResponse = $this->requestService->get($id);

            $oldScheduleData = json_decode($oldSchedule->getData(), true);
            $newScheduleData = json_decode($getResponse->getData(), true);
            // trigger audit trail
            $this->audit($request, $companyId, $newScheduleData, $oldScheduleData, true);
        }

        return $updateResponse;
    }

    /**
     * @SWG\Get(
     *     path="/schedule/upload/preview",
     *     summary="Get Schedule Preview",
     *     description="Get Schedule Upload Preview
    Authorization Scope : **create.schedule**",
     *     tags={"schedule"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {

        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $scheduleData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService
            ->authorizeCreate(
                $scheduleData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(ScheduleUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (ScheduleUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        return $this->requestService->getUploadPreview($jobId);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/schedules/generate_csv",
     *     summary="Generate CSV with schedules",
     *     description="Generate CSV with schedules within company for given params.
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="schedules_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Schedules Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function generateCsv(Request $request, $id)
    {
        $schedule = (object)[
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanySchedules(
                $schedule,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->generateCsv($id, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/schedules/download/{fileName}",
     *     summary="Download Schedule CSV",
     *     description="Download generated CSV with schedules
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="fileName",
     *         in="path",
     *         description="File name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadCsv($companyId, $fileName, Request $request)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $schedule = (object)[
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanySchedules(
                $schedule,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->downloadCsv($companyId, $fileName);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/schedule/is_name_available",
     *     summary="Is schedule name available",
     *     description="Check availability of schedule name with the given company",
     *     tags={"schedule"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Schedule Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="schedule_id",
     *         in="formData",
     *         description="Schedule Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($companyId, Request $request)
    {
        $response = $this->requestService->isNameAvailable($companyId, $request->all());

        $user = $request->attributes->get('user');

        $authData = (object)[
            'account_id' => $companyId ? Company::getAccountId($companyId) : null,
            'company_id' => $companyId
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($authData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{id}/schedules/search",
     *     summary="Search all Employee Schedules",
     *     description="Search all Employee Schedules by name.
    Authorization Scope : **view.schedule**",
     *     tags={"schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term by name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Number of results to return by search. Default is 10.",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function searchEmployeeSchedules($id, Request $request, EmployeeRequestService $employeeService)
    {
        $queryString = $request->getQueryString() ?? '';
        $response = $this->requestService->searchEmployeeSchedules($id, $queryString);

        $employeeResponse = $employeeService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());

        $schedule = (object)[
            'account_id' => Company::getAccountId($employee->company_id),
            'company_id' => $employee->company_id
        ];

        if (
        !$this->authorizationService->authorizeGetCompanySchedules(
            $schedule,
            $request->attributes->get('user')
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }
}
