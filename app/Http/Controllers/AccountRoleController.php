<?php

namespace App\Http\Controllers;

use App\Role\RoleRequestService;
use Dingo\Api\Http\Response;
use Illuminate\Http\Request;

class AccountRoleController extends Controller
{
    /**
     * @var \App\Role\RoleRequestService
     */
    protected $roleRequestService;

    /**
     * AccountRolesController constructor
     *
     * @param RoleRequestService $roleRequestService
     *
     * @SWG\Definition(
     *     definition="AccountRoleRequest",
     *     required={"name", "is_employee", "permissions"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         example="Role Name"
     *     ),
     *     @SWG\Property(
     *         property="is_employee",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="is_all_companies",
     *         type="boolean"
     *     ),
     *     @SWG\Property(
     *         property="company_ids",
     *         type="array"
     *     ),
     *     @SWG\Property(
     *         property="permissions",
     *         type="object",
     *         example={
     *             "control_panel.subscriptions": {
     *                 "scopes": {
     *                     "COMPANY": {"__ALL__"},
     *                     "DEPARTMENT": {1, 2, 3},
     *                 },
     *                 "actions": {"CREATE", "READ", "UPDATE", "DELETE"}
     *             }
     *         }
     *     )
     * )
     */
    public function __construct(RoleRequestService $roleRequestService)
    {
        $this->roleRequestService = $roleRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/accounts/{accountId}/roles",
     *     summary="Get account roles",
     *     description="Get list of roles by account",
     *     tags={"roles"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="List of Roles",
     *         examples={
     *             "application/json": {
     *                 "from": 1,
     *                 "last_page": 1,
     *                 "per_page": 4,
     *                 "to": 4,
     *                 "total": 4,
     *                 "data": {
     *                     {
     *                         "id": 1,
     *                         "owner_account_id": 1,
     *                         "name": "Role Name",
     *                         "level": "ONE",
     *                         "is_employee": false,
     *                         "is_system_defined": true,
     *                         "company_ids": { 1 },
     *                         "policy": {
     *                             "subject": {
     *                                 "role_id": 1,
     *                                 "user_id": 1,
     *                                 "account_id": 1,
     *                                 "owner_account_id": 1
     *                             },
     *                             "action": "IN_RESOURCE",
     *                             "resource": {
     *                                 "module_name": {
     *                                     "scopes": {
     *                                         "COMPANY": { 1, 2, 3 }
     *                                     },
     *                                     "actions": { "CREATE", "READ", "UPDATE", "DELETE" }
     *                                 }
     *                             },
     *                             "environment": {}
     *                         }
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Account ID",
     *     )
     * )
     */
    public function index(Request $request, int $accountId)
    {
        $user = $request->attributes->get('user');

        if ($accountId !== $user['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $this->validate($request, [
            'filter.role_ids' => 'nullable|array',
            'filter.role_ids.*' => 'required|integer|min:1',
            'page' => 'nullable|integer|min:1',
            'per_page' => 'nullable|integer|min:1',
        ]);

        return $this->roleRequestService->getRolesByAccount(
            $accountId,
            $request->only(['filter', 'page', 'per_page'])
        );
    }

    /**
     * @SWG\Get(
     *     path="/accounts/{accountId}/roles/{roleId}",
     *     summary="Get account role",
     *     description="Get role by account",
     *     tags={"roles"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Account Role",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     "id": 1,
     *                     "owner_account_id": 1,
     *                     "name": "Role Name",
     *                     "level": "ONE",
     *                     "is_employee": false,
     *                     "is_system_defined": false,
     *                     "company_ids": { 1 },
     *                     "policy": {
     *                         "subject": {
     *                             "role_id": 1,
     *                             "user_id": 1,
     *                             "account_id": 1,
     *                             "owner_account_id": 1
     *                         },
     *                         "action": "IN_RESOURCE",
     *                         "resource": {
     *                             "module_name": {
     *                                 "scopes": {
     *                                     "COMPANY": { 1, 2, 3 }
     *                                 },
     *                                 "actions": { "CREATE", "READ", "UPDATE", "DELETE" }
     *                             }
     *                         },
     *                         "environment": {}
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Account ID",
     *     )
     * )
     */
    public function get(Request $request, int $accountId, int $roleId)
    {
        $user = $request->attributes->get('user');

        if ($accountId !== $user['account_id']) {
            $this->response()->errorUnauthorized();
        }

        return $this->roleRequestService->getRoleByAccount($accountId, $roleId);
    }

    /**
     * @SWG\Post(
     *     path="/accounts/{accountId}/roles",
     *     summary="Create account role",
     *     description="Create role by account",
     *     tags={"roles"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AccountRoleRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Account Role",
     *         examples={
     *             "application/json": {
     *                 "data": {
     *                     "id": 1,
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Wrong or Missing Data Input (JSON or Param)",
     *     )
     * ),
     */
    public function create(Request $request, int $accountId)
    {
        $user = $request->attributes->get('user');

        if ($accountId !== $user['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->input();

        $result = $this->roleRequestService->createRoleByAccount($accountId, $data);

        return response()->json($result, 201);
    }

    /**
     * @SWG\Put(
     *     path="/accounts/{accountId}/roles/{roleId}",
     *     summary="Update account role",
     *     description="Update role by account",
     *     tags={"roles"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AccountRoleRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful Response",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Invalid Account ID",
     *     )
     * ),
     */
    public function update(Request $request, int $accountId, int $roleId)
    {
        $user = $request->attributes->get('user');

        if ($accountId !== $user['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->input();

        return $this->roleRequestService->updateRoleByAccount($accountId, $roleId, $data);
    }

    /**
     * @SWG\Delete(
     *     path="/accounts/{accountId}/roles/{roleId}",
     *     summary="Delete account role",
     *     description="Delete role by account",
     *     tags={"roles"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="roleId",
     *         in="path",
     *         description="Role ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful Response",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * ),
     */
    public function delete(Request $request, int $accountId, int $roleId)
    {
        $user = $request->attributes->get('user');

        if ($accountId !== $user['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $this->roleRequestService->deleteRoleByAccount($accountId, $roleId);

        return response(null, 204);
    }
}
