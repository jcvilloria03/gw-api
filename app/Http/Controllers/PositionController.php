<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Position\PositionAuditService;
use App\Position\PositionAuthorizationService;
use App\Position\PositionEsIndexQueueService;
use App\Position\PositionRequestService;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Authz\AuthzDataScope;
/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class PositionController extends Controller
{
    /**
     * @var \App\Position\PositionRequestService
     */
    private $requestService;

    /**
     * @var \App\Position\PositionAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\Position\PositionEsIndexQueueService
     */
    private $indexQueueService;

    public function __construct(
        PositionRequestService $requestService,
        PositionAuthorizationService $authorizationService,
        AuditService $auditService,
        PositionEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }


    /**
     * @SWG\Get(
     *     path="/position/{id}",
     *     summary="Get Position",
     *     description="Get Position Details

Authorization Scope : **view.position**",
     *     tags={"position"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Position ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="integer", example=1),
     *              @SWG\Property(property="name", type="string", example="Manager"),
     *              @SWG\Property(property="company_id", type="integer", example=1),
     *              @SWG\Property(property="account_id", type="integer", example=1),
     *              @SWG\Property(property="department_id", type="integer", example=1),
     *              @SWG\Property(property="parent_id", type="integer", example=1),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", example="Position not found."),
     *              @SWG\Property(property="status_code", type="integer", example=404)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string", example="Position ID should be numeric."),
     *              @SWG\Property(property="status_code", type="integer", example=406)
     *         )
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $positionData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($positionData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/position/",
     *     summary="Create company employee positions",
     *     description="Create company position

Authorization Scope : **create.position**",
     *     tags={"position"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Position Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="department_id",
     *         in="formData",
     *         description="Department Id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="parent_id",
     *         in="formData",
     *         description="Parent Position Id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "Creating new position":{
     *                  "id":1
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "Name is missing": {
     *                 "message": "The name field is required.",
     *                 "code": 406
     *             },
     *             "Name has been used within the company": {
     *                 "message": "Position name already exists on the given company",
     *                 "code": 406
     *             },
     *             "Company ID is missing": {
     *                 "message": "The company id field is required.",
     *                 "code": 406
     *             },
     *             "Company ID is invalid": {
     *                 "message": "The selected company id is invalid.",
     *                 "code": 406
     *             },
     *             "Department ID is invalid": {
     *                 "message": "The selected department id is invalid.",
     *                 "code": 406
     *             },
     *             "Department ID is not a number": {
     *                 "message": "The department id must be an integer.",
     *                 "code": 406
     *             },
     *             "Position ID is not a number": {
     *                 "message": "The position id must be an integer.",
     *                 "code": 406
     *             },
     *             "Position ID is invalid": {
     *                 "message": "The selected position id is invalid.",
     *                 "code": 406
     *             }
     *         }
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $positionData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($positionData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        $this->indexQueueService->queue([$responseData['id']]);

        // audit log
        $positionGetResponse = $this->requestService->get($responseData['id']);
        $positionData = json_decode($positionGetResponse->getData(), true);
        $details = [
            'new' => $positionData,
        ];
        $item = new AuditCacheItem(
            PositionAuditService::class,
            PositionAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/position/bulk_create",
     *     summary="Create multiple company positions",
     *     description="You can create positions in bulk. You may also assign department and parent position
Authorization Scope : **create.position**",
     *     tags={"position"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         description="Position ID and Department ID should be under the same and selected Company ID",
     *         @SWG\Schema(ref="#/definitions/BulkCreatePositions"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *            "data": {
     *                {
     *                    "id": 1,
     *                    "name": "test1",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": null,
     *                    "parent_id": null
     *                },
     *                {
     *                    "id": 1,
     *                    "name": "test2",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": 1,
     *                    "parent_id": null
     *                },
     *                {
     *                    "id": 1,
     *                    "name": "test3",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": null,
     *                    "parent_id": 1
     *                }
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *            "Missing Company ID": {
     *                "message": "The company id field is required.",
     *                "code": 406
     *            },
     *            "Missing data field": {
     *                "message": "The data field is required.",
     *                "code": 406
     *            },
     *            "Department ID does not exists": {
     *                "message": "The selected data.*.department_id is invalid.",
     *                "code": 406
     *            },
     *            "Department ID does not belong to company": {
     *                "message": "The selected data.*.department_id is invalid.",
     *                "code": 406
     *            },
     *            "Parent position ID does not exists": {
     *                "message": "The selected data.*.parent_id is invalid.",
     *                "code": 406
     *            },
     *            "Parent position ID does not belong to company": {
     *                "message": "The selected data.*.parent_id is invalid.",
     *                "code": 406
     *            }
     *        }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="BulkCreatePositions",
     *     required={"company_id", "data"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/NewPosition"
     *         )
     *     )
     * ),
     * @SWG\Definition(
     *     definition="NewPosition",
     *     required={"name"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="parent_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *          property="department_id",
     *          type="integer",
     *     )
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');

        $inputs = $request->all();

        $positionData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($positionData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        $positionIds = [];

         // audit log
        foreach ($responseData['data'] as $position) {
            $positionGetResponse = $this->requestService->get($position['id']);
            $positionData = json_decode($positionGetResponse->getData(), true);
            $details = [
                'new' => $positionData,
            ];

            $item = new AuditCacheItem(
                PositionAuditService::class,
                PositionAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );

            $this->auditService->queue($item);

            $positionIds[] = $position['id'];
        }

        $this->indexQueueService->queue($positionIds);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/position/bulk_update",
     *     summary="Update multiple company positions details",
     *     description="You can update company position details in bulk.
Authorization Scope : **edit.position**",
     *     tags={"position"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="Body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/BulkUpdatePositions"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *            "data": {
     *                {
     *                    "id": 1,
     *                    "name": "test1",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": null,
     *                    "parent_id": null
     *                },
     *                {
     *                    "id": 1,
     *                    "name": "test2",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": 1,
     *                    "parent_id": null
     *                },
     *                {
     *                    "id": 1,
     *                    "name": "test3",
     *                    "company_id": 1,
     *                    "account_id": 1,
     *                    "department_id": null,
     *                    "parent_id": 1
     *                }
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *            "Missing Company ID": {
     *                "message": "The selected company id field is required.",
     *                "code": 406
     *            },
     *            "Missing data field": {
     *                "message": "The data field is required.",
     *                "code": 406
     *            },
     *            "Department ID does not exists": {
     *                "message": "The selected data.*.department_id is invalid.",
     *                "code": 406
     *            },
     *            "Department ID does not belong to company": {
     *                "message": "The selected data.*.department_id is invalid.",
     *                "code": 406
     *            },
     *            "Parent position ID does not exists": {
     *                "message": "The selected data.*.parent_id is invalid.",
     *                "code": 406
     *            },
     *            "Parent position ID does not belong to company": {
     *                "message": "The selected data.*.parent_id is invalid.",
     *                "code": 406
     *            }
     *        }
     *     )
     * ),
     * @SWG\Definition(
     *     definition="BulkUpdatePositions",
     *     required={"company_id", "data"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="data",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/ExistingPosition"
     *         )
     *     )
     * ),
     * @SWG\Definition(
     *     definition="ExistingPosition",
     *     required={"id","name"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @SWG\Property(
     *         property="parent_id",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *          property="department_id",
     *          type="integer",
     *     )
     * )
     */
    public function bulkUpdate(Request $request)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $positionData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate($positionData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $oldDataResponse = $this->requestService->getCompanyPositions($inputs['company_id']);
        $oldData = json_decode($oldDataResponse->getData(), true);
        $oldData = collect($oldData['data']);

        $response = $this->requestService->bulkUpdate($request->all());
        $responseData = json_decode($response->getData(), true);

        $positionIds = [];

         // audit log
        foreach ($responseData['data'] as $position) {
            $old = $oldData->where('id', $position['id']);
            $details = [
                'new' => $position,
                'old' => $old->first()
            ];
            $item = new AuditCacheItem(
                PositionAuditService::class,
                PositionAuditService::ACTION_UPDATE,
                new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
                $details
            );
            $this->auditService->queue($item);

            $positionIds[] = $position['id'];
        }

        $this->indexQueueService->queue($positionIds);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/position/is_name_available",
     *     summary="Is position name available",
     *     description="Check availability of position name with the given company",
     *     tags={"position"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Position Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="position_id",
     *         in="formData",
     *         description="Position Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {

        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');
        $positionData = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($positionData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/positions",
     *     summary="Get all Position",
     *     description="Get all Position within company.

Authorization Scope : **view.position**",
     *     tags={"position"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyPositions($id, Request $request)
    {
        $params = $request->only([
            'keyword',
            'sort_by',
            'sort_order',
            'page',
            'per_page'
        ]);

        $authzEnabled = $this->isAuthzEnabled($request);
        $authzDataScope = $this->getAuthzDataScope($request);

        if ($authzEnabled
            && !$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyPositions($id, $params);
        $positionData = json_decode($response->getData())->data;

        if (empty($positionData)) {
            return $response;
        }

        if (!$authzEnabled &&
            !$this->authorizationService->authorizeGetCompanyPositions(
                $positionData[0],
                $request->attributes->get('user')
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

 /**
     * @SWG\Put(
     *     path="/position/{id}",
     *     summary="Update Position",
     *     description="Update Position
     Authorization Scope : **edit.position**",
     *     tags={"position"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Position Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Position Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="parent_id",
     *         in="formData",
     *         description="Parent Position ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="department_id",
     *         in="formData",
     *         description="Department Id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="integer", example=1),
     *              @SWG\Property(property="name", type="string", example="Manager"),
     *              @SWG\Property(property="company_id", type="integer", example=1),
     *              @SWG\Property(property="account_id", type="integer", example=1),
     *              @SWG\Property(property="department_id", type="integer", example=1),
     *              @SWG\Property(property="parent_id", type="integer", example=1),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Request not found",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string"),
     *              @SWG\Property(property="status_code", type="integer")
     *         ),
     *         examples={
     *             "The name field is required": {"message": "The name field is required.", "status_code": 406},
     *             "Position name already exists on the given company": {
     *                 "message": "Position name already exists on the given company.",
     *                 "status_code": 406
     *             },
     *             "The company id field is required": {
     *                 "message": "The company id field is required.",
     *                 "status_code": 406
     *             },
     *             "The company id must be an integer": {
     *                 "message": "The company id must be an integer.",
     *                 "status_code": 406
     *             },
     *             "The selected company id is invalid": {
     *                 "message": "The selected company id is invalid.",
     *                 "status_code": 406
     *             },
     *             "The parent id must be an integer": {
     *                 "message": "The parent id must be an integer.",
     *                 "status_code": 406
     *             },
     *             "The selected parent id is invalid": {
     *                 "message": "The selected parent id is invalid.",
     *                 "status_code": 406
     *             },
     *             "The department id must be an integer": {
     *                 "message": "The department id must be an integer.",
     *                 "status_code": 406
     *             },
     *             "The selected department id is invalid": {
     *                 "message": "The selected department id is invalid.",
     *                 "status_code": 406
     *             },
     *             "Position ID should be numeric": {"message": "Position ID should be numeric.", "status_code": 406},
     *             "Position ID doesn't exists": {"message": "Position ID doesn't exists.", "status_code": 406}
     *         }
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldPositionData = json_decode($response->getData());
        $oldPositionData->account_id = Company::getAccountId($inputs['company_id']);

        if (!$this->authorizationService->authorizeUpdate($oldPositionData, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $this->indexQueueService->queue([$id]);

        $getResponse = $this->requestService->get($id);
        $oldPositionData = json_decode($response->getData(), true);
        $newPositionData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldPositionData,
            'new' => $newPositionData,
        ];
        $item = new AuditCacheItem(
            PositionAuditService::class,
            PositionAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

     /**
      * @SWG\Delete(
      *     path="/position/{id}",
      *     summary="Delete Position",
      *     description="Delete Position
     Authorization Scope : **delete.position**",
      *     tags={"position"},
      *     consumes={"application/x-www-form-urlencoded"},
      *     produces={"application/json"},
      *     @SWG\Parameter(
      *         type="string",
      *         name="Authorization",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         type="string",
      *         name="X-Authz-Entities",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         name="id",
      *         in="path",
      *         description="Position ID",
      *         required=true,
      *         type="integer"
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
      *         description="Successful operation",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
      *         description="Unauthorized",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
      *         description="Invalid request",
      *         @SWG\Schema(
      *              type="object",
      *              @SWG\Property(property="message", type="string", example="Position ID should be numeric."),
      *              @SWG\Property(property="status_code", type="integer", example=406)
      *         ),
      *         examples={
      *             "Currently in user": {
      *                 "message": "Selected record's is currently in use and cannot be deleted.",
      *                 "status_code": 406
      *             },
      *             "Position id should be numeric": {
      *                 "message": "Position ID should be numeric.",
      *                 "status_code": 406
      *             },
      *             "Position not found.": {
      *                 "message": "Position not found",
      *                 "status_code": 406
      *             }
      *         }
      *     )
      * )
      */
    public function delete(
        Request $request,
        PhilippineCompanyRequestService $companyRequestService,
        int $positionId
    ) {
        $deletedBy = $request->attributes->get('user');

        // get original position details
        $positionResponse = $this->requestService->get($positionId);
        $positionData = json_decode($positionResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $positionData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete(
                $positionData,
                $deletedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inUseResponse = $companyRequestService->isInUse('position', ['ids' => [$positionId]]);
        $inUseData = json_decode($inUseResponse->getData());

        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->delete($positionId);

        // audit log
        $details = [
            'old' => json_decode(json_encode($positionData), true)
        ];
        $item = new AuditCacheItem(
            PositionAuditService::class,
            PositionAuditService::ACTION_DELETE,
            new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);
        return $response;
    }
}
