<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\CompanyUser\CompanyUserService;
use App\Payroll\PayrollAuthorizationService;
use App\Payroll\PayrollFinalPayRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Traits\AuditTrailTrait;

class PayrollFinalPayController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Payroll\PayrollAuthorizationService
     */
    private $authorizationService;

    public function __construct(PayrollAuthorizationService $authorizationService)
    {
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/payroll/final",
     *     security={ {"api_key": {}} },
     *     summary="Create payroll for final pay",
     *     tags={"payroll"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="body",
     *         name="body",
     *         type="object",
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 type="object",
     *                 property="data",
     *                 @SWG\Property(property="final_pay_ids", type="array", @SWG\Items(type="integer")),
     *                 @SWG\Property(property="company_id", type="integer"),
     *                 @SWG\Property(property="account_id", type="integer"),
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function createFinalPay(
        Request $request,
        PayrollFinalPayRequestService $requestService
    ) {
        $authorized = false;
        $companyId = array_get($request->all(), 'data.company_id');
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $user = $request->attributes->get('user');
            $authData = (object) ['account_id' => $user['account_id'] ?? '', 'company_id' => $companyId];
            $authorized = $this->authorizationService->authorizeCreate($authData, $user);
        }
        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        try {
            $respnse = $requestService->createFinalPayroll($request->getContent(), $authzDataScope);

            try {
                $respnseData = json_decode($respnse->getData());
                $this->audit($request, $companyId, $respnseData);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return $respnse;
        } catch (HttpException $e) {
            $e->getPrevious()->getResponse()->getBody()->rewind();
            return response()->json(
                json_decode($e->getPrevious()->getResponse()->getBody()->getContents(), true),
                $e->getPrevious()->getResponse()->getStatusCode()
            );
        }
    }
}
