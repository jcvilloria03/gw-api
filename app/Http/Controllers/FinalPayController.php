<?php

namespace App\Http\Controllers;

use App\Audit\AuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\CompanyUser\CompanyUserService;
use App\FinalPay\FinalPayAuditService;
use App\FinalPay\FinalPayRequestService;
use App\FinalPay\FinalPayAuthorizationService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Arr;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class FinalPayController extends Controller
{
    use AuditTrailTrait;

    /*
     * App\FinalPay\FinalPayRequestService
     */
    protected $requestService;

    /*
     * App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /*
     * App\FinalPay\FinalPayAuthorizationService
     */
    protected $authorizationService;

    /*
     * App\Audit\AuditService
    */
    protected $auditService;

    public function __construct(
        FinalPayRequestService $requestService,
        EmployeeRequestService $employeeRequestService,
        FinalPayAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->employeeRequestService = $employeeRequestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/bonuses",
     *     summary="Get undeducted bonuses for given employee",
     *     description="Get undeducted bonuses for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getBonuses($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getBonuses((int) $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/commissions",
     *     summary="Get undeducted commissions for given employee",
     *     description="Get undeducted commissions for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCommissions($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCommissions((int) $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/loans",
     *     summary="Get unpaid loans for given employee",
     *     description="Get unpaid loans for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getLoans(
        $employeeId,
        Request $request,
        PayrollLoanRequestService $payrollLoanRequestService
    ) {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $payrollLoanRequestService->getEmployeeUnpaidLoans(
            (int) $companyId,
            (int) $employeeId
        );
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/allowances",
     *     summary="Get allowances available for final pay for given employee",
     *     description="Get allowances available for final pay for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAllowances($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAllowances((int) $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/deductions",
     *     summary="Get deductions available for final pay for given employee",
     *     description="Get deductions available for final pay for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getDeductions($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getDeductions((int) $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/adjustments",
     *     summary="Get adjustments available for final pay for given employee",
     *     description="Get adjustments available for final pay for given employee
     Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAdjustments($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAdjustments((int) $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/contributions/{terminationInformationId}",
     *     summary="Get contributions for final pay for given employee",
     *     description="Get contributions for final pay for given employee
    Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="terminationInformationId",
     *         in="path",
     *         description="Termination Information Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getContributions($employeeId, Request $request, $terminationInformationId)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeGet($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getContributions((int) $employeeId, (int) $terminationInformationId);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employees/final_pay",
     *     summary="Get list of Employees for Final Pay (Final Pay Candidates)",
     *     description="Get list of Employees for Final Pay (Final Pay Candidates)
    Authorization Scope : **create.final_pays**",
     *     tags={"final_pay"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *      *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="release_date_from",
     *         in="query",
     *         description="Proposed Release Date From",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="release_date_to",
     *         in="query",
     *         description="Proposed Release Date To",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="type", type="string", example="final-pay"),
     *                      @SWG\Property(property="id", type="integer", example=1),
     *                      @SWG\Property(
     *                          property="attributes",
     *                          type="object",
     *                          @SWG\Property(property="employee_id", type="integer"),
     *                          @SWG\Property(property="employee_name", type="integer"),
     *                          @SWG\Property(property="start_date_basis", type="integer"),
     *                          @SWG\Property(property="last_date", type="integer"),
     *                          @SWG\Property(property="release_date", type="integer"),
     *                          @SWG\Property(property="location_id", type="integer"),
     *                          @SWG\Property(property="department_id", type="integer"),
     *                          @SWG\Property(property="position_id", type="integer"),
     *                          @SWG\Property(property="team_id", type="integer"),
     *                      )
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *          description="Invalid request",
     *          ref="$/responses/InvalidRequestResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getUnprocessedFinalPays(Request $request, $companyId)
    {
        $user = $request->attributes->get('user');
        $authorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $data = (object) [
                'account_id' => (int) Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $authorized = $this->authorizationService->authorizeCreate($data, $user);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getUnprocessedFinalPays($companyId, $request->all(), $authzDataScope);
        if (!$response->isSuccessful()) {
            return $response;
        }
        $responseData = json_decode($response->getData(), true);
        $item = new AuditCacheItem(
            FinalPayAuditService::class,
            FinalPayAuditService::ACTION_CREATE,
            new AuditUser($user['user_id'], $user['account_id']),
            [
                'new' => $responseData,
            ]
        );
        $this->auditService->queue($item);

        return $response;
    }


    /**
     * @SWG\POST(
     *     path="/employee/{employeeId}/final_pay/compute",
     *     summary="Compute final pay",
     *     description="Compute final pay for given parameters
    Authorization Scope : **create.final_pays**",
     *     tags={"final_pay"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_group_id",
     *         in="formData",
     *         description="Payroll group id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="last_day",
     *         in="formData",
     *         description="Date of last working day",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_date",
     *         in="formData",
     *         description="Date for final pay to be paid",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="loan_ids[]",
     *         in="formData",
     *         description="Array of ids of payroll loans to be included in final pay",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="other_income_item_ids[]",
     *         in="formData",
     *         description="Array of ids of other incomes to be included in final pay",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function compute($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');
        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        if (!$this->authorizationService->authorizeCreate($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->compute((int) $employeeId, $request->all());

        if ($response->isSuccessful()) {
            // trigger audit trail
            $responseData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $responseData);
        }

        return $response;
    }

    /**
     * @SWG\POST(
     *     path="/employee/{employeeId}/final_pay/save",
     *     summary="Save final pay",
     *     description="Compute final pay for given parameters
    Authorization Scope : **create.final_pays**",
     *     tags={"final_pay"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="final_pay",
     *         in="body",
     *         description="Create final pay",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/finalPay")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     * ),
     *@SWG\Definition(
     *     definition="finalPay",
     *     required={"termination_information_id", "company_id", "status", "final_pay_items"},
     *     @SWG\Property(
     *         property="termination_information_id",
     *         type="integer",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="status",
     *         type="string",
     *         example="OPENED"
     *     ),
     *     @SWG\Property(
     *         property="final_pay_items",
     *         type="array",
     *         items={
     *                  "type"="object",
     *                  "properties"={
     *                      "item_id"={"type"="integer", "example"="1"},
     *                      "amount"={"type"="number", "example"="100"},
     *                      "type"={"type"="string", "example"="allowance_type"},
     *                      "type_name"={"type"="string", "example"="Fuel Allowance"},
     *                  }
     *              }
     *     ),
     * )
     */
    public function save($employeeId, Request $request)
    {
        $user = $request->attributes->get('user');
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);
        $companyId = array_get($employee, 'company_id');

        $data = (object) [
            'account_id' => (int) Company::getAccountId($companyId),
            'company_id' => $companyId
        ];

        // authorize
        if (!$this->authorizationService->authorizeCreate($data, $user)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->save((int) $employeeId, $request->all());

        // if there are validation errors, display errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        if ($response->isSuccessful()) {
            $this->audit($request, $companyId, $responseData['final_pay']);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/final_pay",
     *     security={ {"api_key": {}} },
     *     summary="Save final pay items",
     *     description="Save the final pay items to be considered in the Final Pay computation of the employee.<br/>
                This also changes the Employee status from ACTIVE to SEMI-ACTIVE.<br/>
                Authorization Scope : **create.final_pays**",
     *     tags={"final_pay"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *             required={"data"},
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 required={"employee_id", "termination_information_id", "final_pay_items"},
     *                 @SWG\Property(property="employee_id", type="integer"),
     *                 @SWG\Property(property="company_id", type="integer"),
     *                 @SWG\Property(property="termination_information_id", type="integer"),
     *                 @SWG\Property(
     *                     property="final_pay_items",
     *                     type="array",
     *                     @SWG\Items(ref="#/definitions/FinalPayItem")
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(ref="#/definitions/FinalPay")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Invalid request.",
     *         examples={
     *              "Field validation errors": {
     *                  "errors": {
     *                      "employee_id": {
     *                          "Employee does not exist"
     *                      }
     *                  }
     *              },
     *              "Items field validation errors": {
     *                  "errors": {
     *                      "final_pay_items.0.type": {
     *                          "The selected Final pay item type is invalid."
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         ref="$/responses/InternalServerErrorResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function saveFinalPay(Request $request)
    {
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $data = $request->get('data', []);
            if (!Arr::has($data, 'employee_id')) {
                return response()->json([
                    'message' => 'Invalid Request',
                    'errors' => ['employee_id' => ['Employee does not exist.']]
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $employee = json_decode(
                $this->employeeRequestService->getEmployee(Arr::get($data, 'employee_id'))->getData(),
                true
            );
            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $user = $request->attributes->get('user');
            $authData = (object)[
                'account_id' => $user['account_id'] ?? '',
                'company_id' => array_get($request->all(), 'data.company_id'),
            ];

            $authorized = $this->authorizationService->authorizeCreate($authData, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        try {
            $response = $this->requestService->saveFinalPay($request->getContent());

            if ($response->isSuccessful()) {
                $responseData = json_decode($response->getData(), true);
                $companyId = array_get($request->all(), 'data.company_id');
                $this->audit($request, $companyId, $responseData);
            }

            return $response;
        } catch (HttpException $e) {
            $e->getPrevious()->getResponse()->getBody()->rewind();
            return response()->json(
                json_decode($e->getPrevious()->getResponse()->getBody()->getContents(), true),
                $e->getPrevious()->getResponse()->getStatusCode()
            );
        }
    }

    /**
     * @SWG\Patch(
     *     path="/final_pay/{id}",
     *     summary="Patch final pay",
     *     security={ {"api_key": {}} },
     *     description="Make employee semi active usng final pay id",
     *     tags={"final_pay"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *          description="Final pay id",
     *          required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *             required={"data"},
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                 required={"company_id", "employee_id"},
     *                 @SWG\Property(property="company_id", type="integer"),
     *                 @SWG\Property(property="employee_id", type="integer")
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *          response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *          description="Final pay not found",
     *          ref="$/responses/ErrorResponseMessage",
     *          @SWG\Schema(@SWG\Property(
     *              property="status_code",
     *              type="integer",
     *              description="Status Code",
     *              example=404
     *          )),
     *     ),
     *     @SWG\Response(
     *          response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *          description="Invalid request",
     *          ref="$/responses/InvalidRequestResponse"
     *      ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function patchFinalPay(Request $request, int $id)
    {
        $user = $request->attributes->get('user');
        $data = $request->get('data');
        $companyId = $data['company_id'] ?? null;
        $employeeId = $data['employee_id'] ?? null;
        if ($companyId === null) {
            $this->invalidRequestError('Company id is required.');
        } elseif ($employeeId === null) {
            $this->invalidRequestError('Employee id is required.');
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode($this->employeeRequestService->getEmployee($employeeId)->getData(), true);
            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => (int) Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $authorized = $this->authorizationService->authorizeGet($data, $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->patchFinalPay($id, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/employee/{employeeId}/final_pay/available_items",
     *     summary="Get contributions for final pay for given employee",
     *     description="Get contributions for final pay for given employee
                Authorization Scope : **view.final_pays**",
     *     tags={"final_pay"},
     *     security={ {"api_key": {}} },
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function getAvailableItems(Request $request, $employeeId)
    {
        $response = $this->employeeRequestService->getEmployee((int) $employeeId);
        $employee = json_decode($response->getData(), true);


        if ($this->isAuthzEnabled($request)) {
            $dataScope = $this->getAuthzDataScope($request);

            $isAuthorized =
                $dataScope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => data_get($employee, 'company_id'),
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employee, 'department_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employee, 'location_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group.id'),
                        AuthzDataScope::SCOPE_POSITION => data_get($employee, 'position_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employee, 'team_id'),
                    ]
                );
        } else {
            $companyId = array_get($employee, 'company_id');
            $data = (object) [
                'account_id' => (int) Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $data,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAvailableItems((int) $employeeId);
    }
}
