<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ESS\EssNotificationRequestService;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\Notification\EssNotificationAuthorizationService;

class EssNotificationController extends EssBaseController
{
    /**
     * @var \App\ESS\EssNotificationRequestService
     */
    protected $requestService;

    /**
     * @var \App\Notification\EssNotificationAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        EssNotificationRequestService $requestService,
        EssNotificationAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/notifications",
     *     summary="Get User Notifications",
     *     description="Get all Notifications for user.
     Authorization Scope : **ess.view.notification**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *         description="Items to display per one page. Default: 10",
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1. Default is 1.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request"
     *     )
     * )
     */
    public function getNotifications(Request $request)
    {
        $userId = $this->getFirstEmployeeUser($request)['user_id'];
        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeView($userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        return  $this->requestService->getNotifications($userId, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/ess/notifications_status",
     *     summary="Get Notifications Status",
     *     description="Get Notifications Status for user.
     Authorization Scope : **ess.view.notification**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request"
     *     )
     * )
     */
    public function getNotificationsStatus(Request $request)
    {
        $userId = $this->getFirstEmployeeUser($request)['user_id'];
        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeView($userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $notificationsStatus = $this->requestService->getNotificationsStatus($userId);

        return $notificationsStatus;
    }

    /**
     * @SWG\Put(
     *     path="/ess/notifications_status",
     *     summary="Update Notifications Status",
     *     description="Update Notifications Status for user.
     Authorization Scope : **ess.view.notification**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request"
     *     )
     * )
     */
    public function updateNotificationsStatus(Request $request)
    {
        $userId = $this->getFirstEmployeeUser($request)['user_id'];
        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeView($userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $notificationsStatus = $this->requestService->updateNotificationsStatus($userId);

        return $notificationsStatus;
    }

     /**
     * @SWG\Put(
     *     path="/ess/notifications/{id}/clicked",
     *     summary="Update Notification clicked status",
     *     description="Update Notification clicked status
     Authorization Scope : **ess.view.notification**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Notification ID (It is an ID from notifications table on nt-api database)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function notificationClicked(Request $request, $id)
    {
        $userId = $this->getFirstEmployeeUser($request)['user_id'];
        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeView($userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $notification = $this->requestService->notificationClicked($id);

        return $notification;
    }
}
