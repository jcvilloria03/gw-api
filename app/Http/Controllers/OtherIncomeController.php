<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\CSV\CsvValidator;
use App\Employee\EmployeeRequestService;
use App\OtherIncome\OtherIncomeService;
use App\OtherIncome\OtherIncomeUploadTask;
use App\OtherIncome\OtherIncomeUploadTaskException;
use App\Facades\Company;
use App\Jobs\JobsRequestService;
use App\OtherIncome\OtherIncomeAuditService;
use App\OtherIncome\OtherIncomeAuthorizationServiceFactory;
use App\OtherIncome\OtherIncomeRequestService;
use App\Storage\OtherIncomeUploadService;
use Aws\S3\Exception\S3Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class OtherIncomeController extends Controller
{
    use AuditTrailTrait;

    const BONUS = 'bonus';
    const COMMISSION = 'commission';
    const ALLOWANCE = 'allowance';
    const DEDUCTION = 'deduction';
    const ADJUSTMENT = 'adjustment';

    const OTHER_INCOME_TYPES_NAMESPACES = [
        self::BONUS => 'App\Model\PhilippineBonus',
        self::COMMISSION => 'App\Model\PhilippineCommission',
        self::ALLOWANCE => 'App\Model\PhilippineAllowance',
        self::DEDUCTION => 'App\Model\PhilippineDeduction',
        self::ADJUSTMENT => 'App\Model\PhilippineAdjustment',
    ];

    const OTHER_INCOME_TYPES_MODULES = [
        self::BONUS => 'employees.people.bonuses',
        self::COMMISSION => 'employees.people.commissions',
        self::ALLOWANCE => 'employees.people.allowances',
        self::DEDUCTION => 'employees.people.deductions',
        self::ADJUSTMENT => 'employees.people.adjustments',
    ];

    const OTHER_INCOME_TYPES_EMPLOYEE_MODULES = [
        self::ALLOWANCE => 'employees.allowances',
        self::DEDUCTION => 'employees.deductions',
    ];

    /*
     * App\OtherIncome\OtherIncomeRequestService
     */
    protected $requestService;

    /*
     * App\OtherIncome\OtherIncomeAuditService
     */
    protected $auditService;

    /**
     * @var App\CSV\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var App\Storage\OtherIncomeUploadService
     */
    protected $uploadService;

    public function __construct(
        OtherIncomeRequestService $requestService,
        OtherIncomeAuditService $auditService,
        CsvValidator $csvValidator,
        OtherIncomeUploadService $uploadService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
        $this->csvValidator = $csvValidator;
        $this->uploadService = $uploadService;
    }

    /**
     * @SWG\Get(
     *     path="/other_income/{id}",
     *     summary="Get Other Income ",
     *     description="Get Other Income Details
    Authorization Scope : **view.bonuses**, **view.commissions**,
     **view.allowances**, **view.deductions**, **view.adjustments**",
     *     tags={"other_income"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Other Income ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $authzDataScope = $this->getAuthzDataScope($request);
        $response = $this->requestService->get($id, $authzDataScope);
        $otherIncome = json_decode($response->getData(), true);
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            foreach (OtherIncomeAuthorizationServiceFactory::getModules($otherIncome['type_name']) as $module) {
                if ($authzDataScope
                    ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $otherIncome['company_id'], $module)
                ) {
                    $authorized = true;
                    break;
                }
            }
        } else {
            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                array_get($otherIncome, 'type_name')
            );

            $user = $request->attributes->get('user');
            $companyId = array_get($otherIncome, 'company_id');

            $authorized = $authorizationService->authorizeGet((object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ], $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/other_incomes/{type}",
     *     summary="Get Other Incomes",
     *     description="Get Other Incomes,
    Authorization Scope : **view.bonuses**, **view.commissions**,
     **view.allowances**, **view.deductions**, **view.adjustments**",
     *     tags={"other_income"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="path",
     *         description="Other Income Type",
     *         required=true,
     *         enum={"bonus_type","commission_type","allowance_type", "deduction_type", "adjustment_type"}
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[keyword]",
     *         in="query",
     *         description="Filter by Keyword",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[other_income_type_ids][]",
     *         type="array",
     *         in="query",
     *         description="Other Income Type Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[location_ids][]",
     *         type="array",
     *         in="query",
     *         description="Location Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[department_ids][]",
     *         type="array",
     *         in="query",
     *         description="Department Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[position_ids][]",
     *         type="array",
     *         in="query",
     *         description="Position Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[employee_ids][]",
     *         type="array",
     *         in="query",
     *         description="Employee Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="filter[payroll_group_ids][]",
     *         type="array",
     *         in="query",
     *         description="Payroll Group Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Adjustment": {
     *                  "data": {{
     *                      "id": 1,
     *                      "type_id": 1,
     *                      "company_id": 1,
     *                      "amount": 100,
     *                      "reason": "Adjustment Reason",
     *                      "type_name": "App\Model\PhilippineAdjustment",
     *                      "release_details": {},
     *                      "type": {
     *                          "id": 1,
     *                          "company_id": 1,
     *                          "name": "Other Income Type",
     *                          "fully_taxable": false,
     *                          "max_non_taxable": null,
     *                          "other_income_type_subtype_id": 1,
     *                          "other_income_type_subtype_type": "App\Model\PhilippineAdjustmentType",
     *                      },
     *                  }}
     *              },
     *              "Bonus": {
     *                  "data": {{
     *                      "id": 1,
     *                      "type_id": 1,
     *                      "type": {
     *                          "id": 1,
     *                          "company_id": 1,
     *                          "name": "Other Income Type",
     *                          "fully_taxable": false,
     *                          "max_non_taxable": null,
     *                          "other_income_type_subtype_id": 1,
     *                          "other_income_type_subtype_type": "App\Model\PhilippineBonusType",
     *                      },
     *                      "company_id": 1,
     *                      "amount": 1000,
     *                      "percentage": 0,
     *                      "auto_assign": false,
     *                      "inactive": false,
     *                      "type_name": "App\Model\PhilippineBonus",
     *                      "basis": "BASIS",
     *                      "release_details": {},
     *                  }}
     *              },
     *              "Commission": {
     *                  "data": {{
     *                      "id": 1,
     *                      "type_id": 1,
     *                      "company_id": 1,
     *                      "amount": 1000,
     *                      "percentage": 0,
     *                      "auto_assign": false,
     *                      "inactive": false,
     *                      "type_name": "App\Model\PhilippineCommission",
     *                      "release_details": {},
     *                      "type": {
     *                          "id": 1,
     *                          "company_id": 1,
     *                          "name": "Other Income Type",
     *                          "fully_taxable": false,
     *                          "max_non_taxable": null,
     *                          "other_income_type_subtype_id": 1,
     *                          "other_income_type_subtype_type": "App\Model\PhilippineCommissionType",
     *                      },
     *                  }}
     *              },
     *              "Deduction": {
     *                  "data": {{
     *                      "id": 1,
     *                      "type_id": 1,
     *                      "company_id": 1,
     *                      "amount": 100,
     *                      "inactive": false,
     *                      "recurring": true,
     *                      "frequency": "EVERY_PAY_OF_THE_MONTH",
     *                      "valid_from": "2020-01-01",
     *                      "valid_to": "2020-02-01",
     *                      "type_name": "App\Model\PhilippineCommission",
     *                      "type": {
     *                          "id": 1,
     *                          "company_id": 1,
     *                          "name": "Other Income Type",
     *                          "fully_taxable": false,
     *                          "max_non_taxable": null,
     *                          "other_income_type_subtype_id": 1,
     *                          "other_income_type_subtype_type": "App\Model\PhilippineDeductionType",
     *                      },
     *                      "created_at": "2020-06-19 00:00:00",
     *                  }}
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function getAllCompanyIncomes(Request $request, $id, $type)
    {
        $this->validate($request, [
            'page' => 'integer',
            'per_page' => 'integer',
            'filter' => 'array',
            'filter.other_income_type_ids' => 'array',
            'filter.other_income_type_ids.*' => 'integer',
            'filter.location_ids' => 'array',
            'filter.location_ids.*' => 'integer',
            'filter.department_ids' => 'array',
            'filter.department_ids.*' => 'integer',
            'filter.position_ids' => 'array',
            'filter.position_ids.*' => 'integer',
            'filter.employee_ids' => 'array',
            'filter.employee_ids.*' => 'integer',
            'filter.payroll_group_ids' => 'array',
            'filter.payroll_group_ids.*' => 'integer',
            'filter.payroll_date' => 'string'
        ]);

        $authorized = false;
        $authzDataScope = null;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $modules = OtherIncomeService::AUTHZ_MODULES[str_replace('_type', '', $type)] ?? [];
            foreach ($modules as $module) {
                if ($this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id, $module)) {
                    $authorized = true;
                    break;
                }
            }
        } else {
            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                self::OTHER_INCOME_TYPES_NAMESPACES[str_replace('_type', '', $type)]
            );

            $authorized = $authorizationService->authorizeGetAll((object) [
                'account_id' => $id ? Company::getAccountId($id) : null,
                'company_id' => $id,
            ], $request->attributes->get('user'));
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->getAllCompanyIncomes(
            (int) $id,
            $type,
            $authzDataScope,
            $request->only([ // Cascade the query string to pr-api endpoint
                'page',
                'per_page',
                'filter.keyword',
                'filter.other_income_type_ids',
                'filter.location_ids',
                'filter.department_ids',
                'filter.position_ids',
                'filter.employee_ids',
                'filter.payroll_group_ids',
                'filter.payroll_date'
            ])
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/other_incomes/{type}/download",
     *     summary="Download Other Incomes CSV",
     *     description="Download Other Incomes CSV,
    Authorization Scope : **view.bonuses**, **view.commissions**,
     **view.allowances**, **view.deductions**, **view.adjustments**",
     *     tags={"other_income"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="path",
     *         description="Other Income Type",
     *         required=true,
     *         enum={"bonus_type","commission_type","allowance_type", "deduction_type", "adjustment_type"}
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="formData",
     *         description="Other Income Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function downloadOtherIncomesCsv(Request $request, $id, $type)
    {
        $otherIncomeIds = $request->input('ids') ?: [];
        $otherIncomes = [];

        if ($this->isAuthzEnabled($request)) {
            if (!$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
                $this->response()->errorUnauthorized();
            }
        } else {
            $user = $request->attributes->get('user');

            $response = $this->requestService->getMultiple(
                (int) $id,
                $otherIncomeIds
            );

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                return $response;
            }

            $otherIncomes = array_get(
                json_decode($response->getData(), true),
                'data',
                []
            );

            $this->checkExistenceAndAuthorization(
                $otherIncomeIds,
                $otherIncomes,
                $user,
                'authorizeGetAll'
            );
        }

        $response = $this->requestService->downloadOtherIncomesCsv(
            (int) $id,
            $type,
            $otherIncomeIds,
            $this->getAuthzDataScope($request)
        );

        if ($response->isSuccessful()) {
            if (empty($otherIncomes)) {
                $result = $this->requestService->getMultiple(
                    (int) $id,
                    $otherIncomeIds
                );
                $otherIncomes = array_get(
                    json_decode($result->getData(), true),
                    'data',
                    []
                );
            }
            // trigger audit trail
            $this->audit($request, $id, [], $otherIncomes);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/other_income/is_delete_available",
     *     summary="Is delete available for Other Income Types",
     *     description="Is delete available for Other Income Types,
    Authorization Scope : **view.bonuses**, **view.commissions**,
     **view.allowances**, **view.deductions**, **view.adjustments**",
     *     tags={"other_income"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="formData",
     *         description="Other Income Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function isDeleteAvailable(Request $request, OtherIncomeService $otherIncomeService, $id)
    {
        if (
            $this->isAuthzEnabled($request)
            && !$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)
        ) {
            $this->response()->errorUnauthorized();
        }

        $otherIncomeIds = $request->input('ids') ?: [];
        $response = $this->requestService->getMultiple(
            (int) $id,
            $otherIncomeIds
        );
        $otherIncomes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );
        if ($this->isAuthzEnabled($request)) {
            $result = $otherIncomeService->checkAuthorization(
                $otherIncomeIds,
                $otherIncomes,
                $this->getAuthzDataScope($request)
            );

            if ($result['code'] !== Response::HTTP_OK) {
                $this->response()->error($result['message'], $result['code']);
            }
        } else {
            $user = $request->attributes->get('user');
            $this->checkExistenceAndAuthorization(
                $otherIncomeIds,
                $otherIncomes,
                $user,
                'authorizeGetAll'
            );
        }

        return $this->requestService->isDeleteAvailable(
            (int) $id,
            $otherIncomeIds
        );
    }

    /**
     * @SWG\Delete(
     *     path="/company/{id}/other_income",
     *     summary="Delete Multiple Other Incomes",
     *     description="Delete Multiple Other Incomes,
    Authorization Scope : **delete.bonuses**, **delete.commissions**,
     **delete.allowances**, **delete.deductions**, **delete.adjustments**",
     *     tags={"other_income"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="query",
     *         description="Other Income Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function delete(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $ids = $request->input('ids') ?: [];

        if ($this->isAuthzEnabled($request)) {
            if (!$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
                $this->response()->errorUnauthorized();
            }
        }

        $response = $this->requestService->getMultiple(
            (int) $id,
            $ids
        );

        $otherIncomes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if ($this->isAuthzEnabled($request)) {
            $responseOtherIncomeIds = array_column($otherIncomes, 'id');
            $invalidOtherIncomes = array_diff($ids, $responseOtherIncomeIds);

            if (!empty($invalidOtherIncomes)) {
                abort(406, 'Other income id not found. [' . implode(',', $invalidOtherIncomes) . ']');
            }

            $unauthorizedOtherIncomes = collect($otherIncomes)->reject(function ($otherIncome) use ($request) {
                foreach (OtherIncomeAuthorizationServiceFactory::getModules($otherIncome['type_name']) as $module) {
                    if ($this->getAuthzDataScope($request)
                        ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $otherIncome['company_id'], $module)
                    ) {
                        return true;
                        break;
                    }
                }
                return false;
            });

            if ($unauthorizedOtherIncomes->isNotEmpty()) {
                $this->response->errorUnauthorized();
            }
        } else {
            $this->checkExistenceAndAuthorization(
                $ids,
                $otherIncomes,
                $user,
                'authorizeDelete'
            );
        }

        $response = $this->requestService->deleteMultiple(
            (int) $id,
            $ids,
            $this->getAuthzDataScope($request)
        );

        if ($response->isSuccessful()) {
            foreach ($otherIncomes as $otherIncome) {
                // trigger audit trail
                $this->audit($request, $id, [], $otherIncome);
            }
        }

        return $response;
    }

    /**
     * Authorize task
     *
     * @param $providedIds
     * @param $otherIncomes
     * @param $user
     * @param $authorizeMethod
     * @throws \Exception
     */
    private function checkExistenceAndAuthorization(
        $providedIds,
        $otherIncomes,
        $user,
        $authorizeMethod
    ) {
        $specifiedAndFoundDiff = collect($providedIds)
            ->diff(
                collect($otherIncomes)->map(function ($otherIncome) {
                    return array_get($otherIncome, 'id');
                })
            );

        if ($specifiedAndFoundDiff->count() > 0) {
            $this->notFoundError(
                'Specified Other Income(s) not found. Id(s): ' .
                    $specifiedAndFoundDiff->implode(',')
            );
        }

        $authorizedIds = collect([]);

        foreach ($otherIncomes as $otherIncome) {
            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                array_get($otherIncome, 'type_name')
            );
            $authData = (object) [
                'other_income_id' => array_get($otherIncome, 'id'),
                'account_id' => Company::getAccountId(
                    array_get($otherIncome, 'company_id')
                ),
                'company_id' => array_get($otherIncome, 'company_id')
            ];
            if ($authorizationService->{$authorizeMethod}($authData, $user)) {
                $authorizedIds->push(
                    array_get($otherIncome, 'id')
                );
            }
        }

        $unauthorizedIds = collect($providedIds)
            ->diff(
                $authorizedIds
            );

        if ($unauthorizedIds->count() > 0) {
            $this->response()->errorUnauthorized(
                'Not authorized. Ids: ' .
                    $unauthorizedIds->implode(',')
            );
        }
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/other_income/{type}/upload",
     *     summary="Upload Other income",
     *     description="Upload Other income
    Authorization Scope : **create.bonuses**, **create.allowances**,
     **create.commissions**,**create.deductions**, **create.adjustments**",
     *     tags={"other_income"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         type="integer",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Other Income Types",
     *         type="string",
     *         required=true,
     *         enum=\App\OtherIncome\OtherIncomeService::OTHER_INCOMES
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "other_income:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request, int $id, string $type)
    {
        $createdBy = $request->attributes->get('user');
        $authzModule = null;
        $companyId = $id;

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $module = OtherIncomeService::AUTH_MODULES[$type] ?? null;

            if ($module) {
                $currentAuthz = $this->getAuthzDataScope($request);
                $authzModule = new AuthzDataScope(
                    $currentAuthz->getDataScopeForModule($module),
                    $currentAuthz->getClearance()
                );
            }

            $authorized = $this
                ->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id, $module);
        } else {
            $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                $this->getOtherIncomeTypeNamespace($type)
            );
            $authorized = $authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized('Not authorized.');
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            // $uploadTask = \App::make(OtherIncomeUploadTask::class);

            // $uploadTask->create($companyId);
            //$s3Key = $this->uploadService->saveFile($file->getPathName());
            //dd('before');
            $s3Key = $this->uploadService->saveFile($file, $createdBy, 'public-read');
            //dd($s3Key);
            $s3Bucket = $this->uploadService->getS3Bucket();
            //call Payroll API to create processing job
            
            $response = $this->requestService->processFileUpload(
                $companyId,
                $type,
                $s3Bucket,
                $s3Key,
                $authzModule
            );
            $responseData = json_decode($response->getOriginalContent(), true)["data"];
            $jobId = $responseData['id'];
            return response()->json([
                'id' => $jobId,
                'job_id' => $jobId
            ]);
        } catch (S3Exception $e) {
            //Log::error($e->getMessage());
            //$response = $e->getResponse();
            dd($e->getMessage());
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/other_income/{type}/upload/status",
     *     summary="Get Job Status",
     *     description="Get Other Income Upload Status
    Authorization Scope : **create.bonuses**, **create.allowances**,
     **create.commissions**,**create.deductions**",
     *     tags={"other_income"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         type="integer",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Other Income Types",
     *         type="string",
     *         required=true,
     *         enum=\App\OtherIncome\OtherIncomeService::OTHER_INCOMES
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Employee not found"
     *                          },
     *                          "4": {
     *                              "Amount is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request, int $id, string $type)
    {
        $companyId = $id;
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $module = OtherIncomeService::AUTH_MODULES[$type] ?? null;
            $authorized = $this
                ->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id, $module);
        } else {
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                $this->getOtherIncomeTypeNamespace($type)
            );
            $authorized = $authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized('Not authorized.');
        }

        $jobId = $request->input('job_id');
        //return $this->requestService->getFileUploadStatus($companyId, $type, $jobId);
        $response = $this->requestService->getFileUploadStatus($companyId, $type, $jobId);
        //TODO:: make this just return JM-API response
        if ($response->status() == 400) {
            $this->notFoundError('Job not found');
        }

        $responseBody = json_decode($response->getOriginalContent(), true);
        $data = $responseBody['data'];
        $attributes = $data['attributes'];
        $status = $attributes['status'];
        $errors = $responseBody['included'] ?? null;

        //TODO: temporary matching
        $returnedErrors = null;
        $returnedStatus = "saved";
        $errorName = null;
        $error = null;
        if (empty($errors) && $status == 'PENDING') {
            $returnedStatus = "validating";
        } else if (!empty($errors) && $status == 'FINISHED') {
            $error = $errors[0]["attributes"]["description"];
            $errorName = $error["name"];
        }

        if ($errorName == "task-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = (object) ["0" =>  $error["errors"]];
        } else if ($errorName == "validation-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = JobsRequestService::flattenErrorData($error["errors"]);
        }

        return response()->json([   //TODO:: replace this with JM-API raw response
            'status' => $returnedStatus,
            'errors' => $returnedErrors
        ]);
    }

    /**
     * getCompanyAccountId
     */
    public function getCompanyAccountId(int $companyId)
    {
        return Company::getAccountId($companyId);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/other_income/{type}/upload/save",
     *     summary="Save Other Income",
     *     deprecated=true,
     *     description="Saves Other Income from Previously Uploaded CSV
    Authorization Scope : **create.bonuses**, **create.allowances**,
     **create.commissions**,**create.deductions**",
     *     tags={"other_income"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Other Income Types",
     *         type="string",
     *         required=true,
     *         enum=\App\OtherIncome\OtherIncomeService::OTHER_INCOMES
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "other_income_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Other Income needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request, $type)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
            $this->getOtherIncomeTypeNamespace($type)
        );

        // authorize
        if (!$authorizationService->authorizeCreate($data, $createdBy)) {
            $this->response()->errorUnauthorized('Not authorized.');
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(OtherIncomeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(OtherIncomeUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (OtherIncomeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
            'type' => $type
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        \Amqp::publish(config('queues.other_income_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/company/other_income/{type}/upload/preview",
     *     summary="Get Other income Preview",
     *     description="Get Other income Upload Preview
    Authorization Scope : **create.bonuses**, **create.allowances**,
     **create.commissions**,**create.deductions**",
     *     tags={"other_income"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Other Income Types",
     *         type="string",
     *         required=true,
     *         enum=\App\OtherIncome\OtherIncomeService::OTHER_INCOMES
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request, $type)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $moduleName = Arr::get(self::OTHER_INCOME_TYPES_EMPLOYEE_MODULES, $type);

            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId,
                $moduleName
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];

            $authorizationService = OtherIncomeAuthorizationServiceFactory::get(
                $this->getOtherIncomeTypeNamespace($type)
            );

            $isAuthorized = $authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized('Not authorized.');
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(OtherIncomeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (OtherIncomeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employee/{employeeId}/other_incomes/{type}",
     *     summary="Get Employee Other Incomes",
     *     description="Get Employee Other Incomes
     Authorization Scope : **view.bonuses**, **view.allowances**,
     **view.commissions**,**view.deductions**",
     *     tags={"other_income"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         type="integer",
     *         in="path",
     *         description="Employee ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="path",
     *         description="Other Income Type",
     *         required=true,
     *         enum=\App\OtherIncomeType\OtherIncomeTypeService::OTHER_INCOME_TYPES
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function getEmployeeOtherIncomes(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $companyId,
        int $employeeId,
        string $type
    ) {
        $isAuthzEnabled = $this->isAuthzEnabled($request);

        if ($isAuthzEnabled) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $otherIncomeType = str_replace('_type', '', $type);
            $moduleName = Arr::get(self::OTHER_INCOME_TYPES_MODULES, $otherIncomeType);

            $scopes = [
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ];

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized($scopes, $moduleName);

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $user = $request->attributes->get('user');
        try {
            $response = $this->requestService->getEmployeeOtherIncomes(
                (int) $companyId,
                (int) $employeeId,
                $type
            );
        } catch (\Throwable $th) {
            $response = [];
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $otherIncomes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if (!$isAuthzEnabled) {
            $this->checkExistenceAndAuthorization(
                collect($otherIncomes)
                    ->map(function ($otherIncome) {
                        return $otherIncome['id'];
                    }),
                $otherIncomes,
                $user,
                'authorizeGetAll'
            );
        }

        return $response;
    }

    /**
     * Get Other Income Type namespace based on type
     *
     * @param string $type
     * @return string
     */
    private function getOtherIncomeTypeNamespace($type)
    {
        if (empty(self::OTHER_INCOME_TYPES_NAMESPACES[$type])) {
            $this->response()->errorNotFound(
                "Type {$type} not found."
            );
        }
        return self::OTHER_INCOME_TYPES_NAMESPACES[$type];
    }
}
