<?php

namespace App\Http\Controllers;

use App\Audit\AuditService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\ESS\EssTimeDisputeRequestRequestService;
use App\Facades\Company;
use App\Workflow\WorkflowRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TimeDisputeController
 *
 * @package App\Http\Controllers
 */
class TimeDisputeRequestController extends Controller
{

    /**
     * @var \App\ESS\EssTimeDisputeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EssTimeDisputeRequestRequestService $requestService,
        EmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/time_dispute_request/{id}",
     *     summary="Get Time Dispute Request",
     *     description="Get Time Dispute Request.
        Authorization Scope : **view.request**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Time Dispute Request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id
    ) {
        $response = $this->requestService->get($id);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }
        $timeDisputeRequest = json_decode($response->getData());

        $authorized = true;
        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $accountId = !empty($timeDisputeRequest->company_id)
                ? Company::getAccountId($timeDisputeRequest->company_id)
                : null;

            $timeDisputeRequest->account_id = (int)$accountId;

            $user = $request->attributes->get('user');
            $workflows = $timeDisputeRequest->workflow_levels;

            $authorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $timeDisputeRequest
            );

            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this
            ->response
            ->array((array) $timeDisputeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
