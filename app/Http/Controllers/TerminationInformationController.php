<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\Termination\TerminationAuthorizationService;
use App\Termination\TerminationInformationAuditService;
use App\Termination\TerminationInformationRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TerminationInformationController extends Controller
{
    use AuditTrailTrait;

    const RESIGNATION_REASON = 'RESIGNATION';
    const TERMINATION_REASON = 'TERMINATION';
    const RETIREMENT_REASON = 'RETIREMENT';

    const LEAVING_REASONS = [
        self::RESIGNATION_REASON,
        self::TERMINATION_REASON,
        self::RETIREMENT_REASON
    ];

    /**
     * @var \App\Termination\TerminationInformationRequestService
     */
    private $requestService;

    /**
     * @var \App\Termination\TerminationAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditServiceAuditService
     */
    private $auditService;

    public function __construct(
        TerminationInformationRequestService $requestService,
        TerminationAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/termination_informations",
     *     summary="Create a Termination Information of an Employee",
     *     description="Create a Termination Information to Terminate, Resign,
or Retire an Employee
    Authorization Scope : **create.terminations**",
     *     tags={"termination_informations"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="last_date",
     *         in="formData",
     *         description="Expected Last Day of the Employee (YYYY-MM-DD)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date_basis",
     *         in="formData",
     *         description="Start date of the attendance up to Expected Last Day, to be considered
 when calculating the attendance in Final Pay Payroll (YYYY-MM-DD). Start date should be less than the last date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="proposed_release_date",
     *         in="formData",
     *         description="Proposed Release Date, for filtering the employees when creating the
 Final Pay Payroll (YYYY-MM-DD). This should be equal or past the last date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="reason_for_leaving",
     *         in="formData",
     *         description="Select a Reason for Leaving in the list",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\TerminationInformationController::LEAVING_REASONS
     *     ),
     *     @SWG\Parameter(
     *         name="remarks",
     *         in="formData",
     *         description="Additional Information for an Employee's Termination Info",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully Terminated, Resigned, or Retired an Employee": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_projection_job_id": null,
     *                          "final_pay_projected": null,
     *                          "final_pay_calculated": null
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Termination Info already exists": {
     *                  "message": "Termination information already exists for this employee.",
     *                  "status_code": 406
     *              },
     *              "Termination of INACTIVE employees": {
     *                  "message": "Terminating employee not allowed because the employee's
 status is Inactive.",
     *                  "status_code": 406
     *              },
     *              "Missing employee_id": {
     *                  "message": "The employee id field is required.",
     *                  "status_code": 406
     *              },
     *              "Missing company_id": {
     *                  "message": "The company id field is required.",
     *                  "status_code": 406
     *              },
     *              "Missing last_date": {
     *                  "message": "The last date field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid last_date format": {
     *                  "message": "The last date does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Invalid start_date_basis": {
     *                  "message": "Final Pay Start Date Basis should be less than or equal to
 the employee's last day",
     *                  "status_code": 406
     *              },
     *              "Missing start_date_basis": {
     *                  "message": "The start date basis field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid start_date_basis format": {
     *                  "message": "The start date basis does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Invalid proposed_release_date": {
     *                  "message": "Final Pay Proposed Release Date should be greater than or equal to
 the employee's last day",
     *                  "status_code": 406
     *              },
     *              "Missing proposed_release_date": {
     *                  "message": "The proposed release date field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid release_date format": {
     *                  "message": "The proposed release date field does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Missing reason_for_leaving": {
     *                  "message": "The reason for leaving field is required.",
     *                  "status_code": 406
     *              },
     *              "Selected reason_for_leaving is not on the list": {
     *                  "message": "The selected reason for leaving is invalid.",
     *                  "status_code": 406
     *              },
     *              "Invalid or non existing employee_id": {
     *                  "message": "The selected employee id is invalid.",
     *                  "status_code": 406
     *              },
     *              "Invalid or non existing company_id": {
     *                  "message": "The selected company id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function create(Request $request, EmployeeRequestService $employeeRequestService)
    {
        $this->validate($request, ['employee_id' => 'required', 'company_id' => 'required']);

        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($request->get('employee_id'))->getData(),
                true
            );
            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $companyId = $request->get('company_id');
            $teamData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeCreate($teamData, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        if ($response->getStatusCode() !== Response::HTTP_CREATED) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);
        $getResponse = $this->requestService->get($responseData['data']['id']);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $companyId = $request->get('company_id');
            $data = json_decode($getResponse->getData(), true);
            $this->audit($request, $companyId, $data);
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/termination_informations/{id}",
     *     summary="Edit a Termination Information",
     *     description="Edit a Termination Information to Terminate, Resign,
or Retire an Employee
     Authorization Scope : **edit.terminations**",
     *     tags={"termination_informations"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Termination Information ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="last_date",
     *         in="formData",
     *         description="Expected Last Day of the Employee (YYYY-MM-DD)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date_basis",
     *         in="formData",
     *         description="Start date of the attendance up to Expected Last Day, to be considered
 when calculating the attendance in Final Pay Payroll (YYYY-MM-DD). This date should be less than
 the last date.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="proposed_release_date",
     *         in="formData",
     *         description="Proposed Release Date, for filtering the employees when creating the
 Final Pay Payroll (YYYY-MM-DD). This should be equal or past the last date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="reason_for_leaving",
     *         in="formData",
     *         description="Select a Reason for Leaving in the list",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\TerminationInformationController::LEAVING_REASONS
     *     ),
     *     @SWG\Parameter(
     *         name="remarks",
     *         in="formData",
     *         description="Additional Information for an Employee's Termination Info",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully updated the Termination, Resignation,
 or Retirement of an Employee": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_projection_job_id": null,
     *                          "final_pay_projected": null,
     *                          "final_pay_calculated": null
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Termination Information not found": {
     *                  "message": "Termination Information does not exist.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Missing Termination information id": {
     *                  "message": "Termination Information does not exist.",
     *                  "status_code": 406
     *              },
     *              "Wrong Termination information id format": {
     *                  "message": "The id must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Missing last_date": {
     *                  "message": "The last date field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid last_date format": {
     *                  "message": "The last date does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Invalid start_date_basis": {
     *                  "message": "Final Pay Start Date Basis should be less than or equal to
 the employee's last day",
     *                  "status_code": 406
     *              },
     *              "Missing start_date_basis": {
     *                  "message": "The start date basis field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid start_date_basis format": {
     *                  "message": "The start date basis does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Invalid proposed_release_date": {
     *                  "message": "Final Pay Proposed Release Date should be greater than or equal to
 the employee's last day",
     *                  "status_code": 406
     *              },
     *              "Missing proposed_release_date": {
     *                  "message": "The proposed release date field is required.",
     *                  "status_code": 406
     *              },
     *              "Invalid proposed release_date format": {
     *                  "message": "The proposed release date field does not match the format Y-m-d.",
     *                  "status_code": 406
     *              },
     *              "Missing reason_for_leaving": {
     *                  "message": "The reason for leaving field is required.",
     *                  "status_code": 406
     *              },
     *              "Selected reason_for_leaving is not on the list": {
     *                  "message": "The selected reason for leaving is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Unprocessable Entity.",
     *         examples={
     *              "Employee Termination has been processed": {
     *                  "message": "You cannot update this termination details. This employee
 termination information has been processed.",
     *                  "status_code": 422
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function update(Request $request, EmployeeRequestService $employeeRequestService, $terminationId)
    {
        $createdBy = $request->attributes->get('user');
        $terminationResponse = $this->requestService->get((int)$terminationId);
        $terminationData = json_decode($terminationResponse->getData(), true);
        if (empty($terminationData)) {
            $this->notFoundError('Termination Information does not exist.');
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode($employeeRequestService->getEmployee((int) Arr::get(
                $terminationData,
                'data.attributes.employee_id'
            ))->getData(), true);

            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeUpdate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->update($terminationId, $request->all());
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        if ($response->isSuccessful()) {
            // trigger audit trail
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData, $terminationData, true);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/termination_informations/{id}",
     *     summary="Get Termination Information",
     *     description="Get Termination Information Details
     Authorization Scope : **view.terminations**",
     *     tags={"termination_informations"},
     *     security={ {"api_key": {}} },
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Termination Information ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include_final_pay",
     *         description="Include final pay id and payroll id (1 is true, 0 is false)",
     *         in="query",
     *         type="integer",
     *         enum={0, 1}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
    *         examples={
     *              "Successfully fetched a Termination Information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_projection_job_id": null,
     *                          "final_pay_projected": null,
     *                          "final_pay_calculated": null
     *                      }
     *                  }
     *              },
     *              "Successfully fetched a Termination Information with projected final pay information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_id": 1,
     *                          "payroll": null,
     *                          "final_pay_projection_job_id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                          "final_pay_projected": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          },
     *                          "final_pay_computed": null
     *                      }
     *                  }
     *              },
     *              "Successfully fetched a Termination Information with payroll information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "release_date": "2019-11-11",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_id": 1,
     *                          "final_pay_projection_job_id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                          "final_pay_projected": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          },
     *                          "payroll": {
     *                              "id": 1,
     *                              "status": "CLOSED",
     *                              "posting_date": "2019-11-11"
     *                          },
     *                          "final_pay_computed": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Termination Information not found": {
     *                  "message": "Termination Information does not exist.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Termination Information ID": {
     *                  "message": "Termination Information ID should not be empty.",
     *                  "status_code": 406
     *              },
     *              "Termination Information ID is not an integer": {
     *                  "message": "Termination Information ID should be numeric.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function get(Request $request, EmployeeRequestService $employeeRequestService, $id)
    {
        $this->validate($request, ['include_final_pay' => 'sometimes|integer|in:0,1']);

        $terminationResponse = $this->requestService->get($id, $request->all());
        $terminationData = json_decode($terminationResponse->getData(), true);

        if (empty($terminationData)) {
            $this->notFoundError('Termination Information does not exist.');
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode($employeeRequestService->getEmployee((int) Arr::get(
                $terminationData,
                'data.attributes.employee_id'
            ))->getData(), true);

            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $createdBy = $request->attributes->get('user');
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $authorized = $this->authorizationService->authorizeGet($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $terminationResponse;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{id}/termination_informations",
     *     summary="Get Termination Information of an Employee",
     *     description="Get the Termination Information Details of an Employee via the Employee ID
     Authorization Scope : **view.terminations**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include_final_pay",
     *         description="Include final pay id and payroll id (1 is true, 0 is false)",
     *         in="query",
     *         type="integer",
     *         enum={0, 1}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         examples={
     *              "Successfully fetched a Termination Information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_projection_job_id": null,
     *                          "final_pay_projected": null,
     *                          "final_pay_calculated": null
     *                      }
     *                  }
     *              },
     *              "Successfully fetched a Termination Information with projected final pay information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_id": 1,
     *                          "payroll": null,
     *                          "final_pay_projection_job_id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                          "final_pay_projected": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          },
     *                          "final_pay_computed": null
     *                      }
     *                  }
     *              },
     *              "Successfully fetched a Termination Information with payroll information": {
     *                  "data": {
     *                      "id": 2,
     *                      "type": "termination-information",
     *                      "attributes": {
     *                          "employee_id": "271",
     *                          "company_id": "22",
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "proposed_release_date": "2020-01-31",
     *                          "release_date": "2019-11-11",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": "test remarks",
     *                          "final_pay_id": 1,
     *                          "final_pay_projection_job_id": "bb88a596-56d4-491d-97b9-5cf216098d67",
     *                          "final_pay_projected": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          },
     *                          "payroll": {
     *                              "id": 1,
     *                              "status": "CLOSED",
     *                              "posting_date": "2019-11-11"
     *                          },
     *                          "final_pay_computed": {
     *                              "gross_income": 324324.34,
     *                              "non_taxable_income": 23.32,
     *                              "taxable_income": 3212.323,
     *                              "withholding_tax_as_adjusted": 32434.34,
     *                              "net_pay": 3424.343,
     *                              "taxable_incomes": {
     *                                  {"label": "Base Pay", "amount": 25000.00},
     *                                  {"label": "Sales Commission", "amount": 25000.00},
     *                                  {"label": "Performance Bonus", "amount": 25000.00},
     *                                  {"label": "Taxable Adjustment", "amount": 25000.00}
     *                              },
     *                              "non_taxable_incomes": {
     *                                  {"label": "Meal Allowance","amount": 25000.00},
     *                                  {"label": "Transportation Allowance","amount": 25000.00},
     *                                  {"label": "Non Taxable Adjustment", "amount": 25000.00},
     *                                  {"label": "Non Income Adjustment", "amount": 25000.00},
     *                                  {"label": "13th Month Pay", "amount": 25000.00}
     *                              },
     *                              "deductions": {
     *                                  {"label": "SSS Salary Loan", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Loan", "amount": 25000.00},
     *                                  {"label": "Company Loan", "amount": 25000.00},
     *                                  {"label": "SSS Contribution", "amount": 25000.00},
     *                                  {"label": "Pag-IBIG Contribution", "amount": 25000.00},
     *                                  {"label": "Withholding Tax as Adjusted", "amount": -25000.00}
     *                              }
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         examples={
     *              "Termination Information not found": {
     *                  "message": "Termination Information does not exist.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Field validation errors": {
     *                  "message": "The selected employee id is invalid.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function getByEmployeeId(Request $request, $id)
    {
        $this->validate($request, ['include_final_pay' => 'sometimes|integer|in:0,1']);

        $response = app(EmployeeRequestService::class)->getEmployee((int) $id);
        $employee = json_decode($response->getData(), true);

        $terminationResponse = $this->requestService->getByEmployeeId($id, $request->all());
        $terminationData = json_decode($terminationResponse->getData(), true);

        if ($this->isAuthzEnabled($request)) {
            $dataScope = $this->getAuthzDataScope($request);

            $isAuthorized =
                $dataScope->isAllAuthorized(
                    [
                        AuthzDataScope::SCOPE_COMPANY => data_get($employee, 'company_id'),
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employee, 'department_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employee, 'location_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group.id'),
                        AuthzDataScope::SCOPE_POSITION => data_get($employee, 'position_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employee, 'team_id'),
                    ]
                );
        } else {
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $data,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (empty($terminationData)) {
            $this->notFoundError('Termination Information does not exist.');
        }

        return $terminationResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/termination_informations/{id}",
     *     summary="Cancel a Termination Information",
     *     description="Cancel a Termination Information of an Employee.
    Authorization Scope : **delete.terminations**",
     *     tags={"termination_informations"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     security={ {"api_key": {}} },
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Termination Information ID of an Employee",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successfully cancelled Employee's Termination. NO CONTENT.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Termination Information ID": {
     *                  "message": "The id must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Invalid Termination Information ID": {
     *                  "message": "Termination Information does not exist.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Invalid request.",
     *         examples={
     *              "Invalid Termination Information ID": {
     *                  "message": "You cannot revoke termination details of this employee.
 EMPLOYEE NAME was already included in Final Pay Payroll",
     *                  "status_code": 422
     *              },
     *              "No Final Pay related to Termination Information ID": {
     *                  "message": "Final Pay does not exist.",
     *                  "status_code": 422
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function delete(Request $request, EmployeeRequestService $employeeRequestService, $terminationId)
    {
        $createdBy = $request->attributes->get('user');
        $terminationResponse = $this->requestService->get((int) $terminationId);
        $terminationData = json_decode($terminationResponse->getData(), true);
        if (empty($terminationData)) {
            $this->notFoundError('Termination Information does not exist.');
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode($employeeRequestService->getEmployee((int) Arr::get(
                $terminationData,
                'data.attributes.employee_id'
            ))->getData(), true);

            $authorized = $employee !== null && $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'data.company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'data.department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'data.position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'data.location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'data.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'data.payroll_group.id'),
            ]);
        } else {
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeDelete($data, $createdBy);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->delete($terminationId);
        if ($response->getStatusCode() !== Response::HTTP_NO_CONTENT) {
            return $response;
        }

        if ($response->isSuccessful()) {
            // trigger audit trail
            $companyId = (int) array_get($terminationData, 'data.attributes.company_id');
            $this->audit($request, $companyId, [], $terminationData);
        }

        return $response;
    }
}
