<?php

namespace App\Http\Controllers;

use App\Facades\Company;
use App\WorkflowEntitlement\WorkflowEntitlementAuditService;
use App\WorkflowEntitlement\WorkflowEntitlementAuthorizationService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\WorkflowEntitlement\WorkflowEntitlementRequestService;
use App\Employee\EmployeeRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\WorkflowEntitlement\WorkflowEntitlementUploadTask;
use Aws\S3\Exception\S3Exception;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;
use App\WorkflowEntitlement\WorkflowEntitlementUploadTaskException;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WorkflowEntitlementController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\WorkflowEntitlement\WorkflowEntitlementRequestService
     */
    private $requestService;

    /**
     * @var \App\WorkflowEntitlement\WorkflowEntitlementAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        CsvValidator $csvValidator,
        WorkflowEntitlementRequestService $requestService,
        WorkflowEntitlementAuthorizationService $authorizationService
    ) {
        $this->csvValidator = $csvValidator;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/workflow_entitlement/{id}",
     *     summary="Get Workflow Entitlement.",
     *     description="Get WorkflowEntitlement Details
    Authorization Scope : **view.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Workflow Entitlement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $workflowEntitlementData = json_decode($response->getData());
        $workflowEntitlementData->account_id = Company::getAccountId($workflowEntitlementData->company_id);
        if (!$this->authorizationService->authorizeGet($workflowEntitlementData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/workflow_entitlements",
     *     summary="Get all Workflow Entitlements.",
     *     description="Get all Workflow Entitlements within company.
    Authorization Scope : **view.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1 . Default is 1",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results to return per page. Default is 10",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID), workflow and request type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="formData",
     *         description="Attribute to sort by.
 Possible values: employee_name, employee_id, workflow_name, request_type",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="formData",
     *         description="Sort order: asc, desc.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Employee IDs to filter by",
     *         type="array",
     *         items={ "type": "integer" }
     *     ),
     *     @SWG\Parameter(
     *         name="request_types_ids[]",
     *         in="formData",
     *         description="Request Type IDs to filter by",
     *         type="array",
     *         items={ "type": "integer" }
     *     ),
     *     @SWG\Parameter(
     *         name="workflows_ids[]",
     *         in="formData",
     *         description="Workflow IDs to filter by",
     *         type="array",
     *         items={ "type": "integer" }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyWorkflowEntitlements($id, Request $request)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $workflowEntitlement = (object)[
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyWorkflowEntitlements(
                $workflowEntitlement,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyWorkflowEntitlements(
            $id,
            $request->all(),
            $authzDataScope
        );
        $workflowEntitlementData = json_decode($response->getData())->data;

        if (empty($workflowEntitlementData)) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{id}/workflow_entitlements",
     *     summary="Get all Employee Workflow Entitlements.",
     *     description="Get all Employee Workflow Entitlements by given employee id.
    Authorization Scope : **view.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeWorkflowEntitlements(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $employeeId
    ) {
        $response = $this->requestService->getEmployeeWorkflowEntitlements($employeeId);
        $workflowEntitlementData = json_decode($response->getData())->data;

        if (empty($workflowEntitlementData)) {
            return $response;
        }

        $companyId = collect($workflowEntitlementData)->first()->company_id;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $workflowEntitlement = (object)[
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyWorkflowEntitlements(
                $workflowEntitlement,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/workflow_entitlement/bulk_create_or_update",
     *     summary="Create or Update Workflow Entitlements.",
     *     description="Create or update Workflow entitlements.
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newWorkflowEntitlement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newWorkflowEntitlement",
     *     required={"company_id", "workflow_request_type_pairs", "affected_employees"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="workflow_request_type_pairs",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/workflow_request_type_pairs"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_leave_entitlement"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="workflow_request_type_pairs",
     *     required={"workflow_id", "request_type"},
     *     @SWG\Property(
     *         property="workflow_id",
     *         type="integer",
     *         description="Workflow ID"
     *     ),
     *     @SWG\Property(
     *         property="request_type",
     *         type="string",
     *         enum={"Time record", "Shift change", "Leave", "Overtime", "Undertime", "Loan"},
     *         description="Request Type. Possible values: Time record, Shift change,
 Leave, Overtime, Undertime, Loan"
     *     )
     * )
     */
    public function bulkCreateOrUpdate(Request $request)
    {
        // authorize
        $userData = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['user_id'] = $userData['user_id'];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreateOrUpdate($inputs, $authzDataScope);
        $responseData = json_decode($response->getData(), true);

        // log created or updated workflow entitlements
        $this->logWorkflowEntitlementActionTypes($request, $responseData);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employee/{id}/workflow_entitlement/create_or_update",
     *     summary="Create or Update Employee Workflow Entitlements.",
     *     description="Create or update Employee Workflow entitlements.
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/new_workflow_entitlements"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="new_workflow_entitlements",
     *     required={"company_id", "workflow_entitlements"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="workflow_entitlements",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/workflow_entitlements"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="workflow_entitlements",
     *     required={"workflow_id", "request_type"},
     *     @SWG\Property(
     *         property="workflow_id",
     *         type="integer",
     *         description="Workflow ID"
     *     ),
     *     @SWG\Property(
     *         property="request_type",
     *         type="string",
     *         enum={"Time record", "Shift change", "Leave", "Overtime", "Undertime", "Loan"},
     *         description="Request Type. Possible values: Time record, Shift change,
 Leave, Overtime, Undertime, Loan"
     *     )
     * )
     */
    public function createOrUpdateEmployeeWorkflowEntitlements(
        $id,
        Request $request,
        EmployeeRequestService $employeeRequestService
    ) {
        $userData = $request->attributes->get('user');
        $inputs = $request->all();

        $employeeResponse = $employeeRequestService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());
        $companyId = $employee->company_id;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeData = json_decode($employeeResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['company_id'] = $companyId;
        $inputs['user_id'] = $userData['user_id'];

        $response = $this->requestService->createOrUpdateEmployeeWorkflowEntitlements($id, $inputs);
        $responseData = json_decode($response->getData(), true);

        // log created, updated or deleted employee workflow entitlements
        $this->logWorkflowEntitlementActionTypes(
            $request,
            $responseData,
            WorkflowEntitlementAuditService::ACTIONS_CREATE_OR_UPDATE_OR_DELETE
        );

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/workflow_entitlement/{id}",
     *     summary="Update Workflow Entitlement.",
     *     description="Update Workflow entitlement.
    Authorization Scope : **edit.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Workflow Entitlement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="workflow_id",
     *         in="formData",
     *         description="Workflow ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="request_type",
     *         in="formData",
     *         description="Request Type. Possible values: Time record, Shift change,
 Leave, Overtime, Undertime, Loan",
     *         required=true,
     *         type="string",
     *         enum={"Time record", "Shift change", "Leave", "Overtime", "Undertime", "Loan"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(int $id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['user_id'] = $updatedBy['user_id'];

        $response = $this->requestService->get($id);
        $oldWorkflowEntitlementData = json_decode($response->getData());
        $oldWorkflowEntitlementData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee(
                $oldWorkflowEntitlementData->employee_id
            );
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldWorkflowEntitlementData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldWorkflowEntitlementData = json_decode($response->getData(), true);
        $newWorkflowEntitlementData = json_decode($getResponse->getData(), true);

        // trigger audit trail
        $this->audit($request, $inputs['company_id'], $newWorkflowEntitlementData, $oldWorkflowEntitlementData);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/workflow_entitlement/bulk_delete",
     *     summary="Delete multiple Workflow Entitlements.",
     *     description="Delete multiple Workflow Entitlements
    Authorization Scope : **delete.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="workflow_entitlements_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Workflow Entitlements Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($workflowEntitlementData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs, $authzDataScope);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('workflow_entitlements_ids', []) as $id) {
                $oldData = [
                    'id' => $id,
                    'company_id' => $request->get('company_id')
                ];
                $this->audit($request, $inputs['company_id'], [], $oldData);
            }
        }

        return $response;
    }

    /**
     * Logs workflow entitlement action types.
     *
     * @param array $responseData
     * @param array $userData
     * @param array $actions default values ['created', 'updated']
     */
    private function logWorkflowEntitlementActionTypes(
        Request $request,
        array $responseData,
        $actions = ['created', 'updated']
    ) {
        $inputs = $request->all();
        foreach (array_only($responseData, $actions) as $key => $workflowEntitlements) {
            foreach ($workflowEntitlements as $workflowEntitlement) {
                $defaultActions = [
                    'created' => 'create',
                    'updated' => 'update',
                    'deleted' => 'delete'
                ];
                $action = $defaultActions[$key];

                $details = $action !== 'delete'
                    ? ['new' => $workflowEntitlement]
                    : ['old' => $workflowEntitlement];

                $id = $workflowEntitlement['id'];

                if ($key === 'updated') {
                    $details['old'] = array_first($responseData['before_updated'], function ($item) use ($id) {
                        return $item['id'] === $id;
                    });
                }

                // trigger audit trail
                $newData = isset($details['new']) ? $details['new'] : [];
                $oldData = isset($details['old']) ? $details['old'] : [];
                $this->audit($request, $inputs['company_id'], $newData, $oldData);
            }
        }
    }

    /**
     * @SWG\Post(
     *     path="/workflow_entitlement/upload",
     *     summary="Upload Workflow Entitlements",
     *     description="Uploads Workflow Entitlements
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "workflow_entitlement_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $workflowEntitlementData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(WorkflowEntitlementUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => WorkflowEntitlementUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $this->isAuthzEnabled($request)
                    ? $this->getAuthzDataScope($request)->getDataScope()
                    : null
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            Amqp::publish(config('queues.workflow_entitlement_validation_queue'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/workflow_entitlement/upload/status",
     *     summary="Get Job Status",
     *     description="Get Workflow Entitlements Upload Status
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\WorkflowEntitlement\WorkflowEntitlementUploadTask::PROCESS_VALIDATION,
     *              App\WorkflowEntitlement\WorkflowEntitlementUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Name is invalid"
     *                          },
     *                          "4": {
     *                              "Email is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $workflowEntitlementData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(WorkflowEntitlementUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (WorkflowEntitlementUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, WorkflowEntitlementUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(
                    ',',
                    WorkflowEntitlementUploadTask::VALID_PROCESSES
                )
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === WorkflowEntitlementUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === WorkflowEntitlementUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }

        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/workflow_entitlement/upload/preview",
     *     summary="Get Workflow Entitlements Preview",
     *     description="Get Workflow Entitlements Upload Preview
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $workflowEntitlementData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(WorkflowEntitlementUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (WorkflowEntitlementUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        return $this->requestService->getUploadPreview($jobId);
    }

    /**
     * @SWG\Post(
     *     path="/workflow_entitlement/upload/save",
     *     summary="Save uploaded workflow entitlements",
     *     description="Saves Workflow Entitlements from Previously Uploaded CSV
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "workflow_entitlement_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Workflow entitlements needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(WorkflowEntitlementUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(WorkflowEntitlementUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (WorkflowEntitlementUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            'user_id' => $uploadTask->getUserId(),
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.workflow_entitlement_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/workflow_entitlement/upload/has_pending_requests",
     *     summary="Check if uploaded workflow entitlements reset requests",
     *     description="Check if Workflow Entitlements from Previously Uploaded CSV reset requests
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="has_pending_requests",
     *                 type="boolean",
     *                 description="Whether there are pending requests to be reset"
     *             )
     *         ),
     *         examples={
     *              "application/json": {
     *                  "has_pending_requests": false
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Workflow entitlements needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadHasPendingRequests(Request $request)
    {
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // check company exists (will throw exception if company doesn't exist)
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $createdBy = $request->attributes->get('user');
            $authorizationData = (object)[
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($authorizationData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkUploadHasPendingRequests($request->all());
    }

    /**
     * @SWG\Post(
     *     path="/workflow_entitlement/has_pending_requests",
     *     summary="Check if employees have any requests for workflow change",
     *     description="Check if there are any pending requests for affected employees on workflow
     *                  that is about to be changed.
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newWorkflowEntitlement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function hasPendingRequestsOfAnyType(Request $request)
    {
        // authorize
        $userData = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['user_id'] = $userData['user_id'];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->checkAffectedEmployeesHavePendingRequests($inputs, $authzDataScope);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employee/{id}/workflow_entitlement/has_pending_requests",
     *     summary="Check if employee has any requests for workflow change",
     *     description="Check if there are any pending requests for affected employee on workflow
     *                  that is about to be changed.
    Authorization Scope : **create.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/new_workflow_entitlements"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function employeeHasPendingRequests(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $employeeId
    ) {
        $userData = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['user_id'] = $userData['user_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $workflowEntitlementData,
                $userData
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->checkEmployeeHasPendingRequests($employeeId, $inputs);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/workflow_entitlement/{id}/has_pending_requests",
     *     summary="Assign Workflow inline update has pending requests",
     *     description="Check if inline edit of assigned workflow has any pending requests.
    Authorization Scope : **edit.workflow_entitlement**",
     *     tags={"workflow_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Workflow Entitlement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="workflow_id",
     *         in="formData",
     *         description="Workflow ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="request_type",
     *         in="formData",
     *         description="Request Type. Possible values: Time record, Shift change,
 Leave, Overtime, Undertime, Loan",
     *         required=true,
     *         type="string",
     *         enum={"Time record", "Shift change", "Leave", "Overtime", "Undertime", "Loan"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function inlineUpdateHasPendingRequests(Request $request, $id)
    {
        $userData = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['user_id'] = $userData['user_id'];

        $this->validate($request, [
            'company_id' => 'required|integer|min:1'
        ]);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $workflowEntitlementData = (object)[
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($workflowEntitlementData, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->checkAssignWorkflowInlineUpdateHasPendingRequests(
            $id,
            $inputs
        );

        return $response;
    }
}
