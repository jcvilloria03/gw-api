<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\Schedule\ScheduleRequestService;
use App\Http\Controllers\EssBaseController;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\DefaultSchedule\EssDefaultScheduleAuthorizationService;
use App\DefaultSchedule\DefaultScheduleRequestService;

class EssScheduleController extends EssBaseController
{
    /**
     * @var \App\Schedule\ScheduleRequestService
     */
    protected $scheduleRequestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $employeeRequestAuthorizationService;

    /**
     * @var \App\DefaultSchedule\DefaultScheduleRequestService
     */
    private $defaultScheduleRequestService;

    public function __construct(
        ScheduleRequestService $scheduleRequestService,
        EssEmployeeRequestAuthorizationService $employeeRequestAuthorizationService,
        DefaultScheduleRequestService $defaultScheduleRequestService
    ) {
        $this->scheduleRequestService = $scheduleRequestService;
        $this->employeeRequestAuthorizationService = $employeeRequestAuthorizationService;
        $this->defaultScheduleRequestService = $defaultScheduleRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/schedules",
     *     summary="Get employee schedules",
     *     description="Get all schedules where employee with given ID is entitled.
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeSchedules(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->employeeRequestAuthorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->scheduleRequestService->getEmployeeSchedules($user['employee_id'], $user['employee_company_id']);
    }

    /**
     * @SWG\Get(
     *     path="/ess/company/{id}/default_schedule",
     *     summary="Get default schedules",
     *     description="Get default schedules details
Authorization Scope : **ess.view.default_schedule**",
     *     tags={"default_schedule"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompaniesDefaultSchedule(
        $id,
        Request $request,
        EssDefaultScheduleAuthorizationService $essAuthorizationService
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized =
                $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized =
                $essAuthorizationService->authorizeView($user['user_id']) &&
                $id != $user['employee_company_id'];
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->defaultScheduleRequestService->index($id);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/current-schedules",
     *     summary="Get employee current schedules",
     *     description="Call endpoint to get employee current schedule with the given date
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeCurrentSchedules(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (
            !$this->isAuthzEnabled($request)
            && !$this->employeeRequestAuthorizationService->authorizeCreate($user)
        ) {
            $this->response()->errorUnauthorized();
        }

        return $this->scheduleRequestService->getEmployeeCurrentShiftSchedules(
            $user['employee_id'],
            $user['employee_company_id'],
            $request['date']
        );
    }
}
