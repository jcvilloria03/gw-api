<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\BonusType\BonusTypeAuthorizationService;
use App\BonusType\PhilippineBonusTypeRequestService;
use App\Facades\Company;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class PhilippineBonusTypeController extends Controller
{
    /**
     * @var \App\BonusType\PhilippineBonusTypeRequestService
     */
    protected $requestService;

    /**
     * @var \App\BonusType\BonusTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncomeType\OtherIncomeTypeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineBonusTypeRequestService $requestService,
        BonusTypeAuthorizationService $authorizationService,
        OtherIncomeTypeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/bonus_type/bulk_create",
     *     summary="Create Multiple Bonus Types",
     *     description="Create multiple bonus types,
Authorization Scope : **create.bonus_types**",
     *     tags={"bonus_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="bonus_types",
     *         in="body",
     *         description="Create multiple bonus types",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bonusTypesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      {
     *                          "id": 1,
     *                          "name": "Bonus Type",
     *                          "company_id": 1,
     *                          "fully_taxable": true,
     *                          "max_non_taxable": null,
     *                          "basis": "FIXED",
     *                          "frequency": "ONE_TIME",
     *                          "type_name": "App\Model\PhilippineBonusType",
     *                          "deleted_at": null
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="bonusTypesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/bonusTypesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     *
     * @SWG\Definition(
     *     definition="bonusTypesRequestItem",
     *     required={"name", "basis", "frequency", "fully_taxable"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Name of custom bonus type",
     *         example="Good bonus"
     *     ),
     *     @SWG\Property(
     *         property="basis",
     *         type="string",
     *         enum=App\OtherIncomeType\OtherIncomeTypeService::BASIS,
     *         description="Bonus type basis",
     *         example="FIXED"
     *     ),
     *     @SWG\Property(
     *         property="frequency",
     *         type="string",
     *         enum=App\BonusType\PhilippineBonusTypeService::FREQUENCIES,
     *         description="Bonus type frequency",
     *         example="ONE_TIME"
     *     ),
     *     @SWG\Property(
     *         property="fully_taxable",
     *         type="boolean",
     *         enum={1,0},
     *         description="Is bonus with this bonus type fully taxable",
     *         example="1"
     *     ),
     *      @SWG\Property(
     *         property="max_non_taxable",
     *         type="number",
     *         description="If not fully taxable specify max non taxable limit amount",
     *         example="100"
     *     )
     * )
     */
    public function bulkCreate(Request $request, $id)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];

            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($id, $inputs);
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $otherIncomeTypes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if ($response->isSuccessful()) {
            $this->auditService->mapAndQueueAuditItems(
                $otherIncomeTypes,
                OtherIncomeTypeAuditService::ACTION_CREATE,
                $createdBy
            );
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/bonus_type/is_name_available",
     *     summary="Is philippine bonus type name available",
     *     description="Check availability of philippine bonus type name with the given company,
Authorization Scope : **create.bonus_types**",
     *     tags={"bonus_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Type name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="other_income_type_id",
     *         in="formData",
     *         description="Other Income Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="object", @SWG\Property(property="available", type="boolean", example=true))
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/InvalidRequestResponse",
     *         examples={
     *              "Id required": {"message": "The id field is required.", "status_code": 406},
     *              "Other income type id must be an integer": {
     *                  "message": "The other income type id must be an integer.",
     *                  "status_code": 406
     *              },
     *              "Name is required": {"message": "The name field is required.", "status_code": 406},
     *         }
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $createdBy = $request->attributes->get('user');
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id,
            ];

            $authorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->isNameAvailable($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/bonus_type/{id}",
     *     summary="Update the bonus type",
     *     description="Update the bonus type
Authorization Scope : **edit.bonus_types**",
     *     tags={"bonus_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         type="string",
     *         required=true,
     *         description="Name of custom bonus type"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="basis",
     *         type="string",
     *         enum=App\OtherIncomeType\OtherIncomeTypeService::BASIS,
     *         required=true,
     *         description="Bonus type basis"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="frequency",
     *         type="string",
     *         enum=App\BonusType\PhilippineBonusTypeService::FREQUENCIES,
     *         required=true,
     *         description="Bonus type frequency"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="fully_taxable",
     *         type="integer",
     *         enum={1,0},
     *         required=true,
     *         description="Is bonus with this bonus type fully taxable"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="max_non_taxable",
     *         type="number",
     *         description="If not fully taxable specify max non taxable limit amount"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Default": {
     *                  "data": {
     *                      "id": 1,
     *                      "name": "Bonus Test",
     *                      "company_id": 1,
     *                      "fully_taxable": true,
     *                      "max_non_taxable": null,
     *                      "basis": "FIXED",
     *                      "frequency": "ONE_TIME",
     *                      "type_name": "App\\Model\\PhilippineBonusType",
     *                      "deleted_at": null
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *         examples={
     *              "Default": {"message": "Other Income Type not found.", "status_code": 404}
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *              "Default": {
     *                  "id": "The id field is required.",
     *                  "name": "The name field is required.",
     *                  "fully_taxable": "The fully taxable field is required.",
     *                  "basis": "The basis field is required.",
     *                  "frequency": "The frequency field is required."
     *              }
     *         }
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeTypeRequestService $otherIncomeTypeRequestService,
        $id
    ) {
        $bonusTypeResponse = $otherIncomeTypeRequestService->get($id);
        if (!$bonusTypeResponse->isSuccessful()) {
            return $bonusTypeResponse;
        }
        $bonusTypeData = json_decode($bonusTypeResponse->getData(), true);
        $companyId = array_get($bonusTypeData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];
            $authorized = $this->authorizationService->authorizeUpdate($authData, $updatedBy);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $request->all());
        if ($response->isSuccessful()) {
            $this->auditService->mapAndQueueAuditItems(
                [json_decode($response->getData(), true)],
                OtherIncomeTypeAuditService::ACTION_UPDATE,
                $updatedBy,
                [$bonusTypeData]
            );
        }
        return $response;
    }
}
