<?php

namespace App\Http\Controllers;

use App\Authorization\AuthorizationService;
use Illuminate\Http\Request;
use App\Transformer\TaskScopesTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager as FractalManager;

class AuthorizationController extends Controller
{

    /*
     * App\Role\RoleAuthorizationService
     */
    protected $authorizationService;

    /*
     * App\Transformer\TaskScopesTransformer
     */
    private $transformer;

    /*
     * League\Fractal\Manager
     */
    private $fractal;

    public function __construct(
        AuthorizationService $authorizationService,
        TaskScopesTransformer $transformer,
        FractalManager $fractal
    ) {
        $this->authorizationService = $authorizationService;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
    }

    /**
     * @SWG\Post(
     *     path="/user/permissions",
     *     summary="Get List of Scopes",
     *     description="Get List of Scopes for Given tasks, for the Given user",
     *     tags={"user"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="tasks",
     *         in="formData",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="csv"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getUserPermissions(Request $request)
    {
        // authorize
        $userId = $request->attributes->get('user')['user_id'];
        $tasks = explode(",", $request->input('tasks'));

        // fetch user permissions per task
        $this->authorizationService->buildUserPermissions($userId);
        $taskScopes = $this->authorizationService->getMultipleTaskScopes($tasks);
        $result = new Collection($taskScopes, $this->transformer);
        return $this->fractal->createData($result)->toArray();
    }
}
