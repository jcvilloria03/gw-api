<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Role\UserRoleService;
use App\Authn\AuthnService;
use App\User\UserRequestService;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\Employee\EssEmployeeAuthorizationService;

class EssUserController extends EssBaseController
{
    /**
     * @var \App\Employee\EssEmployeeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Authn\authnService
     */
    private $authnService;

    /**
     * @var \App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $requestService;

    public function __construct(
        UserRequestService $requestService,
        EssEmployeeAuthorizationService $authorizationService,
        UserRoleService $userRoleService,
        AuthnService $authnService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->userRoleService = $userRoleService;
        $this->authnService = $authnService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/user/{id}",
     *     summary="Get",
     *     description="Get user Details

Authorization Scope : **ess.view.user**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="User Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        // call microservice
        $headerUserId = $request->attributes->get('user')['user_id'];

        if ($id != $headerUserId) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->get($id);
        $userData = json_decode($response->getData());

        if (!$this->isAuthzEnabled($request)) {
            // authorize
            $viewer = $this->getFirstEmployeeUser($request);
            $userResponse = $this->requestService->get($viewer['user_id']);
            $userResponseData = json_decode($userResponse->getData());
            // TODO support array of company ids, because user can have more then one company
            // for now, it will take only first company id from array
            $authData = (object) [
                'account_id' => $userResponseData->account_id,
                'company_id' => $userResponseData->companies[0]->id,
            ];
            if (!$this->authorizationService->authorizeGet($viewer['user_id'], $authData)) {
                $this->response()->errorUnauthorized();
            }
        }

        $userData->companies = collect($userData->companies)->map(function ($company) use ($userData) {
            return array_merge(collect($company)->toArray(), [
                'assigned_seats' => $userData->product_seats,
                'assigned_roles' => $this->userRoleService->getUserRolesInCompany(
                    $userData->id,
                    $company->id,
                    $userData->account->id
                ),
                'employee_id' => $company->employee_id,
                'employee_uid' => $company->employee_uid
            ]);
        });

        // add user roles (for now assume 1 role per user)
        $userRoles = $this->userRoleService->getUserRoles($userData->id);
        $userData->role = $userRoles->map(function ($userRole) {
            return $userRole->role;
        })
        ->all();

        $userData->status = $this->authnService->getUser($userData->id)->status;

        $response->setData($userData);

        return $response;
    }

}
