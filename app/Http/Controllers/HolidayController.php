<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\Holiday\HolidayAuditService;
use App\Holiday\HolidayAuthorizationService;
use Illuminate\Http\Request;
use App\Holiday\HolidayRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class HolidayController extends Controller
{
    /**
     * @var \App\Holiday\HolidayRequestService
     */
    private $requestService;

    /**
     * @var \App\Holiday\HolidayAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        HolidayRequestService $requestService,
        HolidayAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/holiday/{id}",
     *     summary="Get Holiday",
     *     description="Get Holiday Details
Authorization Scope : **view.holiday**",
     *     tags={"holiday"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Holiday ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, int $id)
    {
        $response = $this->requestService->get($id);
        $holidayData = json_decode($response->getData());
        $holidayData->account_id = Company::getAccountId($holidayData->company_id);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $holidayData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet(
                $holidayData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/holidays",
     *     summary="Get all Holidays",
     *     description="Get all Holidays within company.
Authorization Scope : **view.holiday**",
     *     tags={"holiday"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyHolidays(Request $request, int $companyId)
    {
        $params = $request->only([
            'types',
            'years',
            'dates',
            'month',
            'sort_order',
            'sort_by'
        ]);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $holiday = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyHolidays(
                $holiday,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyHolidays($companyId, $params);
    }

    /**
     * @SWG\Post(
     *     path="/holiday",
     *     summary="Create Holiday",
     *     description="Create Holiday
Authorization Scope : **create.holiday**",
     *     tags={"holiday"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newHoliday"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="newHoliday",
     *     required={"company_id", "name", "type", "date", "recurring", "entitled_locations"},
     *     @SWG\Property(property="company_id", type="integer", description="Company ID"),
     *     @SWG\Property(property="name", type="string", description="Holiday name"),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="Regular",
     *         description="Possible values: Regular, Special"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Date or a holiday that occurs in just 1 day in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="recurring",
     *         type="boolean",
     *         description="Holiday repeats every year"
     *     ),
     *     @SWG\Property(
     *         property="repeat_until",
     *         type="string",
     *         default="2018-12-29",
     *         description="End date or the holiday if more than 1 day in a holiday record in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="entitled_locations",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/entitled_locations_holidays"),
     *         collectionFormat="multi"
     *     )
     *  ),
     * @SWG\Definition(
     *     definition="entitled_locations_holidays",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *         description="Location ID"
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $holidayData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($holidayData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $holidayResponse = $this->requestService->get($responseData['id']);
        $holidayData = json_decode($holidayResponse->getData(), true);

        $details = [
            'new' => $holidayData,
        ];
        $item = new AuditCacheItem(
            HolidayAuditService::class,
            HolidayAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/holiday/{id}",
     *     summary="Update Holiday",
     *     description="Update Holiday
Authorization Scope : **edit.holiday**",
     *     tags={"holiday"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/existingHoliday"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="existingHoliday",
     *     required={"company_id", "name", "type", "date", "recurring", "entitled_locations"},
     *     @SWG\Property(property="company_id", type="integer", description="Company ID"),
     *     @SWG\Property(property="name", type="string", description="Holiday name"),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="Regular",
     *         description="Possible values: Regular, Special"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Date or a holiday that occurs in just 1 day in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="recurring",
     *         type="boolean",
     *         description="Holiday repeats every year"
     *     ),
     *     @SWG\Property(
     *         property="repeat_until",
     *         type="string",
     *         default="2018-12-29",
     *         description="End date or the holiday if more than 1 day in a holiday record in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="entitled_locations",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/entitled_locations_holidays"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function update(Request $request, int $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldHolidayData = json_decode($response->getData());
        $oldHolidayData->account_id = Company::getAccountId($inputs['company_id']);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldHolidayData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldHolidayData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldHolidayData = json_decode($response->getData(), true);
        $newHolidayData = json_decode($getResponse->getData(), true);

        $details = [
            'old' => $oldHolidayData,
            'new' => $newHolidayData,
        ];

        $item = new AuditCacheItem(
            HolidayAuditService::class,
            HolidayAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );

        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
      * @SWG\Delete(
      *     path="/holiday/bulk_delete",
      *     summary="Delete multiple Holidays",
      *     description="Delete multiple Holidays
Authorization Scope : **delete.holiday**",
      *     tags={"holiday"},
      *     consumes={"application/x-www-form-urlencoded"},
      *     produces={"application/json"},
      *     @SWG\Parameter(
      *         type="string",
      *         name="Authorization",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         type="string",
      *         name="X-Authz-Entities",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         name="company_id",
      *         in="formData",
      *         description="Company ID",
      *         required=true,
      *         type="integer"
      *     ),
      *     @SWG\Parameter(
      *         name="holidays_ids[]",
      *         type="array",
      *         in="formData",
      *         description="Holiday Ids",
      *         required=true,
      *         @SWG\Items(type="integer"),
      *         collectionFormat="multi"
      *     ),
      *     @SWG\Response(
      *         response=204,
      *         description="Successful operation",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
      *         description="Company not found.",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
      *         description="Invalid request",
      *     )
      * )
      */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $inputs['company_id']
            );
        } else {
            $holidayData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($holidayData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // prepare holiday data for holiday trigger before delete
        $response = $this->requestService->getCompanyHolidays($inputs['company_id']);
        $holidayData = json_decode($response->getData(), true);

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('holidays_ids', []) as $id) {
                $item = new AuditCacheItem(
                    HolidayAuditService::class,
                    HolidayAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $request->get('company_id')
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/holiday/is_name_available",
     *     summary="Is Holiday name available",
     *     description="Check availability of Holiday name with the given company",
     *     tags={"holiday"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Holiday Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="holiday_id",
     *         in="formData",
     *         description="Holiday Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $user = $request->attributes->get('user');

            $holidayData = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $holidayData,
                $user
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($companyId, $request->all());
    }

    /**
     * @SWG\Post(
     *     path="/holiday/check_in_use",
     *     summary="Are Holidays in use",
     *     description="Check are Holidays in use
Authorization Scope : **view.holiday**",
     *     tags={"holiday"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="holiday_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Holidays Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $holidayData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $request->get('company_id'));
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanyHolidays(
                $holidayData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }
}
