<?php

namespace App\Http\Controllers;

use App\AffectedEntity\AffectedEntityRequestService;
use App\Authz\AuthzDataScope;
use App\Bonus\BonusAuthorizationService;
use App\Bonus\PhilippineBonusRequestService;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\OtherIncome\OtherIncomeAuditService;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PhilippineBonusController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Bonus\PhilippineBonusRequestService
     */
    protected $requestService;

    /**
     * @var \App\Bonus\BonusAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncome\OtherIncomeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineBonusRequestService $requestService,
        BonusAuthorizationService $authorizationService,
        OtherIncomeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/bonus/bulk_create",
     *     summary="Assign Multiple Bonuses",
     *     description="Assign multiple bonuses,
Authorization Scope : **create.bonuses**",
     *     tags={"bonus"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="bonuses",
     *         in="body",
     *         description="Create multiple bonuses",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bonusesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="bonusesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/bonusesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     * @SWG\Definition(
     *     definition="bonusesRequestItem",
     *     required={"type_id","recipients", "auto_assign", "basis",
     *     "release_details"},
     *     @SWG\Property(
     *         property="type_id",
     *         type="integer",
     *         description="Bonus type id"
     *     ),
     *     @SWG\Property(
     *         property="recipients",
     *         type="object",
     *             @SWG\Property(
     *              property="employees",
     *              type="array",
     *              items={"type"="integer", "example"="1"}
     *             ),
     *             @SWG\Property(
     *              property="payroll_groups",
     *              type="array",
     *              items={"type"="integer", "example"="1"}
     *             ),
     *             @SWG\Property(
     *              property="departments",
     *              type="array",
     *              items={"type"="integer", "example"="1"}
     *             ),
     *     ),
     *     @SWG\Property(
     *         property="auto_assign",
     *         type="boolean",
     *         example="1",
     *         description="Is bonus auto signed to newly added users to payroll group or department"
     *     ),
     *     @SWG\Property(
     *         property="basis",
     *         type="string",
     *         description="Basis of bonus",
     *         example="CURRENT_BASIC_SALARY"
     *     ),
     *     @SWG\Property(
     *         property="percentage",
     *         type="integer",
     *         description="Percentage of bonus",
     *         example="10"
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number",
     *         description="Amount of bonus",
     *         example="100"
     *     ),
     *     @SWG\Property(
     *         property="release_details",
     *         type="array",
     *         items={"type"="object",
     *                  "properties"={
     *                      "id"={"type"="integer", "example"=1},
     *                      "date"={"type"="string", "example"="2019-02-22"},
     *                      "disburse_through_special_pay_run"={"type"="boolean", "example"=true},
     *                      "prorate_based_on_tenure"={"type"="boolean", "example"=true},
     *                      "coverage_from"={"type"="string", "example"="2019-01-22"},
     *                      "coverage_to"={"type"="string", "example"="2019-02-20"},
     *                      "status"={"type"="boolean", "example"=true}
     *                  }
     *              }
     *     ),
     * )
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function bulkCreate(
        Request $request,
        AffectedEntityRequestService $entityService,
        EmployeeRequestService $employeeService,
        $companyId
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;
        $errorMesssage = 'Unauthorized';

        if ($this->isAuthzEnabled($request)) {
            $authorizedCompany = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            $affectedEntities = $this->getAffectedEntities($request);
            $entityResponse = $entityService->getAllAffecteEntities($companyId, $affectedEntities);
            if ($entityResponse->getStatusCode() !== Response::HTTP_OK) {
                return $entityResponse;
            }

            $employeeIds = data_get(
                json_decode($entityResponse->getData(), true),
                'data.*.companies.*.employee_id'
            );

            $employeeResponse = $employeeService->getCompanyEmployeesById($companyId, $employeeIds);
            if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
                return $employeeResponse;
            }

            $unAuthorizedEmployees = collect(json_decode($employeeResponse->getData(), true))->reject(
                function ($employee) use ($request) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized([
                        AuthzDataScope::SCOPE_COMPANY => [data_get($employee, 'company_id')],
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group_id'),
                    ]);
                }
            );

            if (!$unAuthorizedEmployees->isEmpty()) {
                $errorMesssage = 'Unauthorized: Unable to process employees:[' .
                    implode(',', $unAuthorizedEmployees->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = $authorizedCompany && $unAuthorizedEmployees->isEmpty();
            }
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMesssage);
        }

        // call microservice
        $response = $this->requestService->bulkCreate($companyId, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $bonusIds = json_decode($response->getData(), true);
        $bonuses = collect($inputs)->map(function ($bonus, $index) use ($bonusIds) {
            array_set($bonus, 'id', array_get($bonusIds, $index));
            array_set($bonus, 'type', 'App\Model\PhilippineBonus');
            return $bonus;
        })->toArray();

        if ($response->isSuccessful()) {
            foreach ($bonuses as $bonus) {
                // trigger audit trail
                $this->audit($request, $companyId, $bonus);
            }
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/bonus/{id}",
     *     summary="Update the bonus",
     *     description="Update the bonus
Authorization Scope : **edit.bonuses**",
     *     tags={"bonus"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         description="Bonus ID",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="bonus",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bonusesRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeRequestService $otherIncomeRequestService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $bonusResponse = $otherIncomeRequestService->get($id);
        if (!$bonusResponse->isSuccessful()) {
            return $bonusResponse;
        }
        $bonusData = json_decode($bonusResponse->getData(), true);
        $companyId = array_get($bonusData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $authData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->update($id, $request->all(), $this->getAuthzDataScope($request));
        if ($response->isSuccessful()) {
            $newData = json_decode($response->getData(), true);
            // trigger audit trail
            $this->audit($request, $companyId, $newData, $bonusData, true);
        }
        return $response;
    }

    private function getAffectedEntities(Request $request)
    {
        $employeeIds = array_map(
            function ($employeeId) {
                return [
                    'type' => 'employee',
                    'id' => $employeeId
                ];
            },
            array_flatten($request->json('*.recipients.employees', []), 1)
        );

        $departmentIds = array_map(
            function ($departmentId) {
                return [
                    'type' => 'department',
                    'id' => $departmentId
                ];
            },
            array_flatten($request->json('*.recipients.departments', []), 1)
        );

        $payrollGroupIds = array_map(
            function ($payrollGroupId) {
                return [
                    'type' => 'payroll_group',
                    'id' => $payrollGroupId
                ];
            },
            array_flatten($request->json('*.recipients.payroll_groups', []), 1)
        );

        return array_merge($employeeIds, $departmentIds, $payrollGroupIds);
    }
}
