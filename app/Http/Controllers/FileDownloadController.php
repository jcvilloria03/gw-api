<?php

namespace App\Http\Controllers;

use App;
use App\Authz\AuthzDataScope;
use App\Payroll\PayrollRequestService;
use App\Payslip\PayslipRequestService;
use App\GovernmentForm\GovernmentFormRequestService;
use App\Jobs\JobsRequestService;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Aws\S3\Exception\S3Exception;
use Illuminate\Http\Response;
use App\Payroll\PayrollAuthorizationService;
use App\Payroll\PayslipAuthorizationService;
use App\Storage\UploadService;
use App\Traits\AuditTrailTrait;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class FileDownloadController extends Controller
{
    use AuditTrailTrait;

    private $bucket;
    private $payrollService;
    private $payslipService;
    private $governmentFormService;
    private $payrollAuthorizationService;
    private $payslipAuthorizationService;

    const TYPE_ATTENDANCE = 'attendance';
    const TYPE_ALLOWANCE = 'allowance';
    const TYPE_BONUS = 'bonus';
    const TYPE_COMMISSION = 'commission';
    const TYPE_DEDUCTION = 'deduction';
    const TYPE_PAYROLL_REGISTER = 'payroll_register';
    const TYPE_PAYROLL_REGISTERS = 'payroll_registers';
    const TYPE_PAYSLIP = 'payslip';
    const TYPE_PAYSLIPS = 'payslips';
    const TYPE_GOVT_FORM = 'govt_form';
    const TYPE_MASTERFILE = 'masterfile';

    const ACCEPTED_TYPE = [
        self::TYPE_ATTENDANCE,
        self::TYPE_ALLOWANCE,
        self::TYPE_BONUS,
        self::TYPE_COMMISSION,
        self::TYPE_DEDUCTION,
        self::TYPE_PAYROLL_REGISTER,
        self::TYPE_PAYROLL_REGISTERS,
        self::TYPE_PAYSLIP,
        self::TYPE_PAYSLIPS,
        self::TYPE_GOVT_FORM,
        self::TYPE_MASTERFILE,
    ];

    public function __construct()
    {
        $this->bucket = env('UPLOADS_BUCKET');
        $this->payrollService = App::make(PayrollRequestService::class);
        $this->payslipService = App::make(PayslipRequestService::class);
        $this->governmentFormService = App::make(GovernmentFormRequestService::class);
        $this->payrollAuthorizationService = App::make(PayrollAuthorizationService::class);
        $this->payslipAuthorizationService = App::make(PayslipAuthorizationService::class);
    }
    /**
     * @SWG\Get(
     *     path="/download/{type}/{uuid}",
     *     summary="Download s3 files",
     *     description="Endpoint to download s3 files",
     *     tags={"file download"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="uuid",
     *         type="string",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadFile(Request $request, string $type, string $id)
    {
        $stream = true;

        if (!in_array($type, self::ACCEPTED_TYPE)) {
            return $this->response->error('Invalid file request type', 406);
        }

        switch ($type) {
            case self::TYPE_ATTENDANCE:
            case self::TYPE_ALLOWANCE:
            case self::TYPE_BONUS:
            case self::TYPE_COMMISSION:
            case self::TYPE_DEDUCTION:
                $resourcePath = $this->getPayrollUpload($id, $type);
            break;

            case self::TYPE_PAYSLIP:
                $resourcePath = $this->getPayslip($id);
            break;

            case self::TYPE_PAYSLIPS:
                $resourcePath = $this->getZippedPayslips($id);
            break;

            case self::TYPE_PAYROLL_REGISTER:
                $resourcePath  = $this->getPayrollRegister($id);
                $uploadService = App::make(UploadService::class);
                return [ "data"=> ["uri"=>$uploadService->createSignedlUrl($resourcePath)]];
            break;

            case self::TYPE_PAYROLL_REGISTERS:
                $stream = false;
                $resourcePath  = $this->getZippedPayrollRegister($id);
            break;

            case self::TYPE_GOVT_FORM:
                $stream = false;
                $resourcePath  = $this->getGovernmentForm($id);
            break;
            case self::TYPE_MASTERFILE:
                $stream = false;
                $resourcePath  = $this->getEmployeeMasterFile($id);
            break;
        }

        if (empty($resourcePath)) {
            return $this->response()->errorNotFound('Requested file not found');
        }

        if ($stream === false) {
            return $resourcePath;
        }
        $extension = last(explode('.', $resourcePath));
        $extensionArr = explode('?', $extension);
        $extension = array_shift($extensionArr);

        $data = $this->streamData(function () use ($resourcePath) {
            $this->getFile($resourcePath);
        }, $this->makeHeaders($extension, $type));

        try {
            $this->audit($request, null, [], [ 'Export File' => strtoupper($type)]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $data;
    }

    /**
     * Get payroll upload data
     * @param $uuid job id
     * @param $type payroll item type
     * @return string
     */
    public function getPayrollUpload($uuid, $type)
    {
        $response = $this->payrollService->getPayrollJob($uuid);
        $response = json_decode($response->getOriginalContent());

        if (
            !$this->checkPayrollAuthorizationMultipleItems(
                [$response->data->payroll_id],
                'get'
            )
        ) {
            return $this->response->errorUnauthorized();
        }

        if (strtolower($response->data->type) !== $type) {
            return $this->response->errorNotFound('File not found');
        }

        return '/' . $response->data->uploaded_file;
    }

    /**
     * Get employee masterfile data
     * @param $uuid job id
     * @return string
     */
    public function getEmployeeMasterFile($uuid)
    {
        $request = app('request');
        $key = "";
        // Get s3 data from job
        $jobRequestService = App::make(JobsRequestService::class);
        $response = $jobRequestService->getJobResult($uuid);
        $response = json_decode($response->getData());
        $data = collect($response->data)->first(); // Get generated zip file only

        if (!empty($data)) {
            // Get company id from url
            $key = $data->attributes->result->Key;
            $companyId = explode("_", explode("/", $key)[1])[1];
            if ($this->isAuthzEnabled($request)
                && !$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)
            ) {
                $this->response()->errorUnauthorized();
            }
        }
        $uploadService = App::make(UploadService::class);
        return [ "data"=> ["uri"=>$uploadService->createSignedlUrl($key)]];
    }

    /**
     * @todo create test for this
     */
    public function getGovernmentForm($uuid)
    {
        $request = app('request');
        $govtFormResponse = $this->governmentFormService->get($uuid);
        if ($govtFormResponse->getStatusCode() !== Response::HTTP_OK) {
            return $this->response->errorNotFound('File not found');
        }
        $govtForm = json_decode($govtFormResponse->getData(), true);
        $companyId = Arr::get($govtForm, 'data.attributes.companyId')
            ?? Arr::get($govtForm, 'data.attributes.company_id');

        if ($this->isAuthzEnabled($request)
            && !$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->governmentFormService->generateForm($uuid);

        return $response;
    }

    /**
     * @todo create test for this
     */
    public function getPayslip($uuid)
    {
        $response = $this->payslipService->download($uuid, ['download'=>true, 'read'=>false]);
        $responseData = json_decode($response->getOriginalContent());

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $this->response->errorNotFound('File not found');
        }

        $uri = parse_url($responseData->uri);
        $bucket = head(explode('.', $uri['host']));

        $this->bucket = $bucket;

        return $uri['path'];
    }

    /**
     * @todo create test for this
     */
    public function getZippedPayslips(string $uuid)
    {
        $response = $this->payslipService->getZippedPayslipsUrl($uuid);
        $responseData = json_decode($response->getOriginalContent());

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $this->response->errorNotFound('File not found');
        }

        $uri = parse_url($responseData->uri);
        $bucket = head(explode('.', $uri['host']));

        $this->bucket = $bucket;

        return $uri['path'];
    }

    /**
     * @todo provide when payroll register is up
     */
    public function getPayrollRegister($uuid)
    {
        $response = $this->payrollService->getPayrollRegister($uuid);
        $responseData = json_decode($response->getOriginalContent());

        if (
            !$this->checkPayrollAuthorizationMultipleItems(
                [$responseData->data->attributes->payroll_id],
                'get'
            )
        ) {
            return $this->response->errorUnauthorized();
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $this->response->errorNotFound('File not found');
        }

        return $responseData->data->attributes->file;
    }

    /**
     * @todo get zipped payroll file details
     */
    public function getZippedPayrollRegister($uuid)
    {
        $response = $this->payrollService->getZippedPayrollRegisterByHash($uuid);
        $responseData = json_decode($response->getOriginalContent());

        if (
            !$this->checkPayrollAuthorizationMultipleItems(
                explode(',', $responseData->data->attributes->payroll_ids),
                'get'
            )
        ) {
            return $this->response->errorUnauthorized();
        }


        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $this->response->errorNotFound('File not found');
        }

        return $responseData->data->attributes->signed_url;
    }

    public function getFile($s3Resource)
    {
        app('aws')->createClient('s3')->registerStreamWrapper();

        try {
            if ($stream = fopen('s3://'.$this->bucket . $s3Resource, 'r')) {
                while (!feof($stream)) {
                    echo fread($stream, 1024);
                }
                fclose($stream);
            }
        } catch (S3Exception $exception) {
            return $this->response->error('cannot read file', 401);
        }
    }

    //stream to make a little memory footprint during execution for big files
    private function streamData($callback, $headers)
    {
        $response = new StreamedResponse($callback, Response::HTTP_OK, $headers);

        return $response;
    }
    /**
     * Header generator for downloads
     */
    public function makeHeaders($extension, $type)
    {
        $mimes = [
            'pdf' => 'application/pdf',
            'csv' => 'text/csv',
            'zip' => 'application/zip',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];

        if (!in_array($extension, array_keys($mimes))) {
            throw new \Exception("Mime $extension not acceptable");
        }

        return [
            "Cache-Control" => "no-store",
            "Access-Control-Allow-Origin" => "*",
            "Content-Disposition" => "attachment; filename=" . uniqid("download_{$type}_", true) . '.' . $extension,
            "Content-Type" => $mimes[$extension]
        ];
    }

    /**
     * @todo add endpoint to get multiple payrolls
     * get payroll data and run authorization tasks
     */
    private function checkPayrollAuthorizationMultipleItems(array $payrollIds, $authorizationTask)
    {
        $payrolls = [];

        $request = app('request');

        $authUser = $request->attributes->get('user');

        $task = camel_case("authorize_{$authorizationTask}");

        $methods = get_class_methods(PayrollAuthorizationService::class);

        if (!in_array($task, $methods)) {
            return false;
        }

        foreach ($payrollIds as $id) {
            $response = $this->payrollService->get($id);
            $payrollData = $payrolls[] = json_decode($response->getData());

            if ($this->isAuthzEnabled($request)) {
                $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $payrollData->company_id,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollData->payroll_group_id,
                ]);
            } else {
                $isAuthorized = $this->payrollAuthorizationService->{$task}(
                    $payrollData,
                    $authUser
                );
            }

            //authorize
            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $payrolls;
    }
}
