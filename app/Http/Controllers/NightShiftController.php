<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\NightShift\NightShiftAuditService;
use App\NightShift\NightShiftAuthorizationService;
use Illuminate\Http\Request;
use App\NightShift\NightShiftRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

class NightShiftController extends Controller
{
    /**
     * @var \App\NightShift\NightShiftRequestService
     */
    private $requestService;

    /**
     * @var \App\NightShift\NightShiftAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        NightShiftRequestService $requestService,
        NightShiftAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/night_shift/{id}",
     *     summary="Get Night Shift details",
     *     description="Get Night Shift details

Authorization Scope : **view.night_shift**",
     *     tags={"night_shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Night Shift ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);

        $nightShiftData = json_decode($response->getData());
        $nightShiftData->account_id = Company::getAccountId($nightShiftData->company_id);

        if (!$this->authorizationService->authorizeGet($nightShiftData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/night_shift",
     *     summary="Get night shift settings for the company",
     *     description="Get night shift settings for the company

Authorization Scope : **view.night_shift**",
     *     tags={"night_shift"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyNightShift(Request $request, int $companyId)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $nightShift = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyNightShift(
                $nightShift,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyNightShift($companyId);
    }

    /**
     * @SWG\Put(
     *     path="/night_shift/{id}",
     *     summary="Update night shift",
     *     description="Update night shift

     Authorization Scope : **edit.night_shift**",
     *     tags={"night_shift"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Night Shift Id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         type="integer",
     *         in="formData",
     *         description="Company Id",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="activated",
     *         type="boolean",
     *         in="formData",
     *         description="Is Night Shift activated?",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="start",
     *         type="string",
     *         in="formData",
     *         description="Start time of the night shift in HH:mm format",
     *         required=true,
     *         default="22:00"
     *     ),
     *     @SWG\Parameter(
     *         name="end",
     *         type="string",
     *         in="formData",
     *         description="End time of the night shift in HH:mm format",
     *         required=true,
     *         default="06:00"
     *     ),
     *     @SWG\Parameter(
     *         name="time_shift",
     *         type="string",
     *         in="formData",
     *         description="Time shift. Possible values: Carry over, Counter per hour",
     *         required=true,
     *         enum={"Carry over", "Counter per hour"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');

        $response = $this->requestService->get($id);
        $oldNightShiftData = json_decode($response->getData());
        $oldNightShiftData->account_id = Company::getAccountId($inputs['company_id']);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldNightShiftData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldNightShiftData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($id, $inputs);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldNightShiftData = json_decode($response->getData(), true);
        $newNightShiftData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldNightShiftData,
            'new' => $newNightShiftData,
        ];
        $item = new AuditCacheItem(
            NightShiftAuditService::class,
            NightShiftAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }
}
