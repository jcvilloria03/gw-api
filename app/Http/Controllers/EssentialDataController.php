<?php

namespace App\Http\Controllers;

use App\Account\AccountRequestService;
use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;

class EssentialDataController extends Controller
{
    /**
     * @var \App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * EssentialDataController constructor
     *
     * @param AccountRequestService $accountRequestService
     */
    public function __construct(AccountRequestService $accountRequestService)
    {
        $this->accountRequestService = $accountRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/accounts/{accountId}/essential_data",
     *     summary="Get account essential data",
     *     description="Get list of essential data by account",
     *     tags={"essential_data"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="List of Essential Data",
     *         examples={
     *             "application/json": {
     *                 "department": {
     *                     {
     *                         "id": 1,
     *                         "name": "Department Name"
     *                     }
     *                 },
     *                 "position": {
     *                     {
     *                         "id": 1,
     *                         "name": "Position Name"
     *                     }
     *                 },
     *                 "team": {
     *                     {
     *                         "id": 1,
     *                         "name": "Team Name"
     *                     }
     *                 },
     *                 "location": {
     *                     {
     *                         "id": 1,
     *                         "name": "Location Name"
     *                     }
     *                 },
     *                 "payroll_group": {
     *                     {
     *                         "id": 1,
     *                         "name": "Payroll Group Name"
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getAccountEssentialData(int $accountId)
    {
        $response = $this->accountRequestService->getAccountEssentialData($accountId, true);

        $essentialData = json_decode($response->getData(), true);

        return array_merge_recursive(...array_values($essentialData));
    }

    /**
     * @SWG\Get(
     *     path="/accounts/{accountId}/companies/{companyId}/essential_data",
     *     summary="Get company essential data",
     *     description="Get list of essential data by company",
     *     tags={"essential_data"},
     *     deprecated=true,
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="List of Essential Data",
     *         examples={
     *             "application/json": {
     *                 "department": {
     *                     {
     *                         "id": 1,
     *                         "name": "Department Name"
     *                     }
     *                 },
     *                 "position": {
     *                     {
     *                         "id": 1,
     *                         "name": "Position Name"
     *                     }
     *                 },
     *                 "team": {
     *                     {
     *                         "id": 1,
     *                         "name": "Team Name"
     *                     }
     *                 },
     *                 "location": {
     *                     {
     *                         "id": 1,
     *                         "name": "Location Name"
     *                     }
     *                 },
     *                 "payroll_group": {
     *                     {
     *                         "id": 1,
     *                         "name": "Payroll Group Name"
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid Request",
     *     )
     * )
     */
    public function getCompanyEssentialData(Request $request, int $accountId, int $companyId)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->accountRequestService->getCompanyEssentialData($accountId, $companyId, true);

        $essentialData = json_decode($response->getData(), true);

        return $essentialData;
    }
}
