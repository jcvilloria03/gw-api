<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Laravel\Lumen\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Subscriptions\SubscriptionsRequestService;
use App\Role\RoleRequestService;

/**
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 *
 */
class Controller extends BaseController
{
    use Helpers;

    // Returns \Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
    protected function invalidRequestError($msg = 'Your request was invalid.')
    {
        throw new \Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException($msg);
    }

    // Returns \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND
    protected function notFoundError($msg = 'Request not found.')
    {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException($msg);
    }

    /*
    * Overrides method of \Laravel\Lumen\Routing\ProvidesConvenienceMethods
    */
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            $error = current($this->formatValidationErrors($validator));
            $this->invalidRequestError(current($error));
        }
    }

    public function validateArray(array $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $this->makeValidation($request, $rules, $messages, $customAttributes);
    }

    public function makeValidation($request, $rules, $messages, $customAttributes)
    {
        $validator = $this->getValidationFactory()->make($request, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            $error = current($this->formatValidationErrors($validator));
            $this->invalidRequestError(current($error));
        }
    }

    /**
     * @param Request $request
     * @return AuthzDataScope|null
     */
    protected function getAuthzDataScope(Request $request)
    {
        return $request->attributes->get(AuthzDataScope::REQUEST_AUTHZ_DATA_SCOPE);
    }

    protected function isAuthzEnabled(Request $request): bool
    {
        return $request->attributes->get(AuthzDataScope::REQUEST_AUTHZ_ENABLED, false)
            && $this->getAuthzDataScope($request) instanceof AuthzDataScope;
    }

    protected function getRequestCachingKey(Request $request) : string
    {
        $env           = env('APP_ENV', 'local');
        $requestMethod = $request->method();
        $requestPath   = $request->fullUrl();

        return "{$env}-{$requestMethod}-{$requestPath}";
    }

    protected function isAccountSuspended(array $user)
    {
        $roleRequestService = app()->make(RoleRequestService::class);
        $subscriptionRequestService = app()->make(SubscriptionsRequestService::class);
        $response = $subscriptionRequestService->getCustomerByAccountId($user['account_id']);
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (empty($customer['subscriptions'][0])) {
            return 'No active subscription for user\'s account.';
        }

        $subscription = $customer['subscriptions'][0];
        if (!$subscription['is_expired']) {
            return null;
        }

        $role = $this->roleRequestService->getRoleByAccount(
            $user['account_id'],
            $user['role_id'],
            true
        );

        if (empty($role)) {
            return $roleRequestService::ROLE_ADMIN;
        }

        $roleType = $roleRequestService->getRoleType($role['data']);
        if ($roleType) {
            return $roleType;
        }

        return $roleRequestService::ROLE_ADMIN;
    }
}
