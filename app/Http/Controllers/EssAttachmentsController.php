<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\ESS\EssAttachmentRequestService;
use App\Workflow\WorkflowRequestService;
use App\Attachment\AttachmentAuditService;
use App\Http\Controllers\EssBaseController;
use App\ESS\EssEmployeeRequestRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\LeaveRequest\LeaveRequestRequestService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Authz\AuthzDataScope;

class EssAttachmentsController extends EssBaseController
{
    /**
     * @var \App\ESS\EssAttachmentRequest
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EssAttachmentRequestService $requestService,
        EssEmployeeRequestRequestService $employeeRequestRequestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->employeeRequestRequestService = $employeeRequestRequestService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/attachments",
     *     summary="Upload Attachment",
     *     description="Upload Attachment for request",
     *     tags={"ess"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="attachment",
     *         in="formData",
     *         required=true,
     *         type="file",
     *         description="Attachment file"
     *     ),
     *     @SWG\Parameter(
     *         name="request_id",
     *         in="formData",
     *         type="integer",
     *         description="Employee Request Id. If exists, request should be reset."
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $requestId = $request->get('request_id');
        $employeeRequest = null;
        $requestData = [];

        if (!empty($requestId)) {
            $employeeRequest = json_decode(
                $this->employeeRequestRequestService->get($requestId)->getData()
            );
            $requestData['request_id'] = $requestId;
        }

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeAttachmentUpload($user, $employeeRequest)) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('attachment');

        $data = [
            'attachment' => $file,
            'attachment_name' => $file->getClientOriginalName()
        ];

        $response = $this->requestService->upload(array_merge($data, $requestData));
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'new' => $responseData,
        ];
        $this->performAuditLog($user, AttachmentAuditService::ACTION_CREATE, $details);

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/ess/employee_request/{requestId}/attachments/{id}",
     *     summary="Delete Attachment",
     *     description="Delete Attachment",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="requestId",
     *         in="path",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function delete(Request $request, $requestId, $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $employeeRequest = json_decode(
            $this->employeeRequestRequestService->get($requestId)->getData()
        );

        if (!$this->authorizationService->authorizeAttachmentUpload($user, $employeeRequest)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->delete($id);

        // audit log
        $details = [
            'old' => [
                'id' => $id
            ]
        ];
        $this->performAuditLog($user, AttachmentAuditService::ACTION_DELETE, $details);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/ess/attachments/replace",
     *     summary="Replace Attachment",
     *     description="Replace Attachment for request",
     *     tags={"ess"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="attachment",
     *         in="formData",
     *         required=true,
     *         type="file",
     *     ),
     *     @SWG\Parameter(
     *         name="request_id",
     *         in="formData",
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="replacing_attachment_id",
     *         in="formData",
     *         description="Replacing attachment Id",
     *         type="integer",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function replace(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $requestId = $request->get('request_id');
        $employeeRequest = [];

        if (!empty($requestId)) {
            $employeeRequest = json_decode(
                $this->employeeRequestRequestService->get($requestId)->getData()
            );
        }

        if (!$this->authorizationService->authorizeAttachmentUpload($user, $employeeRequest)) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('attachment');
        $replacingAttachmentId = $request->get('replacing_attachment_id');

        $data = [
            'attachment' => $file,
            'request_id' => $requestId,
            'attachment_name' => $file->getClientOriginalName(),
            'replacing_attachment_id' => $replacingAttachmentId
        ];

        $response = $this->requestService->replace($data);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'new' => $responseData,
        ];
        $this->performAuditLog($user, AttachmentAuditService::ACTION_CREATE, $details);

        $details = [
            'old' => [
                'id' => $replacingAttachmentId
            ]
        ];
        $this->performAuditLog($user, AttachmentAuditService::ACTION_DELETE, $details);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee_request/{id}/download_attachments",
     *     summary="Download all attachments of employee request",
     *     description="Download all attachments of employee request",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/zip"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee Request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function download(
        Request $request,
        LeaveRequestRequestService $leaveRequestService,
        WorkflowRequestService $workflowRequestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        $id
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $employeeRequest = json_decode($this->employeeRequestRequestService->get($id)->getData());
        $leaveRequest = json_decode($leaveRequestService->get($employeeRequest->request->id)->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = ApprovalService::isEitherRequestorOrApprover(
                $user['employee_id'],
                $leaveRequest,
                $user
            );
        } else {
            $responseData = $workflowRequestService->getEmployeeWorkflows($user['employee_id']);
            $workflows = json_decode($responseData->getData(), true);
            $isAuthorized = $authorizationService->authorizeViewSingleRequest($user, $workflows, $leaveRequest);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->downloadAttachments($id);
    }

    /**
     * Call audit service and log given action for given user with given details
     * @param array $user that performed action
     * @param string $action performed
     * @param array $details to log
     * @return void
     */
    private function performAuditLog(array $user, string $action, array $details)
    {
        $item = new AuditCacheItem(
            AttachmentAuditService::class,
            $action,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );

        $this->auditService->queue($item);
    }
}
