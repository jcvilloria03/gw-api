<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request as LaravelRequest;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Authz\AuthzDataScope;
use App\User\UserRequestService;
use App\Salpay\SalpayAuthorizationService;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class SalpayIntegrationController extends Controller
{
    use AuditTrailTrait;

    const ACTION_VIEW = 'VIEW';

    protected $client;

    /**
     * @var \App\Salpay\SalpayAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->authorizationService = app(SalpayAuthorizationService::class);
        $this->userRequestService = app(UserRequestService::class);
    }

    public function passThroughToApi(LaravelRequest $request)
    {
        $requestContent = $request->getContent();

        $jsonContent = null;
        if (!empty($requestContent)) {
            $jsonContent = json_decode($requestContent, true);
            // make sure content body is a valid json
            if (null === $jsonContent) {
                throw new HttpException(400, "Request content is an invalid json.");
            }
        }

        // authcheck
        $userReqAttribArr = $request->attributes->get('user');

        $userResponse = $this->userRequestService->getUserDetails($userReqAttribArr['user_id']);
        $userData = json_decode($userResponse->getData());

        $companyId = $this->determineCompanyId($request);
        if (!$companyId && isset($userData->last_active_company_id)) {
            $companyId = $userData->last_active_company_id;
        }

        $allowedCompanyIds = array_column($userData->companies, 'id');

        $authzDataScope = $this->getAuthzDataScope($request);

        $isAuthorized = false;
        $isAuthzEnabled = $this->isAuthzEnabled($request);
        $requestMethod = $request->method();

        if ($isAuthzEnabled && $companyId !== null) {
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else if ($isAuthzEnabled && $companyId === null) {
            // No company ID. Endpoint is account level
            $isAuthorized = true;
        } else {
            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $companyId,
            ];

            $authMethods = [
                Request::METHOD_GET => 'authorizeGet',
                Request::METHOD_POST => 'authorizeCreate',
                Request::METHOD_PUT => 'authorizeUpdate',
                Request::METHOD_DELETE => 'authorizeDelete',
            ];

            $authMethod = $authMethods[$requestMethod];

            $isActionAuthorized = $this->authorizationService->$authMethod($authData, $userReqAttribArr);
            $isCompanyAllowed = $companyId === null || in_array($companyId, $allowedCompanyIds);

            $isAuthorized = !$isActionAuthorized || !$isCompanyAllowed;
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        try {
            $options = [
                'headers' => [
                    'Authorization' => $request->header('Authorization'),
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'query' => array_merge($request->query(), [
                    'authz_data_scope' => $isAuthzEnabled
                        ? json_encode($authzDataScope->getDataScope())
                        : null,
                    'user_data' => json_encode([
                        "user_id" => $userReqAttribArr['user_id'],
                        "account_role" => $userData->role ?? null
                    ])
                ]),
            ];

            if (!empty($jsonContent)) {
                $options['json'] = $jsonContent;
            }

            $response = $this->client->request(
                $requestMethod,
                substr($request->path(), strlen('salpay')),
                $options
            );

            try {
                $actionType = $this->getModuleActionType(
                    $request->method(),
                    $request->route()[1]['uri'],
                    $request->headers->get('X-Authz-Entities')
                );

                if (!$actionType == self::ACTION_VIEW) {
                    $responseData = $response->getBody()->getContents();
                    $responseData = is_string($responseData) ? json_decode($responseData, true) : $responseData;

                    $this->audit($request, $companyId, $responseData);
                }
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return $response;
        } catch (BadResponseException $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            // just throw the internal api reponse
            $response = $e->getResponse();

            $headers = $response->getHeaders();
            if (array_key_exists('Sal-Src', $headers)) {
                $baseUriArr = array_reverse(explode('.', $headers['Sal-Src'][0]));
                $headers['Sal-Src'] = $baseUriArr[min(count($baseUriArr)-1, 1)];
            }

            return new Response(
                $response->getBody()->getContents(),
                $response->getStatusCode(),
                $headers
            );
        }
    }

    /**
     * Determine company ID from request params/url/body
     *
     * @param  LaravelRequest $request Request object
     * @return int|null
     */
    protected function determineCompanyId(LaravelRequest $request)
    {
        $route = $request->route();

        $inputParams = $request->only(['company_id', 'companyId']);

        $pathParams = is_array($route)
            ? array_last($route)
            : [];

        $params = array_merge($inputParams, $pathParams);

        $requestedCompanyId = current(array_filter([
            $params['companyId'] ?? null,
            $params['company_id'] ?? null,
        ]));

        return is_numeric($requestedCompanyId)
            ? (int) $requestedCompanyId
            : null;
    }
}
