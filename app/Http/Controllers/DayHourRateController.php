<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\DayHourRate\DayHourRateAuditService;
use App\DayHourRate\DayHourRateAuthorizationService;
use Illuminate\Http\Request;
use App\DayHourRate\DayHourRateRequestService;
use Symfony\Component\HttpFoundation\Response;

class DayHourRateController extends Controller
{
    /**
     * @var \App\DayHourRate\DayHourRateRequestService
     */
    private $requestService;

    /**
     * @var \App\DayHourRate\DayHourRateAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        DayHourRateRequestService $requestService,
        DayHourRateAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/day_hour_rate/{id}",
     *     summary="Get Day/Hour Rate details",
     *     description="Get Day/Hour Rate details

Authorization Scope : **view.day_hour_rate**",
     *     tags={"day_hour_rate"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Day/Hour Rate ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);

        $dayHourRateData = json_decode($response->getData());
        $dayHourRateData->account_id = Company::getAccountId($dayHourRateData->company_id);

        if (!$this->authorizationService->authorizeGet($dayHourRateData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/day_hour_rate/",
     *     summary="Create day/hour rates for the company",
     *     description="Create day/hour rates for the company

Authorization Scope : **create.day_hour_rate**",
     *     tags={"day_hour_rate"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $dayHourRateData = (object) [
            'account_id' => Company::getAccountId($companyId),
            'company_id' => $companyId
        ];
        if (!$this->authorizationService->authorizeCreate($dayHourRateData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());

        // audit log
        $dayHourRatesGetResponse = $this->requestService->get($companyId);
        $dayHourRatesData = json_decode($dayHourRatesGetResponse->getData(), true);

        foreach ($dayHourRatesData['data'] as $dayHourRate) {
            $item = new AuditCacheItem(
                DayHourRateAuditService::class,
                DayHourRateAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                [
                    'new' => $dayHourRate,
                ]
            );
            $this->auditService->queue($item);
        }

        return $response;
    }


    /**
     * @SWG\Get(
     *     path="/company/{id}/day_hour_rates",
     *     summary="Get all day/hour rates for the company",
     *     description="Get all day/hour rates for the company

Authorization Scope : **view.day_hour_rate**",
     *     tags={"day_hour_rate"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyDayHourRates($id, Request $request)
    {
        $response = $this->requestService->getCompanyDayHourRates($id);
        $dayHourRateData = json_decode($response->getData())->data;
        if (empty($dayHourRateData)) {
            return $response;
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $dayHourRate = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyDayHourRates(
                $dayHourRate,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/day_hour_rate/{id}",
     *     summary="Update day/hour rate",
     *     description="Update day/hour rate

     Authorization Scope : **edit.day_hour_rate**",
     *     tags={"day_hour_rate"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Day/Hour Rate Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="rate",
     *         in="formData",
     *         description="Rate value",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);

        $oldDayHourRateData = json_decode($response->getData());
        $oldDayHourRateData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldDayHourRateData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldDayHourRateData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldDayHourRateData = json_decode($response->getData(), true);
        $newDayHourRateData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldDayHourRateData,
            'new' => $newDayHourRateData,
        ];
        $item = new AuditCacheItem(
            DayHourRateAuditService::class,
            DayHourRateAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }
}
