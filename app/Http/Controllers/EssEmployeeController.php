<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth0\Auth0UserService;
use App\User\UserRequestService;
use App\Shift\ShiftRequestService;
use App\Ess\EssPayrollRequestService;
use App\Ess\EssEmployeeRequestService;
use App\ESS\EssNotificationRequestService;
use App\Http\Controllers\EssBaseController;
use App\Shift\EssShiftsAuthorizationService;
use App\ESS\EssEmployeeRequestRequestService;
use App\Validator\EssPasswordChangeValidator;
use Symfony\Component\HttpFoundation\Response;
use App\PayrollGroup\PayrollGroupRequestService;
use App\Employee\EssEmployeeAuthorizationService;
use App\PayrollGroup\EssPayrollGroupAuthorizationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EssEmployeeController extends EssBaseController
{
    /**
     * @var \App\Ess\EssEmployeeRequestService
     */
    protected $requestService;

    /**
     * @var \App\Ess\EssPayrollRequestService
     */
    protected $payrollRequestService;

    /**
     * @var \App\Employee\EssEmployeeAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Shift\EssShiftsAuthorizationService
     */
    protected $essShiftsAuthorizationService;

    /**
     * @var \App\Shift\ShiftRequestService
     */
    protected $shiftRequestService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(
        EssEmployeeRequestService $requestService,
        EssPayrollRequestService $payrollRequestService,
        EssEmployeeAuthorizationService $authorizationService,
        ShiftRequestService $shiftRequestService,
        EssShiftsAuthorizationService $essShiftsAuthorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $requestService;
        $this->payrollRequestService = $payrollRequestService;
        $this->authorizationService = $authorizationService;
        $this->shiftRequestService = $shiftRequestService;
        $this->essShiftsAuthorizationService = $essShiftsAuthorizationService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/philippine/employee/profile",
     *     summary="Get employee profile",
     *     description="View Philippine Employee details:
Basic Information, Employment Details, Payroll Information, and Year to Date Figures

Authorization Scope : **ess.view.profile**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="integer", description="Employee Id (internal)"),
     *             @SWG\Property(property="account_id", type="integer", description="Account Id"),
     *             @SWG\Property(property="company_id", type="integer", description="Company Id"),
     *             @SWG\Property(property="payroll_group_name", type="string", description="Payroll Group Name"),
     *             @SWG\Property(property="employee_id", type="integer", description="Employee Id"),
     *             @SWG\Property(property="first_name", type="string", description="First Name"),
     *             @SWG\Property(property="middle_name", type="string", description="Middle Name"),
     *             @SWG\Property(property="last_name", type="string", description="Last Name"),
     *             @SWG\Property(property="email", type="string", description="Email Address"),
     *             @SWG\Property(property="gender", type="string", description="Gender"),
     *             @SWG\Property(property="birth_date", type="date", description="Date Of Birth"),
     *             @SWG\Property(property="telephone_number", type="integer", description="Telephone Number"),
     *             @SWG\Property(property="mobile_number", type="string", description="Mobile Number"),
     *             @SWG\Property(property="base_pay", type="string", description="Base Pay"),
     *             @SWG\Property(property="base_pay_unit", type="string", description="Base Pay Unit"),
     *             @SWG\Property(property="hours_per_day", type="number", description="Hours Per Day"),
     *             @SWG\Property(property="date_hired", type="date", description="Date Hired"),
     *             @SWG\Property(property="date_ended", type="date", description="Date Ended"),
     *             @SWG\Property(property="location_name", type="string", description="Location Name"),
     *             @SWG\Property(property="department_name", type="string", description="Department Name"),
     *             @SWG\Property(property="rank_name", type="string", description="Rank Name"),
     *             @SWG\Property(property="cost_center_name", type="string", description="Cost Center Name"),
     *             @SWG\Property(property="position_name", type="string", description="Position Name"),
     *             @SWG\Property(property="address", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *             @SWG\Property(property="tax_status", type="string", description="Tax Status"),
     *             @SWG\Property(property="tax_type", type="string", description="Tax Type"),
     *             @SWG\Property(property="consultant_tax_rate", type="integer", description="Consultant Tax Rate"),
     *             @SWG\Property(property="tin", type="string", description="TIN Number"),
     *             @SWG\Property(property="sss_number", type="string", description="SSS Number"),
     *             @SWG\Property(property="philhealth_number", type="string", description="PhilHealth Number"),
     *             @SWG\Property(property="hdmf_number", type="string", description="HDMF Number"),
     *             @SWG\Property(property="sss_basis", type="string", description="SSS Basis"),
     *             @SWG\Property(property="philhealth_basis", type="string", description="PhilHealth Basis"),
     *             @SWG\Property(property="hdmf_basis", type="string", description="HDMF Basis"),
     *             @SWG\Property(property="sss_amount", type="string", description="SSS Amount or Basis Label"),
     *             @SWG\Property(
     *                 property="philhealth_amount",
     *                 type="string",
     *                 description="PhilHealth Amount or Basis Label"
     *             ),
     *             @SWG\Property(property="hdmf_amount", type="string", description="HDMF Amount or Basis Label"),
     *             @SWG\Property(property="rdo", type="string", description="RDO"),
     *             @SWG\Property(property="gross_income", type="string", description="Gross Income"),
     *             @SWG\Property(property="taxable_income", type="string", description="Taxable Income"),
     *             @SWG\Property(property="withholding_tax", type="string", description="Withholding Tax"),
     *             @SWG\Property(property="net_pay", type="string", description="Net Pay"),
     *             @SWG\Property(property="deduction", type="string", description="Deduction"),
     *             @SWG\Property(property="sss_employer", type="string", description="SSS Employer"),
     *             @SWG\Property(property="sss_ec_employer", type="string", description="SSS EC Employer"),
     *             @SWG\Property(property="philhealth_employer", type="string", description="PhilHealth Employer"),
     *             @SWG\Property(property="hdmf_employer", type="string", description="HDMF Employer")
     *         ),
     *         examples={
     *              "application/json": {
     *                  {
     *                      "data": {
     *                          "id": 3366,
     *                          "account_id": 269,
     *                          "company_id": 205,
     *                          "payroll_group_name": "Payroll Group A",
     *                          "employee_id": "2017001",
     *                          "first_name": "Warden",
     *                          "middle_name": "Demuth",
     *                          "last_name": "Shawn",
     *                          "email": "shawnwardendemuthedited@example.com",
     *                          "gender": "Male",
     *                          "birth_date": "1952-11-03",
     *                          "telephone_number": 1234567,
     *                          "mobile_number": "(63)9053343745",
     *                          "base_pay": "P 50000.00",
     *                          "base_pay_unit": "Per Month",
     *                          "hours_per_day": 8.00,
     *                          "date_hired": "1974-09-01",
     *                          "date_ended": "2016-09-01",
     *                          "location_name": "HQ Location",
     *                          "department_name": "Test Department",
     *                          "rank_name": "Test Rank",
     *                          "cost_center_name": "Test Cost Center",
     *                          "position_name": "Test Position",
     *                          "address": {
     *                              "address_line_1": "address line 1",
     *                              "address_line_2": "address line 2",
     *                              "city": "Makati",
     *                              "zip_code": "4478",
     *                              "country": "Philippines"
     *                          },
     *                          "tax_status": "S",
     *                          "tax_status_label": "Single",
     *                          "tax_type": "CONSULTANT",
     *                          "tax_type_label": "Consultant",
     *                          "consultant_tax_rate": 12,
     *                          "tin": "123-456-789-012",
     *                          "sss_number": "04-7451477-1",
     *                          "philhealth_number": "74-560045512-8",
     *                          "hdmf_number": "4177-3254-4787",
     *                          "sss_basis": "Basic Pay",
     *                          "philhealth_basis": "Basic Pay",
     *                          "hdmf_basis": "Basic Pay",
     *                          "sss_amount": "Based on Base Rate",
     *                          "philhealth_amount": "Based on Base Rate",
     *                          "hdmf_amount": "Based on Base Rate",
     *                          "rdo": "047",
     *                          "gross_income": "P 34,028.84",
     *                          "taxable_income": "P 31,213.84",
     *                          "withholding_tax": "P 4,157.71",
     *                          "net_pay": "P 29,871.13",
     *                          "deduction": "P -514.00",
     *                          "sss_employer": "P 736.70",
     *                          "sss_ec_employer": "P 10.00",
     *                          "philhealth_employer": "P 112.50",
     *                          "hdmf_employer": "P 100.00"
     *                      }
     *                  },
     *                  {
     *                      "data": {
     *                          "id": 3498,
     *                          "account_id": 269,
     *                          "company_id": 205,
     *                          "payroll_group_name": "Payroll Group A",
     *                          "employee_id": "2020975",
     *                          "first_name": "Robert",
     *                          "middle_name": "Joel",
     *                          "last_name": "Christner",
     *                          "email": "robertjoelchristner@example.com",
     *                          "gender": "Male",
     *                          "birth_date": "1952-11-03",
     *                          "telephone_number": 1234567,
     *                          "mobile_number": "(63)9053343745",
     *                          "base_pay": "P 50,000.00",
     *                          "base_pay_unit": "Per Month",
     *                          "hours_per_day": 8.00,
     *                          "date_hired": "1974-09-01",
     *                          "date_ended": "2016-09-01",
     *                          "location_name": "HQ Location",
     *                          "department_name": "Test Department",
     *                          "rank_name": "Test Rank",
     *                          "cost_center_name": "Test Cost Center",
     *                          "position_name": "Test Position",
     *                          "address": {
     *                              "address_line_1": "address line 1",
     *                              "address_line_2": "address line 2",
     *                              "city": "Makati",
     *                              "zip_code": "4478",
     *                              "country": "Philippines"
     *                          },
     *                          "tax_status": "M",
     *                          "tax_status_label": "Married",
     *                          "tax_type": "REGULAR",
     *                          "tax_type_label": "Regular",
     *                          "tin": "123-456-789-012",
     *                          "sss_number": "04-7451477-1",
     *                          "philhealth_number": "74-560045512-8",
     *                          "hdmf_number": "4177-3254-4787",
     *                          "sss_basis": "Gross Income",
     *                          "philhealth_basis": "Gross Income",
     *                          "hdmf_basis": "Gross Income",
     *                          "sss_amount": "Based on Gross",
     *                          "philhealth_amount": "Based on Gross",
     *                          "hdmf_amount": "Based on Gross",
     *                          "rdo": "047",
     *                          "gross_income": "P 34,028.84",
     *                          "taxable_income": "P 31,213.84",
     *                          "withholding_tax": "P 4,157.71",
     *                          "net_pay": "P 29,871.13",
     *                          "deduction": "P -514.00",
     *                          "sss_employer": "P 736.70",
     *                          "sss_ec_employer": "P 10.00",
     *                          "philhealth_employer": "P 112.50",
     *                          "hdmf_employer": "P 100.00"
     *                      }
     *                  },
     *                  {
     *                      "data": {
     *                          "id": 3666,
     *                          "account_id": 269,
     *                          "company_id": 205,
     *                          "payroll_group_name": "Payroll Group A",
     *                          "employee_id": "2020001",
     *                          "first_name": "Candida",
     *                          "middle_name": "Smith",
     *                          "last_name": "Huckins",
     *                          "email": "candidahuckins@example.com",
     *                          "gender": "F",
     *                          "birth_date": "1952-11-03",
     *                          "telephone_number": 1234567,
     *                          "mobile_number": "(63)9053343745",
     *                          "base_pay": "P 50,000.00",
     *                          "base_pay_unit": "Per Month",
     *                          "hours_per_day": 8.00,
     *                          "date_hired": "1974-09-01",
     *                          "date_ended": "2016-09-01",
     *                          "location_name": "HQ Location",
     *                          "department_name": "Test Department",
     *                          "rank_name": "Test Rank",
     *                          "cost_center_name": "Test Cost Center",
     *                          "position_name": "Test Position",
     *                          "address": {
     *                              "address_line_1": "address line 1",
     *                              "address_line_2": "address line 2",
     *                              "city": "Makati",
     *                              "zip_code": "4478",
     *                              "country": "Philippines"
     *                          },
     *                          "tax_status": "M3",
     *                          "tax_status_label": "Married - 3 Dependents",
     *                          "tax_type": "ROHQ",
     *                          "tax_type_label": "ROHQ",
     *                          "tin": "123-456-789-012",
     *                          "sss_number": "04-7451477-1",
     *                          "philhealth_number": "74-560045512-8",
     *                          "hdmf_number": "4177-3254-4787",
     *                          "sss_basis": "Fixed",
     *                          "philhealth_basis": "Fixed",
     *                          "hdmf_basis": "Fixed",
     *                          "sss_amount": "500.12",
     *                          "philhealth_amount": "500.12",
     *                          "hdmf_amount": "215.23",
     *                          "rdo": "047",
     *                          "gross_income": "P 34,028.84",
     *                          "taxable_income": "P 31,213.84",
     *                          "withholding_tax": "P 4,157.71",
     *                          "net_pay": "P 29,871.13",
     *                          "deduction": "P -514.00",
     *                          "sss_employer": "P 736.70",
     *                          "sss_ec_employer": "P 10.00",
     *                          "philhealth_employer": "P 112.50",
     *                          "hdmf_employer": "P 100.00"
     *                      }
     *                  },
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getProfile(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        //authorize
        if (!$this->authorizationService->authorizeGet($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        $employeeProfile    = $this->requestService->getProfile($user['employee_id']);
        $employeeProfile    = json_decode($employeeProfile->getData(), true);
        $employeeYtdProfile = $this->payrollRequestService->getYtdProfile($user['employee_id']);
        $employeeYtdProfile = json_decode($employeeYtdProfile->getData(), true);

        return array_merge($employeeProfile, $employeeYtdProfile);
    }

    /**
     * @SWG\Get(
     *     path="/ess/shifts",
     *     summary="Get all Shifts",
     *     description="Get all Shifts for employee.",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         description="Shifts start date (date from): Y-m-d",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         description="Shifts end date (date to): Y-m-d",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getShifts(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (!$this->isAuthzEnabled($request)
            && !$this->essShiftsAuthorizationService->authorizeGet($user['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        $employeeShifts = $this->shiftRequestService->getEmployeeShifts($user['employee_id'], $request->all());

        return $employeeShifts;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/user",
     *     summary="Get logged-in user data",
     *     description="Get data for logged-in user",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getUser(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userResponseData = json_decode($userResponse->getData());
            $authData = (object) [
                'account_id' => $userResponseData->account_id,
                'company_id' => $userResponseData->companies[0]->id,
            ];

            if (!$this->authorizationService->authorizeGet($user['user_id'], $authData)) {
                $this->response()->errorUnauthorized();
            }
        }

        $employeeResponse = $this->requestService->getEmployeePersonalInfo($user['employee_id']);
        $employee = json_decode($employeeResponse->getData(), true);

        $user['employee'] = $employee;

        return $user;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/calendar_data",
     *     summary="Get data for employee calendar",
     *     description="Get shifts and leaves for employee calendar",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCalendarData(
        Request $request,
        EssEmployeeRequestRequestService $employeeRequestRequestService,
        PayrollGroupRequestService $payrollGroupRequestService,
        EssPayrollGroupAuthorizationService $essPayrollGroupAuthorizationService
    ) {
        $user = $this->getFirstEmployeeUser($request);
        $employeeId = $user['employee_id'];

        $response = [];

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->essShiftsAuthorizationService->authorizeGet($user['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        // get all id's of employee and team members
        $ids = [$employeeId];
        
        // Set Key; formula = 'calendar-data:employee_id:' + $employeeId
        $cacheKey = 'calendar-data:employee_id:' . $employeeId;

        // Get leaves from cache, if none, fetch new data and set to cache
        if (Redis::exists($cacheKey)) {
            $cache = Redis::hGetAll($cacheKey);
            $leaves = json_decode($cache['leaves'], true);
            $shifts = json_decode($cache['shifts'], true);
        } else {
            // Fetch new data from microservice
            $leavesResponse = $employeeRequestRequestService->getLeaves(['employee_ids' => $ids]);
            $leaves = json_decode($leavesResponse->getData(), true);

            // get shifts
            $shiftsResponse = $this->shiftRequestService->getFilteredShifts(['employee_ids' => $ids]);
            $shifts = json_decode($shiftsResponse->getData(), true);
            
            // Save data to cache
            Redis::hMSet(
                $cacheKey,
                [
                    'leaves'=>$leavesResponse->getData(),
                    'shifts'=>$shiftsResponse->getData()
                ]
            );
            // Set life of cache to maximum of 15 days
            Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
        }

        $response['employee_leaves'] = collect($leaves['data'])->where('employee_id', $employeeId)->values()->all();
        $response['employee_shifts'] = collect($shifts['data'])->where('employee_id', $employeeId)->values()->all();

        if (
            !$this->isAuthzEnabled($request) &&
            $essPayrollGroupAuthorizationService->authorizeView($user['user_id'])
        ) {
            // get paydates
            $payrollGroup =
                json_decode($payrollGroupRequestService->getEmployeePayrollGroup($employeeId)->getData());

            if ($payrollGroup) {
                $response['employee_pay_dates'] =
                    $payrollGroupRequestService->getPayDates($payrollGroup->payroll_group, $employeeId);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/ess/employee/count",
     *     summary="Get count of affected entities",
     *     description="Get count of affected entities
using department, location, position, employee or user id",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/FilterBody")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="FilterBody",
     *     required={"affected_employees"},
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/Filter"
     *         )
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="Filter",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Possible values: department,position,location,employee"
     *     )
     * )
     */
    public function affectedEmployeesCount(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        //authorize
        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeGet($userId)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getEntitledEmployees(
            $request->input('affected_employees', []),
            $user['employee_company_id']
        );
        $employeesData = json_decode($response->getData(), true);
        $employees = $employeesData['data'];

        $directUsersIds = collect($request->input('affected_employees', []))->where('type', 'user')->pluck('id');
        $userIds = collect($employees)
            ->pluck('user_id')
            ->merge($directUsersIds)
            ->filter(function ($id) use ($userId) {
                return !empty($id) && $id !== $userId;
            })
            ->unique();

        return [
            'count' => $userIds->count()
        ];
    }

    /**
     * @SWG\Post(
     *     path="/ess/change_password",
     *     summary="Set new employee user password",
     *     description="Set new employee user password",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="new_password",
     *         in="formData",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="new_password_confirmation",
     *         in="formData",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
     *         description="Invalid request",
     *     )
     * )
     */
    public function changePassword(
        Request $request,
        EssPasswordChangeValidator $validator,
        Auth0UserService $auth0UserService,
        EssNotificationRequestService $essNotificationRequestService
    ) {
        $input = $request->all();
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeGet($userId)) {
            $this->response()->errorUnauthorized();
        }

        $errors = $validator->validate($input);
        if ($errors) {
            return $this
                ->response
                ->array($errors)
                ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
        }

        try {
            $response = $auth0UserService->changeEmployeePassword($user['employee_id'], $input['new_password']);
            $essNotificationRequestService->sendEmailNotificationToUser($response);
        } catch (\Exception $e) {
            return $this->response->array([
                'error' => $e->getMessage(),
                'password_changed' => false,
            ])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->response->array([
            'password_changed' => true,
        ]);
    }
}
