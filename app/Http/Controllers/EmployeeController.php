<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Authn\AuthnService;
use App\Model\Auth0User;
use App\Model\AuthnUser;
use App\CSV\CsvValidator;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Audit\AuditCacheItem;
use App\Role\UserRoleService;
use App\Auth0\Auth0UserService;
use Bschmitt\Amqp\Facades\Amqp;
use App\Role\DefaultRoleService;
use App\CSV\CsvValidatorException;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeUploadTask;
use App\Auth0\Auth0ManagementService;
use App\CSV\PersonalInfoCsvValidator;
use App\Employee\EmployeeAuditService;
use App\CompanyUser\CompanyUserService;
use App\ProductSeat\ProductSeatService;
use App\Employee\EmployeeRequestService;
use App\Employee\EmployeeUpdateUploadTask;
use App\Employee\EmployeeESIndexQueueService;
use App\Employee\EmployeeUploadTaskException;
use App\Employee\EmployeeAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Jobs\JobsRequestService;
use App\Http\Controllers\Concerns\ProcessesEmployeeEmailUpdatesTrait;
use App\Http\Controllers\Concerns\ProcessesEmployeeActiveStatusUpdatesTrait;
use App\User\UserRequestService;
use App\Authz\AuthzDataScope;
use App\User\UserAuditService;
use Illuminate\Support\Facades\Redis;
use App\Cache\FragmentedRedisCacheService;
use App\User\UserService;
use App\Account\AccountRequestService;
use App\Authentication\AuthenticationRequestService;
use App\Role\RoleRequestService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Traits\AuditTrailTrait;
use App\Authn\AuthnRequestService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class EmployeeController extends Controller
{
    use AuditTrailTrait;

    use ProcessesEmployeeEmailUpdatesTrait,
        ProcessesEmployeeActiveStatusUpdatesTrait;

    const ACTION_CREATE = 'create';
    const OBJECT_NAME = 'employee';
    const CACHE_BUCKET_PREFIX = 'user-status';
    const CACHE_FIELD_PREFIX = 'id';

    /**
     * App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /**
     * App\Employee\EmployeeRequestService
     */
    protected $requestService;

    /**
     * App\Employee\EmployeeAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\ProductSeat\ProductSeatService
     */
    protected $productSeatService;

    /**
     * @var \App\Role\DefaultRoleService
     */
    protected $defaultRoleService;

    /**
     * @var \App\CompanyUser\CompanyUserService
     */
    protected $companyUserService;

    /**
     * @var \App\Cache\FragmentedRedisCacheService
     */
    protected $cacheService;

    /**
     * @var \App\User\UserService
     */
    protected $userService;

    /**
     * @var \App\Auth0\Auth0UserService
     */
    private $auth0UserService;

    /**
     * @var App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * @var App\Role\RoleRequestService
     */
    protected $roleRequestService;

    /**
     * @var App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * @var App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        CsvValidator $csvValidator,
        PhilippineCompanyRequestService $companyRequestService,
        EmployeeRequestService $requestService,
        EmployeeAuthorizationService $authorizationService,
        ProductSeatService $productSeatService,
        DefaultRoleService $defaultRoleService,
        CompanyUserService $companyUserService,
        UserRoleService $userRoleService,
        FragmentedRedisCacheService $cacheService,
        UserService $userService,
        Auth0UserService $auth0UserService,
        AccountRequestService $accountRequestService,
        RoleRequestService $roleRequestService,
        UserRequestService $userRequestService
    ) {
        $this->csvValidator          = $csvValidator;
        $this->companyRequestService = $companyRequestService;
        $this->requestService        = $requestService;
        $this->authorizationService  = $authorizationService;
        $this->productSeatService    = $productSeatService;
        $this->defaultRoleService    = $defaultRoleService;
        $this->companyUserService    = $companyUserService;
        $this->userRoleService       = $userRoleService;
        $this->cacheService          = $cacheService;
        $this->userService           = $userService;
        $this->auth0UserService      = $auth0UserService;
        $this->accountRequestService = $accountRequestService;
        $this->roleRequestService    = $roleRequestService;
        $this->userRequestService    = $userRequestService;
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/personal_info",
     *     summary="Upload Personal Info",
     *     description="Uploads Employee Personal Information

Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadPersonalInfo(
        Request $request,
        PersonalInfoCsvValidator $csvValidator,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // authorize
            $authorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // Get uploader product seat
        $userRequestService = app()->make(UserRequestService::class);
        $uploaderData = $userRequestService->get($createdBy['user_id']);
        $uploaderData = json_decode($uploaderData->getData());
        $uploaderProductSeat = reset($uploaderData->product_seats)->name ?? '';

        if (empty($uploaderProductSeat)) {
            $this->invalidRequestError('Invalid uploader product license');
        }

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId(
            $companyData->account_id,
            array('plan')
        );
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (!empty($customer['subscriptions'][0]['is_expired'])) {
            $this->invalidRequestError('Company owner does not have an active subscription.');
        }

        $subscription = $customer['subscriptions'][0];

        // Get subscription stats
        $response = $subscriptionsRequestService->getCustomerSubscriptionStats($subscription['id']);
        $subscriptionStats = json_decode($response->getData(), true);
        $subscriptionStats = $subscriptionStats['data'] ?? null;

        if (empty($subscriptionStats)) {
            $this->invalidRequestError('Failed to get customer subscription usage statistics.');
        }

        // Get product code of
        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $csvValidator->validate($file, $subscriptionStats, $uploaderProductSeat);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->savePersonalInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUploadTask::PROCESS_PERSONAL,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_validation_queue'), $message);
            $this->audit($request, $companyId, ['Upload Personal Info' => $file]);
            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/people",
     *     summary="Upload People",
     *     description="Uploads Employee Information (personal, payroll & time and attendance)

Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadPeople(
        Request $request,
        PersonalInfoCsvValidator $csvValidator,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');
        $crbacDataScope = [];
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $crbacDataScope = $authzDataScope->getDataScope();
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // authorize
            $authorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // Get uploader product seat
        $userRequestService = app()->make(UserRequestService::class);
        $uploaderData = $userRequestService->get($createdBy['user_id']);
        $uploaderData = json_decode($uploaderData->getData());
        $uploaderProductSeat = reset($uploaderData->product_seats)->name ?? '';

        if (empty($uploaderProductSeat)) {
            $this->invalidRequestError('Invalid uploader product license');
        }

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId(
            $companyData->account_id,
            array('plan')
        );
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (!empty($customer['subscriptions'][0]['is_expired'])) {
            $this->invalidRequestError('Company owner does not have an active subscription.');
        }

        $subscription = $customer['subscriptions'][0];

        // Get subscription stats
        $response = $subscriptionsRequestService->getCustomerSubscriptionStats($subscription['id']);
        $subscriptionStats = json_decode($response->getData(), true);
        $subscriptionStats = $subscriptionStats['data'] ?? null;

        if (empty($subscriptionStats)) {
            $this->invalidRequestError('Failed to get customer subscription usage statistics.');
        }

        // Get product code of
        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $csvValidator->validate($file, $subscriptionStats, $uploaderProductSeat);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->savePeopleInfo($file->getPathName());
            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUploadTask::PROCESS_PEOPLE,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $crbacDataScope,
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.people_validation_queue'), $message);

            try {
                $this->audit($request, $companyId, ['Mass Add People Trigger']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/payroll_info",
     *     summary="Upload Payroll Info",
     *     description="Uploads Employee Payroll Information

Authorization Scope : **create.employee**",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadPayrollInfo(
        Request $request,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');
        $crbacDataScope = [];
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $crbacDataScope = $authzDataScope->getDataScope();
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // authorize
            $authorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // check company exists (will throw exception if company doesn't exist)
        $this->companyRequestService->get($companyId);

        // Get uploader product seat
        $userRequestService = app()->make(UserRequestService::class);
        $uploaderData = $userRequestService->get($createdBy['user_id']);
        $uploaderData = json_decode($uploaderData->getData());
        $uploaderProductSeat = reset($uploaderData->product_seats)->name ?? '';

        if (empty($uploaderProductSeat)) {
            $this->invalidRequestError('Invalid uploader product license');
        }

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId(
            $companyData->account_id,
            array('plan')
        );
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (!empty($customer['subscriptions'][0]['is_expired'])) {
            $this->invalidRequestError('Company owner does not have an active subscription.');
        }

        $subscription = $customer['subscriptions'][0];

        // Get subscription stats
        $response = $subscriptionsRequestService->getCustomerSubscriptionStats($subscription['id']);
        $subscriptionStats = json_decode($response->getData(), true);
        $subscriptionStats = $subscriptionStats['data'] ?? null;

        if (empty($subscriptionStats)) {
            $this->invalidRequestError('Failed to get customer subscription usage statistics.');
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file, $subscriptionStats, $uploaderProductSeat);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->savePayrollInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUploadTask::PROCESS_PAYROLL,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $crbacDataScope,
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_validation_queue'), $message);
            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/save",
     *     summary="Save Employee Info (personal & payroll)",
     *     description="Saves Employee Information from Previously Uploaded CSVs (personal & payroll)

Authorization Scope : **create.employee**",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll information needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.people_write_queue'), $message);
        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);

        $this->audit($request, $companyId, ['Mass Add People Trigger Save' => $uploadTask->getId()]);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/time_attendance_info",
     *     summary="Upload Time and Attendance Info",
     *     description="Uploads Employee Time and Attendance Information

Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadTimeAttendanceInfo(
        Request $request,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');
        $crbacDataScope = [];
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $crbacDataScope = $authzDataScope->getDataScope();
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // authorize
            $authorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        // check company exists (will throw exception if company doesn't exist)
        $this->companyRequestService->get($companyId);

        // Get uploader product seat
        $userRequestService = app()->make(UserRequestService::class);
        $uploaderData = $userRequestService->get($createdBy['user_id']);
        $uploaderData = json_decode($uploaderData->getData());
        $uploaderProductSeat = reset($uploaderData->product_seats)->name ?? '';

        if (empty($uploaderProductSeat)) {
            $this->invalidRequestError('Invalid uploader product license');
        }

        // Get customer subscription by account id
        $response = $subscriptionsRequestService->getCustomerByAccountId(
            $companyData->account_id,
            array('plan')
        );
        $customer = json_decode($response->getData(), true);
        $customer = current($customer['data']);

        if (!empty($customer['subscriptions'][0]['is_expired'])) {
            $this->invalidRequestError('Company owner does not have an active subscription.');
        }

        $subscription = $customer['subscriptions'][0];

        // Get subscription stats
        $response = $subscriptionsRequestService->getCustomerSubscriptionStats($subscription['id']);
        $subscriptionStats = json_decode($response->getData(), true);
        $subscriptionStats = $subscriptionStats['data'] ?? null;

        if (empty($subscriptionStats)) {
            $this->invalidRequestError('Failed to get customer subscription usage statistics.');
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file, $subscriptionStats, $uploaderProductSeat);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveTimeAttendanceInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUploadTask::PROCESS_TIME_ATTENDANCE,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $crbacDataScope,
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_time_attendance_validation_queue'), $message);
            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);
            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_add/ta_save",
     *     summary="Save Employee Info (personal & time attendance)",
     *     description="Saves Employee Information from Previously Uploaded CSVs (personal & time attendance)
     *
Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function uploadTaSave(Request $request)
    {
        $companyId = $request->input('company_id');
        $createdBy = $request->attributes->get('user');
        if (
            $this->isAuthzEnabled($request)
            && !$this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)
        ) {
            $this->response()->errorUnauthorized();
        }
        $companyResponse = $this->companyRequestService->get($companyId);
        if (!$this->isAuthzEnabled($request)) {
            $companyData = json_decode($companyResponse->getData());
            if (!$this->authorizationService->authorizeCreate($companyData, $createdBy)) {
                $this->response()->errorUnauthorized();
            }
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateTaSaveStatus(EmployeeUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.employee_time_attendance_write_queue'), $message);
        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);
        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/employee/batch_add/status",
     *     summary="Get Job Status",
     *     description="Get Employee Upload Status

Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\Employee\EmployeeUploadTask::PROCESS_PERSONAL,
     *              App\Employee\EmployeeUploadTask::PROCESS_PAYROLL,
     *              App\Employee\EmployeeUploadTask::PROCESS_TIME_ATTENDANCE,
     *              App\Employee\EmployeeUploadTask::PROCESS_SAVE,
     *              App\Employee\EmployeeUploadTask::PROCESS_TA_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Email is invalid",
     *                              "Payroll Group does not exist"
     *                          },
     *                          "4": {
     *                              "Email is invalid",
     *                              "Payroll Group does not exist"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        $isAuthorized = false;

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, EmployeeUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(',', EmployeeUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === EmployeeUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === EmployeeUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }
        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/employee/batch_add/preview",
     *     summary="Get Employee Preview",
     *     description="Get Employee Upload Preview

Authorization Scope : **create.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        $isAuthorized = false;

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(EmployeeUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);

        try {
            $responseData = json_decode($response->getData());
            $this->audit($request, $companyId, ['Mass Add People Preview' => $responseData]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/employee/batch_add/step_status_list",
     *     summary="Get List of Step Statuses",
     *     description="List of Possible Employee Upload Step Statuses",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="personal", type="array",  items={"type"="string"})
     *         ),
     *         examples={
     *              "application/json": {
     *                  "personal": {
     *                      "validating",
     *                      "validated"
     *                  },
     *                  "payroll": {
     *                      "validating",
     *                      "validated"
     *                  },
     *                  "time_attendance": {
     *                      "validating",
     *                      "validated"
     *                  },
     *                  "save": {
     *                      "validating",
     *                      "validated"
     *                  }
     *              }
     *         }
     *     )
     * )
     */
    public function createUploadStatuses()
    {
        return response()->json([
            'personal' => EmployeeUploadTask::VALIDATION_STATUSES,
            'payroll' => EmployeeUploadTask::VALIDATION_STATUSES,
            'time_attendance' => EmployeeUploadTask::VALIDATION_STATUSES,
            'save' => EmployeeUploadTask::SAVE_STATUSES
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employees",
     *     summary="Get company employees",
     *     description="Get list of employees within a certain company

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include additional data to response",
     *         required=false,
     *         type="string",
     *         enum={"payroll"}
     *     ),
     *     @SWG\Parameter(
     *         name="filter[employee_ids][]",
     *         type="array",
     *         in="query",
     *         description="Employee Ids",
     *         required=false,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyEmployees($id, Request $request)
    {
        $this->validate($request, [
            'filter' => 'array',
            'filter.employee_ids' => 'array',
            'filter.employee_ids.*' => 'integer',
        ]);

        $queryData = $request->query();
        $companyResponse = $this->companyRequestService->get($id);
        $companyData = json_decode($companyResponse->getData());
        $viewer = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        $dataScope = [];
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyData->id);
            $dataScope = $dataScope->getDataScope();
        } else {
            $isAuthorized = $this->authorizationService->authorizeViewCompanyEmployees($companyData, $viewer);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyEmployees($id, $queryData, (array) $dataScope);
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/inactive_employees",
     *     summary="Get company inactive employees",
     *     description="Get list of inactive employees within a certain company

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="request",
     *         in="body",
     *         type="array",
     *         description="Request body with affected entities the inactive employees are connected to.",
     *         required=true,
     *         @SWG\Schema(
     *             ref="#/definitions/affected_employees"
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function getCompanyInactiveEmployees($id, Request $request)
    {
        $authorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $authorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $companyResponse = $this->companyRequestService->get($id);
            $companyData = json_decode($companyResponse->getData());
            $viewer = $request->attributes->get('user');
            $authorized = $this->authorizationService->authorizeViewCompanyEmployees($companyData, $viewer);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyInactiveEmployees($id, $request->all(), $authzDataScope);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employees/search",
     *     summary="Get company employees filtered by name or ID",
     *     description="Get list of employees within a certain company filtered by name or ID

    Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="query",
     *         description="Search employee_id",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyEmployeesFilteredByNameOrId($id, Request $request)
    {
        $companyResponse = $this->companyRequestService->get($id);
        $companyData = json_decode($companyResponse->getData());
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeViewCompanyEmployees($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyEmployeesFilteredByNameOrId($id, $request->all());
        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/ta_employees",
     *     summary="Get company employees with time and attendance",
     *     description="Get list of employees with time and attendance within a certain company

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include",
     *         in="query",
     *         description="Include additional data to response",
     *         required=false,
     *         type="string",
     *         enum={"payroll"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyTaEmployees(Request $request, $companyId)
    {
        $queryData = $request->query();
        $isAuthorized = false;
        $dataScope = null;

        if ($this->isAuthzEnabled($request)) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $dataScope = $this->getAuthzDataScope($request);

            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeViewCompanyEmployees(
                $companyData,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyTaEmployees($companyId, $queryData, $dataScope);
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/ta_employees/id",
     *     summary="Get company T&A employees by IDs",
     *     description="Get company T&A employees using the given ids",
     *     tags={"employee"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="values",
     *         in="formData",
     *         description="ids of employees",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="pagination",
     *         in="formData",
     *         description="pagination and filter data",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="response_size",
     *         in="formData",
     *         description="Response size",
     *         type="string",
     *         enum={"full", "minimal"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyTaEmployeesByIds($id, Request $request)
    {
        $inputs = $request->all();

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $companyResponse = $this->companyRequestService->get($id);
            $companyData = json_decode($companyResponse->getData());
            $user = $request->attributes->get('user');
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyTaEmployeesByIds(
            $id,
            $inputs
        );
    }

    /**
     * @SWG\Get(
     *     path="/employee/{id}",
     *     summary="Get employee",
     *     description="Get employee with T&A, Payroll or both details. Fetches the full Employee
201 details.

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         description="The JSON Web Token of the user provided upon login:
Bearer (token)",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         description="",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID of the employee",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation.",
     *         examples={
     *              "Successfully found the Employee": {
     *                  "data": {
     *                      "id": 271,
     *                      "account_id": 9,
     *                      "company_id": 22,
     *                      "employee_id": "pr_10232019-12",
     *                      "first_name": "Exo",
     *                      "middle_name": "",
     *                      "last_name": "Kpop",
     *                      "full_name": "Exo Kpop",
     *                      "email": "test+local.exokpoppr_10232019-12@salarium.com",
     *                      "gender": "female",
     *                      "birth_date": "1984-10-15",
     *                      "telephone_number": 4425159,
     *                      "mobile_number": "(63)9954132492",
     *                      "hours_per_day": null,
     *                      "date_hired": "2010-10-16",
     *                      "date_ended": "2020-01-31",
     *                      "location_id": 5,
     *                      "location_name": "Makati",
     *                      "department_id": 7,
     *                      "department_name": "Product",
     *                      "rank_id": 7,
     *                      "rank_name": "Junior. Quality Assurance Analyst",
     *                      "position_id": 7,
     *                      "position_name": "QA",
     *                      "address": {
     *                          "address_line_1": "1st  Street",
     *                          "address_line_2": "Brgy 12",
     *                          "city": "Baliuag",
     *                          "zip_code": "14501",
     *                          "country": "Philippines"
     *                      },
     *                      "active": false,
     *                      "non_taxed_other_income_limit": "90000.0000",
     *                      "user_id": 146,
     *                      "payroll_group": {
     *                          "id": 24,
     *                          "name": "pg1 semi monthly",
     *                          "company_id": 22,
     *                          "payroll_frequency": "SEMI_MONTHLY",
     *                          "day_factor": "260.00",
     *                          "hours_per_day": "8.00",
     *                          "non_working_day_option": "BEFORE",
     *                          "pay_date": "2019-10-15 00:00:00",
     *                          "pay_run_posting": "2019-10-09 00:00:00",
     *                          "cut_off_date": "2019-10-09 00:00:00",
     *                          "pay_date_2": "2019-10-31 00:00:00",
     *                          "pay_run_posting_2": "2019-10-25 00:00:00",
     *                          "cut_off_date_2": "2019-10-25 00:00:00",
     *                          "fpd_end_of_month": false,
     *                          "fpp_end_of_month": false,
     *                          "fcd_end_of_month": false,
     *                          "spd_end_of_month": true,
     *                          "spp_end_of_month": false,
     *                          "scd_end_of_month": false,
     *                          "payroll_group_subtype_id": 24,
     *                          "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
     *                          "compute_overbreak": 0,
     *                          "compute_undertime": 1,
     *                          "compute_tardiness": 1,
     *                          "created_at": "2019-11-08 10:57:27",
     *                          "updated_at": "2019-11-08 13:12:10",
     *                          "deleted_at": null,
     *                          "enforce_gap_loans": false,
     *                          "minimum_net_take_home_pay": null
     *                      },
     *                      "updated_at": {
     *                          "date": "2020-01-09 14:55:11.000000",
     *                          "timezone_type": 3,
     *                          "timezone": "Asia/Manila"
     *                      },
     *                      "termination_information": {
     *                          "id": 2,
     *                          "employee_id": 271,
     *                          "company_id": 22,
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": null,
     *                          "created_at": "2020-01-09 14:55:11",
     *                          "updated_at": "2020-01-09 14:55:11"
     *                      },
     *                      "team_id": null,
     *                      "payroll": {
     *                          "payroll_group_id": 24,
     *                          "payroll_group_name": "pg1 semi monthly",
     *                          "hours_per_day": null,
     *                          "date_hired": "2010-10-16",
     *                          "date_ended": "2020-01-31",
     *                          "cost_center_id": null,
     *                          "cost_center_name": null,
     *                          "tax_status": "S",
     *                          "tax_type": "Minimum Wage",
     *                          "consultant_tax_rate": null,
     *                          "tin": "131-456-789-019",
     *                          "sss_number": "07-7451477-8",
     *                          "philhealth_number": "82-560045513-0",
     *                          "hdmf_number": "4177-3254-4805",
     *                          "sss_basis": "Basic Pay",
     *                          "philhealth_basis": "Basic Pay",
     *                          "hdmf_basis": "Basic Pay",
     *                          "sss_amount": null,
     *                          "philhealth_amount": null,
     *                          "hdmf_amount": null,
     *                          "rdo": "14",
     *                          "entitled_deminimis": 1,
     *                          "payment_method": "Bank",
     *                          "bank_name": "UNION BANK OF THE PHILIPPINES",
     *                          "bank_account_number": "123456789",
     *                          "bank_type": "Current",
     *                          "base_pay": "300,000.00",
     *                          "base_pay_unit": "PER MONTH"
     *                      },
     *                      "final_pay_projection": {
     *                          "input": {
     *                              "payroll_details": {
     *                                  "type": "payroll-settings",
     *                                  "id": "2632",
     *                                  "attributes": {
     *                                      "payrollStartDate": null,
     *                                      "payrollEndDate": null,
     *                                      "attendanceStartDate": null,
     *                                      "attendanceEndDate": null,
     *                                      "postingDate": "0000-00-00",
     *                                      "payRunDate": "2019-12-10",
     *                                      "employeeIds": {
     *                                          17627
     *                                      },
     *                                      "inactiveEmployeeIds": {
     *                                          17627
     *                                      },
     *                                      "payrollJobs": {
     *                                          "ATTENDANCE": {
     *                                              "job_id": "0eace677-d427-4c45-8d15-795449017349",
     *                                              "job_file": "12-03-2019_Final_Pay_Valid_emp-17627-524-1490-157594481331585def026d4d1c8372756496.csv"
     *                                          },
     *                                          "CALCULATION": {
     *                                              "job_id": "e22c9f4c-c4b9-4389-8e80-f30c959a7f6a",
     *                                              "job_file": ""
     *                                          }
     *                                      },
     *                                      "annualize": 1,
     *                                      "payrollGroupInfo": {
     *                                          "id": "",
     *                                          "accountId": "",
     *                                          "companyId": "",
     *                                          "payrollGroupName": null,
     *                                          "payrollFrequency": null,
     *                                          "dayFactor": 0,
     *                                          "cutoffDate": null,
     *                                          "cutoffDateEom": null,
     *                                          "cutoffDate2": null,
     *                                          "cutoffDate2Eom": null,
     *                                          "postingDate": null,
     *                                          "postingDateEom": null,
     *                                          "postingDate2": null,
     *                                          "postingDate2Eom": null,
     *                                          "hoursPerDay": 0,
     *                                          "contributionSchedules": {
     *                                              "SSS": null,
     *                                              "HDMF": null,
     *                                              "PHILHEALTH": null
     *                                          },
     *                                          "wtaxSchedule": null,
     *                                          "wtaxMethod": null,
     *                                          "gapLoans": {
     *                                              "enforce": null,
     *                                              "minimumNetTakeHomePay": 0
     *                                          },
     *                                          "computeOverbreak": null,
     *                                          "computeUndertime": null,
     *                                          "computeTardiness": null
     *                                      },
     *                                      "dayHourRates": {
     *                                          "REGULAR": 1,
     *                                          "PAID_LEAVE": 1,
     *                                          "UNPAID_LEAVE": 1,
     *                                          "SH": 1.3,
     *                                          "RH": 2,
     *                                          "2RH": 3,
     *                                          "NT": 1.1,
     *                                          "SH+NT": 1.43,
     *                                          "RH+NT": 2.2,
     *                                          "2RH+NT": 3.3,
     *                                          "OT": 1.25,
     *                                          "SH+OT": 1.69,
     *                                          "RH+OT": 2.6,
     *                                          "2RH+OT": 3.9,
     *                                          "NT+OT": 1.375,
     *                                          "SH+NT+OT": 1.859,
     *                                          "RH+NT+OT": 2.86,
     *                                          "2RH+NT+OT": 4.29,
     *                                          "RD": 1.3,
     *                                          "RD+SH": 1.5,
     *                                          "RD+RH": 2.6,
     *                                          "RD+2RH": 3.9,
     *                                          "RD+NT": 1.43,
     *                                          "RD+SH+NT": 1.65,
     *                                          "RD+RH+NT": 2.86,
     *                                          "RD+2RH+NT": 4.29,
     *                                          "RD+OT": 1.69,
     *                                          "RD+SH+OT": 1.95,
     *                                          "RD+RH+OT": 3.38,
     *                                          "RD+2RH+OT": 5.07,
     *                                          "RD+NT+OT": 1.859,
     *                                          "RD+SH+NT+OT": 2.145,
     *                                          "RD+RH+NT+OT": 3.718,
     *                                          "RD+2RH+NT+OT": 5.577,
     *                                          "RH+SH": 2.6,
     *                                          "RH+SH+NT": 2.86,
     *                                          "RD+RH+SH": 3,
     *                                          "RH+SH+OT": 3.38,
     *                                          "RH+SH+NT+OT": 3.718,
     *                                          "RD+RH+SH+NT": 3.3,
     *                                          "RD+RH+SH+OT": 3.9,
     *                                          "RD+RH+SH+NT+OT": 4.29
     *                                      },
     *                                      "employees": {
     *                                          {
     *                                              "id": 17627,
     *                                              "tax_status": "S",
     *                                              "tax_type": "Regular",
     *                                              "consultant_tax_rate": null,
     *                                              "tin": "131-456-789-019",
     *                                              "sss_number": "07-7451477-8",
     *                                              "philhealth_number": "82-560045513-0",
     *                                              "hdmf_number": "4177-3254-4805",
     *                                              "sss_basis": "No Contribution",
     *                                              "philhealth_basis": "No Contribution",
     *                                              "hdmf_basis": "No Contribution",
     *                                              "sss_amount": null,
     *                                              "philhealth_amount": null,
     *                                              "hdmf_amount": null,
     *                                              "rdo": "140",
     *                                              "created_at": "2019-11-13 10:52:53",
     *                                              "updated_at": "2019-12-04 16:19:59",
     *                                              "entitled_deminimis": 1,
     *                                              "payment_method": "Bank",
     *                                              "bank_name": "UNION BANK OF THE PHILIPPINES",
     *                                              "bank_type": "Current",
     *                                              "bank_account_number": "123456789",
     *                                              "company_id": 711,
     *                                              "payroll_group_id": 4277,
     *                                              "employee_id": "NV13-0005",
     *                                              "first_name": "Semi-Regular-PYear",
     *                                              "middle_name": "",
     *                                              "last_name": "Kpop",
     *                                              "email": "tapayroll01+NV13-0005@gmail.com",
     *                                              "gender": "female",
     *                                              "birth_date": "1984-10-15",
     *                                              "telephone_number": 4425159,
     *                                              "mobile_number": "(63)9954132492",
     *                                              "hours_per_day": null,
     *                                              "date_hired": "2019-01-01",
     *                                              "date_ended": "2019-11-08",
     *                                              "location_id": 631,
     *                                              "department_id": 1391,
     *                                              "rank_id": 1134,
     *                                              "cost_center_id": null,
     *                                              "employee_subtype_id": 11304,
     *                                              "employee_subtype_type": "App\\Model\\PhilippineEmployee",
     *                                              "country": "Philippines",
     *                                              "region": null,
     *                                              "city": "Baliuag",
     *                                              "zip_code": "14501",
     *                                              "first_address_line": "1st  Street",
     *                                              "second_address_line": "Brgy 12",
     *                                              "position_id": 10660,
     *                                              "non_taxed_other_income_limit": "90000.0000",
     *                                              "active": "semi-active",
     *                                              "payroll_group_name": "2328_Semi_Monthly",
     *                                              "payroll_group_frequency": "SEMI_MONTHLY",
     *                                              "department_name": "Department Parent1556085724",
     *                                              "rank_name": "Junior. Quality Assurance Analyst",
     *                                              "cost_center_name": null,
     *                                              "position_name": "QA",
     *                                              "location_name": "salqajeff_location",
     *                                              "user": {
     *                                                  "id": 23447,
     *                                                  "account_id": 524,
     *                                                  "last_active_company_id": null,
     *                                                  "first_name": "Semi-Regular-PYear",
     *                                                  "middle_name": "",
     *                                                  "last_name": "Kpop",
     *                                                  "email": "tapayroll01+NV13-0005@gmail.com",
     *                                                  "user_type": "employee",
     *                                                  "created_at": "2019-11-13 10:52:53",
     *                                                  "updated_at": "2019-11-28 14:30:44",
     *                                                  "deleted_at": null,
     *                                                  "status": "semi-active",
     *                                                  "suspended_inactive": 0,
     *                                                  "pivot": {
     *                                                      "employee_id": 17627,
     *                                                      "user_id": 23447
     *                                                  }
     *                                              },
     *                                              "account_id": 524,
     *                                              "termination_information": {
     *                                                  "id": 470,
     *                                                  "employee_id": 17627,
     *                                                  "company_id": 711,
     *                                                  "last_date": "2019-11-08",
     *                                                  "start_date_basis": "2019-10-28",
     *                                                  "release_date": "2019-11-11",
     *                                                  "reason_for_leaving": "TERMINATION",
     *                                                  "remarks": "",
     *                                                  "created_at": "2019-12-04 16:19:58",
     *                                                  "updated_at": "2019-12-04 16:19:58"
     *                                              },
     *                                              "team_id": null
     *                                          }
     *                                      }
     *                                  }
     *                              },
     *                              "employee_details": {
     *                                  "type": "employee",
     *                                  "id": "17627",
     *                                  "attributes": {
     *                                      "accountId": "524",
     *                                      "companyId": "711",
     *                                      "employeeId": "NV13-0005",
     *                                      "firstName": "Semi-Regular-PYear",
     *                                      "middleName": "",
     *                                      "lastName": "Kpop",
     *                                      "email": "tapayroll01+NV13-0005@gmail.com",
     *                                      "gender": "female",
     *                                      "birthDate": "1984-10-15",
     *                                      "telephoneNumber": "4425159",
     *                                      "mobileNumber": "(63)9954132492",
     *                                      "hoursPerDay": 0,
     *                                      "dateHired": "2019-01-01",
     *                                      "dateEnded": "2019-11-08",
     *                                      "locationId": "631",
     *                                      "locationName": "salqajeff_location",
     *                                      "departmentId": "1391",
     *                                      "departmentName": "Department Parent1556085724",
     *                                      "rankId": "1134",
     *                                      "rankName": "Junior. Quality Assurance Analyst",
     *                                      "costCenterId": "",
     *                                      "costCenterName": null,
     *                                      "positionId": "10660",
     *                                      "positionName": "QA",
     *                                      "address": "Brgy 12 1st  Street",
     *                                      "city": "Baliuag",
     *                                      "zipCode": "14501",
     *                                      "country": "Philippines",
     *                                      "taxStatus": "S",
     *                                      "taxType": "Regular",
     *                                      "taxRate": 0,
     *                                      "tin": "131-456-789-019",
     *                                      "sssNumber": "07-7451477-8",
     *                                      "philhealthNumber": "82-560045513-0",
     *                                      "hdmfNumber": "4177-3254-4805",
     *                                      "rdo": "140",
     *                                      "active": "semi-active",
     *                                      "nonTaxedOtherIncomeLimit": "90000.0000",
     *                                      "payrollGroup": {
     *                                          "id": 4277,
     *                                          "name": "2328_Semi_Monthly",
     *                                          "company_id": 711,
     *                                          "payroll_frequency": "SEMI_MONTHLY",
     *                                          "day_factor": "260.00",
     *                                          "hours_per_day": "8.00",
     *                                          "non_working_day_option": "BEFORE",
     *                                          "pay_date": "2019-06-15 00:00:00",
     *                                          "pay_run_posting": "2019-06-10 00:00:00",
     *                                          "cut_off_date": "2019-06-10 00:00:00",
     *                                          "pay_date_2": "2019-06-30 00:00:00",
     *                                          "pay_run_posting_2": "2019-06-25 00:00:00",
     *                                          "cut_off_date_2": "2019-06-25 00:00:00",
     *                                          "fpd_end_of_month": false,
     *                                          "fpp_end_of_month": false,
     *                                          "fcd_end_of_month": false,
     *                                          "spd_end_of_month": false,
     *                                          "spp_end_of_month": false,
     *                                          "scd_end_of_month": false,
     *                                          "payroll_group_subtype_id": 4277,
     *                                          "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
     *                                          "compute_overbreak": false,
     *                                          "compute_undertime": true,
     *                                          "compute_tardiness": true,
     *                                          "created_at": "2019-08-20 13:22:35",
     *                                          "updated_at": "2019-08-20 13:22:35",
     *                                          "deleted_at": null,
     *                                          "enforce_gap_loans": true,
     *                                          "minimum_net_take_home_pay": "2000.0000",
     *                                          "contributionSchedules": {
     *                                              "SSS": "EVERY_PAY",
     *                                              "HDMF": "EVERY_PAY",
     *                                              "PHILHEALTH": "EVERY_PAY"
     *                                          }
     *                                      },
     *                                      "contributionBasis": {
     *                                          "SSS": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          },
     *                                          "PHILHEALTH": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          },
     *                                          "HDMF": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          }
     *                                      },
     *                                      "terminationInformation": {
     *                                          "id": 470,
     *                                          "type": "termination-information",
     *                                          "attributes": {
     *                                              "employee_id": 17627,
     *                                              "company_id": 711,
     *                                              "last_date": "2019-11-08",
     *                                              "start_date_basis": "2019-10-28",
     *                                              "release_date": "2019-11-11",
     *                                              "reason_for_leaving": "TERMINATION",
     *                                              "remarks": ""
     *                                          }
     *                                      },
     *                                      "finalPayId": 210,
     *                                      "finalPayItems": {
     *                                          {
     *                                              "id": 1951,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3232,
     *                                              "type": "ALLOWANCE",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "release_id": 3232,
     *                                                  "name": "final_pay_allowance5 non tax",
     *                                                  "other_income_id": 3082,
     *                                                  "amount": 5000,
     *                                                  "release_date": "2019-11-10"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1952,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3192,
     *                                              "type": "ADJUSTMENT",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3042,
     *                                                  "reason": "For Qa Testing",
     *                                                  "release_date": "2019-11-15",
     *                                                  "include": true,
     *                                                  "name": "TAXABLE_INCOME",
     *                                                  "release_id": 3192,
     *                                                  "amount": 500
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1953,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 4094,
     *                                              "type": "BONUS",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 4255,
     *                                                  "release_date": "2019-12-02",
     *                                                  "include": true,
     *                                                  "name": "Payroll Group Bonus Periodic",
     *                                                  "amount": 0,
     *                                                  "percentage": 100,
     *                                                  "type": "SALARY_BASED",
     *                                                  "frequency": "PERIODIC",
     *                                                  "basis": "BASIC_PAY"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1954,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3217,
     *                                              "type": "COMMISSION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "other_income_id": 3067,
     *                                                  "amount": 3000,
     *                                                  "release_date": "2019-10-10"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1955,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3223,
     *                                              "type": "COMMISSION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "other_income_id": 3073,
     *                                                  "amount": 3000,
     *                                                  "release_date": "2019-10-25"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1956,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3930,
     *                                              "type": "DEDUCTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3930,
     *                                                  "release_date": "2019-03-11",
     *                                                  "include": true,
     *                                                  "name": "Mobile Plans 83",
     *                                                  "valid_from": "2019-03-11",
     *                                                  "amount": 1500,
     *                                                  "valid_to": null
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1957,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3973,
     *                                              "type": "DEDUCTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3973,
     *                                                  "release_date": "2019-05-11",
     *                                                  "include": true,
     *                                                  "name": "Mobile Plans 88",
     *                                                  "valid_from": "2019-05-11",
     *                                                  "amount": 1500,
     *                                                  "valid_to": null
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1958,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 326,
     *                                              "type": "LOAN",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "is_full": false,
     *                                                  "include": true,
     *                                                  "loan_id": 326,
     *                                                  "name": "Final_Pay_Car-Loan",
     *                                                  "monthly_amortization": "3,050.00",
     *                                                  "remaining_balance": "6,000.00",
     *                                                  "repayment_date": "2019-12-30",
     *                                                  "category": ""
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1959,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 1820,
     *                                              "type": "LOAN",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "is_full": false,
     *                                                  "include": true,
     *                                                  "loan_id": 1820,
     *                                                  "name": "SSS",
     *                                                  "monthly_amortization": "1,500.00",
     *                                                  "remaining_balance": "10,000.00",
     *                                                  "repayment_date": "2019-10-30",
     *                                                  "category": "Salary"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1960,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -1,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "SSS",
     *                                                  "type": "SSS",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1961,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -2,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "Pag-Ibig",
     *                                                  "type": "HDMF",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1962,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -3,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "Philhealth",
     *                                                  "type": "PHILHEALTH",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          }
     *                                      },
     *                                      "basePay": {
     *                                          {
     *                                              "id": "17735",
     *                                              "type": "base_pay",
     *                                              "attributes": {
     *                                                  "amount": "400000.00000000",
     *                                                  "unit": "YEARLY",
     *                                                  "effectiveDate": "2019-01-01"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": "19019",
     *                                              "type": "base_pay",
     *                                              "attributes": {
     *                                                  "amount": "945000.00000000",
     *                                                  "unit": "YEARLY",
     *                                                  "effectiveDate": "2019-11-19"
     *                                              }
     *                                          }
     *                                      },
     *                                      "aabcd": {
     *                                          "adjustment": {
     *                                              {
     *                                                  "type": "adjustment",
     *                                                  "name": "For Qa Testing",
     *                                                  "reason": "For Qa Testing",
     *                                                  "category": "TAXABLE_INCOME",
     *                                                  "fully_taxable": false,
     *                                                  "max_non_taxable": 0,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3192,
     *                                                          "date": "2019-11-15",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3042,
     *                                                      "subtype_id": 247,
     *                                                      "subtype_name": "PhilippineAdjustment",
     *                                                      "other_income_type_id": 3,
     *                                                      "other_income_type_subtype_type_id": 1
     *                                                  }
     *                                              }
     *                                          },
     *                                          "commission": {
     *                                              {
     *                                                  "type": "commission",
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 3000
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3217,
     *                                                          "date": "2019-10-10",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3067,
     *                                                      "subtype_id": 299,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "type": "commission",
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 3000
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3223,
     *                                                          "date": "2019-10-25",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3073,
     *                                                      "subtype_id": 305,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              }
     *                                          },
     *                                          "allowance": {
     *                                              {
     *                                                  "type": "allowance",
     *                                                  "name": "final_pay_allowance5 non tax",
     *                                                  "fully_taxable": false,
     *                                                  "max_non_taxable": 3000,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 5000,
     *                                                      "prorated": 0,
     *                                                      "prorated_by": null,
     *                                                      "entitled_when": null
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-11-10",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3232,
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3082,
     *                                                      "subtype_id": 1104,
     *                                                      "subtype_name": "PhilippineAllowance",
     *                                                      "other_income_type_id": 13905,
     *                                                      "other_income_type_subtype_type_id": 1738,
     *                                                      "basis_amount": 5000,
     *                                                      "given_amount": 5000,
     *                                                      "deducted_amount": 0,
     *                                                      "max_non_taxable": 3000,
     *                                                      "taxable_split": 2000
     *                                                  }
     *                                              }
     *                                          },
     *                                          "bonus": {
     *                                              {
     *                                                  "type": "bonus",
     *                                                  "name": "Payroll Group Bonus Periodic",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "SALARY_BASED",
     *                                                      "basis": "BASIC_PAY",
     *                                                      "amount": null,
     *                                                      "percentage": 100
     *                                                  },
     *                                                  "frequency_type": "PERIODIC",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 4094,
     *                                                          "date": "2019-12-02",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false,
     *                                                          "prorate_based_on_tenure": 1,
     *                                                          "coverage_from": "2019-01-01",
     *                                                          "coverage_to": "2019-11-30",
     *                                                          "amount": null
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 4255,
     *                                                      "subtype_id": 1301,
     *                                                      "subtype_name": "PhilippineBonus",
     *                                                      "other_income_type_id": 16022,
     *                                                      "other_income_type_subtype_type_id": 8610,
     *                                                      "release_amount": 18375.534188034,
     *                                                      "tax_exempt_amount": 0,
     *                                                      "taxed_amount": 18375.534188034
     *                                                  }
     *                                              }
     *                                          },
     *                                          "deduction": {
     *                                              {
     *                                                  "type": "deduction",
     *                                                  "name": "Mobile Plans 83",
     *                                                  "fully_taxable": null,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 1500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-03-11",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3930,
     *                                                      "subtype_id": 492,
     *                                                      "subtype_name": "PhilippineDeduction",
     *                                                      "other_income_type_id": 15347,
     *                                                      "other_income_type_subtype_type_id": 1771
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "type": "deduction",
     *                                                  "name": "Mobile Plans 88",
     *                                                  "fully_taxable": null,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 1500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-05-11",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3973,
     *                                                      "subtype_id": 504,
     *                                                      "subtype_name": "PhilippineDeduction",
     *                                                      "other_income_type_id": 15390,
     *                                                      "other_income_type_subtype_type_id": 1783
     *                                                  }
     *                                              }
     *                                          },
     *                                          "salaryBasis": {
     *                                              "grossIncome": {
     *                                                  {
     *                                                      "amount": 221789.01282051,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 119103.06410256,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 155210.26923077,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 270734.13141026,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 257171.91346154,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 163403.92307692,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 75857.435897436,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 37761.282051282,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "grossBasic": {
     *                                                  {
     *                                                      "amount": 218039.01282051,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 115353.06410256,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 151460.26923077,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 266984.13141026,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 253421.91346154,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 159653.92307692,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 72107.435897436,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 34011.282051282,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "basicPay": {
     *                                                  {
     *                                                      "amount": 14551.282051282,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": -1868.5897435897,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 13022.435897436,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16378.205128205,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 12987.179487179,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16442.307692308,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 14743.58974359,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "basePay": {
     *                                                  {
     *                                                      "id": "17735",
     *                                                      "type": "base_pay",
     *                                                      "attributes": {
     *                                                          "amount": "400000.00000000",
     *                                                          "unit": "YEARLY",
     *                                                          "effectiveDate": "2019-01-01"
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "id": "19019",
     *                                                      "type": "base_pay",
     *                                                      "attributes": {
     *                                                          "amount": "945000.00000000",
     *                                                          "unit": "YEARLY",
     *                                                          "effectiveDate": "2019-11-19"
     *                                                      }
     *                                                  }
     *                                              }
     *                                          }
     *                                      },
     *                                      "amortizations": {
     *                                          {
     *                                              "id": 50549,
     *                                              "loanId": 326,
     *                                              "subtype": "",
     *                                              "employerRemarks": null,
     *                                              "principalAmount": 6000,
     *                                              "referenceNumber": "",
     *                                              "loanName": "Final_Pay_Car-Loan",
     *                                              "amountDue": 1525,
     *                                              "dueDate": "2019-12-13"
     *                                          },
     *                                          {
     *                                              "id": 50550,
     *                                              "loanId": 326,
     *                                              "subtype": "",
     *                                              "employerRemarks": null,
     *                                              "principalAmount": 6000,
     *                                              "referenceNumber": "",
     *                                              "loanName": "Final_Pay_Car-Loan",
     *                                              "amountDue": 1425,
     *                                              "dueDate": "2019-12-30"
     *                                          }
     *                                      },
     *                                      "attendanceData": {
     *                                          "2019-10-28": {
     *                                              "SH+NT+OT": 1.4166666666667,
     *                                              "PAID_LEAVE": 8,
     *                                              "TARDY": 2.25,
     *                                              "SH+NT": 1.1666666666667,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+NT+OT": 1.5,
     *                                              "NT": 1,
     *                                              "RH+NT+OT": 1.75,
     *                                              "2RH+NT": 1,
     *                                              "2RH+NT+OT": 1.2,
     *                                              "NT+OT": 2.75,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 1.25,
     *                                              "RH": 6
     *                                          },
     *                                          "2019-10-29": {
     *                                              "OVERBREAK": 1,
     *                                              "RH+OT": 2.1666666666667,
     *                                              "RH+SH": 2.75,
     *                                              "SH+NT+OT": 2.75,
     *                                              "PAID_LEAVE": 4,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "2RH+OT": 3.2,
     *                                              "REGULAR": 8,
     *                                              "SH": 2.5666666666667,
     *                                              "RH+SH+OT": 2.25,
     *                                              "NT": 2,
     *                                              "SH+OT": 4.2,
     *                                              "NT+OT": 6,
     *                                              "OT": 6,
     *                                              "RH+NT": 2.75
     *                                          },
     *                                          "2019-10-30": {
     *                                              "RH+OT": 2,
     *                                              "RH+SH": 6,
     *                                              "SH+NT+OT": 6,
     *                                              "PAID_LEAVE": 4,
     *                                              "RH+SH+NT": 1.4333333333333,
     *                                              "2RH": 1.3833333333333,
     *                                              "UNPAID_LEAVE": 8,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 2.75,
     *                                              "UNDERTIME": 2.5,
     *                                              "RH+NT+OT": 3,
     *                                              "2RH+NT+OT": 2.2,
     *                                              "NT+OT": 2.75,
     *                                              "RH+NT": 6,
     *                                              "RH": 6
     *                                          },
     *                                          "2019-10-31": {
     *                                              "RH+SH": 2.75,
     *                                              "SH+NT+OT": 2.75,
     *                                              "PAID_LEAVE": 8,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "TARDY": 0.56666666666667,
     *                                              "SH+NT": 2,
     *                                              "REGULAR": 8,
     *                                              "SH": 6,
     *                                              "RH+SH+NT+OT": 2.25,
     *                                              "RH+SH+OT": 6,
     *                                              "2RH+NT": 2.5833333333333,
     *                                              "SH+OT": 2,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 2.75
     *                                          },
     *                                          "2019-11-01": {
     *                                              "RD+2RH+OT": 6.5666666666667,
     *                                              "RD+SH+NT": 6.5666666666667,
     *                                              "RH+OT": 3.4166666666667,
     *                                              "RH+SH": 6,
     *                                              "SH+NT+OT": 6,
     *                                              "RD+SH": 2,
     *                                              "RD+RH+SH+NT+OT": 6.5666666666667,
     *                                              "RD+OT": 1.25,
     *                                              "RH+SH+OT": 6,
     *                                              "RD+RH+OT": 2,
     *                                              "NT": 1.5,
     *                                              "RD+RH": 6.5666666666667,
     *                                              "RD+NT": 2,
     *                                              "RD+NT+OT": 2,
     *                                              "RD": 6,
     *                                              "RD+RH+NT+OT": 2,
     *                                              "RD+RH+SH": 1.5833333333333,
     *                                              "RD+2RH+NT+OT": 6.5666666666667,
     *                                              "2RH+NT+OT": 1,
     *                                              "OT": 6,
     *                                              "RH+NT": 6,
     *                                              "RH": 8,
     *                                              "RD+RH+SH+NT": 2
     *                                          },
     *                                          "2019-11-02": {
     *                                              "RD+2RH+OT": 5,
     *                                              "RD+SH+NT": 5,
     *                                              "RH+OT": 5,
     *                                              "RD+2RH+NT": 5,
     *                                              "RH+SH": 5,
     *                                              "RD+2RH": 5,
     *                                              "SH+NT+OT": 5,
     *                                              "RH+SH+NT": 5,
     *                                              "2RH": 5,
     *                                              "RD+SH+OT": 5,
     *                                              "RD+SH": 5,
     *                                              "SH+NT": 5,
     *                                              "2RH+OT": 5,
     *                                              "RD+RH+SH+NT+OT": 5,
     *                                              "SH": 5,
     *                                              "RH+SH+NT+OT": 5,
     *                                              "RD+OT": 5,
     *                                              "RH+SH+OT": 5,
     *                                              "RD+RH+OT": 5,
     *                                              "NT": 5,
     *                                              "RD+RH": 5,
     *                                              "RD+NT": 5,
     *                                              "RH+NT+OT": 5,
     *                                              "RD+NT+OT": 5,
     *                                              "RD": 5,
     *                                              "RD+RH+NT+OT": 5,
     *                                              "RD+RH+SH": 5,
     *                                              "2RH+NT": 5,
     *                                              "RD+SH+NT+OT": 5,
     *                                              "RD+RH+SH+OT": 5,
     *                                              "RD+2RH+NT+OT": 5,
     *                                              "SH+OT": 5,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 5,
     *                                              "OT": 5,
     *                                              "RH+NT": 5,
     *                                              "RD+RH+NT": 5,
     *                                              "RH": 5,
     *                                              "RD+RH+SH+NT": 5
     *                                          },
     *                                          "2019-11-03": {
     *                                              "RD+2RH+OT": 5,
     *                                              "RD+SH+NT": 5,
     *                                              "RH+OT": 5,
     *                                              "RD+2RH+NT": 5,
     *                                              "RH+SH": 5,
     *                                              "RD+2RH": 5,
     *                                              "SH+NT+OT": 5,
     *                                              "RH+SH+NT": 5,
     *                                              "2RH": 5,
     *                                              "RD+SH+OT": 5,
     *                                              "RD+SH": 5,
     *                                              "SH+NT": 5,
     *                                              "2RH+OT": 5,
     *                                              "RD+RH+SH+NT+OT": 5,
     *                                              "SH": 5,
     *                                              "RH+SH+NT+OT": 5,
     *                                              "RD+OT": 5,
     *                                              "RH+SH+OT": 5,
     *                                              "RD+RH+OT": 5,
     *                                              "NT": 5,
     *                                              "RD+RH": 5,
     *                                              "RD+NT": 5,
     *                                              "RH+NT+OT": 5,
     *                                              "RD+NT+OT": 5,
     *                                              "RD": 5,
     *                                              "RD+RH+NT+OT": 5,
     *                                              "RD+RH+SH": 5,
     *                                              "2RH+NT": 5,
     *                                              "RD+SH+NT+OT": 5,
     *                                              "RD+RH+SH+OT": 5,
     *                                              "RD+2RH+NT+OT": 5,
     *                                              "SH+OT": 5,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 5,
     *                                              "OT": 5,
     *                                              "RH+NT": 5,
     *                                              "RD+RH+NT": 5,
     *                                              "RH": 5,
     *                                              "RD+RH+SH+NT": 5
     *                                          },
     *                                          "2019-11-04": {
     *                                              "PAID_LEAVE": 8,
     *                                              "2RH": 5,
     *                                              "SH+NT": 2.1666666666667,
     *                                              "2RH+OT": 1.75,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 2.75,
     *                                              "NT": 2.75,
     *                                              "2RH+NT": 3,
     *                                              "SH+OT": 2.75,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 1,
     *                                              "OT": 2.75
     *                                          },
     *                                          "2019-11-05": {
     *                                              "OVERBREAK": 1,
     *                                              "SH+NT+OT": 2.2,
     *                                              "RH+SH+NT": 4.25,
     *                                              "UNPAID_LEAVE": 8,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 6,
     *                                              "NT": 6,
     *                                              "RH+NT+OT": 3.25,
     *                                              "SH+OT": 6,
     *                                              "OT": 6
     *                                          },
     *                                          "2019-11-06": {
     *                                              "RH+SH": 3.25,
     *                                              "PAID_LEAVE": 4,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "TARDY": 1,
     *                                              "REGULAR": 8,
     *                                              "SH": 2.25,
     *                                              "RH+SH+OT": 2.5,
     *                                              "NT": 8,
     *                                              "UNDERTIME": 2.25,
     *                                              "SH+OT": 0.75,
     *                                              "2RH+NT+OT": 3.1666666666667,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 2
     *                                          },
     *                                          "2019-11-07": {
     *                                              "SH+NT+OT": 3.4166666666667,
     *                                              "PAID_LEAVE": 8,
     *                                              "2RH+OT": 1,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+NT+OT": 4.25,
     *                                              "RH+SH+OT": 2.75,
     *                                              "NT": 2.5,
     *                                              "RH+NT+OT": 2.2,
     *                                              "NT+OT": 5,
     *                                              "OT": 6,
     *                                              "RH+NT": 3
     *                                          },
     *                                          "2019-11-08": {
     *                                              "RH+OT": 1.4,
     *                                              "RD+SH+OT": 3.75,
     *                                              "RD+RH+SH+NT+OT": 3,
     *                                              "RH+SH+OT": 6,
     *                                              "RD+NT": 2.25,
     *                                              "RD+RH+NT+OT": 2.5,
     *                                              "RD+RH+SH": 7.5,
     *                                              "RD+RH+SH+OT": 2,
     *                                              "2RH+NT+OT": 1,
     *                                              "RH": 2,
     *                                              "RD+RH+SH+NT": 0.5
     *                                          }
     *                                      },
     *                                      "previousContributions": {
     *                                          "SSS": {},
     *                                          "HDMF": {},
     *                                          "PHILHEALTH": {}
     *                                      },
     *                                      "annualEarnings": {
     *                                          "grossCompensationIncome": 1405531.0320513,
     *                                          "nonTaxableIncome": 10500,
     *                                          "contributions": 0,
     *                                          "withholdingTax": 373001.35589744,
     *                                          "taxableIncome": 1395031.0320513,
     *                                          "breakdown": {
     *                                              "ytd": {
     *                                                  "grossCompensationIncome": 1405531.0320513,
     *                                                  "withholdingTax": 373001.35589744,
     *                                                  "nonTaxableIncome": 10500,
     *                                                  "contributions": 0,
     *                                                  "taxableIncome": 1395031.0320513
     *                                              },
     *                                              "currentEmployer": {
     *                                                  "grossCompensationIncome": 0,
     *                                                  "nonTaxableIncome": 0,
     *                                                  "contributions": 0,
     *                                                  "withholdingTax": 0,
     *                                                  "taxableIncome": 0
     *                                              },
     *                                              "previousEmployer": {
     *                                                  "grossCompensationIncome": 0,
     *                                                  "nonTaxableIncome": 0,
     *                                                  "contributions": 0,
     *                                                  "withholdingTax": 0,
     *                                                  "taxableIncome": 0
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                              }
     *                          },
     *                          "output": {
     *                              "contributions": {
     *                                  "SSS": {
     *                                      "ee_share": 0,
     *                                      "er_share": 0,
     *                                      "ec_share": 0,
     *                                      "emp_income_deduction": 0,
     *                                      "records": {
     *                                          {
     *                                              "basis_amount": 0,
     *                                              "basis_type": "NO_CONTRIBUTION",
     *                                              "deduction_schedule": "EVERY_PAY",
     *                                              "employee_income_deduction": 0,
     *                                              "employee_share_amount": 0,
     *                                              "employer_share_amount": 0,
     *                                              "max_pay_schedule": 2,
     *                                              "pay_schedule": 2,
     *                                              "current_gross_income": 398068.56623932,
     *                                              "employee_compensation_amount": 0
     *                                          }
     *                                      }
     *                                  },
     *                                  "HDMF": {
     *                                      "ee_share": 0,
     *                                      "er_share": 0,
     *                                      "emp_income_deduction": 0,
     *                                      "records": {
     *                                          {
     *                                              "basis_amount": 0,
     *                                              "basis_type": "NO_CONTRIBUTION",
     *                                              "deduction_schedule": "EVERY_PAY",
     *                                              "employee_income_deduction": 0,
     *                                              "employee_share_amount": 0,
     *                                              "employer_share_amount": 0,
     *                                              "max_pay_schedule": 2,
     *                                              "pay_schedule": 2,
     *                                              "current_gross_income": 398068.56623932,
     *                                              "employee_compensation_amount": 0
     *                                          }
     *                                      }
     *                                  },
     *                                  "PHILHEALTH": {
     *                                      "ee_share": 0,
     *                                      "er_share": 0,
     *                                      "emp_income_deduction": 0,
     *                                      "records": {
     *                                          {
     *                                              "basis_amount": 0,
     *                                              "basis_type": "NO_CONTRIBUTION",
     *                                              "deduction_schedule": "EVERY_PAY",
     *                                              "employee_income_deduction": 0,
     *                                              "employee_share_amount": 0,
     *                                              "employer_share_amount": 0,
     *                                              "max_pay_schedule": 2,
     *                                              "pay_schedule": 2,
     *                                              "current_gross_income": 398068.56623932,
     *                                              "employee_compensation_amount": 0
     *                                          }
     *                                      }
     *                                  }
     *                              },
     *                              "refunded_contributions": {},
     *                              "tax": {
     *                                  "tax_amount": 54028.523589745,
     *                                  "tax_type": "REGULAR",
     *                                  "basis_amount": 1790099.5982906,
     *                                  "refund_tax_amount": 0
     *                              },
     *                              "other_incomes": {
     *                                  "ALLOWANCE": {
     *                                      "taxable": {
     *                                          {
     *                                              "type": "final_pay_allowance5 non tax",
     *                                              "amount": 2000,
     *                                              "meta": {
     *                                                  "id": 3082,
     *                                                  "subtype_id": 1104,
     *                                                  "subtype_name": "PhilippineAllowance",
     *                                                  "other_income_type_id": 13905,
     *                                                  "other_income_type_subtype_type_id": 1738,
     *                                                  "basis_amount": 5000,
     *                                                  "given_amount": 5000,
     *                                                  "deducted_amount": 0,
     *                                                  "max_non_taxable": 3000,
     *                                                  "taxable_split": 2000
     *                                              }
     *                                          }
     *                                      },
     *                                      "non_taxable": {
     *                                          {
     *                                              "type": "final_pay_allowance5 non tax",
     *                                              "amount": 3000,
     *                                              "meta": {
     *                                                  "id": 3082,
     *                                                  "subtype_id": 1104,
     *                                                  "subtype_name": "PhilippineAllowance",
     *                                                  "other_income_type_id": 13905,
     *                                                  "other_income_type_subtype_type_id": 1738,
     *                                                  "basis_amount": 5000,
     *                                                  "given_amount": 5000,
     *                                                  "deducted_amount": 0,
     *                                                  "max_non_taxable": 3000,
     *                                                  "taxable_split": 2000
     *                                              }
     *                                          }
     *                                      }
     *                                  },
     *                                  "BONUS": {
     *                                      "taxable": {
     *                                          {
     *                                              "type": "Payroll Group Bonus Periodic",
     *                                              "amount": 18375.534188034,
     *                                              "meta": {
     *                                                  "id": 4255,
     *                                                  "subtype_id": 1301,
     *                                                  "subtype_name": "PhilippineBonus",
     *                                                  "other_income_type_id": 16022,
     *                                                  "other_income_type_subtype_type_id": 8610,
     *                                                  "release_amount": 18375.534188034,
     *                                                  "tax_exempt_amount": 0,
     *                                                  "taxed_amount": 18375.534188034
     *                                              }
     *                                          }
     *                                      },
     *                                      "non_taxable": {}
     *                                  },
     *                                  "COMMISSION": {
     *                                      "taxable": {
     *                                          {
     *                                              "type": "Final_Pay_Taxable_Commission",
     *                                              "amount": 3000,
     *                                              "meta": {
     *                                                  "id": 3067,
     *                                                  "subtype_id": 299,
     *                                                  "subtype_name": "PhilippineCommission",
     *                                                  "other_income_type_id": 13352,
     *                                                  "other_income_type_subtype_type_id": 2667
     *                                              }
     *                                          },
     *                                          {
     *                                              "type": "Final_Pay_Taxable_Commission",
     *                                              "amount": 3000,
     *                                              "meta": {
     *                                                  "id": 3073,
     *                                                  "subtype_id": 305,
     *                                                  "subtype_name": "PhilippineCommission",
     *                                                  "other_income_type_id": 13352,
     *                                                  "other_income_type_subtype_type_id": 2667
     *                                              }
     *                                          }
     *                                      },
     *                                      "non_taxable": {}
     *                                  }
     *                              },
     *                              "deductions": {
     *                                  {
     *                                      "type": "Mobile Plans 83",
     *                                      "amount": 1500,
     *                                      "meta": {
     *                                          "id": 3930,
     *                                          "subtype_id": 492,
     *                                          "subtype_name": "PhilippineDeduction",
     *                                          "other_income_type_id": 15347,
     *                                          "other_income_type_subtype_type_id": 1771
     *                                      }
     *                                  },
     *                                  {
     *                                      "type": "Mobile Plans 88",
     *                                      "amount": 1500,
     *                                      "meta": {
     *                                          "id": 3973,
     *                                          "subtype_id": 504,
     *                                          "subtype_name": "PhilippineDeduction",
     *                                          "other_income_type_id": 15390,
     *                                          "other_income_type_subtype_type_id": 1783
     *                                      }
     *                                  }
     *                              },
     *                              "adjustments": {
     *                                  "TAX_ADJUSTMENT": {},
     *                                  "TAXABLE_INCOME": {
     *                                      {
     *                                          "type": "TAXABLE_INCOME",
     *                                          "amount": 500,
     *                                          "reason": "For Qa Testing",
     *                                          "meta": {
     *                                              "id": 3042,
     *                                              "subtype_id": 247,
     *                                              "subtype_name": "PhilippineAdjustment",
     *                                              "other_income_type_id": 3,
     *                                              "other_income_type_subtype_type_id": 1
     *                                          }
     *                                      }
     *                                  },
     *                                  "NON_TAXABLE_INCOME": {},
     *                                  "NON_INCOME": {}
     *                              },
     *                              "loan_amortizations": {
     *                                  "GOVERNMENT": {},
     *                                  "OTHER": {
     *                                      {
     *                                          "type": "Final_Pay_Car-Loan",
     *                                          "amount": 1525,
     *                                          "due_date": "2019-12-13",
     *                                          "meta": {"id": 50549, "loanId": 326,
 "subtype": "","employerRemarks": null, "principalAmount": 6000, "referenceNumber": ""}
     *                                      },
     *                                      {
     *                                          "type": "Final_Pay_Car-Loan",
     *                                          "amount": 1425,
     *                                          "due_date": "2019-12-30",
     *                                          "meta": {"id": 50550, "loanId": 326,
 "subtype": "", "employerRemarks": null, "principalAmount": 6000, "referenceNumber": ""}
     *                                      }
     *                                  }
     *                              },
     *                              "basic_pay": {
     *                                  "full_pay": {
     *                                      "records": {
     *                                          {
     *                                              "amount": 400000,
     *                                              "pay_rate": "YEARLY",
     *                                              "effective_date": "2019-01-01",
     *                                              "conversion_rates": {
     *                                                  "yearly_rate_amount": 400000,
     *                                                  "monthly_rate_amount": 33333.333333333,
     *                                                  "daily_rate_amount": 1538.4615384615,
     *                                                  "hourly_rate_amount": 192.30769230769
     *                                              }
     *                                          }
     *                                      },
     *                                      "latest_base_pay": {
     *                                          "amount": 400000,
     *                                          "pay_rate": "YEARLY",
     *                                          "effective_date": "2019-01-01",
     *                                          "conversion_rates": {
     *                                              "yearly_rate_amount": 400000,
     *                                              "monthly_rate_amount": 33333.333333333,
     *                                              "daily_rate_amount": 1538.4615384615,
     *                                              "hourly_rate_amount": 192.30769230769
     *                                          }
     *                                      },
     *                                      "total_amount": 24615.384615385
     *                                  },
     *                                  "undertime": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 2.5,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 480.76923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 2.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 432.69230769231
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "UNDERTIME": {
     *                                              "total_hours": 4.75,
     *                                              "total_amount": 913.46153846154
     *                                          }
     *                                      },
     *                                      "total_hours": 4.75,
     *                                      "total_amount": 913.46153846154
     *                                  },
     *                                  "tardy": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 2.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 432.69230769231
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 0.56666666666667,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 108.97435897436
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 192.30769230769
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "TARDY": {
     *                                              "total_hours": 3.8166666666667,
     *                                              "total_amount": 733.97435897436
     *                                          }
     *                                      },
     *                                      "total_hours": 3.8166666666667,
     *                                      "total_amount": 733.97435897436
     *                                  },
     *                                  "overbreak": {
     *                                      "records": {},
     *                                      "summary": {},
     *                                      "total_hours": 0,
     *                                      "total_amount": 0
     *                                  },
     *                                  "unpaid_leave": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "UNPAID_LEAVE": {
     *                                              "total_hours": 28,
     *                                              "total_amount": 5384.6153846154
     *                                          }
     *                                      },
     *                                      "total_hours": 28,
     *                                      "total_amount": 5384.6153846154
     *                                  },
     *                                  "absent": {
     *                                      "records": {},
     *                                      "summary": {},
     *                                      "total_hours": 0,
     *                                      "total_amount": 0
     *                                  },
     *                                  "paid_leave": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 4,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 8,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1538.4615384615
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "PAID_LEAVE": {
     *                                              "total_hours": 44,
     *                                              "total_amount": 8461.5384615385
     *                                          }
     *                                      },
     *                                      "total_hours": 44,
     *                                      "total_amount": 8461.5384615385
     *                                  },
     *                                  "unworked_holiday": {
     *                                      "records": {},
     *                                      "summary": {},
     *                                      "total_hours": 0,
     *                                      "total_amount": 0
     *                                  },
     *                                  "holiday_work": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2307.6923076923,
     *                                              "add_amount": 1153.8461538462
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 846.15384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.5666666666667,
     *                                              "hour_type": "SH",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 641.66666666668,
     *                                              "add_amount": 148.07692307693
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3000,
     *                                              "add_amount": 1846.1538461538
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1.3833333333333,
     *                                              "hour_type": "2RH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 798.0769230769,
     *                                              "add_amount": 532.05128205127
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2307.6923076923,
     *                                              "add_amount": 1153.8461538462
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 846.15384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "SH",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1500,
     *                                              "add_amount": 346.15384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3000,
     *                                              "add_amount": 1846.1538461538
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 8,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3076.9230769231,
     *                                              "add_amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500,
     *                                              "add_amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "2RH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2884.6153846154,
     *                                              "add_amount": 1923.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "SH",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1250,
     *                                              "add_amount": 288.46153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1923.0769230769,
     *                                              "add_amount": 961.53846153846
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500,
     *                                              "add_amount": 1538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "2RH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2884.6153846154,
     *                                              "add_amount": 1923.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "SH",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1250,
     *                                              "add_amount": 288.46153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1923.0769230769,
     *                                              "add_amount": 961.53846153846
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 5,
     *                                              "hour_type": "2RH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2884.6153846154,
     *                                              "add_amount": 1923.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 3.25,
     *                                              "hour_type": "RH+SH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625,
     *                                              "add_amount": 1000
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.25,
     *                                              "hour_type": "SH",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 562.5,
     *                                              "add_amount": 129.80769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 2,
     *                                              "hour_type": "RH",
     *                                              "hour_type_multiplier": 2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 769.23076923077,
     *                                              "add_amount": 384.61538461538
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "RH": {
     *                                              "total_hours": 32,
     *                                              "total_amount": 12307.692307692,
     *                                              "total_add_amount": 6153.8461538462
     *                                          },
     *                                          "RH+SH": {
     *                                              "total_hours": 30.75,
     *                                              "total_amount": 15375,
     *                                              "total_add_amount": 9461.5384615385
     *                                          },
     *                                          "SH": {
     *                                              "total_hours": 20.816666666667,
     *                                              "total_amount": 5204.1666666667,
     *                                              "total_add_amount": 1200.9615384615
     *                                          },
     *                                          "2RH": {
     *                                              "total_hours": 16.383333333333,
     *                                              "total_amount": 9451.9230769231,
     *                                              "total_add_amount": 6301.282051282
     *                                          }
     *                                      },
     *                                      "total_hours": 99.95,
     *                                      "total_amount": 42338.782051282,
     *                                      "total_add_amount": 23117.628205128
     *                                  },
     *                                  "night_differential": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1.1666666666667,
     *                                              "hour_type": "SH+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 320.83333333334,
     *                                              "add_amount": 96.474358974362
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 211.53846153846,
     *                                              "add_amount": 19.230769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1,
     *                                              "hour_type": "2RH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 634.61538461538,
     *                                              "add_amount": 442.30769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1.25,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 528.84615384615,
     *                                              "add_amount": 288.46153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 423.07692307692,
     *                                              "add_amount": 38.461538461538
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1163.4615384615,
     *                                              "add_amount": 634.61538461538
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 1.4333333333333,
     *                                              "hour_type": "RH+SH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 788.33333333332,
     *                                              "add_amount": 512.6923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2538.4615384615,
     *                                              "add_amount": 1384.6153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2,
     *                                              "hour_type": "SH+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 550,
     *                                              "add_amount": 165.38461538462
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.5833333333333,
     *                                              "hour_type": "2RH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1639.4230769231,
     *                                              "add_amount": 1142.6282051282
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1163.4615384615,
     *                                              "add_amount": 634.61538461538
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 6.5666666666667,
     *                                              "hour_type": "RD+SH+NT",
     *                                              "hour_type_multiplier": 1.65,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2083.6538461539,
     *                                              "add_amount": 2083.6538461539
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 1.5,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 317.30769230769,
     *                                              "add_amount": 317.30769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 2,
     *                                              "hour_type": "RD+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 550,
     *                                              "add_amount": 550
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 6,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2538.4615384615,
     *                                              "add_amount": 2538.4615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 2,
     *                                              "hour_type": "RD+RH+SH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1269.2307692308,
     *                                              "add_amount": 1269.2307692308
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+NT",
     *                                              "hour_type_multiplier": 1.65,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1586.5384615385,
     *                                              "add_amount": 1586.5384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+NT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125,
     *                                              "add_amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750,
     *                                              "add_amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "SH+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 1375
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1057.6923076923,
     *                                              "add_amount": 1057.6923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 1375
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3173.0769230769,
     *                                              "add_amount": 3173.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2115.3846153846,
     *                                              "add_amount": 2115.3846153846
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750,
     *                                              "add_amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3173.0769230769,
     *                                              "add_amount": 3173.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+NT",
     *                                              "hour_type_multiplier": 1.65,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1586.5384615385,
     *                                              "add_amount": 1586.5384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+NT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125,
     *                                              "add_amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750,
     *                                              "add_amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "SH+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 1375
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1057.6923076923,
     *                                              "add_amount": 1057.6923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1375,
     *                                              "add_amount": 1375
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3173.0769230769,
     *                                              "add_amount": 3173.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2115.3846153846,
     *                                              "add_amount": 2115.3846153846
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750,
     *                                              "add_amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3173.0769230769,
     *                                              "add_amount": 3173.0769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.1666666666667,
     *                                              "hour_type": "SH+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 595.83333333334,
     *                                              "add_amount": 179.16666666667
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.75,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 581.73076923077,
     *                                              "add_amount": 52.884615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 3,
     *                                              "hour_type": "2RH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1903.8461538462,
     *                                              "add_amount": 1326.9230769231
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 4.25,
     *                                              "hour_type": "RH+SH+NT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2337.5,
     *                                              "add_amount": 1520.1923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 6,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1269.2307692308,
     *                                              "add_amount": 115.38461538462
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 8,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1692.3076923077,
     *                                              "add_amount": 153.84615384615
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 846.15384615385,
     *                                              "add_amount": 461.53846153846
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 2.5,
     *                                              "hour_type": "NT",
     *                                              "hour_type_multiplier": 1.1,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 528.84615384615,
     *                                              "add_amount": 48.076923076923
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "scheduled_hours": 8,
     *                                              "hours": 3,
     *                                              "hour_type": "RH+NT",
     *                                              "hour_type_multiplier": 2.2,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1269.2307692308,
     *                                              "add_amount": 692.30769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 2.25,
     *                                              "hour_type": "RD+NT",
     *                                              "hour_type_multiplier": 1.43,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 618.75,
     *                                              "add_amount": 618.75
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "scheduled_hours": 0,
     *                                              "hours": 0.5,
     *                                              "hour_type": "RD+RH+SH+NT",
     *                                              "hour_type_multiplier": 3.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 317.30769230769,
     *                                              "add_amount": 317.30769230769
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "SH+NT": {
     *                                              "total_hours": 15.333333333333,
     *                                              "total_amount": 4216.6666666667,
     *                                              "total_add_amount": 3191.0256410256
     *                                          },
     *                                          "NT": {
     *                                              "total_hours": 33.75,
     *                                              "total_amount": 7139.4230769231,
     *                                              "total_add_amount": 2860.5769230769
     *                                          },
     *                                          "2RH+NT": {
     *                                              "total_hours": 16.583333333333,
     *                                              "total_amount": 10524.038461538,
     *                                              "total_add_amount": 9258.0128205128
     *                                          },
     *                                          "RH+NT": {
     *                                              "total_hours": 33.75,
     *                                              "total_amount": 14278.846153846,
     *                                              "total_add_amount": 10865.384615385
     *                                          },
     *                                          "RH+SH+NT": {
     *                                              "total_hours": 15.683333333333,
     *                                              "total_amount": 8625.8333333333,
     *                                              "total_add_amount": 7532.8846153846
     *                                          },
     *                                          "RD+SH+NT": {
     *                                              "total_hours": 16.566666666667,
     *                                              "total_amount": 5256.7307692308,
     *                                              "total_add_amount": 5256.7307692308
     *                                          },
     *                                          "RD+NT": {
     *                                              "total_hours": 14.25,
     *                                              "total_amount": 3918.75,
     *                                              "total_add_amount": 3918.75
     *                                          },
     *                                          "RD+RH+SH+NT": {
     *                                              "total_hours": 12.5,
     *                                              "total_amount": 7932.6923076923,
     *                                              "total_add_amount": 7932.6923076923
     *                                          },
     *                                          "RD+2RH+NT": {
     *                                              "total_hours": 10,
     *                                              "total_amount": 8250,
     *                                              "total_add_amount": 8250
     *                                          },
     *                                          "RD+RH+NT": {
     *                                              "total_hours": 10,
     *                                              "total_amount": 5500,
     *                                              "total_add_amount": 5500
     *                                          }
     *                                      },
     *                                      "total_hours": 178.41666666667,
     *                                      "total_amount": 75642.980769231,
     *                                      "total_add_amount": 64566.057692308
     *                                  },
     *                                  "rest_day_work": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 2,
     *                                              "hour_type": "RD+SH",
     *                                              "hour_type_multiplier": 1.5,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 576.92307692308
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6.5666666666667,
     *                                              "hour_type": "RD+RH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3283.3333333334
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6,
     *                                              "hour_type": "RD",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 1.5833333333333,
     *                                              "hour_type": "RD+RH+SH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 913.46153846152
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH",
     *                                              "hour_type_multiplier": 1.5,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2884.6153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH",
     *                                              "hour_type_multiplier": 1.5,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD",
     *                                              "hour_type_multiplier": 1.3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2884.6153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 7.5,
     *                                              "hour_type": "RD+RH+SH",
     *                                              "hour_type_multiplier": 3,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4326.9230769231
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "RD+SH": {
     *                                              "total_hours": 12,
     *                                              "total_amount": 3461.5384615385
     *                                          },
     *                                          "RD+RH": {
     *                                              "total_hours": 16.566666666667,
     *                                              "total_amount": 8283.3333333334
     *                                          },
     *                                          "RD": {
     *                                              "total_hours": 16,
     *                                              "total_amount": 4000
     *                                          },
     *                                          "RD+RH+SH": {
     *                                              "total_hours": 19.083333333333,
     *                                              "total_amount": 11009.615384615
     *                                          },
     *                                          "RD+2RH": {
     *                                              "total_hours": 10,
     *                                              "total_amount": 7500
     *                                          }
     *                                      },
     *                                      "total_hours": 73.65,
     *                                      "total_amount": 34254.487179487
     *                                  },
     *                                  "overtime": {
     *                                      "records": {
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 1.4166666666667,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 506.45833333335
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 1.5,
     *                                              "hour_type": "RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1072.5
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 1.75,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 962.5
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 1.2,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 990
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 2.75,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 727.16346153846
     *                                          },
     *                                          {
     *                                              "date": "2019-10-28",
     *                                              "hours": 2.75,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 661.05769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 2.1666666666667,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1083.3333333334
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 2.75,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 983.125
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 3.2,
     *                                              "hour_type": "2RH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2400
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 2.25,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1462.5
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 4.2,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1365
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 6,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1586.5384615385
     *                                          },
     *                                          {
     *                                              "date": "2019-10-29",
     *                                              "hours": 6,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 2,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1000
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 6,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2145
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 3,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1650
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 2.2,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1815
     *                                          },
     *                                          {
     *                                              "date": "2019-10-30",
     *                                              "hours": 2.75,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 727.16346153846
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 2.75,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 983.125
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 2.25,
     *                                              "hour_type": "RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1608.75
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3900
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 2,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 650
     *                                          },
     *                                          {
     *                                              "date": "2019-10-31",
     *                                              "hours": 2.75,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 661.05769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6.5666666666667,
     *                                              "hour_type": "RD+2RH+OT",
     *                                              "hour_type_multiplier": 5.07,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 6402.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 3.4166666666667,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1708.3333333334
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2145
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6.5666666666667,
     *                                              "hour_type": "RD+RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 5417.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 1.25,
     *                                              "hour_type": "RD+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 406.25
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3900
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 2,
     *                                              "hour_type": "RD+RH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1300
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 2,
     *                                              "hour_type": "RD+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 715
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 2,
     *                                              "hour_type": "RD+RH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1430
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6.5666666666667,
     *                                              "hour_type": "RD+2RH+NT+OT",
     *                                              "hour_type_multiplier": 5.577,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 7042.75
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 1,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 825
     *                                          },
     *                                          {
     *                                              "date": "2019-11-01",
     *                                              "hours": 6,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+OT",
     *                                              "hour_type_multiplier": 5.07,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4875
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+OT",
     *                                              "hour_type_multiplier": 1.95,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1875
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3575
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3575
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+NT+OT",
     *                                              "hour_type_multiplier": 2.145,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2062.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+NT+OT",
     *                                              "hour_type_multiplier": 5.577,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 5362.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1322.1153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-02",
     *                                              "hours": 5,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1201.9230769231
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+OT",
     *                                              "hour_type_multiplier": 5.07,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4875
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+OT",
     *                                              "hour_type_multiplier": 1.95,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1875
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3575
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3250
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3575
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+SH+NT+OT",
     *                                              "hour_type_multiplier": 2.145,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2062.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+RH+SH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "RD+2RH+NT+OT",
     *                                              "hour_type_multiplier": 5.577,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 5362.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1322.1153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-03",
     *                                              "hours": 5,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1201.9230769231
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 1.75,
     *                                              "hour_type": "2RH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1312.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 2.75,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 893.75
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 5,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 4125
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 1,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 264.42307692308
     *                                          },
     *                                          {
     *                                              "date": "2019-11-04",
     *                                              "hours": 2.75,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 661.05769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 2.2,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 786.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3900
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 3.25,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 6,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1950
     *                                          },
     *                                          {
     *                                              "date": "2019-11-05",
     *                                              "hours": 6,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 2.5,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1625
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 0.75,
     *                                              "hour_type": "SH+OT",
     *                                              "hour_type_multiplier": 1.69,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 243.75
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 3.1666666666667,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2612.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-06",
     *                                              "hours": 2.75,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 661.05769230769
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 3.4166666666667,
     *                                              "hour_type": "SH+NT+OT",
     *                                              "hour_type_multiplier": 1.859,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1221.4583333333
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 1,
     *                                              "hour_type": "2RH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 750
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 4.25,
     *                                              "hour_type": "RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3038.75
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 2.75,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 2.2,
     *                                              "hour_type": "RH+NT+OT",
     *                                              "hour_type_multiplier": 2.86,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1210
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 5,
     *                                              "hour_type": "NT+OT",
     *                                              "hour_type_multiplier": 1.375,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1322.1153846154
     *                                          },
     *                                          {
     *                                              "date": "2019-11-07",
     *                                              "hours": 6,
     *                                              "hour_type": "OT",
     *                                              "hour_type_multiplier": 1.25,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1442.3076923077
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 1.4,
     *                                              "hour_type": "RH+OT",
     *                                              "hour_type_multiplier": 2.6,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 700
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 3.75,
     *                                              "hour_type": "RD+SH+OT",
     *                                              "hour_type_multiplier": 1.95,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1406.25
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 3,
     *                                              "hour_type": "RD+RH+SH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 2475
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 6,
     *                                              "hour_type": "RH+SH+OT",
     *                                              "hour_type_multiplier": 3.38,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 3900
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 2.5,
     *                                              "hour_type": "RD+RH+NT+OT",
     *                                              "hour_type_multiplier": 3.718,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1787.5
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 2,
     *                                              "hour_type": "RD+RH+SH+OT",
     *                                              "hour_type_multiplier": 3.9,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 1500
     *                                          },
     *                                          {
     *                                              "date": "2019-11-08",
     *                                              "hours": 1,
     *                                              "hour_type": "2RH+NT+OT",
     *                                              "hour_type_multiplier": 4.29,
     *                                              "hourly_rate_amount": 192.30769230769,
     *                                              "amount": 825
     *                                          }
     *                                      },
     *                                      "summary": {
     *                                          "SH+NT+OT": {
     *                                              "total_hours": 34.533333333333,
     *                                              "total_amount": 12345.666666667
     *                                          },
     *                                          "RH+SH+NT+OT": {
     *                                              "total_hours": 18,
     *                                              "total_amount": 12870
     *                                          },
     *                                          "RH+NT+OT": {
     *                                              "total_hours": 20.2,
     *                                              "total_amount": 11110
     *                                          },
     *                                          "2RH+NT+OT": {
     *                                              "total_hours": 23.566666666667,
     *                                              "total_amount": 19442.5
     *                                          },
     *                                          "NT+OT": {
     *                                              "total_hours": 27.5,
     *                                              "total_amount": 7271.6346153846
     *                                          },
     *                                          "OT": {
     *                                              "total_hours": 45,
     *                                              "total_amount": 10817.307692308
     *                                          },
     *                                          "RH+OT": {
     *                                              "total_hours": 18.983333333333,
     *                                              "total_amount": 9491.6666666667
     *                                          },
     *                                          "2RH+OT": {
     *                                              "total_hours": 15.95,
     *                                              "total_amount": 11962.5
     *                                          },
     *                                          "RH+SH+OT": {
     *                                              "total_hours": 47,
     *                                              "total_amount": 30550
     *                                          },
     *                                          "SH+OT": {
     *                                              "total_hours": 25.7,
     *                                              "total_amount": 8352.5
     *                                          },
     *                                          "RD+2RH+OT": {
     *                                              "total_hours": 16.566666666667,
     *                                              "total_amount": 16152.5
     *                                          },
     *                                          "RD+RH+SH+NT+OT": {
     *                                              "total_hours": 19.566666666667,
     *                                              "total_amount": 16142.5
     *                                          },
     *                                          "RD+OT": {
     *                                              "total_hours": 11.25,
     *                                              "total_amount": 3656.25
     *                                          },
     *                                          "RD+RH+OT": {
     *                                              "total_hours": 12,
     *                                              "total_amount": 7800
     *                                          },
     *                                          "RD+NT+OT": {
     *                                              "total_hours": 12,
     *                                              "total_amount": 4290
     *                                          },
     *                                          "RD+RH+NT+OT": {
     *                                              "total_hours": 14.5,
     *                                              "total_amount": 10367.5
     *                                          },
     *                                          "RD+2RH+NT+OT": {
     *                                              "total_hours": 16.566666666667,
     *                                              "total_amount": 17767.75
     *                                          },
     *                                          "RD+SH+OT": {
     *                                              "total_hours": 13.75,
     *                                              "total_amount": 5156.25
     *                                          },
     *                                          "RD+SH+NT+OT": {
     *                                              "total_hours": 10,
     *                                              "total_amount": 4125
     *                                          },
     *                                          "RD+RH+SH+OT": {
     *                                              "total_hours": 12,
     *                                              "total_amount": 9000
     *                                          }
     *                                      },
     *                                      "total_hours": 414.63333333333,
     *                                      "total_amount": 228671.52564103
     *                                  }
     *                              },
     *                              "employee_details": {
     *                                  "basic_info": {
     *                                      "accountId": "524",
     *                                      "companyId": "711",
     *                                      "employeeId": "NV13-0005",
     *                                      "firstName": "Semi-Regular-PYear",
     *                                      "middleName": "",
     *                                      "lastName": "Kpop",
     *                                      "email": "tapayroll01+NV13-0005@gmail.com",
     *                                      "gender": "female",
     *                                      "birthDate": "1984-10-15",
     *                                      "telephoneNumber": "4425159",
     *                                      "mobileNumber": "(63)9954132492",
     *                                      "hoursPerDay": 0,
     *                                      "dateHired": "2019-01-01",
     *                                      "dateEnded": "2019-11-08",
     *                                      "locationId": "631",
     *                                      "locationName": "salqajeff_location",
     *                                      "departmentId": "1391",
     *                                      "departmentName": "Department Parent1556085724",
     *                                      "rankId": "1134",
     *                                      "rankName": "Junior. Quality Assurance Analyst",
     *                                      "costCenterId": "",
     *                                      "costCenterName": null,
     *                                      "positionId": "10660",
     *                                      "positionName": "QA",
     *                                      "address": "Brgy 12 1st  Street",
     *                                      "city": "Baliuag",
     *                                      "zipCode": "14501",
     *                                      "country": "Philippines",
     *                                      "taxStatus": "S",
     *                                      "taxType": "Regular",
     *                                      "taxRate": 0,
     *                                      "tin": "131-456-789-019",
     *                                      "sssNumber": "07-7451477-8",
     *                                      "philhealthNumber": "82-560045513-0",
     *                                      "hdmfNumber": "4177-3254-4805",
     *                                      "rdo": "140",
     *                                      "active": "semi-active",
     *                                      "nonTaxedOtherIncomeLimit": "90000.0000",
     *                                      "payrollGroup": {
     *                                          "id": 4277,
     *                                          "name": "2328_Semi_Monthly",
     *                                          "company_id": 711,
     *                                          "payroll_frequency": "SEMI_MONTHLY",
     *                                          "day_factor": "260.00",
     *                                          "hours_per_day": "8.00",
     *                                          "non_working_day_option": "BEFORE",
     *                                          "pay_date": "2019-06-15 00:00:00",
     *                                          "pay_run_posting": "2019-06-10 00:00:00",
     *                                          "cut_off_date": "2019-06-10 00:00:00",
     *                                          "pay_date_2": "2019-06-30 00:00:00",
     *                                          "pay_run_posting_2": "2019-06-25 00:00:00",
     *                                          "cut_off_date_2": "2019-06-25 00:00:00",
     *                                          "fpd_end_of_month": false,
     *                                          "fpp_end_of_month": false,
     *                                          "fcd_end_of_month": false,
     *                                          "spd_end_of_month": false,
     *                                          "spp_end_of_month": false,
     *                                          "scd_end_of_month": false,
     *                                          "payroll_group_subtype_id": 4277,
     *                                          "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
     *                                          "compute_overbreak": false,
     *                                          "compute_undertime": true,
     *                                          "compute_tardiness": true,
     *                                          "created_at": "2019-08-20 13:22:35",
     *                                          "updated_at": "2019-08-20 13:22:35",
     *                                          "deleted_at": null,
     *                                          "enforce_gap_loans": true,
     *                                          "minimum_net_take_home_pay": "2000.0000",
     *                                          "contributionSchedules": {
     *                                              "SSS": "EVERY_PAY",
     *                                              "HDMF": "EVERY_PAY",
     *                                              "PHILHEALTH": "EVERY_PAY"
     *                                          }
     *                                      },
     *                                      "contributionBasis": {
     *                                          "SSS": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          },
     *                                          "PHILHEALTH": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          },
     *                                          "HDMF": {
     *                                              "type": "No Contribution",
     *                                              "amount": "0.00000000"
     *                                          }
     *                                      },
     *                                      "terminationInformation": {
     *                                          "id": 470,
     *                                          "type": "termination-information",
     *                                          "attributes": {
     *                                              "employee_id": 17627,
     *                                              "company_id": 711,
     *                                              "last_date": "2019-11-08",
     *                                              "start_date_basis": "2019-10-28",
     *                                              "release_date": "2019-11-11",
     *                                              "reason_for_leaving": "TERMINATION",
     *                                              "remarks": ""
     *                                          }
     *                                      },
     *                                      "finalPayId": 210,
     *                                      "finalPayItems": {
     *                                          {
     *                                              "id": 1951,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3232,
     *                                              "type": "ALLOWANCE",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "release_id": 3232,
     *                                                  "name": "final_pay_allowance5 non tax",
     *                                                  "other_income_id": 3082,
     *                                                  "amount": 5000,
     *                                                  "release_date": "2019-11-10"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1952,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3192,
     *                                              "type": "ADJUSTMENT",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3042,
     *                                                  "reason": "For Qa Testing",
     *                                                  "release_date": "2019-11-15",
     *                                                  "include": true,
     *                                                  "name": "TAXABLE_INCOME",
     *                                                  "release_id": 3192,
     *                                                  "amount": 500
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1953,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 4094,
     *                                              "type": "BONUS",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 4255,
     *                                                  "release_date": "2019-12-02",
     *                                                  "include": true,
     *                                                  "name": "Payroll Group Bonus Periodic",
     *                                                  "amount": 0,
     *                                                  "percentage": 100,
     *                                                  "type": "SALARY_BASED",
     *                                                  "frequency": "PERIODIC",
     *                                                  "basis": "BASIC_PAY"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1954,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3217,
     *                                              "type": "COMMISSION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "other_income_id": 3067,
     *                                                  "amount": 3000,
     *                                                  "release_date": "2019-10-10"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1955,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3223,
     *                                              "type": "COMMISSION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "other_income_id": 3073,
     *                                                  "amount": 3000,
     *                                                  "release_date": "2019-10-25"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1956,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3930,
     *                                              "type": "DEDUCTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3930,
     *                                                  "release_date": "2019-03-11",
     *                                                  "include": true,
     *                                                  "name": "Mobile Plans 83",
     *                                                  "valid_from": "2019-03-11",
     *                                                  "amount": 1500,
     *                                                  "valid_to": null
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1957,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 3973,
     *                                              "type": "DEDUCTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "other_income_id": 3973,
     *                                                  "release_date": "2019-05-11",
     *                                                  "include": true,
     *                                                  "name": "Mobile Plans 88",
     *                                                  "valid_from": "2019-05-11",
     *                                                  "amount": 1500,
     *                                                  "valid_to": null
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1958,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 326,
     *                                              "type": "LOAN",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "is_full": false,
     *                                                  "include": true,
     *                                                  "loan_id": 326,
     *                                                  "name": "Final_Pay_Car-Loan",
     *                                                  "monthly_amortization": "3,050.00",
     *                                                  "remaining_balance": "6,000.00",
     *                                                  "repayment_date": "2019-12-30",
     *                                                  "category": ""
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1959,
     *                                              "final_pay_id": 210,
     *                                              "item_id": 1820,
     *                                              "type": "LOAN",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "is_full": false,
     *                                                  "include": true,
     *                                                  "loan_id": 1820,
     *                                                  "name": "SSS",
     *                                                  "monthly_amortization": "1,500.00",
     *                                                  "remaining_balance": "10,000.00",
     *                                                  "repayment_date": "2019-10-30",
     *                                                  "category": "Salary"
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1960,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -1,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "SSS",
     *                                                  "type": "SSS",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1961,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -2,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "Pag-Ibig",
     *                                                  "type": "HDMF",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          },
     *                                          {
     *                                              "id": 1962,
     *                                              "final_pay_id": 210,
     *                                              "item_id": -3,
     *                                              "type": "CONTRIBUTION",
     *                                              "type_name": "",
     *                                              "amount": "0.0000",
     *                                              "meta": {
     *                                                  "include": true,
     *                                                  "refund": false,
     *                                                  "basis": "No Contribution",
     *                                                  "name": "Philhealth",
     *                                                  "type": "PHILHEALTH",
     *                                                  "amount": 0
     *                                              },
     *                                              "deleted_at": null,
     *                                              "created_at": "2019-12-06 11:12:53",
     *                                              "updated_at": "2019-12-06 11:12:53"
     *                                          }
     *                                      },
     *                                      "basePay": {
     *                                          {
     *                                              "id": "17735",
     *                                              "type": "base_pay",
     *                                              "attributes": {
     *                                                  "amount": "400000.00000000",
     *                                                  "unit": "YEARLY",
     *                                                  "effectiveDate": "2019-01-01"
     *                                              }
     *                                          },
     *                                          {
     *                                              "id": "19019",
     *                                              "type": "base_pay",
     *                                              "attributes": {
     *                                                  "amount": "945000.00000000",
     *                                                  "unit": "YEARLY",
     *                                                  "effectiveDate": "2019-11-19"
     *                                              }
     *                                          }
     *                                      },
     *                                      "aabcd": {
     *                                          "adjustment": {
     *                                              {
     *                                                  "type": "adjustment",
     *                                                  "name": "For Qa Testing",
     *                                                  "reason": "For Qa Testing",
     *                                                  "category": "TAXABLE_INCOME",
     *                                                  "fully_taxable": false,
     *                                                  "max_non_taxable": 0,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3192,
     *                                                          "date": "2019-11-15",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3042,
     *                                                      "subtype_id": 247,
     *                                                      "subtype_name": "PhilippineAdjustment",
     *                                                      "other_income_type_id": 3,
     *                                                      "other_income_type_subtype_type_id": 1
     *                                                  }
     *                                              }
     *                                          },
     *                                          "commission": {
     *                                              {
     *                                                  "type": "commission",
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 3000
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3217,
     *                                                          "date": "2019-10-10",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3067,
     *                                                      "subtype_id": 299,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "type": "commission",
     *                                                  "name": "Final_Pay_Taxable_Commission",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 3000
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3223,
     *                                                          "date": "2019-10-25",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3073,
     *                                                      "subtype_id": 305,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              }
     *                                          },
     *                                          "allowance": {
     *                                              {
     *                                                  "type": "allowance",
     *                                                  "name": "final_pay_allowance5 non tax",
     *                                                  "fully_taxable": false,
     *                                                  "max_non_taxable": 3000,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 5000,
     *                                                      "prorated": 0,
     *                                                      "prorated_by": null,
     *                                                      "entitled_when": null
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-11-10",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 3232,
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3082,
     *                                                      "subtype_id": 1104,
     *                                                      "subtype_name": "PhilippineAllowance",
     *                                                      "other_income_type_id": 13905,
     *                                                      "other_income_type_subtype_type_id": 1738,
     *                                                      "basis_amount": 5000,
     *                                                      "given_amount": 5000,
     *                                                      "deducted_amount": 0,
     *                                                      "max_non_taxable": 3000,
     *                                                      "taxable_split": 2000
     *                                                  }
     *                                              }
     *                                          },
     *                                          "bonus": {
     *                                              {
     *                                                  "type": "bonus",
     *                                                  "name": "Payroll Group Bonus Periodic",
     *                                                  "fully_taxable": true,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "SALARY_BASED",
     *                                                      "basis": "BASIC_PAY",
     *                                                      "amount": null,
     *                                                      "percentage": 100
     *                                                  },
     *                                                  "frequency_type": "PERIODIC",
     *                                                  "frequency_schedule": null,
     *                                                  "release_details": {
     *                                                      {
     *                                                          "id": 4094,
     *                                                          "date": "2019-12-02",
     *                                                          "disburse_through_special_pay_run": true,
     *                                                          "payroll_id": null,
     *                                                          "status": false,
     *                                                          "prorate_based_on_tenure": 1,
     *                                                          "coverage_from": "2019-01-01",
     *                                                          "coverage_to": "2019-11-30",
     *                                                          "amount": null
     *                                                      }
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 4255,
     *                                                      "subtype_id": 1301,
     *                                                      "subtype_name": "PhilippineBonus",
     *                                                      "other_income_type_id": 16022,
     *                                                      "other_income_type_subtype_type_id": 8610,
     *                                                      "release_amount": 18375.534188034,
     *                                                      "tax_exempt_amount": 0,
     *                                                      "taxed_amount": 18375.534188034
     *                                                  }
     *                                              }
     *                                          },
     *                                          "deduction": {
     *                                              {
     *                                                  "type": "deduction",
     *                                                  "name": "Mobile Plans 83",
     *                                                  "fully_taxable": null,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 1500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-03-11",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3930,
     *                                                      "subtype_id": 492,
     *                                                      "subtype_name": "PhilippineDeduction",
     *                                                      "other_income_type_id": 15347,
     *                                                      "other_income_type_subtype_type_id": 1771
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "type": "deduction",
     *                                                  "name": "Mobile Plans 88",
     *                                                  "fully_taxable": null,
     *                                                  "max_non_taxable": null,
     *                                                  "amount_details": {
     *                                                      "type": "FIXED",
     *                                                      "amount": 1500
     *                                                  },
     *                                                  "frequency_type": "ONE_TIME",
     *                                                  "frequency_schedule": null,
     *                                                  "validity": {
     *                                                      "valid_from": "2019-05-11",
     *                                                      "valid_to": null
     *                                                  },
     *                                                  "meta": {
     *                                                      "id": 3973,
     *                                                      "subtype_id": 504,
     *                                                      "subtype_name": "PhilippineDeduction",
     *                                                      "other_income_type_id": 15390,
     *                                                      "other_income_type_subtype_type_id": 1783
     *                                                  }
     *                                              }
     *                                          },
     *                                          "salaryBasis": {
     *                                              "grossIncome": {
     *                                                  {
     *                                                      "amount": 221789.01282051,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 119103.06410256,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 155210.26923077,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 270734.13141026,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 257171.91346154,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 163403.92307692,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 75857.435897436,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 37761.282051282,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 17416.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "grossBasic": {
     *                                                  {
     *                                                      "amount": 218039.01282051,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 115353.06410256,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 151460.26923077,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 266984.13141026,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 253421.91346154,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 159653.92307692,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 72107.435897436,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 34011.282051282,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "basicPay": {
     *                                                  {
     *                                                      "amount": 14551.282051282,
     *                                                      "startDate": "2019-03-11",
     *                                                      "endDate": "2019-03-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2201,
     *                                                          "payroll_employees_id": 11790,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": -1868.5897435897,
     *                                                      "startDate": "2019-02-26",
     *                                                      "endDate": "2019-03-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2202,
     *                                                          "payroll_employees_id": 11796,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 13022.435897436,
     *                                                      "startDate": "2019-04-26",
     *                                                      "endDate": "2019-05-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2203,
     *                                                          "payroll_employees_id": 11801,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16378.205128205,
     *                                                      "startDate": "2019-05-11",
     *                                                      "endDate": "2019-05-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2204,
     *                                                          "payroll_employees_id": 11807,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 12987.179487179,
     *                                                      "startDate": "2019-09-26",
     *                                                      "endDate": "2019-10-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2288,
     *                                                          "payroll_employees_id": 12222,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16442.307692308,
     *                                                      "startDate": "2019-10-11",
     *                                                      "endDate": "2019-10-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2294,
     *                                                          "payroll_employees_id": 12252,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-08-26",
     *                                                      "endDate": "2019-09-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2300,
     *                                                          "payroll_employees_id": 12268,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 14743.58974359,
     *                                                      "startDate": "2019-09-11",
     *                                                      "endDate": "2019-09-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2314,
     *                                                          "payroll_employees_id": 12310,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2018-12-26",
     *                                                      "endDate": "2019-01-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2410,
     *                                                          "payroll_employees_id": 12531,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-11",
     *                                                      "endDate": "2019-01-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2412,
     *                                                          "payroll_employees_id": 12541,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-01-26",
     *                                                      "endDate": "2019-02-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2413,
     *                                                          "payroll_employees_id": 12546,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-02-11",
     *                                                      "endDate": "2019-02-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2414,
     *                                                          "payroll_employees_id": 12551,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-03-26",
     *                                                      "endDate": "2019-04-10",
     *                                                      "meta": {
     *                                                          "payroll_id": 2415,
     *                                                          "payroll_employees_id": 12556,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "amount": 16666.666666667,
     *                                                      "startDate": "2019-04-11",
     *                                                      "endDate": "2019-04-25",
     *                                                      "meta": {
     *                                                          "payroll_id": 2416,
     *                                                          "payroll_employees_id": 12562,
     *                                                          "employee_id": 17627
     *                                                      }
     *                                                  }
     *                                              },
     *                                              "basePay": {
     *                                                  {
     *                                                      "id": "17735",
     *                                                      "type": "base_pay",
     *                                                      "attributes": {
     *                                                          "amount": "400000.00000000",
     *                                                          "unit": "YEARLY",
     *                                                          "effectiveDate": "2019-01-01"
     *                                                      }
     *                                                  },
     *                                                  {
     *                                                      "id": "19019",
     *                                                      "type": "base_pay",
     *                                                      "attributes": {
     *                                                          "amount": "945000.00000000",
     *                                                          "unit": "YEARLY",
     *                                                          "effectiveDate": "2019-11-19"
     *                                                      }
     *                                                  }
     *                                              }
     *                                          }
     *                                      },
     *                                      "amortizations": {
     *                                          {
     *                                              "id": 50549,
     *                                              "loanId": 326,
     *                                              "subtype": "",
     *                                              "employerRemarks": null,
     *                                              "principalAmount": 6000,
     *                                              "referenceNumber": "",
     *                                              "loanName": "Final_Pay_Car-Loan",
     *                                              "amountDue": 1525,
     *                                              "dueDate": "2019-12-13"
     *                                          },
     *                                          {
     *                                              "id": 50550,
     *                                              "loanId": 326,
     *                                              "subtype": "",
     *                                              "employerRemarks": null,
     *                                              "principalAmount": 6000,
     *                                              "referenceNumber": "",
     *                                              "loanName": "Final_Pay_Car-Loan",
     *                                              "amountDue": 1425,
     *                                              "dueDate": "2019-12-30"
     *                                          }
     *                                      },
     *                                      "attendanceData": {
     *                                          "2019-10-28": {
     *                                              "SH+NT+OT": 1.4166666666667,
     *                                              "PAID_LEAVE": 8,
     *                                              "TARDY": 2.25,
     *                                              "SH+NT": 1.1666666666667,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+NT+OT": 1.5,
     *                                              "NT": 1,
     *                                              "RH+NT+OT": 1.75,
     *                                              "2RH+NT": 1,
     *                                              "2RH+NT+OT": 1.2,
     *                                              "NT+OT": 2.75,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 1.25,
     *                                              "RH": 6
     *                                          },
     *                                          "2019-10-29": {
     *                                              "OVERBREAK": 1,
     *                                              "RH+OT": 2.1666666666667,
     *                                              "RH+SH": 2.75,
     *                                              "SH+NT+OT": 2.75,
     *                                              "PAID_LEAVE": 4,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "2RH+OT": 3.2,
     *                                              "REGULAR": 8,
     *                                              "SH": 2.5666666666667,
     *                                              "RH+SH+OT": 2.25,
     *                                              "NT": 2,
     *                                              "SH+OT": 4.2,
     *                                              "NT+OT": 6,
     *                                              "OT": 6,
     *                                              "RH+NT": 2.75
     *                                          },
     *                                          "2019-10-30": {
     *                                              "RH+OT": 2,
     *                                              "RH+SH": 6,
     *                                              "SH+NT+OT": 6,
     *                                              "PAID_LEAVE": 4,
     *                                              "RH+SH+NT": 1.4333333333333,
     *                                              "2RH": 1.3833333333333,
     *                                              "UNPAID_LEAVE": 8,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 2.75,
     *                                              "UNDERTIME": 2.5,
     *                                              "RH+NT+OT": 3,
     *                                              "2RH+NT+OT": 2.2,
     *                                              "NT+OT": 2.75,
     *                                              "RH+NT": 6,
     *                                              "RH": 6
     *                                          },
     *                                          "2019-10-31": {
     *                                              "RH+SH": 2.75,
     *                                              "SH+NT+OT": 2.75,
     *                                              "PAID_LEAVE": 8,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "TARDY": 0.56666666666667,
     *                                              "SH+NT": 2,
     *                                              "REGULAR": 8,
     *                                              "SH": 6,
     *                                              "RH+SH+NT+OT": 2.25,
     *                                              "RH+SH+OT": 6,
     *                                              "2RH+NT": 2.5833333333333,
     *                                              "SH+OT": 2,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 2.75
     *                                          },
     *                                          "2019-11-01": {
     *                                              "RD+2RH+OT": 6.5666666666667,
     *                                              "RD+SH+NT": 6.5666666666667,
     *                                              "RH+OT": 3.4166666666667,
     *                                              "RH+SH": 6,
     *                                              "SH+NT+OT": 6,
     *                                              "RD+SH": 2,
     *                                              "RD+RH+SH+NT+OT": 6.5666666666667,
     *                                              "RD+OT": 1.25,
     *                                              "RH+SH+OT": 6,
     *                                              "RD+RH+OT": 2,
     *                                              "NT": 1.5,
     *                                              "RD+RH": 6.5666666666667,
     *                                              "RD+NT": 2,
     *                                              "RD+NT+OT": 2,
     *                                              "RD": 6,
     *                                              "RD+RH+NT+OT": 2,
     *                                              "RD+RH+SH": 1.5833333333333,
     *                                              "RD+2RH+NT+OT": 6.5666666666667,
     *                                              "2RH+NT+OT": 1,
     *                                              "OT": 6,
     *                                              "RH+NT": 6,
     *                                              "RH": 8,
     *                                              "RD+RH+SH+NT": 2
     *                                          },
     *                                          "2019-11-02": {
     *                                              "RD+2RH+OT": 5,
     *                                              "RD+SH+NT": 5,
     *                                              "RH+OT": 5,
     *                                              "RD+2RH+NT": 5,
     *                                              "RH+SH": 5,
     *                                              "RD+2RH": 5,
     *                                              "SH+NT+OT": 5,
     *                                              "RH+SH+NT": 5,
     *                                              "2RH": 5,
     *                                              "RD+SH+OT": 5,
     *                                              "RD+SH": 5,
     *                                              "SH+NT": 5,
     *                                              "2RH+OT": 5,
     *                                              "RD+RH+SH+NT+OT": 5,
     *                                              "SH": 5,
     *                                              "RH+SH+NT+OT": 5,
     *                                              "RD+OT": 5,
     *                                              "RH+SH+OT": 5,
     *                                              "RD+RH+OT": 5,
     *                                              "NT": 5,
     *                                              "RD+RH": 5,
     *                                              "RD+NT": 5,
     *                                              "RH+NT+OT": 5,
     *                                              "RD+NT+OT": 5,
     *                                              "RD": 5,
     *                                              "RD+RH+NT+OT": 5,
     *                                              "RD+RH+SH": 5,
     *                                              "2RH+NT": 5,
     *                                              "RD+SH+NT+OT": 5,
     *                                              "RD+RH+SH+OT": 5,
     *                                              "RD+2RH+NT+OT": 5,
     *                                              "SH+OT": 5,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 5,
     *                                              "OT": 5,
     *                                              "RH+NT": 5,
     *                                              "RD+RH+NT": 5,
     *                                              "RH": 5,
     *                                              "RD+RH+SH+NT": 5
     *                                          },
     *                                          "2019-11-03": {
     *                                              "RD+2RH+OT": 5,
     *                                              "RD+SH+NT": 5,
     *                                              "RH+OT": 5,
     *                                              "RD+2RH+NT": 5,
     *                                              "RH+SH": 5,
     *                                              "RD+2RH": 5,
     *                                              "SH+NT+OT": 5,
     *                                              "RH+SH+NT": 5,
     *                                              "2RH": 5,
     *                                              "RD+SH+OT": 5,
     *                                              "RD+SH": 5,
     *                                              "SH+NT": 5,
     *                                              "2RH+OT": 5,
     *                                              "RD+RH+SH+NT+OT": 5,
     *                                              "SH": 5,
     *                                              "RH+SH+NT+OT": 5,
     *                                              "RD+OT": 5,
     *                                              "RH+SH+OT": 5,
     *                                              "RD+RH+OT": 5,
     *                                              "NT": 5,
     *                                              "RD+RH": 5,
     *                                              "RD+NT": 5,
     *                                              "RH+NT+OT": 5,
     *                                              "RD+NT+OT": 5,
     *                                              "RD": 5,
     *                                              "RD+RH+NT+OT": 5,
     *                                              "RD+RH+SH": 5,
     *                                              "2RH+NT": 5,
     *                                              "RD+SH+NT+OT": 5,
     *                                              "RD+RH+SH+OT": 5,
     *                                              "RD+2RH+NT+OT": 5,
     *                                              "SH+OT": 5,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 5,
     *                                              "OT": 5,
     *                                              "RH+NT": 5,
     *                                              "RD+RH+NT": 5,
     *                                              "RH": 5,
     *                                              "RD+RH+SH+NT": 5
     *                                          },
     *                                          "2019-11-04": {
     *                                              "PAID_LEAVE": 8,
     *                                              "2RH": 5,
     *                                              "SH+NT": 2.1666666666667,
     *                                              "2RH+OT": 1.75,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 2.75,
     *                                              "NT": 2.75,
     *                                              "2RH+NT": 3,
     *                                              "SH+OT": 2.75,
     *                                              "2RH+NT+OT": 5,
     *                                              "NT+OT": 1,
     *                                              "OT": 2.75
     *                                          },
     *                                          "2019-11-05": {
     *                                              "OVERBREAK": 1,
     *                                              "SH+NT+OT": 2.2,
     *                                              "RH+SH+NT": 4.25,
     *                                              "UNPAID_LEAVE": 8,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+OT": 6,
     *                                              "NT": 6,
     *                                              "RH+NT+OT": 3.25,
     *                                              "SH+OT": 6,
     *                                              "OT": 6
     *                                          },
     *                                          "2019-11-06": {
     *                                              "RH+SH": 3.25,
     *                                              "PAID_LEAVE": 4,
     *                                              "UNPAID_LEAVE": 4,
     *                                              "TARDY": 1,
     *                                              "REGULAR": 8,
     *                                              "SH": 2.25,
     *                                              "RH+SH+OT": 2.5,
     *                                              "NT": 8,
     *                                              "UNDERTIME": 2.25,
     *                                              "SH+OT": 0.75,
     *                                              "2RH+NT+OT": 3.1666666666667,
     *                                              "OT": 2.75,
     *                                              "RH+NT": 2
     *                                          },
     *                                          "2019-11-07": {
     *                                              "SH+NT+OT": 3.4166666666667,
     *                                              "PAID_LEAVE": 8,
     *                                              "2RH+OT": 1,
     *                                              "REGULAR": 8,
     *                                              "RH+SH+NT+OT": 4.25,
     *                                              "RH+SH+OT": 2.75,
     *                                              "NT": 2.5,
     *                                              "RH+NT+OT": 2.2,
     *                                              "NT+OT": 5,
     *                                              "OT": 6,
     *                                              "RH+NT": 3
     *                                          },
     *                                          "2019-11-08": {
     *                                              "RH+OT": 1.4,
     *                                              "RD+SH+OT": 3.75,
     *                                              "RD+RH+SH+NT+OT": 3,
     *                                              "RH+SH+OT": 6,
     *                                              "RD+NT": 2.25,
     *                                              "RD+RH+NT+OT": 2.5,
     *                                              "RD+RH+SH": 7.5,
     *                                              "RD+RH+SH+OT": 2,
     *                                              "2RH+NT+OT": 1,
     *                                              "RH": 2,
     *                                              "RD+RH+SH+NT": 0.5
     *                                          }
     *                                      },
     *                                      "previousContributions": {
     *                                          "SSS": {},
     *                                          "HDMF": {},
     *                                          "PHILHEALTH": {}
     *                                      },
     *                                      "annualEarnings": {
     *                                          "grossCompensationIncome": 1405531.0320513,
     *                                          "nonTaxableIncome": 10500,
     *                                          "contributions": 0,
     *                                          "withholdingTax": 373001.35589744,
     *                                          "taxableIncome": 1395031.0320513,
     *                                          "breakdown": {
     *                                              "ytd": {
     *                                                  "grossCompensationIncome": 1405531.0320513,
     *                                                  "withholdingTax": 373001.35589744,
     *                                                  "nonTaxableIncome": 10500,
     *                                                  "contributions": 0,
     *                                                  "taxableIncome": 1395031.0320513
     *                                              },
     *                                              "currentEmployer": {
     *                                                  "grossCompensationIncome": 0,
     *                                                  "nonTaxableIncome": 0,
     *                                                  "contributions": 0,
     *                                                  "withholdingTax": 0,
     *                                                  "taxableIncome": 0
     *                                              },
     *                                              "previousEmployer": {
     *                                                  "grossCompensationIncome": 0,
     *                                                  "nonTaxableIncome": 0,
     *                                                  "contributions": 0,
     *                                                  "withholdingTax": 0,
     *                                                  "taxableIncome": 0
     *                                              }
     *                                          }
     *                                      }
     *                                  },
     *                                  "tax_status": "S",
     *                                  "tax_type": "REGULAR",
     *                                  "hrs_per_day": 8,
     *                                  "day_factor": 260,
     *                                  "contribution_basis": {
     *                                      "SSS": {
     *                                          "type": "NO_CONTRIBUTION",
     *                                          "amount": 0
     *                                      },
     *                                      "HDMF": {
     *                                          "type": "NO_CONTRIBUTION",
     *                                          "amount": 0
     *                                      },
     *                                      "PHILHEALTH": {
     *                                          "type": "NO_CONTRIBUTION",
     *                                          "amount": 0
     *                                      }
     *                                  }
     *                              },
     *                              "pay_schedule": {
     *                                  "cur_pay_schedule": 2,
     *                                  "max_pay_schedule": 2
     *                              },
     *                              "payroll_items": {
     *                                  "attendance": {
     *                                      "UNDERTIME": {
     *                                          "total_hours": 4.75,
     *                                          "total_amount": 913.46153846154
     *                                      },
     *                                      "TARDY": {
     *                                          "total_hours": 3.8166666666667,
     *                                          "total_amount": 733.97435897436
     *                                      },
     *                                      "RH": {
     *                                          "total_hours": 32,
     *                                          "total_amount": 12307.692307692,
     *                                          "total_add_amount": 6153.8461538462
     *                                      },
     *                                      "RH+SH": {
     *                                          "total_hours": 30.75,
     *                                          "total_amount": 15375,
     *                                          "total_add_amount": 9461.5384615385
     *                                      },
     *                                      "SH": {
     *                                          "total_hours": 20.816666666667,
     *                                          "total_amount": 5204.1666666667,
     *                                          "total_add_amount": 1200.9615384615
     *                                      },
     *                                      "2RH": {
     *                                          "total_hours": 16.383333333333,
     *                                          "total_amount": 9451.9230769231,
     *                                          "total_add_amount": 6301.282051282
     *                                      },
     *                                      "SH+NT": {
     *                                          "total_hours": 15.333333333333,
     *                                          "total_amount": 4216.6666666667,
     *                                          "total_add_amount": 3191.0256410256
     *                                      },
     *                                      "NT": {
     *                                          "total_hours": 33.75,
     *                                          "total_amount": 7139.4230769231,
     *                                          "total_add_amount": 2860.5769230769
     *                                      },
     *                                      "2RH+NT": {
     *                                          "total_hours": 16.583333333333,
     *                                          "total_amount": 10524.038461538,
     *                                          "total_add_amount": 9258.0128205128
     *                                      },
     *                                      "RH+NT": {
     *                                          "total_hours": 33.75,
     *                                          "total_amount": 14278.846153846,
     *                                          "total_add_amount": 10865.384615385
     *                                      },
     *                                      "RH+SH+NT": {
     *                                          "total_hours": 15.683333333333,
     *                                          "total_amount": 8625.8333333333,
     *                                          "total_add_amount": 7532.8846153846
     *                                      },
     *                                      "RD+SH+NT": {
     *                                          "total_hours": 16.566666666667,
     *                                          "total_amount": 5256.7307692308,
     *                                          "total_add_amount": 5256.7307692308
     *                                      },
     *                                      "RD+NT": {
     *                                          "total_hours": 14.25,
     *                                          "total_amount": 3918.75,
     *                                          "total_add_amount": 3918.75
     *                                      },
     *                                      "RD+RH+SH+NT": {
     *                                          "total_hours": 12.5,
     *                                          "total_amount": 7932.6923076923,
     *                                          "total_add_amount": 7932.6923076923
     *                                      },
     *                                      "RD+2RH+NT": {
     *                                          "total_hours": 10,
     *                                          "total_amount": 8250,
     *                                          "total_add_amount": 8250
     *                                      },
     *                                      "RD+RH+NT": {
     *                                          "total_hours": 10,
     *                                          "total_amount": 5500,
     *                                          "total_add_amount": 5500
     *                                      },
     *                                      "RD+SH": {
     *                                          "total_hours": 12,
     *                                          "total_amount": 3461.5384615385
     *                                      },
     *                                      "RD+RH": {
     *                                          "total_hours": 16.566666666667,
     *                                          "total_amount": 8283.3333333334
     *                                      },
     *                                      "RD": {
     *                                          "total_hours": 16,
     *                                          "total_amount": 4000
     *                                      },
     *                                      "RD+RH+SH": {
     *                                          "total_hours": 19.083333333333,
     *                                          "total_amount": 11009.615384615
     *                                      },
     *                                      "RD+2RH": {
     *                                          "total_hours": 10,
     *                                          "total_amount": 7500
     *                                      },
     *                                      "SH+NT+OT": {
     *                                          "total_hours": 34.533333333333,
     *                                          "total_amount": 12345.666666667
     *                                      },
     *                                      "RH+SH+NT+OT": {
     *                                          "total_hours": 18,
     *                                          "total_amount": 12870
     *                                      },
     *                                      "RH+NT+OT": {
     *                                          "total_hours": 20.2,
     *                                          "total_amount": 11110
     *                                      },
     *                                      "2RH+NT+OT": {
     *                                          "total_hours": 23.566666666667,
     *                                          "total_amount": 19442.5
     *                                      },
     *                                      "NT+OT": {
     *                                          "total_hours": 27.5,
     *                                          "total_amount": 7271.6346153846
     *                                      },
     *                                      "OT": {
     *                                          "total_hours": 45,
     *                                          "total_amount": 10817.307692308
     *                                      },
     *                                      "RH+OT": {
     *                                          "total_hours": 18.983333333333,
     *                                          "total_amount": 9491.6666666667
     *                                      },
     *                                      "2RH+OT": {
     *                                          "total_hours": 15.95,
     *                                          "total_amount": 11962.5
     *                                      },
     *                                      "RH+SH+OT": {
     *                                          "total_hours": 47,
     *                                          "total_amount": 30550
     *                                      },
     *                                      "SH+OT": {
     *                                          "total_hours": 25.7,
     *                                          "total_amount": 8352.5
     *                                      },
     *                                      "RD+2RH+OT": {
     *                                          "total_hours": 16.566666666667,
     *                                          "total_amount": 16152.5
     *                                      },
     *                                      "RD+RH+SH+NT+OT": {
     *                                          "total_hours": 19.566666666667,
     *                                          "total_amount": 16142.5
     *                                      },
     *                                      "RD+OT": {
     *                                          "total_hours": 11.25,
     *                                          "total_amount": 3656.25
     *                                      },
     *                                      "RD+RH+OT": {
     *                                          "total_hours": 12,
     *                                          "total_amount": 7800
     *                                      },
     *                                      "RD+NT+OT": {
     *                                          "total_hours": 12,
     *                                          "total_amount": 4290
     *                                      },
     *                                      "RD+RH+NT+OT": {
     *                                          "total_hours": 14.5,
     *                                          "total_amount": 10367.5
     *                                      },
     *                                      "RD+2RH+NT+OT": {
     *                                          "total_hours": 16.566666666667,
     *                                          "total_amount": 17767.75
     *                                      },
     *                                      "RD+SH+OT": {
     *                                          "total_hours": 13.75,
     *                                          "total_amount": 5156.25
     *                                      },
     *                                      "RD+SH+NT+OT": {
     *                                          "total_hours": 10,
     *                                          "total_amount": 4125
     *                                      },
     *                                      "RD+RH+SH+OT": {
     *                                          "total_hours": 12,
     *                                          "total_amount": 9000
     *                                      }
     *                                  },
     *                                  "adjustments": {
     *                                      "TAX_ADJUSTMENT": {},
     *                                      "TAXABLE_INCOME": {
     *                                          {
     *                                              "type": "TAXABLE_INCOME",
     *                                              "amount": 500,
     *                                              "reason": "For Qa Testing",
     *                                              "meta": {
     *                                                  "id": 3042,
     *                                                  "subtype_id": 247,
     *                                                  "subtype_name": "PhilippineAdjustment",
     *                                                  "other_income_type_id": 3,
     *                                                  "other_income_type_subtype_type_id": 1
     *                                              }
     *                                          }
     *                                      },
     *                                      "NON_TAXABLE_INCOME": {},
     *                                      "NON_INCOME": {}
     *                                  },
     *                                  "leaves": {
     *                                      "UNPAID_LEAVE": {
     *                                          "total_hours": 28,
     *                                          "total_amount": 5384.6153846154
     *                                      },
     *                                      "PAID_LEAVE": {
     *                                          "total_hours": 44,
     *                                          "total_amount": 8461.5384615385
     *                                      }
     *                                  },
     *                                  "deductions": {
     *                                      {
     *                                          "type": "Mobile Plans 83",
     *                                          "amount": 1500,
     *                                          "meta": {
     *                                              "id": 3930,
     *                                              "subtype_id": 492,
     *                                              "subtype_name": "PhilippineDeduction",
     *                                              "other_income_type_id": 15347,
     *                                              "other_income_type_subtype_type_id": 1771
     *                                          }
     *                                      },
     *                                      {
     *                                          "type": "Mobile Plans 88",
     *                                          "amount": 1500,
     *                                          "meta": {
     *                                              "id": 3973,
     *                                              "subtype_id": 504,
     *                                              "subtype_name": "PhilippineDeduction",
     *                                              "other_income_type_id": 15390,
     *                                              "other_income_type_subtype_type_id": 1783
     *                                          }
     *                                      }
     *                                  },
     *                                  "other_incomes": {
     *                                      "ALLOWANCE": {
     *                                          "taxable": {
     *                                              {
     *                                                  "type": "final_pay_allowance5 non tax",
     *                                                  "amount": 2000,
     *                                                  "meta": {
     *                                                      "id": 3082,
     *                                                      "subtype_id": 1104,
     *                                                      "subtype_name": "PhilippineAllowance",
     *                                                      "other_income_type_id": 13905,
     *                                                      "other_income_type_subtype_type_id": 1738,
     *                                                      "basis_amount": 5000,
     *                                                      "given_amount": 5000,
     *                                                      "deducted_amount": 0,
     *                                                      "max_non_taxable": 3000,
     *                                                      "taxable_split": 2000
     *                                                  }
     *                                              }
     *                                          },
     *                                          "non_taxable": {
     *                                              {
     *                                                  "type": "final_pay_allowance5 non tax",
     *                                                  "amount": 3000,
     *                                                  "meta": {
     *                                                      "id": 3082,
     *                                                      "subtype_id": 1104,
     *                                                      "subtype_name": "PhilippineAllowance",
     *                                                      "other_income_type_id": 13905,
     *                                                      "other_income_type_subtype_type_id": 1738,
     *                                                      "basis_amount": 5000,
     *                                                      "given_amount": 5000,
     *                                                      "deducted_amount": 0,
     *                                                      "max_non_taxable": 3000,
     *                                                      "taxable_split": 2000
     *                                                  }
     *                                              }
     *                                          }
     *                                      },
     *                                      "BONUS": {
     *                                          "taxable": {
     *                                              {
     *                                                  "type": "Payroll Group Bonus Periodic",
     *                                                  "amount": 18375.534188034,
     *                                                  "meta": {
     *                                                      "id": 4255,
     *                                                      "subtype_id": 1301,
     *                                                      "subtype_name": "PhilippineBonus",
     *                                                      "other_income_type_id": 16022,
     *                                                      "other_income_type_subtype_type_id": 8610,
     *                                                      "release_amount": 18375.534188034,
     *                                                      "tax_exempt_amount": 0,
     *                                                      "taxed_amount": 18375.534188034
     *                                                  }
     *                                              }
     *                                          },
     *                                          "non_taxable": {}
     *                                      },
     *                                      "COMMISSION": {
     *                                          "taxable": {
     *                                              {
     *                                                  "type": "Final_Pay_Taxable_Commission",
     *                                                  "amount": 3000,
     *                                                  "meta": {
     *                                                      "id": 3067,
     *                                                      "subtype_id": 299,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              },
     *                                              {
     *                                                  "type": "Final_Pay_Taxable_Commission",
     *                                                  "amount": 3000,
     *                                                  "meta": {
     *                                                      "id": 3073,
     *                                                      "subtype_id": 305,
     *                                                      "subtype_name": "PhilippineCommission",
     *                                                      "other_income_type_id": 13352,
     *                                                      "other_income_type_subtype_type_id": 2667
     *                                                  }
     *                                              }
     *                                          },
     *                                          "non_taxable": {}
     *                                      }
     *                                  },
     *                                  "loan_amortizations": {
     *                                      "GOVERNMENT": {},
     *                                      "OTHER": {
     *                                          {
     *                                              "type": "Final_Pay_Car-Loan",
     *                                              "amount": 1525,
     *                                              "due_date": "2019-12-13",
     *                                              "meta": {"id": 50549, "loanId": 326,
 "subtype": "", "employerRemarks": null, "principalAmount": 6000, "referenceNumber": ""}
     *                                          },
     *                                          {
     *                                              "type": "Final_Pay_Car-Loan",
     *                                              "amount": 1425,
     *                                              "due_date": "2019-12-30",
     *                                              "meta": {"id": 50550, "loanId": 326,
 "subtype": "", "employerRemarks": null, "principalAmount": 6000, "referenceNumber": ""}
     *                                          }
     *                                      }
     *                                  },
     *                                  "base_pay": 400000,
     *                                  "base_pay_unit": "YEARLY",
     *                                  "monthly_income": 33333.333333333,
     *                                  "daily_rate": 1538.4615384615,
     *                                  "hourly_rate": 192.30769230769,
     *                                  "full_pay": 24615.384615385,
     *                                  "gross_basic": 368193.03205128,
     *                                  "basic_pay": 17583.333333333,
     *                                  "taxable_income": 1790099.5982906,
     *                                  "total_reduce_taxable_deminimis_amount": 0,
     *                                  "total_non_taxable_deminimis_amount": 0,
     *                                  "gross": 398068.56623932,
     *                                  "withholding_tax": 54028.523589745,
     *                                  "total_deduction": 59978.523589745,
     *                                  "net_pay": 338090.04264957,
     *                                  "tax_status": "S",
     *                                  "tax_type": "REGULAR",
     *                                  "tax_rate": null,
     *                                  "sss_employee": 0,
     *                                  "hdmf_employee": 0,
     *                                  "philhealth_employee": 0,
     *                                  "total_mandatory_employee": 0,
     *                                  "refund_sss_employee": 0,
     *                                  "refund_hdmf_employee": 0,
     *                                  "refund_philhealth_employee": 0,
     *                                  "total_mandatory_employee_refund": 0,
     *                                  "sss_employer": 0,
     *                                  "sss_ec_employer": 0,
     *                                  "hdmf_employer": 0,
     *                                  "philhealth_employer": 0,
     *                                  "total_mandatory_employer": 0,
     *                                  "total_scheduled_hours": 64,
     *                                  "total_worked_hours": 579.03333333333
     *                              }
     *                          }
     *                      },
     *                      "time_attendance": {
     *                          "time_attendance_id": null,
     *                          "full_name": "Exo Kpop",
     *                          "address_line_1": "1st  Street",
     *                          "address_line_2": "Brgy 12",
     *                          "department_name": "Product",
     *                          "rank_name": "Product",
     *                          "employment_type_id": null,
     *                          "employment_type_name": null,
     *                          "position_id": null,
     *                          "position_name": null,
     *                          "team_id": null,
     *                          "team_name": null,
     *                          "team_role": null,
     *                          "primary_location_id": null,
     *                          "primary_location_name": null,
     *                          "secondary_location_id": null,
     *                          "secondary_location_name": null,
     *                          "status": null,
     *                          "user_id": 146,
     *                          "timesheet_required": false,
     *                          "overtime": false,
     *                          "differential": false,
     *                          "regular_holiday_pay": false,
     *                          "special_holiday_pay": false,
     *                          "holiday_premium_pay": false,
     *                          "rest_day_pay": false
     *                      }
     *                  }
     *              },
     *              "Successfully found the employee, no final pay projection": {
     *                  "data": {
     *                      "id": 271,
     *                      "account_id": 9,
     *                      "company_id": 22,
     *                      "employee_id": "pr_10232019-12",
     *                      "first_name": "Exo",
     *                      "middle_name": "",
     *                      "last_name": "Kpop",
     *                      "full_name": "Exo Kpop",
     *                      "email": "test+local.exokpoppr_10232019-12@salarium.com",
     *                      "gender": "female",
     *                      "birth_date": "1984-10-15",
     *                      "telephone_number": 4425159,
     *                      "mobile_number": "(63)9954132492",
     *                      "hours_per_day": null,
     *                      "date_hired": "2010-10-16",
     *                      "date_ended": "2020-01-31",
     *                      "location_id": 5,
     *                      "location_name": "Makati",
     *                      "department_id": 7,
     *                      "department_name": "Product",
     *                      "rank_id": 7,
     *                      "rank_name": "Junior. Quality Assurance Analyst",
     *                      "position_id": 7,
     *                      "position_name": "QA",
     *                      "address": {
     *                          "address_line_1": "1st  Street",
     *                          "address_line_2": "Brgy 12",
     *                          "city": "Baliuag",
     *                          "zip_code": "14501",
     *                          "country": "Philippines"
     *                      },
     *                      "active": false,
     *                      "non_taxed_other_income_limit": "90000.0000",
     *                      "user_id": 146,
     *                      "payroll_group": {
     *                          "id": 24,
     *                          "name": "pg1 semi monthly",
     *                          "company_id": 22,
     *                          "payroll_frequency": "SEMI_MONTHLY",
     *                          "day_factor": "260.00",
     *                          "hours_per_day": "8.00",
     *                          "non_working_day_option": "BEFORE",
     *                          "pay_date": "2019-10-15 00:00:00",
     *                          "pay_run_posting": "2019-10-09 00:00:00",
     *                          "cut_off_date": "2019-10-09 00:00:00",
     *                          "pay_date_2": "2019-10-31 00:00:00",
     *                          "pay_run_posting_2": "2019-10-25 00:00:00",
     *                          "cut_off_date_2": "2019-10-25 00:00:00",
     *                          "fpd_end_of_month": false,
     *                          "fpp_end_of_month": false,
     *                          "fcd_end_of_month": false,
     *                          "spd_end_of_month": true,
     *                          "spp_end_of_month": false,
     *                          "scd_end_of_month": false,
     *                          "payroll_group_subtype_id": 24,
     *                          "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
     *                          "compute_overbreak": 0,
     *                          "compute_undertime": 1,
     *                          "compute_tardiness": 1,
     *                          "created_at": "2019-11-08 10:57:27",
     *                          "updated_at": "2019-11-08 13:12:10",
     *                          "deleted_at": null,
     *                          "enforce_gap_loans": false,
     *                          "minimum_net_take_home_pay": null
     *                      },
     *                      "updated_at": {
     *                          "date": "2020-01-09 14:55:11.000000",
     *                          "timezone_type": 3,
     *                          "timezone": "Asia/Manila"
     *                      },
     *                      "termination_information": {
     *                          "id": 2,
     *                          "employee_id": 271,
     *                          "company_id": 22,
     *                          "last_date": "2020-01-31",
     *                          "start_date_basis": "2020-01-01",
     *                          "release_date": "2020-01-31",
     *                          "reason_for_leaving": "RESIGNATION",
     *                          "remarks": null,
     *                          "created_at": "2020-01-09 14:55:11",
     *                          "updated_at": "2020-01-09 14:55:11"
     *                      },
     *                      "team_id": null,
     *                      "payroll": {
     *                          "payroll_group_id": 24,
     *                          "payroll_group_name": "pg1 semi monthly",
     *                          "hours_per_day": null,
     *                          "date_hired": "2010-10-16",
     *                          "date_ended": "2020-01-31",
     *                          "cost_center_id": null,
     *                          "cost_center_name": null,
     *                          "tax_status": "S",
     *                          "tax_type": "Minimum Wage",
     *                          "consultant_tax_rate": null,
     *                          "tin": "131-456-789-019",
     *                          "sss_number": "07-7451477-8",
     *                          "philhealth_number": "82-560045513-0",
     *                          "hdmf_number": "4177-3254-4805",
     *                          "sss_basis": "Basic Pay",
     *                          "philhealth_basis": "Basic Pay",
     *                          "hdmf_basis": "Basic Pay",
     *                          "sss_amount": null,
     *                          "philhealth_amount": null,
     *                          "hdmf_amount": null,
     *                          "rdo": "14",
     *                          "entitled_deminimis": 1,
     *                          "payment_method": "Bank",
     *                          "bank_name": "UNION BANK OF THE PHILIPPINES",
     *                          "bank_account_number": "123456789",
     *                          "bank_type": "Current",
     *                          "base_pay": "300,000.00",
     *                          "base_pay_unit": "PER MONTH"
     *                      },
     *                      "final_pay_projection": null,
     *                      "time_attendance": {
     *                          "time_attendance_id": null,
     *                          "full_name": "Exo Kpop",
     *                          "address_line_1": "1st  Street",
     *                          "address_line_2": "Brgy 12",
     *                          "department_name": "Product",
     *                          "rank_name": "Product",
     *                          "employment_type_id": null,
     *                          "employment_type_name": null,
     *                          "position_id": null,
     *                          "position_name": null,
     *                          "team_id": null,
     *                          "team_name": null,
     *                          "team_role": null,
     *                          "primary_location_id": null,
     *                          "primary_location_name": null,
     *                          "secondary_location_id": null,
     *                          "secondary_location_name": null,
     *                          "status": null,
     *                          "user_id": 146,
     *                          "timesheet_required": false,
     *                          "overtime": false,
     *                          "differential": false,
     *                          "regular_holiday_pay": false,
     *                          "special_holiday_pay": false,
     *                          "holiday_premium_pay": false,
     *                          "rest_day_pay": false
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Employee not found.",
     *         examples={
     *              "Employee not found": {
     *                  "message": "Employee not found.",
     *                  "status_code": 404
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request.",
     *         examples={
     *              "Employee ID should not be empty": {
     *                  "message": "Employee ID should not be empty.",
     *                  "status_code": 406
     *              },
     *              "Employee ID should be numeric": {
     *                  "message": "Employee ID should be numeric.",
     *                  "status_code": 406
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
     *         description="Internal Server Error.",
     *         examples={
     *              "Internal Server Error": {
     *                  "message": "500 Internal Server Error.",
     *                  "status_code": 500
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized.",
     *         examples={
     *              "Unauthorized": {
     *                  "message": "Unauthorized.",
     *                  "status_code": 401
     *              }
     *         }
     *     )
     * )
     */
    public function getEmployee($id, Request $request)
    {
        $response = $this->requestService->getEmployeeInfo($id);
        $employeeData = json_decode($response->getData(), true);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');

            $isAuthorized = $dataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $employeeData['company_id'],
                AuthzDataScope::SCOPE_DEPARTMENT => $employeeData['department_id'] ?? null,
                AuthzDataScope::SCOPE_POSITION => $employeeData['position_id'] ?? null,
                AuthzDataScope::SCOPE_LOCATION => $employeeData['location_id'] ?? null,
                AuthzDataScope::SCOPE_TEAM => $employeeData['time_attendance']['team_id'] ?? null,
                AuthzDataScope::SCOPE_PAYROLL_GROUP => $employeeData['payroll']['payroll_group_id'] ?? null
            ]);
        } else {
            $companyId = $employeeData['company_id'];
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $viewer = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeViewCompanyEmployees(
                $companyData,
                $viewer
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getEmployeeInfo($id);
        $responseData = json_decode($response->getData(), true);

        return $response->setData($responseData);
    }

    /**
     * @SWG\Patch(
     *     path="/employee/{id}",
     *     summary="Update employee info, time attendance, philippine employee",
     *     description="Update employee info with time attendance or philippine employee or both
Authorization Scope : **edit.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Philippine Employee Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "payroll_group_name"={"type"="string"},
     *                 "employee_id"={"type"="string"},
     *                 "first_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "email"={"type"="string"},
     *                 "gender"={"type"="string"},
     *                 "birth_date"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "mobile_number"={"type"="string"},
     *                 "base_pay"={"type"="string"},
     *                 "base_pay_unit"={"type"="string"},
     *                 "hours_per_day"={"type"="string"},
     *                 "date_hired"={"type"="string"},
     *                 "date_ended"={"type"="string"},
     *                 "location_name"={"type"="string"},
     *                 "department_name"={"type"="string"},
     *                 "rank_name"={"type"="string"},
     *                 "cost_center_name"={"type"="string"},
     *                 "position_name"={"type"="string"},
     *                 "tax_status"={"type"="string"},
     *                 "tax_type"={"type"="string"},
     *                 "consultant_tax_rate"={"type"="string"},
     *                 "tin"={"type"="string"},
     *                 "sss_number"={"type"="string"},
     *                 "philhealth_number"={"type"="string"},
     *                 "hdmf_number"={"type"="string"},
     *                 "sss_basis"={"type"="string"},
     *                 "philhealth_basis"={"type"="string"},
     *                 "hdmf_basis"={"type"="string"},
     *                 "sss_amount"={"type"="number"},
     *                 "philhealth_amount"={"type"="number"},
     *                 "hdmf_amount"={"type"="number"},
     *                 "rdo"={"type"="string"},
     *                 "address_line_1"={"type"="string"},
     *                 "address_line_2"={"type"="string"},
     *                 "city"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "regular_holiday_pay"={"type"="string"},
     *                 "special_holiday_pay"={"type"="string"},
     *                 "holiday_premium_pay"={"type"="string"},
     *                 "timesheet_required"={"type"="string"},
     *                 "overtime"={"type"="string"},
     *                 "differential"={"type"="string"},
     *                 "rest_day_pay"={"type"="string"},
     *                 "primary_location_name"={"type"="string"},
     *                 "secondary_location_name"={"type"="string"},
     *                 "employment_type_name"={"type"="string"},
     *                 "team_name"={"type"="string"},
     *                 "team_role"={"type"="string"},
     *                 "country"={"type"="string"},
     *                 "active"={"type"="boolean"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateEmployee(
        $id,
        Request $request,
        EmployeeESIndexQueueService $indexQueueService,
        AuditService $auditService,
        Auth0UserService $auth0UserService
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $attributes = $request->all();
        $editedBy = $request->attributes->get('user');

        $response = $this->requestService->getEmployeeInfo($id);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());

        // authorize
        if (
            !$this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            )
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->updateEmployee($id, $attributes);

        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        $responseData     = $responseData['data'];
        $companyUserData  = $responseData['user'];

        // create company user record
        // backwards compatibility for #491
        $companyUser = $this->companyUserService->getByUserId($companyUserData['id']);
        if (empty($companyUser)) {
            $companyUser = $this->companyUserService->create([
                'user_id'     => $companyUserData['id'],
                'company_id'  => $responseData['company_id'],
                'employee_id' => $responseData['id']
            ]);
        }

        // create preactive auth0 record
        if (empty($auth0UserService->getUser($companyUserData['id']))) {
            $auth0UserService->create([
                'auth0_user_id' => Auth0UserService::generatePreactiveId(
                    $responseData['account_id'],
                    $companyUser->user_id
                ),
                'user_id'       => $companyUser->user_id,
                'account_id'    => $responseData['account_id'],
                'status'        => Auth0User::STATUS_INACTIVE
            ]);
        }

        // if email is changed, check if user exists in Auth0, then update email there too
        if (isset($attributes['email'])) {
            $this->processEmployeeEmailUpdate($responseData);
        }

        // employee's activation or deactivation
        if (isset($attributes['active'])) {
            $this->processEmployeeActiveStatus(
                $responseData,
                $attributes['active'],
                $auth0UserService->getUserByEmployeeId($responseData['id'])
            );
        }

        $auth0User = $auth0UserService->getUserByEmployeeId($responseData['id']);
        $responseData['auth0_active'] = (!empty($auth0User) && !Auth0UserService::isPreactive($auth0User))
            ? true
            : false;

        $indexQueueService->queue([$id]);

        $details = [
            'old' => $employeeData,
            'new' => $responseData,
            'company_id' => $companyId,
            'employee_id' => $id,
        ];

        $item = new AuditCacheItem(
            EmployeeAuditService::class,
            EmployeeAuditService::ACTION_EDIT,
            new AuditUser($editedBy['user_id'], $editedBy['account_id']),
            $details
        );

        $auditService->queue($item);

        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);

        return $response->setData($responseData);
    }

    /**
     * @SWG\Put(
     *     path="/employee/{id}",
     *     summary="Update employee info, time attendance, philippine employee based on product seat id",
     *     description="Update employee info with time attendance or philippine employee or both
Authorization Scope : **edit.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Philippine Employee Data",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "properties"={
     *                 "payroll_group_name"={"type"="string"},
     *                 "employee_id"={"type"="string"},
     *                 "first_name"={"type"="string"},
     *                 "middle_name"={"type"="string"},
     *                 "last_name"={"type"="string"},
     *                 "email"={"type"="string"},
     *                 "gender"={"type"="string"},
     *                 "birth_date"={"type"="string"},
     *                 "telephone_number"={"type"="string"},
     *                 "mobile_number"={"type"="string"},
     *                 "base_pay"={"type"="string"},
     *                 "base_pay_unit"={"type"="string"},
     *                 "hours_per_day"={"type"="string"},
     *                 "date_hired"={"type"="string"},
     *                 "date_ended"={"type"="string"},
     *                 "location_name"={"type"="string"},
     *                 "department_name"={"type"="string"},
     *                 "rank_name"={"type"="string"},
     *                 "cost_center_name"={"type"="string"},
     *                 "position_name"={"type"="string"},
     *                 "tax_status"={"type"="string"},
     *                 "tax_type"={"type"="string"},
     *                 "consultant_tax_rate"={"type"="string"},
     *                 "tin"={"type"="string"},
     *                 "sss_number"={"type"="string"},
     *                 "philhealth_number"={"type"="string"},
     *                 "hdmf_number"={"type"="string"},
     *                 "sss_basis"={"type"="string"},
     *                 "philhealth_basis"={"type"="string"},
     *                 "hdmf_basis"={"type"="string"},
     *                 "sss_amount"={"type"="number"},
     *                 "philhealth_amount"={"type"="number"},
     *                 "hdmf_amount"={"type"="number"},
     *                 "rdo"={"type"="string"},
     *                 "entitled_deminimis"={"type"="string"},
     *                 "payment_method"={"type"="string"},
     *                 "bank_name"={"type"="string"},
     *                 "bank_account_number"={"type"="string"},
     *                 "bank_type"={"type"="string"},
     *                 "address_line_1"={"type"="string"},
     *                 "address_line_2"={"type"="string"},
     *                 "city"={"type"="string"},
     *                 "zip_code"={"type"="string"},
     *                 "regular_holiday_pay"={"type"="string"},
     *                 "special_holiday_pay"={"type"="string"},
     *                 "holiday_premium_pay"={"type"="string"},
     *                 "timesheet_required"={"type"="string"},
     *                 "overtime"={"type"="string"},
     *                 "differential"={"type"="string"},
     *                 "rest_day_pay"={"type"="string"},
     *                 "primary_location_name"={"type"="string"},
     *                 "secondary_location_name"={"type"="string"},
     *                 "employment_type_name"={"type"="string"},
     *                 "team_name"={"type"="string"},
     *                 "team_role"={"type"="string"},
     *                 "country"={"type"="string"},
     *                 "active"={"type"="string"}
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function updateTAPayrollEmployee(
        $id,
        Request $request,
        Auth0UserService $auth0UserService
    ) {
        $bearerToken = $request->headers->get('Authorization');

        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $isEmailUpdate  = false;
        $attributes     = $request->all();
        $attributes['bearer'] = $bearerToken;

        $response       = $this->requestService->getEmployeeInfo($id);
        $employeeData   = json_decode($response->getData(), true);
        $companyId      = $employeeData['company_id'];

        if (empty($employeeData['company'])) {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
        } else {
            $companyData = $employeeData['company'];
        }

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (!isset($attributes['product_seat_id'])) {
            $this->invalidRequestError('product_seat_id is required');
        }

        if (!empty($attributes['email']) && $attributes['email'] != $employeeData['email']) {
            $authnRequestService = app()->make(AuthnRequestService::class);
            $authnData = $authnRequestService->getAuthnUserProfileByEmail($bearerToken, $attributes['email']);
            $authnResponseData = json_decode($authnData->getData(), true);

            if (!empty($authnResponseData['data'])) {
                ## If existing, error, already exist.
                $this->invalidRequestError('Email ' . $attributes['email'] . ' already exist');
            }
        }

        $response = $this->requestService->updateTAPayrollEmployee(
            $id,
            $attributes,
            $this->isAuthzEnabled($request) ? $this->getAuthzDataScope($request) : null
        );

        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $responseData = json_decode($response->getData(), true);

        try {
            $companyUser = $this->companyUserService->getByUserAndCompanyId($responseData['user_id'], $companyId);
            if (empty($companyUser)) {
                $companyUser = $this->companyUserService->create([
                    'user_id'     => $responseData['user_id'],
                    'company_id'  => $companyId,
                    'employee_id' => $responseData['id']
                ]);
            } else {
                $companyUser = $this->companyUserService->update($responseData['user_id'], $companyId, [
                    'employee_id' => $responseData['id']
                ]);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . ' : Could not handle create companyUser');
        }

        // if email is changed, check if user exists in Auth0, then update email there too
        if (!empty($attributes['email']) && $attributes['email'] != $employeeData['email']) {
            ## Check target email existence in auth0

            // update user status
            $userRequestService = app()->make(UserRequestService::class);
            $userResponse = $userRequestService->getUserStatus($responseData['user_id']);
            $userResponseData = json_decode($userResponse->getData(), true);

            if ($userResponseData['status'] === 'active') {
                # Set user to inactive so admin can set to active again and email verification will send.
                $userRequestService->setStatus($responseData['user_id'], 'inactive');
            }

            if ($userResponseData['status'] === 'active') {
                # Set user to active
                $userRequestService->setStatus($responseData['user_id'], 'active');
            }

            # Owner email update handling
            $getAccountDetails = $this->accountRequestService->getAccountDetailsByUserId($responseData['user_id']);
            $accountData = json_decode($getAccountDetails->getData(), true);
            $accountEmail = $accountData['data']['email'] ?? [];
            $userEmail = $employeeData['email'];

            if ($userEmail === $accountEmail) {
                $this->accountRequestService->updateAccount($responseData['account_id'], $responseData);

                $subscriptionsRequestService = app()->make(SubscriptionsRequestService::class);
                $customerData = $subscriptionsRequestService->searchCustomerDetailsByUserId($responseData['user_id']);
                $customerData = json_decode($customerData->getData(), true);
                $customerData = $customerData['data'][0] ?? null;

                if (!empty($customerData)) {
                    $subscriptionsRequestService->updateCustomer($customerData['id'], $responseData);
                }
            }
            $isEmailUpdate = true;
        }

        $responseData['update_email'] = $isEmailUpdate;
        $responseData['user_id'] = '';

        unset($responseData['company']);

        $this->audit($request, $companyId, $responseData, $employeeData, true);

        // Delete Company Calendar Data Cache
        Redis::del('calendar-data:company_id:' . $companyId);

        return $response->setData($responseData);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/ta_employees/search",
     *     summary="Search company employees with time and attendance by first, middle and last name",
     *     description="Get searched list of employees by first, middle and
     *      last name with time and attendance within a certain company
Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Search limit",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function searchCompanyTaEmployees($id, Request $request)
    {
        $companyResponse = $this->companyRequestService->get($id);
        $companyData = json_decode($companyResponse->getData());
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeViewCompanyEmployees($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->searchCompanyTaEmployees($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_update/personal_info",
     *     summary="Update Personal Info",
     *     description="
Used for batch updating Personal Info.
It requires the CSV file which contains new personal information for existing employees.


The endpoint will enqueue the task for validation if all preliminary checks are passed.
Upon successful validation, the new personal info is automatically saved.
There is no “save” step for Batch Update.


Once this returns a successful response, the endpoint for fetching the upload status should be used.

Authorization Scope : **edit.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function updateUploadPersonalInfo(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');
        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUpdateUploadTask::class);
            $uploadTask->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PERSONAL);
            $uploadTask->setUserId($createdBy['user_id']);
            $s3Key = $uploadTask->savePersonalInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUpdateUploadTask::PROCESS_PERSONAL,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $authzDataScope ? $authzDataScope->getDataScope() : []
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_update_validation_queue'), $message);
            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);

            try {
                $this->audit($request, $companyId, ['Mass Update Personal Info Trigger']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_update/people",
     *     summary="Update People",
     *     description="Updates Employee Information (personal, payroll & time and attendance)

Authorization Scope : **edit.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_update_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function updateUploadPeople(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');
        $crbacDataScope = [];
        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $crbacDataScope = $authzDataScope->getDataScope();
            $authorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // authorize
            $authorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUpdateUploadTask::class);
            $uploadTask->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PEOPLE);
            $uploadTask->setUserId($createdBy['user_id']);
            $s3Key = $uploadTask->savePeopleInfo($file->getPathName());
            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUpdateUploadTask::PROCESS_PEOPLE,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $crbacDataScope,
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.people_update_validation_queue'), $message);

            try {
                $this->audit($request, $companyId, ['Mass Update People Trigger']);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/employee/batch_update/payroll_info",
     *     summary="Update Payroll Info",
     *     description="
Used for batch updating Payroll Info.
It requires the CSV file which contains new payroll information for existing employees.

The endpoint will enqueue the task for validation if all preliminary checks are passed.
Upon successful validation, the new payroll info is automatically saved.
There is no “save” step for Batch Update.

Once this returns a successful response, the endpoint for fetching the upload status should be used.

Authorization Scope : **edit.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function updateUploadPayrollInfo(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $createdBy = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUpdateUploadTask::class);
            $uploadTask->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_PAYROLL);
            $uploadTask->setUserId($createdBy['user_id']);
            $s3Key = $uploadTask->savePersonalInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUpdateUploadTask::PROCESS_PAYROLL,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => !empty($authzDataScope) ? $authzDataScope->getDataScope() : []
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_update_validation_queue'), $message);
            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);
            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }


    /**
     * @SWG\Post(
     *     path="/employee/batch_update/time_attendance_info",
     *     summary="Update Time and Attendance Info",
     *     description="
Used for batch updating Time and Attendance Info.
It requires the CSV file which contains a new time and attendance information for existing employees.

The endpoint will enqueue the task for validation if all preliminary checks are passed.
Upon successful validation, the new time and attendance info is automatically saved.
There is no “save” step for Batch Update.

Once this returns a successful response, the endpoint for fetching the upload status should be used.

Authorization Scope : **edit.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "employee_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function updateUploadTimeAttendanceInfo(Request $request)
    {
        $companyId = $request->input('company_id');
        $createdBy = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(EmployeeUpdateUploadTask::class);
            $uploadTask->create($companyId, null, EmployeeUpdateUploadTask::PROCESS_TIME_ATTENDANCE);
            $uploadTask->setUserId($createdBy['user_id']);
            $s3Key = $uploadTask->saveTimeAttendanceInfo($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EmployeeUpdateUploadTask::PROCESS_TIME_ATTENDANCE,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => $authzDataScope ? $authzDataScope->getDataScope() : []
            ];
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
            Amqp::publish(config('queues.employee_update_validation_queue'), $message);
            // Delete Company Calendar Data Cache
            Redis::del('calendar-data:company_id:' . $companyId);
            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/employee/batch_update/status",
     *     summary="Get Employee Batch Update Status",
     *     description="
Checks upload status for a given job id.

It always returns the current status of the job, which can be used by the frontend by polling this endpoint.

If the job fails, the response will include an array of errors.

Authorization Scope : **edit.employee**
",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Email is invalid",
     *                              "Payroll Group does not exist"
     *                          },
     *                          "4": {
     *                              "Email is invalid",
     *                              "Payroll Group does not exist"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     )
     * )
     */
    public function updateUploadStatus(Request $request)
    {
        $isAuthorized = false;

        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(EmployeeUpdateUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (EmployeeUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $process = $request->input('step') ?? ($uploadTask->fetch(['process']))[0];

        $fields = [
            $process . '_status',
            'save_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === EmployeeUploadTask::STATUS_VALIDATION_FAILED ||
            $details['save_status'] === EmployeeUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }

        //batch update
        if ($details['save_status'] === 'saved') {
            $keys = Redis::sMembers($jobId . ':personal:set');
            foreach ($keys as $key) {
                $email = Redis::hGet(
                    $key,
                    'email'
                );

                if ($email !== null) {
                    $user = $this->userRequestService->getUserByEmail($email);
                    $user = json_decode($user->getData(), true);
                    $auth0UserFromDB = $this->auth0UserService->getUser($user['id']);

                    try {
                        $auth0ManagementService = app()->make(Auth0ManagementService::class);
                        $userAuth0Profile = $auth0ManagementService->getAuth0UserProfile(
                            $auth0UserFromDB->auth0_user_id
                        );

                        if ($user['status'] === 'inactive') {
                            $this->auth0UserService->updateEmployeeUser(
                                $auth0UserFromDB,
                                ['email' => $user['email']]
                            );
                            if ($userAuth0Profile !== null) {
                                $auth0ManagementService->updateUser(
                                    $auth0UserFromDB->auth0_user_id,
                                    [
                                        'connection' => 'Username-Password-Authentication',
                                        'email' => $user['email'],
                                        'email_verified' => false,
                                        'verify_email' => false,
                                        'client_id' => env('MANAGEMENT_CLIENT_ID')
                                    ]
                                );
                            }
                        } else {
                            if ($userAuth0Profile !== null) {
                                $auth0ManagementService->updateUser(
                                    $auth0UserFromDB->auth0_user_id,
                                    [
                                        'connection' => 'Username-Password-Authentication',
                                        'email' => $user['email'],
                                        'email_verified' => false,
                                        'verify_email' => true,
                                        'client_id' => env('MANAGEMENT_CLIENT_ID')
                                    ]
                                );
                            }
                        }
                    } catch (\Throwable $e) {
                        \Log::info('Could not update user email on auth0 : ' . $e->getMessage());
                    }
                }
            }
            try {
                $this->audit($request, $companyId, ['Mass Update People Status' => $details['save_status']]);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }

        return response()->json([
            'status' => $details['save_status'] ?? $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/employee/batch_update/status_list",
     *     summary="Get List of Statuses",
     *     description="Gives a static list of possible statuses for Employee Batch Update.",
     *     tags={"employee"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             items={"type"="string"}
     *         ),
     *         examples={
     *              "application/json": {
     *                      "validating",
     *                      "validated",
     *                      "saving",
     *                      "save_failed",
     *              }
     *         }
     *     )
     * )
     */
    public function updateUploadStatuses()
    {
        return response()->json(array_merge(
            EmployeeUpdateUploadTask::VALIDATION_STATUSES,
            EmployeeUpdateUploadTask::SAVE_STATUSES
        ));
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/sorted_employees",
     *     summary="Get company sorted employees",
     *     description="Get company sorted employees
     Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         type="string",
     *         in="query",
     *         description="Sort By Field"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         type="string",
     *         in="query",
     *         description="Sort By Field Order (sorts by full name if it is empty)",
     *         enum={ "asc", "desc" }
     *     ),
     *     @SWG\Parameter(
     *         name="offset",
     *         type="string",
     *         in="query",
     *         description="Sort Offset"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         type="string",
     *         in="query",
     *         description="Sort Limit"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function sortCompanyEmployees(
        Request $request,
        $companyId
    ) {
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $user = $request->attributes->get('user');

        if (!$this->authorizationService->authorizeViewCompanyEmployees($companyData, $user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getSortedCompanyEmployees(
            $companyId,
            $request->all()
        );
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employee/events",
     *     summary="Get employee events",
     *     description="Get company employee events
     Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="month",
     *         in="query",
     *         description="Year-month format",
     *         type="string",
     *         required=false,
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Parameter(
     *         name="dates[]",
     *         in="query",
     *         description="Dates with 'Y-m-d' format",
     *         required=false,
     *         type="array",
     *         collectionFormat="multi",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEventsForEmployees($id, Request $request)
    {
        $inputs = $request->all();
        $inputs['company_id'] = $id;
        $isAuthorized = false;
        $authzDataScope = null;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $user = $request->attributes->get('user');

            $companyResponse = $this->companyRequestService->get($id);
            $companyData = json_decode($companyResponse->getData());
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getEventsForEmployees($inputs, $authzDataScope);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/employees/export",
     *     summary="Export employee 201s",
     *     description="Export employee 201s
     Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="data",
     *         in="body",
     *         description="Attributes/Values",
     *         required=true,
     *         schema={
     *             "type"="object",
     *             "required"={
     *                 "employees"
     *             },
     *             "properties"={
     *                 "employees"={
     *                     "type"="array",
     *                     "items"={
     *                         "type"="integer"
     *                     }
     *                 }
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Ok",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function export201s(Request $request, $id)
    {


        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $companyResponse = $this->companyRequestService->get($id);
            $companyData = json_decode($companyResponse->getData());
            $user = $request->attributes->get('user');
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->exportEmployee201s(
            $id,
            $request->only('employees'),
            $this->isAuthzEnabled($request) ? $authzDataScope : null
        );
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employees/generate_masterfile/status",
     *     summary="Generate Employee Master file of a Company",
     *     description="Generate Employee Master file of a Company

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getGenerateEmployeeMasterfileStatus(
        Request $request,
        JobsRequestService $jobsRequestService,
        $companyId
    ) {
        $attributes = $request->all();

        $this->validateArray(array_merge(
            $attributes,
            [
                'company_id' => intval($companyId),
            ]
        ), [
            'company_id' => 'required|integer',
            'job_id' => 'required',
        ]);

        $response = $jobsRequestService->getJobStatus($attributes['job_id']);
        $responseData = json_decode($response->getData(), true);
        $payload = $responseData['data']['attributes']['payload'];
        $responseData['data']['attributes']['payload'] = collect($payload)
            ->only([
                'name',
                'companyId',
                'companyName',
            ])
            ->all();

        return $responseData;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employees/generate_masterfile/result",
     *     summary="Generate Employee Master file of a Company",
     *     description="Generate Employee Master file of a Company

Authorization Scope : **view.employee**",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Payroll or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getGenerateEmployeeMasterfileResult(
        Request $request,
        JobsRequestService $jobsRequestService,
        $companyId
    ) {
        $attributes = $request->all();

        $this->validateArray(array_merge(
            $attributes,
            [
                'company_id' => intval($companyId),
            ]
        ), [
            'company_id' => 'required|integer',
            'job_id' => 'required',
        ]);

        $response = $jobsRequestService->getJobResult($attributes['job_id']);
        $responseData = json_decode($response->getData(), true);
        $payload = $responseData['data'][0]['attributes'];
        $responseData['data']['attributes'] = collect($payload)
            ->only([
                'name',
                'companyId',
                'companyName',
            ])
            ->all();

        return $responseData;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/employees/generate_masterfile",
     *     summary="Generate Employee Master file of a Company",
     *     description="Generate Employee Master file of a Company",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function generateEmployeeMasterfile(Request $request, $companyId)
    {
        $dataScopeFilter = [];
        $company = [];

        $inputs = $request->all();
        $dataScope = $this->getAuthzDataScope($request);
        $dataScopeFilter = $dataScope;

        if ($inputs['companyId']) {
            array_push($company, $inputs['companyId']);
            $dataScopeFilter->dataScopeFilter['COMPANY'] = $company;
        }
        if ($inputs['employeeStatus']) {
            $dataScopeFilter->dataScopeFilter['STATUS'] = $inputs['employeeStatus'];
        }
        if ($inputs['location']) {
            $dataScopeFilter->dataScopeFilter['LOCATION'] = $inputs['location'];
        }
        if ($inputs['department']) {
            $dataScopeFilter->dataScopeFilter['DEPARTMENT'] = $inputs['department'];
        }
        if ($inputs['payrollGroup']) {
            $dataScopeFilter->dataScopeFilter['PAYROLL_GROUP'] = $inputs['payrollGroup'];
        }

        $user = $request->attributes->get('user');

        $response = $this->requestService->generateEmployeeMasterfile(
            $companyId,
            ['user_id' => $user['user_id'], 'search_term' => $inputs['searchTerm']],
            $dataScopeFilter
        );

        try {
            $responseData = json_decode($response->getData(), true);
            $data = array_get($responseData, 'data.attributes.payload', []);
            $this->audit($request, $companyId, ['Generate Employee Master File Trigger' => $data]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    public function getCompanyEmployeesByIds(Request $request, $companyId)
    {
        $inputs = $request->all();
        $isAuthorized = false;
        $authzDataScope = null;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $user = $request->attributes->get('user');

            $companyResponse = $this->companyRequestService->get($companyId);
            $companyData = json_decode($companyResponse->getData());
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyEmployeesByEmployeeIds(
            $companyId,
            $inputs['uids'],
            $inputs['mode'],
            [],
            $authzDataScope
        );

    }


    /**
     * @SWG\Post(
     *     path="/people",
     *     summary="Create new company person",
     *     description="Create new company person",
     *     tags={"people"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="people",
     *         in="body",
     *         description="People data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/createPeopleRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="createPeopleRequestItem",
     *     required={
     *         "account_id", "company_id", "employee_id",
     *         "last_name", "first_name", "middle_name",
     *         "email","primary_location_id", "role_id",
     *         "active"
     *     },
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="employee_id",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="last_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="first_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="middle_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="email",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="primary_location_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="role_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="active",
     *         type="string"
     *     )
     * )
     */
    public function store(Request $request)
    {
        $bearerToken = $request->headers->get('Authorization');

        $inputs = $request->only([
            'company_id',
            'employee_id',
            'last_name',
            'first_name',
            'middle_name',
            'email',
            'primary_location_id',
            'role_id',
            'active'
        ]);

        $inputs['bearer'] = $bearerToken;

        $user = $request->attributes->get('user');
        $inputs['account_id'] = $user['account_id'];
        $isAuthorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        $companyId = $inputs['company_id'];

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $personData = $this->requestService->createPerson(
            $inputs,
            $authzDataScope
        );

        if (!$personData->isSuccessful()) {
            // if there are validation errors in the create request, display these errors
            return $personData;
        }

        $responseData = json_decode($personData->getData(), true);
        $philippineEmployeeData = $responseData['data'];
        $userId = $philippineEmployeeData['user_id'];

        $companyUser = $this->companyUserService->getByUserAndCompanyId($userId, $companyId);
        if (empty($companyUser)) {
            $companyUser = $this->companyUserService->create([
                'user_id'     => $userId,
                'company_id'  => $companyId,
                'employee_id' => $philippineEmployeeData['id']
            ]);
        } else {
            $companyUser = $this->companyUserService->update($userId, $companyId, [
                'employee_id' => $philippineEmployeeData['id']
            ]);
        }

        $authnService = app()->make(AuthnService::class);
        $authnUser = $authnService->getUser($userId);
        if (empty($authnUser)) {
            $authnService->createAuthnUser([
                'user_id' => $userId,
                'account_id' => $inputs['account_id'],
                'company_id' => $companyId,
                'employee_id' => $philippineEmployeeData['employee_id'],
                'authn_user_id' => $philippineEmployeeData['authn_user_id'],
                'status' => AuthnUser::STATUS_UNVERIFIED
            ]);
        } else {
            $authnService->updateAuthnUser($userId, [
                'employee_id' => $philippineEmployeeData['id'],
                'company_id' => $companyId
            ]);
        }

        $this->audit($request, $companyId, $philippineEmployeeData);

        return response()->json([
            'data' => [
                'employee_id' => $philippineEmployeeData['id'],
                'user_id' => !empty($userId) ? $userId : '',
                'job_id' => !empty($jobId) ? $jobId : ''
            ],
        ]);

    }

    public function setUserStatus(
        $id,
        $status,
        $auth0User,
        $employeeId
    ) {
        $inputs = [];

        $this->cacheService->setPrefix(static::CACHE_BUCKET_PREFIX);
        $this->cacheService->setHashFieldPrefix(static::CACHE_FIELD_PREFIX);

        if ($this->cacheService->exists($id)) {
            $this->cacheService->delete($id);
        }
        array_push($inputs, $status);
        // validate permissions related requests
        $user = $this->auth0UserService->getUser($id);

        if (!$user) {
            $this->notFoundError('User not found.');
        }

        // don't allow inactivating of first OWNER
        $userResponse = $this->userRequestService->get($id);
        $userData = json_decode($userResponse->getData(), true);

        $accountId = $this->auth0UserService->getUser($id)->account_id;
        $activeUserIds = $this->auth0UserService->getActiveUserIds($accountId);

        // call microservice to validate new status
        $isValidStatusResponse = $this->userRequestService->validateStatus(
            $id,
            array_merge(
                ['status' => $status],
                ['active_user_ids' => $activeUserIds]
            )
        );

        if (!$isValidStatusResponse->isSuccessful()) {
            return $isValidStatusResponse;
        }

        $userRole = $this->roleRequestService->getRoleByAccount(
            $userData['account_id'],
            $userData['role']['id'],
            true
        );

        $userRoleType = $this->roleRequestService->getRoleType($userRole['data']);

        $isEmployee = !empty(
            array_filter(
                array_column($userData['companies'], 'employee_id')
            )
        );

        // update user status
        $response = $this->userRequestService->setStatus($id, $status);

        if (!$response->isSuccessful()) {
            return $response;
        }

        // If user type is employee, use ProcessesEmployeeActiveStatusUpdatesTrait
        if ($userRoleType === RoleRequestService::ROLE_EMPLOYEE || $isEmployee) {
            $this->processEmployeeActiveStatus(
                [
                    'id'              => $employeeId,
                    'email'           => $userData['email'],
                    'first_name'      => $userData['first_name'],
                    'last_name'       => $userData['last_name'],
                    'middle_name'     => $userData['middle_name'],
                    'account_id'      => $userData['account']['id'],
                    'company_id'      => $userData['companies'][0]['id'],
                    'employee_id'     => $employeeId
                ],
                ($status === Auth0User::STATUS_ACTIVE),
                $user
            );
        }

        $auth0ManagementService = app()->make(Auth0ManagementService::class);
        $userAuth0Profile = $auth0ManagementService->getAuth0UserProfile($user->auth0_user_id);

        if ($userAuth0Profile !== null && $status == 'active') {
            if (!$userAuth0Profile['email_verified']) {
                $auth0ManagementService->verifyEmail($userAuth0Profile['user_id']);
            }
        }

        $userUpdated = $user->fresh()->toArray();

        return $userUpdated;
    }

    public function uploadEmployeePic(
        $id,
        Request $request
    ) {
        $attributes = $request->all();

        $response = $this->requestService->getEmployeeInfo($id);
        $employeeData = json_decode($response->getData(), true);

        $companyId = $employeeData['company_id'];

        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $companyData,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->uploadEmployeePic($attributes, $id);

        try {
            if ($response->getStatusCode() == 200) {
                $responseData = $response->getBody()->getContents();

                $old = ['picture' => $employeeData['picture']];
                $new = json_decode($responseData, true);

                $this->audit($request, $companyId, $new, $old);
            }
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employee/employees_for_payroll_validation",
     *     summary="Validate employee payroll details",
     *     description="Validate employee payroll details",
     *     tags={"employee"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="formData",
     *         description="Start Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="formData",
     *         description="End Date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="ids of employees",
     *         required=false,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="payroll id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="formData",
     *         description="Page of data to request",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="perPage",
     *         in="formData",
     *         description="Number of items per page requested. Default is 10.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeesForPayrollValidation(Request $request)
    {
        $inputs = $request->all();
        $isAuthorized = false;
        $authzDataScope = null;

        $companyId = $request->input('company_id');
        $companyResponse = $this->companyRequestService->get($companyId);
        $companyData = json_decode($companyResponse->getData());
        $createdBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id']
            ]);
        } else {
            // authorize
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $companyData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getEmployeesForPayrollValidation($inputs);
        return $response;
    }

     /**
     * @SWG\Get(
     *     path="/employee/employees_for_payroll_validation/{job_id}",
     *     summary="Gets the status of a employee payroll details validation",
     *     description="This endpoint gets the status of a employee payroll details validation.",
     *     tags={"employee"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
     *         description="Invalid job id",
     *     )
     * )
     */
    public function getEmployeesForPayrollValidationStatus($jobId)
    {
        $response =$this->requestService->getEmployeesForPayrollValidationStatus($jobId);
        return $response;
    }
}
