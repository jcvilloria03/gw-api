<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use App\OtherIncomeType\OtherIncomeTypeAuthorizationServiceFactory;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use App\OtherIncomeType\OtherIncomeTypeService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Swagger\Annotations as SWG;

class OtherIncomeTypeController extends Controller
{
    const OTHER_INCOME_TYPES_NAMESPACES = [
        OtherIncomeTypeService::BONUS_TYPE => 'App\Model\PhilippineBonusType',
        OtherIncomeTypeService::COMMISSION_TYPE => 'App\Model\PhilippineCommissionType',
        OtherIncomeTypeService::ALLOWANCE_TYPE => 'App\Model\PhilippineAllowanceType',
        OtherIncomeTypeService::DEDUCTION_TYPE => 'App\Model\PhilippineDeductionType',
        OtherIncomeTypeService::ADJUSTMENT_TYPE => 'App\Model\PhilippineAdjustmentType',
    ];

    /*
     * App\OtherIncomeType\OtherIncomeTypeRequestService
     */
    protected $requestService;

    /*
     * App\OtherIncomeType\OtherIncomeTypeAuditService
     */
    protected $auditService;

    /**
     * @var OtherIncomeTypeService
     */
    protected $otherIncomeTypeService;

    public function __construct(
        OtherIncomeTypeRequestService $requestService,
        OtherIncomeTypeAuditService $auditService,
        OtherIncomeTypeService $otherIncomeTypeService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
        $this->otherIncomeTypeService = $otherIncomeTypeService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/other_income_types/{type}",
     *     summary="Get Other Income Types",
     *     description="Get Other Income types,
Authorization Scope : **view.bonus_types**, **view.commission_types**, **view.allowance_types**,
**view.deduction_types**, **view.adjustment_types**",
     *     tags={"other_income_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         type="string",
     *         in="path",
     *         description="Other Income Type",
     *         required=true,
     *         enum=\App\OtherIncomeType\OtherIncomeTypeService::OTHER_INCOME_TYPES
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Allowance": {"data": {{
     *                  "id": 1,
     *                  "name": "Allowance Name",
     *                  "company_id": 1,
     *                  "fully_taxable": true,
     *                  "max_non_taxable": 10000,
     *                  "type_name": "App\Model\PhilippineAllowance",
     *                  "deleted_at": null
     *              }}},
     *              "Adjustment": {"data": {{
     *                  "id": 1,
     *                  "name": "Adjustment Name",
     *                  "company_id": 1,
     *                  "type_name": "App\Model\PhilippineAdjustment",
     *                  "deleted_at": 1
     *              }}},
     *              "BONUS": {"data": {{
     *                  "id": 1,
     *                  "name": "Bonus Name",
     *                  "company_id": 1,
     *                  "fully_taxable": false,
     *                  "max_non_taxable": false,
     *                  "basis": "FIXED",
     *                  "frequency": "ONE_TIME",
     *                  "type_name": "App\Model\PhilippineBonus",
     *                  "deleted_at": null
     *              }}},
     *              "COMMISSION": {"data": {{
     *                  "id": 1,
     *                  "name": "Commission Name",
     *                  "company_id": 1,
     *                  "fully_taxable": false,
     *                  "basis": "FIXED",
     *                  "max_non_taxable": false,
     *                  "type_name": "App\Model\PhilippineCommission",
     *                  "deleted_at": null
     *              }}},
     *              "DEDUCTION": {"data": {{
     *                  "id": 1,
     *                  "name": "Commission Name",
     *                  "company_id": 1,
     *                  "type_name": "App\Model\PhilippineCommission",
     *                  "deleted_at": null
     *              }}}
     *         }

     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *              "Not Acceptable": {
     *                  "message": "Provided type not valid.",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function getAllCompanyIncomeTypes(Request $request, $id, $type)
    {
        $authModules = OtherIncomeTypeService::AUTH_MODULES[$type] ?? [];
        if (empty($authModules)) {
            $this->invalidRequestError('Provided type not valid.');
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            foreach ($authModules as $authModule) {
                if ($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id, $authModule)) {
                    $authorized = true;
                    break;
                }
            }
        } else {
            $user = $request->attributes->get('user');
            $authorizationService = OtherIncomeTypeAuthorizationServiceFactory::get(
                $this->getOtherIncomeTypeNamespace($type)
            );
            $authData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $authorized = $authorizationService->authorizeGetAll($authData, $user);
        }

        if (!$authorized) {
            $this->response->errorUnauthorized();
        }

        $response = $this->requestService->getAllCompanyIncomeTypes(
            (int) $id,
            $type,
            $request->only(['group_by', 'exclude_trashed', 'term'])
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/other_income_type/is_delete_available",
     *     summary="Is delete available for Other Income Types",
     *     description="Is delete available for Other Income Types,
Authorization Scope : **view.bonus_types**, **view.commission_types**, **view.allowance_types**,
**delete.deduction_types**",
     *     tags={"other_income_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="formData",
     *         description="Other Income Type Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="available", type="boolean", example=true)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Not found": { "message": "Specified Other Income Types not found. Ids:", "status_code": 404 }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse",
     *         examples={
     *              "Default": {
     *                  "message": "Unauthorized",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED
     *              },
     *              "Unauthorized Ids": {
     *                  "message": "Not authorized. Ids: 1,2,3",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED
     *              }
     *         }
     *     )
     * )
     */
    public function isDeleteAvailable(Request $request, $id)
    {
        $otherIncomeTypeIds = $request->input('ids') ?: [];
        $authzDataScope = $this->getAuthzDataScope($request);
        $result = $this->otherIncomeTypeService
            ->isDeleteAvailable((int) $id, $otherIncomeTypeIds, $authzDataScope->getModules());

        if ($this->isAuthzEnabled($request)) {
            if (!$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
                $this->response->errorUnauthorized();
            }

            if (!$result['available']) {
                $this->response->error($result['message'], $result['status_code']);
            }
        } elseif (!$this->isAuthzEnabled($request)) {
            $user = $request->attributes->get('user');
            $response = $this->requestService->getMultiple(
                (int) $id,
                $otherIncomeTypeIds
            );

            $otherIncomeTypes = array_get(
                json_decode($response->getData(), true),
                'data',
                []
            );

            $this->checkExistenceAndAuthorization(
                $otherIncomeTypeIds,
                $otherIncomeTypes,
                $user,
                'authorizeGetAll'
            );
        }

        return ['available' => $result['available']];
    }

    /**
     * @SWG\Delete(
     *     path="/company/{id}/other_income_type",
     *     summary="Delete Multiple Other Income Types",
     *     description="Delete Multiple Other Income Types,
Authorization Scope : **delete.bonus_types**, **delete.commission_types**, **delete.allowance_types**,
**delete.deduction_types**",
     *     tags={"other_income_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="query",
     *         description="Other Income Type Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Not found": { "message": "Specified Other Income Types not found. Ids:", "status_code": 404 }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse",
     *         examples={
     *              "Default": {
     *                  "message": "Unauthorized",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED
     *              },
     *              "Unauthorized Ids": {
     *                  "message": "Not authorized. Ids: 1,2,3",
     *                  "status_code": Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED
     *              }
     *         }
     *     )
     * )
     */
    public function delete(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $otherIncomeTypesId = $request->input('ids');
        $response = $this->requestService->getMultiple((int) $id, $otherIncomeTypesId);
        $otherIncomeTypes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            if (!$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id)) {
                $this->response->errorUnauthorized();
            }

            $result = $this->otherIncomeTypeService
                ->isDeleteAvailable($id, $otherIncomeTypesId, $authzDataScope->getModules());
            if (!$result['available']) {
                $this->response->error($result['message'], $result['status_code']);
            }
        } else {
            $this->checkExistenceAndAuthorization(
                $request->input('ids'),
                $otherIncomeTypes,
                $user,
                'authorizeDelete'
            );
        }

        $response = $this->requestService->deleteMultiple((int) $id, $otherIncomeTypesId);
        if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
            $this->auditService->mapAndQueueAuditItems(
                $otherIncomeTypes,
                OtherIncomeTypeAuditService::ACTION_DELETE,
                $user
            );
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/other_income_type/{id}",
     *     summary="Get Other Income Type",
     *     description="Get Other Income Type Details
    Authorization Scope : **view.bonus_types**, **view.commission_types**, **view.allowance_types**",
     *     tags={"other_income_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Bonus Type ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              "Allowance": {
     *                  "id": 1,
     *                  "name": "Allowance Name",
     *                  "company_id": 1,
     *                  "fully_taxable": true,
     *                  "max_non_taxable": 10000,
     *                  "type_name": "App\Model\PhilippineAllowance",
     *                  "deleted_at": null
     *              },
     *              "Adjustment": {
     *                  "id": 1,
     *                  "name": "Adjustment Name",
     *                  "company_id": 1,
     *                  "type_name": "App\Model\PhilippineAdjustment",
     *                  "deleted_at": 1
     *              },
     *              "BONUS": {
     *                  "id": 1,
     *                  "name": "Bonus Name",
     *                  "company_id": 1,
     *                  "fully_taxable": false,
     *                  "max_non_taxable": false,
     *                  "basis": "FIXED",
     *                  "frequency": "ONE_TIME",
     *                  "type_name": "App\Model\PhilippineBonus",
     *                  "deleted_at": null
     *              },
     *              "COMMISSION": {
     *                  "id": 1,
     *                  "name": "Commission Name",
     *                  "company_id": 1,
     *                  "fully_taxable": false,
     *                  "basis": "FIXED",
     *                  "max_non_taxable": false,
     *                  "type_name": "App\Model\PhilippineCommission",
     *                  "deleted_at": null
     *              },
     *              "DEDUCTION": {
     *                  "id": 1,
     *                  "name": "Commission Name",
     *                  "company_id": 1,
     *                  "type_name": "App\Model\PhilippineCommission",
     *                  "deleted_at": null
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Not found": { "message": "Other income type not found.", "status_code": 404 }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $otherIncomeType = json_decode($response->getData(), true);

        $authorized = false;
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            $module = OtherIncomeTypeAuthorizationServiceFactory::getModule($otherIncomeType['type_name']);
            $authorized = $authzDataScope
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $otherIncomeType['company_id'], $module);
        } else {
            $authorizationService = OtherIncomeTypeAuthorizationServiceFactory::get(
                array_get($otherIncomeType, 'type_name')
            );

            $user = $request->attributes->get('user');
            $companyId = array_get($otherIncomeType, 'company_id');

            $authorized = $authorizationService->authorizeGet((object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ], $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/other_income_type/is_edit_available",
     *     summary="Check if type is fully editable",
     *     description="Check if type is fully editable
Authorization Scope : **edit.bonus_types**",
     *     tags={"other_income_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         type="integer",
     *         name="other_income_type_id",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="object", @SWG\Property(property="available", type="boolean", example=true))
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         ref="$/responses/NotFoundResponse",
     *         examples={
     *              "Id is required": { "message": "The other income type id field is required.", "status_code": 406 },
     *              "Id is not exists": { "message": "The selected id is invalid.", "status_code": 406 }
     *         }
     *     )
     * )
     */
    public function isEditAvailable(Request $request)
    {
        $response = $this->requestService->isEditAvailable($request->all());

        if (!$response->isSuccessful()) {
            return $response;
        }

        $otherIncomeTypeResponse = $this->requestService->get($request->input('other_income_type_id'));
        $otherIncomeType = json_decode($otherIncomeTypeResponse->getData(), true);

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $module = OtherIncomeTypeAuthorizationServiceFactory::getModule($otherIncomeType['type_name']);
            $authorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $otherIncomeType['company_id'],
                $module
            );
        } else {
            $authorizationService = OtherIncomeTypeAuthorizationServiceFactory::get(
                array_get($otherIncomeType, 'type_name')
            );

            $user = $request->attributes->get('user');
            $companyId = array_get($otherIncomeType, 'company_id');

            $authorized = $authorizationService->authorizeUpdate((object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ], $user);
        }

        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * Authorize task
     *
     * @param $providedIds
     * @param $otherIncomeTypes
     * @param $user
     * @param $authorizeMethod
     * @throws \Exception
     */
    private function checkExistenceAndAuthorization(
        $providedIds,
        $otherIncomeTypes,
        $user,
        $authorizeMethod
    ) {
        $specifiedAndFoundDiff = collect($providedIds)
            ->diff(
                collect($otherIncomeTypes)->map(function ($otherIncomeType) {
                    return array_get($otherIncomeType, 'id');
                })
            );

        if ($specifiedAndFoundDiff->count() > 0) {
            $this->notFoundError(
                'Specified Other Income Types not found. Ids: ' .
                $specifiedAndFoundDiff->implode(',')
            );
        }

        $authorizedIds = collect([]);
        foreach ($otherIncomeTypes as $otherIncomeType) {
            $authorizationService = OtherIncomeTypeAuthorizationServiceFactory::get(
                array_get($otherIncomeType, 'type_name')
            );
            $authData = (object) [
                'other_income_type_id' => array_get($otherIncomeType, 'id'),
                'account_id' => Company::getAccountId(
                    array_get($otherIncomeType, 'company_id')
                ),
                'company_id' => array_get($otherIncomeType, 'company_id')
            ];
            if ($authorizationService->{$authorizeMethod}($authData, $user)) {
                $authorizedIds->push(
                    array_get($otherIncomeType, 'id')
                );
            }
        }

        $unauthorizedIds = collect($providedIds)
            ->diff(
                $authorizedIds
            );
        if ($unauthorizedIds->count() > 0) {
            $this->response()->errorUnauthorized(
                'Not authorized. Ids: ' .
                $unauthorizedIds->implode(',')
            );
        }
    }

    /**
     * Get Other Income Type namespace based on type
     *
     * @param string $type
     * @return string
     */
    private function getOtherIncomeTypeNamespace($type)
    {
        if (empty(self::OTHER_INCOME_TYPES_NAMESPACES[$type])) {
            $this->response()->errorNotFound(
                "Type {$type} not found."
            );
        }
        return self::OTHER_INCOME_TYPES_NAMESPACES[$type];
    }

    /**
     * @SWG\Get(
     *     path="/other_income_type/tax_option_choices/{otherIncomeType}",
     *     summary="Get Other Income Type tax options based on type",
     *     description="Get Other Income Type tax options based on type",
     *     tags={"other_income_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Other income type",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *              {
     *                  "text": "Fully Taxable",
     *                  "value": "fully_taxable"
     *              },
     *              {
     *                  "text": "Partially Non Taxable",
     *                  "value": "partially_non_taxable"
     *              },
     *              {
     *                  "text": "Fully Non Taxable",
     *                  "value": "fully_non_taxable"
     *              },
     *              {
     *                  "text": "Reduce to Taxable Income",
     *                  "value": "reduce_to_taxable_income"
     *              }
     *         }
     *     )
     * )
     */
    public function getTaxOptionsChoices($type)
    {
        return $this->requestService->getTaxOptionsChoices($type);
    }
}
