<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EssBaseController;
use App\DayHourRate\DayHourRateRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;

class EssDayHourRateController extends EssBaseController
{
    /**
     * @var \App\DayHourRate\DayHourRateRequestService
     */
    private $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    private $authorizationService;

    /**
     * EssDayHourRateController constructor
     * @param \App\DayHourRate\DayHourRateRequestService
     * @param \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    public function __construct(
        DayHourRateRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/time_types",
     *     summary="Get time types for employee company",
     *     description="Get all time types within employee company
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getTimeTypes(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $companyId = $user['employee_company_id'];

        return $this->requestService->getCompanyTimeTypes($companyId);
    }
}
