<?php

namespace App\Http\Controllers;

use App\Account\AccountRequestService;
use App\Company\PhilippineCompanyRequestService;
use App\Role\RoleAuthorizationService;
use App\Role\RoleService;
use App\Transformer\RoleTemplateTransformer;
use League\Fractal\Manager as FractalManager;
use Illuminate\Http\Request;
use League\Fractal\Resource\Collection;

class RoleTemplateController extends Controller
{
    /**
     * @var \App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * @var \App\Role\RoleService
     */
    private $roleService;

    /**
     * @var \App\Transformer\RoleTemplateTransformer
     */
    private $transformer;

    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    /**
     * App\Company\PhilippineCompanyRequestService
     */
    protected $companyRequestService;

    /**
     * @var \App\Role\RoleAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        AccountRequestService $accountRequestService,
        PhilippineCompanyRequestService $companyRequestService,
        RoleService $roleService,
        RoleTemplateTransformer $transformer,
        FractalManager $fractal,
        RoleAuthorizationService $authorizationService
    ) {
        $this->accountRequestService = $accountRequestService;
        $this->companyRequestService = $companyRequestService;
        $this->roleService = $roleService;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/role_templates",
     *     summary="Get List of Role templates",
     *     description="Get List of Role templatess for given account id or company id
    Authorization Scope : **view.role**",
     *     tags={"role"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="exclude_system_defined",
     *         in="query",
     *         description="Exclude system defined",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company id",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getRoleTemplates(Request $request)
    {
        // check if account exists
        // this will throw any errors related to missing account, etc.
        $accountId = $request->attributes->get('user')['account_id'];
        $this->accountRequestService->get($accountId);

        $this->validate($request, [
            'exclude_system_defined' => 'in:' . implode(',', [true, false, 1, 0, '1', '0', 'true', 'false']),
            'company_id' => 'nullable|integer',
        ]);

        // check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        if ($companyId) {
            $this->companyRequestService->get($companyId);
        }

        $excludeSystemDefined = array_get($request->all(), 'exclude_system_defined', false);

        // fetch account roles
        $roles = $this->roleService->getRoles(
            $accountId,
            filter_var($excludeSystemDefined, FILTER_VALIDATE_BOOLEAN),
            false,
            $companyId
        );
        $collection = new Collection($roles, $this->transformer);
        $result = $this->fractal->createData($collection)->toArray();

        return $result;
    }
}
