<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\Earning\EarningAuditService;
use App\Earning\EarningAuthorizationService;
use App\Earning\EarningRequestService;
use App\Earning\EarningUploadTask;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EarningController extends Controller
{
    /**
     * @var \App\Earning\EarningAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Earning\EarningRequestService
     */
    protected $requestService;

    /**
     * @var CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\Audit\AuditServiceAuditService
     */
    private $auditService;

    public function __construct(
        EarningAuthorizationService $earningsAuthorizationService,
        EarningRequestService $earningRequestService,
        CsvValidator $csvValidator,
        AuditService $auditService
    ) {
        $this->authorizationService = $earningsAuthorizationService;
        $this->requestService = $earningRequestService;
        $this->csvValidator = $csvValidator;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/company/{id}/earnings",
     *     summary="Get Earnings",
     *     description="Get list of Earnings within the given company Id
    Authorization Scope : **view.earnings**",
     *     tags={"earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getAll(Request $request, $id)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $accountId = Company::getAccountId($id);

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $id
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet($data, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAll($id);
    }

    /**
     * @SWG\Post(
     *     path="/earning/upload",
     *     summary="Upload Earning",
     *     description="Upload earning
    Authorization Scope : **create.earnings**",
     *     tags={"earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "earnings_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = \App::make(EarningUploadTask::class);

            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => EarningUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            \Amqp::publish(config('queues.earning_validation_queue'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/earning/upload/status",
     *     summary="Get Job Status",
     *     description="Get earning Upload Status
    Authorization Scope : **create.earnings**",
     *     tags={"earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\Earning\EarningUploadTask::PROCESS_VALIDATION,
     *              App\Earning\EarningUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Earning is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(EarningUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (EarningUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, EarningUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(",", EarningUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === EarningUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === EarningUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }
        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/earning/upload/save",
     *     summary="Save Earnings",
     *     description="Saves Earnings from Previously Uploaded CSV
    Authorization Scope : **create.earnings**",
     *     tags={"earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "earnings_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Earnings needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(EarningUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(EarningUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (EarningUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        \Amqp::publish(config('queues.earning_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/earning/upload/preview",
     *     summary="Get Earnings Preview",
     *     description="Get Earnings Upload Preview
    Authorization Scope : **create.earnings**",
     *     tags={"earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(EarningUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (EarningUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);

        return $response;
    }

    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/earning/{id}",
     *     summary="Get Earning",
     *     description="Get single earning by given ID
    Authorization Scope : **view.earnings**",
     *     tags={"earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Earning ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $earningData = $this->requestService->get((int) $id);
        $earning = json_decode($earningData->getData(), true);

        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = array_get($earning, 'company_id');
        $accountId = Company::getAccountId($companyId);

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet($data, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        return $earningData;
    }

    /**
     * @SWG\Get(
     *     deprecated=true,
     *     path="/employee/{id}/earning",
     *     summary="Get Employee Earning",
     *     description="Get single earning by given employee ID
    Authorization Scope : **view.earnings**",
     *     tags={"earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found request",
     *     )
     * )
     */
    public function getByEmployeeId(Request $request, $id)
    {
        $earningData = $this->requestService->getByEmployeeId((int) $id);
        $earning = json_decode($earningData->getData(), true);

        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = array_get($earning, 'company_id');
        $accountId = Company::getAccountId($companyId);

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet($data, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        return $earningData;
    }

    /**
     * @SWG\Patch(
     *     deprecated=true,
     *     path="/earning/{id}/update",
     *     summary="Update earning",
     *     description="Update earning for an employee, and create new basic pay adjusmtent
Authorization Scope : **edit.earnings**",
     *     tags={"earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Earning ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="earning",
     *         in="body",
     *         description="Update earning",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/earning")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="earning",
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="unit",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="with_holding_tax_type",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="expanded_with_holding_tax_rate",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="attendance_data_required",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_unworked_regular_holiday_pay",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_unworked_special_holiday_pay",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_holiday_premium_pay",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_rest_day_pay",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_overtime",
     *         type="integer",
     *         enum={0,1}
     *     ),
     *     @SWG\Property(
     *         property="entitled_night_differential",
     *         type="integer",
     *         enum={0,1}
     *     ),
     * )
     */
    public function update(Request $request, $id)
    {
        $earningData = $this->requestService->get((int) $id);
        $earning = json_decode($earningData->getData(), true);

        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = array_get($earning, 'company_id');
        $accountId = Company::getAccountId($companyId);

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId
        ];

        // authorize
        if (!$this->authorizationService->authorizeUpdate($data, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->update($id, $request->all());

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $updatedEarning = json_decode($response->getData(), true);

        $details = [
            'new' => $updatedEarning,
            'old' => $earning
        ];

        $item = new AuditCacheItem(
            EarningAuditService::class,
            EarningAuditService::ACTION_UPDATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updatedEarning;
    }
}
