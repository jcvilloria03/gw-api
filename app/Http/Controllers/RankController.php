<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\Rank\RankAuditService;
use App\Rank\RankAuthorizationService;
use App\Rank\RankEsIndexQueueService;
use Illuminate\Http\Request;
use App\Rank\RankRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Authz\AuthzDataScope;
/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RankController extends Controller
{
    /**
     * @var \App\Rank\RankRequestService
     */
    private $requestService;

    /**
     * @var \App\Rank\RankAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\Rank\RankEsIndexQueueService
     */
    private $indexQueueService;

    public function __construct(
        RankRequestService $requestService,
        RankAuthorizationService $authorizationService,
        AuditService $auditService,
        RankEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * @SWG\Get(
     *     path="/rank/{id}",
     *     summary="Get Rank",
     *     description="Get Rank Details

Authorization Scope : **view.rank**",
     *     tags={"rank"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Rank ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $rankData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($rankData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/rank/",
     *     summary="Create company rank",
     *     description="Create company rank

Authorization Scope : **create.rank**",
     *     tags={"rank"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Rank Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="Rank Description",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $rankData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($rankData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        $this->indexQueueService->queue([$responseData['id']]);

        // audit log
        $rankGetResponse = $this->requestService->get($responseData['id']);
        $rankData = json_decode($rankGetResponse->getData(), true);
        $details = [
            'new' => $rankData,
        ];
        $item = new AuditCacheItem(
            RankAuditService::class,
            RankAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;

    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/rank/is_name_available",
     *     summary="Is rank name available",
     *     description="Check availability of rank name with the given company",
     *     tags={"rank"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Rank Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="rank_id",
     *         in="formData",
     *         description="Rank Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $rankData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($rankData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/ranks",
     *     summary="Get all Ranks",
     *     description="Get all Rank within company.

Authorization Scope : **view.rank**",
     *     tags={"rank"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyRanks($id, Request $request)
    {
        $response = $this->requestService->getCompanyRanks($id);
        $rankData = json_decode($response->getData())->data;

        if (empty($rankData)) {
            return $response;
        }
        $rank = current($rankData);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $rank->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGetCompanyRanks(
                $rank,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }


        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/rank/bulk_create",
     *     summary="Create multiple company ranks",
     *     description="Create multiple company ranks

Authorization Scope : **create.rank**",
     *     tags={"rank"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newRanks"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newRanks",
     *     required={"company_id", "names"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="names",
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *             description="Array of rank names"
     *         )
     *     )
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $rankData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($rankData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);

        $rankIds = [];

        // audit log
        foreach ($responseData['ids'] as $id) {
            $rankGetResponse = $this->requestService->get($id);
            $rankData = json_decode($rankGetResponse->getData(), true);
            $details = [
                'new' => $rankData,
            ];
            $item = new AuditCacheItem(
                RankAuditService::class,
                RankAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);

            $rankIds[] = $id;
        }

        $this->indexQueueService->queue($rankIds);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/rank/{id}",
     *     summary="Update Rank",
     *     description="Update Rank
     Authorization Scope : **edit.rank**",
     *     tags={"rank"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Rank Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Rank Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldRankData = json_decode($response->getData());
        $oldRankData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldRankData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldRankData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $this->indexQueueService->queue([$id]);

        $getResponse = $this->requestService->get($id);
        $oldRankData = json_decode($response->getData(), true);
        $newRankData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldRankData,
            'new' => $newRankData,
        ];
        $item = new AuditCacheItem(
            RankAuditService::class,
            RankAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

      /**
      * @SWG\Delete(
      *     path="/rank/bulk_delete",
      *     summary="Delete multiple Ranks",
      *     description="Delete multiple Ranks
      Authorization Scope : **delete.rank**",
      *     tags={"rank"},
      *     consumes={"application/x-www-form-urlencoded"},
      *     produces={"application/json"},
      *     @SWG\Parameter(
      *         type="string",
      *         name="Authorization",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         type="string",
      *         name="X-Authz-Entities",
      *         in="header",
      *         required=true
      *     ),
      *     @SWG\Parameter(
      *         name="company_id",
      *         in="formData",
      *         description="Company ID",
      *         required=true,
      *         type="integer"
      *     ),
      *     @SWG\Parameter(
      *         name="rank_ids[]",
      *         type="array",
      *         in="formData",
      *         description="Rank Ids",
      *         required=true,
      *         @SWG\Items(type="integer"),
      *         collectionFormat="multi"
      *     ),
      *     @SWG\Response(
      *         response=204,
      *         description="Successful operation",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
      *         description="Company not found.",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
      *         description="Invalid request",
      *     )
      * )
      */
    public function bulkDelete(Request $request, PhilippineCompanyRequestService $companyRequestService)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $rankData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($rankData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inUseResponse = $companyRequestService->isInUse('rank', ['ids' => $inputs['rank_ids']]);
        $inUseData = json_decode($inUseResponse->getData());
        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        foreach ($request->input('rank_ids') as $id) {
            $item = new AuditCacheItem(
                RankAuditService::class,
                RankAuditService::ACTION_DELETE,
                new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                [
                    'old' => [
                        'id' => $id,
                        'company_id' => $request->get('company_id')
                    ]
                ]
            );
            $this->auditService->queue($item);
        }

        return $response;
    }
}
