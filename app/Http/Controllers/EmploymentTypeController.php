<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Facades\Company;
use App\EmploymentType\EmploymentTypeAuditService;
use App\EmploymentType\EmploymentTypeAuthorizationService;
use Illuminate\Http\Request;
use App\EmploymentType\EmploymentTypeRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Authz\AuthzDataScope;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EmploymentTypeController extends Controller
{
    /**
     * @var \App\EmploymentType\EmploymentTypeRequestService
     */
    private $requestService;

    /**
     * @var \App\EmploymentType\EmploymentTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditServiceAuditService
     */
    private $auditService;

    public function __construct(
        EmploymentTypeRequestService $requestService,
        EmploymentTypeAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/employment_type/{id}",
     *     summary="Get Employment Type",
     *     description="Get Employment Type Details

Authorization Scope : **view.employment_type**",
     *     tags={"employment_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="EmploymentType ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $employmentTypeData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($employmentTypeData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employment_type/",
     *     summary="Create company employment type",
     *     description="Create company employment type

Authorization Scope : **create.employment_type**",
     *     tags={"employment_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Employment Type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $employmentTypeData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];
        if (!$this->authorizationService->authorizeCreate($employmentTypeData, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $employmentTypeGetResponse = $this->requestService->get($responseData['id']);
        $employmentTypeData = json_decode($employmentTypeGetResponse->getData(), true);
        $details = [
            'new' => $employmentTypeData,
        ];
        $item = new AuditCacheItem(
            EmploymentTypeAuditService::class,
            EmploymentTypeAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/employment_type/bulk_create",
     *     summary="Create multiple company employment types",
     *     description="Create multiple company employment types

Authorization Scope : **create.employment_type**",
     *     tags={"employment_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newEmploymentTypes"),
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newEmploymentTypes",
     *     required={"company_id", "names"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="names",
     *         type="array",
     *         @SWG\Items(
     *             type="string"
     *         )
     *     )
     * )
     */
    public function bulkCreate(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $employmentTypeData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($employmentTypeData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreate($request->all());
        $responseData = json_decode($response->getData(), true);
        // audit log
        foreach ($responseData['ids'] as $id) {
            $employmentTypeGetResponse = $this->requestService->get($id);
            $employmentTypeData = json_decode($employmentTypeGetResponse->getData(), true);
            $details = [
                'new' => $employmentTypeData,
            ];
            $item = new AuditCacheItem(
                EmploymentTypeAuditService::class,
                EmploymentTypeAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/employment_type/is_name_available",
     *     summary="Is employment type name available",
     *     description="Check availability of employment type name with the given company",
     *     tags={"employment_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Employment Type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="employment_type_id",
     *         in="formData",
     *         description="Employment Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $response = $this->requestService->isNameAvailable($id, $request->all());
        $user = $request->attributes->get('user');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $employmentTypeData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($employmentTypeData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employment_types",
     *     summary="Get all Employment Types",
     *     description="Get all Employment Types within company.

Authorization Scope : **view.employment_type**",
     *     tags={"employment_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyEmploymentTypes($id, Request $request)
    {
        $response = $this->requestService->getCompanyEmploymentTypes($id);
        $employmentTypeData = json_decode($response->getData())->data;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $employmentType = current($employmentTypeData);
            $isAuthorized = $this->authorizationService->authorizeGetCompanyEmploymentTypes(
                $employmentType,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        if (empty($employmentTypeData)) {
            return $response;
        }

        return $response;
    }

     /**
     * @SWG\Put(
     *     path="/employment_type/{id}",
     *     summary="Update Employment Type",
     *     description="Update Employment Type
     Authorization Scope : **edit.employment_type**",
     *     tags={"employment_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employment Type Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Employment Type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldEmploymentTypeData = json_decode($response->getData());
        $oldEmploymentTypeData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldEmploymentTypeData->company_id
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldEmploymentTypeData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldEmploymentTypeData = json_decode($response->getData(), true);
        $newEmploymentTypeData = json_decode($getResponse->getData(), true);

        $details = [
            'old' => $oldEmploymentTypeData,
            'new' => $newEmploymentTypeData,
        ];
        $item = new AuditCacheItem(
            EmploymentTypeAuditService::class,
            EmploymentTypeAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

       /**
       * @SWG\Delete(
       *     path="/employment_type/bulk_delete",
       *     summary="Delete multiple Employment Types",
       *     description="Delete multiple Employment Types
       Authorization Scope : **delete.employment_type**",
       *     tags={"employment_type"},
       *     consumes={"application/x-www-form-urlencoded"},
       *     produces={"application/json"},
       *     @SWG\Parameter(
       *         type="string",
       *         name="Authorization",
       *         in="header",
       *         required=true
       *     ),
       *     @SWG\Parameter(
       *         type="string",
       *         name="X-Authz-Entities",
       *         in="header",
       *         required=true
       *     ),
       *     @SWG\Parameter(
       *         name="company_id",
       *         in="formData",
       *         description="Company ID",
       *         required=true,
       *         type="integer"
       *     ),
       *     @SWG\Parameter(
       *         name="employment_type_ids[]",
       *         type="array",
       *         in="formData",
       *         description="Employment Type Ids",
       *         required=true,
       *         @SWG\Items(type="integer"),
       *         collectionFormat="multi"
       *     ),
       *     @SWG\Response(
       *         response=204,
       *         description="Successful operation",
       *     ),
       *     @SWG\Response(
       *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
       *         description="Company not found.",
       *     ),
       *     @SWG\Response(
       *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
       *         description="Invalid request",
       *     )
       * )
       */
    public function bulkDelete(Request $request, PhilippineCompanyRequestService $companyRequestService)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $employmentTypeData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete($employmentTypeData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inUseResponse = $companyRequestService->isInUse('employment_type', ['ids' => $inputs['employment_type_ids']]);
        $inUseData = json_decode($inUseResponse->getData());

        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        foreach ($request->input('employment_type_ids') as $id) {
            $item = new AuditCacheItem(
                EmploymentTypeAuditService::class,
                EmploymentTypeAuditService::ACTION_DELETE,
                new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                [
                    'old' => [
                        'id' => $id,
                        'company_id' => $request->get('company_id')
                    ]
                ]
            );
            $this->auditService->queue($item);
        }

        return $response;
    }
}
