<?php

namespace App\Http\Controllers;

use App\Http\Controllers\EssBaseController;
use App\ESS\EssTeamCalendarService;
use App\Team\TeamRequestService;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class EssTeamController extends EssBaseController
{
    /** @var \App\Team\TeamRequestService */
    protected $teamService;

    /** @var \App\ESS\EssTeamCalendarService */
    protected $calendarService;

    public function __construct(
        TeamRequestService $teamService,
        EssTeamCalendarService $calendarService
    ) {
        $this->teamService = $teamService;

        $this->calendarService = $calendarService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/teams",
     *     summary="Get teams",
     *     description="Get teams",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         examples={
     *            "application/json": {
     *                "data": {
     *                    {
     *                        "id"=1,
     *                        "company_id"=2,
     *                        "name"="Team 1",
     *                        "avatar"=null,
     *                        "edit_team_schedule"=true,
     *                        "view_team_attendance"=true,
     *                        "regenerate_team_attendance"=true,
     *                        "members_count"=99,
     *                    },
     *                    {
     *                        "id"=2,
     *                        "company_id"=2,
     *                        "name"="Team 2",
     *                        "avatar"=null,
     *                        "edit_team_schedule"=true,
     *                        "view_team_attendance"=true,
     *                        "regenerate_team_attendance"=true,
     *                        "members_count"=1234,
     *                    }
     *                }
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function index(Request $request)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        return $this->teamService->getTeamsByLeaderId($requestor['employee_company_id'], $requestor['employee_id']);
    }

    /**
     * @SWG\Get(
     *     path="/ess/teams/{id}",
     *     summary="Get single team",
     *     description="Get single team by ID",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Team ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         examples={
     *            "application/json": {
     *                "data": {
     *                    "id"=1,
     *                    "company_id"=1,
     *                    "name"="Lorem Ipsum",
     *                    "avatar"=null,
     *                    "edit_team_schedule"=true,
     *                    "view_team_attendance"=true,
     *                    "regenerate_team_attendance"=true,
     *                    "members_count"=99,
     *                }
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function view(Request $request, int $id)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($id, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        return $team;
    }

    /**
     * @SWG\Get(
     *     path="/ess/teams/{id}/members",
     *     summary="Get team members",
     *     description="Get team members by ID",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Team ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *         examples={
     *            "application/json": {
     *                "total"=2,
     *                "per_page"=20,
     *                "current_page"=1,
     *                "last_page"=1,
     *                "next_page_url"=null,
     *                "prev_page_url"=null,
     *                "from"=1,
     *                "from"=2,
     *                "data": {
     *                    {
     *                        "id"=1,
     *                        "employee_id"="EMP-001",
     *                        "first_name"="Lorem",
     *                        "middle_name"="",
     *                        "last_name"="Ipsum",
     *                        "full_name"="Lorem Ipsum",
     *                        "avatar"=null,
     *                        "is_leader"=true,
     *                    },
     *                    {
     *                        "id"=2,
     *                        "employee_id"="EMP-002",
     *                        "first_name"="Dolor",
     *                        "middle_name"="Sit",
     *                        "last_name"="Amet",
     *                        "full_name"="Dolor Sit Amet",
     *                        "avatar"=null,
     *                        "is_leader"=false,
     *                    }
     *                }
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getMembers(Request $request, int $id)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($id, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $page = $request->input('page');
        $perPage = $request->input('per_page');

        return $this->teamService->getMembers($id, $page, $perPage);
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/calendar",
    *     summary="Get Team Calendar Data",
    *     description="Get Team Calendar Data",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="start_date",
    *         in="query",
    *         description="Start date in YYYY-MM-DD format",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Parameter(
    *         name="end_date",
    *         in="query",
    *         description="End date in YYYY-MM-DD format",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "2020-01-01": {
    *                        "summary": {
    *                            "no_infractions": 0,
    *                            "infractions": 1,
    *                            "pending": 1
    *                        },
    *                        "members": {
    *                            "1": {
    *                                "shifts": {
    *                                    {
    *                                        "id": 1,
    *                                        "schedule_id": 1,
    *                                        "schedule": {
    *                                            "name": "My Schedule",
    *                                            "type": "fixed",
    *                                            "default": false,
    *                                            "day_type": "regular",
    *                                            "start_time": "08:00",
    *                                            "end_time": "17:00",
    *                                            "total_hours": "08:00"
    *                                        }
    *                                    }
    *                                },
    *                                "badges": {
    *                                    {
    *                                        "label": "On Leave",
    *                                        "color": "primary"
    *                                    },
    *                                    {
    *                                        "label": "Tardy",
    *                                        "color": "danger"
    *                                    },
    *                                }
    *                            }
    *                        }
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getCalendar(Request $request, int $id)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($id, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->validate($request, [
            'start_date' => 'required|date:Y-m-d|before_or_equal:end_date',
            'end_date' => 'required|date:Y-m-d|after_or_equal:start_date',
        ]);

        $startDate = Carbon::parse($request->input('start_date'))->startOfDay();
        $endDate = Carbon::parse($request->input('end_date'))->endOfDay();

        $companyId = $team['data']['company_id'];

        $employeeIds = $this->teamService->getMemberIds($id);
        $defaultSchedules = $this->calendarService->getDefaultSchedules($team['data']['company_id']);

        $allShifts = $this->calendarService->getMultipleShifts($companyId, $employeeIds, $startDate, $endDate);
        $allRestDays = $this->calendarService->getMultipleRestDays($companyId, $employeeIds, $startDate, $endDate);
        $allBadges = $this->calendarService->getMultipleBadges($employeeIds, $startDate, $endDate);

        $result = [];

        foreach ($this->calendarService->getDates($startDate, $endDate) as $date) {
            $summary = [
                'no_infractions' => 0,
                'infractions' => 0,
                'pending' => 0,
            ];

            $members = [];

            foreach ($employeeIds as $employeeId) {
                $shifts = $allShifts[$date][$employeeId] ?? collect();
                $restDays = $allRestDays[$date][$employeeId] ?? collect();

                $badges = [];

                $badgesData = $allBadges[$date][$employeeId] ?? null;

                if ($badgesData) {
                    $badges = $badgesData['badges'];

                    $summary['no_infractions'] += $badgesData['summary']['no_infractions'];
                    $summary['infractions'] += $badgesData['summary']['infractions'];
                    $summary['pending'] += $badgesData['summary']['pending'];
                }

                if ($shifts->isEmpty() && $restDays->isNotEmpty()) {
                    $shifts->push(...$restDays);
                }

                if ($shifts->isEmpty()) {
                    $dayOfWeek = Carbon::parse($date)->dayOfWeek;

                    if ($dayOfWeek === Carbon::SUNDAY) {
                        $dayOfWeek = 7; // Use week day number 7 for Sunday
                    }

                    $shifts->push($defaultSchedules->get($dayOfWeek));
                }

                $members[$employeeId] = [
                    'shifts' => $shifts,
                    'badges' => $badges
                ];
            }

            $result[$date] = [
                'summary' => $summary,
                'members' => $members
            ];
        }

        return [
            'data' => $result
        ];
    }
}
