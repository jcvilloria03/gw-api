<?php

namespace App\Http\Controllers;

use App\Allowance\AllowanceAuthorizationService;
use App\Allowance\PhilippineAllowanceRequestService;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class PhilippineAllowanceController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Allowance\PhilippineAllowanceRequestService
     */
    protected $requestService;

    /**
     * @var \App\Allowance\AllowanceAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        PhilippineAllowanceRequestService $requestService,
        AllowanceAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/allowance/bulk_create",
     *     summary="Assign Multiple Allowances",
     *     description="Assign multiple allowances,
Authorization Scope : **create.allowances**",
     *     tags={"allowance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *
     *     @SWG\Parameter(
     *         name="allowances",
     *         in="body",
     *         description="Create multiple allowances",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/allowancesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="allowancesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/allowancesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     * @SWG\Definition(
     *     definition="allowancesRequestItem",
     *     @SWG\Property(
     *         property="type_id",
     *         type="integer",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="recipients",
     *         type="object",
     *             @SWG\Property(
     *              property="employees",
     *              type="array",
     *              items={"type"="integer", "example"="1"}
     *             )
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number",
     *         example="200"
     *     ),
     *     @SWG\Property(
     *         property="recurring",
     *         type="integer",
     *         enum={"0,1"},
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="credit_frequency",
     *         type="string",
     *         example="DAILY"
     *     ),
     *     @SWG\Property(
     *         property="prorated",
     *         type="integer",
     *         enum={"0,1"},
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="prorated_by",
     *         type="string",
     *         example="PRORATED_BY_HOUR"
     *     ),
     *     @SWG\Property(
     *         property="entitled_when",
     *         type="string",
     *         example="ON_PAID_LEAVE"
     *     ),
     *     @SWG\Property(
     *         property="valid_from",
     *         type="string",
     *         example="valid_from"
     *     ),
     *     @SWG\Property(
     *         property="valid_to",
     *         type="string",
     *         example="valid_to"
     *     ),
     *     @SWG\Property(
     *         property="release_details",
     *         type="array",
     *         items={"type"="object",
     *                  "properties"={
     *                      "id"={"type"="integer", "example"=1},
     *                      "date":{"type"="string", "format"="date", "pattern"="YYYY-MM-DD", "example"="2019-11-01"},
     *                      "disburse_through_special_pay_run"={"type"="boolean", "example"=true},
     *                  }
     *              }
     *     ),
     * )
     */
    public function bulkCreate(
        Request $request,
        EmployeeRequestService $employeeService,
        $companyId
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;
        $errorMesssage = 'Unauthorized';

        if ($this->isAuthzEnabled($request)) {
            $authorizedCompany = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            $employeeIds = array_flatten($request->json('*.recipients.employees'), 1);

            $employeeResponse = $employeeService->getCompanyEmployeesById($companyId, $employeeIds);
            if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
                return $employeeResponse;
            }

            $unAuthorizedEmployees = collect(json_decode($employeeResponse->getData(), true))->reject(
                function ($employee) use ($request) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized([
                        AuthzDataScope::SCOPE_COMPANY => data_get($employee, 'company_id'),
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employee, 'department_id'),
                        AuthzDataScope::SCOPE_POSITION => data_get($employee, 'position_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employee, 'location_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employee, 'team_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group_id'),
                    ]);
                }
            );

            if (!$unAuthorizedEmployees->isEmpty()) {
                $errorMesssage = 'Unauthorized: Unable to process employees:[' .
                    implode(',', $unAuthorizedEmployees->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = $authorizedCompany && $unAuthorizedEmployees->isEmpty();
            }
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMesssage);
        }

        // call microservice
        $response = $this->requestService->bulkCreate($companyId, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $allowanceIds = json_decode($response->getData(), true);
        $allowances = collect($inputs)->map(function ($allowance, $index) use ($allowanceIds) {
            array_set($allowance, 'id', array_get($allowanceIds, $index));
            array_set($allowance, 'type', 'App\Model\PhilippineAllowance');
            return $allowance;
        })->toArray();

        if ($response->isSuccessful()) {
            foreach ($allowances as $allowance) {
                // trigger audit trail
                $this->audit($request, $companyId, $allowance);
            }
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/allowance/{allowanceId}",
     *     summary="Update the allowance",
     *     description="Update the allowance
Authorization Scope : **edit.allowances**",
     *     tags={"allowance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="allowanceId",
     *         in="path",
     *         description="Allowance ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="allowance",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/allowancesRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeRequestService $otherIncomeRequestService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $allowanceResponse = $otherIncomeRequestService->get($id);
        if (!$allowanceResponse->isSuccessful()) {
            return $allowanceResponse;
        }
        $allowanceData = json_decode($allowanceResponse->getData(), true);
        $companyId = array_get($allowanceData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $authData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->update($id, $request->all(), $this->getAuthzDataScope($request));
        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData, $allowanceData, true);
        }
        return $response;
    }
}
