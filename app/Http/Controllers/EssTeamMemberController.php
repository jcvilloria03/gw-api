<?php

namespace App\Http\Controllers;

use App\Http\Controllers\EssBaseController;
use App\ESS\EssEmployeeCalendarService;
use App\RestDay\RestDayRequestService;
use App\Schedule\ScheduleRequestService;
use App\Shift\ShiftRequestService;
use App\Team\TeamRequestService;
use App\Transformer\EssEmployeeScheduleTransformer;
use App\Transformer\EssEmployeeShiftTransformer;
use Illuminate\Http\Request;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager as FractalManager;
use Carbon\Carbon;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class EssTeamMemberController extends EssBaseController
{
    /** @var \League\Fractal\Manager */
    protected $fractal;

    /** @var \App\Team\TeamRequestService */
    protected $teamService;

    /** @var \App\Shift\ShiftService */
    protected $shiftService;

    /** @var \App\Schedule\ScheduleService */
    protected $scheduleService;

    /** @var \App\RestDay\RestDayRequestService */
    protected $restDayService;

    /** @var \App\ESS\EssEmployeeCalendarService */
    protected $calendarService;

    public function __construct(
        FractalManager $fractal,
        TeamRequestService $teamService,
        ShiftRequestService $shiftService,
        ScheduleRequestService $scheduleService,
        RestDayRequestService $restDayService,
        EssEmployeeCalendarService $calendarService
    ) {
        $this->fractal = $fractal;

        $this->teamService = $teamService;

        $this->shiftService = $shiftService;

        $this->scheduleService = $scheduleService;

        $this->restDayService = $restDayService;

        $this->calendarService = $calendarService;
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/members/{memberId}",
    *     summary="Get Single Team Member",
    *     description="Get Single Team Member",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "id"=1,
    *                    "employee_id"="EMP-001",
    *                    "first_name"="Lorem",
    *                    "middle_name"="",
    *                    "last_name"="Ipsum",
    *                    "full_name"="Lorem Ipsum",
    *                    "avatar"=null,
    *                    "is_leader"=true,
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function get(Request $request, int $teamId, int $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        return $this->teamService->getSingleMember($teamId, $memberId);
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/members/{memberId}/calendar",
    *     summary="Get Single Team Member Calendar Data",
    *     description="Get Single Team Member Calendar Data",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="start_date",
    *         in="query",
    *         description="Start date in YYYY-MM-DD format",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Parameter(
    *         name="end_date",
    *         in="query",
    *         description="End date in YYYY-MM-DD format",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "2020-01-01": {
    *                        "shifts": {
    *                            {
    *                                "id": 1,
    *                                "schedule_id": 1,
    *                                "schedule": {
    *                                    "name": "My Schedule",
    *                                    "type": "fixed",
    *                                    "default": false,
    *                                    "day_type": "regular",
    *                                    "start_time": "08:00",
    *                                    "end_time": "17:00",
    *                                    "total_hours": "08:00"
    *                                }
    *                            }
    *                        },
    *                        "timesheet": {
    *                            {
    *                                "state": true,
    *                                "timestamp": 1578355200
    *                            },
    *                            {
    *                                "state": false,
    *                                "timestamp": 1578387600
    *                            }
    *                        },
    *                        "badges": {
    *                            {
    *                                "label": "On Leave",
    *                                "color": "primary"
    *                            },
    *                            {
    *                                "label": "Tardy",
    *                                "color": "danger"
    *                            },
    *                        }
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getCalendar(Request $request, int $teamId, int $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $this->validate($request, [
            'start_date' => 'required|date:Y-m-d|before_or_equal:end_date',
            'end_date' => 'required|date:Y-m-d|after_or_equal:start_date',
        ]);

        $startDate = Carbon::parse($request->input('start_date'))->startOfDay();
        $endDate = Carbon::parse($request->input('end_date'))->endOfDay();

        // Prevent requesting data with start and end date range greater than 60 days
        if ($startDate->diffInDays($endDate, true) > 60) {
            return $this->response->errorBadRequest(
                sprintf('%s is too far from %s', $endDate->format('Y-m-d'), $startDate->format('Y-m-d'))
            );
        }

        $dates = collect($this->calendarService->getDates($startDate, $endDate));

        $defaultSchedules = $this->calendarService->getDefaultSchedules($team['data']['company_id']);

        $shifts = $this->calendarService->getShifts($memberId, $startDate, $endDate);
        $restDays = $this->calendarService->getRestDays($memberId, $startDate, $endDate);
        $timesheets = $this->calendarService->getTimesheets($memberId, $startDate, $endDate);
        $badges = $this->calendarService->getBadges($memberId, $startDate, $endDate);

        $data = $dates->mapWithKeys(function ($date) use (
            $shifts,
            $restDays,
            $timesheets,
            $badges,
            $defaultSchedules
        ) {
            $shiftsData = $shifts->get($date, collect());

            if ($shiftsData->isEmpty() && $restDays->has($date) && $restDays->get($date)->isNotEmpty()) {
                $shiftsData->push(...$restDays->get($date));
            }

            if ($shiftsData->isEmpty()) {
                $dayOfWeek = Carbon::parse($date)->dayOfWeek;

                if ($dayOfWeek === Carbon::SUNDAY) {
                    $dayOfWeek = 7; // Use week day number 7 for Sunday
                }

                $shiftsData->push($defaultSchedules->get($dayOfWeek));
            }

            return [
                $date => [
                    'shifts' => $shiftsData,
                    'timesheet' => $timesheets->get($date, []),
                    'badges' => $badges->get($date, [])
                ]
            ];
        });

        return [
            'data' => $data
        ];
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/members/{memberId}/schedules",
    *     summary="Get entitled schedules of a team member",
    *     description="Get entitled schedules of a team member",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    {
    *                        "name": "My Schedule",
    *                        "type": "fixed",
    *                        "default": false,
    *                        "day_type": "regular",
    *                        "start_time": "08:00",
    *                        "end_time": "17:00",
    *                        "total_hours": "08:00"
    *                    },
    *                    {
    *                        "name": "Another Schedule",
    *                        "type": "fixed",
    *                        "default": false,
    *                        "day_type": "regular",
    *                        "start_time": "12:00",
    *                        "end_time": "20:00",
    *                        "total_hours": "08:00"
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getSchedules(Request $request, int $teamId, int $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $schedulesResponse = $this->scheduleService->getEmployeeSchedules(
            $memberId,
            $team['data']['company_id'],
            true
        );

        $schedules = json_decode($schedulesResponse->getData(), true);

        $result = new Collection($schedules, new EssEmployeeScheduleTransformer());

        return $this->fractal->createData($result)->toArray();
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/members/{memberId}/shifts",
    *     summary="Get shifts of a team member",
    *     description="Get shifts of a team member",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="date",
    *         in="query",
    *         description="Date to get shifts for",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    {
    *                        "id": 1,
    *                        "schedule_id": 1,
    *                        "schedule": {
    *                            "name": "My Schedule",
    *                            "type": "fixed",
    *                            "default": false,
    *                            "day_type": "regular",
    *                            "start_time": "08:00",
    *                            "end_time": "17:00",
    *                            "total_hours": "08:00"
    *                        }
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getShifts(Request $request, int $teamId, int $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $this->validate($request, [
            'date' => 'required|date:Y-m-d',
        ]);

        $date = $request->input('date');

        $startDate = Carbon::parse($date)->startOfDay();
        $endDate = Carbon::parse($date)->endOfDay();

        $shifts = $this->calendarService->getShifts($memberId, $startDate, $endDate);

        return [
            'data' => $shifts->get($date, [])
        ];
    }

    /**
    * @SWG\Post(
    *     path="/ess/teams/{id}/members/{memberId}/shifts",
    *     summary="Assign shift to team member",
    *     description="Assign shift to team member",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(
     *                 property="schedule_id",
     *                 type="integer"
     *             ),
     *             @SWG\Property(
     *                 property="start_date",
     *                 type="string"
     *             ),
     *             @SWG\Property(
     *                 property="end_date",
     *                 type="string"
     *             )
     *         )
     *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "id": 1,
    *                    "schedule_id": 1,
    *                    "schedule": {
    *                        "name": "My Schedule",
    *                        "type": "fixed",
    *                        "default": false,
    *                        "day_type": "regular",
    *                        "start_time": "08:00",
    *                        "end_time": "17:00",
    *                        "total_hours": "08:00"
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
    *         description="unassign shifts successful operation"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function assignShift(Request $request, int $teamId, int $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $data = array_merge($request->only(['schedule_id', 'start_date', 'end_date']), [
            'company_id' => $team['data']['company_id'],
            'employee_id' => $memberId
        ]);

        if (filter_var($data['schedule_id'], FILTER_VALIDATE_INT) === 0) {
            $this->shiftService->unassignOverwrite($data);

            return response(null, 204);
        }

        $shift = $this->shiftService->assignOverwrite($data);

        $result = new Item($shift, new EssEmployeeShiftTransformer());

        return $this->fractal->createData($result)->toArray();
    }

    /**
    * @SWG\Get(
    *     path="/ess/teams/{id}/members/{memberId}/rest_day",
    *     summary="Get team member rest day",
    *     description="Get team member rest day",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="date",
    *         in="query",
    *         description="Date to get shifts for",
    *         required=true,
    *         type="string"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "id": 1,
    *                    "company_id": 1,
    *                    "employee_id": 1,
    *                    "start_date": "2020-03-02",
    *                    "end_date": "2020-03-07",
    *                    "dates": {
    *                        "2020-03-02",
    *                        "2020-03-04",
    *                        "2020-03-07",
    *                    },
    *                    "repeat": {
    *                        "id": 1,
    *                        "repeat_every": 1,
    *                        "rest_days": {
    *                            "monday",
    *                            "wednesday",
    *                            "saturday"
    *                        },
    *                        "end_after": 3,
    *                        "end_never": false
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
    *         description="returns 204 no content if there are no user-defined rest day on the given date"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function getRestDay(Request $request, int $teamId, $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $date = $request->input('date', '');

        $restDay = $this->restDayService->getEmployeeRestDayByDate($memberId, $date);

        if (!$restDay) {
            return response(null, 204);
        }

        return [
            'data' => $restDay
        ];
    }

    /**
    * @SWG\Post(
    *     path="/ess/teams/{id}/members/{memberId}/rest_day",
    *     summary="Assign rest day to team member",
    *     description="Assign rest day to team member",
    *     tags={"ess"},
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         type="string",
    *         name="Authorization",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         type="string",
    *         name="X-Authz-Entities",
    *         in="header",
    *         required=true
    *     ),
    *     @SWG\Parameter(
    *         name="id",
    *         in="path",
    *         description="Team ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="memberId",
    *         in="path",
    *         description="Member ID",
    *         required=true,
    *         type="integer"
    *     ),
    *     @SWG\Parameter(
    *         name="body",
    *         in="body",
    *         required=true,
    *         @SWG\Schema(
    *             @SWG\Property(
    *                 property="start_date",
    *                 type="string"
    *             ),
    *             @SWG\Property(
    *                 property="repeat",
    *                 type="object",
    *                 @SWG\Property(
    *                     property="repeat_every",
    *                     type="integer"
    *                 ),
    *                 @SWG\Property(
    *                     property="end_after",
    *                     type="integer"
    *                 ),
    *                 @SWG\Property(
    *                     property="end_never",
    *                     type="boolean"
    *                 ),
    *                 @SWG\Property(
    *                     property="rest_days",
    *                     type="array",
    *                     @SWG\Items(type="string")
    *                 )
    *             )
    *         )
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
    *         description="successful operation",
    *         examples={
    *            "application/json": {
    *                "data": {
    *                    "id": 1,
    *                    "company_id": 1,
    *                    "employee_id": 1,
    *                    "start_date": "2020-03-02",
    *                    "end_date": "2020-03-07",
    *                    "dates": {
    *                        "2020-03-02",
    *                        "2020-03-04",
    *                        "2020-03-07",
    *                    },
    *                    "repeat": {
    *                        "id": 1,
    *                        "repeat_every": 1,
    *                        "rest_days": {
    *                            "monday",
    *                            "wednesday",
    *                            "saturday"
    *                        },
    *                        "end_after": 3,
    *                        "end_never": false
    *                    }
    *                }
    *            }
    *        }
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
    *         description="unassign shifts successful operation"
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
    *         description="Request not found",
    *     ),
    *     @SWG\Response(
    *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
    *         description="Invalid request",
    *     )
    * )
    */
    public function assignRestDay(Request $request, int $teamId, $memberId)
    {
        $requestor = $this->getFirstEmployeeUser($request);

        $teamResponse = $this->teamService->get($teamId, true);
        $team = json_decode($teamResponse->getData(), true);

        if ($team['data']['company_id'] !== $requestor['employee_company_id']
            || !in_array($requestor['employee_id'], $team['data']['leaders_ids'])) {
            abort(404, 'Team not found.');
        }

        $this->teamService->getSingleMember($teamId, $memberId)['data'];

        $data = array_merge($request->only(['repeat', 'start_date', 'end_date']), [
            'company_id' => $team['data']['company_id'],
            'employee_id' => $memberId
        ]);

        $restDay = $this->restDayService->assignOverwrite($data);

        return [
            'data' => $restDay
        ];
    }
}
