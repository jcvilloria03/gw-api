<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveType\LeaveTypeRequestService;
use App\Http\Controllers\EssBaseController;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;

class EssLeaveTypeController extends EssBaseController
{
    /**
     * @var \App\LeaveType\LeaveTypeRequestService
     */
    protected $leaveTypeRequestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $employeeRequestAuthorizationService;

    public function __construct(
        LeaveTypeRequestService $leaveTypeRequestService,
        EssEmployeeRequestAuthorizationService $employeeRequestAuthorizationService
    ) {
        $this->leaveTypeRequestService = $leaveTypeRequestService;
        $this->employeeRequestAuthorizationService = $employeeRequestAuthorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/leave_types",
     *     summary="Get Employee Entitled Leave Types",
     *     description="Get Employee Entitled Leave Types
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEntitledLeaveTypes(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request)
            && !$this->employeeRequestAuthorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        return $this->leaveTypeRequestService->getEntitledLeaveTypes($user['employee_id']);
    }
}
