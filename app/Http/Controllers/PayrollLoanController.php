<?php

namespace App\Http\Controllers;

use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\Facades\Company;
use App\Employee\EmployeeRequestService;
use App\GovernmentForm\GovernmentFormAuthorizationService;
use App\Payroll\PayrollAuthz;
use App\PayrollLoan\PayrollLoanDownloadRequestService;
use App\PayrollLoan\PayrollLoanRequestService;
use App\PayrollLoan\PayrollLoanAuthorizationService;
use App\PayrollLoan\PayrollLoanUploadTask;
use App\PayrollLoan\PayrollLoanUploadTaskException;
use App\PayrollLoan\PayrollLoanDownloadTask;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Storage\UploadService;
use Symfony\Component\HttpFoundation\Response;
use App\Jobs\JobsRequestService;
use App\Authz\AuthzDataScope;
use App\Payroll\PayrollRequestService;
use App\PayrollLoan\PayrollLoanService;
use App\Traits\AuditTrailTrait;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PayrollLoanController extends Controller
{
    use AuditTrailTrait;

    const FIRST_PAY_VALUE = 'FIRST_PAY_OF_THE_MONTH';
    const FIRST_PAY_LABEL = 'First pay of the month';
    const LAST_PAY_VALUE = 'LAST_PAY_OF_THE_MONTH';
    const LAST_PAY_LABEL = 'Last pay of the month';
    const FIRST_AND_LAST_PAY_VALUE = 'FIRST_AND_LAST_PAY_OF_THE_MONTH';
    const FIRST_AND_LAST_PAY_LABEL = 'First and last pay of the month';
    const EVERY_PAY_VALUE = 'EVERY_PAY_OF_THE_MONTH';
    const EVERY_PAY_LABEL = 'Every pay of the month';

    const FREQUENCIES = [
        self::FIRST_PAY_VALUE,
        self::LAST_PAY_VALUE,
        self::FIRST_AND_LAST_PAY_VALUE,
        self::EVERY_PAY_VALUE,
    ];

    const SUBTYPES = [
        'SALARY',
        'CALAMITY',
        'EDUCATIONAL',
        'EMERGENCY',
        'STOCK_INVESTMENT',
        'MULTI_PURPOSE',
        'HOUSING',
        'SHORT_TERM',
        'CALAMITY',
    ];

    const ACTIVE = 1;
    const INACTIVE = 0;

    const STATUSES = [
        self::ACTIVE,
        self::INACTIVE
    ];

    /**
     * @var \App\PayrollLoan\PayrollLoanRequestService
     */
    protected $requestService;

    /**
     * @var \App\PayrollLoan\PayrollLoanAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\GovernmentForm\GovernmentFormAuthorizationService
     */
    protected $governmentFormAuthorizationService;

    /**
     * @var App\CSV\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var PayrollLoanDownloadRequestService
     */
    protected $downloadService;

    public function __construct(
        PayrollLoanRequestService $requestService,
        PayrollLoanAuthorizationService $authorizationService,
        GovernmentFormAuthorizationService $governmentFormAuthorizationService,
        CsvValidator $csvValidator,
        PayrollLoanDownloadRequestService $downloadService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->governmentFormAuthorizationService = $governmentFormAuthorizationService;
        $this->csvValidator = $csvValidator;
        $this->downloadService = $downloadService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payroll_loans",
     *     summary="Get all Payroll Loans",
     *     description="Get all payroll loans for company.
Authorization Scope : **view.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Target Page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Rows per page",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLoans(Request $request, int $companyId)
    {
        $params = $request->only(["page", "per_page", "order_by", "order_dir"]);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetAll(
                $data,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyLoans(
            $companyId,
            $params,
            $this->getAuthzDataScope($request)
        );

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/months_with_payroll_loans/{type}",
     *     summary="Get months with at least one collected loan",
     *     description="Get months with at least one collected loan
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="System Defined Loan type name.",
     *         required=true,
     *         type="string",
     *         enum={"SSS", "Pag-ibig", "Gap"}
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function monthsWithCollectedLoans(Request $request, $id, $type)
    {
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];

            $isAuthorized = $this->governmentFormAuthorizationService->authorizeCreate(
                $data,
                $request->attributes->get('user')
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->monthsWithCollectedLoans($id, $type);
        $loansData = json_decode($response->getData());

        if (empty($loansData)) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan",
     *     summary="Create new payroll loan in redis",
     *     description="Create new user or system defined payroll loan in redis
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Unique identifier that corresponds to employee.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="reference_no",
     *         in="formData",
     *         description="Used to reference the loan voucher.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="formData",
     *         description="Possible values are defined under company settings.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="subtype",
     *         in="formData",
     *         description="SSS/Pag-ibig loan type",
     *         type="string",
     *         enum=App\Http\Controllers\PayrollLoanController::SUBTYPES
     *     ),
     *     @SWG\Parameter(
     *         name="created_date",
     *         in="formData",
     *         description="Date issued. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_start_date",
     *         in="formData",
     *         description="Start date of pay period. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="total_amount",
     *         in="formData",
     *         description="Total amount of loan.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="monthly_amortization",
     *         in="formData",
     *         description="Monthly deduction to be made on affected payroll runs.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_scheme",
     *         in="formData",
     *         description="Determines which pay runs get deducted every month.",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\PayrollLoanController::FREQUENCIES
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Number of months to return the loan",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="active",
     *         in="formData",
     *         description="Loan status. Default is 1 (Active). For gap loans use 0 (inactive) if you are in the middle
               of gap transaction process",
     *         required=false,
     *         type="integer",
     *         enum=\App\Http\Controllers\PayrollLoanController::STATUSES
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll id used for gap loans",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_end_date",
     *         in="formData",
     *         description="End date of pay period. Format: (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function createInitialPreview(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        PayrollRequestService $payrollRequestService
    ) {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $companyIds = [];
            $payrollGroupIds = [];

            $employeeData = json_decode(
                $employeeRequestService->getEmployee($request->input('employee_id'))->getData(),
                true
            );

            $companyIds[] = data_get($employeeData, 'company_id');
            $payrollGroupIds[] = data_get($employeeData, 'payroll_group.id');

            if ($request->input('payroll_id')) {
                $payrollData = json_decode(
                    $payrollRequestService->get($request->input('payroll_id'))->getData(),
                    true
                );

                $companyIds[] = data_get($payrollData, 'company_id');
                $payrollGroupIds[] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $companyIds,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id')
                ]
            );
        } else {
            $data = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->createInitialPreview($request->all());

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            // trigger audit trail
            $this->audit($request, $companyId, $responseData);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/create/{uid}",
     *     summary="Create new payroll loan in DB",
     *     description="Create new user or system defined payroll loan in DB
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Unique identifier that corresponds to employee.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="reference_no",
     *         in="formData",
     *         description="Used to reference the loan voucher.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="formData",
     *         description="Possible values are defined under company settings.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="subtype",
     *         in="formData",
     *         description="SSS/Pag-ibig loan type",
     *         type="string",
     *         enum=App\Http\Controllers\PayrollLoanController::SUBTYPES
     *     ),
     *     @SWG\Parameter(
     *         name="created_date",
     *         in="formData",
     *         description="Date issued. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_start_date",
     *         in="formData",
     *         description="Start date of pay period. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="total_amount",
     *         in="formData",
     *         description="Total amount of loan.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="monthly_amortization",
     *         in="formData",
     *         description="Monthly deduction to be made on affected payroll runs.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_scheme",
     *         in="formData",
     *         description="Determines which pay runs get deducted every month.",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\PayrollLoanController::FREQUENCIES
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Number of months to return the loan",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="active",
     *         in="formData",
     *         description="Loan status. Default is 1 (Active). For gap loans use 0 (inactive) if you are in the middle
               of gap transaction process",
     *         required=false,
     *         type="integer",
     *         enum=\App\Http\Controllers\PayrollLoanController::STATUSES
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll id used for gap loans",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="uid",
     *         in="path",
     *         description="Unique identifier that corresponds to loan.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_end_date",
     *         in="formData",
     *         description="End date of pay period. Format: (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        PayrollRequestService $payrollRequestService,
        $uid
    ) {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        if ($this->isAuthzEnabled($request)) {
            $companyIds = [];
            $payrollGroupIds = [];

            $employeeData = json_decode(
                $employeeRequestService->getEmployee($request->input('employee_id'))->getData(),
                true
            );

            $companyIds[] = data_get($employeeData, 'company_id');
            $payrollGroupIds[] = data_get($employeeData, 'payroll_group.id');

            if ($request->input('payroll_id')) {
                $payrollData = json_decode(
                    $payrollRequestService->get($request->input('payroll_id'))->getData(),
                    true
                );

                $companyIds[] = data_get($payrollData, 'company_id');
                $payrollGroupIds[] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $companyIds,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id')
                ]
            );
        } else {
            $data = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->create($uid, $request->all());

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);

            // get loan regarding to id from response
            $responseData = $this->requestService->get($responseData['id']);
            $loan = json_decode($responseData->getData(), true);

            // trigger audit trail
            $this->audit($request, $companyId, $loan);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/{id}",
     *     summary="Edit Payroll Loan save in DB",
     *     description="Edit Payroll Loan Fields and save in DB
Authorization Scope : **edit.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll loan ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Unique identifier that corresponds to employee.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="reference_no",
     *         in="formData",
     *         description="Used to reference the loan voucher.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="formData",
     *         description="Possible values are defined under company settings.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="subtype",
     *         in="formData",
     *         description="SSS/Pag-ibig loan type",
     *         type="string",
     *         enum=App\Http\Controllers\PayrollLoanController::SUBTYPES
     *     ),
     *     @SWG\Parameter(
     *         name="created_date",
     *         in="formData",
     *         description="Date issued. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_start_date",
     *         in="formData",
     *         description="Start date of pay period. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="total_amount",
     *         in="formData",
     *         description="Total amount of loan.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="monthly_amortization",
     *         in="formData",
     *         description="Monthly deduction to be made on affected payroll runs.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_scheme",
     *         in="formData",
     *         description="Determines which pay runs get deducted every month.",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\PayrollLoanController::FREQUENCIES
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Number of months to return the loan",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="active",
     *         in="formData",
     *         description="Loan status. Default is 1 (Active). For gap loans use 0 (inactive) if you are in the middle
               of gap transaction process",
     *         required=false,
     *         type="integer",
     *         enum=\App\Http\Controllers\PayrollLoanController::STATUSES
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll id used for gap loans",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_end_date",
     *         in="formData",
     *         description="End date of pay period. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        PayrollRequestService $payrollRequestService,
        $id
    ) {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);

        $oldData = json_decode($response->getData());
        $oldData->account_id = Company::getAccountId($oldData->company_id);

        if ($this->isAuthzEnabled($request)) {
            $companyIds = [];
            $payrollGroupIds = [];

            $employeeData = json_decode(
                $employeeRequestService->getEmployee($request->input('employee_id'))->getData(),
                true
            );

            $companyIds[] = data_get($employeeData, 'company_id');
            $payrollGroupIds[] = data_get($employeeData, 'payroll_group.id');

            if ($request->input('payroll_id')) {
                $payrollData = json_decode(
                    $payrollRequestService->get($request->input('payroll_id'))->getData(),
                    true
                );

                $companyIds[] = data_get($payrollData, 'company_id');
                $payrollGroupIds[] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $companyIds,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id')
                ]
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldData, $updatedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($id, $request->all());

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);

        if ($getResponse->isSuccessful()) {
            $oldData = json_decode($response->getData(), true);
            $newData = json_decode($getResponse->getData(), true);

            // trigger audit trail
            $companyId = isset($oldData->company_id) ? $oldData->company_id : 0;
            $this->audit($request, $companyId, $newData, $oldData, true);
        }

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/{id}/update_loan_preview",
     *     summary="Edit loan and save it as temporary for preview",
     *     description="Edit loan and generate new amortizations before saving to DB
    Authorization Scope : **edit.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll loan ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         description="Unique identifier that corresponds to employee.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="reference_no",
     *         in="formData",
     *         description="Used to reference the loan voucher.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type_id",
     *         in="formData",
     *         description="Possible values are defined under company settings.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="subtype",
     *         in="formData",
     *         description="SSS/Pag-ibig loan type",
     *         type="string",
     *         enum=App\Http\Controllers\PayrollLoanController::SUBTYPES
     *     ),
     *     @SWG\Parameter(
     *         name="created_date",
     *         in="formData",
     *         description="Date issued. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_start_date",
     *         in="formData",
     *         description="Start date of pay period. Format: (yyyy-mm-dd)",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="total_amount",
     *         in="formData",
     *         description="Total amount of loan.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="monthly_amortization",
     *         in="formData",
     *         description="Monthly deduction to be made on affected payroll runs.",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="payment_scheme",
     *         in="formData",
     *         description="Determines which pay runs get deducted every month.",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\PayrollLoanController::FREQUENCIES
     *     ),
     *     @SWG\Parameter(
     *         name="payment_end_date",
     *         in="formData",
     *         description="End date of pay period. Format: (yyyy-mm-dd)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="formData",
     *         description="Number of months to return the loan",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="active",
     *         in="formData",
     *         description="Loan status. Default is 1 (Active). For gap loans use 0 (inactive) if you are in the middle
               of gap transaction process",
     *         required=false,
     *         type="integer",
     *         enum=\App\Http\Controllers\PayrollLoanController::STATUSES
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         in="formData",
     *         description="Payroll id used for gap loans",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateLoanPreview(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        PayrollRequestService $payrollRequestService,
        $id
    ) {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);

        $oldData = json_decode($response->getData());
        $oldData->account_id = Company::getAccountId($oldData->company_id);

        if ($this->isAuthzEnabled($request)) {
            $companyIds = [];
            $payrollGroupIds = [];

            $employeeData = json_decode(
                $employeeRequestService->getEmployee($oldData->employee_id)->getData(),
                true
            );

            $companyIds[] = data_get($employeeData, 'company_id');
            $payrollGroupIds[] = data_get($employeeData, 'payroll_group.id');

            if ($oldData->payroll_id) {
                $payrollData = json_decode(
                    $payrollRequestService->get($oldData->payroll_id)->getData(),
                    true
                );

                $companyIds[] = data_get($payrollData, 'company_id');
                $payrollGroupIds[] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $companyIds,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id')
                ]
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldData, $updatedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updatedResponse =  $this->requestService->updatePreview($id, $inputs);

        if ($updatedResponse->isSuccessful()) {
            $oldData = json_decode($response->getData(), true);
            $newData = json_decode($updatedResponse->getData(), true);
            $companyId = isset($oldData->company_id) ? $oldData->company_id : 0;
            // trigger audit trail
            $this->audit($request, $companyId, $newData, $oldData, true);
        }

        return $updatedResponse;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/{id}/update_amortization_preview",
     *     summary="Edit amortization and save it as temporary for preview",
     *     description="Preview edited amortizations before saving
    Authorization Scope : **edit.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll loan ID on edit or UID loan on create",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="uid",
     *         in="formData",
     *         description="Unique id for temp storage",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="amount_collected",
     *         in="formData",
     *         description="Amount collected for specific amortization",
     *         required=true,
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employer_remarks",
     *         in="formData",
     *         description="Optional text to output to report.",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateAmortizationPreview(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        PayrollRequestService $payrollRequestService,
        $id
    ) {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $companyId = $inputs['company_id'];

        if ($this->isAuthzEnabled($request)) {
            $companyIds = [];
            $payrollGroupIds = [];

            $payrollId = null;
            $employeeId = null;
            $tempLoanData = null;

            // Payroll loan ID can be a string if it is temporarily stored and not yet saved to the DB.
            // This is used to differentiate between payroll loan ID and payroll loan temp ID.
            $isTempPayrollLoan = !filter_var($id, FILTER_VALIDATE_INT);

            if ($isTempPayrollLoan) {
                $tempLoanData = $this->requestService->getTemp($id);

                $employeeId = (int) $tempLoanData['employee_id'];
            } else {
                $loanData = json_decode($this->requestService->get($id)->getData(), true);

                $employeeId = $loanData['employee_id'];
                $payrollId = $loanData['payroll_id'];
            }

            $employeeData = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $companyIds[] = data_get($employeeData, 'company_id');
            $payrollGroupIds[] = data_get($employeeData, 'payroll_group.id');

            if (!empty($payrollId)) {
                $payrollData = json_decode(
                    $payrollRequestService->get($payrollId)->getData(),
                    true
                );

                $companyIds[] = data_get($payrollData, 'company_id');
                $payrollGroupIds[] = data_get($payrollData, 'payroll_group_id');
            }

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized(
                [
                    AuthzDataScope::SCOPE_COMPANY => $companyIds,
                    AuthzDataScope::SCOPE_PAYROLL_GROUP => $payrollGroupIds,
                    AuthzDataScope::SCOPE_DEPARTMENT => data_get($employeeData, 'department_id'),
                    AuthzDataScope::SCOPE_LOCATION => data_get($employeeData, 'location_id'),
                    AuthzDataScope::SCOPE_POSITION => data_get($employeeData, 'position_id'),
                    AuthzDataScope::SCOPE_TEAM => data_get($employeeData, 'team_id')
                ]
            );
        } else {
            $data = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate($data, $updatedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->updateAmortizationPreview($id, $inputs);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        if ($updateResponse->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($updateResponse->getData(), true);
            $this->audit($request, $companyId, $newData, $tempLoanData ?? $loanData, true);
        }

        return $updateResponse;
    }

    /**
     * @SWG\Get(
     *     path="/payroll_loan/{id}/initial_preview",
     *     summary="Preview loan with amortizations for edited payroll loan details",
     *     description="Preview loan and amortizations before saving to DB
    Authorization Scope : **edit.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll loan Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function initialPreview(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        $id
    ) {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldData = json_decode($response->getData());
        $oldData->account_id = Company::getAccountId($oldData->company_id);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($oldData->employee_id)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $oldData->company_id,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->initialPreview($id, $inputs);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll_loan/bulk_delete",
     *     summary="Delete multiple Payroll Loans",
     *     description="Delete multiple Payroll Loans,
Authorization Scope : **delete.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_loans_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll loans ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function bulkDelete(
        Request $request,
        PayrollLoanService $loanService
    ) {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        $errorMessage = 'Unauthorized';
        $isAuthorized = false;
        $companyId = $request->input('company_id', 0);
        $loanIds = $request->input('payroll_loans_ids', []);

        if ($this->isAuthzEnabled($request)) {
            $this->validate(
                $request,
                [
                    'company_id' => 'required|integer',
                    'payroll_loans_ids' => 'required|array',
                    'payroll_loans_ids.*' => 'integer',
                ]
            );

            $payrollAuthorizationData = $loanService->checkPayrollLoanScope(
                $this->getAuthzDataScope($request),
                $companyId,
                $loanIds
            );

            $unauthorizedPayrollLoanIds = $payrollAuthorizationData->where('is_authorized', false);

            if (!$unauthorizedPayrollLoanIds->isEmpty()) {
                $errorMessage = 'Unauthorized. Cannot delete Loan Id(s): [' .
                    implode(',', $unauthorizedPayrollLoanIds->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = true;
            }
        } else {
            $payrollLoanData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id'],
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($payrollLoanData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMessage);
        }

        // for audit trail
        $oldLoans = $this->requestService->getCompanyLoansByAttribute(
            $request->get('company_id'),
            'id',
            ['values' => implode(',', $loanIds)]
        );

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            $oldLoansData = json_decode($oldLoans->getData(), true);
            foreach ($oldLoansData as $oldData) {
                // trigger audit trail
                $this->audit($request, $companyId, [], $oldData);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/payroll_loans/generate_csv",
     *     summary="Generate CSV with payroll loans",
     *     description="Generate CSV with payroll loans within company for given ids.
    Authorization Scope : **view.payroll_loan**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="loan_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Loan Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function generateCsv(
        Request $request,
        PayrollLoanService $loanService,
        $companyId
    ) {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        $loanIds = $request->input('loan_ids', []);

        $errorMessage = 'Unauthorized';
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $this->validate(
                $request,
                [
                    'loan_ids' => 'required|array',
                    'loan_ids.*' => 'integer',
                ]
            );

            $payrollAuthorizationData = $loanService->checkPayrollLoanScope(
                $this->getAuthzDataScope($request),
                $companyId,
                $loanIds
            );

            $unauthorizedPayrollLoanIds = $payrollAuthorizationData->where('is_authorized', false);

            if (!$unauthorizedPayrollLoanIds->isEmpty()) {
                $errorMessage = 'Unauthorized. Cannot delete Loan Id(s): [' .
                    implode(',', $unauthorizedPayrollLoanIds->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = true;
            }
        } else {
            $payrollLoanData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id'],
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($payrollLoanData, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMessage);
        }

        $response = $this->downloadService->download($companyId, $request->all());

        if ($response->isSuccessful()) {
            $loans = $this->requestService->getCompanyLoansByAttribute(
                $companyId,
                'id',
                ['values' => implode(',', $loanIds)]
            );
            $loansData = array_get(
                json_decode($loans->getData(), true),
                'data',
                []
            );
            // trigger audit trail
            $this->audit($request, $companyId, [], $loansData);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/payroll_loan/{id}/loan_details",
     *     summary="Get Payroll Loan details",
     *     description="Get Payroll loan for company.
Authorization Scope : **view.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Loan ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getLoanDetails($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $loanData = json_decode($response->getData());

        if (empty($loanData)) {
            return $response;
        }

        $data = (object) [
            'account_id' => Company::getAccountId($loanData->company_id),
            'company_id' => $loanData->company_id
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeGet(
            $data,
            $request->attributes->get('user')
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        return $this->requestService->getLoanDetails($id);
    }

    /**
     * @SWG\Get(
     *     path="/payroll_loan/{id}/amortizations_details",
     *     summary="Get Payroll Loan amortizations details",
     *     description="Get all amortizations for given payroll loan id.
Authorization Scope : **view.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Loan ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAmortizationsDetailsOfLoan($id, Request $request)
    {
        $response = $this->requestService->get($id);
        $loanData = json_decode($response->getData());

        if (empty($loanData)) {
            return $response;
        }

        $data = (object) [
            'account_id' => Company::getAccountId($loanData->company_id),
            'company_id' => $loanData->company_id
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeGet(
            $data,
            $request->attributes->get('user')
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        return $this->requestService->getAmortizationsDetailsOfLoan($id);
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/upload",
     *     summary="Upload Payroll Loans",
     *     description="Uploads Payroll Loans
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "government_loan_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

        $authzDataScope = $this->getAuthzDataScope($request);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $uploadService = \App::make(UploadService::class);
            $s3Key = $uploadService->saveFile($file, $createdBy, 'public-read');
            $s3Bucket = $uploadService->getS3Bucket();

            $response = $this->requestService->processFileUpload(
                $companyId,
                $s3Bucket,
                $s3Key,
                $authzDataScope
            );

            $responseData = json_decode($response->getOriginalContent(), true)["data"];
            $jobId = $responseData['id'];

            return response()->json([
                'id' => $jobId,
                'job_id' => $jobId
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan/upload/save",
     *     summary="Save Payroll Loans",
     *     description="Saves Payroll Loans from Previously Uploaded CSV
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     deprecated=true,
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "government_loan_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Payroll Loans needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }


        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(PayrollLoanUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(PayrollLoanUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (PayrollLoanUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        \Amqp::publish(config('queues.payroll_loan_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/payroll_loan/upload/status",
     *     summary="Get Job Status",
     *     description="Get Payroll Loan Upload Status
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Name is invalid"
     *                          },
     *                          "4": {
     *                              "Email is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //TODO:  This same code exists in OtherIncomeController
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        $jobId = $request->input('job_id');
        //return $this->requestService->getFileUploadStatus($companyId, $type, $jobId);
        $response = $this->requestService->getFileUploadStatus($companyId, $jobId);
        //TODO:: make this just return JM-API response
        if ($response->status() == Response::HTTP_NOT_FOUND) {
            $this->notFoundError('Job not found');
        }

        $responseBody = json_decode($response->getOriginalContent(), true);
        $data = $responseBody['data'];
        $attributes = $data['attributes'];
        $status = $attributes['status'];
        $errors = $responseBody['included'] ?? null;

        //TODO: temporary matching
        $returnedErrors = null;
        $returnedStatus = "saved";
        $errorName = null;
        $error = null;
        if (empty($errors) && $status == 'PENDING') {
            $returnedStatus = "validating";
        } else if (!empty($errors) && $status == 'FINISHED') {
            $error = $errors[0]["attributes"]["description"];
            $errorName = $error["name"];
        }

        if ($errorName == "task-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = (object) ["0" =>  $error["errors"]];
        } else if ($errorName == "validation-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = JobsRequestService::flattenErrorData($error["errors"]);
        }

        return response()->json([   //TODO:: replace this with JM-API raw response
            'status' => $returnedStatus,
            'errors' => $returnedErrors
        ]);
    }


    /**
     * getCompanyAccountId
     */
    public function getCompanyAccountId(int $companyId)
    {
        return Company::getAccountId($companyId);
    }


    /**
     * @SWG\Get(
     *     path="/payroll_loan/upload/preview",
     *     summary="Get Payroll Loans Preview",
     *     description="Get Payroll Loans Upload Preview
Authorization Scope : **create.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                (int) $companyId
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(PayrollLoanUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (PayrollLoanUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/activate_gap_loans",
     *     summary="Activate gap loans after loan transaction is finished",
     *     description="Activate gap loans after loan transaction is finished.
    Authorization Scope : **edit.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_id",
     *         type="integer",
     *         in="formData",
     *         description="Payroll ID",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * )
     */
    public function activateGapLoans(Request $request, PayrollAuthz $payrollAuthz, $id)
    {
        $user = $request->attributes->get('user');
        $payrollId = $request->get('payroll_id');
        $authzDataScope = $this->getAuthzDataScope($request);
        if ($this->isAuthzEnabled($request)) {
            if (!$payrollAuthz->isAuthorized($payrollId, $authzDataScope)) {
                $this->response()->errorUnauthorized();
            }
        }
        $data['values'] = implode(',', [$payrollId]);
        $payrollLoansResponse = $this->requestService->getCompanyLoansByAttribute($id, 'payroll_id', $data);
        $payrollLoans = json_decode($payrollLoansResponse->getData());
        $authorizedIds = [];
        foreach ($payrollLoans->data as $loan) {
            if ($this->isAuthzEnabled($request)
                && $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $loan->company_id)
            ) {
                $authorizedIds[] = $loan->id;
            } elseif (!$this->isAuthzEnabled($request)) {
                $payrollLoan = (object) [
                    'payroll_loan_id' => $loan->id,
                    'account_id' => Company::getAccountId($loan->company_id),
                    'company_id' => $loan->company_id
                ];
                if ($this->authorizationService->authorizeUpdate($payrollLoan, $user)) {
                    $authorizedIds[] = $loan->id;
                }
            }
        }

        $unauthorized = count($payrollLoans->data) !== count($authorizedIds);

        if ($unauthorized) {
            $this->response()->errorUnauthorized('Not authorized to update all specified gap loans');
        }

        return $this->requestService->activateGapLoans($id, $request->all());
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/employee/{employeeId}/payroll_loans",
     *     summary="Get Employee Active Loans",
     *     description="Get all active loans for given employee id.
Authorization Scope : **view.payroll_loans**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeActiveLoans(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $companyId,
        int $employeeId
    ) {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGetAll(
                $data,
                $createdBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getEmployeeActiveLoans(
            $companyId,
            $employeeId
        );
    }

    /**
     * @SWG\Post(
     *     path="/company/{companyId}/payroll_loans/download",
     *     summary="Generate CSV with payroll loans",
     *     description="Generate CSV with payroll loans within company for given ids
Authorization Scope : **view.payroll_loan**",
     *     tags={"payroll_loan"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="loan_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Credits Ids",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         in="formData",
     *         description="Download all indicator",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadPayrollLoans(
        Request $request,
        $companyId
    ) {

        $authzEnabled = $request->attributes->get('authz_enabled');

        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $payrollLoan = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($payrollLoan, $request->attributes->get('user'));
        }


        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Create status cache Check job exists (will throw exception if job doesn't exist)
        try {
            $uploadTask = App::make(PayrollLoanDownloadTask::class);
            $uploadTask->create($companyId);
            $jobId = $uploadTask->getId();
        } catch (PayrollLoanUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // Drop to queue
        $details = [
            'id'        => $jobId,
            'task'      => 'download',
            'authz_data_scope' => $this->getAuthzDataScope($request)->getDataScope(),
            'request'   => array_merge(['id' => $companyId], $request->all())
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.payroll_loans_download_queue'), $message);

        return ['data' => ['job_id' => $jobId]];
    }

    /**
     * @SWG\POST(
     *     path="/{companyId}/payroll_loans/download/status",
     *     summary="Get Leave Credit Download Status",
     *     description="Get Leave Credit Download Status
Authorization Scope : **view.leave_credit**",
     *     tags={"leave_credit"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="x-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function getPayrollLoansDownloadStatus(Request $request, $id)
    {
        $attributes = $request->only(['job_id']);
        $jobId = Arr::get($attributes, 'job_id', null);
        $downloadTask = App::make(PayrollLoanDownloadTask::class);
        $cache = $downloadTask->getRedisKey($id, $jobId);
        return $cache;
    }
}
