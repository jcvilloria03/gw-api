<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request as LaravelRequest;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Authz\AuthzDataScope;
use App\User\UserRequestService;
use App\AuditTrail\AuditTrailAuthorizationService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class AuditTrailController extends Controller
{

    const NAME_OWNER_ROLE = 'Owner';
    const NAME_OWNER_WITH_ESS = 'Owner with ESS';
    const NAME_SUPER_ADMIN = 'Super Admin';
    const NAME_SUPER_ADMIN_WITH_ESS = 'Super Admin with ESS';

    const AUTHORIZED_ROLES = [
        self::NAME_OWNER_ROLE,
        self::NAME_OWNER_WITH_ESS,
        self::NAME_SUPER_ADMIN,
        self::NAME_SUPER_ADMIN_WITH_ESS
    ];

    protected $client;

    /**
     * @var \App\AuditTrail\AuditTrailAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->authorizationService = app(AuditTrailAuthorizationService::class);
        $this->userRequestService = app(UserRequestService::class);
        $this->moduleNames = config('audit_trail.module_names');
    }

    public function passThroughToApi(LaravelRequest $request)
    {
        $requestContent = $request->getContent();
        $requestPath    = substr($request->path(), strlen('audit-trail'));

        $jsonContent = null;
        if (!empty($requestContent)) {
            $jsonContent = json_decode($requestContent, true);
            // make sure content body is a valid json
            if (null === $jsonContent) {
                throw new HttpException(400, "Request content is an invalid json.");
            }
        }

        $user               = $request->attributes->get('user');
        $requestQuery       = $request->query();

        $userResponse       = $this->userRequestService->getUserBydId($user['user_id']);
        $userData           = json_decode($userResponse->getData());

        // authorize via user role
        $roleParts          = explode('-', $userData->role_name);
        $roleName           = trim(end($roleParts));
        $isRoleAuthorized   = in_array($roleName, self::AUTHORIZED_ROLES);

        if (!$isRoleAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $jsonContent = array_merge(
            $jsonContent,
            ['account_id' => $userData->account_id],
            ['user_email' => $userData->email],
            ['first_name' => $userData->first_name]
        );


        try {
            $options = [
                'headers' => [
                    'Authorization' => $request->header('Authorization'),
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'query' => $requestQuery,
            ];

            if (!empty($jsonContent)) {
                $options['json'] = $jsonContent;
            }

            $response = $this->client->request(
                $request->method(),
                $requestPath,
                $options
            );

            return $response;
        } catch (BadResponseException $e) {
            // just throw the internal api reponse
            $response = $e->getResponse();

            $headers = $response->getHeaders();
            if (array_key_exists('AuditTrail-Src', $headers)) {
                $baseUriArr = array_reverse(explode('.', $headers['AuditTrail-Src'][0]));
                $headers['AuditTrail-Src'] = $baseUriArr[min(count($baseUriArr)-1, 1)];
            }

            return new Response(
                $response->getBody()->getContents(),
                $response->getStatusCode(),
                $headers
            );
        }
    }

    public function getModuleList()
    {
        $modules = [];

        try {
            $modules =  collect($this->moduleNames)->unique()->sort()->values()->all();
        } catch (\Exception $e) {
            \Log::error('Audit Trail Modules: ' . $e->getMessage());
            \Log::error($e->getTraceAsString());

            abort(404, 'Error encountered while fetching module names.');
        }

        return $modules;
    }
}
