<?php

namespace App\Http\Controllers;

use App\ProductSeat\ProductSeatRequestService;
use Illuminate\Http\Request;
use App\Authn\AuthnService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Account\AccountRequestService;
use App\Authz\AuthzDataScope;
use Illuminate\Support\Arr;

class ProductSeatController extends Controller
{
    /**
     * @var \App\ProductSeat\ProductSeatRequestService
     */
    protected $requestService;

    /**
     * App\Authn\AuthnService
     */
    private $authnService;

    /**
     * ProductSeatController constructor.
     */
    public function __construct(
        ProductSeatRequestService $productSeatRequestService,
        AuthnService $authnService
    ) {
        $this->requestService = $productSeatRequestService;
        $this->authnService = $authnService;
    }

    /**
     * @SWG\Get(
     *     path="/available_product_seats",
     *     summary="Get All Available Product Seats",
     *     description="Get list of all available Product Seats",
     *     tags={"product_seat"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized request",
     *     )
     * )
     */
    public function getAllAvailable(
        Request $request,
        SubscriptionsRequestService $subscrpitionsService
    ) {
        $newProductSeats = [];
        $userData = $request->attributes->get('user');
        $accountId = $userData['account_id'];

        $isAuthorized = false;
        if ($this->isAuthzEnabled($request)) {
            $accountRequestService = app()->make(AccountRequestService::class);
            $accountCompaniesResponse = $accountRequestService->getAccountCompanies($accountId);
            $accountCompaniesData = json_decode($accountCompaniesResponse->getData(), true);
            $companyId = Arr::get($userData, 'employee_company_id') ?? current($accountCompaniesData['data'])['id'];

            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            // no old rbac authorization check
            // auto pass
            $isAuthorized = true;
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Request the old product seat data
        $oldSeatsRequest = $this->requestService->getAllAvailable();
        $oldSeatsData = json_decode($oldSeatsRequest->getData(), true);

        // Filter only the name from the product seat response
        $oldCodes = array_map(function ($item) {
            return $item["name"];
        }, $oldSeatsData['data']);

        // Retreive HRIS
        $newProductSeats[] = array_filter($oldSeatsData['data'], function ($item) {
            return $item["name"] == 'hris';
        })[0];

        // Retreive the account plan products purchased by the account owner
        $planProductsRequest = $subscrpitionsService->getAccountProducts(
            $accountId
        );

        $planProductsData = json_decode(
            $planProductsRequest->getData(),
            true
        );

        // Filter only the product codes. This will be
        // used for intersecting the product seat names & plan product codes
        $planCodes = array_map(function ($item) {
            return $item["code"];
        }, $planProductsData['data']);

        $new = array_intersect($oldCodes, $planCodes);
        // Construct the response data by starting to
        // iterate through the old product seats data. This
        // is important so that we could retain the product
        // seat name and id thus making this backwards compat.
        foreach ($oldSeatsData['data'] as $item) {
            if (in_array($item['name'], $new)) {
                $currentCode = $item['name'];

                // Get the new product seat data for data from
                // subscriptions
                $planData = array_filter(
                    $planProductsData['data'],
                    function ($plan) use ($currentCode) {
                        return $currentCode == $plan['code'];
                    }
                );
                $newProductSeats[] = [
                    'id'                      => $item['id'],
                    'name'                    => $item['name'],
                    'subscription_product_id' => end($planData)['id']
                ];
            }
        }

        return ['data' => $newProductSeats];
    }

    /**
     * @SWG\Get(
     *     path="/product-seat-details/{companyId}",
     *     summary="Get all product seats details",
     *     description="Get all product seats details",
     *     tags={"product_seat"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company ID",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized request",
     *     )
     * )
     */
    public function getProductSeatsDetails(Request $request, $companyId = null)
    {
        $userData = $request->attributes->get('user');
        $accountId = $userData['account_id'];
        $companyId = (int) $companyId;

        $activeUserIds = $this->authnService->getActiveUserIds($accountId);

        return $this->requestService->getProductSeatsDetails(
            $accountId,
            $activeUserIds,
            $companyId ?: null
        );
    }
}
