<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\CostCenter\CostCenterAuditService;
use App\CostCenter\CostCenterAuthorizationService;
use App\CostCenter\CostCenterEsIndexQueueService;
use App\CostCenter\CostCenterRequestService;
use App\Facades\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

class CostCenterController extends Controller
{
    /**
     * @var \App\CostCenter\CostCenterRequestService
     */
    private $requestService;

    /**
     * @var \App\CostCenter\CostCenterAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\CostCenter\CostCenterEsIndexQueueService
     */
    private $indexQueueService;

    public function __construct(
        CostCenterRequestService $requestService,
        CostCenterAuthorizationService $authorizationService,
        AuditService $auditService,
        CostCenterEsIndexQueueService $indexQueueService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * @SWG\Get(
     *     path="/cost_center/{id}",
     *     summary="Get Cost Center",
     *     description="Get Cost Center Details

Authorization Scope : **view.cost_center**",
     *     tags={"cost_center"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Cost Center ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         examples={
     *            "application/json": {
     *                "id"=1,
     *                "name"="Lorem Ipsum",
     *                "company_id"=1,
     *                "account_id"=1,
     *                "description": "Lorem ipsum dolor sit amet"
     *            }
     *        }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Cost Center not found",
     *         examples={
     *             "application/json": {
     *                 "message"="Cost Center not found."
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Cost Center ID should not be empty."
     *             }
     *         }
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        // authorize
        $response = $this->requestService->get($id);
        $costCenterData = json_decode($response->getData());
        if (!$this->authorizationService->authorizeGet($costCenterData, $request->attributes->get('user'))) {
            $this->response()->errorUnauthorized();
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/cost_center/",
     *     summary="Create company cost center",
     *     description="Create company cost center

Authorization Scope : **create.cost_center**",
     *     tags={"cost_center"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Cost Center Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="Cost Center Description",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         examples={
     *             "application/json": {
     *                 "id": 1,
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message": "Error message here",
     *             }
     *         }
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $costCenterData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($costCenterData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        $this->indexQueueService->queue([$responseData['id']]);

        // audit log
        $costCenterGetResponse = $this->requestService->get($responseData['id']);
        $costCenterData = json_decode($costCenterGetResponse->getData(), true);
        $details = [
            'new' => $costCenterData,
        ];
        $item = new AuditCacheItem(
            CostCenterAuditService::class,
            CostCenterAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

     /**
     * @SWG\Put(
     *     path="/cost_center/{cost_center_id}",
     *     summary="Update Cost Center",
     *     description="Update Cost Center

Authorization Scope : **edit.cost_center**",
     *     tags={"cost_center"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="cost_center_id",
     *         in="path",
     *         description="Cost Center ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Cost Center Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         description="Cost Center Description",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *         examples={
     *             "application/json": {
     *                 "id"=1,
     *                 "name"="Lorem Ipsum",
     *                 "company_id"=1,
     *                 "account_id"=1,
     *                 "description": "Lorem ipsum dolor sit amet"
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Cost Center not found.",
     *         examples={
     *             "application/json": {
     *                 "message"="Cost Center not found."
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "message"="Cost Center ID should not be empty."
     *             }
     *         }
     *     )
     * )
     */
    public function update(Request $request, $costCenterId)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($costCenterId);

        $oldCostCenterData = json_decode($response->getData());

        $oldCostCenterData->account_id = Company::getAccountId($inputs['company_id']);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $oldCostCenterData->company_id
            ) && $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldCostCenterData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($costCenterId, $inputs);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $oldCostCenterData = json_decode($response->getData(), true);
        $newCostCenterData = json_decode($updateResponse->getData(), true);
        $details = [
            'old' => $oldCostCenterData,
            'new' => $newCostCenterData,
        ];
        $item = new AuditCacheItem(
            CostCenterAuditService::class,
            CostCenterAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/cost_center/bulk_delete",
     *     summary="Delete Multiple Cost Centers",
     *     description="Delete Multiple Cost Centers

Authorization Scope : **delete.cost_center**",
     *     tags={"cost_center"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="cost_center_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Cost Center IDs",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         examples={
     *             "application/json": {
     *                 "code"="ASSIGNED_TO_EMPLOYEES",
     *                 "title"="ASSIGNED_TO_EMPLOYEES",
     *                 "source"={
     *                     "pointer"="/cost_center/bulk_delete"
     *                 },
     *                 "details"={
     *                     1, 2, 3
     *                 }
     *             }
     *         }
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $costCenterData = (object) [
             'account_id' => Company::getAccountId($inputs['company_id']),
             'company_id' => $inputs['company_id']
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete($costCenterData, $deletedBy);
        }
        
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        foreach ($request->input('cost_center_ids') as $costCenterId) {
            $item = new AuditCacheItem(
                CostCenterAuditService::class,
                CostCenterAuditService::ACTION_DELETE,
                new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                [
                    'old' => [
                        'cost_center_id' => $costCenterId,
                        'company_id' => $request->get('company_id')
                    ]
                ]
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/cost_center/is_name_available",
     *     summary="Is cost center name available",
     *     description="Check availability of cost center name with the given company",
     *     tags={"cost_center"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Cost Center Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="cost_center_id",
     *         in="formData",
     *         description="Cost Center Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {

        $response = $this->requestService->isNameAvailable($id, $request->all());

        $user = $request->attributes->get('user');
        $costCenterData = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable($costCenterData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/cost_centers",
     *     summary="Get all Cost Centers",
     *     description="Get all Cost Center within company.

Authorization Scope : **view.cost_center**",
     *     tags={"cost_center"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyCostCenters(Request $request, $companyId)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $dataScope = $request->attributes->get('authz_data_scope');

        if ($authzEnabled && !$dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyCostCenters($companyId);

        $costCenterData = json_decode($response->getData())->data;

        if (empty($costCenterData)) {
            return $response;
        }

        if (!$authzEnabled) {
            $costCenter = $costCenterData[0];

            if (
                !$this->authorizationService->authorizeGetCompanyCostCenters(
                    $costCenter,
                    $request->attributes->get('user')
                )
            ) {
                $this->response()->errorUnauthorized();
            }
        }

        return $response;
    }
}
