<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User\UserRequestService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use App\Subscriptions\SubscriptionsRequestService;
use App\Subscriptions\SubscriptionInvoicesAuthorizationService;
use App\Traits\AuditTrailTrait;

class SubscriptionInvoicesController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionInvoicessAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;


    /**
     * SubscriptionsController constructor.
     */
    public function __construct(
        SubscriptionsRequestService $subscriptionsRequestService,
        SubscriptionInvoicesAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $subscriptionsRequestService;
        $this->userRequestService = $userRequestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/invoices",
     *     summary="Get list of invoices",
     *     description="Get list of invoices for the account id.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function search(Request $request)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->searchInvoices(
            [$user['account_id']]
        );
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/invoices/{id}",
     *     summary="Get invoice by ID",
     *     description="Get single invoice by ID.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function get(Request $request, int $id)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $invoiceResponse = $this->requestService->getInvoiceById($id);
        $invoice = json_decode($invoiceResponse->getData(), true);

        if ($user['account_id'] != $invoice['data']['account_id']) {
            $this->response()->errorUnauthorized();
        }

        return $invoice;
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/invoices/{id}/payments/generate_paynamics",
     *     summary="Get invoice paynamics payment details",
     *     description="Get invoice paynamics payment details.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function getPaynamicsPaymentDetails(Request $request, int $id)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                return $this->response()->errorUnauthorized();
            }
        }

        $invoiceResponse = $this->requestService->getInvoiceById($id);
        $invoice = json_decode($invoiceResponse->getData(), true);

        if ($user['account_id'] != $invoice['data']['account_id']) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getInvoicePaynamicsPaymentDetails($id);
    }

    /**
     * @SWG\POST(
     *     path="/subscriptions/invoices/{id}/payments/paypal/orders",
     *     summary="Create PayPal order for Invoice",
     *     description="Create PayPal order for Invoice.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function createPayPalOrder(Request $request, int $id)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $invoiceResponse = $this->requestService->getInvoiceById($id);
        $invoice = json_decode($invoiceResponse->getData(), true);

        if ($user['account_id'] != $invoice['data']['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->createPayPalOrder($id);

        try {
            $companyId = isset($user['employee_company_id']) ?
                $user['employee_company_id'] :
                $userData->companies[0]->id
            ;
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * @SWG\POST(
     *     path="/subscriptions/invoices/{id}/payments/paypal/capture/{orderId}",
     *     summary="Capture PayPal order for Invoice",
     *     description="Capture PayPal order for Invoice.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function capturePayPalOrder(Request $request, int $id, $orderId)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $invoiceResponse = $this->requestService->getInvoiceById($id);
        $invoice = json_decode($invoiceResponse->getData(), true);

        if ($user['account_id'] != $invoice['data']['account_id']) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->capturePayPalOrder($id, $orderId);

        try {
            $companyId = isset($user['employee_company_id']) ?
                $user['employee_company_id'] :
                $userData->companies[0]->id
            ;
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }


    /**
     * @SWG\GET(
     *     path="/subscriptions/invoices/{id}/billed_users",
     *     summary="Get billed users of an invoice",
     *     description="Get list of billed users under an invoice.
Authorization Scope : **view.subscription_invoices**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function getBilledUsers(Request $request, $id)
    {
        $billedUsersList = Cache::remember(
            $this->getRequestCachingKey($request),
            Carbon::now()->addMonth(),
            function () use ($request, $id) {
                // Build list
                $billedUsersResponse = $this->requestService->getBilledUsers($id);
                $billedUsers = json_decode($billedUsersResponse->getData(), true);

                $billedUsersList = [];
                if (!empty($billedUsers['data']['billed_users'])) {
                    $billedUsersList = collect($billedUsers['data']['billed_users'])
                        ->keyBy('user_id')
                        ->toArray();

                    // get list of users from cp-api
                    $requestService = App::make(UserRequestService::class);
                    $requestor      = $request->attributes->get('user');
                    $usersResponse  = $requestService->getAccountUsersByAttribute(
                        $requestor['account_id'],
                        'id',
                        ['values' => implode(',', array_keys($billedUsersList))]
                    );

                    $usersData = json_decode($usersResponse->getData(), true);

                    foreach ($usersData as $userData) {
                        if (!empty($billedUsersList[$userData['id']])) {
                            $billedUsersList[$userData['id']] = array_merge(
                                $billedUsersList[$userData['id']],
                                $userData
                            );

                            unset($billedUsersList[$userData['id']]['user_id']);
                        }
                    }
                }

                return $billedUsersList;
            }
        );

        return $this->response()->array([
            'data' => [
                'billed_users' => array_values($billedUsersList)
            ]
        ]);
    }
}
