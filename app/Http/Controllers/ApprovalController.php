<?php

namespace App\Http\Controllers;


use App\Audit\AuditService;
use App\ESS\EssApprovalRequestService;
use App\Approval\ApprovalRequestService;
use Illuminate\Http\Request;
use App\Approval\ApprovalAuthorizationService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\Facades\Company;
use App\Authz\AuthzDataScope;
use App\User\UserService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ApprovalController extends Controller
{
    /**
     * @var \App\ESS\EssApprovalRequestService
     */
    protected $requestService;

    /**
     * @var \App\Approval\ApprovalAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        EssApprovalRequestService $requestService,
        ApprovalAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/user/approvals",
     *     summary="Get All User Approvals",
     *     description="Get All User approvals.
    Authorization Scope : **view.request**",
     *     tags={"approval"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/UserApprovalParams"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="UserApprovalParams",
     *     required={"page"},
     *     @SWG\Property(
     *         property="page",
     *         type="integer",
     *         description="Page number. It should be >= 1 . Default is 1."
     *     ),
     *     @SWG\Property(
     *         property="per_page",
     *         description="Items to display per one page. F.e. 15. Default: 5",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="sort_by",
     *         description="Attribute to sort by.
  Possible values: employee_name, request_type_name, duration, request_status",
     *         type="string",
     *         enum={"employee_name","request_type_name","duration","request_status"}
     *     ),
     *     @SWG\Property(
     *         property="sort_order",
     *         description="Sort order: asc, desc.",
     *         type="string",
     *         enum={"asc","desc"}
     *     ),
     *     @SWG\Property(
     *         property="types",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         description="List of valid request types.
 Possible values are: leaves, loans, overtime, shift_change, time_dispute, undertime."
     *     ),
     *     @SWG\Property(
     *         property="my_level",
     *         type="boolean",
     *         description="Show requests on exact approver level
 or if it's false, exact level and levels below."
     *     ),
     *     @SWG\Property(
     *         property="statuses",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         description="List of valid request statuses.
 Possible values are: pending, approved, declined, cancelled, rejected."
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         description="A inclusive lower date boundary for filtering YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         description="A inclusive upper date boundary for filtering YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="term",
     *         type="string",
     *         description="Search term for filtering by employee, request type, date or request status."
     *     ),
     *     @SWG\Property(
     *         property="company_id",
     *         description="Company Id",
     *         type="integer"
     *     )
     * )
     */
    public function getUserApprovals(
        Request $request,
        EmployeeRequestAuthorizationService $requestAuthorizationService,
        ApprovalRequestService $approvalRequestService
    ) {
        $attributes = $request->all();
        $attributes['account_id'] = $request->attributes->get('user')['account_id'];
        $user = $request->attributes->get('user');

        // authorize
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = true;

        if (!$authzEnabled) {
            $authorizationData = (object)[
                'company_id' => $attributes['company_id'],
                'account_id' => !empty($attributes['company_id'])
                    ? Company::getAccountId($attributes['company_id'])
                    : null
            ];
            $isAuthorized = $requestAuthorizationService->authorizeView($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        if (empty($attributes['company_id'])) {
            $attributes['company_id'] = $user['employee_company_id'];
        }
        $attributes['account_id'] = $user['account_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        if (!empty($attributes['employee_relations']['company_ids'])) {
            $attributes['company_ids'] = $attributes['employee_relations']['company_ids'];
            unset($attributes['employee_relations']['company_ids']);
        }

        return $approvalRequestService->getUserApprovals($user['user_id'], $attributes);
    }

    /**
     * @SWG\Post(
     *     path="/request/bulk_decline",
     *     summary="Decline multiple requests",
     *     description="Decline requests with given id's.
    Authorization Scope : **approval.request**",
     *     tags={"approval"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests_ids[]",
     *         in="formData",
     *         description="Employee Requests Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDecline(
        Request $request
    ) {
        $user = $request->attributes->get('user');
        $attributes = $request->all();

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                intval($attributes['company_id'])
            );
        } else {
            $authorizationData = (object)[
                'company_id' => $attributes['company_id'],
                'account_id' => $user['account_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeApprove($authorizationData, $user);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        $attributes['company_id'] = $attributes['company_id'];
        $attributes['account_id'] = $user['account_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        $this->requestService->bulkDecline($attributes);

        return $this->response->noContent();
    }

    /**
     * @SWG\Post(
     *     path="/request/bulk_approve",
     *     summary="Approve multiple requests",
     *     description="Approve requests with given id's.
    Authorization Scope : **approval.request**",
     *     tags={"approval"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests_ids[]",
     *         in="formData",
     *         description="Employee Requests Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkApprove(
        Request $request
    ) {
        $attributes = $request->all();
        $user = $request->attributes->get('user');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                intval($attributes['company_id'])
            );
        } else {
            $authorizationData = (object)[
                'company_id' => $attributes['company_id'],
                'account_id' => $user['account_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeApprove($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        $attributes['company_id'] = $attributes['company_id'];
        $attributes['account_id'] = $user['account_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        $this->requestService->bulkApprove($attributes);

        return $this->response->noContent();
    }

    /**
     * @SWG\Post(
     *     path="/request/bulk_cancel",
     *     summary="Approve multiple requests",
     *     description="Approve requests with given id's.
    Authorization Scope : **approval.request**",
     *     tags={"approval"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employee_requests_ids[]",
     *         in="formData",
     *         description="Employee Requests Ids",
     *         required=true,
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkCancel(Request $request)
    {
        $attributes = $request->all();
        $user = $request->attributes->get('user');

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                intval($attributes['company_id'])
            );
        } else {
            $authorizationData = (object)[
                'company_id' => $attributes['company_id'],
                'account_id' => $user['account_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeApprove($authorizationData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $attributes['approver_user_id'] = $user['user_id'];
        $attributes['approver_employee_id'] = $user['employee_id'];
        $attributes['account_id'] = $user['account_id'];
        $attributes['company_id'] = $attributes['company_id'];

        $userService = app()->make(UserService::class);
        $attributes['employee_relations'] = $userService->getEmployeeRelationsData(
            $attributes['account_id'],
            $attributes['approver_user_id'],
            $attributes['approver_employee_id']
        );

        if (!empty($attributes['employee_relations']['company_ids'])) {
            $attributes['company_ids'] = $attributes['employee_relations']['company_ids'];
            unset($attributes['employee_relations']['company_ids']);
        }

        $this->requestService->bulkCancel($attributes);

        return $this->response->noContent();
    }
}
