<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Employee\EmployeeAuthz;
use App\Facades\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;
use App\TimeRecord\TimeRecordService;
use App\Employee\EmployeeRequestService;
use App\TimeRecord\TimeRecordAuthorizationService;
use App\Validator\TimeRecordBulkCreateOrDeleteValidator;
use App\TimeRecord\TimeRecordAuditService;
use App\Attendance\TimeRecordAttendanceTriggerService;
use App\Shift\ShiftRequestService;
use Carbon\Carbon;
use App\HoursWorked\HoursWorkedRequestService;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */

class TimeRecordController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\TimeRecord\TimeRecordService
     */
    private $requestService;

    /**
     * @var \App\TimeRecord\TimeRecordAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    private $employeeRequestService;

    /**
     * @var App\Attendance\TimeRecordAttendanceTriggerService
     */
    private $timeRecordTriggerService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        TimeRecordService $requestService,
        TimeRecordAuthorizationService $authorizationService,
        EmployeeRequestService $employeeRequestService,
        TimeRecordAttendanceTriggerService $timeRecordTriggerService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->employeeRequestService = $employeeRequestService;
        $this->timeRecordTriggerService = $timeRecordTriggerService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/employee/{id}/timesheet",
     *     summary="Get Employee's timesheet",
     *     description="Get the timesheet (all clock-ins and -outs) of an employee.
    Authorization Scope : **view.time_records**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getTimesheetForEmployee(Request $request, $id)
    {
        $employeeResponse = $this->employeeRequestService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());

        $timeRecordData = (object) [
            'account_id' => Company::getAccountId($employee->company_id),
            'company_id' => $employee->company_id
        ];

        // authorize
        if (!$this->authorizationService->authorizeViewTimeRecords(
            $timeRecordData,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getTimesheetForEmployee($id);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/time_records/bulk_create_or_delete",
     *     summary="Create or Delete Employee's Time Records Within Company",
     *     description="Create or Delete Employee's Time Records Within Company
Authorization Scope : **edit.time_records**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/time_records"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *         ref="$/responses/UnauthorizedResponse"
     *     ),
     * ),
     *
     * @SWG\Definition(
     *     definition="time_records",
     *     @SWG\Property(property="create", type="array", @SWG\Items(ref="#/definitions/createTimeRecord")),
     *     @SWG\Property(property="delete", type="array", @SWG\Items(ref="#/definitions/deleteTimeRecord")),
     * ),
     *
     * @SWG\Definition(
     *     definition="createTimeRecord",
     *     required={"employee_id", "type", "tags", "timestamp"},
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer",
     *         default="1"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="clock_in"
     *     ),
     *     @SWG\Property(
     *         property="tags",
     *         type="array",
     *         @SWG\Items(type="string")
     *     ),
     *     @SWG\Property(
     *         property="timestamp",
     *         type="integer",
     *         default=1529488800
     *     )
     * ),
     * @SWG\Definition(
     *     definition="deleteTimeRecord",
     *     required={"employee_id", "timestamp"},
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer",
     *         default="1"
     *     ),
     *     @SWG\Property(
     *         property="timestamp",
     *         type="integer",
     *         default=1529488800
     *     )
     * )
     */
    public function bulkCreateOrDelete(
        Request $request,
        TimeRecordBulkCreateOrDeleteValidator $validator,
        EmployeeAuthz $employeeAuthz,
        $id
    ) {
        $user = $request->attributes->get('user');
        $inputs = $request->all();
        $inputs['company_id'] = $id;
        $errors = $validator->validate($inputs);
        if ($errors) {
            $this->invalidRequestError(current($errors));
        }

        $authorized = false;
        if ($this->isAuthzEnabled($request)) {
            $createData = Arr::pluck($request->get('create', []), 'employee_id');
            $deleteData = Arr::pluck($request->get('delete', []), 'employee_id');
            $employeeIds = collect(array_merge($createData, $deleteData))->unique()->values()->all();
            $authorized = $employeeAuthz->isAllAuthorizedByIdAndCompanyId(
                $id,
                $employeeIds,
                $this->getAuthzDataScope($request)
            );
        } else {
            $timeRecordData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $authorized = $this->authorizationService->authorizeBulkCreateOrDelete($timeRecordData, $user);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreateOrDelete($inputs, $id);

        try {
            $this->audit($request, $id, ['Mass Update Attendance Time Records Trigger' => $inputs]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        $jobIds = $this->timeRecordTriggerService
            ->calculateAttendanceRecords($inputs, $response, $id);

        return response()->json($jobIds);
    }
}
