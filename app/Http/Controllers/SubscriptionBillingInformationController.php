<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\Subscriptions\SubscriptionsRequestService;
use App\Subscriptions\SubscriptionsAuthorizationService;
use App\Traits\AuditTrailTrait;

class SubscriptionBillingInformationController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionsAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;


    /**
     * SubscriptionsController constructor.
     */
    public function __construct(
        SubscriptionsRequestService $subscriptionsRequestService,
        SubscriptionsAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $subscriptionsRequestService;
        $this->userRequestService = $userRequestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/billing_information",
     *     summary="Get billing information",
     *     description="Get billing information for the current account.
Authorization Scope : **view.subscriptions**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function get(Request $request)
    {
        $user = $request->attributes->get('user');

        if (!$this->isAuthzEnabled($request)) {
            $userResponse = $this->userRequestService->get($user['user_id']);
            $userData = json_decode($userResponse->getData());

            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $this->requestService->getBillingInformation($user['account_id']);
    }

    /**
     * @SWG\PATCH(
     *     path="/subscriptions/billing_information",
     *     summary="Update billing information",
     *     description="Update billing information for the current account.
Authorization Scope : **view.subscriptions**",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     */
    public function update(Request $request)
    {
        $user = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($user['user_id']);
        $userData = json_decode($userResponse->getData());
        $companyId = $userData->companies[0]->id;

        if (!$this->isAuthzEnabled($request)) {
            $authData = (object) [
                'account_id' => $userData->account_id,
                'company_id' => $userData->companies[0]->id,
            ];

            $isAuthorized = $this->authorizationService->authorizeGet(
                $authData,
                $user
            );

            if (!$isAuthorized) {
                $this->response()->errorUnauthorized();
            }
        }

        $companyId = isset($user['employee_company_id']) ? $user['employee_company_id'] : 0;

        // For audit trail
        try {
            $oldDataResponse = $this->requestService->getBillingInformation($user['account_id']);
            $oldData = json_decode($oldDataResponse->getData(), true);
        } catch (\Throwable $e) {
            $oldData = [];
        }

        $data = $request->all();

        $response = $this->requestService->setBillingInformation($user['account_id'], $data);

        if ($response->isSuccessful()) {
            try {
                // trigger audit trail
                $newData = json_decode($response->getData(), true);
                $this->audit($request, $companyId, $newData, $oldData, true);
            } catch (\Throwable $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());
            }
        }

        return $response;
    }
}
