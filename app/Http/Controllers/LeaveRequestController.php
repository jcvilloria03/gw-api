<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveRequest\LeaveRequestRequestService;
use App\LeaveRequest\LeaveRequestAuthorizationService;
use App\LeaveRequest\LeaveRequestAuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use Symfony\Component\HttpFoundation\Response;
use App\CSV\CsvValidator;
use App\CSV\CsvValidatorException;
use App\LeaveRequest\LeaveRequestUploadTask;
use Aws\S3\Exception\S3Exception;
use Illuminate\Support\Facades\App;
use Bschmitt\Amqp\Facades\Amqp;
use App\LeaveRequest\LeaveRequestUploadTaskException;
use App\Employee\EmployeeRequestService;
use App\Schedule\ScheduleRequestService;
use App\DefaultSchedule\DefaultScheduleRequestService;
use Illuminate\Support\Arr;
use App\LeaveRequest\LeaveRequestDownloadTask;
use App\Traits\AuditTrailTrait;
use App\User\UserService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class LeaveRequestController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Csv\CsvValidator
     */
    protected $csvValidator;

    /**
     * @var \App\LeaveRequest\LeaveRequestRequestService
     */
    protected $requestService;

    /**
     * @var  \App\LeaveRequest\LeaveRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Schedule\ScheduleRequestService
     */
    protected $scheduleService;

    /**
     * @var \App\DefaultSchedule\DefaultScheduleRequestService
     */
    protected $defaultScheduleService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        CsvValidator $csvValidator,
        LeaveRequestRequestService $requestService,
        LeaveRequestAuthorizationService $authorizationService,
        ScheduleRequestService $scheduleService,
        DefaultScheduleRequestService $defaultScheduleService,
        AuditService $auditService
    ) {
        $this->csvValidator = $csvValidator;
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->scheduleService = $scheduleService;
        $this->defaultScheduleService = $defaultScheduleService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/company/{company_id}/leave_requests",
     *     summary="Get Company Leave Requests",
     *     description="Get Company Leave Requests
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID), schedule or leave type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1 . Default is 1.",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results to return per page. Default is 10.",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="formData",
     *         description="Attribute to sort by.
 Possible values: employee_name, leave_type_name, start_date, end_date, total_value, status, shift_name.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="formData",
     *         description="Sort order: asc, desc.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="locations_ids[]",
     *         in="formData",
     *         description="Filters locations.",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="departments_ids[]",
     *         in="formData",
     *         description="Filters departments.",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="positions_ids[]",
     *         in="formData",
     *         description="Filters positions.",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Filters employees.",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_types_ids[]",
     *         in="formData",
     *         description="Filters leave types.",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="year_month",
     *         in="formData",
     *         description="Search by year and month with format 'Y-m'",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="dates[]",
     *         in="formData",
     *         description="Dates with 'Y-m-d' format",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLeaveRequests(Request $request, $id)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $leaveRequestData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveRequestData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $queryString = $request->getQueryString() ?? '';

        return $this->requestService->getCompanyLeaveRequests($id, $request->all(), $queryString, $authzDataScope);
    }

    /**
     * @SWG\POST(
     *     path="/{id}/leave_requests/download/status",
     *     summary="Get Leave Requests Download Status",
     *     description="Get Leave Requests Download Status
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="x-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */

    public function getLeaveRequestsDownloadStatus(Request $request, $id)
    {
        $attributes = $request->only(['job_id']);
        $jobId = Arr::get($attributes, 'job_id', null);
        $downloadTask = App::make(LeaveRequestDownloadTask::class);
        $cache = $downloadTask->getRedisKey($id, $jobId);
        return $cache;
    }

    /**
     * @SWG\Get(
     *     path="/leave_request/{id}",
     *     summary="Get Leave Request",
     *     description="Get Leave Request
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     produces={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Leave Request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        $user = $request->attributes->get('user');
        $response = $this->requestService->get($id, true);
        $leaveRequestData = json_decode($response->getData());
        $leaveRequestData->company_id = $leaveRequestData->leave_type->company_id;
        $leaveRequestData->account_id = $user['account_id'];
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($leaveRequestData->employee_id);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [Arr::get($employee, 'company_id'), $leaveRequestData->company_id],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveRequestData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_request/admin",
     *     summary="Create Leave Request as Administrator",
     *     description="Create Leave Request as Admin
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newLeaveRequestAdmin"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="newLeaveRequestAdmin",
     *     required={"company_id", "employee_id", "status",
     *     "leave_type_id", "start_date", "end_date", "leaves", "unit"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="status",
     *         type="string",
     *         enum={"Approved", "Declined", "Pending", "Cancelled"},
     *         description="Leave request status. Possible values: Pending, Approved, Declined, Cancelled."
     *     ),
     *     @SWG\Property(
     *         property="leave_type_id",
     *         type="integer",
     *         description="Leave Type ID"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Leave request start date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         default="2018-12-29",
     *         description="Leave request end date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="unit",
     *         type="string",
     *         description="Leave Unit days | hours.",
     *         enum={"days", "hours"}
     *     ),
     *     @SWG\Property(
     *         property="leaves",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/leave_request_leave")
     *     )
     * )
     */
    public function create(Request $request, EmployeeRequestService $employeeRequestService)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($inputs['employee_id']);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveRequestData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['user_id'] = $createdBy['user_id'];

        $inputs['leaves'] = $this->appendSchedules($inputs['company_id'], $inputs['leaves']);

        $response = $this->requestService->createByAdmin($inputs);
        $responseData = json_decode($response->getData(), true);

        $this->audit($request, $inputs['company_id'], array_merge($responseData, $inputs));

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_request/upload",
     *     summary="Upload Filed Leaves",
     *     description="Uploads Filed Leaves
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "leave_request_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $leaveRequestData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');
        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        try {
            $this->csvValidator->validate($file);
        } catch (CsvValidatorException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        try {
            $uploadTask = App::make(LeaveRequestUploadTask::class);
            $uploadTask->create($companyId);
            $s3Key = $uploadTask->saveFile($file->getPathName());

            $details = [
                'id' => $uploadTask->getId(),
                'company_id' => $companyId,
                'task' => LeaveRequestUploadTask::PROCESS_VALIDATION,
                's3_bucket' => $uploadTask->getS3Bucket(),
                's3_key' => $s3Key,
                'authz_data_scope' => !empty($authzDataScope) ? $authzDataScope->getDataScope() : []
            ];

            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );

            Amqp::publish(config('queues.leave_request_validation_queue'), $message);

            return response()->json([
                'id' => $uploadTask->getId()
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * @SWG\Get(
     *     path="/leave_request/upload/status",
     *     summary="Get Job Status",
     *     description="Get Filed Leaves Upload Status
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="step",
     *         in="query",
     *         description="Job Step",
     *         required=true,
     *         type="string",
     *         enum={
     *              App\LeaveRequest\LeaveRequestUploadTask::PROCESS_VALIDATION,
     *              App\LeaveRequest\LeaveRequestUploadTask::PROCESS_SAVE
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Name is invalid"
     *                          },
     *                          "4": {
     *                              "Email is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $leaveRequestData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveRequestUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (LeaveRequestUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // check invalid step
        $process = $request->input('step');
        if (!in_array($process, LeaveRequestUploadTask::VALID_PROCESSES)) {
            $this->invalidRequestError(
                'Invalid Upload Step. Must be one of ' . array_explode(',', LeaveRequestUploadTask::VALID_PROCESSES)
            );
        }

        $fields = [
            $process . '_status',
            $process . '_error_file_s3_key'
        ];
        $errors = null;
        $details = array_combine($fields, $uploadTask->fetch($fields));

        if (
            $details[$process . '_status'] === LeaveRequestUploadTask::STATUS_VALIDATION_FAILED ||
            $details[$process . '_status'] === LeaveRequestUploadTask::STATUS_SAVE_FAILED
        ) {
            $errors = $uploadTask->fetchErrorFileFromS3($details[$process . '_error_file_s3_key']);
        }

        return response()->json([
            'status' => $details[$process . '_status'],
            'errors' => $errors ?? null
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/leave_request/upload/preview",
     *     summary="Get Filed Leaves Preview",
     *     description="Get Filed Leaves Upload Preview
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $leaveRequestData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveRequestUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (LeaveRequestUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        return $this->requestService->getUploadPreview($jobId);
    }

    /**
     * @SWG\Post(
     *     path="/leave_request/upload/save",
     *     summary="Save uploaded filed leaves",
     *     description="Saves Filed Leaves from Previously Uploaded CSV
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "leave_request_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Filed leave requests needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
            ]);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $leaveRequestData = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = App::make(LeaveRequestUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(LeaveRequestUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (LeaveRequestUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            'user_id' => $uploadTask->getUserId(),
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.leave_request_write_queue'), $message);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/leave_requests/download",
     *     summary="Download Leave Requests CSV",
     *     description="Download generated CSV with leave requests
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         type="boolean",
     *         in="formData",
     *         description="Download all requests",
     *         @SWG\Items(type="boolean")
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID), schedule or leave type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_requests_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Requests Ids",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *    @SWG\Parameter(
     *         name="locations_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="departments_ids[]",
     *         in="formData",
     *         description="Filters departments",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="positions_ids[]",
     *         in="formData",
     *         description="Filters locations",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="formData",
     *         description="Filters employees",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_types_ids[]",
     *         in="formData",
     *         description="Filters leave types",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function download(int $id, Request $request)
    {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $leaveRequest = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($leaveRequest, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // Create status cache Check job exists (will throw exception if job doesn't exist)
        try {
            $downloadTask = App::make(LeaveRequestDownloadTask::class);
            $downloadTask->create($id);
            $jobId = $downloadTask->getId();
        } catch (LeaveRequestUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        // Drop to queue
        $details =             [
            'id'        => $jobId,
            'task'      => 'download',
            'authz_data_scope' => $this->getAuthzDataScope($request)->getDataScope(),
            'request'   => array_merge(['id'=>$id], $request->all())
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.leave_requests_download_queue'), $message);

        return ['data'=>['job_id' => $jobId]];
    }

    /**
     * @SWG\Post(
     *     path="/employee/{id}/leave_requests/download",
     *     summary="Download Employee Leave Requests CSV",
     *     description="Download generated CSV with leave requests
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"text/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Employee ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID), schedule or leave type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="download_all",
     *         type="boolean",
     *         in="formData",
     *         description="Download all requests",
     *         @SWG\Items(type="boolean")
     *     ),
     *     @SWG\Parameter(
     *         name="leave_requests_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Requests Ids",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(type="file")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function downloadEmployeeLeaveRequests(int $id, Request $request, EmployeeRequestService $employeeService)
    {
        $employeeResponse = $employeeService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employeeData = json_decode($employeeResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveRequestData = (object) [
                'company_id' => $employee->company_id,
                'account_id' => Company::getAccountId($employee->company_id)
            ];
            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveRequestData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->downloadEmployeeLeaveRequestsCsv($id, $request->all());

        try {
            $employeeData = json_decode($employeeResponse->getData(), true);
            $companyId = Arr::get($employeeData, 'company_id');
            $data = str_getcsv($response->getContent());
            $this->audit($request, $companyId, [], ['Download Employee Leave Requests' => $data]);
        } catch (\Exception $e) {
            throw $e;
            // Do Nothing
        }

        return $response;
    }

    /**
     * @SWG\Delete(
     *     path="/leave_request/bulk_delete",
     *     summary="Delete Leave Requests",
     *     description="Delete Leave Requests,
     Authorization Scope : **delete.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_requests_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Requests Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $inputs['company_id']
            ]);
        } else {
            $leaveRequestData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeDelete($leaveRequestData, $deletedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
        // call microservice
        $response = $this->requestService->bulkDelete($inputs, $authzDataScope);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('leave_requests', []) as $leaveRequest) {
                $old = [
                    'id' => $leaveRequest->id,
                    'company_id' => $request->get('company_id')
                ];
                $this->audit($request, $inputs['company_id'], [], $old);
            }
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/leave_request/{id}/admin",
     *     summary="Update Leave Request as Administrator",
     *     description="Update Leave Request as Admin
Authorization Scope : **edit.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/updateLeaveRequestAsAdmin"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="updateLeaveRequestAsAdmin",
     *     required={"company_id", "status",
     *     "leave_type_id", "start_date", "end_date", "leaves", "unit"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="status",
     *         type="string",
     *         enum={"Approved", "Declined", "Pending", "Cancelled"},
     *         description="Leave request status. Possible values: Pending, Approved, Declined, Cancelled."
     *     ),
     *     @SWG\Property(
     *         property="leave_type_id",
     *         type="integer",
     *         description="Leave Type ID"
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Leave request start date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         default="2018-12-29",
     *         description="Leave request end date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="unit",
     *         type="string",
     *         description="Leave Unit days | hours",
     *         enum={"days", "hours"}
     *     ),
     *     @SWG\Property(
     *         property="leaves",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/leave_request_leave")
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $updatedBy = $request->attributes->get('user');
        $inputs = $request->all();

        $inputs['user_id'] = $updatedBy['user_id'];
        $inputs['leaves']  = $this->appendSchedules($inputs['company_id'], $inputs['leaves']);

        $oldDataResponse = $this->requestService->get($id, true);

        $oldData = json_decode($oldDataResponse->getData(), true);

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employee = $oldData['employee_detail'];
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [$oldData['company_id'], Arr::get($employee, 'company_id')],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveRequestData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeUpdate($leaveRequestData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $updateResponse = $this->requestService->update($id, $inputs);

        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);

        $newData = json_decode($getResponse->getData(), true);
        $this->audit($request, $inputs['company_id'], $newData, $oldData);

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/employee/{employee_id}/leave_requests",
     *     summary="Get Employee Leave Requests",
     *     description="Get Employee Leave Requests
Authorization Scope : **view.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term for filtering by
 employee (first name, last name, employee UID), schedule or leave type.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1 . Default is 1.",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="per_page",
     *         in="query",
     *         description="Number of results to return per page. Default is 10.",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_by",
     *         in="formData",
     *         description="Attribute to sort by.
 Possible values: leave_type_name, start_date, end_date, status, shift_name.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_order",
     *         in="formData",
     *         description="Sort order: asc, desc.",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeLeaveRequests(Request $request, $id, EmployeeRequestService $employeeService)
    {
        $queryString = $request->getQueryString() ?? '';
        $response = $this->requestService->getEmployeeLeaveRequests($id, $request->all(), $queryString);
        $employeeResponse = $employeeService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $employeeData = json_decode($employeeResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveRequestData = (object) [
                'company_id' => $employee->company_id,
                'account_id' => Company::getAccountId($employee->company_id)
            ];
            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveRequestData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_request/admin/calculate_leaves_total_value",
     *     summary="Calculate leave hours or days for Leave Request as Administrator",
     *     description="Calculate leave hours or days for Leave Request as Admin
Authorization Scope : **create.leave_request**",
     *     tags={"leave_request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/leaves_payload_for_calculation"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="leaves_payload_for_calculation",
     *     required={"company_id", "employee_id", "leaves", "unit"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="unit",
     *         type="string",
     *         description="Leave Unit days | hours",
     *         enum={"days", "hours"}
     *     ),
     *     @SWG\Property(
     *         property="leaves",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/leave_request_leave_for_calculation")
     *     )
     * ),
     * @SWG\Definition(
     *     definition="leave_request_leave_for_calculation",
     *     required={"date", "schedule_id", "start_time", "end_time"},
     *     @SWG\Property(
     *         property="schedule_id",
     *         type="integer",
     *         description="Schedule ID. Null for Default Schedule."
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-01-29",
     *         description="Leave on date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="start_time",
     *         type="string",
     *         default="09:00",
     *         description="Leave start time in format HH:mm."
     *     ),
     *     @SWG\Property(
     *         property="end_time",
     *         type="string",
     *         default="18:00",
     *         description="Leave end time in format HH:mm."
     *     )
     * )
     */
    public function calculateLeavesTotalValue(Request $request, EmployeeRequestService $employeeRequestService)
    {
        $inputs = $request->all();
        $this->validate($request, [
            'company_id' => 'required|integer',
            'employee_id' => 'required|integer',
            'leaves' => 'required|array|min:1',
            'leaves.*.schedule_id' => 'nullable|integer'
        ]);
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($inputs['employee_id']);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [$inputs['company_id'], Arr::get($employee, 'company_id')],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $createdBy = $request->attributes->get('user');
            $leaveRequestData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveRequestData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs['leaves'] = $this->appendSchedules($inputs['company_id'], $inputs['leaves']);

        return $this->requestService->calculateLeavesTotalValue($inputs);
    }

    /**
     * Append schedules to leaves data
     *
     * @param  int    $companyId Company ID
     * @param  array  $leaves    List of leaves data
     * @return array             List of leaves data with appended schedules
     */
    protected function appendSchedules(int $companyId, array $leaves)
    {
        $scheduleIds = array_column($leaves, 'schedule_id');
        $hasDefaultSchedule = in_array(null, $scheduleIds, true) || count($scheduleIds) === 0;
        $defaultSchedules = collect();

        // Get schedules per ID
        $schedules = collect(
            $this->scheduleService->getMultiple($companyId, $scheduleIds)
        )->keyBy('id');

        // Get default schedules when leaves includes a shift with default schedule
        if ($hasDefaultSchedule) {
            $defaultSchedulesRequest = $this->defaultScheduleService->index($companyId);

            $defaultSchedules = collect(
                json_decode($defaultSchedulesRequest->getData(), true)['data']
            )->keyBy('day_of_week');
        }

        return array_map(function ($leave) use ($schedules, $defaultSchedules) {
            $leave['schedule'] = $schedules->get($leave['schedule_id'] ?? null);

            if (empty($leave['schedule_id'])) {
                $dayOfWeek = (int) date('w', strtotime($leave['date']));

                $leave['schedule'] = $this->transformDefaultSchedule($defaultSchedules->get($dayOfWeek));
            }

            return $leave;
        }, $leaves);
    }

    /**
     * Transforms default schedule data to similar structure of the assigned schedule
     *
     * @param  array  $schedule Default schedule data
     * @return array
     */
    protected function transformDefaultSchedule(array $schedule)
    {
        return [
            'start_time' => $schedule['work_start'],
            'end_time' => $schedule['work_end'],
            'breaks' => [
                [
                    'type' => 'fixed',
                    'start' => $schedule['work_break_start'],
                    'end' => $schedule['work_break_end']
                ]
            ]
        ];
    }
}
