<?php

namespace App\Http\Controllers;

use App\Authz\AuthzService;
use Log;
use App\Model\Role;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Role\RoleAuditService;
use App\Role\DefaultRoleService;
use App\User\UserRequestService;
use App\Company\CompanyAuditService;
use App\Company\CompanyAuthorizationService;
use Symfony\Component\HttpFoundation\Response;
use App\Company\PhilippineCompanyRequestService;
use App\Company\CompanyRequestService;
use App\Holiday\HolidayRequestService;
use App\Subscriptions\SubscriptionsRequestService;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class PhilippineCompanyController extends Controller
{
    const ACTION_CREATE = 'create';

    const OBJECT_NAME = 'company';

    const METHOD_TYPES = [
        'PUT'
    ];

    const IN_USE_TYPES = [
        'department',
        'position',
        'rank',
        'employment_type',
        'location',
        'company'
    ];

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Company\PhilippineCompanyRequestService
     */
    protected $requestService;

    /**
     * @var \App\Company\CompanyAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Role\DefaultRoleService
     */
    private $defaultRoleService;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    private $subscriptionsRequestService;


    public function __construct(
        PhilippineCompanyRequestService $requestService,
        AuditService $auditService,
        CompanyAuthorizationService $authorizationService,
        DefaultRoleService $defaultRoleService,
        SubscriptionsRequestService $subscriptionsRequestService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
        $this->authorizationService = $authorizationService;
        $this->defaultRoleService = $defaultRoleService;
        $this->subscriptionsRequestService = $subscriptionsRequestService;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company/{id}",
     *     summary="Get Philippine Company",
     *     description="Get Philippine Company Details

Authorization Scope : **view.company**",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get($id, Request $request)
    {
        // authorize
        $response = $this->requestService->get($id);
        $companyData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyData->id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company",
     *     summary="Create Philippine Company",
     *     description="Create New Philippine Company

Authorization Scope : **create.company**",
     *     tags={"company"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Company Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="account_id",
     *         in="formData",
     *         description="Account Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="logo",
     *         description="Company Logo",
     *         in="formData",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Company Email",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="website",
     *         in="formData",
     *         description="Company Website",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="mobile_number",
     *         in="formData",
     *         description="Mobile Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="telephone_number",
     *         in="formData",
     *         description="Telephone Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="telephone_extension",
     *         in="formData",
     *         description="Telephone Extension",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="fax_number",
     *         in="formData",
     *         description="Fax Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Company Type",
     *         required=false,
     *         type="string",
     *         enum={"Private", "Government", "Non - Profit"}
     *     ),
     *     @SWG\Parameter(
     *         name="tin",
     *         in="formData",
     *         description="TIN",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="rdo",
     *         in="formData",
     *         description="RDO",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sss",
     *         in="formData",
     *         description="SSS Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf",
     *         in="formData",
     *         description="HDMF Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth",
     *         in="formData",
     *         description="PhilHealth Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="int"),
     *             @SWG\Property(property="logo", type="string"),
     *         ),
     *         examples={
     *             "application/json": {
     *                 "id": 1,
     *                 "logo": "https://v3-company-logos-local.s3-us-west-2.amazonaws.com/10_5caaf23d5b9e1?response-content-disposition=inline%3B%20filename%3D&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIM6V6OBDMAB3656A%2F20190408%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20190408T070326Z&X-Amz-SignedHeaders=host&X-Amz-Expires=10080&X-Amz-Signature=47a9aac3c7fd200272ca77f68d9dedf65cecd24c3ff4e6d1f4eb277d79ea4aca"
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $inputs['account_id'] = $createdBy['account_id'];
        $authzEnabled = $request->attributes->get('authz_enabled');
        if (
            !$authzEnabled &&
            !$this->authorizationService->authorizeCreate($createdBy['account_id'], $createdBy['user_id'])
        ) {
            $this->response()->errorUnauthorized();
        }

        // Call subscription api before creating
        $customerResponse     = $this->subscriptionsRequestService
            ->getCustomerByAccountId($inputs['account_id'], ['plan']);

        $customerData         = json_decode($customerResponse->getData(), true);
        $accountSubscription  = $customerData['data'][0]['subscriptions'][0] ?? null;

        $productCode = $accountSubscription['plan']['main_product']['code'];

        if (empty($accountSubscription) || empty($productCode)) {
            $this->invalidRequestError('Account does not have a subscription.');
        }

        // call microservice
        $response = $this->requestService->create($inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $responseData = json_decode($response->getBody(), true);

        // create system-defined roles
        $companyRequestService = app()->make(CompanyRequestService::class);
        $companyRequestService->createSystemDefinedRoles($inputs['account_id'], false);
        // create admin role for company
        $companyResponse = $this->requestService->get($responseData['id']);
        $companyData = json_decode($companyResponse->getData(), true);
        $companyAdminRole = $this->defaultRoleService->createAdminRole(
            $inputs['account_id'],
            $companyData,
            $this->buildModuleAccessFromSubscriptionProductCode($productCode)
        );

        // create employee role for company
        $companyEmployeeRole = $this->defaultRoleService->createCompanyEmployeeRole(
            $inputs['account_id'],
            $companyData,
            $this->buildModuleAccessFromSubscriptionProductCode($productCode)
        );

        // create default holidays
        $holidayRequestService = app()->make(HolidayRequestService::class);
        $holidayRequestService->createCompanyDefaultHolidays($responseData['id']);

        // audit log
        $details = [
            'new' => $companyData,
        ];
        $item = new AuditCacheItem(
            CompanyAuditService::class,
            CompanyAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        // log company admin role creation
        $details = [
            'new' => $companyAdminRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        // log company employee role creation
        $details = [
            'new' => $companyEmployeeRole->toArray(),
        ];
        $item = new AuditCacheItem(
            RoleAuditService::class,
            RoleAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Patch(
     *     deprecated=true,
     *     path="/philippine/company/details",
     *     summary="Update Philippine Company Details",
     *     description="Update Philippine Company Details
Authorization Scope : **edit.company**",
     *     tags={"company"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Company Type",
     *         required=false,
     *         type="string",
     *         enum={"Private", "Government", "Non - Profit"}
     *     ),
     *     @SWG\Parameter(
     *         name="tin",
     *         in="formData",
     *         description="TIN",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="rdo",
     *         in="formData",
     *         description="RDO",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sss",
     *         in="formData",
     *         description="SSS Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf",
     *         in="formData",
     *         description="HDMF Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth",
     *         in="formData",
     *         description="PhilHealth Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateDetails(Request $request)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($inputs['company_id']);
        $oldCompanyData = json_decode($response->getData());

        if (!$this->authorizationService->authorizeUpdate($oldCompanyData, $updatedBy)) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->updateDetails($inputs);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($inputs['company_id']);
        $oldCompanyData = json_decode($response->getData(), true);
        $newCompanyData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldCompanyData,
            'new' => $newCompanyData,
        ];
        $item = new AuditCacheItem(
            CompanyAuditService::class,
            CompanyAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company_types",
     *     summary="Get Philippine Company Types",
     *     description="Get Philippine Company Types",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function getTypes()
    {
        // call microservice
        return $this->requestService->getTypes();
    }

    /**
     * @SWG\POST(
     *     path="/company/{company_id}",
     *     summary="Update Company",
     *     description="Update Company Details
Authorization Scope : **edit.company**",
     *     tags={"company"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Company Name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="logo",
     *         description="Company Logo",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Company Email",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="website",
     *         in="formData",
     *         description="Company Website",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="mobile_number",
     *         in="formData",
     *         description="Mobile Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="telephone_number",
     *         in="formData",
     *         description="Telephone Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="telephone_extension",
     *         in="formData",
     *         description="Telephone Extension",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="fax_number",
     *         in="formData",
     *         description="Fax Number",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Company Type",
     *         required=false,
     *         type="string",
     *         enum={"Private", "Government", "Non - Profit"}
     *     ),
     *     @SWG\Parameter(
     *         name="tin",
     *         in="formData",
     *         description="TIN",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="rdo",
     *         in="formData",
     *         description="RDO",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sss",
     *         in="formData",
     *         description="SSS Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="hdmf",
     *         in="formData",
     *         description="HDMF Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="philhealth",
     *         in="formData",
     *         description="PhilHealth Number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="int"),
     *             @SWG\Property(property="name", type="string"),
     *             @SWG\Property(property="account_id", type="int"),
     *             @SWG\Property(property="logo", type="string"),
     *             @SWG\Property(property="email", type="string"),
     *             @SWG\Property(property="website", type="string"),
     *             @SWG\Property(property="mobile_number", type="string"),
     *             @SWG\Property(property="telephone_number", type="string"),
     *             @SWG\Property(property="telephone_extension", type="string"),
     *             @SWG\Property(property="fax_number", type="string"),
     *             @SWG\Property(property="type", type="string"),
     *             @SWG\Property(property="tin", type="string"),
     *             @SWG\Property(property="rdo", type="string"),
     *             @SWG\Property(property="sss", type="string"),
     *             @SWG\Property(property="hdmf", type="string"),
     *             @SWG\Property(property="philhealth", type="string"),
     *             @SWG\Property(property="address", type="string"),
     *         ),
     *         examples={
     *             "application/json": {
     *                 "id": 3243,
     *                 "name": "Foo Company",
     *                 "account_id": 1,
     *                 "logo": "https://v3-company-logos-local.s3-us-west-2.amazonaws.com/10_5caaf23d5b9e1?response-content-disposition=inline%3B%20filename%3D&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIM6V6OBDMAB3656A%2F20190408%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20190408T070326Z&X-Amz-SignedHeaders=host&X-Amz-Expires=10080&X-Amz-Signature=47a9aac3c7fd200272ca77f68d9dedf65cecd24c3ff4e6d1f4eb277d79ea4aca",
     *                 "email": "foocompany@salarium.test",
     *                 "website": "salarium.test",
     *                 "mobile_number": "1234567899",
     *                 "telephone_number": "1234567",
     *                 "telephone_extension": null,
     *                 "fax_number": null,
     *                 "type": "Private",
     *                 "tin": "234-567-890",
     *                 "rdo": "047",
     *                 "sss": "12-3456789-0",
     *                 "hdmf": "4567-8945-6789",
     *                 "philhealth": "78-901278901-2",
     *                 "address": null
     *             }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $companyId)
    {
        // authorize
        $inputs = $request->all();

        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($companyId);
        $oldCompanyData = json_decode($response->getData());
        $inputs['account_id'] = $updatedBy['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldCompanyData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //add subscription toggle if account is subscribed to Payroll or Both
        $subscriptions = $this->subscriptionsRequestService
                              ->getSubscriptionsBySingleUserId($updatedBy['user_id']);

        $payrollSubscribed = false;
        $licenses = @$subscriptions['data']['subscription_licenses'];

        if ($licenses != null && count($licenses > 0)) {
            foreach ($licenses as $license) {
                $code = $license['product']['code'];
                if (str_contains($code, 'payroll')) {
                    $payrollSubscribed = true;
                }
            }
        }
        //$inputs['payroll_subscribed'] = $payrollSubscribed;

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $companyId);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($companyId);
        $oldCompanyData = json_decode($response->getData(), true);
        $newCompanyData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldCompanyData,
            'new' => $newCompanyData,
        ];
        $item = new AuditCacheItem(
            CompanyAuditService::class,
            CompanyAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/company/{type}/is_in_use",
     *     summary="Check is something in use",
     *     description="Check is something in use",
     *     tags={"company"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="formData",
     *         description="ID's",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="Type",
     *         required=true,
     *         type="string",
     *         enum=\App\Http\Controllers\PhilippineCompanyController::IN_USE_TYPES,
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isInUse(string $type, Request $request)
    {
        $response = $this->requestService->get($request->get('company_id'));
        $companyData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyData->id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isInUse($type, $request->all());
    }

    /**
     * @SWG\Delete(
     *     path="/company/bulk_delete",
     *     summary="Delete multiple Companies",
     *     description="Delete multiple Companies for given id's.
Authorization Scope : **delete.company**",
     *     tags={"company"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Company Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $companies = [];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $request->input('company_ids')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        foreach ($request->input('company_ids') as $id) {
            $response = $this->requestService->get($id);
            $oldCompanyData = json_decode($response->getData());
            $companies[] = $oldCompanyData;

            if (!$authzEnabled) {
                // authorize
                if (!$this->authorizationService->authorizeDelete($oldCompanyData, $deletedBy)) {
                    $this->response()->errorUnauthorized();
                }
            }
        }

        $inUseResponse = $this->requestService->isInUse('company', ['ids' => $inputs['company_ids']]);
        $inUseData = json_decode($inUseResponse->getData());

        if ($inUseData->in_use > 0) {
            $this->invalidRequestError('Selected record\'s is currently in use and cannot be deleted.');
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        // audit log
        foreach ($request->input('company_ids') as $id) {
            $details = [
                'old' => [
                    'id' => $id
                ]
            ];
            $item = new AuditCacheItem(
                CompanyAuditService::class,
                CompanyAuditService::ACTION_DELETE,
                new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                $details
            );
            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * Convert subscription product code to list of module accesses
     * allowed for that product
     *
     * @param string $productCode
     * @return array
     */
    private function buildModuleAccessFromSubscriptionProductCode(string $productCode)
    {
        $mapping = [
            'time_and_attendance'              => [Role::ACCESS_MODULE_HRIS, Role::ACCESS_MODULE_TA],
            'payroll'                          => [Role::ACCESS_MODULE_HRIS, Role::ACCESS_MODULE_PAYROLL],
            'time_and_attendance_plus_payroll' => [
                Role::ACCESS_MODULE_HRIS,
                Role::ACCESS_MODULE_TA,
                Role::ACCESS_MODULE_PAYROLL
            ],
        ];

        $formattedProductName = str_ireplace(' ', '_', $productCode);

        return $mapping[$formattedProductName] ?? Role::DEFAULT_MODULE_ACCESSES;
    }

    /**
     * @SWG\Get(
     *     path="/philippine/company_demo/details/{id}",
     *     summary="Get Philippine Company Demo",
     *     description="Get Philippine Company Demo Details

Authorization Scope : **view.company**",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getDemoCompany($id, Request $request)
    {
        // authorize
        $response = $this->requestService->getCompanyDemo($id);
        $companyData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyData->company_id
            );
        } else {
            $isAuthorized = $this->authorizationService->authorizeGet($companyData, $request->attributes->get('user'));
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }
}
