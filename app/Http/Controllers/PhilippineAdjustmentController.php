<?php

namespace App\Http\Controllers;

use App\Adjustment\AdjustmentAuthorizationService;
use App\Adjustment\PhilippineAdjustmentRequestService;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class PhilippineAdjustmentController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Adjustment\PhilippineAdjustmentRequestService
     */
    protected $requestService;

    /**
     * @var \App\Adjustment\AdjustmentAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        PhilippineAdjustmentRequestService $requestService,
        AdjustmentAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/adjustment/bulk_create",
     *     summary="Assign Multiple Adjustments",
     *     description="Assign multiple adjustments,
Authorization Scope : **create.adjustments**",
     *     tags={"adjustment"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="adjustments",
     *         in="body",
     *         description="Create multiple adjustments",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/adjustmentsRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="adjustmentsRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/adjustmentsRequestItem"),
     *     collectionFormat="multiple"
     * ),
     * @SWG\Definition(
     *     definition="adjustmentsRequestItem",
     *     @SWG\Property(
     *         property="type_id",
     *         type="integer",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number",
     *         example="100"
     *     ),
     *     @SWG\Property(
     *         property="reason",
     *         type="string",
     *         example="You are a good employee"
     *     ),
     *     @SWG\Property(
     *         property="recipients",
     *         type="object",
     *             @SWG\Property(
     *              property="employees",
     *              type="array",
     *              items={"type"="integer"}
     *             )
     *     ),
     *     @SWG\Property(
     *         property="release_details",
     *         type="array",
     *         items={"type"="object",
     *                  "properties"={
     *                      "id"={"type"="integer", "example"=1},
     *                      "date"={"type"="string", "example"="2019-02-22"},
     *                      "disburse_through_special_pay_run"={"type"="boolean", "example"=false},
     *                      "status"={"type"="boolean", "example"=true}
     *                  }
     *              }
     *     ),
     * )
     */
    public function bulkCreate(
        Request $request,
        EmployeeRequestService $employeeService,
        $companyId
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;
        $errorMesssage = 'Unauthorized';

        if ($this->isAuthzEnabled($request)) {
            $authorizedCompany = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            $employeeIds = array_flatten($request->json('*.recipients.employees'), 1);

            $employeeResponse = $employeeService->getCompanyEmployeesById($companyId, $employeeIds);
            if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
                return $employeeResponse;
            }

            $unAuthorizedEmployees = collect(json_decode($employeeResponse->getData(), true))->reject(
                function ($employee) use ($request) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized([
                        AuthzDataScope::SCOPE_COMPANY => [data_get($employee, 'company_id')],
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employee, 'department_id'),
                        AuthzDataScope::SCOPE_POSITION => data_get($employee, 'position_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employee, 'location_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employee, 'team_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group_id'),
                    ]);
                }
            );

            if (!$unAuthorizedEmployees->isEmpty()) {
                $errorMesssage = 'Unauthorized: Unable to process employees:[' .
                    implode(',', $unAuthorizedEmployees->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = $authorizedCompany && $unAuthorizedEmployees->isEmpty();
            }
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMesssage);
        }

        // call microservice
        $response = $this->requestService->bulkCreate($companyId, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $adjustmentIds = json_decode($response->getData(), true);
        $adjustments = collect($inputs)->map(function ($adjustment, $index) use ($adjustmentIds) {
            array_set($adjustment, 'id', array_get($adjustmentIds, $index));
            array_set($adjustment, 'type', 'App\Model\PhilippineAdjustment');
            return $adjustment;
        })->toArray();

        if ($response->isSuccessful()) {
            foreach ($adjustments as $adjustment) {
                // trigger audit trail
                $this->audit($request, $companyId, $adjustment);
            }
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/adjustment/{id}",
     *     summary="Update adjustment",
     *     description="Update adjustment
     Authorization Scope : **edit.adjustments**",
     *     tags={"adjustment"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Adjustment ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="adjustment",
     *         in="body",
     *         description="Adjustment data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/adjustmentsRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeRequestService $otherIncomeRequestService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $adjustmentResponse = $otherIncomeRequestService->get($id);
        if (!$adjustmentResponse->isSuccessful()) {
            return $adjustmentResponse;
        }
        $adjustmentData = json_decode($adjustmentResponse->getData(), true);
        $companyId = array_get($adjustmentData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $authData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->update($id, $request->all(), $this->getAuthzDataScope($request));
        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData, $adjustmentData, true);
        }
        return $response;
    }
}
