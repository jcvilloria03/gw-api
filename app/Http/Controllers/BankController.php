<?php

namespace App\Http\Controllers;

use App\Bank\BankRequestService;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class BankController extends Controller
{
    /**
     * @var \App\Bank\BankRequestService
     */
    protected $requestService;

    public function __construct(
        BankRequestService $requestService
    ) {
        $this->requestService = $requestService;
    }

    /**
     * @SWG\Get(
     *     path="/banks",
     *     security={ {"api_key": {}} },
     *     summary="Get Banks",
     *     description="Get list of supported banks",
     *     tags={"company"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Bank"))
     *         )
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     )
     * )
     */
    public function index()
    {
        return $this->requestService->index();
    }
}

