<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\LeaveEntitlement\LeaveEntitlementAuditService;
use App\LeaveEntitlement\LeaveEntitlementAuthorizationService;
use App\LeaveType\LeaveTypeAuthorizationService;
use App\Employee\EmployeeRequestService;
use Illuminate\Http\Request;
use App\LeaveEntitlement\LeaveEntitlementRequestService;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LeaveEntitlementController extends Controller
{
    /**
     * @var \App\LeaveEntitlement\LeaveEntitlementRequestService
     */
    private $requestService;

    /**
     * @var \App\LeaveEntitlement\LeaveEntitlementAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        LeaveEntitlementRequestService $requestService,
        LeaveEntitlementAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

      /**
     * @SWG\Get(
     *     path="/leave_entitlement/{id}",
     *     summary="Get LeaveEntitlement",
     *     description="Get LeaveEntitlement Details
Authorization Scope : **view.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="LeaveEntitlement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $isAuthorized = false;
        $response = $this->requestService->get($id);
        $leaveEntitlementData = json_decode($response->getData());

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized =  $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $leaveEntitlementData->company_id);
        } else {
            $leaveEntitlementData->account_id = Company::getAccountId($leaveEntitlementData->company_id);
            $user = $request->attributes->get('user');

            $isAuthorized = $this->authorizationService->authorizeGet($leaveEntitlementData, $user);
        }

        //authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

     /**
     * @SWG\Get(
     *     path="/employee/{id}/leave_entitlement",
     *     summary="Get leave entitlement for employee and given leave type",
     *     description="Get single leave entitlement for entitled employee and leave type
Authorization Scope : **view.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_type_id",
     *         in="query",
     *         description="Leave Type ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getForEmployeeAndLeaveType($id, Request $request, EmployeeRequestService $employeeRequestService)
    {
        $response = $this->requestService->getForEmployeeAndLeaveType($id, $request->getQueryString());

        $employeeResponse = $employeeRequestService->getEmployee($id);
        $employee = json_decode($employeeResponse->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeData = json_decode($employeeResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employeeData, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveEntitlementData = (object) [
                'account_id' => Company::getAccountId($employee->company_id),
                'company_id' => $employee->company_id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveEntitlementData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/leave_entitlements",
     *     summary="Get all Leave Entitlements",
     *     description="Get all Leave Entitlements within company.
Authorization Scope : **view.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLeaveEntitlements(Request $request, $id)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $leaveEntitlement = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveEntitlements(
                $leaveEntitlement,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyLeaveEntitlements($id);
        $leaveEntitlementData = json_decode($response->getData())->data;

        if (empty($leaveEntitlementData)) {
            return $response;
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_entitlement",
     *     summary="Create Leave entitlement.",
     *     description="Create Leave entitlement.
Authorization Scope : **create.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newLeaveEntitlement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newLeaveEntitlement",
     *     required={"company_id", "leave_type_ids", "accrue_leave_credits", "name",
     * "leave_credit_unit", "accrue_every", "accrual_period", "termination_leave_conversion_type",
     * "leave_conversion_run", "leave_conversion_type", "leave_credits", "termination_leave_credits"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Leave Entitlement Name"
     *     ),
     *     @SWG\Property(
     *         property="leave_type_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi",
     *         description="Leave Type IDs"
     *     ),
     *     @SWG\Property(
     *         property="accrue_leave_credits",
     *         type="integer",
     *         description="No. of Leave Credits to Accrue"
     *     ),
     *     @SWG\Property(
     *         property="leave_credit_unit",
     *         type="string",
     *         description="Leave Credit Unit. Possible values: Hours, Days."
     *     ),
     *     @SWG\Property(
     *         property="accrue_every",
     *         type="integer",
     *         description="Accrue Every (No. of unit)"
     *     ),
     *     @SWG\Property(
     *         property="accrual_period",
     *         description="Accrual Period. Possible values: Once-Off, Monthly, Weekly, Semi-Monthly",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="start_accruing_after",
     *         type="integer",
     *         description="Start Accruing after (No. of unit)"
     *     ),
     *     @SWG\Property(
     *         property="after_accrual_period",
     *         type="string",
     *         description="Start accruing after period value. Possible values: Half Months, Months, Weeks"
     *     ),
     *     @SWG\Property(
     *         property="leave_conversion_run",
     *         type="string",
     *         description="Leave conversion run. Possible values: Semi-Annual, Annual, Anniversary, Specific date"
     *     ),
     *     @SWG\Property(
     *         property="leave_conversion_type",
     *         type="string",
     *         description="Leave conversion type. Possible values: Carry Over the Next Period, For Payroll, Forfeit"
     *     ),
     *     @SWG\Property(
     *         property="leave_credits",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="termination_leave_conversion_type",
     *         type="string",
     *         description="Termination leave conversion type. Possible values: For Payroll, Forfeit"
     *     ),
     *     @SWG\Property(
     *         property="termination_leave_credits",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_leave_entitlement"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="affected_employees_leave_entitlement",
     *     required={"type"},
     *     @SWG\Property(
     *         property="id",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         description="Possible values: department, position, location, employee."
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();
        $isAuthorized = false;

        $leaveEntitlementData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($leaveEntitlementData, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $leaveEntitlementGetResponse = $this->requestService->get($responseData['id']);
        $leaveEntitlement = json_decode($leaveEntitlementGetResponse->getData(), true);
        $details = [
            'new' => $leaveEntitlement,
        ];
        $item = new AuditCacheItem(
            LeaveEntitlementAuditService::class,
            LeaveEntitlementAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

      /**
      * @SWG\Delete(
      *     path="/leave_entitlement/bulk_delete",
      *     summary="Delete multiple Leave Entitlements",
      *     description="Delete multiple Leave Entitlements
Authorization Scope : **delete.leave_entitlement**",
      *     tags={"leave_entitlement"},
      *     consumes={"application/x-www-form-urlencoded"},
      *     produces={"application/json"},
      *     @SWG\Parameter(
      *         type="string",
      *         name="Authorization",
      *         in="header",
      *         required=true
      *     ),
      *      @SWG\Parameter(
      *         type="string",
      *         name="X-Authz-Entities",
      *         in="header",
      *         required=true,
      *         description="Salarium Module Map"
      *     ),
      *     @SWG\Parameter(
      *         name="company_id",
      *         in="formData",
      *         description="Company ID",
      *         required=true,
      *         type="integer"
      *     ),
      *     @SWG\Parameter(
      *         name="leave_entitlements_ids[]",
      *         type="array",
      *         in="formData",
      *         description="Leave entitlemens Ids",
      *         required=true,
      *         @SWG\Items(type="integer"),
      *         collectionFormat="multi"
      *     ),
      *     @SWG\Response(
      *         response=204,
      *         description="Successful operation",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
      *         description="Company not found.",
      *     ),
      *     @SWG\Response(
      *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
      *         description="Invalid request",
      *     )
      * )
      */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        $leaveEntitlementData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        // authorize
        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeDelete(
                $leaveEntitlementData,
                $deletedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('leave_entitlements_ids', []) as $id) {
                $item = new AuditCacheItem(
                    LeaveEntitlementAuditService::class,
                    LeaveEntitlementAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $request->get('company_id')
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/leave_entitlement/{id}",
     *     summary="Update Leave entitlement.",
     *     description="Update Leave entitlement.
Authorization Scope : **edit.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/existingLeaveEntitlement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="existingLeaveEntitlement",
     *     required={"company_id", "leave_type_ids", "accrue_leave_credits", "name",
     * "leave_credit_unit", "accrue_every", "accrual_period", "termination_leave_conversion_type",
     * "leave_conversion_run", "leave_conversion_type", "leave_credits", "termination_leave_credits"},
     *     @SWG\Property(
     *         property="company_id",
     *         type="integer",
     *         description="Company ID"
     *     ),
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Leave Entitlement Name"
     *     ),
     *     @SWG\Property(
     *         property="leave_type_ids",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi",
     *         description="Leave Type IDs"
     *     ),
     *     @SWG\Property(
     *         property="accrue_leave_credits",
     *         type="integer",
     *         description="No. of Leave Credits to Accrue"
     *     ),
     *     @SWG\Property(
     *         property="leave_credit_unit",
     *         type="string",
     *         description="Leave Credit Unit. Possible values: Hours, Days."
     *     ),
     *     @SWG\Property(
     *         property="accrue_every",
     *         type="integer",
     *         description="Accrue Every (No. of unit)"
     *     ),
     *     @SWG\Property(
     *         property="accrual_period",
     *         description="Accrual Period. Possible values: Once-Off, Monthly, Weekly, Semi-Monthly",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="start_accruing_after",
     *         type="integer",
     *         description="Start Accruing after (No. of unit)"
     *     ),
     *     @SWG\Property(
     *         property="after_accrual_period",
     *         type="string",
     *         description="Start accruing after period value. Possible values: Half Months, Months, Weeks"
     *     ),
     *     @SWG\Property(
     *         property="leave_conversion_run",
     *         type="string",
     *         description="Leave conversion run. Possible values: Semi-Annual, Annual, Anniversary, Specific date"
     *     ),
     *     @SWG\Property(
     *         property="leave_conversion_type",
     *         type="string",
     *         description="Leave conversion type. Possible values: Carry Over the Next Period, For Payroll, Forfeit"
     *     ),
     *     @SWG\Property(
     *         property="leave_credits",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="termination_leave_conversion_type",
     *         type="string",
     *         description="Termination leave conversion type. Possible values: For Payroll, Forfeit"
     *     ),
     *     @SWG\Property(
     *         property="termination_leave_credits",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees_leave_entitlement"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function update(Request $request, int $id)
    {
         // authorize
         $inputs = $request->all();
         $updatedBy = $request->attributes->get('user');
         $response = $this->requestService->get($id);
         $oldLeaveEntitlementData = json_decode($response->getData());
         $oldLeaveEntitlementData->account_id = Company::getAccountId($inputs['company_id']);

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => [$inputs['company_id'], $oldLeaveEntitlementData->company_id]
                ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldLeaveEntitlementData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldLeaveEntitlementData = json_decode($response->getData(), true);
        $newLeaveEntitlementData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldLeaveEntitlementData,
            'new' => $newLeaveEntitlementData,
        ];
        $item = new AuditCacheItem(
            LeaveEntitlementAuditService::class,
            LeaveEntitlementAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/leave_entitlement/is_name_available",
     *     summary="Is Leave Entitlement name available",
     *     description="Check availability of Leave Entitlement name with the given company",
     *     tags={"leave_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Leave Entitlement Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_entitlement_id",
     *         in="formData",
     *         description="Leave Entitlement Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);

            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $leaveEntitlementData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $leaveEntitlementData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($id, $request->all());
    }

    /**
     * @SWG\Post(
     *     path="/leave_entitlement/check_in_use",
     *     summary="Are Leave Entitlements in use",
     *     description="Check are Leave Entitlements in use
Authorization Scope : **view.leave_entitlement**",
     *     tags={"leave_entitlement"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_entitlements_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Entitlements Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $leaveEntitlementData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveEntitlements(
                $leaveEntitlementData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }
}
