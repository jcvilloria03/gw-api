<?php

namespace App\Http\Controllers;

use App\DeductionType\DeductionTypeAuthorizationService;
use App\DeductionType\PhilippineDeductionTypeRequestService;
use App\Facades\Company;
use App\OtherIncomeType\OtherIncomeTypeAuditService;
use App\OtherIncomeType\OtherIncomeTypeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;

class PhilippineDeductionTypeController extends Controller
{
    /**
     * @var \App\DeductionType\PhilippineDeductionTypeRequestService
     */
    protected $requestService;

    /**
     * @var \App\DeductionType\DeductionTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\OtherIncomeType\OtherIncomeTypeAuditService
     */
    protected $auditService;

    public function __construct(
        PhilippineDeductionTypeRequestService $requestService,
        DeductionTypeAuthorizationService $authorizationService,
        OtherIncomeTypeAuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/deduction_type/bulk_create",
     *     summary="Create Multiple Deduction Types",
     *     description="Create multiple deduction types,
Authorization Scope : **create.deduction_types**",
     *     tags={"deduction_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="deduction_types",
     *         in="body",
     *         description="Create multiple deduction types",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deductionTypesRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="deductionTypesRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/deductionTypesRequestItem"),
     *     collectionFormat="multiple"
     * ),
     *
     * @SWG\Definition(
     *     definition="deductionTypesRequestItem",
     *     required={"name"},
     *     @SWG\Property(
     *         property="name",
     *         type="string",
     *         description="Deduction custom name",
     *         example="Food"
     *     )
     * )
     */
    public function bulkCreate(Request $request, $id)
    {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $data = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id,
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkCreate($id, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $otherIncomeTypes = array_get(
            json_decode($response->getData(), true),
            'data',
            []
        );

        if ($response->isSuccessful()) {
            // audit log
            $this->auditService->mapAndQueueAuditItems(
                $otherIncomeTypes,
                OtherIncomeTypeAuditService::ACTION_CREATE,
                $createdBy
            );
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/deduction_type/is_name_available",
     *     summary="Is philippine deduction type name available",
     *     description="Check availability of philippine deduction type name with the given company,
Authorization Scope : **create.deduction_types**",
     *     tags={"deduction_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Type name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="other_income_type_id",
     *         in="formData",
     *         description="Other Income Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $data = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id,
        ];
        
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate($data, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->isNameAvailable($id, $request->all());

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/deduction_type/{id}",
     *     summary="Update the deduction type",
     *     description="Update the deduction type
Authorization Scope : **edit.deduction_types**",
     *     tags={"deduction_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Deduction id"
     *     ),
     *     @SWG\Parameter(
     *         in="formData",
     *         name="name",
     *         type="string",
     *         required=true,
     *         description="Deduction custom name"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeTypeRequestService $otherIncomeTypeRequestService,
        $id
    ) {
        $deductionTypeResponse = $otherIncomeTypeRequestService->get($id);

        if (!$deductionTypeResponse->isSuccessful()) {
            return $deductionTypeResponse;
        }
        $deductionTypeData = json_decode($deductionTypeResponse->getData(), true);
        $companyId = array_get($deductionTypeData, 'company_id');
        $updatedBy = $request->attributes->get('user');
        $authData = (object) [
            'account_id' => $companyId ? Company::getAccountId($companyId) : null,
            'company_id' => $companyId,
        ];

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate($authData, $updatedBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
        // call microservice
        $response = $this->requestService->update($id, $request->all());
        
        if ($response->isSuccessful()) {
            // audit log
            $this->auditService->mapAndQueueAuditItems(
                [json_decode($response->getData(), true)],
                OtherIncomeTypeAuditService::ACTION_UPDATE,
                $updatedBy,
                [$deductionTypeData]
            );
        }

        return $response;
    }
}
