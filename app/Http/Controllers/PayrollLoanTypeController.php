<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Facades\Company;
use App\PayrollLoanType\PayrollLoanTypeAuditService;
use App\PayrollLoanType\PayrollLoanTypeAuthorizationService;
use App\PayrollLoanType\PayrollLoanTypeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PayrollLoanTypeController extends Controller
{
    /**
     * @var \App\PayrollLoanType\PayrollLoanRequestService
     */
    protected $requestService;

    /**
     * @var \App\PayrollLoanType\PayrollLoanTypeAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        PayrollLoanTypeRequestService $requestService,
        PayrollLoanTypeAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/payroll_loan_types",
     *     summary="Get all Payroll Loan Types",
     *     description="Get all payroll loan types for company.
Authorization Scope : **view.payroll_loan_types**",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLoanTypes(Request $request, $companyId)
    {
        $response = $this->requestService->getCompanyLoanTypes($companyId);
        $loansData = json_decode($response->getData())->data;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $userData = $request->attributes->get('user');
        $isAuthorized = false;

        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeGetAll($data, $userData);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }
        
        if (empty($loansData)) {
            return $response;
        }
        
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/payroll_loan_type",
     *     summary="Create new payroll loan type",
     *     description="Create new payroll loan type,
    Authorization Scope : **create.payroll_loan_types**",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="names",
     *         in="formData",
     *         description="Loan type identifier. Comma separated values",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            
            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->create($request->all());

        $responseData = json_decode($response->getData(), true);

        if ($response->isSuccessful()) {
            foreach ($responseData as $loanType) {
                // audit log
                $item = new AuditCacheItem(
                    PayrollLoanTypeAuditService::class,
                    PayrollLoanTypeAuditService::ACTION_CREATE,
                    new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                    [
                        'new' => $loanType,
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/payroll_loan_type/subtypes/{loanTypeName}",
     *     summary="Get payroll loan subtypes name",
     *     description="Get payroll loan subtypes by loan name.",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="loanTypeName",
     *         in="path",
     *         description="Loan Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getLoanSubtypesByName($loanTypeName)
    {
        return $this->requestService->getLoanSubtypesByName($loanTypeName);
    }

    /**
     * @SWG\Patch(
     *     path="/payroll_loan_type/{id}",
     *     summary="Update Payroll loan type",
     *     description="Update Payroll loan type
    Authorization Scope : **edit.payroll_loan_type**",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payroll Loan Type Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Payroll Loan Type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldData = json_decode($response->getData());

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = (
                $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']) &&
                $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $oldData->company_id)
            );
        } else {
            $oldData->account_id = Company::getAccountId($inputs['company_id']);
            $isAuthorized = $this->authorizationService->authorizeUpdate($oldData, $updatedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldData = json_decode($response->getData(), true);
        $newData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldData,
            'new' => $newData,
        ];
        $item = new AuditCacheItem(
            PayrollLoanTypeAuditService::class,
            PayrollLoanTypeAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/payroll_loan_type/bulk_delete",
     *     summary="Delete multiple Payroll Loan Types",
     *     description="Delete multiple Payroll Loan Types,
    Authorization Scope : **delete.payroll_loan_types**",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_loan_type_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Payroll loan type ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');
        
        $authzEnabled = $request->attributes->get('authz_enabled');
        $dataScope = $request->attributes->get('authz_data_scope');
        $isAuthorized = false;

        if ($authzEnabled) {
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id'],
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete($data, $deletedBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);

        if ($response->isSuccessful()) {
            // audit log
            foreach ($request->input('payroll_loan_type_ids', []) as $id) {
                $item = new AuditCacheItem(
                    PayrollLoanTypeAuditService::class,
                    PayrollLoanTypeAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $request->get('company_id'),
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/payroll_loan_types/is_name_available",
     *     summary="Is payroll loan type name available",
     *     description="Check availability of payroll loan type name with the given company",
     *     tags={"payroll_loan_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Payroll loan type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="payroll_loan_id",
     *         in="formData",
     *         description="Payroll loan Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable(Request $request, $companyId)
    {
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        
        if ($authzEnabled) {
            $dataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $dataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            
            $isAuthorized = $this->authorizationService->authorizeCreate($data, $user);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($companyId, $request->all());
    }
}
