<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\AffectedEmployee\AffectedEmployeeRequestService;
use App\AffectedEmployee\EssAffectedEmployeeAuthorizationService;

class EssAffectedEmployeeController extends EssBaseController
{
    /**
     * @var \App\AffectedEmployee\AffectedEmployeeRequestService
     */
    private $requestService;

    /**
     * @var \App\AffectedEmployee\EssAffectedEmployeeAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        AffectedEmployeeRequestService $requestService,
        EssAffectedEmployeeAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/affected_employees/search",
     *     summary="Get affected employees by term",
     *     description="Get affected employees by term
Authorization Scope : [**ess.view.profile**]",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Search limit",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="include_admins",
     *         in="query",
     *         description="Include admins in the search",
     *         required=false,
     *         type="boolean"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     *
     * )
     */
    public function search(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeSearch($userId)
        ) {
            $this->response()->errorUnauthorized();
        }

        $includeAdmins = filter_var($request->input('include_admins', false), FILTER_VALIDATE_BOOLEAN);

        return $this->requestService->search(
            $user['employee_company_id'],
            $request->input('term', ''),
            $request->input('limit'),
            !$includeAdmins,
            $includeAdmins
        );
    }
}
