<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Workflow\WorkflowRequestService;
use App\Http\Controllers\EssBaseController;
use App\ESS\EssEmployeeRequestRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\EmployeeRequest\EmployeeRequestAuditService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\ESS\EssApprovalService;

class EssEmployeeRequestController extends EssBaseController
{
    /**
     * @var \App\ESS\EssEmployeeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\ESS\EssApprovalService
     */
    private $essApprovalService;

    public function __construct(
        EssEmployeeRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService,
        EssApprovalService $essApprovalService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->essApprovalService = $essApprovalService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/requests",
     *     summary="Get employee Requests",
     *     description="Get all Requests for employee.
Authorization Scope : **ess.view.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. It should be >= 1. Default is 1.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="statuses",
     *         in="query",
     *         description="Requests statuses,
     *                      Possible values: pending, approved, declined, cancelled, rejected.",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="types",
     *         in="query",
     *         description="Requests types. Possible values:
     *                      leaves, loans, overtime, shift_change, time_dispute, undertime.",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         description="Date range start. A inclusive lower date boundary for filtering YYYY-MM-DD.",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         description="Date range end. A inclusive upper date boundary for filtering YYYY-MM-DD.",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getRequests(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        // authorize
        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeView($user)
        ) {
            $this->response()->errorUnauthorized();
        }

        $employeeId = $user['employee_id'];
        $employeeRequests = $this->requestService->getRequests($employeeId, $request->getQueryString());

        return $employeeRequests;
    }

    /**
     * @SWG\Post(
     *     path="/ess/request/send_message",
     *     summary="Send request message.",
     *     description="Send request message.
Authorization Scope : **ess.update.request**",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="request_id",
     *         in="formData",
     *         description="Employee request Id.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="content",
     *         in="formData",
     *         description="Message content.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function sendMessage(Request $request, WorkflowRequestService $workflowRequestService)
    {
        // authorize
        $user = $this->getFirstEmployeeUser($request);
        $inputs = $request->all();

        $responseData = $workflowRequestService->getEmployeeWorkflows($user['employee_id']);
        $workflows = json_decode($responseData->getData(), true);

        $response = $this->requestService->get($request['request_id']);
        $employeeRequest = json_decode($response->getData());

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeSendMessage($user, $workflows, $employeeRequest)
        ) {
            $this->response()->errorUnauthorized();
        }

        $inputs['sender_id'] = $user['user_id'];

        $response = $this->requestService->sendMessage($inputs);
        $requestMessage = json_decode($response->getData(), true);

        // audit log
        $details = [
            'new' => $requestMessage
        ];
        $item = new AuditCacheItem(
            EmployeeRequestAuditService::class,
            EmployeeRequestAuditService::ACTION_SEND_MESSAGE,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

      /**
     * @SWG\Post(
     *     path="/ess/request/{id}/cancel",
     *     summary="Cancel Employee Request",
     *     description="Cancel Employee Request Details
Authorization Scope : **ess.cancel.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Employee Request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function cancel($id, Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (empty($id)) {
            $this->invalidRequestError('Request ID should not be empty.');
        }

        if (!is_numeric($id)) {
            $this->invalidRequestError('Request should be numeric.');
        }

        $response = $this->requestService->get($id);
        $employeeRequest = json_decode($response->getData());

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->authorizationService->authorizeCancel($user, $employeeRequest)
        ) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->cancel($id);
        $employeeRequest = json_decode($response->getData(), true);

        // audit log
        $details = [
            'request' => $employeeRequest
        ];
        $item = new AuditCacheItem(
            EmployeeRequestAuditService::class,
            EmployeeRequestAuditService::ACTION_CANCEL_REQUEST,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }
}
