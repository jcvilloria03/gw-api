<?php

namespace App\Http\Controllers;

use App\Approval\ApprovalService;
use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\Workflow\WorkflowRequestService;
use App\Http\Controllers\EssBaseController;
use Symfony\Component\HttpFoundation\Response;
use App\ESS\EssTimeDisputeRequestRequestService;
use App\TimeDisputeRequest\TimeDisputeRequestAuditService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\Authz\AuthzDataScope;

class EssTimeDisputeRequestController extends EssBaseController
{
    /**
     * @var \App\ESS\EssTimeDisputeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EssTimeDisputeRequestRequestService $requestService,
        EssEmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/time_dispute_request",
     *     summary="Create Time Dispute Request",
     *     description="Create Time Dispute Request
     Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newTimeDisputeRequest"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newTimeDisputeRequest",
     *     required={"start_date", "end_date", "shifts"},
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         default="2017-10-29",
     *         description="Time dispute request start date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         default="2017-10-30",
     *         description="Time dispute request end date in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="messages",
     *         type="array",
     *         @SWG\Items(type="string"),
     *         collectionFormat="multi",
     *         description="Initial messages."
     *     ),
     *     @SWG\Property(
     *         property="shifts",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/timeDisputeShifts")
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="timeDisputeShifts",
     *     required={"shift_id", "date", "timesheet", "hours_worked"},
     *     @SWG\Property(
     *         property="shift_id",
     *         type="string",
     *         description="Shift ID. Null value for Default Schedule."
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-01-01",
     *         description="Date with time disputes in format YYYY-MM-DD."
     *     ),
     *     @SWG\Property(
     *         property="timesheet",
     *         type="object",
     *         @SWG\Property(property="old_state", type="array", @SWG\Items(ref="#/definitions/singleTimesheet")),
     *         @SWG\Property(property="new_state", type="array", @SWG\Items(ref="#/definitions/singleTimesheet")),
     *         description="Timesheet changes."
     *     ),
     *     @SWG\Property(
     *         property="hours_worked",
     *         type="object",
     *         @SWG\Property(property="old_state", type="array", @SWG\Items(ref="#/definitions/singleHoursWorked")),
     *         @SWG\Property(property="new_state", type="array", @SWG\Items(ref="#/definitions/singleHoursWorked")),
     *         description="Hours worked changes."
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="singleTimesheet",
     *     required={"type", "timestamp", "tags"},
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="clock_in"
     *     ),
     *     @SWG\Property(
     *         property="timestamp",
     *         type="integer",
     *         default=1529488800
     *     ),
     *    @SWG\Property(
     *         property="tags",
     *         type="array",
     *         @SWG\Items(type="string")
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="singleHoursWorked",
     *     required={"time","type"},
     *     @SWG\Property(
     *         property="time",
     *         type="string",
     *         default="08:00",
     *         description="Hours worked in format HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="overtime",
     *         description="Hours worked type. Possible values: 'regular',
 'overtime', 'undertime', 'night_shift', 'overtime_night_shift'"
     *     ),
     * )
     */
    public function create(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeCreate($user)) {
            $this->response()->errorUnauthorized();
        }

        $data = $request->all();

        $data['company_id'] = $user['employee_company_id'];
        $data['employee_id'] = $user['employee_id'];
        $data['user_id'] = $user['user_id'];
        $response = $this->requestService->create($data);

        if ($response->isSuccessful()) {
            $responseData = json_decode($response->getData(), true);
            $timeDisputeRequest = $this->requestService->get($responseData['id']);
            $timeDisputeRequestData = json_decode($timeDisputeRequest->getData(), true);

            $details = [
                'new' => [
                    'employee_company_id' => $user['employee_company_id'],
                    'id' => $timeDisputeRequestData['id'],
                ],
            ];

            $item = new AuditCacheItem(
                TimeDisputeRequestAuditService::class,
                TimeDisputeRequestAuditService::ACTION_CREATE,
                new AuditUser($user['user_id'], $user['account_id']),
                $details
            );

            $this->auditService->queue($item);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/time_dispute_request/{id}",
     *     summary="Get Time Dispute Request",
     *     description="Get Time Dispute Request",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Time Dispute Request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id
    ) {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->get($id);

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $timeDisputeRequest = json_decode($response->getData());
        $timeDisputeRequest->account_id = $user['account_id'];

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $timeDisputeRequest->company_id
            );

            $isAuthorized = ApprovalService::isEitherRequestorOrApproverNew(
                $user['employee_id'],
                $timeDisputeRequest,
                $user
            );
        } else {
            $workflows = $timeDisputeRequest->workflow_levels;

            $isAuthorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $timeDisputeRequest
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this
            ->response
            ->array((array) $timeDisputeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
