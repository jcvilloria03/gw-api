<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attendance\AttendanceRequestService;
use App\Http\Controllers\EssBaseController;

class EssAttendanceController extends EssBaseController
{
    /**
     * @SWG\Get(
     *     path="/ess/attendance/{employee_id}/{date}",
     *     summary="Retrieve a Single Attendance Record",
     *     description="Retrieves a single attendance record for a given employee on a given date.

Authorization Scope : **view.computed_attendance**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="path",
     *         description="Employee Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="path",
     *         description="Attendance Date (yyyy-mm-dd). Date of attendance record.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful Operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Record not found",
     *     ),
     * )
     */
    public function getSingleAttendanceRecord(
        Request $request,
        AttendanceRequestService $requestService,
        $employeeId,
        $attendanceDate
    ): \Illuminate\Http\JsonResponse {
        $requestor = $this->getFirstEmployeeUser($request);

        if (empty($requestor['employee_id']) || $requestor['employee_id'] != $employeeId) {
            $this->response()->errorUnauthorized();
        }

        return $requestService->getSingleAttendanceRecord($employeeId, $attendanceDate);
    }
}
