<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
 */
class EssBaseController extends Controller
{
    /**
     * Get first company user from request with employee_id entry
     *
     * @param Request $request
     * @return void
     */
    protected function getFirstEmployeeUser(Request $request)
    {
        $companiesUsers = $request->attributes->get('companies_users');

        foreach ($companiesUsers as $companyUser) {
            if (!empty($companyUser['employee_id'])) {
                return $companyUser;
            }
        }
    }
}
