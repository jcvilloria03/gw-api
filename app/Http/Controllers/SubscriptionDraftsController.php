<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Request;
use App\User\UserRequestService;
use App\User\UserAuthorizationService;

use App\Subscriptions\SubscriptionsRequestService;
use App\Subscriptions\SubscriptionsAuthorizationService;
use App\Traits\AuditTrailTrait;

class SubscriptionDraftsController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Subscriptions\SubscriptionsRequestService
     */
    protected $requestService;

    /**
     * @var \App\Subscriptions\SubscriptionsAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(
        SubscriptionsRequestService $subscriptionsRequestService,
        SubscriptionsAuthorizationService $authorizationService,
        UserRequestService $userRequestService
    ) {
        $this->requestService = $subscriptionsRequestService;
        $this->userRequestService = $userRequestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\GET(
     *     path="/subscriptions/{subscriptionId}/draft",
     *     summary="Get subscription update draft",
     *     description="Get subscription update draft. Returns a preview of the subscription with draft applied",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="subscriptionId",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Subscription not found or draft not found",
     *     )
     * )
     */
    public function show(Request $request, $subscriptionId)
    {
        if (!$this->isAuthzEnabled($request)) {
            $this->authorizeRequest($request, 'authorizeGet');
        }

        // pass request to sb-api
        return $this->requestService->getSubscriptionDraft($subscriptionId);
    }

    /**
     * @SWG\DELETE(
     *     path="/subscriptions/{subscriptionId}/draft",
     *     summary="Delete subscription update draft",
     *     description="Delete subscription update draft",
     *     tags={"subscriptions"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="subscriptionId",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Subscription not found or draft not found",
     *     )
     * )
     */
    public function destroy(Request $request, $subscriptionId)
    {
        $user = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($user['user_id']);
        $userData = json_decode($userResponse->getData());
        $companyId = $userData->companies[0]->id;

        if (!$this->isAuthzEnabled($request)) {
            $this->authorizeRequest($request, 'authorizeUpdate');
        }

        // For audit trail
        try {
            $oldDataResponse = $this->requestService->getSubscriptionDraft($subscriptionId);
            $oldData = json_decode($oldDataResponse->getData(), true);
        } catch (\Exception $e) {
            $oldData = [];
        }

        $response = $this->requestService->deleteSubscriptionDraft($subscriptionId);
        if ($response->isSuccessful()) {
            // trigger audit trail
            $this->audit($request, $companyId, [], $oldData);
        }
        // pass request to sb-api
        return $response;
    }

    /**
     * Authorize access to resource
     *
     * @param \Illuminate\Http\Request $request
     * @param string $method method name to be used by auth service
     * @return void
     */
    private function authorizeRequest(Request $request, $method)
    {
        $createdBy   = $request->attributes->get('user');
        $userResponse = $this->userRequestService->get($createdBy['user_id']);

        $userData = json_decode($userResponse->getData());

        $authData = (object) [
            'account_id' => $userData->account_id,
            'company_id' => $userData->companies[0]->id,
        ];

        // authorize
        if (!$this->authorizationService->{$method}(
            $authData,
            $createdBy
        )) {
            $this->response()->errorUnauthorized();
        }
    }
}
