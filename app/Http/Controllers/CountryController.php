<?php

namespace App\Http\Controllers;

use App\Country\CountryRequestService;

class CountryController extends Controller
{
    /*
     * App\Country\CountryRequestService
     */
    protected $requestService;

    public function __construct(
        CountryRequestService $requestService
    ) {
        $this->requestService = $requestService;
    }


    /**
     * @SWG\Get(
     *     path="/countries",
     *     summary="Get all supported countries",
     *     description="Get all supported countries",
     *     tags={"country"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getAll()
    {
        // call microservice
        return $this->requestService->getAll();
    }
}
