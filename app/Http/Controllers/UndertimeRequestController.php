<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\ESS\EssUndertimeRequestRequestService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use Illuminate\Http\Request;
use App\ClockState\ClockStateService;
use Symfony\Component\HttpFoundation\Response;
class UndertimeRequestController extends Controller
{
    /**
     * @var \App\ESS\EssUndertimeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    public function __construct(
        EssUndertimeRequestRequestService $requestService,
        EmployeeRequestAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/undertime_request/{id}",
     *     summary="Get Undertime Requests",
     *     description="Get Undertime Request.
        Authorization Scope : **view.request**",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Undertime request ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id,
        ClockStateService $clockStateService
    ) {
        $user = $request->attributes->get('user');

        $response = $this->requestService->get($id);

        if (!$response->isSuccessful()) {
            return $response;
        }

        $undertimeRequest = json_decode($response->getData());
        $undertimeRequest->account_id = $user['account_id'];

        $authorized = true;
        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $workflows = $undertimeRequest->workflow_levels;

            $authorized = $this->authorizationService->authorizeViewSingleRequest($user, $workflows, $undertimeRequest);

            // authorize
            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        // fetch timesheet
        try {
            $shiftStart = collect($undertimeRequest->shifts)->pluck('start')->unique()->all();
            $shiftEnd = collect($undertimeRequest->shifts)->pluck('end')->unique()->all();

            $timeSheets = $clockStateService->getFormattedTimesheet(
                $undertimeRequest->employee_id,
                $shiftStart,
                $shiftEnd
            );
            $undertimeRequest->timesheet = isset($timeSheets['data']) ? $timeSheets['data'] : [];
        } catch (\Throwable $e) {
            \Log::error($e->getMessage() . ' : No timesheet data = ' . $undertimeRequest->employee_id);
            \Log::error($e->getTraceAsString());
        }

        return $this
            ->response
            ->array((array) $undertimeRequest)
            ->setStatusCode(Response::HTTP_OK);
    }
}
