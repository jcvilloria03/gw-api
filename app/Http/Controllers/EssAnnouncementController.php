<?php

namespace App\Http\Controllers;

use App\Audit\AuditUser;
use App\Audit\AuditService;
use Illuminate\Http\Request;
use App\Audit\AuditCacheItem;
use App\ESS\EssAnnouncementRequestService;
use App\Http\Controllers\EssBaseController;
use App\Announcement\AnnouncementAuditService;
use App\Announcement\EssAnnouncementAuthorizationService;
use App\Authz\AuthzDataScope;

class EssAnnouncementController extends EssBaseController
{
    /**
     * @var \App\ESS\EssAnnouncementRequestService
     */
    protected $requestService;

    /**
     * @var \App\Announcement\EssAnnouncementAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        EssAnnouncementRequestService $requestService,
        EssAnnouncementAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Post(
     *     path="/ess/announcement/",
     *     summary="Create Announcement.",
     *     description="Create Announcement.
Authorization Scope : **ess.create.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/newAnnouncement"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     *
     * @SWG\Definition(
     *     definition="newAnnouncement",
     *     required={"affected_employees", "subject", "message", "read_receipt", "allow_response"},
     *     @SWG\Property(
     *         property="subject",
     *         type="string",
     *         description="Subject of announcement"
     *     ),
     *     @SWG\Property(
     *         property="message",
     *         type="string",
     *         description="Content of announcement message"
     *     ),
     *     @SWG\Property(
     *         property="read_receipt",
     *         type="boolean",
     *         description="See the list of announcement recipients who read the announcement"
     *     ),
     *     @SWG\Property(
     *         property="allow_response",
     *         type="boolean",
     *         description="Allow reply on anncouncement"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/affected_employees"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $createdBy = $this->getFirstEmployeeUser($request);
        $inputs = $request->all();

        $inputs['company_id'] = $createdBy['employee_company_id'];
        $inputs['sender_id'] = $createdBy['user_id'];

        if (!$this->isAuthzEnabled($request)
            && !$this->authorizationService->authorizeCreate($createdBy['user_id'])) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($inputs);

        if ($response->isSuccessful()) {
            // audit log
            $this->auditService->queue(new AuditCacheItem(
                AnnouncementAuditService::class,
                AnnouncementAuditService::ACTION_CREATE,
                new AuditUser($createdBy['user_id'], $createdBy['account_id']),
                [
                    'announcement' => json_decode($response->getData(), true),
                ]
            ));
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/ess/announcement/{id}",
     *     summary="Get Announcement",
     *     description="Get Announcement details for given id
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);

        $response = $this->requestService->getUserRoleForAnnouncement($id, $user['user_id']);
        $role = json_decode($response->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $role->sender || $role->recipient;
        } else {
            $isAuthorized = $this->authorizationService->authorizeSingleView($user['user_id'], $role);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $userRole = $role->sender ? 'sender' : 'recipient';

        return $this->requestService->get($id, $userRole, $user['user_id']);
    }

    /**
     * @SWG\Get(
     *     path="/ess/announcement/reply/{id}",
     *     summary="Get Announcement Response",
     *     description="Get Announcement Response details for given ID
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Response ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getReply(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        $response = $this->requestService->getReply($id);
        $reply = json_decode($response->getData(), true);

        $roleResponse = $this->requestService
            ->getUserRoleForAnnouncement($reply['announcement_id'], $userId);
        $role = json_decode($roleResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $role->sender || $role->recipient;
        } else {
            $isAuthorized = $this->authorizationService->authorizeSingleView($userId, $role);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/ess/employee/announcements",
     *     summary="Get employee Announcements",
     *     description="Get all Announcements with filters applied.
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number for announcements. It should be >= 1. Default is 1.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AnnouncementFilter"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     *
     * @SWG\Definition(
     *     definition="AnnouncementFilter",
     *     @SWG\Property(
     *         property="created_by_me",
     *         type="boolean",
     *     ),
     *     @SWG\Property(
     *         property="start_date",
     *         type="string",
     *         description="Start date 'YYYY-MM-DD'"
     *     ),
     *     @SWG\Property(
     *         property="end_date",
     *         type="string",
     *         description="End date 'YYYY-MM-DD'"
     *     ),
     *     @SWG\Property(
     *         property="affected_employees",
     *         type="array",
     *         @SWG\Items(
     *             ref="#/definitions/affected_employees"
     *         )
     *     )
     * )
     */
    public function getAnnouncements(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        // authorize
        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeView($userId)) {
            $this->response()->errorUnauthorized();
        }

        $attributes = $request->all();
        $attributes['company_id'] = $user['employee_company_id'];

        return $this->requestService->getUserAnnouncements($userId, $attributes, $request->getQueryString());
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/announcements/unread",
     *     summary="Get unread user Announcements",
     *     description="Get all unread Announcements within user.
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getUserUnreadAnnouncements(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        // authorize
        if (!$this->isAuthzEnabled($request) && !$this->authorizationService->authorizeView($userId)) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getUserUnreadAnnouncements($userId);
    }

    /**
     * @SWG\Post(
     *     path="/ess/announcement/{id}/reply",
     *     summary="Send reply to Announcement",
     *     description="Send reply message to Announcement with given id",
     *     tags={"ess"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="message",
     *         in="formData",
     *         description="Reply content",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function reply(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $attributes = $request->all();

        $attributes['sender_id'] = $user['user_id'];

        $response = $this->requestService->getUserRoleForAnnouncement($id, $user['user_id']);
        $role = json_decode($response->getData());

        if (!$role->recipient) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->reply($id, $attributes);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'reply' => $responseData,
        ];
        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_SEND_REPLY,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/ess/announcement/{id}/recipient_seen",
     *     summary="Update recipient when saw announcement",
     *     description="Update recipient when saw announcement
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Announcement ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function updateRecipientSeen(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        $response = $this->requestService->getUserRoleForAnnouncement($id, $userId);
        $role = json_decode($response->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $role->recipient;
        } else {
            $isAuthorized = $this->authorizationService->authorizeRecipientView($userId, $role);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $domain = $request->headers->get('origin');
        $response = $this->requestService->recipientSawAnnouncement($id, $userId, $request->ip(), $domain);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'announcement_recipient' => $responseData,
        ];
        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_RECIPIENT_SEEN,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Put(
     *     path="/ess/announcement/reply/{id}/seen",
     *     summary="Update announcement reply as seen",
     *     description="Mark announcement response as seen when announcement sender open it
Authorization Scope : **ess.view.announcement**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Response ID (It is and ID from announcement_replies table on nt-api database)",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function markReplyAsSeen(Request $request, $id)
    {
        $user = $this->getFirstEmployeeUser($request);
        $userId = $user['user_id'];

        $response = $this->requestService->getReply($id);
        $reply = json_decode($response->getData(), true);

        $roleResponse = $this->requestService
            ->getUserRoleForAnnouncement($reply['announcement_id'], $userId);
        $role = json_decode($roleResponse->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $role->sender;
        } else {
            $isAuthorized = $this->authorizationService->authorizeSenderView($userId, $role);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->markReplyAsSeen($id);
        $responseData = json_decode($response->getData(), true);

        // audit log
        $details = [
            'announcement_response' => $responseData,
        ];
        $item = new AuditCacheItem(
            AnnouncementAuditService::class,
            AnnouncementAuditService::ACTION_RESPONSE_SEEN,
            new AuditUser($user['user_id'], $user['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }
}
