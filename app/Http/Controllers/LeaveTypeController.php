<?php

namespace App\Http\Controllers;

use App\Audit\AuditCacheItem;
use App\Audit\AuditService;
use App\Audit\AuditUser;
use App\Authz\AuthzDataScope;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\LeaveType\LeaveTypeAuditService;
use App\LeaveType\LeaveTypeAuthorizationService;
use Illuminate\Http\Request;
use App\LeaveType\LeaveTypeRequestService;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LeaveTypeController extends Controller
{
    /**
     * @var \App\LeaveType\LeaveTypeRequestService
     */
    private $requestService;

    /**
     * @var \App\LeaveType\LeaveTypeAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(
        LeaveTypeRequestService $requestService,
        LeaveTypeAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/leave_type/{id}",
     *     summary="Get Leave Type",
     *     description="Get Leave Type Details

Authorization Scope : **view.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Leave Type ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(Request $request, $id)
    {
        $response = $this->requestService->get($id);
        $leaveTypeData = json_decode($response->getData());

        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);

            $isAuthorized = $authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $leaveTypeData->company_id);
        } else {
            $leaveTypeData->account_id = Company::getAccountId($leaveTypeData->company_id);

            $isAuthorized = $this->authorizationService->authorizeGet(
                $leaveTypeData,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_type/",
     *     summary="Create Company Leave Type",
     *     description="Create Company Leave Type

Authorization Scope : **create.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         type="string",
     *         in="formData",
     *         description="Leave Type Name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="abbreviation",
     *         type="string",
     *         in="formData",
     *         description="Abbreviation",
     *         required=false,
     *     ),
     *     @SWG\Parameter(
     *         name="leave_credit_required",
     *         type="boolean",
     *         in="formData",
     *         description="Requires Leave Credit",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="payable",
     *         type="boolean",
     *         in="formData",
     *         description="Payable",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="documents",
     *         type="string",
     *         in="formData",
     *         description="Required documents",
     *         required=false,
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function create(Request $request)
    {
        // authorize
        $isAuthorized = false;
        $createdBy = $request->attributes->get('user');
        $inputs = $request->all();

        $validator = app('validator')->make(
            $inputs,
            [
                'company_id' => 'required',
                'name' => 'required',
                'leave_credit_required' => 'required',
                'payable' => 'required'
            ]
        );

        if ($validator->fails()) {
            throw new NotAcceptableHttpException(
                'The information you provided is incomplete.' .
                ' Please fill-in values for the new leave type.'
            );
        }

        $leaveTypeData = (object) [
            'account_id' => Company::getAccountId($inputs['company_id']),
            'company_id' => $inputs['company_id']
        ];

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $leaveTypeData,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->create($request->all());
        $responseData = json_decode($response->getData(), true);

        // audit log
        $leaveTypeGetResponse = $this->requestService->get($responseData['id']);
        $leaveTypeData = json_decode($leaveTypeGetResponse->getData(), true);
        $details = [
            'new' => $leaveTypeData,
        ];
        $item = new AuditCacheItem(
            LeaveTypeAuditService::class,
            LeaveTypeAuditService::ACTION_CREATE,
            new AuditUser($createdBy['user_id'], $createdBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/leave_types",
     *     summary="Get all Leave Types",
     *     description="Get all Leave Types within company.
Authorization Scope : **view.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyLeaveTypes(Request $request, $id)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $leaveType = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];

            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveTypes(
                $leaveType,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getCompanyLeaveTypes($id);
        $leaveTypeData = json_decode($response->getData())->data;

        if (empty($leaveTypeData)) {
            return $response;
        }
        
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/leave_type/is_name_available",
     *     summary="Is Leave Type name available",
     *     description="Check availability of Leave Type name with the given company",
     *     tags={"leave_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         description="Leave Type Name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_type_id",
     *         in="formData",
     *         description="Leave Type Id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function isNameAvailable($id, Request $request)
    {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $id);
        } else {
            $leaveType = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];

            $isAuthorized = $this->authorizationService->authorizeIsNameAvailable(
                $leaveType,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->isNameAvailable($id, $request->all());
    }

    /**
     * @SWG\Put(
     *     path="/leave_type/{id}",
     *     summary="Update Leave Type",
     *     description="Update Leave Type
Authorization Scope : **edit.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Leave Type Id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         type="string",
     *         in="formData",
     *         description="Leave Type Name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="abbreviation",
     *         type="string",
     *         in="formData",
     *         description="Abbreviation",
     *         required=false,
     *     ),
     *     @SWG\Parameter(
     *         name="leave_credit_required",
     *         type="boolean",
     *         in="formData",
     *         description="Requires Leave Credit",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="payable",
     *         type="boolean",
     *         in="formData",
     *         description="Payable",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="documents",
     *         type="string",
     *         in="formData",
     *         description="Required documents",
     *         required=false,
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        // authorize
        $inputs = $request->all();
        $updatedBy = $request->attributes->get('user');
        $response = $this->requestService->get($id);
        $oldLeaveTypeData = json_decode($response->getData());
        $oldLeaveTypeData->account_id = Company::getAccountId($inputs['company_id']);

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAllAuthorized([
                    AuthzDataScope::SCOPE_COMPANY => $inputs['company_id'],
                    AuthzDataScope::SCOPE_COMPANY => $oldLeaveTypeData->company_id,
                ]);
        } else {
            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $oldLeaveTypeData,
                $updatedBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $updateResponse = $this->requestService->update($inputs, $id);

        // if there are validation errors in the update request, display these errors
        if ($updateResponse->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $updateResponse;
        }

        $getResponse = $this->requestService->get($id);
        $oldLeaveTypeData = json_decode($response->getData(), true);
        $newLeaveTypeData = json_decode($getResponse->getData(), true);
        $details = [
            'old' => $oldLeaveTypeData,
            'new' => $newLeaveTypeData,
        ];
        $item = new AuditCacheItem(
            LeaveTypeAuditService::class,
            LeaveTypeAuditService::ACTION_UPDATE,
            new AuditUser($updatedBy['user_id'], $updatedBy['account_id']),
            $details
        );
        $this->auditService->queue($item);

        return $updateResponse;
    }

    /**
     * @SWG\Delete(
     *     path="/leave_type/bulk_delete",
     *     summary="Delete multiple Leave Types",
     *     description="Delete multiple Leave Types
Authorization Scope : **delete.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_type_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Type Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found.",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        $isAuthorized = false;
        $inputs = $request->all();
        $deletedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $inputs['company_id']);
        } else {
            $leaveType = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];

            $isAuthorized = $this->authorizationService->authorizeDelete(
                $leaveType,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->bulkDelete($inputs);
        // audit log
        if ($response->isSuccessful()) {
            foreach ($request->input('leave_type_ids', []) as $id) {
                $item = new AuditCacheItem(
                    LeaveTypeAuditService::class,
                    LeaveTypeAuditService::ACTION_DELETE,
                    new AuditUser($deletedBy['user_id'], $deletedBy['account_id']),
                    [
                        'old' => [
                            'id' => $id,
                            'company_id' => $request->get('company_id')
                        ]
                    ]
                );
                $this->auditService->queue($item);
            }
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/leave_type/check_in_use",
     *     summary="Are Leave Types in use",
     *     description="Check are Leave Types in use
Authorization Scope : **view.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="leave_types_ids[]",
     *         type="array",
     *         in="formData",
     *         description="Leave Types Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function checkInUse(Request $request)
    {
        $inputs = $request->all();
        $user = $request->attributes->get('user');
        if (!isset($inputs['company_id'])) {
            $this->invalidRequestError('Company ID should not be empty.');
        }

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $inputs['company_id']
            );
        } else {
            $leaveTypeData = (object) [
                'account_id' => Company::getAccountId($inputs['company_id']),
                'company_id' => $inputs['company_id']
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveTypes($leaveTypeData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->checkInUse($inputs);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/leave_types/search",
     *     summary="Search all Leave Types",
     *     description="Search all Leave Types by name within company.
Authorization Scope : **view.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="term",
     *         in="query",
     *         description="Search term by name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="credit_required",
     *         in="query",
     *         description="Filter by credit required",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Number of results to return by search. Default is 10.",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function searchLeaveTypes($id, Request $request)
    {
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $id
            );
        } else {
            $leaveTypeData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveTypes($leaveTypeData, $user);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $queryString = $request->getQueryString() ?? '';

        return $this->requestService->searchLeaveTypes($id, $queryString);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/employee/{employeeId}/leave_types",
     *     summary="Get all Leave Types Within Company for employee",
     *     description="Get all Leave Types Within Company for employee.
Authorization Scope : **view.leave_type**",
     *     tags={"leave_type"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="Employee ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getLeaveTypesWithinCompanyForEmployee(
        $id,
        $employeeId,
        Request $request,
        EmployeeRequestService $employeeRequestService
    ) {
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $employeeDataResponse = $employeeRequestService->getEmployee($employeeId);
            $employee = json_decode($employeeDataResponse->getData(), true);
            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => [$id, Arr::get($employee, 'company_id')],
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            $leaveType = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGetCompanyLeaveTypes(
                $leaveType,
                $request->attributes->get('user')
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->getEntitledLeaveTypes($employeeId);

        return $response;
    }
}
