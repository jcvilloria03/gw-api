<?php

namespace App\Http\Controllers;

use App\AnnualEarning\AnnualEarningUploadTask;
use App\AnnualEarning\AnnualEarningUploadTaskException;
use App\CSV\CsvValidator;
use App\Facades\Company;
use App\AnnualEarning\AnnualEarningRequestService;
use App\AnnualEarning\AnnualEarningAuthorizationService;
use App\Employee\EmployeeRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Storage\UploadService;
use App\Jobs\JobsRequestService;
use Symfony\Component\HttpFoundation\Response;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class AnnualEarningController extends Controller
{
    use AuditTrailTrait;

    /*
     * App\AnnualEarning\AnnualEarningRequestService
     */
    protected $requestService;

    /*
     * App\AnnualEarning\AnnualEarningAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var CsvValidator
     */
    protected $csvValidator;

    /**
     * @var uploadService
     */
    protected $uploadService;

    public function __construct(
        AnnualEarningRequestService $requestService,
        AnnualEarningAuthorizationService $authorizationService,
        CsvValidator $csvValidator,
        UploadService $uploadService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->csvValidator = $csvValidator;
        $this->uploadService = $uploadService;
    }
    /**
     * @SWG\Post(
     *     path="/company/{id}/annual_earning/set",
     *     summary="Set Annual Earning",
     *     description="Set Annual Earning,
Authorization Scope : **create.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="annual_earning",
     *         in="body",
     *         description="Set Annual Earnings",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/annualEarningSetRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="annualEarningSetRequestItem",
     *     required={"employee_id", "year", "prev_employer", "current_minimum_wage_earner"},
     *     @SWG\Property(
     *         property="employee_id",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="year",
     *         type="integer"
     *     ),
     *     @SWG\Property(
     *         property="current_basic_salary_or_statutory_minimum_wage",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_holiday_pay_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_overtime_pay_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_nt_differential_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_de_minimis_benefits",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_employee_sss_contribution",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_employee_philhealth_contribution",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_employee_pagibig_contribution",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_non_taxable_salaries_and_other_forms_of_compensation",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_13th_month_pay_and_other_benefits",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_basic_salary",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_others_supplementary",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_others_regular",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_taxable_commission",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_taxable_ot_pay",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_premium_paid_on_health_and_hospital_insurance",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="current_tax_withheld",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_employer_tin",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="prev_employer_business_name",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="prev_employer_business_address",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="prev_employer_zip_code",
     *         type="string"
     *     ),
     *     @SWG\Property(
     *         property="prev_gross_compensation_income",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_basic_salary_or_statutory_minimum_wage",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_holiday_pay_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_overtime_pay_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_nt_differential_mwe",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_non_taxable_13th_month_pay_and_other_benefits",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_de_minimis_benefits",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_sss_gsis_phic_pagibig_contribution",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_non_taxable_salaries_and_other_forms_of_compensation",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_total_non_taxable_compensation_income",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_taxable_13th_month_pay_and_other_benefits",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_taxable_salaries_and_other_forms_of_compensation",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_total_taxable_compensation_income",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_premium_paid_on_health_and_hospital_insurance",
     *         type="number"
     *     ),
     *     @SWG\Property(
     *         property="prev_tax_withheld",
     *         type="number"
     *     )
     * )
     */
    public function setAnnualEarning(Request $request, $id, EmployeeRequestService $employeeRequestService)
    {

        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();

        $createdBy = $request->attributes->get('user');
        $accountId = $id ? Company::getAccountId($id) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled && !empty($inputs['employee_id'])) {
            $employeeData = json_decode(
                $employeeRequestService->getEmployee($inputs['employee_id'])->getData(),
                true
            );
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employeeData, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employeeData, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employeeData, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employeeData, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employeeData, 'payroll.payroll_group_id'),
            ]);
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $id,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // For audit trail
        try {
            $oldDataResponse = $this->requestService->getAnnualEarningsByEmployee($id, $inputs['employee_id']);
            $oldData = json_decode($oldDataResponse->getData(), true);
        } catch (\Exception $e) {
            $oldData = [];
        }

        // call microservice
        $response = $this->requestService->setAnnualEarning($id, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $id, $newData, $oldData);
        }

        return $response;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/annual_earning/group_by_year",
     *     summary="Get annual earnings for company grouped by year",
     *     description="Get annual earnings for company grouped by year,
Authorization Scope : **view.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * )
     */
    public function groupByYear(Request $request, $id)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $accountId = $id ? Company::getAccountId($id) : null;

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $id
            ];
            $isAuthorized = $this->authorizationService->authorizeGet($data, $createdBy);
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAnnualEarningsForCompanyGroupedByYear($id, $authzDataScope);
    }

    /**
     * @SWG\Get(
     *     path="/company/{companyId}/employee/{employeeId}/annual_earning",
     *     summary="Get all annual earnings for employee",
     *     description="Get all annual earnings for employee,
Authorization Scope : **view.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="employeeId",
     *         type="integer",
     *         in="path",
     *         description="Employee ID",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getAllByEmployee(
        Request $request,
        EmployeeRequestService $employeeRequestService,
        int $companyId,
        int $employeeId
    ) {
        $isAuthorized = false;

        if ($this->isAuthzEnabled($request)) {
            $employee = json_decode(
                $employeeRequestService->getEmployee($employeeId)->getData(),
                true
            );

            $isAuthorized = $this->getAuthzDataScope($request)->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId,
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'time_attendance.team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll.payroll_group_id'),
            ]);
        } else {
            // check company exists (will throw exception if company doesn't exist)
            $createdBy = $request->attributes->get('user');
            $accountId = $companyId ? Company::getAccountId($companyId) : null;

            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAnnualEarningsByEmployee($companyId, $employeeId);
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/annual_earning/{annualEarningId}",
     *     summary="Get Annual Earning",
     *     description="Get Annual Earning,
Authorization Scope : **view.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="annualEarningId",
     *         in="path",
     *         description="Annual Earning Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Annual earning not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function getAnnualEarning(
        Request $request,
        $id,
        $annualEarningId
    ) {
        $isAuthorized = false;
        $authzDataScope = null;

        if ($this->isAuthzEnabled($request)) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $id
            ]);
        } else {
            // check company exists (will throw exception if company doesn't exist)
            $createdBy = $request->attributes->get('user');
            $accountId = $id ? Company::getAccountId($id) : null;

            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $id
            ];

            $isAuthorized = $this->authorizationService->authorizeGet($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->get($annualEarningId, $authzDataScope);
    }

    /**
     * @SWG\Post(
     *     path="/annual_earning/upload",
     *     summary="Upload Annual Earning",
     *     description="Upload annual earning
    Authorization Scope : **create.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="CSV",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "annual_earnings_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Uploaded file does not have a CSV file extension."
     *              }
     *         }
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            $this->invalidRequestError('The uploaded file is invalid.');
        }

        // try {
        //     $this->csvValidator->validate($file);
        // } catch (CsvValidatorException $e) {
        //     $this->invalidRequestError($e->getMessage());
        // }

        try {
            // $uploadTask = \App::make(OtherIncomeUploadTask::class);

            // $uploadTask->create($companyId);
            //$s3Key = $this->uploadService->saveFile($file->getPathName());
            $s3Key = $this->uploadService->saveFile($file, $createdBy, 'public-read');
            $s3Bucket = $this->uploadService->getS3Bucket();
            //call Payroll API to create processing job
            $response = $this->requestService->processFileUpload($companyId, $s3Bucket, $s3Key, $authzDataScope);
            $responseData = json_decode($response->getOriginalContent(), true)["data"];
            $jobId = $responseData['id'];

            // trigger audit trail
            $this->audit($request, $companyId, $responseData);

            return response()->json([
                'id' => $jobId,
                'job_id' => $jobId
            ]);
        } catch (S3Exception $e) {
            $this->invalidRequestError('Error uploading file to S3');
        }
    }

    /**
     * getCompanyAccountId
     */
    public function getCompanyAccountId($companyId)
    {
        return Company::getAccountId($companyId);
    }


    /**
     * @SWG\Get(
     *     path="/annual_earning/upload/status",
     *     summary="Get Job Status",
     *     description="Get Annual earning Upload Status
    Authorization Scope : **create.annual_earnings**",
     *     tags={"annual_earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string"),
     *             @SWG\Property(property="errors", type="array", items={
     *                  "type"="array",
     *                  "items"={"type"="string"}
     *             }),
     *         ),
     *         examples={
     *              {
     *                  "application/json": {
     *                      "status": "validating",
     *                      "errors": null
     *                  }
     *              },
     *              {
     *                  "application/json": {
     *                      "status": "validation_failed",
     *                      "errors": {
     *                          "1": {
     *                              "Annual earning is invalid"
     *                          }
     *                      }
     *                  }
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Invalid Upload Step."
     *              }
     *         }
     *     )
     *     )
     * )
     */
    public function uploadStatus(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $companyId = $request->input('company_id');
        $createdBy = $request->attributes->get('user');
        $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        $jobId = $request->input('job_id');
        //return $this->requestService->getFileUploadStatus($companyId, $type, $jobId);
        $response = $this->requestService->getFileUploadStatus($companyId, $jobId);
        //TODO:: make this just return JM-API response
        if ($response->status() == Response::HTTP_NOT_FOUND) {
            $this->notFoundError('Job not found');
        }

        $responseBody = json_decode($response->getOriginalContent(), true);
        $data = $responseBody['data'];
        $attributes = $data['attributes'];
        $status = $attributes['status'];
        $included = $responseBody['included'] ?? null;
        $errors = array();
        $results = array();
        if ($included != null) {
            foreach ($included as $include) {
                if ($include["type"] == "job-errors") {
                    array_push($errors, $include);
                } else {
                    array_push($results, $include["attributes"]);
                }
            }
        }

        //TODO: temporary matching
        $returnedErrors = null;
        $returnedStatus = "saved";
        $errorName = null;
        $error = null;
        if (empty($errors) && $status == 'PENDING') {
            $returnedStatus = "validating";
        } else if (!empty($errors) && $status == 'FINISHED') {
            $error = $errors[0]["attributes"]["description"];
            $errorName = $error["name"];
        }

        if ($errorName == "task-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = (object) ["0" =>  $error["errors"]];
        } else if ($errorName == "validation-errors") {
            $returnedStatus = "validation_failed";
            $returnedErrors = JobsRequestService::flattenErrorData($error["errors"]);
        }

        return response()->json([   //TODO:: replace this with JM-API raw response
            'status' => $returnedStatus,
            'errors' => $returnedErrors,
            'results' => $results
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/annual_earning/upload/save",
     *     summary="Save Annual Earnings",
     *     description="Saves Annual Earnings from Previously Uploaded CSV
    Authorization Scope : **create.annual_earnings**",
     *     deprecated=true,
     *     tags={"annual_earning"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="formData",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="formData",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="id", type="string", description="Job ID of upload")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "id": "annual_earnings_upload:1:59c9eb5fc6201"
     *              }
     *         }
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="message", type="string")
     *         ),
     *         examples={
     *              "application/json": {
     *                  "message": "Annual earnings needs to be validated successfully first."
     *              }
     *         }
     *     )
     * )
     */
    public function uploadSave(Request $request)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $accountId = $companyId ? Company::getAccountId($companyId) : null;
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(AnnualEarningUploadTask::class);
            $uploadTask->create($companyId, $jobId);
            $uploadTask->updateSaveStatus(AnnualEarningUploadTask::STATUS_SAVE_QUEUED);
            $uploadTask->setUserId($createdBy['user_id']);
        } catch (AnnualEarningUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        \Amqp::publish(config('queues.annual_earning_write_queue'), $message);

        // trigger audit trail
        $newData = array_merge($details, $request->all());
        $this->audit($request, $companyId, $newData);

        return response()->json([
            'id' => $uploadTask->getId()
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/annual_earning/upload/preview",
     *     summary="Get Annual Earnings Preview",
     *     description="Get Annual Earnings Upload Preview
    Authorization Scope : **create.annual_earnings**",
     *     tags={"annual_earning"},
     *     deprecated=true,
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="query",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="job_id",
     *         in="query",
     *         description="Job ID",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Company or Job not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function uploadPreview(Request $request)
    {
        //check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $companyId = $request->input('company_id');
        $accountId = $companyId ? Company::getAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }

        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = $request->input('job_id');
            $uploadTask = \App::make(AnnualEarningUploadTask::class);
            $uploadTask->create($companyId, $jobId);
        } catch (AnnualEarningUploadTaskException $e) {
            $this->invalidRequestError($e->getMessage());
        }

        $response = $this->requestService->getUploadPreview($jobId);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/annual_earning/download",
     *     summary="Download Annual Earnings CSV",
     *     description="Download Annual Earnings CSV,
    Authorization Scope : **view.annual_earnings**",
     *     tags={"annual_earning"},
     *     deprecated=true,
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/csv"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         type="integer",
     *         in="path",
     *         description="Company ID",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="ids[]",
     *         type="array",
     *         in="formData",
     *         description="Annual Earnings Ids",
     *         required=true,
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     )
     * )
     */
    public function downloadCsv(Request $request, $id)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        $accountId = $id ? Company::getAccountId($id) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $id
        ];

        // authorize
        if (!$this->authorizationService->authorizeGet($data, $createdBy)) {
            $this->response()->errorUnauthorized();
        }

        $annualEarningIds = $request->input('ids') ?: [];
        return $this->requestService->downloadCsv(
            (int) $id,
            $annualEarningIds
        );
    }


        /**
     * @SWG\Delete(
     *     path="/company/{companyId}/annual_earning/{id}",
     *     summary="Delete annual earning",
     *     description="Delete annual earning",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Annual Earning Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     )
     * )
     */
    public function deleteAnnualEarning(Request $request, int $companyId, int $annualEarningId)
    {
        // check company exists (will throw exception if company doesn't exist)
        $createdBy = $request->attributes->get('user');
        //$companyId = $request->input('company_id');
        $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;

        $data = (object) [
            'account_id' => $accountId,
            'company_id' => $companyId,
        ];

        // authorize
        if (
        !$this->authorizationService->authorizeCreate(
            $data,
            $createdBy
        )
        ) {
            $this->response()->errorUnauthorized();
        }
        $response = $this->requestService->deleteAnnualEarning($companyId, [$annualEarningId]);

        if ($response->isSuccessful()) {
            // trigger audit trail
            $oldData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, [], $oldData);
        }

        return $response;
    }

        /**
     * @SWG\Post(
     *     path="/company/{companyId}/annual_earning/bulk_delete",
     *     summary="Delete annual earnings",
     *     description="Delete annual earnings",
     *     tags={"annual_earning"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="companyId",
     *         in="path",
     *         description="Company Id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Annual earning delete list",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/bulkDeleteAnnualEarningsRequest")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Request not found",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="bulkDeleteAnnualEarningsRequest",
     *     required={"ids"},
     *     @SWG\Property(
     *         property="ids",
     *         type="array",
     *         @SWG\Items(type="integer")
     *     )
     * )
     */
    public function bulkDeleteAnnualEarning(Request $request, int $companyId)
    {

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $this->getAuthzDataScope($request);
            $isAuthorized = $authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => $companyId
            ]);
        } else {
            $accountId = $companyId ? $this->getCompanyAccountId($companyId) : null;
            $createdBy = $request->attributes->get('user');
            $data = (object) [
                'account_id' => $accountId,
                'company_id' => $companyId,
            ];
            $isAuthorized = $this->authorizationService->authorizeCreate(
                $data,
                $createdBy
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        $inputs = $request->all();
        $ids = $inputs['ids'] ?? [];
        $response = $this->requestService->deleteAnnualEarning($companyId, $ids, $authzDataScope);

        if ($response->isSuccessful()) {
            foreach ($ids as $id) {
                // trigger audit trail
                $oldData = [
                    'id' => $id,
                    'company_id' => $companyId,
                ];
                $this->audit($request, $companyId, [], $oldData);
            }
        }

        return $response;
    }


}
