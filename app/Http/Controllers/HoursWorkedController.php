<?php

namespace App\Http\Controllers;

use App\Audit\AuditService;
use App\Employee\EmployeeAuthz;
use App\Facades\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;
use App\HoursWorked\HoursWorkedAuthorizationService;
use App\HoursWorked\HoursWorkedRequestService;
use App\HoursWorked\HoursWorkedAuditService;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use App\Attendance\HoursWorkedAttendanceTriggerService;
use App\Authz\AuthzDataScope;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class HoursWorkedController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\HoursWorked\HoursWorkedRequestService
     */
    private $requestService;

    /**
     * @var \App\HoursWorked\HoursWorkedAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\Attendance\HoursWorkedAttendanceTriggerService
     */
    private $hoursWorkedTrigger;

    public function __construct(
        HoursWorkedRequestService $requestService,
        HoursWorkedAuthorizationService $authorizationService,
        HoursWorkedAttendanceTriggerService $hoursWorkedTrigger,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
        $this->hoursWorkedTrigger = $hoursWorkedTrigger;
    }

    /**
     * @SWG\Get(
     *     path="/company/{id}/hours_worked",
     *     summary="Get employees hours worked within company",
     *     description="Get employees hours worked within company
Authorization Scope : **view.hours_worked**",
     *     tags={"attendance"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="employees_ids[]",
     *         in="query",
     *         description="Employees IDs",
     *         type="array",
     *         @SWG\Items(type="integer"),
     *         collectionFormat="multi"
     *     ),
     *     @SWG\Parameter(
     *         name="start_date",
     *         in="query",
     *         description="Date range start. A inclusive lower date boundary for filtering in format YYYY-MM-DD",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="end_date",
     *         in="query",
     *         description="Date range end. A inclusive upper date boundary for filtering in format YYYY-MM-DD.",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getCompanyHoursWorked(Request $request, $id)
    {
        $hoursWorked = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        if (!$this->authorizationService->authorizeGetHoursWorked(
            $hoursWorked,
            $request->attributes->get('user')
        )) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getCompanyHoursWorked($id, $request->getQueryString() ?? '');
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/hours_worked/bulk_create_or_update_or_delete",
     *     summary="Create or Update Or Delete Employee's Hours Worked Within Company",
     *     description="Create or Update Or Delete Employee's Hours Worked Within Company
Authorization Scope : **edit.hours_worked**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/employeesHoursWorked"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="employeesHoursWorked",
     *     @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/employeeHoursWorked")),
     * ),
     * @SWG\Definition(
     *     definition="employeeHoursWorked",
     *     required={"employee_id", "date", "shift_id", "hours_worked"},
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer",
     *         default="1",
     *         description="Employee ID"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-10-10",
     *         description="Hours worked on date in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="shift_id",
     *         type="integer",
     *         default="1",
     *         description="Shift ID, Default Schedule ID or Rest Day. Examples: 1, 2, ds_1, ds_2, rd"
     *     ),
     *     @SWG\Property(
     *         property="hours_worked",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/singleHoursWorked"),
     *         collectionFormat="multi"
     *     )
     * )
     */
    public function bulkCreateOrUpdateOrDelete(Request $request, $id)
    {
        $user = $request->attributes->get('user');
        $hoursWorkedData = (object) [
            'account_id' => Company::getAccountId($id),
            'company_id' => $id
        ];

        // authorize
        if (!$this->authorizationService->authorizeBulkCreateOrUpdateOrDelete($hoursWorkedData, $user)) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->bulkCreateOrUpdateOrDelete($request->all(), $id);
        $responseData = json_decode($response->getData(), true);
        $responseData = $this->hoursWorkedTrigger->calculateAttendanceRecords($request->all(), $id);
        $response = response()->json($responseData);

        // log created or updated or deleted hours worked
        $this->logHoursWorkedAction($responseData, $user, $id);

        return $response;
    }

    /**
     * @SWG\Post(
     *     path="/company/{id}/hours_worked/leaves/bulk_create_or_update_or_delete",
     *     summary="Create or Update Or Delete Employee's Leaves Within Company",
     *     description="Create or Update Or Delete Employee's Leaves Within Company
Authorization Scope : **edit.hours_worked**",
     *     tags={"attendance"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/employeesLeaves"),
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="employeesLeaves",
     *     @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/employeeLeaves")),
     * ),
     * @SWG\Definition(
     *     definition="employeeLeaves",
     *     required={"employee_id", "date", "shift_id", "leaves"},
     *     @SWG\Property(
     *         property="employee_id",
     *         type="integer",
     *         default="1",
     *         description="Employee ID"
     *     ),
     *     @SWG\Property(
     *         property="date",
     *         type="string",
     *         default="2018-10-10",
     *         description="Hours worked on date in format YYYY-MM-DD"
     *     ),
     *     @SWG\Property(
     *         property="shift_id",
     *         type="integer",
     *         default="1",
     *         description="Shift ID, Default Schedule ID or Rest Day. Examples: 1, 2, ds_1, ds_2, rd"
     *     ),
     *     @SWG\Property(
     *         property="leaves",
     *         type="array",
     *         @SWG\Items(ref="#/definitions/singleLeave"),
     *         collectionFormat="multi"
     *     )
     * ),
     * @SWG\Definition(
     *     definition="singleLeave",
     *     required={"type","leave_type_id", "start_datetime", "end_datetime"},
     *     @SWG\Property(
     *         property="type",
     *         type="string",
     *         default="paid_leave",
     *         description="Hours worked type. Possible values: paid_leave, unpaid_leave"
     *     ),
     *     @SWG\Property(
     *         property="leave_type_id",
     *         type="integer",
     *         description="Leave type ID"
     *     ),
     *     @SWG\Property(
     *         property="start_datetime",
     *         type="string",
     *         description="Start date time of the leave interval in format YYYY-MM-DD HH:mm"
     *     ),
     *     @SWG\Property(
     *         property="end_datetime",
     *         type="string",
     *         description="End date time of the leave interval in format YYYY-MM-DD HH:mm"
     *     )
     * )
     */
    public function leavesBulkCreateOrUpdateOrDelete(Request $request, EmployeeAuthz $employeeAuthz, $id)
    {
        $inputs = $request->all();
        $authorized = false;
        $user = $request->attributes->get('user');
        if ($this->isAuthzEnabled($request)) {
            $employeeIds = Arr::pluck($inputs['data'] ?? [], 'employee_id');
            $authorized = $employeeAuthz->isAllAuthorizedByIdAndCompanyId(
                $id,
                $employeeIds,
                $this->getAuthzDataScope($request)
            );
        } else {
            $hoursWorkedData = (object) [
                'account_id' => Company::getAccountId($id),
                'company_id' => $id
            ];
            $authorized = $this->authorizationService->authorizeBulkCreateOrUpdateOrDelete($hoursWorkedData, $user);
        }
        if (!$authorized) {
            $this->response()->errorUnauthorized();
        }

        $response = $this->requestService->leavesBulkCreateOrUpdateOrDelete($request->all(), $id);
        $responseData = json_decode($response->getData(), true);
        $responseData = $this->hoursWorkedTrigger->calculateAttendanceRecords($request->all(), $id);
        $response = response()->json($responseData);

        try {
            $this->audit($request, $id, ['Mass Update Leave Hours Worked Trigger' => $responseData]);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $response;
    }

    /**
     * Logs hours worked action types.
     *
     * @param array $responseData
     * @param array $userData
     * @param integer companyId
     * @param array $actions default values ['created', 'updated', 'deleted']
     */
    private function logHoursWorkedAction(
        array $responseData,
        array $userData,
        int $companyId,
        array $actions = HoursWorkedAuditService::ACTIONS_CREATE_OR_UPDATE_OR_DELETE
    ) {
        foreach (array_only($responseData, $actions) as $key => $hoursWorkedData) {
            foreach ($hoursWorkedData as $hoursWorked) {
                $hoursWorked['company_id'] = $companyId;
                $action = HoursWorkedAuditService::ACTIONS[$key];

                $details = $action !== HoursWorkedAuditService::ACTION_DELETE
                    ? ['new' => $hoursWorked]
                    : ['old' => $hoursWorked];

                if ($key === 'updated') {
                    $details['old'] = array_first($responseData['before_updated'], function ($item) use ($hoursWorked) {
                        return $item['id'] === $hoursWorked['id'];
                    });
                    $details['old']['company_id'] = $hoursWorked['company_id'];
                }

                $item = new AuditCacheItem(
                    HoursWorkedAuditService::class,
                    $action,
                    new AuditUser($userData['user_id'], $userData['account_id']),
                    $details
                );

                $this->auditService->queue($item);
            }
        }
    }

    /**
     * @SWG\Post(
     *     path="/company/{company_id}/hours_worked/leaves",
     *     summary="Get attendance leaves",
     *     description="Get attendance leaves
Authorization Scope : **edit.hours_worked**",
     *     tags={"attendance"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="company_id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         format="date",
     *         description="Leaves on date in format YYYY-MM-DD",
     *     ),
     *     @SWG\Parameter(
     *         name="employee_id",
     *         in="formData",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getLeaves(Request $request, $companyId)
    {
        $user = $request->attributes->get('user');

        $authzEnabled = $request->attributes->get('authz_enabled');
        $isAuthorized = false;
        if ($authzEnabled) {
            $authzDataScope = $request->attributes->get('authz_data_scope');
            $isAuthorized = $authzDataScope->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );
        } else {
            $hoursWorkedData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId
            ];
            $isAuthorized = $this->authorizationService->authorizeBulkCreateOrUpdateOrDelete(
                $hoursWorkedData,
                $user
            );
        }

        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        return $this->requestService->getAttendanceLeaves($companyId, $request->all());
    }
}
