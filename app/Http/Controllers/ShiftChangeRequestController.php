<?php

namespace App\Http\Controllers;

use App\Audit\AuditService;
use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\ESS\EssShiftChangeRequestRequestService;
use App\Facades\Company;
use App\Workflow\WorkflowRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ShiftChangeRequestController
 *
 * @package App\Http\Controllers
 */
class ShiftChangeRequestController extends Controller
{

    /**
     * @var \App\ESS\EssShiftChangeRequestRequestService
     */
    protected $requestService;

    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    protected $authorizationService;

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * ShiftChangeRequestController constructor.
     *
     * @param EssShiftChangeRequestRequestService $requestService       EssShiftChangeRequestRequestService
     * @param EmployeeRequestAuthorizationService $authorizationService EmployeeRequestAuthorizationService
     * @param AuditService                        $auditService         AuditService
     */
    public function __construct(
        EssShiftChangeRequestRequestService $requestService,
        EmployeeRequestAuthorizationService $authorizationService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
        $this->auditService = $auditService;
    }

    /**
     * @SWG\Get(
     *     path="/shift_change_request/{id}",
     *     summary="Get Shift Change Request",
     *     description="Get Shift Change Request
     Authorization Scope : view.request",
     *     tags={"request"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Shift Change Request ID.",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Request not found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function get(
        Request $request,
        $id
    ) {
        $response = $this->requestService->get($id);
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            return $response;
        }

        $authorized = true;
        $isAuthzEnabled = $this->isAuthzEnabled($request);
        if (!$isAuthzEnabled) {
            $shiftChangeRequest = json_decode($response->getData());
            $companyId = $shiftChangeRequest->request->company_id;

            $accountId = $companyId ? Company::getAccountId($companyId) : null;

            $shiftChangeRequest->account_id = (int)$accountId;
            $shiftChangeRequest->company_id = $companyId;

            $user = $request->attributes->get('user');

            $workflows = $shiftChangeRequest->workflow_levels;

            $authorized = $this->authorizationService->authorizeViewSingleRequest(
                $user,
                $workflows,
                $shiftChangeRequest
            );

            if (!$authorized) {
                $this->response()->errorUnauthorized();
            }
        }

        return $response;
    }
}
