<?php

namespace App\Http\Controllers;

use App\Authz\AuthzDataScope;
use App\Deduction\DeductionAuthorizationService;
use App\Deduction\PhilippineDeductionRequestService;
use App\Employee\EmployeeRequestService;
use App\Facades\Company;
use App\OtherIncome\OtherIncomeRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class PhilippineDeductionController extends Controller
{
    use AuditTrailTrait;

    /**
     * @var \App\Deduction\PhilippineDeductionRequestService
     */
    protected $requestService;

    /**
     * @var \App\Deduction\DeductionAuthorizationService
     */
    private $authorizationService;

    public function __construct(
        PhilippineDeductionRequestService $requestService,
        DeductionAuthorizationService $authorizationService
    ) {
        $this->requestService = $requestService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @SWG\Post(
     *     path="/philippine/company/{id}/deduction/bulk_create",
     *     summary="Assign Multiple Deductions",
     *     description="Assign multiple deductions,
Authorization Scope : **create.deductions**",
     *     tags={"deduction"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true,
     *         description="Salarium Module Map"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Company ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="deductions",
     *         in="body",
     *         description="Create multiple deductions",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deductionsRequestItemCollection")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="Invalid request",
     *     )
     * ),
     * @SWG\Definition(
     *     definition="deductionsRequestItemCollection",
     *     type="array",
     *     @SWG\Items(ref="#/definitions/deductionsRequestItem"),
     *     collectionFormat="multiple"
     * ),
     * @SWG\Definition(
     *     definition="deductionsRequestItem",
     *     @SWG\Property(
     *         property="type_id",
     *         type="integer",
     *         description="Deduction type id",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="amount",
     *         type="number",
     *         description="Deduction amount",
     *         example="100"
     *     ),
     *     @SWG\Property(
     *         property="recurring",
     *         type="boolean",
     *         description="Is deduction recurring",
     *         example="1"
     *     ),
     *     @SWG\Property(
     *         property="frequency",
     *         type="string",
     *         description="Deduction frequency",
     *         example="EVERY_PAY_OF_THE_MONTH"
     *     ),
     *     @SWG\Property(
     *         property="valid_from",
     *         type="string",
     *         format="date",
     *         description="Valid from date",
     *         example="2019-02-22"
     *     ),
     *     @SWG\Property(
     *         property="valid_to",
     *         type="string",
     *         format="date",
     *         description="Valid to date",
     *         example="2019-02-22"
     *     ),
     *     @SWG\Property(
     *         property="recipients",
     *         type="object",
     *             @SWG\Property(
     *              property="employees",
     *              type="array",
     *              items={"type"="integer", "example"="1"}
     *             )
     *     )
     * )
     */
    public function bulkCreate(
        Request $request,
        EmployeeRequestService $employeeService,
        $companyId
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $inputs = $request->all();
        $createdBy = $request->attributes->get('user');
        $isAuthorized = false;
        $errorMesssage = 'Unauthorized';

        if ($this->isAuthzEnabled($request)) {
            $authorizedCompany = $this->getAuthzDataScope($request)->isAuthorized(
                AuthzDataScope::SCOPE_COMPANY,
                $companyId
            );

            $employeeIds = array_flatten($request->json('*.recipients.employees'), 1);

            $employeeResponse = $employeeService->getCompanyEmployeesById($companyId, $employeeIds);
            if ($employeeResponse->getStatusCode() !== Response::HTTP_OK) {
                return $employeeResponse;
            }

            $unAuthorizedEmployees = collect(json_decode($employeeResponse->getData(), true))->reject(
                function ($employee) use ($request) {
                    return $this->getAuthzDataScope($request)->isAllAuthorized([
                        AuthzDataScope::SCOPE_COMPANY => [data_get($employee, 'company_id')],
                        AuthzDataScope::SCOPE_DEPARTMENT => data_get($employee, 'department_id'),
                        AuthzDataScope::SCOPE_POSITION => data_get($employee, 'position_id'),
                        AuthzDataScope::SCOPE_LOCATION => data_get($employee, 'location_id'),
                        AuthzDataScope::SCOPE_TEAM => data_get($employee, 'team_id'),
                        AuthzDataScope::SCOPE_PAYROLL_GROUP => data_get($employee, 'payroll_group_id'),
                    ]);
                }
            );

            if (!$unAuthorizedEmployees->isEmpty()) {
                $errorMesssage = 'Unauthorized: Unable to process employees:[' .
                    implode(',', $unAuthorizedEmployees->pluck('id')->all()) . ']';
            } else {
                $isAuthorized = $authorizedCompany && $unAuthorizedEmployees->isEmpty();
            }
        } else {
            $data = (object) [
                'account_id' => Company::getAccountId($companyId),
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeCreate($data, $createdBy);
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized($errorMesssage);
        }

        // call microservice
        $response = $this->requestService->bulkCreate($companyId, $inputs);

        // if there are validation errors in the create request, display these errors
        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            return $response;
        }

        $deductionIds = json_decode($response->getData(), true);
        $deductions = collect($inputs)->map(function ($deduction, $index) use ($deductionIds) {
            array_set($deduction, 'id', array_get($deductionIds, $index));
            return $deduction;
        })->toArray();

        if ($response->isSuccessful()) {
            foreach ($deductions as $deduction) {
                // trigger audit trail
                $this->audit($request, $companyId, $deduction);
            }
        }

        return $response;
    }

    /**
     * @SWG\Patch(
     *     path="/philippine/deduction/{id}",
     *     summary="Update the deduction",
     *     description="Update the deduction
Authorization Scope : **edit.deductions**",
     *     tags={"deduction"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="integer",
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         name="deduction",
     *         in="body",
     *         description="Request body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/deductionsRequestItem")
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="Successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
     *         description="Not Found",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
     *         description="Unauthorized",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function update(
        Request $request,
        OtherIncomeRequestService $otherIncomeRequestService,
        $id
    ) {
        if (!json_decode($request->getContent())) {
            $this->invalidRequestError('JSON body is invalid.');
        }

        $deductionResponse = $otherIncomeRequestService->get($id);
        if (!$deductionResponse->isSuccessful()) {
            return $deductionResponse;
        }
        $deductionData = json_decode($deductionResponse->getData(), true);
        $companyId = array_get($deductionData, 'company_id');
        $updatedBy = $request->attributes->get('user');

        if ($this->isAuthzEnabled($request)) {
            $isAuthorized = $this->getAuthzDataScope($request)
                ->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId);
        } else {
            $authData = (object) [
                'account_id' => $companyId ? Company::getAccountId($companyId) : null,
                'company_id' => $companyId,
            ];

            $isAuthorized = $this->authorizationService->authorizeUpdate(
                $authData,
                $updatedBy
            );
        }

        // authorize
        if (!$isAuthorized) {
            $this->response()->errorUnauthorized();
        }

        // call microservice
        $response = $this->requestService->update($id, $request->all(), $this->getAuthzDataScope($request));
        if ($response->isSuccessful()) {
            // trigger audit trail
            $newData = json_decode($response->getData(), true);
            $this->audit($request, $companyId, $newData, $deductionData, true);
        }
        return $response;
    }
}
