<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RestDay\RestDayRequestService;
use App\Http\Controllers\EssBaseController;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class EssRestDayController extends EssBaseController
{
    /**
     * @var \App\RestDay\RestDayRequestService
     */
    protected $restDayRequestService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    protected $employeeRequestAuthorizationService;

    public function __construct(
        RestDayRequestService $restDayRequestService,
        EssEmployeeRequestAuthorizationService $employeeRequestAuthorizationService
    ) {
        $this->restDayRequestService = $restDayRequestService;
        $this->employeeRequestAuthorizationService = $employeeRequestAuthorizationService;
    }

    /**
     * @SWG\Get(
     *     path="/ess/employee/rest_days",
     *     summary="Get employee rest days",
     *     description="Get all rest days for employee with given ID.
Authorization Scope : **ess.create.request**",
     *     tags={"ess"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         type="string",
     *         name="Authorization",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Parameter(
     *         type="string",
     *         name="X-Authz-Entities",
     *         in="header",
     *         required=true
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_OK,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
     *         description="Invalid request",
     *     )
     * )
     */
    public function getEmployeeRestDays(Request $request)
    {
        $user = $this->getFirstEmployeeUser($request);

        if (
            !$this->isAuthzEnabled($request) &&
            !$this->employeeRequestAuthorizationService->authorizeCreate($user)
        ) {
            $this->response()->errorUnauthorized();
        }

        // Set Key; formula = 'rest_days:employee_id:' + $companyId
        $cacheKey = 'rest_days:employee_id:' . $user['employee_id'];

        // Get leaves from cache, if none, fetch new data and set to cache
        if (Redis::exists($cacheKey)) {
            $cache = Redis::hGetAll($cacheKey);
            $response = $cache['data'];
            return $this->response->array($response);
        } else {
            // Fetch new data from microservice
            $response = $this->restDayRequestService->getEmployeeRestDays($user['employee_id']);

            // Save data to cache
            Redis::hMSet(
                $cacheKey,
                [
                    'data'=>$response->getData(),
                    'status_code'=> $response->getStatusCode()
                ]
            );
            // Set life of cache at midnight
            Redis::expireAt($cacheKey, Carbon::now()->endOfDay()->timestamp);
            return $response;
        }
    }
}
