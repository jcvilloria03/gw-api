<?php

namespace App\Bank;

use App\Common\CommonAuthorizationService;
use Illuminate\Http\JsonResponse;

class CompanyBankAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.disbursement';
    public $createTask = 'create.disbursement';
    public $updateTask = 'edit.disbursement';
    public $deleteTask = 'delete.disbursement';

    public static function generateCompanyModel(JsonResponse $response)
    {
        $model = json_decode($response->getData());
        
        $companyId = 0;

        $accountId = 0;

        if (property_exists($model, 'id')) {
            $companyId = $model->id;
        }
        
        if (property_exists($model, 'account_id')) {
            $accountId = $model->account_id;
        }

        $model->company_id = $companyId;
        $model->account_id = $accountId;

        return $model;
    }
}
