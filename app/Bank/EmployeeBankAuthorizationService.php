<?php

namespace App\Bank;

use App\Common\CommonAuthorizationService;

class EmployeeBankAuthorizationService extends CommonAuthorizationService
{
    public $viewTask   = 'view.payment_method';
    public $createTask = 'create.payment_method';
    public $updateTask = 'edit.payment_method';
    public $deleteTask = 'delete.payment_method';
}
