<?php

namespace App\Bank;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class CompanyBankAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';
    const ACTION_UPDATE = 'update';
    const OBJECT_NAME = 'company_bank';
    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log bank related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }


    /**
     * Log bank creation
     *
     * @param array $cacheItem
     */
    private function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'bank_id' => $data['bank']['id'],
                'bank_name' => $data['bank']['name'],
                'bank_branch' => $data['bank_branch'],
                'account_number' => $data['account_number'],
                'account_type' => $data['account_type'],
                'company_code' => $data['company_code'],
            ]
        ]);
        $this->auditService->log($item);
    }

     /**
     *
     * Log bank deletion
     *
     * @param array $cacheItem
     *
     */
    private function logDelete(array $cacheItem)
    {
        $bankData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $bankData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $bankData->id
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log bank updates
     *
     * @param array $cacheItem
     */
    private function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);
        $this->auditService->log($item);
    }
}
