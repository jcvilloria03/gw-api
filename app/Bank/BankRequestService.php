<?php

namespace App\Bank;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class BankRequestService extends RequestService
{
    /**
     * Call endpoint to fetch list of supported banks
     *
     * @return \Illuminate\Http\JsonResonse
     *
     */
    public function index()
    {
        return $this->send(new Request('GET', '/banks'));
    }

    /**
     * Call endpoint to fetch list of supported banks
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResonse
     *
     */
    public function getCompanyBankAccounts($companyId)
    {
        return $this->send(new Request('GET', "/company/{$companyId}/banks"));
    }

    /**
     * Call endpoint to fetch list the employee's payment methods
     *
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function getEmployeePaymentMethods($employeeId)
    {
        return $this->send(new Request('GET', "/employee/{$employeeId}/payment_methods"));
    }

    /**
     * Add Company Bank Account
     *
     * @param int $companyId Company ID
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCompanyBankAccount($companyId, array $attributes)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/bank",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Add Employee Bank Account
     *
     * @param int $employeeId Employee ID
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEmployeeBankAccount($employeeId, array $attributes)
    {
        $request = new Request(
            'POST',
            "/employee/{$employeeId}/bank",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Delete company Banks
     * @param int $companyId Company ID
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCompanyBanks($companyId, array $attributes)
    {
        $request = new Request(
            'DELETE',
            "/company/$companyId/bank",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode(array_merge_recursive($attributes, ['guard' => true]))
        );
        return $this->send($request);
    }

    /**
     * Delete Employee Payment Method
     *
     * @param int $employeePaymentMethodId Employee Payment Method ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEmployeePaymentMethod($employeePaymentMethodId)
    {
        $request = new Request(
            'DELETE',
            "/employee/payment_method/{$employeePaymentMethodId}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'data' => [
                    'guard' => true,
                ],
            ])
        );
        return $this->send($request);
    }

    /**
     * Activate Employee Payment Method
     *
     * @param int $employeeId Employee ID
     * @param int $employeePaymentMethodId Employee Payment Method ID
     * @param array $attributes
     * @return \Illuminate\Http\JsonResponse
     */
    public function activatePaymentMethod($employeeId, $employeePaymentMethodId, $attributes)
    {
        $request = new Request(
            'PATCH',
            "/employee/{$employeeId}/payment_methods/{$employeePaymentMethodId}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($attributes)
        );
        return $this->send($request);
    }

    /**
     * Delete Multiple Employee Payment Methods
     *
     * @param string $payload Employee Payment Method IDs
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEmployeePaymentMethods(string $payload, int $employeeId)
    {
        $attributes = json_decode($payload, true);

        $request = new Request(
            'DELETE',
            "/employee/$employeeId/payment_method",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode(array_merge_recursive($attributes, ['data' => ['guard' => true]]))
        );

        return $this->send($request);
    }
}
