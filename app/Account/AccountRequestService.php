<?php

namespace App\Account;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\JsonResponse;

class AccountRequestService extends RequestService
{
    /**
     * Call endpoint to fetch Account details
     *
     * @param int $id Account ID
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function get(int $id)
    {
        return $this->send(new Request(
            'POST',
            '/account',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query([
                'id' => $id
            ])
        ));
    }

    /**
     * Call endpoint to fetch Account companies
     *
     * @param int $id Account ID
     * @param AuthzDataScope $authzDataScope Authz Data Scope object
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function getAccountCompanies(int $id, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            '/account/philippine/companies',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query([
                'account_id' => $id
            ])
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to create account and owner
     *
     * @param array $account Account Details
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function signUp(array $account)
    {
        return $this->send(new Request(
            'POST',
            '/account/sign_up',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($account)
        ));
    }

    /**
     * Call endpoint to fetch Account setup details
     *
     * @param int $id Account ID
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function setupProgress(int $id)
    {
        return $this->send(new Request(
            'POST',
            '/account/setup_progress',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query(
                [
                    'id' => $id
                ]
            )
        ));
    }

    /**
     * Call endpoint set Account setup details
     *
     * @param int $id Account ID
     * @param array $data Data to pass to endpoint
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function progress(int $id, array $data)
    {
        $data['id'] = $id;

        return $this->send(new Request(
            'PATCH',
            '/account/progress',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to fetch all system Accounts
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function index($queryParams = [])
    {
        $url = '/accounts';

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams, '', '&', PHP_QUERY_RFC3986);
        }

        return $this->send(new Request('GET', $url));
    }

    public function getAccountDetailsByUserId(int $userId)
    {
        return $this->send(new Request(
            'GET',
            "/user/{$userId}/account"
        ));
    }

    /**
     * Call endpoint to fetch essential data for specific account
     *
     * @param int $accountId
     * @param bool $showNames
     * @return Response
     */
    public function getAccountEssentialData(int $accountId, bool $showNames = false): JsonResponse
    {
        $url = sprintf('/accounts/%d/essential_data?show_names=%d', $accountId, intval($showNames));

        return $this->send(new Request('GET', $url));
    }

    /**
     * Call endpoint to fetch essential data for specific company
     *
     * @param int $accountId
     * @param int $companyId
     * @param bool $showNames
     * @return Response
     */
    public function getCompanyEssentialData(int $accountId, int $companyId, bool $showNames = false): JsonResponse
    {
        $url = sprintf(
            '/accounts/%d/companies/%d/essential_data?show_names=%d',
            $accountId,
            $companyId,
            intval($showNames)
        );

        return $this->send(new Request('GET', $url));
    }

    /**
     * Call endpoint update Account email
     *
     * @param int $id Account ID
     * @param array $data Data to pass to endpoint
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function updateAccount(int $id, array $data)
    {
        $data['account_id'] = $id;

        return $this->send(new Request(
            'PATCH',
            '/account/{$id}',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }
}
