<?php

namespace App\Account;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class AccountAuditService
{
    const OBJECT_NAME = 'account';

    const ACTION_CREATE = 'create';
    const ACTION_SETUP_PROGRESS = 'setup_progress';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Account related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_SETUP_PROGRESS:
                $this->logSetupProgress($cacheItem);
                break;
        }
    }

    /**
     * Log account create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log account setup_progress changes
     *
     * @param array $cacheItem
     */
    public function logSetupProgress(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_SETUP_PROGRESS,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);
        $this->auditService->log($item);
    }
}
