<?php

namespace App\Schedule;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use Illuminate\Support\Facades\Log;

class ScheduleAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const OBJECT_NAME = 'schedule';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var App\Schedule\ScheduleRequestService
     */
    private $requestService;

    public function __construct(
        ScheduleRequestService $requestService,
        AuditService $auditService
    ) {
        $this->requestService = $requestService;
        $this->auditService = $auditService;
    }

    /**
     * Log Schedule related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log Schedule Creation
     *
     * @param array $scheduleIds
     * @param int $userId
     *
     */
    public function logCreatedSchedules(array $scheduleIds, int $userId)
    {
        foreach ($scheduleIds as $scheduleId) {
            try {
                $response = $this->requestService->get($scheduleId);
                $scheduleData = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($scheduleData),
                    'user' => json_encode([
                        'id' => $userId,
                    ]),
                ]);
            } catch (\Exception $e) {
                Log::error($e->getMessage() . " : Could not audit Schedule creation with id = " . $scheduleId);
            }
        }
    }

    /**
     * Log Schedule create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'name' => $data['name']
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Schedule update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);

        $this->auditService->log($item);
    }

     /**
     *
     * Log Schedule Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $schedule = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $schedule->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $schedule->id
            ]
        ]);

        $this->auditService->log($item);
    }
}
