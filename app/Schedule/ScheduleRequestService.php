<?php

namespace App\Schedule;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ScheduleRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get schedule
     *
     * @param int $id Schedule ID
     *
     * @return \Illuminate\Http\JsonResponse Schedule Info
     */
    public function get(int $id, array $authzDataScope = null)
    {
        $url = "/schedule/{$id}";

        if (!empty($authzDataScope)) {
            $url .= "?authz_data_scope=" . json_encode($authzDataScope);
        }

        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request);
    }

    /**
     * Get multiple schedules by ID
     *
     * @param  int    $companyId Company ID
     * @param  int[]  $ids Array of schedule IDs
     * @return array
     */
    public function getMultiple(int $companyId, array $ids)
    {
        if (empty(array_filter($ids))) {
            return [];
        }

        $data = [
            'values' => implode(',', $ids)
        ];

        $request = new Request(
            'POST',
            "/company/{$companyId}/schedules/id",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'];
    }

    /**
     * Call endpoint to get all schedules within company
     *
     * @param int $companyId Company Id
     *
     * @return \Illuminate\Http\JsonResponse List of company schedules
     */
    public function getCompanySchedules(
        int $companyId,
        AuthzDataScope $authzDataScope = null,
        $pageType = null,
        $page = null,
        $perPage = ''
    ) {
        $authzDataScope = (array)$authzDataScope;
        $authzDataScope = json_encode($authzDataScope, true);
        $authzDataScope = str_replace("\u0000*\u0000", "", $authzDataScope);
        $authzDataScope = json_decode($authzDataScope, true);

        $url = "/company/{$companyId}/schedules";

        $url .= "?per_page=" . $perPage;
        if (!empty($authzDataScope)) {
            $url .= "&authz_data_scope=" . json_encode($authzDataScope);
        }

        if (!empty($pageType)) {
            $url .= "&page_type=" . json_encode($pageType);
        }
        if (!empty($page)) {
            $url .= "&page=" . $page;
        }
        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get all schedule IDs within company
     *
     * @param int $companyId Company Id
     *
     * @return \Illuminate\Http\JsonResponse List of company schedule ids
     */
    public function getCompanyScheduleIds(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/schedule_ids"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate csv with schedules
     *
     * @param int   $companyId Company Id
     * @param array $data      filters data
     *
     * @return \Illuminate\Http\JsonResponse Name of generated csv file
     */
    public function generateCsv(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/schedules/generate_csv",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to download generated csv
     *
     * @param int    $companyId Company Id
     * @param string $fileName  csv file name
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadCsv(int $companyId, string $fileName)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/schedules/download/{$fileName}",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create schedule
     *
     * @param array $data schedule information
     *
     * @return \Illuminate\Http\JsonResponse Created Schedule
     */
    public function create(array $data, $dataScope = null)
    {
        $request = new Request(
            'POST',
            "/schedule",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        if ($dataScope instanceof AuthzDataScope) {
            return $this->send($request, $dataScope);
        }

        return $this->send($request);
    }

    /**
     * Call endpoint to update schedule for given id
     *
     * @param int   $id   schedule Id
     * @param array $data schedule informations
     * @param AuthzDataScope $authzDataScope Authz data scope
     *
     * @return \Illuminate\Http\JsonResponse Updated schedule
     */
    public function update(int $id, array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'PUT',
            "/schedule/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to check if schedule name is available
     *
     * @param int   $companyId Company ID
     * @param array $data      schedule information
     *
     * @return \Illuminate\Http\JsonResponse Availability of schedule name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/schedule/is_name_available",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check are schedule in use
     *
     * @param array $data schedules information
     *
     * @return \Illuminate\Http\JsonResponse Count of schedules in use
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            "/schedule/check_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple schedules
     *
     * @param array $data information on schedules to delete
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            "/schedule/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to fetch Data to be saved in Schedule upload job
     *
     * @param string $id Job ID
     *
     * @return json Company information
     *
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/schedule/upload_preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get schedules where employee with given ID is entitled
     *
     * @param int $employeeId Employee ID
     * @param int $companyId  Company ID
     * @param bool $getEndDate  Get last date
     *
     * @return \Illuminate\Http\JsonResponse Employee schedules
     */
    public function getEmployeeSchedules(int $employeeId, int $companyId, bool $getEndDate = false)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/schedules/?company_id={$companyId}&get_end_date={$getEndDate}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to search employee schedules by name
     *
     * @param int    $employeeId Employee ID
     * @param string $query      QueryString
     *
     * @return \Illuminate\Http\JsonResponse Schedule Info
     */
    public function searchEmployeeSchedules(int $employeeId, string $query)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/schedules/search?{$query}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get employee current schedule with the given date
     *
     * @param int $employeeId Employee ID
     * @param int $companyId  Company ID
     * @param bool $date date
     *
     * @return \Illuminate\Http\JsonResponse Employee schedules
     */
    public function getEmployeeCurrentShiftSchedules(
        int $employeeId,
        int $companyId,
        string $date
    ) {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/current-schedules"
            . "?company_id={$companyId}&date={$date}"
        );

        return $this->send($request);
    }
}
