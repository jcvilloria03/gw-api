<?php

namespace App\Schedule;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class ScheduleAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.schedule';
    const CREATE_TASK = 'create.schedule';
    const UPDATE_TASK = 'edit.schedule';
    const DELETE_TASK = 'delete.schedule';

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanySchedules(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $schedule
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $schedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($schedule, $user, self::CREATE_TASK);
    }

    /**
     * Authorize schedule related tasks
     *
     * @param \stdClass $schedule
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $schedule,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($schedule->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $schedule->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($schedule->company_id);
        }

        return false;
    }
}
