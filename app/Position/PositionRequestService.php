<?php

namespace App\Position;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PositionRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get position info
     *
     * @param int $id Position ID
     * @return \Illuminate\Http\JsonResponse Position Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/position/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create position
     *
     * @param array $data position information
     * @return \Illuminate\Http\JsonResponse Created Position
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/position",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create positions in bulk.
     *
     * @param array $data positions information
     * @return \Illuminate\Http\JsonResponse Created Positions
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/position/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create positions in bulk.
     *
     * @param array $data positions information
     * @return \Illuminate\Http\JsonResponse Created Positions
     */
    public function bulkUpdate(array $data)
    {
        $request = new Request(
            'PUT',
            "/position/bulk_update",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if position name is available
     *
     * @param int $companyId Company ID
     * @param array $data position information
     * @return \Illuminate\Http\JsonResponse Availability of position name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/position/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all position within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company position
     */
    public function getCompanyPositions(int $companyId, array $params = [])
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/positions?" . http_build_query($params)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update position for given id
     *
     * @param array $data position informations
     * @param int $id Position Id
     * @return \Illuminate\Http\JsonResponse Updated position
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/position/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to delete position
     *
     * @param int $id position to delete id
     * @return \Illuminate\Http\JsonResponse Deleted position
     */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/position/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        );
        return $this->send($request);
    }
}
