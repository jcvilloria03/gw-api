<?php

namespace App\Position;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class PositionAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.position';
    const CREATE_TASK = 'create.position';
    const UPDATE_TASK = 'edit.position';
    const DELETE_TASK = 'delete.position';

    /**
     * @param \stdClass $position
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $position
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyPositions(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $position
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $position
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $position
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::CREATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $position, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($position, $user, self::DELETE_TASK);
    }

    /**
     * Authorize position related tasks
     *
     * @param \stdClass $position
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $position,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for position's account
            return $accountScope->inScope($position->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as position's account
                return $position->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($position->company_id);
        }

        return false;
    }
}
