<?php

namespace App\Position;

use App\ES\ESIndexQueueService;

class PositionEsIndexQueueService
{
    const TYPE = 'position';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created positions for indexing
     * @param array $positionIds Position Ids to be indexed
     */
    public function queue(array $positionIds)
    {
        $details = [
            'id' => $positionIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
