<?php

namespace App\DeviceManagement;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class DeviceManagementAuditService
{
    const ACTION_SYNC = 'sync';
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const OBJECT_NAME = 'ra08t';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Rank related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_SYNC:
                $this->logSync($cacheItem);
                break;
        }
    }

    /**
     * Log role creation
     *
     * @param array $cacheItem
     */
    public function logSync(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'type' => 'user.ra08_device_sync',
                'user_id' => $data['user_id'],
                'device_ids' => $data['device_ids'],
            ]
        ]);
        $this->auditService->log($item);
    }
}
