<?php

namespace App\DeviceManagement;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;

class DeviceManagementRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    public function getAccountDevice(int $accountId, array $params = [])
    {
        $data = $params;
        $data['filter']['account_id'] = $accountId;
        $request = new Request(
            'GET',
            "/api/face_pass/ra08t/devices?".http_build_query($data)
        );
        return $this->send($request);
    }

    public function getUserDevice(int $id)
    {
        $request = new Request(
            "GET",
            "/api/face_pass/ra08t/users/{$id}"
        );
        return $this->send($request);
    }

    public function searchDeviceByFilter(array $filter)
    {
        $params = [
            "filter" => $filter
        ];

        $request = new Request(
            "GET",
            "/api/face_pass/ra08t/devices?".http_build_query($params)
        );
        return $this->send($request);
    }

    public function registerDevice($accountId, $params)
    {
        $params['account_id'] = $accountId;

        return $this->send(new Request(
            "POST",
            '/api/face_pass/ra08t/devices',
            ['Content-type' => 'application/json'],
            json_encode($params)
        ));
    }

    public function updateDevice($id, $params)
    {
        return $this->send(new Request(
            "PUT",
            "/api/face_pass/ra08t/devices/{$id}",
            ['Content-type' => 'application/json'],
            json_encode($params)
        ));
    }

    /**
     * Call endpoint to Sync FaceID to selected devices
     *
     * @param int $userId User ID
     * @param array $data List of devices to sync
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncFaceIdToDevices($userId, array $data)
    {
        $request = new Request(
            'POST',
            "/api/face_pass/ra08t/users/{$userId}/sync",
            [
                'Content-Type' => 'application/vnd.api+json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to unregister user from device
     *
     * @param int $userId User ID
     * @param array $data List of parameters
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unregisterUserDevice($userId, array $data)
    {
        $request = new Request(
            'DELETE',
            "/api/face_pass/ra08t/users/{$userId}/devices",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to unregister device
     *
     * @param int $deviceID The Device unique ID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unregisterDevice($deviceID)
    {
        $request = new Request(
            'DELETE',
            "/api/face_pass/ra08t/devices/{$deviceID}"
        );

        return $this->send($request);
    }

    /**
     * Register user to device using unlinked device user record
     *
     * @param int $userId User ID
     * @param int $unlinkedDeviceUserId Unlinked Device User ID
     * @param string $name User's full name
     */
    public function registerUser($userId, $unlinkedDeviceUserId, $name)
    {
        $request = new Request(
            'POST',
            "/api/face_pass/ra08t/users",
            [
                'Content-Type' => 'application/vnd.api+json'
            ],
            json_encode([
                'user_id'                 => $userId,
                'unlinked_device_user_id' => $unlinkedDeviceUserId,
                'name'                    => $name
            ])
        );

        return $this->send($request);
    }

    /**
     * Undocumented function
     *
     * @param int $accountId
     * @param array $query
     * @return JsonResponse
     */
    public function getUnlinkedDeviceUsers($accountId, $query = [])
    {
        $request = new Request(
            'GET',
            "/api/face_pass/ra08t/accounts/{$accountId}/unlinked_device_users",
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($query)
        );

        return $this->send($request);
    }

    /**
     * Delete Unlinked Users in an Account
     *
     * @param int $accountId Account ID
     * @param array $data List of Unlinked Users in the Account
     * @return JsonResponse
     */
    public function deleteUnlinkedDeviceUsers($accountId, array $data)
    {
        $request = new Request(
            'DELETE',
            "/api/face_pass/ra08t/accounts/{$accountId}/unlinked_device_users",
            [
                'Content-Type' => 'application/vnd.api+json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }
}
