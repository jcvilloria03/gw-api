<?php

namespace App\BasicPayAdjustment;

use App\Common\CommonAuthorizationService;

class BasicPayAdjustmentAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.basic_pay';
    public $createTask = 'create.basic_pay';
    public $updateTask = 'edit.basic_pay';
    public $deleteTask = 'delete.basic_pay';
}
