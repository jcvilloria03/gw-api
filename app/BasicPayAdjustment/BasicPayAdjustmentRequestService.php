<?php

namespace App\BasicPayAdjustment;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class BasicPayAdjustmentRequestService extends RequestService
{
    /**
     * Call endpoint to create Basic Pay Adjustment
     *
     * @param int $employeeId Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(int $employeeId, array $data)
    {
        return $this->send(new Request(
            'POST',
            "/employee/{$employeeId}/basic_pay_adjustment",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to get Basic Pay Adjustment for employee
     *
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByEmployeeId(int $employeeId)
    {
        return $this->send(new Request(
            'GET',
            "/employee/{$employeeId}/basic_pay_adjustments",
            ['Content-Type' => 'application/x-www-form-urlencoded']
        ));
    }


    /**
     * Call endpoint to get Basic Pay Adjustment
     *
     * @param int $id ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/basic_pay_adjustment/{$id}",
            ['Content-Type' => 'application/x-www-form-urlencoded']
        ));
    }

    /**
     * Call endpoint to update Basic Pay Adjustment
     *
     * @param int $employeeId
     * @param int $basicPayAdjustmentId
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $employeeId, int $basicPayAdjustmentId, array $data)
    {
        return $this->send(new Request(
            'PATCH',
            "/employee/{$employeeId}/basic_pay_adjustment/{$basicPayAdjustmentId}/update",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to delete Basic Pay Adjustment
     *
     * @param int $employeeId
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $employeeId, array $data)
    {
        return $this->send(new Request(
            'DELETE',
            "/employee/{$employeeId}/basic_pay_adjustment/delete",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }
}
