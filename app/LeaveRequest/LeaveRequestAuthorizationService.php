<?php

namespace App\LeaveRequest;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class LeaveRequestAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.leave_request';
    const CREATE_TASK = 'create.leave_request';
    const UPDATE_TASK = 'edit.leave_request';
    const DELETE_TASK = 'delete.leave_request';

    /**
     * @param \stdClass $leaveRequest
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $leaveRequest, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveRequest, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveRequest
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $leaveRequest, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveRequest, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $leaveRequest
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $leaveRequest, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveRequest, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $leaveRequest
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $leaveRequest, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveRequest, $user, self::DELETE_TASK);
    }

    /**
     * Authorize leave Request related tasks
     *
     * @param \stdClass $leaveRequest
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $leaveRequest,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for leave Request account
            return $accountScope->inScope($leaveRequest->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll() || $taskType == self::VIEW_TASK) {
                // check if user's account is same as leave Request's account
                return $leaveRequest->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($leaveRequest->company_id);
        }

        return false;
    }
}
