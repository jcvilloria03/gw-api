<?php

namespace App\LeaveRequest;

use App\Audit\AuditService;
use App\Audit\AuditItem;

class LeaveRequestAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_CREATE_BY_ADMIN = 'create_by_admin';
    const ACTION_BATCH_CREATE_BY_ADMIN = 'batch_create_by_admin';
    const ACTION_UPDATE = 'update';

    const OBJECT_NAME = 'leave_request';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\LeaveRequest\LeaveRequestRequestService
     */
    private $leaveRequestRequestService;

    public function __construct(
        AuditService $auditService,
        LeaveRequestRequestService $leaveRequestRequestService
    ) {
        $this->auditService = $auditService;
        $this->leaveRequestRequestService = $leaveRequestRequestService;
    }

    /**
     *
     * Log Payroll related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_CREATE_BY_ADMIN:
                $this->logCreateByAdmin($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
        }
    }

    /**
     * Log LeaveRequest create
     * @param  array  $cacheItem
     * @return void
     */
    public function logCreate(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $data = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $data['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
            ],
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log LeaveRequest create by admin
     *
     * @param  array  $cacheItem
     * @param boolean $isBatch is batch create
     * @return void
     */
    public function logCreateByAdmin(array $cacheItem, bool $isBatch = false)
    {
        $user = json_decode($cacheItem['user'], true);
        $data = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE_BY_ADMIN : self::ACTION_CREATE_BY_ADMIN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
            ],
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log batch create leave requests
     *
     * @param array $ids leave requests ids
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->leaveRequestRequestService->get($id);
                $leaveRequestData = json_decode($response->getData(), true);

                $this->logCreateByAdmin([
                    'new' => json_encode($leaveRequestData),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit Filed leave request batch create with id = ' . $id);
            }
        }
    }

    /**
     * Log LeaveRequest update
     * @param  array  $cacheItem
     * @return void
     */
    public function logUpdate(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $oldData = json_decode($cacheItem['old'], true);
        $newData = json_decode($cacheItem['new'], true);
        $companyId = json_decode($cacheItem['company_id'], true);

        $item = new AuditItem([
            'company_id' => $companyId,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'old' => $oldData,
                'new' => $newData,
            ],
        ]);

        $this->auditService->log($item);
    }
}
