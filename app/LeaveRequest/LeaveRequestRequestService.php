<?php

namespace App\LeaveRequest;

use App\Authz\AuthzDataScope;
use App\Request\DownloadRequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class LeaveRequestRequestService extends DownloadRequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get company leave requests
     * @param int $companyId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompanyLeaveRequests(
        int $companyId,
        array $data,
        string $query,
        AuthzDataScope $authzDataScope = null
    ) {
        return $this->send(
            new Request(
                'POST',
                "/company/{$companyId}/leave_requests?{$query}",
                [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                http_build_query($data)
            ),
            $authzDataScope
        );
    }

    /**
     * Call endpoint to get leave request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id, bool $includeEmployee = false)
    {
        return $this->send(
            new Request(
                'GET',
                "/leave_request/{$id}?include_employee={$includeEmployee}"
            )
        );
    }

    /**
     * Call endpoint to create leave request
     *
     * @param array $data leave request information
     * @return \Illuminate\Http\JsonResponse Created LeaveRequest
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            '/leave_request',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to create leave request by admin
     *
     * @param array $data leave request information
     * @return \Illuminate\Http\JsonResponse Created LeaveRequest
     */
    public function createByAdmin(array $data)
    {
        $request = new Request(
            'POST',
            '/leave_request/admin',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update leave request by admin
     *
     * @param int $id of LeaveRequest
     * @param array $data leave request information
     * @return \Illuminate\Http\JsonResponse Updated LeaveRequest
     */
    public function update(int $id, array $data)
    {
        $request = new Request(
            'PUT',
            "/leave_request/{$id}/admin",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to calculate leave hours
     *
     * @param array $data leave request information
     * @return \Illuminate\Http\JsonResponse Leave hours
     */
    public function calculateLeavesTotalValue(array $data)
    {
        $request = new Request(
            'POST',
            '/leave_request/admin/calculate_leaves_total_value',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Data to be saved in Leave Request upload job
     *
     * @param string $jobId Job ID
     * @return \Illuminate\Http\JsonResponse Leave Requests upload preview
     *
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/leave_request/upload_preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple leave requests
     *
     * @param array $data leave requests to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted leave requests id's
     */
    public function bulkDelete(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            '/leave_request/bulk_delete',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to download leave requests csv
     *
     * @param int $id Company ID
     * @param array $data from request
     * @return \Illuminate\Http\JsonResponse LeaveRequests
     */
    public function downloadCsv(int $id, array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/leave_requests/download",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->download($request, $authzDataScope);
    }

    /**
     * Call endpoint to download employee leave requests csv
     *
     * @param int $id Employee ID
     * @param array $data from request
     * @return \Illuminate\Http\JsonResponse LeaveRequests
     */
    public function downloadEmployeeLeaveRequestsCsv(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/leave_requests/download",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->download($request);
    }

    /**
     * Call endpoint to get employee leave requests
     * @param int $employeeId
     * @param array $data
     * @param string $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeLeaveRequests(int $employeeId, array $data, string $query)
    {
        return $this->send(
            new Request(
                'POST',
                "/employee/{$employeeId}/leave_requests?{$query}",
                [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                http_build_query($data)
            )
        );
    }
}
