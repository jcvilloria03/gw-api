<?php

namespace App\DayHourRate;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class DayHourRateRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get day/hour rates info.
     *
     * @param int $id Day/Hour Rate ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/day_hour_rate/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get day/hour rates for the company.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function getCompanyDayHourRates(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/day_hour_rates"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get time types for the company.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function getCompanyTimeTypes(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/time_types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create day/hour rates for the company.
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Created Day/Hour Rates
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/day_hour_rate",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update day/hour rate
     *
     * @param array $data
     * @param int $id Day/Hour Rate ID
     * @return \Illuminate\Http\JsonResponse Updated Day/Hour Rate
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PATCH',
            "/day_hour_rate/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
