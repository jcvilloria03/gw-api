<?php

namespace App\DayHourRate;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class DayHourRateAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.day_hour_rate';
    const CREATE_TASK = 'create.day_hour_rate';
    const UPDATE_TASK = 'edit.day_hour_rate';

    /**
     * @param \stdClass $dayHourRate
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $dayHourRate, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($dayHourRate, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $dayHourRate
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyDayHourRates(\stdClass $dayHourRate, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($dayHourRate, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $dayHourRate
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $dayHourRate, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($dayHourRate, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $dayHourRate, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($dayHourRate, $user, self::UPDATE_TASK);
    }

    /**
     * Authorize project related tasks
     *
     * @param \stdClass $project
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $project,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for project account
            return $accountScope->inScope($project->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as project's account
                return $project->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($project->company_id);
        }

        return false;
    }
}
