<?php

namespace App\GovernmentForm;

use App\Audit\AuditService;
use App\Audit\AuditItem;

class GovernmentFormAuditService
{
    const ACTION_CREATE = 'create';

    const OBJECT_NAME = 'government_form';

    const TYPE_HDMF_MSRF = 'HDMF MSRF';
    const TYPE_STLRF = 'STLRF';
    const TYPE_SSS_R5 = 'SSS R5';
    const TYPE_SSS_LOAN_COLLECTION = 'SSS LOAN COLLECTION';
    const TYPE_BIR_1601C = 'BIR 1601C';
    const TYPE_BIR_1601E = 'BIR 1601E';
    const TYPE_BIR_1601F = 'BIR 1601F';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        AuditService $auditService
    ) {
        $this->auditService = $auditService;
    }

    /**
     *
     * Log Government Form related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
        }
    }

    /**
     *
     * Log Government Form Generation
     *
     * @param array $cacheItem
     *
     */
    public function logCreate(array $cacheItem)
    {
        $formData = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $formData['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $formData
        ]);
        $this->auditService->log($item);
    }
}
