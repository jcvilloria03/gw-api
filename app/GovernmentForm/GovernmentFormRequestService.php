<?php

namespace App\GovernmentForm;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class GovernmentFormRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get a specific government form
     *
     * @param int $id Government Form ID
     * @return json
     */
    public function get($id)
    {
        $request = new Request(
            'GET',
            "/government_forms/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get a specific government form
     *
     * @param int $id Government Form ID
     * @return json
     */
    public function generateForm($id)
    {
        $request = new Request(
            'POST',
            "/government_forms/{$id}/generate"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to generate BIR 2316
     *
     * @param array $attributes
     * @return json
     */
    public function generateBir2316(array $attributes, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'POST',
            "/employee/government_forms/bir_2316/bulk/generate",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to generate BIR 2316
     *
     * @param int $companyId
     * @param array $attributes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function regenerateBir2316(int $companyId, array $attributes, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'POST',
            '/company/' . $companyId . '/government_forms/bir_2316/regenerate',
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to download single/multiple BIR 2316s
     *
     * @param int $companyId
     * @param array $attributes
     * @return json
     */
    public function downloadEmployee2316($companyId, array $attributes, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/government_forms/bir_2316/download",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to update BIR 2316
     *
     * @param array $attributes
     * @return json
     */
    public function updateBir2316($id, $employeeId, array $attributes)
    {
        $request = new Request(
            'PATCH',
            "/employee/{$employeeId}/government_forms/bir_2316/{$id}",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get a specific BIR 2316 form
     *
     * @param int $id Government Form ID
     * @return json
     */
    public function getBir2316($id, $employeeId)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/government_forms/bir_2316/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get generated BIR 2316 government forms
     *
     * @param int $id Government Form ID
     * @param int $companyId Company ID
     * @return json
     */
    public function getGenerated2316s($companyId, array $attributes, AuthzDataScope $dataScope = null)
    {
        $queryString = empty($attributes) ? '' : '?' . http_build_query($attributes);

        $request = new Request(
            'GET',
            "/company/{$companyId}/government_forms/bir_2316" . $queryString
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to list candidate employees for BIR 2316
     *
     * @param int $id Government Form ID
     * @param int $companyId Company ID
     * @return json
     */
    public function getCandidate2316Employees($companyId, array $attributes, AuthzDataScope $dataScope = null)
    {
        $queryString = empty($attributes) ? '' : '?' . http_build_query($attributes);

        $request = new Request(
            'GET',
            "/company/{$companyId}/government_forms/bir_2316/employees" . $queryString
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to get generated government forms, per company, per kind
     *
     * @param array $filters Filters of company id, kind
     * @return json List of generated government forms
     */
    public function getAll(array $filters)
    {
        $queryString = empty($filters) ? '' : '?' . http_build_query($filters);

        return $this->send(
            new Request(
                'GET',
                "/government_forms" . $queryString
            )
        );
    }

    /**
     * Generate a government form
     *
     * @param array $data Government Form generation parameters
     * @return json Government Form
     */
    public function store(array $data)
    {
        $request = new Request(
            'POST',
            "/government_forms",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Update a Government form
     *
     * @param int $id Government Form ID
     * @param array $data Government Form fields
     * @return json
     */
    public function update($id, $data)
    {
        $request = new Request(
            'PUT',
            "/government_forms/{$id}",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Months where user can generate government forms
     *
     * @param string $id Company ID
     * @return json List of generatable months
     *
     */
    public function getGovernmentFormPeriods(string $id)
    {
        $request = new Request(
            'GET',
            "/philippine/company/{$id}/government_form_periods"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch available years for 1604c
     *
     * @param int $companyId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableYearsForBir1604c(int $companyId)
    {
        $request = new Request(
            'GET',
            '/government_forms/1604c/available_years?company_id=' . $companyId
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch SSS R5 payment methods
     *
     * @return json List of SSS R5 payment methods
     *
     */
    public function getSssR5Methods()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/sss/r5_methods"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch SSS R5 form options
     *
     * @return json List of SSS R5 form options
     *
     */
    public function getSssR5FormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/sss/r5_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch SSS R5 form values
     *
     * @param array $data Query string parameters
     * @return json List of SSS R5 form values
     *
     */
    public function getSssR5FormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/sss/r5_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch SSS loan collection report form values
     *
     * @param array $data Query string parameters
     * @return json List of SSS  loan collection report form values
     *
     */
    public function getSssLoanCollectionValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/sss/loan_collection_report_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch SSS Loan Collection form options
     *
     * @return json List of SSS Loan Collection form options
     *
     */
    public function getSssLoanCollectionFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/sss/loan_collection_report_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate SSS R5 form
     *
     * @param array $data
     * @return json URI of zip file containing form
     *
     */
    public function generateSssR5(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/sss/r5_generate",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch HDMF MSRF payment channels
     *
     * @return json List of HDMF MSRF payment channels
     *
     */
    public function getHdmfMsrfChannels()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/hdmf/msrf_channels"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch HDMF MSRF form options
     *
     * @return json List of HDMF MSRF form options
     *
     */
    public function getHdmfMsrfFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/hdmf/msrf_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch HDMF MSRF form values
     *
     * @param array $data Query string parameters
     * @return json List of HDMF MSRF form values
     *
     */
    public function getHdmfMsrfFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/hdmf/msrf_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate HDMF MSRF form
     *
     * @param array $data
     * @return json URI of zip file containing form
     *
     */
    public function generateHdmfMsrf(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/hdmf/msrf_generate",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch STLRF form values
     *
     * @param array $data Query string parameters
     * @return json List of STLRF form values
     *
     */
    public function getStlrfFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/stlrf/stlrf_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate STLRF form
     *
     * @param array $data
     * @return json URI of zip file containing form
     *
     */
    public function generateStlrf(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/stlrf/stlrf_generate",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch STLRF form options
     *
     * @return json List of STLRF form options
     *
     */
    public function getStlrfFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/stlrf/stlrf_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Philhealth form values
     *
     * @param array $data Query string parameters
     * @return json List of Philhealth form values
     *
     */
    public function getPhilhealthFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/philhealth/values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601C form options
     *
     * @return json List of BIR 1601C form options
     *
     */
    public function getBir1601cFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601c_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate BIR 1601C form
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse URI of PDF
     */
    public function generateBir1601C(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/bir/1601c_generate",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601 C form values
     *
     * @param array $data Query string parameters
     * @return json List of BIR 1601 C form values
     *
     */
    public function getBir1601CFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601c_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601E form options
     *
     * @return json List of BIR 1601E form options
     *
     */
    public function getBir1601EFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601e_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601E form values
     *
     * @param array $data Query string parameters
     * @return json List of BIR 1601E form values
     *
     */
    public function getBir1601EFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601e_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate BIR 1601E form
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse URI of PDF
     */
    public function generateBir1601E(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/bir/1601e_generate",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601F form options
     *
     * @return json List of BIR 1601F form options
     *
     */
    public function getBir1601fFormOptions()
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601f_options"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch BIR 1601 F form values
     *
     * @param array $data Query string parameters
     * @return json List of BIR 1601 F form values
     *
     */
    public function getBir1601FFormValues(array $data)
    {
        $request = new Request(
            'GET',
            "/philippine/government_forms/bir/1601f_values?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate BIR 1601F form
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse URI of PDF
     */
    public function generateBir1601F(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/bir/1601f_generate",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate SSS R5 form
     *
     * @param array $data
     * @return json URI of zip file containing form
     *
     */
    public function generateSssCollection(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/government_forms/sss/loan_collection_report_generate",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }
}
