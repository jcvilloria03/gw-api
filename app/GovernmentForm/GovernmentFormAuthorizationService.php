<?php

namespace App\GovernmentForm;

use App\Authorization\AuthorizationService;
use App\Model\Permission;
use App\Permission\TargetType;

class GovernmentFormAuthorizationService extends AuthorizationService
{
    const CREATE_TASK = 'create.government_form';
    const VIEW_TASK = 'view.government_form';
    const UPDATE_TASK = 'edit.government_form';

    /**
     * Authorize government form related tasks
     *
     * @param \stdClass $company Company owning the government form
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $company,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for company's account
            return $accountScope->inScope($company->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as company's account
                return $company->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($company->id);
        }

        return false;
    }

    /**
     * @param \stdClass $company Company creating the gov form
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company Company updating the gov form
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $company Company who owns the gov form
     * @param array $user
     * @return bool
     */
    public function authorizeView(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::VIEW_TASK);
    }
}
