<?php

namespace App\Team;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TeamRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get team info
     *
     * @param int $id Team ID
     * @param bool $ess ESS team view
     * @return \Illuminate\Http\JsonResponse Team Info
     */
    public function get(int $id, bool $ess = false)
    {
        $params = $ess ? ['ess' => 1] : [];

        $request = new Request(
            'GET',
            "/team/{$id}?" . http_build_query($params)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get team members
     *
     * @param int $id Team ID
     * @param bool $ess ESS team view
     * @return array Team Info
     */
    public function getMembers(int $id, $page = 1, $perPage = 20)
    {
        $params = [
            'page' => $page,
            'per_page' => $perPage
        ];

        $request = new Request(
            'GET',
            "/team/{$id}/members?" . http_build_query($params)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Call endpoint to get team member IDs
     *
     * @param  int    $id Team ID
     * @return array
     */
    public function getMemberIds(int $id)
    {
        $request = new Request(
            'GET',
            "/team/{$id}/member_ids"
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'];
    }

    /**
     * Call endpoint to get single team member
     *
     * @param int $id Team ID
     * @param int $memberId Member ID
     * @return array Member Info
     */
    public function getSingleMember(int $id, int $memberId)
    {
        $request = new Request('GET', "/team/{$id}/members/{$memberId}");

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Call endpoint to create team
     *
     * @param array $data team information
     * @return \Illuminate\Http\JsonResponse Created Team
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/team",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create time attendance team
     *
     * @param array $data team information
     * @return \Illuminate\Http\JsonResponse Created Team
     */
    public function createTimeAttendanceTeam(array $data)
    {
        $request = new Request(
            'POST',
            "/time_attendance_team",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to create multiple teams
     *
     * @param array $data teams information
     * @return \Illuminate\Http\JsonResponse Created Teams ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/team/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update team for given id
     *
     * @param array $data team informations
     * @param int $id Team Id
     * @return \Illuminate\Http\JsonResponse Updated team
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/team/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if team name is available
     *
     * @param int $companyId Company ID
     * @param array $data team information
     * @return \Illuminate\Http\JsonResponse Availability of team name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/team/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all team within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company teams
     */
    public function getCompanyTeams(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/teams"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all team within Account
     *
     * @param int $accountId Account Id
     * @return \Illuminate\Http\JsonResponse List of account teams
     */
    public function getAccountTeams(int $accountId)
    {
        $request = new Request(
            'GET',
            "/account/{$accountId}/teams"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple teams
     *
     * @param array $data teams to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted teams id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/team/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check are teams in use
     *
     * @param array $data teams information
     * @return \Illuminate\Http\JsonResponse Usement of teams
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            "/team/check_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Get teams by leader ID
     *
     * @param  int    $companyId Company ID
     * @param  int    $leaderId  Leader ID
     * @return array
     */
    public function getTeamsByLeaderId(int $companyId, int $leaderId)
    {
        $request = new Request('GET', "/company/{$companyId}/teams/by_leader/{$leaderId}");
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }
}
