<?php

namespace App\Team;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class TeamAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.team';
    const CREATE_TASK = 'create.team';
    const UPDATE_TASK = 'edit.team';
    const DELETE_TASK = 'delete.team';

    /**
     * @param \stdClass $ream
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $team
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyTeams(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $team
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $team
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $team
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $team
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $team, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($team, $user, self::DELETE_TASK);
    }

    /**
     * Authorize team related tasks
     *
     * @param \stdClass $team
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $team,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($team->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $team->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($team->company_id);
        }

        return false;
    }
}
