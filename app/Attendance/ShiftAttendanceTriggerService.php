<?php

namespace App\Attendance;

use Carbon\Carbon;
use App\Traits\DateTrait;

/**
 * Class ShiftAttendanceTriggerService
 *
 * @package App\Attendance
 */
class ShiftAttendanceTriggerService extends AttendanceTriggerService
{
    use DateTrait;

    /**
     * Calculate Attendance record for a approved request
     *
     * @param int      $employeeId Employee ID
     * @param array    $dates      Dates
     * @param int|null $companyId  Company ID
     *
     * @return string
     */
    public function calculateAttendance(int $employeeId, array $dates, int $companyId = null)
    {
        return $this->calculateAttendanceRecordsForEmployeeOnDates($employeeId, $dates, $companyId);
    }

    /**
     * Calculate Attendance records for multiple employees
     *
     * @param array    $employeeIds Employee IDs
     * @param array    $dates       Dates
     * @param int|null $companyId   Company ID
     *
     * @return string
     */
    public function calculateAttendanceForMultipleEmployees(array $employeeIds, array $dates, int $companyId = null)
    {
        return $this->calculateAttendanceRecordsForMultipleEmployeesOnDates($employeeIds, $dates, $companyId);
    }
}
