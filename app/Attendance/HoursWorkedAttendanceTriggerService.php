<?php

namespace App\Attendance;

/**
 * Class HoursWorkedAttendanceTriggerService
 *
 * @package App\Attendance
 */
class HoursWorkedAttendanceTriggerService extends AttendanceTriggerService
{
    /**
     * HoursWorkedAttendanceTriggerService constructor.
     *
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     */
    public function __construct(
        AttendanceRequestService $attendanceRequestService
    ) {
        parent::__construct($attendanceRequestService);
    }

    /**
     * Calculate Employees Attendance Records
     * @param array $holidayDates holiday dates (contains one or more date)
     * @param       $companyId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecords(array $requestData, $companyId = null)
    {
        $employeeDates = [];
        $jobIds = [];
        foreach ($requestData['data'] as $item) {
            if (key_exists($item['employee_id'], $employeeDates)) {
                if (!in_array($item['date'], $employeeDates[$item['employee_id']])) {
                    array_push($employeeDates[$item['employee_id']], $item['date']);
                }
            } else {
                $employeeDates[$item['employee_id']] = [$item['date']];
            }
        }
        foreach ($employeeDates as $employeeId => $dates) {
            $jobIds[] = $this->calculateAttendanceRecordsForEmployeeOnDates($employeeId, $dates, $companyId);
        }

        return ['job_ids' => $jobIds];
    }
}
