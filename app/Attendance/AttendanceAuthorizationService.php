<?php

namespace App\Attendance;

use App\Common\CommonAuthorizationService;
use Exception;

class AttendanceAuthorizationService extends CommonAuthorizationService
{
    public $viewTask          = 'view.attendance_computation';
    public $createTask        = 'create.attendance_computation';
    public $updateTask        = 'edit.attendance_computation';
    public $deleteTask        = 'delete.attendance_computation';
    public $exportCalculation = 'export.attendance_computation';

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $model, array $user)
    {
        throw new Exception('Not implemented.');
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $model, array $user)
    {
        throw new Exception('Not implemented.');
    }

    public function authorizeGenerateCalculation(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->createTask);
    }

    public function authorizeExportComputedAttendance(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->exportCalculation);
    }
}
