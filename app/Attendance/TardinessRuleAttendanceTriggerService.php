<?php

namespace App\Attendance;

use App\Ess\EssEmployeeRequestService;
/**
 * Class TardinessRuleAttendanceTriggerService
 *
 * @package App\Attendance
 */
class TardinessRuleAttendanceTriggerService extends AttendanceTriggerService {

    /**
     * Attendance Request Service
     *
     * @var \App\Attendance\AttendanceRequestService
     */
    protected $attendanceRequestService;

    /**
     * Attendance Request Service
     *
     * @var \App\Ess\EssEmployeeRequestService
     */
    protected $essEmployeeRequestService;

    /**
     * EmployeeRequestAttendanceTriggerService constructor.
     *
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     */
    public function __construct(
        AttendanceRequestService $attendanceRequestService,
        EssEmployeeRequestService $essEmployeeRequestService
    ) {
        parent::__construct($attendanceRequestService);
        $this->essEmployeeRequestService = $essEmployeeRequestService;
    }

    /**
     * Calculates Attendance for entities affected by tardiness
     *
     * @param array $affectedEntities
     * @return void
     */
    public function calculateAttendance(array $affectedEntities, int $companyId)
    {
        $affectedEmployeesData = $this->getAffectedEmployees($affectedEntities, $companyId);
        $affectedEmployeesIds = $this->collectAffectedEmployeesIds($affectedEmployeesData);

        $this->calculateAttendanceRecordsForEmployees($affectedEmployeesIds);
    }

    /**
     * Collects affected employees ids.
     *
     * @param array $affectedEmployees
     * @return array
     */
    private function collectAffectedEmployeesIds(array $affectedEmployees)
    {
        return collect($affectedEmployees)->pluck('id')->all();
    }

    /**
     * Gets unique affected entities by id and name.
     *
     * @param array $affectedEntities
     * @return void
     */
    public function getUniqueAffectedEntitiesByIdAndEntityName(array $affectedEntities)
    {
        return collect($affectedEntities)->unique(function ($affectedEntitie) {
            return $affectedEntitie['id'] && $affectedEntitie['name'];
        })->all();
    }

    /**
     * Gets affected employees data.
     *
     * @param array $affectedEntities
     * @param int $companyId
     * @return array
     */
    private function getAffectedEmployees(array $affectedEntities, int $companyId)
    {
        $response = $this->essEmployeeRequestService->getEntitledEmployees(
            $affectedEntities,
            $companyId
        );

        $affectedEmployeesResponseData = json_decode($response->getData(), true);
        $affectedEmployeesData = array_get($affectedEmployeesResponseData, 'data');

        return $affectedEmployeesData ?? [];
    }
}
