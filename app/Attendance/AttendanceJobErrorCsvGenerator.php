<?php

namespace App\Attendance;

class AttendanceJobErrorCsvGenerator
{
    const HEADERS = [
        'Date',
        'Employee',
        'Message',
        'Severity'
    ];

    /**
     * Errors data
     *
     * @var array
     */
    protected $errorsData;

    /**
     * Employee ID and name mapping
     *
     * @var array
     */
    protected $employeesMap;

    /**
     * Constructs AttendanceJobErrorCsvGenerator
     *
     * @param array  $errorsData    Errors data
     * @param array  $employeesMap  Employee ID and name mapping
     */
    public function __construct(array $errorsData, array $employeesMap)
    {
        $this->errorsData = $errorsData;

        $this->employeesMap = $employeesMap;
    }

    /**
     * Get CSV header data
     *
     * @return array
     */
    protected function getHeader()
    {
        return self::HEADERS;
    }

    /**
     * Generator function for getting row data
     *
     * @return array
     */
    protected function getRowData()
    {
        foreach ($this->errorsData as $error) {
            $employeeName = $this->employeesMap[$error['employee_uid']] ?? null;

            yield [
                $error['attendance_date'] ?? null,
                $employeeName,
                $error['message'] ?? null,
                $error['is_warning'] ? 'Warning' : 'Error'
            ];
        }
    }

    /**
     * Callback function for StreamedResponse
     *
     * @return file
     */
    public function getCsvFile()
    {
        $fileName = tempnam(sys_get_temp_dir(), 'attendance-job-error');

        $file = fopen($fileName, 'w');
        fputcsv($file, $this->getHeader());

        foreach ($this->getRowData() as $row) {
            fputcsv($file, $row);
        }

        fclose($file);

        return $fileName;
    }
}
