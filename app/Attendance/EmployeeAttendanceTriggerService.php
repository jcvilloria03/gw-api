<?php

namespace App\Attendance;

use App\Traits\ArrayFlattenTrait;
use App\Traits\DateTrait;
use Carbon\Carbon;

/**
 * Class EmployeeAttendanceTriggerService
 *
 * @package App\Attendance
 */
class EmployeeAttendanceTriggerService extends AttendanceTriggerService
{
    use DateTrait, ArrayFlattenTrait;

    /**
     * These fields are correlated to the employee data array
     * Data that they are bound to must be of type string, in Y-m-d format
     */
    const MAP_DATE_FIELDS_FOR_COMPARISON = [
        'date_hired',
        'birth_date',
        'date_ended'
    ];

    const MAP_ATTENDANCE_EMPLOYEE_FIELDS_FOR_CALCULATION = [
        'team_name',
        'team_role',
        'primary_location_name',
        'department_name',
        'position_name',
        'secondary_location_name',
    ];

    const MAP_PAYROLL_EMPLOYEE_FIELDS_FOR_CALCULATION = [
        'payroll_group_name',
    ];

    const DATE_FORMAT = 'Y-m-d';

    /**
     * Calculate Attendance record for a approved request
     *
     * @param array $attributes   Update Attributes
     * @param array $employeeData Employee Data
     * @param int   $employeeId   Employee ID
     *
     * @return string
     */
    public function calculateAttendance(array $attributes, array $employeeData, int $employeeId)
    {
        $dates = $this->prepEmployeeCalculationDates($attributes, $employeeData);
        $companyId = array_key_exists('company_id', $attributes) ? $attributes['company_id'] : null;

        if (empty($dates)) {
            return [];
        }

        return $this->calculateAttendanceRecordsForEmployeeOnDates($employeeId, $dates, $companyId);
    }

    /**
     * Prepare employee dates for the attendance record calculation
     *
     * @param array $attributes   Update Attributes
     * @param array $employeeData Employee Data
     *
     * @return array
     */
    public function prepEmployeeCalculationDates(array $attributes, array $employeeData)
    {
        $dates = [];

        $this->prepDateIfEmployeeFieldsChanged($attributes, $employeeData, $dates);

        foreach (self::MAP_DATE_FIELDS_FOR_COMPARISON as $field) {
            if (array_key_exists($field, $employeeData)
                && array_key_exists($field, $attributes)
            ) {
                if ($attributes[$field] !== $employeeData[$field]) {
                    $dates[] = $this->generateDateRange($attributes[$field], $employeeData[$field]);
                }
            }
        }

        $flattenArray = $this->arrayFlatten($dates, false);

        return array_unique($flattenArray);
    }

    /**
     * If mapped fields were updated, then use today as date for Attendance Calculation
     *
     * @param array $attributes   Attributes that should be compared with Employee Data
     * @param array $employeeData Employee Data
     * @param array $dates        Dates for Attendance Calculation service
     */
    private function prepDateIfEmployeeFieldsChanged(array $attributes, array $employeeData, array &$dates)
    {
        $flagForASingleDay = false;

        $this->checkIfMappedFieldsWereUpdated(
            $attributes,
            $employeeData,
            'time_attendance',
            self::MAP_ATTENDANCE_EMPLOYEE_FIELDS_FOR_CALCULATION,
            $flagForASingleDay
        );
        $this->checkIfMappedFieldsWereUpdated(
            $attributes,
            $employeeData,
            'payroll',
            self::MAP_PAYROLL_EMPLOYEE_FIELDS_FOR_CALCULATION,
            $flagForASingleDay
        );

        if ($flagForASingleDay) {
            $dates[] = [
                Carbon::now()->format(self::DATE_FORMAT)
            ];
        }
    }

    /**
     * Check if mapped fields were updated
     *
     * @param array  $dataToBeUpdated   Attributes that should be compared with Employee Data
     * @param array  $employeeData      Employee Data
     * @param string $key               Key for Employee Data array
     * @param array  $map               Map for employee calculation
     * @param bool   $flagForASingleDay If it's a single day addition
     */
    private function checkIfMappedFieldsWereUpdated(
        array $dataToBeUpdated,
        array $employeeData,
        string $key,
        array $map,
        bool &$flagForASingleDay
    ) {
        if (array_key_exists($key, $employeeData)
            && !empty($employeeData[$key])
        ) {
            foreach ($map as $field) {
                if (array_key_exists($field, $dataToBeUpdated)
                    && array_key_exists($field, $employeeData[$key])
                    && ($dataToBeUpdated[$field] !== $employeeData[$key][$field])
                ) {
                    $flagForASingleDay = true;
                }
            }
        }
    }
}
