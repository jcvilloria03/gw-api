<?php

namespace App\Attendance;

use App\Tasks\UploadTask;

class AttendanceUploadTask extends UploadTask
{
    const PROCESS_TIME_ATTENDANCE = 'time_attendance_import';

    /**
     * Company Id
     *
     * @var int
     */
    protected $companyId;

    /**
     * Generate Task Id, to be used as Redis Key
     */
    protected function generateId()
    {
        return null;
    }

    /**
     * Create task in Redis
     *
     * @param int $companyId Company Id
     * @param string $id Job id
     *
     */
    public function create(int $companyId)
    {
        $this->companyId = $companyId;
        $this->currentValues = [
            's3_bucket' => $this->s3Bucket
        ];
    }

    /**
     * Save Personal Info
     *
     * @param string $path Full path of file to upload
     *
     */
    public function saveAttendanceInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key();
        $this->saveFileToS3($s3Key, $path);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     */
    protected function generateS3Key()
    {
        return self::PROCESS_TIME_ATTENDANCE .
               ':' .
               $this->companyId .
               ':' .
               uniqid();
    }
}
