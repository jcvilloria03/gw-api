<?php

namespace App\Attendance;

/**
 * Class EmployeeRequestAttendanceTriggerService
 *
 * @package App\Attendance
 */
class EmployeeRequestAttendanceTriggerService extends AttendanceTriggerService
{

    /**
     * Calculate Attendance record for a approved request
     *
     * @param array $approvedRequest Approved Request
     *
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function calculateAttendance(array $approvedRequest)
    {
        switch ($approvedRequest['request_type']) {
            case 'overtime_request':
            case 'undertime_request':
                $employeeId = $approvedRequest['employee_id'];
                $date = substr($approvedRequest['request']['date'], 0, 10);
                return $this->calculateAttendanceRecordsForEmployeeOnADay($employeeId, $date);
            case 'time_dispute_request':
                $params = $this->timeDisputeRequestDataPrep($approvedRequest);
                $companyId = array_key_exists('company_id', $approvedRequest) ? $approvedRequest['company_id'] : null;
                return $this->calculateAttendanceRecordsForEmployeeOnDates(
                    $params['employee_id'],
                    $params['dates'],
                    $companyId
                );
            case 'leave_request':
                $params = $this->leaveRequestDataPrep($approvedRequest);
                $companyId = array_key_exists('company_id', $approvedRequest) ? $approvedRequest['company_id'] : null;
                return $this->calculateAttendanceRecordsForEmployeeOnDates(
                    $params['employee_id'],
                    $params['dates'],
                    $companyId
                );
            case 'shift_change_request':
                $params = $this->shiftChangeRequestDataPrep($approvedRequest);
                $companyId = array_key_exists('company_id', $approvedRequest) ? $approvedRequest['company_id'] : null;
                return $this->calculateAttendanceRecordsForEmployeeOnDates(
                    $params['employee_id'],
                    $params['dates'],
                    $companyId
                );
            default:
                throw new \Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException(
                    'Invalid request type.'
                );
        }
    }

    /**
     * Prepare data for time dispute request attendance calculation
     *
     * @param array $approvedRequest Time Dispute Request data
     *
     * @return array
     */
    private function timeDisputeRequestDataPrep(array $approvedRequest)
    {
        $shifts = $approvedRequest['request']['params']['shifts'];
        $dates = [];

        foreach ($shifts as $shift) {
            $dates[] = $shift['date'];
        }

        return [
            'employee_id' => $approvedRequest['employee_id'],
            'dates' => $dates
        ];
    }

    /**
     * Prepare data for leave request attendance calculation
     *
     * @param array $approvedRequest Leave Request data
     *
     * @return array
     */
    private function leaveRequestDataPrep(array $approvedRequest)
    {
        $leaves = $approvedRequest['request']['leaves'];
        $dates = [];

        foreach ($leaves as $leave) {
            $dates[] = $leave['date'];
        }

        return [
            'employee_id' => $approvedRequest['employee_id'],
            'dates' => $dates
        ];
    }

    /**
     * Prepare data for shift change request attendance calculation
     *
     * @param array $approvedRequest Shift Change Request data
     *
     * @return array
     */
    private function shiftChangeRequestDataPrep(array $approvedRequest)
    {
        $shiftChangeDates = $approvedRequest['request']['params']['shift_change_dates'];
        $dates = [];

        foreach ($shiftChangeDates as $shiftChangeDate) {
            $dates[] = $shiftChangeDate['date'];
        }

        return [
            'employee_id' => $approvedRequest['employee_id'],
            'dates' => $dates
        ];
    }
}
