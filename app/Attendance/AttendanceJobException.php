<?php

namespace App\Attendance;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AttendanceJobException extends BadRequestHttpException
{
}
