<?php

namespace App\Attendance;

use App\Ess\EssEmployeeRequestService;
use App\Holiday\HolidayRequestService;

use Carbon\Carbon;
use App\Traits\DateTrait;

/**
 * Class HolidayAttendanceTriggerService
 *
 * @package App\Attendance
 */
class HolidayAttendanceTriggerService extends AttendanceTriggerService
{
    use DateTrait;
    /**
     * EssEmployeeRequestService
     *
     * @var \App\Ess\EssEmployeeRequestService
     */
    protected $essEmployeeRequestService;

    /**
     * HolidayAttendanceTriggerService constructor.
     *
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     * @param EssEmployeeRequestService $essEmployeeRequestService EssEmployeeRequestService
     */
    public function __construct(
        AttendanceRequestService $attendanceRequestService,
        EssEmployeeRequestService $essEmployeeRequestService
    ) {
        parent::__construct($attendanceRequestService);
        $this->essEmployeeRequestService = $essEmployeeRequestService;
    }

    /**
     * Calculate Employees Attendance Records
     * @param $employeeGroups array of employee groups
     * @param int $companyId company ID
     * @param array $holidayDates holiday dates (contains one or more date)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecords(array $employeeGroups, int $companyId, array $holidayDates)
    {
        $result = $this->essEmployeeRequestService->getEntitledEmployees($employeeGroups, $companyId);
        $result = json_decode($result->getData(), true);
        $employeeIds = collect($result['data'])->pluck('id')->unique()->all();
        if (!empty($employeeIds) && !empty($holidayDates)) {
            return $this->calculateAttendanceRecordsForMultipleEmployeesOnDates($employeeIds, $holidayDates);
        }
        return null;
    }

    /**
     * Calculate Employees Attendance Records for company holidays
     * @param array $holidayData holiday data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecordsByCompany(array $holidayData)
    {
        $results = [];
        foreach ($holidayData as $holiday) {
            $holidayDates = $this->generateDateRange($holiday['date'], $holiday['repeat_until']);

            $results[] = $this->calculateAttendanceRecords(
                $holiday['affected_employees'],
                $holiday['company_id'],
                $holidayDates
            );
        }
        return $results;
    }
}
