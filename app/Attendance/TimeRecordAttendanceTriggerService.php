<?php

namespace App\Attendance;

use App\Holiday\HolidayRequestService;
use Carbon\Carbon;
use App\Traits\DateTrait;

/**
 * Class TimeRecordAttendanceTriggerService
 *
 * @package App\Attendance
 *
 * @SuppressWarnings(PHPMD)
 */
class TimeRecordAttendanceTriggerService extends AttendanceTriggerService
{
    use DateTrait;

    /**
     * TimeRecordAttendanceTriggerService constructor.
     *
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     */
    public function __construct(
        AttendanceRequestService $attendanceRequestService
    ) {
        parent::__construct($attendanceRequestService);
    }

    /**
     * Calculate Employees Attendance Records
     * @param array $requestData
     * @param array $responseData
     * @param $companyId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecords(array $requestData, array $responseData, $companyId = null)
    {
        $jobIds = [];
        $result = $this->getUniqueEmplyeesIdsWithDates($requestData, $responseData);
        foreach ($result as $employeeId => $dates) {
            $jobIds[] = $this->calculateAttendanceRecordsForEmployeeOnDates($employeeId, $dates, $companyId);
        }

        return ['job_ids' => $jobIds];
    }

    /**
     * Get unique Employee IDs and unique dates
     * @param array $requesteData
     * @param array $responseData
     * @param array $result
     *
     * @return array
     */
    private function getUniqueEmplyeesIdsWithDates(array $requestData, array $responseData)
    {
        $result = [];
        if (key_exists('delete', $requestData)) {
            foreach ($requestData['delete'] as $value) {
                $date = Carbon::createFromTimestamp($value['timestamp'])->format('Y-m-d');
                if (!key_exists($value['employee_id'], $result)) {
                    $result[$value['employee_id']] = [$date];
                } else {
                    if (!in_array($date, $result[$value['employee_id']])) {
                        array_push($result[$value['employee_id']], $date);
                    }
                }
            }
        }

        if (key_exists('created', $responseData)) {
            foreach ($responseData['created'] as $value) {
                if (key_exists('timestamp', $value) && key_exists('employee_uid', $value)) {
                    $employeeId = (string) $value['employee_uid']['N'];
                    $date = Carbon::createFromTimestamp($value['timestamp']['N'])->format('Y-m-d');
                    if (!key_exists($employeeId, $result)) {
                        $result[$employeeId] = [$date];
                    } else {
                        if (!in_array($date, $result[$employeeId])) {
                            array_push($result[$employeeId], $date);
                        }
                    }
                }
            }
        }

        return $result;
    }
}
