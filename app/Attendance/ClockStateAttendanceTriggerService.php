<?php

namespace App\Attendance;

/**
 * Class ClockStateAttendanceTriggerService
 *
 * @package App\Attendance
 */
class ClockStateAttendanceTriggerService extends AttendanceTriggerService
{
    /**
     * Calculate Attendance record when clock state is changed
     *
     * @param int $employeeId Employee ID
     * @param string $date 'Y-m-d' format
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendance(int $employeeId, string $date)
    {
        $response = $this->calculateAttendanceRecordsForEmployeeOnADay($employeeId, $date);

        return json_decode($response->getData(), true);
    }
}
