<?php

namespace App\Attendance;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class AttendanceTriggerService
 *
 * @package App\Attendance
 */
class AttendanceTriggerService
{

    /**
     * Attendance Request Service
     *
     * @var \App\Attendance\AttendanceRequestService
     */
    protected $attendanceRequestService;

    /**
     * EmployeeRequestAttendanceTriggerService constructor.
     *
     * @param AttendanceRequestService $attendanceRequestService AttendanceRequestService
     */
    public function __construct(
        AttendanceRequestService $attendanceRequestService
    ) {
        $this->attendanceRequestService = $attendanceRequestService;
    }

    /**
     * Calculate Attendance Record for a single day
     *
     * @param int    $employeeId Employee ID
     * @param string $date       Date
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecordsForEmployeeOnADay(int $employeeId, string $date)
    {
        try {
            $response = $this->attendanceRequestService->calculateAttendanceRecord(
                $employeeId,
                $date
            );
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            $response = null;
        }

        return $response;
    }

    /**
     * Calculate Attendance Record for set of days
     *
     * @param int      $employeeId Employee ID
     * @param array    $dates      Dates
     * @param int|null $companyId  Company ID
     *
     * @return string job_id
     */
    public function calculateAttendanceRecordsForEmployeeOnDates(int $employeeId, array $dates, int $companyId = null)
    {
        try {
            $data = [
                'employee_ids' => [$employeeId],
                'dates' => $dates
            ];
            if (!empty($companyId)) {
                $data['company_id'] = $companyId;
            }
            $response = $this->attendanceRequestService->bulkAttendanceCalculateByEmployees($data);
            $responseData = json_decode($response->getData(), true);

            $responseData = array_get($responseData, 'data', null);

            return array_get($responseData, 'job_id', null);
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            $response = null;
        }

        return $response;
    }

    /**
     * Calculate Attendance Record for set of days
     *
     * @param array    $employeeIds Employee IDs
     * @param array    $dates       Dates
     * @param int|null $companyId   Company ID
     *
     * @return string job_id
     */
    public function calculateAttendanceRecordsForMultipleEmployeesOnDates(
        array $employeeIds,
        array $dates,
        int $companyId = null
    ) {
        try {
            $data = [
                'employee_ids' => $employeeIds,
                'dates' => $dates
            ];
            if (!empty($companyId)) {
                $data['company_id'] = $companyId;
            }
            $response = $this->attendanceRequestService->bulkAttendanceCalculateByEmployees($data);
            $responseData = json_decode($response->getData(), true);
            $responseData = array_get($responseData, 'data', null);

            return array_get($responseData, 'job_id', null);
        } catch (HttpException $e) {
            Log::error($e->getMessage());
            $response = null;
        }

        return $response;
    }

    /**
     * Calculate Attendance Record for a single day.
     *
     * @param array $employeeIds Employee IDs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateAttendanceRecordsForEmployees(array $employeeIds)
    {
        // TODO: Change return, when bulk calculation for attendance records is implemented
        return $employeeIds;
    }
}
