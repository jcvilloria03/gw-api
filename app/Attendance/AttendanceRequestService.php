<?php

namespace App\Attendance;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AttendanceRequestService
 *
 * @package App\Attendance
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AttendanceRequestService extends RequestService
{

    /**
     * Call endpoint to Search Attendance Records
     *
     * @param array $data Search filters
     *
     * @return \Illuminate\Http\JsonResponse Attendance Records
     */
    public function searchAttendanceRecords(array $data, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'POST',
            "/attendance/records",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to Download Attendance Records
     *
     * @param array $data Search filters
     *
     * @return \Illuminate\Http\JsonResponse Attendance Records
     */
    public function exportAttendanceRecords(array $data)
    {
        $request = new Request(
            'POST',
            "/attendance/records/export",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to Start Download Attendance Records Job
     *
     * @param array $data Search filters
     *
     * @return \Illuminate\Http\JsonResponse Attendance Records
     */
    public function exportAttendanceRecordsJob(array $data)
    {
        $request = new Request(
            'POST',
            "/attendance/records/export/job",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    public function exportAttendanceRecordsJobStatus($jobId)
    {
        $request = new Request(
            'GET',
            "/attendance/records/export/job/{$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get a Single Attendance Record
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     *
     * @return \Illuminate\Http\JsonResponse Attendance Record
     */
    public function getSingleAttendanceRecord(int $employeeId, $attendanceDate)
    {
        $request = new Request(
            'GET',
            "/attendance/{$employeeId}/{$attendanceDate}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get Job Details
     *
     * @param string    $jobId     Job Id
     *
     * @return \Illuminate\Http\JsonResponse Attendance Calculation Job Details
     */
    public function getJobDetails($jobId)
    {
        $request = new Request(
            'GET',
            "/attendance/job/{$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get Job Errors
     *
     * @param string    $jobId     Job Id
     *
     * @return \Illuminate\Http\JsonResponse Attendance Calculation Job Errors
     */
    public function getJobErrors($jobId)
    {
        $request = new Request(
            'GET',
            "/attendance/job/{$jobId}/errors"
        );

        return $this->send($request);
    }

    /**
     * Get aggregated job errors data
     *
     * @param  string $jobId Job ID
     * @return array
     */
    public function getJobErrorsData(string $jobId)
    {
        $errors = [];
        $errorsResponse = null;

        try {
            $errorsResponse = $this->getJobErrors($jobId);
        } catch (HttpException $e) {
            $errors[]['message'] = $e->getMessage();
        }

        if ($errorsResponse !== null) {
            $data = $errorsResponse->getData();

            if (is_object($data) && property_exists($data, 'Message')) {
                if ($errorsResponse->getStatusCode() !== Response::HTTP_OK) {
                    throw new AttendanceJobException($data->Message);
                }

                $errors[]['message'] = $data->Message;
            }

            if (is_string($data)) {
                $jobErrors = json_decode($data, true)['data'];

                $errors = array_merge($errors, $jobErrors);
            }
        }

        return $errors;
    }

    /**
     * Call endpoint to get a Single Attendance Record Lock Status
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     *
     * @return \Illuminate\Http\JsonResponse Attendance Record
     */
    public function getSingleAttendanceRecordLockStatus(int $employeeId, $attendanceDate)
    {
        $request = new Request(
            'GET',
            "/attendance/{$employeeId}/{$attendanceDate}/lock_status"
        );

        return $this->send($request);
    }

    /**
     * Get employee IDs of the specified attendance records
     *
     * @param  int    $companyId           Company ID
     * @param  array  $attendanceRecordIds List of attendance record IDs
     * @return array
     */
    public function getEmployeeIdsByAttendanceRecords(int $companyId, array $attendanceRecordIds)
    {
        $url = sprintf('/attendance/company/%d/employees', $companyId);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $data = json_encode([
            'ids' => $attendanceRecordIds
        ]);

        $request = new Request('POST', $url, $headers, $data);
        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'] ?? [];
    }

    /**
     * Call endpoint to calculate an attendance record
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     *
     * @return \Illuminate\Http\JsonResponse Response HTTP status
     */
    public function calculateAttendanceRecord(int $employeeId, $attendanceDate)
    {
        $request = new Request(
            'GET',
            "/attendance/{$employeeId}/{$attendanceDate}/calculate"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to LOCK an attendance record
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     *
     * @return \Illuminate\Http\JsonResponse Response HTTP status
     */
    public function lockAttendanceRecord(int $employeeId, $attendanceDate)
    {
        $request = new Request(
            'GET',
            "/attendance/{$employeeId}/{$attendanceDate}/lock"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to UNLOCK an attendance record
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     *
     * @return \Illuminate\Http\JsonResponse Response HTTP status
     */
    public function unlockAttendanceRecord(int $employeeId, $attendanceDate)
    {
        $request = new Request(
            'GET',
            "/attendance/{$employeeId}/{$attendanceDate}/unlock"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to batch UNLOCK attendance records
     *
     * @param array $data List of employee_uid and date to be unlocked
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function bulkUnlockAttendanceRecord(array $data)
    {
        $request = new Request(
            'POST',
            '/attendance/unlock/bulk',
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to batch LOCK attendance records
     *
     * @param array $data List of employee_uid and date to be locked
     * @param \App\Authz\AuthzDataScope $authzDataScope Authz Data Scope
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function bulkLockAttendanceRecord(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            '/attendance/lock/bulk',
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to EDIT attendance records
     *
     * @param int    $employeeId     Employee Id
     * @param string $attendanceDate Date of attendance record (YYYY-MM-DD)
     * @param array  $data           JSON request body of computed attendance values
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function editAttendanceRecord(int $employeeId, $attendanceDate, array $data)
    {
        $request = new Request(
            'PUT',
            "/attendance/{$employeeId}/{$attendanceDate}/edit",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to batch calculate attendance records by Employees, Attendance Dates
     *
     * @param array $data List of employee_uids and dates to be calculated
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function bulkAttendanceCalculateByEmployees(array $data)
    {
        $request = new Request(
            'POST',
            '/attendance/calculate/bulk',
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to batch calculate attendance records by Company, Attendance Date
     *
     * @param int    $companyId      Company ID
     * @param string $attendanceDate Attendance Date
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function bulkAttendanceCalculateByCompany($companyId, $attendanceDate)
    {
        $request = new Request(
            'POST',
            "/attendance/company/{$companyId}/{$attendanceDate}/calculate"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to import Attendance Records per Employee
     *
     * @param array $data Attendance Import Data
     *
     * @return \Illuminate\Http\JsonResponse HTTP response
     */
    public function uploadAttendanceInfo(array $data)
    {
        $request = new Request(
            'POST',
            "/attendance/records/upload",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request);
    }

    public function getBadges(array $data)
    {
        $request = new Request(
            'POST',
            '/attendance/badges',
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'];
    }

    /**
     * Call endpoint to get Attendance Statistics for the current date
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAttendanceStatistics($companyId, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'GET',
            "/attendance/company/{$companyId}/attendance_stats"
        );

        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to get Attendance Statistics for the current date
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDashboardAttendance(int $companyId, array $queryParams, AuthzDataScope $authzDataScope)
    {
        $queryString = http_build_query($queryParams);

        $request = new Request(
            'GET',
            "/attendance/company/{$companyId}/dashboard/attendance?{$queryString}",
            ['Content-Type' => 'application/json'],
            json_encode($queryParams)
        );

        return $this->send($request, $authzDataScope);
    }
}
