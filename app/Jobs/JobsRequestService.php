<?php

namespace App\Jobs;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class JobsRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get Job info
     *
     * @param string $jobId Job ID
     * @param string|null $includes Includes
     * @return \Illuminate\Http\JsonResponse Job Info
     */
    public function getJobStatus($jobId, $includes = null)
    {
        $queryString = '';

        if (!empty($includes)) {
            $queryString .= "?include={$includes}";
        }

        $request = new Request(
            'GET',
            "/jobs/{$jobId}" . $queryString
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get Job info
     *
     * @param string $jobId Job ID
     * @param string|null $includes Includes
     * @return \Illuminate\Http\JsonResponse Job Info
     */
    public function getJobResult($jobId)
    {
        $queryString = "?filter[job_id]=" . $jobId;
        $url = "/job-results" . $queryString;
        $request = new Request(
            'GET',
            $url
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create the a new Job
     *
     * @param array $jobPayload Request body of new Job
     * @return \Illuminate\Http\JsonResponse New Job reponse
     */
    public function createJob(array $jobPayload)
    {
        return $this->send(
            new Request(
                'POST',
                '/jobs',
                [
                    'Content-Type' => 'application/vnd.api+json'
                ],
                json_encode($jobPayload)
            )
        );
    }

    /**
     * Error translation
     */
    public static function flattenErrorData(array $arr)
    {
        $flat = array();
        foreach ($arr as $key => $errors) {
            $flat[$key] = array();
            foreach ($errors as $error) {
                $detail = @$error["details"][0] ?? [];
                $params = $detail["parameters"] ?? [];
                switch ($error["code"]) {
                    case "VE-110":
                        foreach ($params as $param) {
                            array_push($flat[$key], "{$param} header is required.");
                        }
                        break;
                    case "VE-111":
                        foreach ($params as $param) {
                            array_push($flat[$key], "{$param} header is invalid.");
                        }
                        break;
                    case "VE-100":
                        foreach ($params as $param) {
                            array_push($flat[$key], "{$param} is required.");
                        }
                        break;
                    case "VE-101":
                        foreach ($params as $param) {
                            $errString = "Invalid value for field {$param}.";
                            $expectedFormat = $detail["expectedFormat"] ?? "";
                            if ($expectedFormat == "MM/DD/YYYY") {
                                $errString .= " Date must be in MM/DD/YYYY format.";
                            } else if ($expectedFormat == "number") {
                                $errString .= " This field can only accept numeric values.";
                            }
                            array_push($flat[$key], $errString);
                        }
                        break;
                    case "VE-102":
                        foreach ($params as $param) {
                            $errString = "Invalid value for field {$param}. ";
                            $expectedFormat = $detail["expectedFormat"] ?? "string";
                            if ($expectedFormat == "number") {
                                $errString .= "This field can only accept numeric values";
                                $min = $detail["expectedValue"]["min"] ?? null;
                                $max = $detail["expectedValue"]["max"] ?? null;
                                if ($min == null && $max == null) {
                                    $errString .= ".";
                                } else if ($min !== null && $max == null) {
                                    $errString .= " greater than or equal to $min.";
                                } else if ($min == null && $max !== null) {
                                    $errString .= " less than or equal to $max.";
                                } else if ($min !== null && $max !== null) {
                                    $errString .= " from $min to $max.";
                                }
                            } else {
                                $expectedValue = $detail["expectedValue"] ?? "";
                                if (!empty($expectedValue)) {
                                    $find = ', ';
                                    $options = explode('/', $expectedValue);
                                    $optString = implode($find, $options);
                                    $result = preg_replace(strrev("/$find/"), strrev(' or '), strrev($optString), 1);
                                    $optString = strrev($result);
                                    $errString .= "Possible values are {$optString}.";
                                }
                            }
                            array_push($flat[$key], $errString);
                        }
                        break;
                    case "VE-103":
                        array_push($flat[$key], "Duplicate data.");
                        break;
                    case "VE-200":
                        array_push(
                            $flat[$key],
                            "Employee does not exist.  Please check if this employee belong to this company."
                        );
                        break;
                    case "VE-203":
                        $detailStr = "Processing error.";
                        if (is_string($detail)) {
                            $detailStr = $detail;
                        } else if (is_array($detail)) {
                            $detailStr = @$detail['description'];
                        }
                        array_push($flat[$key], $detailStr);
                        break;
                }
            }
        }
        return $flat;
    }
}
