<?php

namespace App\EmployeeRequestModule;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class EmployeeRequestModuleAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.employee_request_modules';

    /**
     * @param \stdClass $employeeRequestModule
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $employeeRequestModule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($employeeRequestModule, $user, self::VIEW_TASK);
    }

    /**
     * Authorize EmployeeRequestModule related tasks
     *
     * @param \stdClass $employeeRequestModule
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $employeeRequestModule,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for workflow account
            return $accountScope->inScope($employeeRequestModule->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as workflow's account
                return $employeeRequestModule->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($employeeRequestModule->company_id);
        }

        return false;
    }
}
