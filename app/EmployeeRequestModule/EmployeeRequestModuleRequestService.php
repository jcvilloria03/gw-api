<?php

namespace App\EmployeeRequestModule;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EmployeeRequestModuleRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to index employee request modules
     *
     * @return \Illuminate\Http\JsonResponse List of employee request modules
     */
    public function get()
    {
        $request = new Request(
            'GET',
            "/employee_request_module"
        );
        return $this->send($request);
    }
}
