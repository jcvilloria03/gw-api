<?php

namespace App\LeaveEntitlement;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class LeaveEntitlementRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get leave entitlement info
     *
     * @param int $id LeaveEntitlement ID
     * @return \Illuminate\Http\JsonResponse LeaveEntitlement Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/leave_entitlement/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create leave entitlement
     *
     * @param array $data leave entitlement information
     * @return \Illuminate\Http\JsonResponse Created Leave Entitlement
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/leave_entitlement",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all leave entitlements within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company leave entitlements
     */
    public function getCompanyLeaveEntitlements(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/leave_entitlements"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple leave entitlements
     *
     * @param array $data leave entitlements to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted leave entitlements id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/leave_entitlement/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update leave entitlement for given id
     *
     * @param array $data leave entitlement informations
     * @param int $id leave entitlement Id
     * @return \Illuminate\Http\JsonResponse Updated leave entitlement
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/leave_entitlement/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if leave entitlement name is available
     *
     * @param int $companyId Company ID
     * @param array $data leave entitlement information
     * @return \Illuminate\Http\JsonResponse Availability of leave entitlement name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/leave_entitlement/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check are leave entitlements in use
     *
     * @param array $data leave entitlements information
     * @return \Illuminate\Http\JsonResponse Count of leave entitlements in use
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            "/leave_entitlement/check_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get leave entitlements for employee and leave type
     *
     * @param int $employeeId Employee Id
     * @return \Illuminate\Http\JsonResponse LeaveEntitlement
     */
    public function getForEmployeeAndLeaveType(int $employeeId, string $query)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/leave_entitlement?{$query}"
        );
        return $this->send($request);
    }
}
