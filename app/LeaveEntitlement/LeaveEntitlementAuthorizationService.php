<?php

namespace App\LeaveEntitlement;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class LeaveEntitlementAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.leave_entitlement';
    const CREATE_TASK = 'create.leave_entitlement';
    const UPDATE_TASK = 'edit.leave_entitlement';
    const DELETE_TASK = 'delete.leave_entitlement';

    /**
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyLeaveEntitlements(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $leaveEntitlement, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveEntitlement, $user, self::DELETE_TASK);
    }

    /**
     * Authorize leaveEntitlement related tasks
     *
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $leaveEntitlement,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for leaveEntitlement account
            return $accountScope->inScope($leaveEntitlement->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as leaveEntitlement's account
                return $leaveEntitlement->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($leaveEntitlement->company_id);
        }

        return false;
    }
}
