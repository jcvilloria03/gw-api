<?php

namespace App\Task;

use App\Model\Task;

class TaskService
{
    const SUPERADMIN_ALLOWED_OWNER_TASKS = [
        'view.account',
        'create.company',
        'view.subscription_invoices',
    ];

    public function getOwnerTasks()
    {
        return Task::where([
            ['allowed_scopes', '=', Task::ACCOUNT_ONLY_SCOPE]
        ])->get();
    }

    public function getSuperAdminTasks()
    {
        return Task::whereIn(
            'name',
            self::SUPERADMIN_ALLOWED_OWNER_TASKS
        )->where([
            ['allowed_scopes', '=', Task::ACCOUNT_ONLY_SCOPE]
        ])->get();
    }

    public function getAdminTasks()
    {
        return Task::where([
            ['allowed_scopes', '!=', Task::ACCOUNT_ONLY_SCOPE],
            ['ess', '=', false]
        ])->get();
    }

    public function getEmployeeTasks()
    {
        return Task::where([
            ['ess', '=', true]
        ])->get();
    }

    public function getTasksByAttributeValues($attribute, $values)
    {
        return Task::whereIn($attribute, $values)->get();
    }

    public function getTasksArrangedByModules(array $moduleAccess = Task::MODULE_ACCESSES)
    {
        $tasks = Task::whereIn('module_access', $moduleAccess)
            ->orderBy('sort_id', 'ASC')
            ->get()
            ->groupBy('module');

        $tasks->transform(function ($item) {
            $result = [];
            $firstItem = $item->first();
            $submodules = collect($item)->groupBy('submodule');
            $submodules->transform(function ($submoduleItem) {
                $submoduleFirstItem = $submoduleItem->first();
                $submoduleTasks = collect($submoduleItem);
                $submoduleTasks->transform(function ($task) {
                    return [
                        'id' => $task->id,
                        'name' => $task->name,
                        'display_name' => $task->display_name,
                        'scopes' => $task->allowed_scopes,
                        'hidden' => !!$task->hidden
                    ];
                });

                return [
                    'name' => $submoduleFirstItem->submodule,
                    'tasks' => $submoduleTasks
                ];
            });

            $result['module'] = [
                'name' => $firstItem->module,
                'ess' => (boolean)$firstItem->ess,
                'submodules' => array_values($submodules->toArray()),
            ];

            return $result;
        });

        $result = array_values($tasks->toArray());

        return $result;
    }
}
