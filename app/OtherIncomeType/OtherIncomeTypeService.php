<?php

namespace App\OtherIncomeType;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class OtherIncomeTypeService
{
    const BASIS_FIXED = 'FIXED';
    const BASIS_SALARY_BASED = 'SALARY_BASED';

    const BASIS = [
        self::BASIS_FIXED,
        self::BASIS_SALARY_BASED,
    ];

    const BONUS_TYPE = 'bonus_type';
    const COMMISSION_TYPE = 'commission_type';
    const ALLOWANCE_TYPE = 'allowance_type';
    const DEDUCTION_TYPE = 'deduction_type';
    const ADJUSTMENT_TYPE = 'adjustment_type';

    const OTHER_INCOME_TYPES = [
        self::BONUS_TYPE,
        self::COMMISSION_TYPE,
        self::ALLOWANCE_TYPE,
        self::DEDUCTION_TYPE,
        self::ADJUSTMENT_TYPE,
    ];

    const AUTH_MODULES = [
        self::ADJUSTMENT_TYPE => [
            'employees.people.adjustments',
            'employees.adjustments',
        ],
        self::ALLOWANCE_TYPE => [
            'company_settings.company_payroll.allowance_types',
            'employees.people.allowances',
            'employees.allowances',
        ],
        self::BONUS_TYPE => [
            'company_settings.company_payroll.bonus_types',
            'employees.people.bonuses',
            'employees.bonuses',
            'payroll.bonuses',
        ],
        self::COMMISSION_TYPE => [
            'company_settings.company_payroll.commission_types',
            'employees.people.commissions',
            'employees.commissions',
            'payroll.commissions',
        ],
        self::DEDUCTION_TYPE => [
            'company_settings.company_payroll.deduction_types',
            'employees.people.deductions',
            'employees.deductions',
            'payroll.deductions' ,
        ],
    ];

    /**
     * @var OtherIncomeTypeRequestService
     */
    private $otherIncomeTypeRequestService;

    public function __construct(OtherIncomeTypeRequestService $otherIncomeTypeRequestService)
    {
        $this->otherIncomeTypeRequestService = $otherIncomeTypeRequestService;
    }

    public function isDeleteAvailable(int $companyId, array $otherIncomeTypeIds, array $modules = []): array
    {
        $types = [];
        foreach ($modules as $module) {
            $types[] = OtherIncomeTypeAuthorizationServiceFactory::AUTH_MODULE_TYPES[$module];
        }
        $response = $this->otherIncomeTypeRequestService->isDeleteAvailable($companyId, $otherIncomeTypeIds, $types);
        $data = json_decode($response->getData(true), true);
        $result = ['status_code' => Response::HTTP_OK, 'available' => $data['available']];

        if (!$data['available'] && !empty($data['ids'])) {
            $notInList = [];
            $unableToDelete = [];

            foreach ($otherIncomeTypeIds as $id) {
                if (!Arr::has($data['ids'], $id)) {
                    $notInList[] = $id;
                } elseif (!Arr::get($data['ids'], $id)) {
                    $unableToDelete[] = $id;
                }
            }

            if (!empty($notInList)) {
                $result['status_code'] = Response::HTTP_NOT_FOUND;
                $result['message'] = 'Specified Other Income Types not found. Ids: ' . implode(',', $notInList);
            } elseif (!empty($unableToDelete)) {
                $result['status_code'] = Response::HTTP_UNAUTHORIZED;
                $result['message'] = 'Not authorized. Ids: ' . implode(',', $unableToDelete);
            }
        }

        return $result;
    }
}
