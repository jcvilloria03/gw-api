<?php

namespace App\OtherIncomeType;

use App\AdjustmentType\AdjustmentTypeAuthorizationService;
use App\AllowanceType\AllowanceTypeAuthorizationService;
use App\BonusType\BonusTypeAuthorizationService;
use App\CommissionType\CommissionTypeAuthorizationService;
use App\DeductionType\DeductionTypeAuthorizationService;
use Illuminate\Support\Arr;

class OtherIncomeTypeAuthorizationServiceFactory
{
    const BONUS_TYPE = 'App\Model\PhilippineBonusType';
    const COMMISSION_TYPE = 'App\Model\PhilippineCommissionType';
    const ALLOWANCE_TYPE = 'App\Model\PhilippineAllowanceType';
    const DEDUCTION_TYPE = 'App\Model\PhilippineDeductionType';
    const ADJUSTMENT_TYPE = 'App\Model\PhilippineAdjustmentType';

    const AUTH_SERVICES = [
        self::BONUS_TYPE => BonusTypeAuthorizationService::class,
        self::COMMISSION_TYPE => CommissionTypeAuthorizationService::class,
        self::ALLOWANCE_TYPE => AllowanceTypeAuthorizationService::class,
        self::DEDUCTION_TYPE => DeductionTypeAuthorizationService::class,
        self::ADJUSTMENT_TYPE => AdjustmentTypeAuthorizationService::class,
    ];


    const AUTH_MODULE_TYPES = [
        'company_settings.company_payroll.bonus_types' => self::BONUS_TYPE,
        'company_settings.company_payroll.commission_types' => self::COMMISSION_TYPE,
        'company_settings.company_payroll.allowance_types' => self::ALLOWANCE_TYPE,
        'company_settings.company_payroll.deduction_types' => self::DEDUCTION_TYPE,
        'company_settings.company_payroll.adjustment_types' => self::ADJUSTMENT_TYPE,
    ];

    const AUTH_MODULES = [
        self::BONUS_TYPE => 'company_settings.company_payroll.bonus_types',
        self::COMMISSION_TYPE => 'company_settings.company_payroll.commission_types',
        self::ALLOWANCE_TYPE => 'company_settings.company_payroll.allowance_types',
        self::DEDUCTION_TYPE => 'company_settings.company_payroll.deduction_types',
        self::ADJUSTMENT_TYPE => 'company_settings.company_payroll.adjustment_types',
    ];

    public static function get($type)
    {
        if (empty(self::AUTH_SERVICES[$type])) {
            throw new \Exception('Other Income Type not found!');
        }

        return app()->make(self::AUTH_SERVICES[$type]);
    }

    public static function getModule($type): string
    {
        if (!Arr::has(self::AUTH_MODULES, $type)) {
            throw new \Exception("Other Income Type {$type} not found!");
        }

        return Arr::get(self::AUTH_MODULES, $type);
    }

    public function getTypeForModules(array $modules): array
    {
        $types = [];
        foreach ($modules as $module) {
            $types[] = self::AUTH_MODULE_TYPES[$module];
        }

        return $types;
    }
}
