<?php

namespace App\OtherIncomeType;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class OtherIncomeTypeRequestService extends RequestService
{
    /**
     * Call endpoint to get other income type.
     *
     * @param int $id Other Income Type ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function get(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/other_income_type/{$id}"
        ));
    }

    /**
     * Call endpoint to get multiple Other Income Types.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeTypesIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMultiple(int $companyId, array $otherIncomeTypesIds)
    {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/other_income_types",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeTypesIds])
        ));
    }

    /**
     * Call endpoint to get Other Income Types for Company.
     *
     * @param int $companyId Company ID
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCompanyIncomeTypes(int $companyId, string $type, array $queryString = [])
    {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/other_income_types/{$type}?" . http_build_query($queryString)
        ));
    }

    /**
     * Call endpoint to delete multiple Other Income Types.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeTypesIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMultiple(int $companyId, array $otherIncomeTypesIds)
    {
        return $this->send(new Request(
            'DELETE',
            "/company/{$companyId}/other_income_type",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeTypesIds])
        ));
    }

    /**
     * Call endpoint to check if editing is available
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function isEditAvailable(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/other_income_type/is_edit_available",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to check if delete is available.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeTypesIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function isDeleteAvailable(int $companyId, array $otherIncomeTypesIds, array $types = [])
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/other_income_type/is_delete_available",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeTypesIds, 'types' => $types])
        ));
    }

    /**
     * Get Other Income Type tax options based on type.
     *
     * @param string $type other income type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaxOptionsChoices(string $type)
    {
        return $this->send(new Request(
            'GET',
            "/other_income_type/tax_option_choices/{$type}/"
        ));
    }
}
