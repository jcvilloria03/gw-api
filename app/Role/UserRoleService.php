<?php

namespace App\Role;

use stdClass;
use App\Model\Role;
use App\Model\UserRole;
use App\Model\Auth0User;
use Dingo\Api\Auth\Auth;
use App\Task\TaskService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Company\CompanyAuthorizationService;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class UserRoleService
{
    const ADMIN = 'Admin';
    const EMPLOYEE = 'Employee';

    const OWNER = 'OWNER';
    const SUPER_ADMIN = 'SUPER ADMIN';

    const IS_USER_OWNER_OR_SUPER_ADMIN = [
        self::OWNER,
        self::SUPER_ADMIN
    ];

    const TYPE_OWNER = 'owner';
    const TYPE_SUPER_ADMIN = 'super admin';
    const TYPE_ADMIN = 'admin';
    const TYPE_EMPLOYEE = 'employee';

    /**
     * Assign rules to user
     *
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return UserRole::create($attributes);
    }

    /**
     * Assign rules to user
     *
     * @param array $attributes
     * @return mixed
     */
    public function insert(array $attributes)
    {
        return UserRole::insert($attributes);
    }

    /**
     *
     * Fetch Roles for a given User ID
     *
     * @param int $userId User ID
     * @param array $eagerLoadRelations
     * @return \Illuminate\Support\Collection $roles collection of App\Model\Role objects
     */
    public function getUserRoles(
        $userId,
        array $eagerLoadRelations = [],
        array $eagerConstraints = []
    ) {
        $userRolesQuery = UserRole::where('user_id', $userId);
        if ($eagerLoadRelations) {
            $userRolesQuery->with($eagerLoadRelations);
        }

        if ($eagerConstraints) {
            foreach ($eagerConstraints as $relationship => $constraint) {
                $userRolesQuery->whereHas($relationship, $constraint);
            }
        }

        return $userRolesQuery
            ->get();
    }

    /**
     *
     * Fetch Roles for a given User ID
     *
     * @param int $userId User ID
     * @param int $companyId Company Id
     * @param int $accountId Account Id
     * @return array $roles Array of App\Model\Role objects
     */
    public function getUserRolesInCompany(int $userId, int $companyId, int $accountId)
    {
        $userRolesQuery = UserRole::where('user_id', $userId)
            ->whereHas('role', function ($query) use ($companyId, $accountId) {
                $query->where('company_id', $companyId)
                    ->orWhere(function ($q) use ($accountId) {
                        $q->where('company_id', 0)
                            ->where('account_id', $accountId);
                    });
            });

        return $userRolesQuery
            ->get()
            ->map(function ($userRole) {
                return $userRole->role;
            })
            ->all();
    }

    /**
     *
     * Delete Roles for a given User ID
     *
     * @param int $userId User ID
     * @param int $userType User Type
     */
    public function deleteUserRoles(int $userId, $userType = null)
    {
        UserRole::where('user_id', $userId)
            ->whereHas('role', function ($q) use ($userType) {
                if ($userType === self::TYPE_OWNER) {
                    $q->where('name', '!=', Role::OWNER_NAME);
                }

                if ($userType === self::TYPE_SUPER_ADMIN) {
                    $q->where('name', '!=', Role::SUPERADMIN_NAME);
                }
            })
            ->delete();
    }

    /**
     * Return roles using given filter
     *
     * @param array $conditions Filter conditions
     * @return \Illuminate\Support\Collection $roles Collection of UserRole objects matching filter
     */
    public function getWithConditions(array $conditions)
    {
        return UserRole::where($conditions)->get();
    }

    /**
     * Get company id from scope with key Company.
     *
     * @param array $roles
     * @param \App\Task\TaskService $taskService
     * @return int
     */
    public function getCompanyIdForAdminRole(
        array $roles,
        TaskService $taskService,
        bool $getAll = false
    ) {
        $taskId = $taskService->getTasksByAttributeValues('name', [CompanyAuthorizationService::VIEW_TASK])
            ->pluck('id')
            ->first();
        $ids = [];
        foreach ($roles as $role) {
            if ($role->type === Role::ADMIN) {
                $companyPermission = collect($role->permissions)
                    ->where('task_id', $taskId)
                    ->where('role_id', $role->id);
                if (!$getAll) {
                    $companyPermission = $companyPermission->first(function ($permission) {
                        return array_key_exists('Company', $permission['scope']);
                    });

                    if ($companyPermission) {
                        return $companyPermission['scope']['Company'][0];
                    }
                } else {
                    $companyPermission->filter(function ($permission) {
                        return array_key_exists('Company', $permission['scope']);
                    })->each(function ($item) use (&$ids) {
                        $ids = array_merge($ids, $item['scope']['Company']);
                    });
                }
            }
        }
        return $ids;
    }

    /**
     * Check Role type is Owner or Super Admin
     *
     * @param array $roles
     * @return Role
     */
    public function isOwnerOrSuperAdmin(array $roles)
    {
        return collect($roles)
            ->whereIn('name', [Role::OWNER_NAME, Role::SUPERADMIN_NAME])
            ->first();
    }

    /**
     * Check Role type is admin
     *
     * @param array $roles
     * @return Role
     */
    public function isTypeAdmin(array $roles)
    {
        return !!collect($roles)->filter(function ($role) {
            return $role->type === Role::ADMIN &&
                !in_array($role->name, [Role::OWNER_NAME, Role::SUPERADMIN_NAME]);
        })->first();
    }

    /**
     * If a user is an owner or admin, get all companies, or
     * get all companies where role type is admin or employee.
     *
     * @param \stdClass $user
     * @param array $userRoles
     * @param string $type
     * @return array
     */
    public function getCompanies(stdClass $user, array $userRoles, string $type)
    {
        // Employee companies are specific so we do them first
        if ($type === self::EMPLOYEE) {
            return $this->getCompaniesForRoleType($user, $userRoles, $type);
        }

        if ($this->isUserOwnerOrSuperAdmin($user)) {
            return array_map(function ($company) {
                return [
                    'company_id' => $company->id,
                    'company_name' => $company->name
                ];
            }, $user->companies);
        }

        return $this->getCompaniesForRoleType($user, $userRoles, $type);
    }

    /**
     * Get basic company information based on user's roles and the type of role.
     *
     * @param \stdClass $user
     * @param array $userRoles
     * @param string $type
     * @return array
     */
    private function getCompaniesForRoleType(stdClass $user, array $userRoles, string $type)
    {
        $roles = collect($userRoles)->where('type', $type)->values();

        if ($roles->isEmpty()) {
            return [];
        }

        $companies = collect($user->companies);

        return $roles->map(function ($role) use ($companies) {
            $company = $companies->where('id', $role['company_id'])->first();

            return [
                'company_id' => $role['company_id'],
                'company_name' => data_get($company, 'name', '')
            ];
        })->all();
    }

    /**
     * Check is user owner or super admin.
     *
     * @param stdClass $user
     * @return bool
     */
    public function isUserOwnerOrSuperAdmin(stdClass $user)
    {
        return in_array(strtoupper($user->user_type), self::IS_USER_OWNER_OR_SUPER_ADMIN);
    }

    /**
     * Check User Role type is admin
     *
     * @param array $roles
     * @return Role
     */
    public function isUserAdmin(array $roles)
    {
        return collect($roles)
            ->whereIn('name', [self::ADMIN])
            ->first();
    }
    /**
     * Delete all user roles, and assign new roles.
     *
     * @param array  $userRoles
     * @param string $userId
     * @param string $userType
     */
    public function deleteAndInsert(array $userRoles, string $userId, $userType = null)
    {
        DB::transaction(function () use ($userRoles, $userId, $userType) {
            $this->deleteUserRoles($userId, $userType);
            $this->insert($userRoles);
        });
    }

    /**
     * Assign Owner or Super Admin Role to user.
     *
     * @param array $attributes
     * @param int $userId
     */
    public function assignOwnerOrSuperAdminRoleToUser(array $attributes, int $userId)
    {
        if (strtoupper($attributes['user_type']) === UserRoleService::OWNER) {
            $this->saveUserRole($attributes['account_id'], $userId, UserRoleService::OWNER);
        }

        if (strtoupper($attributes['user_type']) === UserRoleService::SUPER_ADMIN) {
            $this->saveUserRole($attributes['account_id'], $userId, UserRoleService::SUPER_ADMIN);
        }
    }

    /**
     * Save user role.
     *
     * @param int $accountId
     * @param int $userId
     * @param string $type
     */
    private function saveUserRole(int $accountId, int $userId, string $type)
    {
        $role = Role::where([
            ['account_id', $accountId],
            ['company_id', 0],
            ['name', $type]
        ])->first();

        if ($role) {
            $this->create(['role_id' => $role->id, 'user_id' => $userId]);
        }
    }

    /**
     *
     * Check if user has owner role for a given User ID
     *
     * @param int $userId User ID
     * @return bool
     */
    public static function hasOwnerRole(int $userId)
    {
        return UserRole::where('user_id', $userId)
                ->whereHas('role', function ($q) {
                    $q->where('name', Role::OWNER_NAME);
                })->count() > 0;
    }

    /**
     *
     * Check if user has super admin role for a given User ID
     *
     * @param int $userId User ID
     * @return bool
     */
    public static function hasSuperAdminRole(int $userId)
    {
        return UserRole::where('user_id', $userId)
                ->whereHas('role', function ($q) {
                    $q->where('name', Role::SUPERADMIN_NAME);
                })->count() > 0;
    }

    /**
     *
     * Check if user has admin role for a given User ID
     *
     * @param int $userId User ID
     * @return bool
     */
    public static function hasAdminRole(int $userId)
    {
        return UserRole::where('user_id', $userId)
                ->whereHas('role', function ($q) {
                    $q->where('type', Role::ADMIN);
                })->count() > 0;
    }

    /**
     * Calculate assigned users to given role and, total users for account.
     *
     * @param int  $accountId
     * @param Role $role
     * @return string
     */
    public function getTotalAssignedUsers(int $accountId, Role $role)
    {
        $totalUsers = Auth0User::where('account_id', $accountId)
            ->when($role->type === Role::ADMIN, function ($q) use ($role) {
                $q->whereDoesntHave('userRoles.role', function ($query) use ($role) {
                    $query->where('name', Role::OWNER_NAME)
                        ->orWhere('name', Role::SUPERADMIN_NAME);
                });
            })
            ->rightJoin('user_roles', function ($join) use ($role) {
                $join->on('auth0_users.user_id', '=', 'user_roles.user_id')
                    ->where('user_roles.role_id', $role->id);
            })->get();

        $totalUsersCount = $totalUsers->count();
        $activeUsersCount =  $totalUsers->where('status', Auth0User::STATUS_ACTIVE)->count();

        return $activeUsersCount . '/' . $totalUsersCount . ' Users';
    }

    /**
     * Update user role module access
     *
     * @param int   $userId
     * @param array $newModuleAccessList
     * @return void
     */
    public function updateUserRolesModuleAccess(int $userId, array $newModuleAccessList)
    {
        // Make sure default HRIS module access is always present in the module access list
        if (!in_array(Role::ACCESS_MODULE_HRIS, $newModuleAccessList)) {
            $newModuleAccessList = Arr::prepend($newModuleAccessList, Role::ACCESS_MODULE_HRIS);
        }
        // update
        UserRole::where('user_id', $userId)
            ->update(['module_access' => json_encode($newModuleAccessList)]);
    }

        /**
     * Update account's roles
     *
     * @param int   $accountId
     * @param array $newModuleAccessList
     * @return void
     */
    public function updateRolesByAccountId(int $accountId, array $newModuleAccessList)
    {
        // Make sure default HRIS module access is always present in the module access list
        if (!in_array(Role::ACCESS_MODULE_HRIS, $newModuleAccessList)) {
            $newModuleAccessList = Arr::prepend($newModuleAccessList, Role::ACCESS_MODULE_HRIS);
        }
        // Update roles
        Role::where('account_id', $accountId)
            ->update(['module_access' => json_encode($newModuleAccessList)]);
    }

    /**
     * Updates all users under the account Id
     *
     * @param int $accountId
     * @return array $newModuleAccessList
     */
    public function updateUserRolesByAccountId(int $accountId, array $newModuleAccessList, int $ownerId)
    {
        // Make sure default HRIS module access is always present in the module access list
        if (!in_array(Role::ACCESS_MODULE_HRIS, $newModuleAccessList)) {
            $newModuleAccessList = Arr::prepend($newModuleAccessList, Role::ACCESS_MODULE_HRIS);
        }
        //Get list of users to update
        $usersToUpdate = UserRole::whereHas('role', function ($query) use ($accountId, $ownerId) {
            $query->where('account_id', '=', $accountId);
            //Don't include owner when updating employees
            $query->where('user_id', '<>', $ownerId);
        })->pluck('user_id')->toArray();

        //Update user roles
        if ($usersToUpdate) {
            UserRole::whereIn('user_id', $usersToUpdate)
                ->update(['module_access' => json_encode($newModuleAccessList)]);
        }
        return $usersToUpdate;
    }

    public function getUsersByAccountId(int $accountId)
    {
        return UserRole::whereHas('role', function ($query) use ($accountId) {
            $query->where('account_id', '=', $accountId);
        })->pluck('user_id')->toArray();
    }

    /**
     * Returns user role type by given user id
     *
     * @param int $userId
     *
     * @return string
     */
    public function getUserRoleType(int $userId)
    {
        if ($this->hasOwnerRole($userId)) {
            return strtolower(self::OWNER);
        }

        if ($this->hasSuperAdminRole($userId)) {
            return strtolower(Role::SUPERADMIN_NAME);
        }

        if ($this->hasAdminRole($userId)) {
            return strtolower(self::ADMIN);
        }

        return strtolower(self::EMPLOYEE);
    }
}
