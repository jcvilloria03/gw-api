<?php

namespace App\Role;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class RoleAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.role';
    const CREATE_TASK = 'create.role';
    const DELETE_TASK = 'delete.role';
    const UPDATE_TASK = 'edit.role';

    /**
     * @param \stdClass $role
     * @param array $user
     * @return bool
     */
    public function authorizeGetAccountRoles(\stdClass $role, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        // verify account scope
        return $this->authorizeTask($role, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $role
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $role, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        // verify account scope
        return $this->authorizeTask($role, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $role
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $role, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        // verify account scope
        return $this->authorizeTask($role, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $data
     * @param array $user
     * @return bool
     */
    public function authorizeDeleteAccountRoles(\stdClass $data, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($data, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $data
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $data, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($data, $user, self::CREATE_TASK);
    }

    /**
     * Authorize role related tasks
     *
     * @param \stdClass $role
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $role,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for role's account
            return $accountScope->inScope($role->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as role's account
                return $role->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($role->company_id);
        }

        return false;
    }

}
