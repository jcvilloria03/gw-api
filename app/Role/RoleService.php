<?php

namespace App\Role;

use App\Model\Role;
use App\Model\UserRole;
use App\Model\AuthnUser;
use App\Authn\AuthnService;
use Illuminate\Validation\Rule;
use App\Permission\PermissionService;
use App\Task\TaskService;

class RoleService
{
    const CUSTOM_ROLE_ATTRIBUTES = [
        'custom_role' => true,
        'type' => null,
    ];

    const COMMON_VALIDATION_ATTRIBUTES = [
        'role_ids.*' => 'Role Ids',
        'permissions.*' => 'Permissions'
    ];

    const COMMON_VALIDATION_MESSAGES = [
        'role_ids.*.exists' => 'Given Role IDs contain invalid ID.',
        'permissions.*.exists' => 'Given Permissions contain invalid permission.'
    ];

    /*
     * App\Task\TaskService
     */
    private $taskService;

    /*
     * App\Permission\PermissionService
     */
    private $permissionService;

    public function __construct(
        TaskService $taskService,
        PermissionService $permissionService
    ) {
        $this->taskService = $taskService;
        $this->permissionService = $permissionService;
    }

    /**
     * Get role
     *
     * @param int $id Role ID
     * @return \App\Model\Role
     *
     */
    public function get($id)
    {
        return Role::find($id);
    }

    /**
     * Create role
     *
     * @param array $attributes Role data
     * @return Role $role
     */
    public function create(array $attributes)
    {
        return Role::create($attributes);
    }

    /**
     * Create custom role
     *
     * @param array $attributes Role details
     * @return \App\Model\Role
     */
    public function createCustomRole(array $attributes)
    {
        $role = $this->create(
            array_merge(
                $attributes,
                self::CUSTOM_ROLE_ATTRIBUTES
            )
        );

        $this->createRolePermissions($role, $attributes);

        return $role;
    }

    /**
     * Update role
     *
     * @param int $id Role ID
     * @param array $attributes Holiday attributes
     * @return \App\Model\Role
     */
    public function update(int $id, array $attributes)
    {
        $role = Role::findOrFail($id);
        $role->update(
            array_merge(
                $attributes,
                [
                    'company_id' => $attributes['company_id'] ?? 0,
                ]
            )
        );

        if (isset($attributes['tasks'])) {
            \DB::transaction(function () use ($role, $attributes) {
                $role->permissions()->delete();
                $this->createRolePermissions($role, $attributes);
            });
        }

        return $role;
    }

    /**
     * Return roles using given filter
     *
     * @param array $conditions Filter conditions
     * @return Collection $roles Collection of Role objects matching filter
     */
    public function getWithConditions(array $conditions)
    {
        return Role::where($conditions)->get();
    }

    /**
     * Fetch Roles with the same account ID
     *
     * @param int $accountId Account ID
     * @param bool $excludeSystemDefinedRoles Exclude System Defined Roles
     * @param int $companyId Company id
     * @param bool $adminOnly Include admin only roles
     * @return Collection $roles Collection of Role objects
     */
    public function getRoles(
        int $accountId,
        bool $excludeSystemDefinedRoles = false,
        bool $adminOnly = true,
        int $companyId = null
    ) {
        $conditions = [
            ['account_id', $accountId]
        ];

        if ($companyId) {
            $conditions = array_merge($conditions, [['company_id', $companyId]]);
        }

        if ($excludeSystemDefinedRoles) {
            $conditions = array_merge($conditions, [['custom_role', true]]);
        }

        if ($adminOnly) {
            $conditions = array_merge($conditions, [['type', Role::ADMIN]]);
        }

        return $this->getWithConditions($conditions);
    }

    /**
     * Fetch All Roles with the same account ID
     *
     * @param int $accountId Account ID
     * @return \Illuminate\Support\Collection $roles Collection of Role objects
     */
    public function getAllAccountRoles(int $accountId)
    {
        return $this->getWithConditions([
            'account_id' => $accountId
        ]);
    }

    /**
     * Check are roles in use
     *
     * @param int $accountId Account ID
     * @return Collection $roles Collection of Role objects
     */
    public function checkInUse(int $accountId, array $roleIds, AuthnService $authnService)
    {
        $userRoles = UserRole::leftJoin('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.account_id', $accountId)
            ->whereIn('user_roles.role_id', $roleIds)
            ->get();
        $userIds = $userRoles->pluck('user_id')->all();
        $users = $authnService->getActiveUserIds($accountId, $userIds);

        $countInUse = collect($users)->filter(function ($user) {
            return !empty($user['status'] && $user['status'] === AuthnUser::STATUS_ACTIVE);
        })->count();

        return $countInUse;
    }

    /**
     * Delete all roles for given id's and account ID
     *
     * @param int $accountId Account ID
     * @param array $roleIds Role id's
     * @return array $roleIds Array of role id's that are deleted
     */
    public function bulkDelete(int $accountId, array $roleIds)
    {
        Role::where('account_id', $accountId)->whereIn('id', $roleIds)->delete();

        return $roleIds;
    }

    /**
     * Returns validation rules for methods that receives role id's in request for given account ID
     *
     * @param int $accountId Account ID
     * @return array $rules generated validation rules
     */
    public function getCommonValidationRulesForRoleIds(int $accountId)
    {
        return [
            'role_ids.*' => [
                'required',
                Rule::exists('roles', 'id')->where(function ($query) use ($accountId) {
                    $query->where('account_id', $accountId)->where('custom_role', true);
                })
            ],
            'role_ids' => 'required|array'
        ];
    }

    /**
     * Create role permissions
     *
     * @param \App\Model\Role $role Role
     * @param array $attributes Role attributes
     */
    private function createRolePermissions(Role $role, array $attributes)
    {
        if (isset($attributes['company_id'])) {
            $scope = '[' . $attributes['company_id'] . ']';
        } else {
            $scope = 'all';
        }

        foreach ($attributes['tasks'] as $task) {
            $this->permissionService->create([
                'task_id' => $task,
                'role_id' => $role->id,
                'scope' => [
                    'Company' => $scope
                ]
            ]);
        }
    }

    /**
     * Return all company roles.
     *
     * @param string $accountId
     * @param string $companyId
     * @return Collection
     */
    public function getCompanyRoles(string $accountId, string $companyId)
    {
        return Role::where('account_id', $accountId)->where('company_id', $companyId)->get();
    }

    /**
     * Check is name available.
     *
     * @param array $inputs
     * @return bool
     */
    public function isNameAvailable(array $inputs)
    {
        $role = Role::where('name', trim($inputs['name']))->where(
            'account_id',
            $inputs['account_id']
        )->first();

        if ($role && array_key_exists('company_id', $inputs)) {
            return (($role->company_id == $inputs['company_id']) || ($role->company_id == 0));
        } elseif ($role) {
            return true;
        }

        return false;
    }
}
