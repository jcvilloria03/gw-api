<?php

namespace App\Role;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class RoleRequestService extends RequestService
{
    const ROLE_OWNER = 'OWNER';

    const ROLE_SUPER_ADMIN = 'SUPER_ADMIN';

    const ROLE_ADMIN = 'ADMIN';

    const ROLE_EMPLOYEE = 'EMPLOYEE';

    const ROLE_MAPPING = [
        [
            'name_pattern' => '/^Salarium Account Owner$/',
            'role_type' => self::ROLE_OWNER
        ],
        [
            'name_pattern' => '/^Salarium Account Super Admin$/',
            'role_type' => self::ROLE_SUPER_ADMIN
        ],
        [
            'name_pattern' => '/^Salarium Company \[(\d+)\] Admin$/',
            'role_type' => self::ROLE_ADMIN
        ],
        [
            'name_pattern' => '/^Salarium Account Owner with ESS$/',
            'role_type' => self::ROLE_OWNER
        ],
        [
            'name_pattern' => '/^Salarium Account Super Admin with ESS$/',
            'role_type' => self::ROLE_SUPER_ADMIN
        ],
        [
            'name_pattern' => '/^Salarium Company \[(\d+)\] Admin with ESS$/',
            'role_type' => self::ROLE_ADMIN
        ],
        [
            'name_pattern' => '/^Salarium Company \[(\d+)\] Non Admin$/',
            'role_type' => self::ROLE_EMPLOYEE
        ]
    ];

    const ROLE_NAME_REGEX_ALL_EXCEPT_ESS = '/^(?!ess|root\.ess).*/';

    const ROLE_NAME_REGEX_ONLY_ESS = '/^(ess|root\.ess).*/';

    const ROLE_NAME_REGEX_WITH_MAIN_PAGE = '/^(main_page).*/';

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Get roles by company
     *
     * @param  int    $companyId Company ID
     * @return array
     */
    public function getRolesByCompany(int $companyId, $queryParams = null)
    {
        $url = sprintf('/companies/%d/roles', $companyId);

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        $request = new Request('GET', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Get single role by company
     *
     * @param  int    $companyId Company ID
     * @param  int    $roleId    Role ID
     * @return array
     */
    public function getRoleByCompany(int $companyId, int $roleId)
    {
        $url = sprintf('/companies/%d/roles/%d', $companyId, $roleId);

        $request = new Request('GET', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Create role by company
     *
     * @param  int    $companyId Company ID
     * @param  array  $data      Role data
     * @return array
     */
    public function createRoleByCompany(int $companyId, array $data)
    {
        $url = sprintf('/companies/%d/roles', $companyId);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $request = new Request('POST', $url, $headers, json_encode($data));
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Update role by company
     *
     * @param  int    $companyId Company ID
     * @param  int    $roleId    Role ID
     * @param  array  $data      Role data
     * @return array
     */
    public function updateRoleByCompany(int $companyId, int $roleId, array $data)
    {
        $url = sprintf('/companies/%d/roles/%d', $companyId, $roleId);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $request = new Request('PUT', $url, $headers, json_encode($data));
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Delete role by company
     *
     * @param  int    $companyId Company ID
     * @param  int    $roleId    Role ID
     * @return array
     */
    public function deleteRoleByCompany(int $companyId, int $roleId)
    {
        $url = sprintf('/companies/%d/roles/%d', $companyId, $roleId);

        $request = new Request('DELETE', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Get roles by account
     *
     * @param  int    $accountId Account ID
     * @return array
     */
    public function getRolesByAccount(int $accountId, $queryParams = null)
    {
        $url = sprintf('/accounts/%d/roles', $accountId);

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        $request = new Request('GET', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Get single role by account
     *
     * @param  int      $accountId        Account ID
     * @param  int      $roleId           Role ID
     * @param  boolean  $isDotNotation    Get resources as dot notation
     * @return array
     */
    public function getRoleByAccount(int $accountId, int $roleId, bool $isDotNotation = false)
    {
        $url = sprintf(
            '/accounts/%d/roles/%d?dot_notation=%d',
            $accountId,
            $roleId,
            intval($isDotNotation)
        );

        $request = new Request('GET', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Create role by account
     *
     * @param  int    $accountId Account ID
     * @param  array  $data      Role data
     * @return array
     */
    public function createRoleByAccount(int $accountId, array $data)
    {
        $url = sprintf('/accounts/%d/roles', $accountId);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $request = new Request('POST', $url, $headers, json_encode($data));
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Update role by account
     *
     * @param  int    $accountId Account ID
     * @param  int    $roleId    Role ID
     * @param  array  $data      Role data
     * @return array
     */
    public function updateRoleByAccount(int $accountId, int $roleId, array $data)
    {
        $url = sprintf('/accounts/%d/roles/%d', $accountId, $roleId);

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $request = new Request('PUT', $url, $headers, json_encode($data));
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Delete role by account
     *
     * @param  int    $accountId Account ID
     * @param  int    $roleId    Role ID
     * @return array
     */
    public function deleteRoleByAccount(int $accountId, int $roleId)
    {
        $url = sprintf('/accounts/%d/roles/%d', $accountId, $roleId);

        $request = new Request('DELETE', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Get role type by role name
     *
     * @param  array $roleData Role data
     * @return string
     */
    public function getRoleType(array $roleData)
    {
        if ($roleData['is_system_defined']) {
            foreach (self::ROLE_MAPPING as $roleMap) {
                $matches = preg_match($roleMap['name_pattern'], $roleData['name']);

                if ($matches) {
                    return $roleMap['role_type'];
                }
            }
        }

        $modules = array_keys($roleData['policy']['resource']);

        $hasAdminModules = !empty(preg_grep(self::ROLE_NAME_REGEX_WITH_MAIN_PAGE, $modules));

        if ($hasAdminModules) {
            return self::ROLE_ADMIN;
        }

        $hasEmployeeModules = !empty(preg_grep(self::ROLE_NAME_REGEX_ONLY_ESS, $modules));

        if ($hasEmployeeModules) {
            return self::ROLE_EMPLOYEE;
        }

        return null;
    }
}
