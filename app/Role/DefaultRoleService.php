<?php

namespace App\Role;

use App\Model\Role;
use App\Permission\PermissionService;
use App\Permission\Scope;
use App\Permission\TargetType;
use App\Task\TaskService;
use App\User\UserRequestService;

class DefaultRoleService
{
    /**
     *  @var \App\Role\RoleService
     */
    private $roleService;

    /**
     *  @var \App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     *  @var \App\Permission\PermissionService
     */
    protected $permissionService;

    /**
     *  @var \App\Task\TaskService
     */
    private $taskService;

    /**
     * @var \App\User\UserRequestService
     */
    private $userRequestService;

    public function __construct(
        RoleService $roleService,
        UserRoleService $userRoleService,
        PermissionService $permissionService,
        TaskService $taskService,
        UserRequestService $userRequestService
    ) {
        $this->roleService = $roleService;
        $this->userRoleService = $userRoleService;
        $this->permissionService = $permissionService;
        $this->taskService = $taskService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Create OWNER role for account
     *
     * @param int $accountId Account ID
     * @param array $selectedModules Selected access modules, defaults to HRIS and T&A
     * @return Role $role
     */
    public function createOwnerRole(int $accountId, array $selectedModules = Role::DEFAULT_MODULE_ACCESSES)
    {
        $role = $this->roleService->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => Role::OWNER_NAME,
            'type' => Role::ADMIN,
            'module_access' => $selectedModules
        ]);
        $tasks = $this->taskService->getOwnerTasks();
        $tasks
            ->merge($this->taskService->getAdminTasks())
            ->each(function ($item) use ($role) {
                $this->permissionService->create([
                    'task_id' => $item->id,
                    'role_id' => $role->id,
                    'scope' => [TargetType::ACCOUNT => [$role->account_id]],
                ]);
            });
        return $role;
    }

    /**
     * Create SUPER ADMIN role for account
     *
     * @param int $accountId Account ID
     * @param array $selectedModules Selected access modules, defaults to HRIS and T&A
     * @return Role $role
     */
    public function createSuperAdminRole(int $accountId, array $selectedModules = Role::DEFAULT_MODULE_ACCESSES)
    {
        $role = $this->roleService->create([
            'account_id' => $accountId,
            'company_id' => 0,
            'name' => Role::SUPERADMIN_NAME,
            'type' => Role::ADMIN,
            'module_access' => $selectedModules
        ]);

        // Create Account scope tasks
        // whitelisted for super admins
        $tasks = $this->taskService->getSuperAdminTasks();
        $tasks->each(function ($item) use ($role) {
            $this->permissionService->create([
                'task_id' => $item->id,
                'role_id' => $role->id,
                'scope' => [TargetType::ACCOUNT => [$role->account_id]],
            ]);
        });

        // Create Company scope tasks
        $tasks = $this->taskService->getAdminTasks();
        $tasks->each(function ($item) use ($role) {
            $this->permissionService->create([
                'task_id' => $item->id,
                'role_id' => $role->id,
                'scope' => [TargetType::COMPANY => Scope::TARGET_ALL],
            ]);
        });

        return $role;
    }

    /**
     * Get OWNER role for account
     *
     * @param int $accountId Account ID
     * @return Role $role
     */
    public function getOwnerRole(int $accountId)
    {
        return $this->roleService->getWithConditions([
            'account_id' => $accountId,
            'name' => Role::OWNER_NAME,
        ])->first();
    }

    /**
     * Get SuperAdmin role for account
     *
     * @param int $accountId Account ID
     * @return Role $role
     */
    public function getSuperAdminRole(int $accountId)
    {
        return $this->roleService->getWithConditions([
            'account_id' => $accountId,
            'name' => Role::SUPERADMIN_NAME,
        ])->first();
    }

    /**
     * Create COMPANY ADMIN role for account
     *
     * @param int $accountId Account ID
     * @param array $company Company Information (id, name)
     * @param array $selectedModules Selected access modules, defaults to HRIS and T&A
     * @return Role $role
     */
    public function createAdminRole(
        int $accountId,
        array $company,
        array $selectedModules = Role::DEFAULT_MODULE_ACCESSES
    ) {
        $role = $this->roleService->create([
            'account_id' => $accountId,
            'company_id' => $company['id'],
            'name' => Role::ADMIN,
            'type' => Role::ADMIN,
            'module_access' => $selectedModules
        ]);

        $tasks = $this->taskService->getAdminTasks();

        $tasks->each(function ($item) use ($role, $company) {
            $this->permissionService->create([
                'task_id' => $item->id,
                'role_id' => $role->id,
                'scope' => [TargetType::COMPANY => [$company['id']]],
            ]);
        });

        $this->assignAdminRoleToOwnerAndSuperAdmin($role, $accountId);

        return $role;
    }

    /**
     * Create ADMIN role
     *
     * @param int $accountId Account ID
     * @param array $company Company Information (id, name)
     * @param array $selectedModules Selected access modules, defaults to HRIS and T&A
     * @return Role $role
     */
    public function createOnlyAdminRole(
        int $accountId,
        array $company,
        array $selectedModules = Role::DEFAULT_MODULE_ACCESSES
    ) {
        $role = $this->roleService->create([
            'account_id' => $accountId,
            'company_id' => $company['id'],
            'name' => Role::ADMIN,
            'type' => Role::ADMIN,
            'module_access' => $selectedModules
        ]);

        $tasks = $this->taskService->getAdminTasks();

        $tasks->each(function ($item) use ($role, $company) {
            $this->permissionService->create([
                'task_id' => $item->id,
                'role_id' => $role->id,
                'scope' => [TargetType::COMPANY => [$company['id']]],
            ]);
        });

        return $role;
    }

    /**
     * Assign owner and super admin as admins of newly created company.
     *
     * @param \App\Model\Role $companyAdminRole
     * @param int $accountId
     * @return void
     */
    private function assignAdminRoleToOwnerAndSuperAdmin(Role $companyAdminRole, int $accountId)
    {
        $adminsResponse = $this->userRequestService->getAccountOwnerAndSuperAdmins($accountId);

        if ($adminsResponse->isSuccessful()) {
            $admins = array_get(json_decode($adminsResponse->getData(), true), 'data', []);
            $adminIds = array_pluck($admins, 'id');

            foreach ($adminIds as $adminId) {
                $companyAdminRole->userRoles()->create(['user_id' => $adminId]);
            }
        }
    }

    /**
     * Create Employee role for company
     *
     * @param int $accountId Account ID
     * @param array $company Company Information (id, name)
     * @param array $selectedModules Selected access modules, defaults to HRIS and T&A
     * @return Role $role
     */
    public function createCompanyEmployeeRole(
        int $accountId,
        array $company,
        array $selectedModules = Role::DEFAULT_MODULE_ACCESSES
    ) {
        $role = $this->roleService->create([
            'account_id' => $accountId,
            'company_id' => $company['id'],
            'name' => Role::EMPLOYEE,
            'type' => Role::EMPLOYEE,
            'module_access' => $selectedModules
        ]);
        $tasks = $this->taskService->getEmployeeTasks();
        $tasks->each(function ($item) use ($role, $company) {
            $this->permissionService->create([
                'task_id' => $item->id,
                'role_id' => $role->id,
                'scope' => (object) null, // stored as '{}'
            ]);
        });
        return $role;
    }

    /**
     * Get Employee role for Company
     *
     * @param int $companyId Co ID
     * @return Role $role
     */
    public function getCompanyEmployeeRole(int $companyId)
    {
        return $this->roleService->getWithConditions([
            'company_id' => $companyId,
            'type' => Role::EMPLOYEE,
        ])->first();
    }

    /**
     * Fetch list of OWNERS for account
     *
     * @param int $accountId Account ID
     * @return Collection $role
     */
    public function getAccountOwners(int $accountId)
    {
        $ownerRole = $this->getOwnerRole($accountId);
        return $this->userRoleService->getWithConditions([
            'role_id' => $ownerRole->id
        ]);
    }

    /**
     * Check if given user is Account Owner
     *
     * @param int $accountId Account ID
     * @param int $userId User ID
     * @return boolean
     */
    public function isUserOwner(int $accountId, int $userId)
    {
        $ownerRole = $this->getOwnerRole($accountId);
        $userOwnerRole = $this->userRoleService->getWithConditions([
            'role_id' => $ownerRole->id,
            'user_id' => $userId,
        ]);
        return !$userOwnerRole->isEmpty();
    }

    /**
     * Return Employee System defined role.
     *
     * @param string $accountId
     * @param string $companyId
     *
     * @return Collection
     */
    public function getEmployeeSystemRole(string $accountId, string $companyId)
    {
        return Role::where('account_id', $accountId)
            ->where('custom_role', 0)
            ->where('type', Role::EMPLOYEE)
            ->where('company_id', $companyId)
            ->first();
    }
}
