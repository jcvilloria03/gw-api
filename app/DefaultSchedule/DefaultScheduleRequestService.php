<?php

namespace App\DefaultSchedule;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class DefaultScheduleRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to index default schedules.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Default Schedules
     */
    public function index(int $companyId)
    {
        $request = new Request(
            'GET',
            "/default_schedule?company_id={$companyId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create multiple default schedules.
     *
     * @param array $data Default Schedules information
     * @return \Illuminate\Http\JsonResponse Created Default Schedules
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/default_schedule/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update multiple default schedules.
     *
     * @param array $data Default Schedules information
     * @return \Illuminate\Http\JsonResponse Updated Default Schedules
     */
    public function bulkUpdate(array $data)
    {
        $request = new Request(
            'PUT',
            "/default_schedule/bulk_update",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
