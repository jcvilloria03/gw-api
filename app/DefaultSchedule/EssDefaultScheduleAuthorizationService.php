<?php

namespace App\DefaultSchedule;

use App\Authorization\EssBaseAuthorizationService;

class EssDefaultScheduleAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.default_schedule';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeView(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
