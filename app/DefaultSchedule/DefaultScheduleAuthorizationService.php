<?php

namespace App\DefaultSchedule;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class DefaultScheduleAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.default_schedule';
    const CREATE_TASK = 'create.default_schedule';
    const UPDATE_TASK = 'edit.default_schedule';

    /**
     * @param \stdClass $defaultSchedule
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $defaultSchedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($defaultSchedule, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $defaultSchedule
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $defaultSchedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($defaultSchedule, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $defaultSchedule
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $defaultSchedule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($defaultSchedule, $user, self::UPDATE_TASK);
    }

    /**
     * Authorize default schedule related tasks
     *
     * @param \stdClass $defaultSchedule
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $defaultSchedule,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for department's account
            return $accountScope->inScope($defaultSchedule->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as departments's account
                return $defaultSchedule->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($defaultSchedule->company_id);
        }

        return false;
    }
}
