<?php

namespace App\Rank;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class RankRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get rank info
     *
     * @param int $id Rank ID
     * @return \Illuminate\Http\JsonResponse Rank Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/rank/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create rank
     *
     * @param array $data rank information
     * @return \Illuminate\Http\JsonResponse Created Rank
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/rank",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if rank name is available
     *
     * @param int $companyId Company ID
     * @param array $data rank information
     * @return \Illuminate\Http\JsonResponse Availability of rank name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/rank/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all rank within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company rank
     */
    public function getCompanyRanks(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/ranks"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create multiple ranks
     *
     * @param array $data ranks information
     * @return \Illuminate\Http\JsonResponse Created Rank ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/rank/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update rank for given id
     *
     * @param array $data rank informations
     * @param int $id Rank Id
     * @return \Illuminate\Http\JsonResponse Updated rank
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/rank/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
      * Call endpoint to delete multiple ranks
      *
      * @param array $data ranks to delete informations
      * @return \Illuminate\Http\JsonResponse Deleted ranks id's
      */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/rank/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
