<?php

namespace App\Rank;

use App\ES\ESIndexQueueService;

class RankEsIndexQueueService
{
    const TYPE = 'rank';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created ranks for indexing
     * @param array $rankIds Rank Ids to be indexed
     */
    public function queue(array $rankIds)
    {
        $details = [
            'id' => $rankIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
