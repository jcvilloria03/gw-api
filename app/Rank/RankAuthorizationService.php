<?php

namespace App\Rank;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class RankAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.rank';
    const CREATE_TASK = 'create.rank';
    const UPDATE_TASK = 'edit.rank';
    const DELETE_TASK = 'delete.rank';

    /**
     * @param \stdClass $rank
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $rank
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyRanks(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $rank
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $rank
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $rank
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::UPDATE_TASK);
    }

     /**
      * @param \stdClass $rank
      * @param array $user
      * @return bool
      */
    public function authorizeDelete(\stdClass $rank, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($rank, $user, self::DELETE_TASK);
    }


    /**
     * Authorize rank related tasks
     *
     * @param \stdClass $rank
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $rank,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for rank's account
            return $accountScope->inScope($rank->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as ranks's account
                return $rank->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($rank->company_id);
        }

        return false;
    }
}
