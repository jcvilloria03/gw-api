<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\Traits\AuditTrailTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class AuditTrailConsumer extends Consumer
{
    use AuditTrailTrait;

    public function __construct()
    {
        parent::__construct(config('queues.audit_queue'));
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (empty($details['route_uri'])) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        try {
            $this->publishAuditLog($details);
        } catch (\Throwable $th) {
            Log::error(
                "[AuditTrailConsumer][ERROR]|" . $th->getMessage()
            );
            Log::error($th->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }
}
