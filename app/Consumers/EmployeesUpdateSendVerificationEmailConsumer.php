<?php

namespace App\Consumers;

use App\Auth0\Auth0UserService;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Auth0\Auth0ManagementService;

class EmployeesUpdateSendVerificationEmailConsumer extends Consumer
{
    /**
     * Construct employee upload response consumer
     */
    public function __construct()
    {
        parent::__construct(config('queues.employees_update_send_verification_email_queue'));
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (empty($details['auth0_user_ids'])) {
            $resolver->acknowledge($message);
            return;
        }

        $this->sendVerificationEmails($details['auth0_user_ids']);

        $resolver->acknowledge($message);
    }

    /**
     * Send verification emails to all auth0 user ids
     * @param array $auth0UserIds
     */
    protected function sendVerificationEmails(array $auth0UserIds)
    {
        $auth0 = app(Auth0ManagementService::class);

        foreach ($auth0UserIds as $userId) {
            $jobDetails = $auth0->createEmailVerificationJob($userId);

            Log::info('Sent verification email to auth0 user: ' . $userId, $jobDetails);
        }
    }
}
