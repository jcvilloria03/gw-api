<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;

use App\OtherIncome\OtherIncomeAuditService;
use App\OtherIncome\OtherIncomeUploadTask;
use App\User\UserRequestService;

class OtherIncomeUploadResponseConsumer extends Consumer
{
    /**
     * App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(
        OtherIncomeAuditService $auditService,
        UserRequestService $userRequestService
    ) {
        parent::__construct(config('queues.other_income_upload_response_queue'));
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Generate upload other income task.
     */
    public function generateTask()
    {
        return App::make(OtherIncomeUploadTask::class);
    }

    /**
     * Validate message and pass it to the upload other income task.
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['company_id'], $details['id']);
        if ($details['task'] === OtherIncomeUploadTask::PROCESS_SAVE) {
            $task->updateSaveStatus($details['status']);
            if (!empty($details['other_income_ids'])) {
                $response = $this->userRequestService->get($task->getUserId());
                $userData = json_decode($response->getData(), true);
                $this->auditService->logBatchCreate(
                    array_merge([
                        'user_id' => $task->getUserId(),
                        'account_id' => array_get($userData, 'account_id')
                    ], $details)
                );
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
