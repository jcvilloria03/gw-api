<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\Payroll\PayslipGenerationTask;

class PayslipGenerationResponseConsumer extends Consumer {

    public function __construct()
    {
        parent::__construct(config('queues.payslip_generation_response_queue'));
    }

    /**
     *  Generate Task
     *
     */
    public function generateTask()
    {
        return App::make(PayslipGenerationTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['payroll_id']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $jobId = PayslipGenerationTask::ID_PREFIX . $details['payroll_id'];
        $task->create($details['payroll_id'], $jobId);
        $task->updateGenerationStatus($details['status']);

        $resolver->acknowledge($message);
    }
}
