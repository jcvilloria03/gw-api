<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Payroll\PayrollUploadTask;
use App\Payroll\PayrollAttendanceUploadTask;
use App\Payroll\PayrollAllowanceUploadTask;
use App\Payroll\PayrollBonusUploadTask;
use App\Payroll\PayrollCommissionUploadTask;
use App\Payroll\PayrollDeductionUploadTask;

class PayrollUploadResponseConsumer extends Consumer {

    const ATTENDANCE = 'attendance';
    const ALLOWANCE = 'allowance';
    const BONUS = 'bonus';
    const COMMISSION = 'commission';
    const DEDUCTION = 'deduction';

    public function __construct()
    {
        parent::__construct(config('queues.payroll_upload_response_queue'));
    }

    /**
     *  Generate Attendance Upload Task
     *
     */
    public function generateAttendanceUploadTask()
    {
        return App::make(PayrollAttendanceUploadTask::class);
    }

    /**
     *  Generate Allowance Upload Task
     *
     */
    public function generateAllowanceUploadTask()
    {
        return App::make(PayrollAllowanceUploadTask::class);
    }

    /**
     *  Generate Bonus Upload Task
     *
     */
    public function generateBonusUploadTask()
    {
        return App::make(PayrollBonusUploadTask::class);
    }

    /**
     *  Generate Commission Upload Task
     *
     */
    public function generateCommissionUploadTask()
    {
        return App::make(PayrollCommissionUploadTask::class);
    }

    /**
     *  Generate Deduction Upload Task
     *
     */
    public function generateDeductionUploadTask()
    {
        return App::make(PayrollDeductionUploadTask::class);
    }


    /**
     *  Generate Upload Task depending on payroll type
     *
     * @param string $payrollType
     *
     */
    protected function generateUploadTask(string $payrollType)
    {
        switch ($payrollType) {
            case self::ATTENDANCE:
                return $this->generateAttendanceUploadTask();
                break;
            case self::ALLOWANCE:
                return $this->generateAllowanceUploadTask();
                break;
            case self::BONUS:
                return $this->generateBonusUploadTask();
                break;
            case self::COMMISSION:
                return $this->generateCommissionUploadTask();
                break;
            case self::DEDUCTION:
                return $this->generateDeductionUploadTask();
                break;
            default:
                break;
        }
        return null;
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['payroll_id']) ||
            empty($details['task']) ||
            empty($details['payroll_type']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask = $this->generateUploadTask($details['payroll_type']);
        if (!$uploadTask) {
            //disregard message
            Log::error('Invalid type passed to PayrollUploadResponseConsumer : ' . $message->body);
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask->create($details['payroll_id'], $details['id']);
        if ($details['task'] === PayrollUploadTask::PROCESS_SAVE) {
            $uploadTask->updateSaveStatus($details['status']);
        } else {
            $uploadTask->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $uploadTask->updateErrorFileLocation($details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
