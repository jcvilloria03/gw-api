<?php

namespace App\Consumers;

use App\Earning\EarningAuditService;
use App\Earning\EarningUploadTask;
use App\User\UserRequestService;
use Illuminate\Support\Facades\App;

class EarningUploadResponseConsumer extends Consumer
{
    /**
     * App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(
        EarningAuditService $auditService,
        UserRequestService $userRequestService
    ) {
        parent::__construct(config('queues.earning_upload_response_queue'));
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Generate upload earning task.
     */
    public function generateTask()
    {
        return App::make(EarningUploadTask::class);
    }

    /**
     * Validate message and pass it to the upload earning task.
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['company_id'], $details['id']);
        if ($details['task'] === EarningUploadTask::PROCESS_SAVE) {
            $task->updateSaveStatus($details['status']);

            if (!empty($details['earnings_ids'])) {
                $response = $this->userRequestService->get($task->getUserId());
                $userData = json_decode($response->getData(), true);

                $this->auditService->logBatchCreate($details['earnings_ids'], $userData);
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
