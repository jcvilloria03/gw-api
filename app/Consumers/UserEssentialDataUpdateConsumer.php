<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Model\UserEssentialData;

class UserEssentialDataUpdateConsumer extends Consumer
{
    /**
     * UserEssentialDataUpdateConsumer constructor
     */
    public function __construct()
    {
        parent::__construct(config('queues.user_essential_data_update_queue'));
    }

    /**
     * Callback on received message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        try {
            DB::transaction(function () use ($details) {
                $subject = $details['subject'];

                UserEssentialData::where([
                    ['user_id', '=', $subject['user_id']],
                    ['account_id', '=', $subject['account_id']]
                ])->delete();

                foreach ($details['userData'] as $data) {
                    UserEssentialData::create($data);
                }
            });
        } catch (\Exception $e) {
            Log::error('[UserEssentialDataUpdateConsumer] Error: ' . $e->getMessage());
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }
}
