<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\Payroll\PayrollCalculationTask;

class PayrollCalculationResponseConsumer extends Consumer {

    public function __construct()
    {
        parent::__construct(config('queues.payroll_calculation_response_queue'));
    }

    /**
     *  Generate Task
     *
     */
    public function generateTask()
    {
        return App::make(PayrollCalculationTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['payroll_id']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['payroll_id'], $details['id']);
        $task->updateCalculationStatus($details['status']);

        $resolver->acknowledge($message);
    }
}
