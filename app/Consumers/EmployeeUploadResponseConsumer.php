<?php

namespace App\Consumers;

use Illuminate\Support\Arr;
use App\Auth0\Auth0UserService;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeUploadTask;
use App\Employee\EmployeeAuditService;
use App\Employee\EmployeeESIndexQueueService;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\Redis;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */

class EmployeeUploadResponseConsumer extends Consumer
{
    /**
     * @var \App\Employee\EmployeeAuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeESIndexQueueService
     */
    protected $indexQueueService;

    /**
     * Construct employee upload response consumer
     *
     * @param EmployeeAuditService $auditService
     * @param EmployeeESIndexQueueService $indexQueueService
     */
    public function __construct(
        EmployeeAuditService $auditService,
        EmployeeESIndexQueueService $indexQueueService
    ) {
        parent::__construct(config('queues.employee_uploads_queue'));
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     *  Generate Upload Task
     *
     */
    public function generateUploadTask()
    {
        return App::make(EmployeeUploadTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask = $this->generateUploadTask();
        $uploadTask->create($details['company_id'], $details['id']);


        if ($details['task'] === EmployeeUploadTask::PROCESS_SAVE) {
            $uploadTask->updateSaveStatus($details['status']);

            if (!empty($details['employees'])) {
                $uploadTask->createUserRecords($details['employees']);
                $employeeIds = Arr::pluck($details['employees'], 'id');
                $this->indexQueueService->queue($employeeIds);
                $this->auditService->logCreatedEmployees($employeeIds, $uploadTask->getUserId());
            }
        } else {
            $uploadTask->updateValidationStatus($details['task'], $details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $uploadTask->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
