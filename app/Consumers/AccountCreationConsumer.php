<?php

namespace App\Consumers;

use App\Subscriptions\SubscriptionsRequestService;
use App\CompanyUser\CompanyUserService;
use App\Holiday\HolidayRequestService;
use Illuminate\Support\Facades\Log;
use App\Role\DefaultRoleService;
use Bschmitt\Amqp\Facades\Amqp;
use App\Role\UserRoleService;
use App\Authn\AuthnService;
use Illuminate\Support\Arr;
use Bschmitt\Amqp\Message;

/**
 * Continuation of account related data creation from cp-api
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class AccountCreationConsumer extends Consumer
{
    const USER_STATUS_ACTIVE = 'active';

    public function __construct()
    {
        parent::__construct(config('queues.account_creation'));
    }

    /**
     * Function to run for each consumed message
     *
     * @param mixed $message
     * @param mixed $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $data = json_decode(base64_decode($message->body), true);
        $accountId = Arr::get($data, 'account_id');
        $userId = Arr::get($data, 'user_id');
        $companyId = Arr::get($data, 'company_id');
        $employeeId = Arr::get($data, 'employee_id');
        $authnUserId = Arr::get($data, 'authn_id');
        $moduleAccess = Arr::get($data, 'module_access');
        $planId = Arr::get($data, 'plan_id');
        $accountName = Arr::get($data, 'account_name');
        $firstName = Arr::get($data, 'first_name');
        $lastName = Arr::get($data, 'last_name');
        $email = Arr::get($data, 'email');
        $company = ['id' => $companyId];

        if (
            empty($accountId)
            || empty($userId) || empty($companyId)
            || empty($authnUserId) || empty($planId)
            || empty($accountName) || empty($firstName)
            || empty($lastName) || empty($email)
        ) {
            $this->logError('Missing required parameter.', $message->body);

            $resolver->acknowledge($message);
            return;
        }

        $authnService = app()->make(AuthnService::class);
        $userRoleService = app()->make(UserRoleService::class);
        $defaultRoleService = app()->make(DefaultRoleService::class);
        $defaultRoleService = app()->make(DefaultRoleService::class);
        $companyUserService = app()->make(CompanyUserService::class);
        $holidayRequestService = app()->make(HolidayRequestService::class);
        $subscriptionsService = app()->make(SubscriptionsRequestService::class);

        try {
            $ownerRole = $defaultRoleService->createOwnerRole($accountId, $moduleAccess);

            $defaultRoleService->createSuperAdminRole($accountId, $moduleAccess);

            $userRoleService->create(['role_id' => $ownerRole->id, 'user_id' => $userId]);

            $defaultRoleService->createAdminRole($accountId, $company, $moduleAccess);

            $defaultRoleService->createCompanyEmployeeRole($accountId, $company, $moduleAccess);

            $holidayRequestService->createCompanyDefaultHolidays($companyId);

            $authnService->createAuthnUser([
                'user_id' => $userId,
                'account_id' => $accountId,
                'company_id' => $companyId,
                'employee_id' => $employeeId,
                'authn_user_id' => $authnUserId,
                'status' => self::USER_STATUS_ACTIVE
            ]);

            $companyUserService->create([
                'user_id'     => $userId,
                'company_id'  => $companyId,
                'employee_id' => $employeeId
            ]);

            $subscriptionsService->createCustomer([
                "plan_id"       => $planId,
                "user_id"       => $userId,
                "first_name"    => $firstName,
                "last_name"     => $lastName,
                "account_name"  => $accountName,
                "is_trial"      => true,
                "account_id"    => $accountId,
                "email"         => $email
            ]);

            $this->triggerDemoCompanyCreation($accountId, $userId);
        } catch (\Exception $e) {
            $this->logError($e->getMessage(), $message->body);
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }

    /**
     * Drops a message to queue to trigger demo company creation
     *
     * @param int $accountId
     * @param int $userId
     * @return void
     */
    private function triggerDemoCompanyCreation(int $accountId, int $userId)
    {
        $details = [
            'name' => 'democompany.created',
            'data' => [
                'account_id' => $accountId,
                'user_id'    => $userId,
                'name'       => 'Demo Company',
                'type'       => 'Private',
                'tin'        => '000-000-000-000',
                'rdo'        => '048',
                'sss'        => '00-0000000-0',
                'hdmf'       => '0000-0000-0000',
                'philhealth' => '00-000000000-0'
            ]
        ];

        $message = new Message(
            json_encode($details),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish('', $message, [
            'exchange_type' => 'fanout',
            'exchange' => env('EVENT_STREAM_EXCHANGE')
        ]);
    }

    /**
     * Log error with given error message and request data
     *
     * @param string $message
     * @param mixed $data
     *
     * @return void
     */
    private function logError(string $message, $data)
    {
        Log::error(
            'AccountCreationConsumer Error: '. $message,
            [
                'request_data' => $data
            ]
        );
    }
}
