<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\LeaveRequest\LeaveRequestAuditService;
use App\LeaveRequest\LeaveRequestUploadTask;
use App\User\UserRequestService;

class LeaveRequestUploadResponseConsumer extends Consumer
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * LeaveRequestUploadResponseConsumer constructor
     *
     * @param LeaveRequestAuditService $auditService
     * @param UserRequestService $userRequestService
     */
    public function __construct(
        LeaveRequestAuditService $auditService,
        UserRequestService $userRequestService
    ) {
        parent::__construct(config('queues.leave_request_upload_response_queue'));
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Generate upload leave request task.
     */
    public function generateTask()
    {
        return App::make(LeaveRequestUploadTask::class);
    }

    /**
     * Callback on received message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['company_id'], $details['id']);

        if ($details['task'] === LeaveRequestUploadTask::PROCESS_SAVE) {
            $task->updateSaveStatus($details['status']);

            if (!empty($details['leave_requests_ids'])) {
                $response = $this->userRequestService->get($task->getUserId());
                $userData = json_decode($response->getData(), true);

                $this->auditService->logBatchCreate(
                    $details['leave_requests_ids'],
                    $userData
                );
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
