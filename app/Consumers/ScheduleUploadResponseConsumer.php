<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\Schedule\ScheduleUploadTask;
use App\Schedule\ScheduleAuditService;

class ScheduleUploadResponseConsumer extends Consumer
{
    /**
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(ScheduleAuditService $auditService)
    {
        parent::__construct(config('queues.schedule_upload_response_queue'));
        $this->auditService = $auditService;
    }

    /**
     * Generate upload schedules task.
     */
    public function generateTask()
    {
        return App::make(ScheduleUploadTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['company_id'], $details['id']);
        if ($details['task'] === ScheduleUploadTask::PROCESS_SAVE) {
            $task->updateSaveStatus($details['status']);
            if (!empty($details['schedule_ids'])) {
                $this->auditService->logCreatedSchedules($details['schedule_ids'], $task->getUserId());
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
