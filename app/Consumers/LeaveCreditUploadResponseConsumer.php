<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\LeaveCredit\LeaveCreditAuditService;
use App\LeaveCredit\LeaveCreditUploadTask;
use App\User\UserRequestService;

class LeaveCreditUploadResponseConsumer extends Consumer
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * LeaveCreditUploadResponseConsumer constructor
     *
     * @param LeaveCreditAuditService $auditService
     * @param UserRequestService $userRequestService
     */
    public function __construct(
        LeaveCreditAuditService $auditService,
        UserRequestService $userRequestService
    ) {
        parent::__construct(config('queues.leave_credit_upload_response_queue'));
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Generate upload leave credits task.
     */
    public function generateTask()
    {
        return App::make(LeaveCreditUploadTask::class);
    }

    /**
     * Callback on reseived message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['company_id'], $details['id']);

        if ($details['task'] === LeaveCreditUploadTask::PROCESS_SAVE) {
            $task->updateSaveStatus($details['status']);

            if (!empty($details['leave_credits_ids'])) {
                $response = $this->userRequestService->get($task->getUserId());
                $userData = json_decode($response->getData(), true);

                $this->auditService->logUploadedLeaveCredits(
                    $details['leave_credits_ids'],
                    $userData
                );
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }
}
