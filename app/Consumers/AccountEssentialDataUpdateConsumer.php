<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\Model\AccountEssentialData;

class AccountEssentialDataUpdateConsumer extends Consumer
{
    /**
     * AccountEssentialDataUpdateConsumer constructor
     */
    public function __construct()
    {
        parent::__construct(config('queues.account_essential_data_update_queue'));
    }

    /**
     * Callback on received message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        try {
            AccountEssentialData::where('account_id', $details['account_id'])->delete();

            AccountEssentialData::create([
                'account_id' => $details['account_id'],
                'data' => $details['data']
            ]);
        } catch (\Exception $e) {
            Log::error('[AccountEssentialDataUpdateConsumer] Error:' . $e->getMessage());
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }
}
