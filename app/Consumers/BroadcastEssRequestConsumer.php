<?php

namespace App\Consumers;

use App\Events\EssRequestUpdated;

class BroadcastEssRequestConsumer extends Consumer
{
    public function __construct()
    {
        parent::__construct(config('queues.broadcast_ess_request_queue'));
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['request'])
        ) {
            // Disregard incomplete message.
            $resolver->acknowledge($message);
            return;
        }

        event(new EssRequestUpdated($details['request']));

        $resolver->acknowledge($message);
    }
}
