<?php

namespace App\Consumers;

use App\Auth0\Auth0UserService;
use App\Employee\EmployeeAuditService;
use App\Employee\EmployeeESIndexQueueService;
use App\Employee\EmployeeRequestService;
use App\Employee\EmployeeUpdateUploadTask;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\App;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class EmployeeUpdateUploadResponseConsumer extends Consumer
{

    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        EmployeeAuditService $auditService,
        EmployeeESIndexQueueService $indexQueueService
    ) {
        parent::__construct(config('queues.employee_update_upload_response_queue'));
        $this->auditService = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     *  Generate Upload Task
     *
     */
    public function generateUploadTask()
    {
        return App::make(EmployeeUpdateUploadTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask = $this->generateUploadTask();
        $uploadTask->create($details['company_id'], $details['id']);

        if ($details['task'] === EmployeeUpdateUploadTask::PROCESS_SAVE) {
            $uploadTask->updateSaveStatus($details['status']);
        } else if ($details['task'] === EmployeeUpdateUploadTask::PROCESS_ACCOUNT_PROCESSING) {
            $this->queueAccountProcessing($uploadTask, $details);
        } else {
            $uploadTask->updateValidationStatus($details['task'], $details['status']);
            if ($details['status'] === EmployeeUpdateUploadTask::STATUS_VALIDATED) {
                // trigger employee write after successful validation
                $uploadTask->updateSaveStatus(EmployeeUpdateUploadTask::STATUS_SAVE_QUEUED);
                $this->queueWrite($uploadTask, $details['company_id']);
            }
        }

        if (isset($details['error_file_s3_key'])) {
            $uploadTask->updateErrorFileLocation($uploadTask->getProcess(), $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }

    /**
     * Queue write of employee update data
     *
     * @param EmployeeUpdateUploadTask $uploadTask
     * @param int $companyId
     *
     */
    protected function queueWrite(EmployeeUpdateUploadTask $uploadTask, int $companyId)
    {
        $details = [
            'id' => $uploadTask->getId(),
            'company_id' => $companyId,
            'task' => $uploadTask->getProcess(),
            's3_bucket' => $uploadTask->getS3Bucket(),
        ];
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );
        Amqp::publish(config('queues.people_write_queue'), $message);
    }

    /**
     * Queue write of employee update data
     *
     * @param EmployeeUpdateUploadTask $uploadTask
     * @param int $companyId
     *
     */
    protected function queueAccountProcessing(EmployeeUpdateUploadTask $uploadTask, array $details)
    {
        $details = array_merge(
            $details,
            [
                'id'        => $uploadTask->getId(),
                'task'      => $uploadTask->getProcess(),
                's3_bucket' => $uploadTask->getS3Bucket(),
            ]
        );

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.employees_update_status_queue'), $message);
    }
}
