<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\Authn\AuthnService;
use Illuminate\Support\Arr;

/**
 * Revokes user's existing tokens
 */
class RevokeAuthnTokensConsumer extends Consumer
{
    public function __construct()
    {
        parent::__construct(config('queues.revoke_authn_tokens'));
    }

    /**
     * Function to run for each consumed message
     *
     * @param mixed $message
     * @param mixed $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        try {
            $data = json_decode(base64_decode($message->body), true);
            $userId = Arr::get($data, 'user_id');

            if (empty($userId)) {
                $this->logError('Missing required parameter.', $message->body);

                $resolver->acknowledge($message);
                return;
            }

            $authnService = app()->make(AuthnService::class);
            $authnService->revokeUserTokens($userId);
        } catch (\Exception $e) {
            $this->logError($e->getMessage(), $message->body);
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }

    /**
     * Log error with given error message and request data
     *
     * @param string $message
     * @param mixed $data
     *
     * @return void
     */
    private function logError(string $message, $data)
    {
        Log::error(
            'RevokeAuthnTokensConsumer Error: '. $message,
            [
                'request_data' => $data
            ]
        );
    }
}
