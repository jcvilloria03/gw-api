<?php

namespace App\Consumers;

use App\Events\AnnouncementPublished;

class BroadcastAnnouncementConsumer extends Consumer
{
    public function __construct()
    {
        parent::__construct(config('queues.broadcast_announcement_queue'));
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['user_id']) ||
            empty($details['announcement'])
        ) {
            // Disregard incomplete message.
            $resolver->acknowledge($message);
            return;
        }

        event(new AnnouncementPublished($details['announcement'], $details['user_id']));

        $resolver->acknowledge($message);
    }
}
