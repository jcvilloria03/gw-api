<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\Authn\AuthnService;
use Illuminate\Support\Arr;
use App\CompanyUser\CompanyUserService;
use App\Model\AuthnUser;
use App\Role\DefaultRoleService;
use App\Role\UserRoleService;

/**
 * Creates created user from Authn
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class AuthnUserCreateUpdateConsumer extends Consumer
{
    const USER_STATUS_ACTIVE = 'active';

    public function __construct()
    {
        parent::__construct(config('queues.authn_user_create_update'));
    }

    /**
     * Function to run for each consumed message
     *
     * @param mixed $message
     * @param mixed $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        try {
            $data = json_decode(base64_decode($message->body), true);
            $accountId = Arr::get($data, 'account_id');
            $userId = Arr::get($data, 'user_id');
            $companyId = Arr::get($data, 'company_id');
            $employeeId = Arr::get($data, 'employee_id');
            $authnUserId = Arr::get($data, 'authn_user_id');
            $isUpdate = Arr::get($data, 'is_update', false);

            if (
                empty($accountId) ||
                empty($userId) ||
                empty($companyId) ||
                empty($authnUserId)
            ) {
                $this->logError('Missing required parameter.', $message->body);

                $resolver->acknowledge($message);
                return;
            }

            if ($isUpdate) {
                $companyUserService = app()->make(CompanyUserService::class);
                $companyUser = $companyUserService->getByUserAndCompanyId($userId, $companyId);
                if (empty($companyUser)) {
                    $companyUser = $companyUserService->create([
                        'user_id'     => $userId,
                        'company_id'  => $companyId,
                        'employee_id' => $employeeId
                    ]);
                } else {
                    $companyUser = $companyUserService->update($userId, $companyId, [
                        'employee_id' => $employeeId
                    ]);
                }
            } else {
                $authnService = app()->make(AuthnService::class);
                $authnUser = $authnService->getAuthnUserByAuthnId($authnUserId);

                if ($authnUser) {
                    $this->logError('Authn User record already exists.', $message->body);

                    $resolver->acknowledge($message);
                    return;
                }

                $authnUser = $authnService->getUser($userId);
                if (empty($authnUser)) {
                    $authnService->createAuthnUser([
                        'user_id' => $userId,
                        'account_id' => $accountId,
                        'company_id' => $companyId,
                        'employee_id' => $employeeId,
                        'authn_user_id' => $authnUserId,
                        'status' => AuthnUser::STATUS_UNVERIFIED
                    ]);
                } else {
                    $authnService->updateAuthnUser($userId, [
                        'employee_id' => $employeeId,
                        'company_id' => $companyId,
                        'status' => AuthnUser::STATUS_UNVERIFIED
                    ]);
                }

                $companyUserService = app()->make(CompanyUserService::class);

                $companyUser = $companyUserService->getByUserAndCompanyId($userId, $companyId);
                if (empty($companyUser)) {
                    $companyUser = $companyUserService->create([
                        'user_id'     => $userId,
                        'company_id'  => $companyId,
                        'employee_id' => $employeeId
                    ]);
                } else {
                    $companyUser = $companyUserService->update($userId, $companyId, [
                        'employee_id' => $employeeId
                    ]);
                }

                $defaultRoleService = app()->make(DefaultRoleService::class);
                $employeeDefaultRole = $defaultRoleService->getEmployeeSystemRole(
                    $accountId,
                    $companyId
                );

                $assignedCompanies = [[
                    'company_id'  => $companyId,
                    'employee_id' => $employeeId,
                    'roles_ids'   => [$employeeDefaultRole->id]
                ]];

                $this->assignDefaultEmployeeRolesToUser(
                    $authnUser,
                    $accountId,
                    $assignedCompanies
                );
            }
        } catch (\Exception $e) {
            $this->logError($e->getMessage(), $message->body);
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }

    /**
     * Log error with given error message and request data
     *
     * @param string $message
     * @param mixed $data
     *
     * @return void
     */
    private function logError(string $message, $data)
    {
        Log::error(
            'AuthnUserCreateUpdateConsumer Error: '. $message,
            [
                'request_data' => $data
            ]
        );
    }

    /**
     * Assigns default employee roles to given auth0User
     *
     * @param  \App\Model\AuthnUser $authnUser
     * @param  int                  $accountId
     * @param  array                $assignedCompanies
     * @return void
     */
    private function assignDefaultEmployeeRolesToUser($authnUser, $accountId, array $assignedCompanies)
    {
        // Assign defualt roles per assigned company
        $defaultRoleService = app()->make(DefaultRoleService::class);
        foreach ($assignedCompanies as $assignedCompany) {
            // get default system role for employee
            $defaultRole = $defaultRoleService->getEmployeeSystemRole(
                $accountId,
                $assignedCompany['company_id']
            );

            $this->assignRoleToUser($authnUser, $defaultRole);
        }
    }

    /**
     * Assigns given role to user if not yet assigned
     *
     * @param  \App\Model\AuthnUser $authnUser
     * @param  \App\Model\Role      $role
     * @return void
     */
    private function assignRoleToUser($authnUser, $role)
    {
        // Check if employee default role is not yet set for user
        $userRoleService = app()->make(UserRoleService::class);
        $userRoles = $userRoleService->getWithConditions([
            ['user_id', $authnUser->user_id],
            ['role_id', $role->id]
        ]);

        if ($userRoles->isEmpty()) {
            $userRoleService->create([
                'role_id' => $role->id,
                'user_id' => $authnUser->user_id,
            ]);
        }
    }
}
