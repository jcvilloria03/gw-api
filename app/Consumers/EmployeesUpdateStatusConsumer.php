<?php

namespace App\Consumers;

use App\Auth0\Auth0UserService;
use App\Employee\EmployeeAuditService;
use App\Employee\EmployeeESIndexQueueService;
use App\Employee\EmployeeUpdateUploadTask;
use App\Exceptions\Auth0BatchCreateJobException;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class EmployeesUpdateStatusConsumer extends Consumer
{
    /**
     * @var \App\Employee\EmployeeAuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeESIndexQueueService
     */
    protected $indexQueueService;

    /**
     * Construct employee upload response consumer
     *
     * @param EmployeeAuditService $auditService
     * @param EmployeeESIndexQueueService $indexQueueService
     */
    public function __construct(
        EmployeeAuditService $auditService,
        EmployeeESIndexQueueService $indexQueueService
    ) {
        parent::__construct(config('queues.employees_update_status_queue'));
        $this->publishQueue      = config('queues.employee_update_upload_response_queue');
        $this->auditService      = $auditService;
        $this->indexQueueService = $indexQueueService;
    }

    /**
     *  Generate Upload Task
     *
     */
    public function generateUploadTask()
    {
        return App::make(EmployeeUpdateUploadTask::class);
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['account_id']) ||
            empty($details['task']) ||
            empty($details['status']) ||
            (empty($details['account_processing']['for_activation'])
                && empty($details['account_processing']['for_deactivation'])
                && empty($details['account_processing']['for_semi_activation'])
            )
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask = $this->generateUploadTask();
        $uploadTask->create($details['company_id'], $details['id']);

        // Immediately set status to saved and let the consumer process employee activation in the background.
        $this->publishMessage(
            $details['id'],
            $details['company_id'],
            EmployeeUpdateUploadTask::STATUS_SAVED
        );

        try {
            $uploadTask->activateAuthnEmployees(
                $details['account_processing']['for_activation']
            );
        } catch (Auth0BatchCreateJobException $e) {
            // Requeue message if auth0 activation tasks has failed
            Log::warning('Requeued employees update status job: ' . $details['id'], [
                'company_id' => $details['company_id'],
                'account_id' => $details['account_id'],
                'task' => $details['task'],
                'status' => $details['status']
            ]);

            $resolver->reject($message, true);
            return;
        }

        $resolver->acknowledge($message);
    }

    /**
     * Publish status message update to queue
     *
     * @param string $status What the new status of the write task should be
     * @param array $ids Employees saved if any
     * @param string $errorFileS3Key Error File of S3Key
     */
    protected function publishMessage(
        $taskId,
        $companyId,
        string $status,
        array $ids = null,
        string $errorFileS3Key = null
    ) {
        $details = [
            'id'                => $taskId,
            'company_id'        => $companyId,
            'task'              => EmployeeUpdateUploadTask::PROCESS_SAVE,
            'status'            => $status,
            'employee_ids'      => $ids,
            'error_file_s3_key' => $errorFileS3Key,
        ];

        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish($this->publishQueue, $message);
    }

    /**
     * Publish send verification emails async task
     *
     * @param array $auth0UserIds
     */
    protected function sendVerificationEmails(array $auth0UserIds)
    {
        foreach ($auth0UserIds as $user) {
            $details = [
                'auth0_user_ids' => [$user]
            ];
    
            $message = new \Bschmitt\Amqp\Message(
                base64_encode(json_encode($details)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            );
    
            Amqp::publish(config('queues.employees_update_send_verification_email_queue'), $message);
        }
    }
}
