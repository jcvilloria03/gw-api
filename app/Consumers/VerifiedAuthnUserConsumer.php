<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\Authn\AuthnService;
use Illuminate\Support\Arr;
use App\Model\AuthnUser;

/**
 * Creates created user from Authn
 */
class VerifiedAuthnUserConsumer extends Consumer
{
    const USER_STATUS_ACTIVE = 'active';

    public function __construct()
    {
        parent::__construct(config('queues.authn_user_verified'));
    }

    /**
     * Function to run for each consumed message
     *
     * @param mixed $message
     * @param mixed $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        try {
            $data = json_decode(base64_decode($message->body), true);
            $accountId = Arr::get($data, 'account_id');
            $userId = Arr::get($data, 'user_id');
            $companyId = Arr::get($data, 'company_id');
            $employeeId = Arr::get($data, 'employee_id');
            $authnUserId = Arr::get($data, 'authn_user_id');

            if (
                empty($accountId) ||
                empty($userId) ||
                empty($companyId) ||
                empty($authnUserId)
            ) {
                $this->logError('Missing required parameter.', $message->body);

                $resolver->acknowledge($message);
                return;
            }

            $authnService = app()->make(AuthnService::class);
            $employeeId = Arr::get($authnService, 'employee_id', $employeeId);

            $authnUser = $authnService->getUser($userId);
            if (empty($authnUser)) {
                $authnService->createAuthnUser([
                    'user_id' => $userId,
                    'account_id' => $accountId,
                    'company_id' => $companyId,
                    'employee_id' => $employeeId,
                    'authn_user_id' => $authnUserId,
                    'status' => AuthnUser::STATUS_ACTIVE
                ]);
            } else {
                $authnService->updateAuthnUser($userId, [
                    'employee_id' => $employeeId,
                    'company_id' => $companyId,
                    'status' => AuthnUser::STATUS_ACTIVE
                ]);
            }
        } catch (\Exception $e) {
            $this->logError($e->getMessage(), $message->body);
            Log::error($e->getTraceAsString());
        }

        $resolver->acknowledge($message);
    }

    /**
     * Log error with given error message and request data
     *
     * @param string $message
     * @param mixed $data
     *
     * @return void
     */
    private function logError(string $message, $data)
    {
        Log::error(
            'VerifiedAuthnUserConsumer Error: '. $message,
            [
                'request_data' => $data
            ]
        );
    }
}
