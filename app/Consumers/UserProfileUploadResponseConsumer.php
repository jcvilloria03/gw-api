<?php

namespace App\Consumers;

use Illuminate\Support\Facades\App;
use App\User\UserAuditService;
use App\User\UserProfileUploadTask;
use App\User\UserRequestService;
use App\Role\UserRoleService;
use App\CompanyUser\CompanyUserService;
use App\Auth0\Auth0ManagementService;
use App\Auth0\Auth0UserService;
use App\Audit\AuditService;
use App\Account\AccountRequestService;
use App\Model\Auth0User;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UserProfileUploadResponseConsumer extends Consumer
{
    /**
     * @var \App\Audit\AuditService
     */
    protected $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * @var \App\Role\UserRoleService
     */
    protected $userRoleService;

    /**
     * @var \App\CompanyUser\CompanyUserService
     */
    protected $companyUserService;

    /**
     * @var \App\Auth0\Auth0ManagementService
     */
    private $auth0ManagementService;

    /**
     * @var \App\Auth0\Auth0UserService
     */
    private $auth0UserService;

    /**
     * @var \App\Account\AccountRequestService
     */
    private $accountRequestService;

    /**
     * UserProfileUploadResponseConsumer constructor
     *
     * @param UserAuditService $auditService
     * @param UserRequestService $userRequestService
     * @param UserRoleService $userRoleService
     * @param CompanyUserService $companyUserService
     * @param Auth0ManagementService $companyUserService
     * @param Auth0UserService $companyUserService
     * @param AccountRequestService $companyUserService
     */
    public function __construct(
        UserAuditService $auditService,
        UserRequestService $userRequestService,
        UserRoleService $userRoleService,
        CompanyUserService $companyUserService,
        Auth0ManagementService $auth0ManagementService,
        Auth0UserService $auth0UserService,
        AccountRequestService $accountRequestService
    ) {
        parent::__construct(config('queues.user_profile_upload_response_queue'));
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
        $this->userRoleService = $userRoleService;
        $this->companyUserService = $companyUserService;
        $this->auth0ManagementService = $auth0ManagementService;
        $this->auth0UserService = $auth0UserService;
        $this->accountRequestService = $accountRequestService;
    }

    /**
     * Generate upload leave request task.
     */
    public function generateTask()
    {
        return App::make(UserProfileUploadTask::class);
    }

    /**
     * Callback on received message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['id']) ||
            empty($details['account_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $task = $this->generateTask();
        $task->create($details['account_id'], $details['company_id'] ?? null, $details['id']);

        if ($details['task'] === UserProfileUploadTask::PROCESS_SAVE) {
            if (!empty($details['user_profiles_ids'])) {
                $this->executeActionsAfterStoringUsers($details);
            }

            $task->updateSaveStatus($details['status']);

            if (!empty($details['user_profiles_ids'])) {
                // audit log
                $uploaderUserResponse = $this->userRequestService->get($details['uploader_user_id']);
                $uploaderUser = json_decode($uploaderUserResponse->getData(), true);

                $this->auditService->logBatchCreate(
                    $details['user_profiles_ids'],
                    $uploaderUser
                );
            }
        } else {
            $task->updateValidationStatus($details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $task->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }

    /**
     * Perform actions after storing users
     *
     * @param array $data
     * @return void
     */
    public function executeActionsAfterStoringUsers(array $data)
    {
        $accountData = $this->accountRequestService->get($data['account_id']);
        $account = json_decode($accountData->getData(), true);
        $users = $this->getStoredUsersData($data);
        $currentTime = \Carbon\Carbon::now()->toDateTimeString();
        $usersRoles = [];

        foreach ($users as $user) {
            // create user in Auth0
            try {
                $userData = [
                    'email' => $user['email'],
                    'first_name' => array_get($user, 'first_name', ''),
                    'middle_name' => array_get($user, 'middle_name', ''),
                    'last_name' => array_get($user, 'last_name', ''),
                    'account_name' => array_get($account, 'name', ''),
                ];

                $auth0UserDetails = $this->auth0ManagementService->createUser($userData, false);
            } catch (\Exception $e) {
                \Log::info('Could not create user in auth0 : ' . $e->getMessage());
                $auth0UserDetails = [];
            }
            // create auth0_user entry
            $auth0UserData = [
                'auth0_user_id' => array_get($auth0UserDetails, 'user_id', ''),
                'user_id' => $user['id'],
                'account_id' => $user['account_id']
            ];
            $this->auth0UserService->create($auth0UserData);

            $userRoles = collect($user['assigned_companies'])->map(function ($company) use ($user, $currentTime) {
                return collect($company['roles_ids'])->map(function ($roleId) use ($user, $currentTime) {
                    return [
                        'role_id' => $roleId,
                        'user_id' => $user['id'],
                        'created_at' => $currentTime,
                        'updated_at' => $currentTime
                    ];
                })->toArray();
            })->flatten(1)->toArray();

            $usersRoles = array_merge($usersRoles, $userRoles);

            $this->userRoleService->assignOwnerOrSuperAdminRoleToUser($user, $user['id']);
            $this->companyUserService->sync($user['id']);

            // Activate user
            $activeUserIds = $this->auth0UserService->getActiveUserIds($data['account_id']);
            // call microservice to validate new status
            $isValidStatusResponse = $this->userRequestService->validateStatus($user['id'], [
                'status' => strtolower($user['status']),
                'active_user_ids' => $activeUserIds
            ]);

            if ($isValidStatusResponse->isSuccessful()) {
                $this->auth0UserService->setStatus($user['id'], strtolower($user['status']));
                // send verification auth0 user email, if user is activated, and email not verified.
                if (strtolower($user['status']) === Auth0User::STATUS_ACTIVE) {
                    try {
                        $userAuth0Profile = $this->auth0ManagementService
                            ->getAuth0UserProfile($auth0UserData['auth0_user_id']);
                        if (!$userAuth0Profile['email_verified']) {
                            $this->auth0ManagementService->verifyEmail($auth0UserData['auth0_user_id']);
                        }
                    } catch (\Exception $e) {
                        \Log::info('Could not send verification email to auth0 user : ' . $e->getMessage());
                    }
                }
            }
        }

        $this->userRoleService->insert($usersRoles);
    }

    /**
     * Get stored users data
     *
     * @param array $data
     * @return array
     */
    private function getStoredUsersData(array $data)
    {
        $formData['values'] = collect($data['user_profiles_ids'])->implode(',');
        $storedUsersResponse = $this->userRequestService
            ->getAccountUsersByAttribute($data['account_id'], 'id', $formData);
        $storedUsers = json_decode($storedUsersResponse->getData(), true);

        $uploadedUsersResponse = $this->userRequestService->getUploadPreview($data['id']);
        $uploadedUsers = json_decode($uploadedUsersResponse->getData(), true);

        return collect($uploadedUsers)->map(function ($uploadedUser) use ($storedUsers) {
            $user = collect($storedUsers)->first(function ($storedUser) use ($uploadedUser) {
                return $storedUser['email'] == $uploadedUser['email'] &&
                    $storedUser['account_id'] == $uploadedUser['account_id'];
            });

            if (!$user) {
                throw new Exception('User does not exists.');
            }

            $uploadedUser['id'] = $user['id'];
            $uploadedUser['assigned_companies'] = json_decode($uploadedUser['assigned_companies'], true);
            $uploadedUser['assigned_product_seats_ids'] = json_decode(
                $uploadedUser['assigned_product_seats_ids'],
                true
            );
            $uploadedUser['user_type'] = strtolower($uploadedUser['user_type']);

            return $uploadedUser;
        })->toArray();
    }
}
