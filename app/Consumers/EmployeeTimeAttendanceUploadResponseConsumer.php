<?php

namespace App\Consumers;

use Illuminate\Support\Arr;
use App\Auth0\Auth0UserService;
use Illuminate\Support\Facades\App;
use App\Employee\EmployeeUploadTask;
use App\Employee\EmployeeAuditService;
use App\Employee\EmployeeRequestService;

class EmployeeTimeAttendanceUploadResponseConsumer extends Consumer
{
    /**
     * @var \App\Employee\EmployeeAuditService
     */
    protected $auditService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $requestService;

    /**
     * Construct employee T&A upload response consumer
     *
     * @param EmployeeAuditService $auditService
     * @param EmployeeRequestService $requestService
     */
    public function __construct(
        EmployeeAuditService $auditService,
        EmployeeRequestService $requestService
    ) {
        parent::__construct(config('queues.employee_time_attendance_uploads_queue'));
        $this->auditService = $auditService;
        $this->requestService = $requestService;
    }

    /**
     *  Generate Upload Task
     *
     */
    public function generateUploadTask()
    {
        return App::make(EmployeeUploadTask::class);
    }

    /**
     * Consumer callback
     *
     * @param $message
     * @param $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);
        if (
            empty($details['id']) ||
            empty($details['company_id']) ||
            empty($details['task']) ||
            empty($details['status'])
        ) {
            //disregard incomplete message
            $resolver->acknowledge($message);
            return;
        }

        $uploadTask = $this->generateUploadTask();
        $uploadTask->create($details['company_id'], $details['id']);

        if ($details['task'] === EmployeeUploadTask::PROCESS_TA_SAVE) {
            $uploadTask->updateTaSaveStatus($details['status']);

            if (!empty($details['employees'])) {
                $uploadTask->createUserRecords($details['employees']);
                $employeeIds = Arr::pluck($details['employees'], 'id');
                $employees = $this->getUploadedTaEmployees($details['company_id'], $employeeIds);
                $this->auditService->logCreatedTaEmployees($employees, $uploadTask->getUserId());
            }
        } else {
            $uploadTask->updateValidationStatus($details['task'], $details['status']);
        }

        if (isset($details['error_file_s3_key'])) {
            $uploadTask->updateErrorFileLocation($details['task'], $details['error_file_s3_key']);
        }

        $resolver->acknowledge($message);
    }

    /**
     * Get recently uploaded T&A employees
     *
     * @param int|string $companyId
     * @param array $employeeIds Employee's IDs
     * @return array $employees
     */
    private function getUploadedTaEmployees($companyId, array $employeeIds)
    {
        $response = $this->requestService->getCompanyTaEmployeesByAttribute(
            $companyId,
            'id',
            [
                'values' => implode(',', $employeeIds)
            ]
        );
        $employeeData = json_decode($response->getData(), true);
        $employees = $employeeData['data'];

        return $employees;
    }
}
