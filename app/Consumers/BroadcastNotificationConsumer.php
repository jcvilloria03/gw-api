<?php

namespace App\Consumers;

use App\Events\UserNotificationReceived;

class BroadcastNotificationConsumer extends Consumer
{
    public function __construct()
    {
        parent::__construct(config('queues.broadcast_notification_queue'));
    }

    public function callback($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        if (
            empty($details['user_id']) ||
            empty($details['notification'])
        ) {
            // Disregard incomplete message.
            $resolver->acknowledge($message);
            return;
        }

        $notification = $details['notification'];
        $userId = $details['user_id'];

        event(new UserNotificationReceived($notification, $userId));

        $resolver->acknowledge($message);
    }
}
