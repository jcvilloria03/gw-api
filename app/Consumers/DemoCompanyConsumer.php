<?php

namespace App\Consumers;

use Illuminate\Support\Facades\Log;
use App\DemoCompany\DemoCompanyRequestService;
use App\DemoCompany\DemoCompanyService;

class DemoCompanyConsumer extends Consumer
{
    /**
     * @var \App\DemoCompany\DemoCompanyService
     */
    protected $demoCompanyService;

    /**
     * DemoCompanyConsumer constructor
     *
     * @param DemoCompanyService $demoCompanyService
     */
    public function __construct(DemoCompanyService $demoCompanyService)
    {
        $this->demoCompanyService = $demoCompanyService;
        parent::__construct(config('queues.demo_company_gw_queue'));
    }

    /**
     * Callback on reseived message
     *
     * @param object $message
     * @param object $resolver
     * @return void
     */
    public function callback($message, $resolver)
    {
        $env = strtoupper(getenv('APP_ENV'));
        $details = json_decode(base64_decode($message->body), true);

        try {
            $resolver->acknowledge($message);
            Log::info('gw-api acknowledged');
            $this->demoCompanyService->process($details);
        } catch (\Exception $e) {
            $requestService = app()->make(DemoCompanyRequestService::class);
            $msg = $e->getMessage() . '|' . $e->getTraceAsString();
            $requestService->setError(
                $details['company_id'],
                $details['type'],
                $msg
            );

            Log::error('Error with task passed to DemoCompanyConsumer ' . $details['type'] . ' : '
                . $e->getMessage() . '|' . $e->getTraceAsString());

            $msg = ':no_entry: **[' . $env . ']' . '[GW-API]' . '[DemoCompany] ' . ucwords($details['type']) . '**' .
                "\n" . "```\n". $msg . "\n```";
            $requestService->mattermostNotif($msg);

            $resolver->acknowledge($message);
            return;
        }
    }
}
