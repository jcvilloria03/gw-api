<?php

namespace App\AccountDeletion;

use App\Common\CommonAuthorizationService;

class AccountDeletionAuthorizationService extends CommonAuthorizationService
{
    const VIEW_TASK = 'view.account_deletion';
    const CREATE_TASK = 'create.account_deletion';
    const UPDATE_TASK = 'edit.account_deletion';
    const DELETE_TASK = 'delete.account_deletion';
}
