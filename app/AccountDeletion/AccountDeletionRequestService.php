<?php

namespace App\AccountDeletion;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class AccountDeletionRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Get Account Deletion Request
     *
     * @param int $accountId Account ID
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getAccountDeletion($accountId)
    {
        return $this->send(new Request(
            'GET',
            "/subscriptions/account_deletion/{$accountId}"
        ));
    }


    /**
     * Call endpoint to create account deletion request.
     *
     * @param int $userId User ID
     * @return \Illuminate\Http\JsonResponse Account owner deletion request
     */
    public function createAcountDeletionRequest(array $data)
    {
        $request = new Request(
            'POST',
            "/subscriptions/account_deletion",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to confirm the otp code in account deletion request
     *
     * @param int $id Account deletion ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function confirmOtp(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/subscriptions/account_deletion/{$id}/confirm_otp",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update the otp code in account deletion request
     *
     * @param int $id Account deletion ID
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function resendOtp(int $id)
    {
        return $this->send(new Request(
            'POST',
            "/subscriptions/account_deletion/{$id}/resend_otp",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ));
    }

    /**
     * Call endpoint to confirm the account deletion request to proceed.
     *
     * @param int $id Account deletion ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function confirmRequest(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/subscriptions/account_deletion/{$id}/confirm",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Delete account deletion
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function deleteRequest($id)
    {
        $request = new Request(
            'DELETE',
            "/subscriptions/account_deletion/{$id}/cancel"
        );

        return $this->send($request);
    }
}
