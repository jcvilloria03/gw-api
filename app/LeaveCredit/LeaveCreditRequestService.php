<?php

namespace App\LeaveCredit;

use App\Authz\AuthzDataScope;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Request\DownloadRequestService;

class LeaveCreditRequestService extends DownloadRequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get leave credit info
     *
     * @param int $id LeaveCredit ID
     *
     * @return \Illuminate\Http\JsonResponse LeaveCredit Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/leave_credit/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get company leave credits
     *
     * @param int    $id    Company ID
     * @param array  $data  from request
     * @param string $query string
     *
     * @return \Illuminate\Http\JsonResponse LeaveCredits
     */
    public function getCompanyLeaveCredits(int $id, array $data, string $query, AuthzDataScope $queryDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/leave_credits?{$query}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request, $queryDataScope);
    }

    /**
     * Call endpoint to create leave credit
     *
     * @param array $data leave credit information
     *
     * @return \Illuminate\Http\JsonResponse Created LeaveCredit
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            '/leave_credit',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update leave credit for given id
     *
     * @param array $data leave credit informations
     * @param int   $id   LeaveCredit Id
     *
     * @return \Illuminate\Http\JsonResponse Updated leave credit
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/leave_credit/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple leave credits
     *
     * @param array $data leave credits to delete informations
     *
     * @return \Illuminate\Http\JsonResponse Deleted leave credits id's
     */
    public function bulkDelete(array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            '/leave_credit/bulk_delete',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to fetch Data to be saved in Leave Credit upload job
     *
     * @param string $jobId Job ID
     *
     * @return \Illuminate\Http\JsonResponse Leave Credits upload preview
     *
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/leave_credit/upload_preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to download leave credits csv
     *
     * @param int   $companyId Company ID
     * @param array $data      from request
     *
     * @return \Illuminate\Http\JsonResponse LeaveCredits
     */
    public function downloadCompanyLeaveCredits(int $companyId, array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/leave_credits/download",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->download($request, $authzDataScope);
    }

    /**
     * Call endpoint to download employee leave credits csv
     *
     * @param int   $employeeId Company ID
     * @param array $data       from request
     *
     * @return \Illuminate\Http\JsonResponse LeaveCredits
     */
    public function downloadEmployeeLeaveCredits(int $employeeId, array $data, string $queryString)
    {
        $request = new Request(
            'POST',
            "/employee/{$employeeId}/leave_credits/download?{$queryString}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->download($request);
    }
}
