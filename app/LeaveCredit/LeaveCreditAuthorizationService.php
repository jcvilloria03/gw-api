<?php

namespace App\LeaveCredit;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class LeaveCreditAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.leave_credit';
    const CREATE_TASK = 'create.leave_credit';
    const UPDATE_TASK = 'edit.leave_credit';
    const DELETE_TASK = 'delete.leave_credit';

    /**
     * @param \stdClass $leaveCredit
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $leaveCredit, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveCredit, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveCredit
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $leaveCredit, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveCredit, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $leaveCredit
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $leaveCredit, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveCredit, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $leaveCredit
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $leaveCredit, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveCredit, $user, self::DELETE_TASK);
    }

    /**
     * Authorize leave Credit related tasks
     *
     * @param \stdClass $leaveCredit
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $leaveCredit,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for leave Credit account
            return $accountScope->inScope($leaveCredit->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as leave Credit's account
                return $leaveCredit->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($leaveCredit->company_id);
        }

        return false;
    }
}
