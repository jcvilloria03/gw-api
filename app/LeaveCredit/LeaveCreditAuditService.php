<?php

namespace App\LeaveCredit;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class LeaveCreditAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_UPDATE = 'update';
    const ACTION_BATCH_UPDATE = 'batch_update';
    const ACTION_DELETE = 'delete';
    const OBJECT_NAME = 'leave_credit';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\LeaveCredit\LeaveCreditRequestService
     */
    private $leaveCreditRequestService;

    /**
     * LeaveCreditAuditService constructor
     *
     * @param AuditService $auditService
     * @param LeaveCreditRequestService $leaveCreditRequestService
     */
    public function __construct(
        AuditService $auditService,
        LeaveCreditRequestService $leaveCreditRequestService
    ) {
        $this->auditService = $auditService;
        $this->leaveCreditRequestService = $leaveCreditRequestService;
    }

    /**
     * Log LeaveCredit related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log LeaveCredit create
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch create
     * @return void
     */
    public function logCreate(array $cacheItem, bool $isBatch = false)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $employee = $new['employee'];

        $item = new AuditItem([
            'company_id' => $employee['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE : self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'employee_id' => $new['employee_id'],
                'employee_full_name' => $employee['full_name'],
                'leave_type_id' => $new['leave_type_id'],
                'value' => $new['value'],
                'unit' => $new['unit']
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log LeaveCredit update
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch update
     * @return void
     */
    public function logUpdate(array $cacheItem, bool $isBatch = false)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $employee = $new['employee'];

        $data = [
            'id' => $new['id'],
            'employee_full_name' => $employee['full_name'],
            'new' => $new,
        ];

        if (!$isBatch) {
            $data['old'] = json_decode($cacheItem['old'], true);
        }

        $item = new AuditItem([
            'company_id' => $employee['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_UPDATE : self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);

        $this->auditService->log($item);
    }

    /**
     *
     * Log LeaveCredit Delete
     *
     * @param array $cacheItem
     */
    public function logDelete(array $cacheItem)
    {
        $leaveCreditData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $leaveCreditData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $leaveCreditData->id
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log Leave Credit upload
     *
     * @param array $leaveCreditIds
     * @param array $user
     * @return void
     */
    public function logUploadedLeaveCredits(array $leaveCreditIds, array $user)
    {
        $this->logBatchCreate($leaveCreditIds['created_ids'], $user);
        $this->logBatchUpdate($leaveCreditIds['updated_ids'], $user);
    }

    /**
     * Log batch create leave credits
     *
     * @param array $ids leave credits ids
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->leaveCreditRequestService->get($id);
                $leaveCreditData = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($leaveCreditData),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit Leave Credit batch create with id = ' . $id);
            }
        }
    }

    /**
     * Log batch update leave credits
     *
     * @param array $ids leave credits ids
     * @param array $user
     * @return void
     */
    public function logBatchUpdate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->leaveCreditRequestService->get($id);
                $leaveCreditData = json_decode($response->getData(), true);

                $this->logUpdate([
                    'new' => json_encode($leaveCreditData),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit Leave Credit batch update with id = ' . $id);
            }
        }
    }
}
