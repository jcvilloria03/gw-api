<?php

namespace App\Contribution;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ContributionRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get Contribution Basis
     *
     * @return \Illuminate\Http\JsonResponse Contribution Basis list
     */
    public function getBasis()
    {
        $request = new Request(
            'GET',
            "/contribution/basis"
        );
        return $this->send($request);
    }
}
