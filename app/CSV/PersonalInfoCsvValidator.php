<?php

namespace App\CSV;

use League\Csv\Reader;
use Illuminate\Http\UploadedFile;

/**
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class PersonalInfoCsvValidator
{
    /**
     * @var array header columns for checking of duplicate
     * */
    protected $headerColumns = [];

    /**
     * Validates a csv
     *
     * @param UploadedFile $file
     * @param array $subscriptionStats
     * @param string $uploaderProductSeat
     * @throws CsvValidatorException
     */
    public function validate(
        UploadedFile $file,
        array $subscriptionStats,
        string $uploaderProductSeat
    ) {
        // check file extension
        if ($file->getClientOriginalExtension() !== 'csv') {
            throw new CsvValidatorException('Uploaded file does not have a CSV file extension.');
        }

        $fileName = $file->getPathName();
        $reader = Reader::createFromPath($fileName);

        $numRows = $reader->each(function () {
            return true;
        });

        if ($numRows <= 1) {
            throw new CsvValidatorException('Uploaded CSV should have 2 or more rows.');
        }

        // Determine available licenses for the uploader's product code
        if (!empty($uploaderProductSeat)) {
            $mainProductCode = $uploaderProductSeat;
            // Check subscription stats if product code would be
            $foundSubscriptionLicense = false;
            $remaining = 0;
            // Check main product code license availablility
            foreach ($subscriptionStats['products'] as $subscriptionLicense) {
                if ($mainProductCode == $subscriptionLicense['product_code']) {
                    $remaining = $subscriptionLicense['licenses_available'];
                    $foundSubscriptionLicense = true;
                    if (empty($remaining) || ($remaining < ($numRows-1))) {
                        $foundSubscriptionLicense = false;
                        break;
                    }
                }
            }

            // If for some reason the main product code is not found to the list of available
            // products or the available units is insufficient then raise an error
            if (!$foundSubscriptionLicense) {
                throw new CsvValidatorException(
                    'Account does not have enough licenses. '
                    . ucwords($mainProductCode) . " has {$remaining} available licenses."
                );
            }
        } else {
            throw new CsvValidatorException(
                'Unable to find plan details'
            );
        }

        $header = $reader->fetchOne();
        foreach ($header as $column) {
            if (empty($column)) {
                throw new CsvValidatorException('Uploaded CSV should not have blank CSV Header.');
            }

            if (in_array($column, $this->headerColumns)) {
                throw new CsvValidatorException('Uploaded CSV should not have duplicate CSV Header.');
            }
            $this->headerColumns[] = $column;
        }
    }
}
