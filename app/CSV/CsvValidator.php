<?php

namespace App\CSV;

use League\Csv\Reader;
use Illuminate\Http\UploadedFile;

class CsvValidator {
    /**
     * @var array header columns for checking of duplicate
     * */
    protected $headerColumns = [];

    /**
     * Validates a csv
     *
     * @param UploadedFile $file
     * @throws CsvValidatorException
     */
    public function validate(UploadedFile $file)
    {
        // check file extension
        if ($file->getClientOriginalExtension() !== 'csv') {
            throw new CsvValidatorException('Uploaded file does not have a CSV file extension.');
        }

        $fileName = $file->getPathName();
        $reader = Reader::createFromPath($fileName);

        $numRows = $reader->each(function () {
            return true;
        });

        if ($numRows <= 1) {
            throw new CsvValidatorException('Uploaded CSV should have 2 or more rows.');
        }

        $header = $reader->fetchOne();
        foreach ($header as $column) {
            if (empty($column)) {
                throw new CsvValidatorException('Uploaded CSV should not have blank CSV Header.');
            }

            if (in_array($column, $this->headerColumns)) {
                throw new CsvValidatorException('Uploaded CSV should not have duplicate CSV Header.');
            }
            $this->headerColumns[] = $column;
        }
    }
}
