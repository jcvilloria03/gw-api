<?php

namespace App\BasePay;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class BasePayRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get Base Pay Units
     *
     * @return \Illuminate\Http\JsonResponse Base Pay Units list
     */
    public function getUnits()
    {
        $request = new Request(
            'GET',
            "/base_pay/units"
        );
        return $this->send($request);
    }
}
