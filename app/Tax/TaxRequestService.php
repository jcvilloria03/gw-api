<?php

namespace App\Tax;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TaxRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get Tax Types
     *
     * @return \Illuminate\Http\JsonResponse Tax Types list
     */
    public function getTypes()
    {
        $request = new Request(
            'GET',
            "/tax/types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get consultant tax rates
     *
     * @return \Illuminate\Http\JsonResponse consultant tax rate list
     */
    public function getConsultantTaxRates()
    {
        $request = new Request(
            'GET',
            "/tax/consultant_tax_rates"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Tax Statuses
     *
     * @return \Illuminate\Http\JsonResponse Tax Statuses list
     */
    public function getStatuses()
    {
        $request = new Request(
            'GET',
            "/tax/statuses"
        );
        return $this->send($request);
    }
}
