<?php

namespace App\Approval;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class ApprovalAuthorizationService extends AuthorizationService
{
    const APPROVE_TASK = 'approval.request';

    /**
     * Authorize admin approve task scope.
     *
     * @param \stdClass $request
     * @param array $user
     *
     * @return bool
     */
    public function authorizeApprove(\stdClass $request, array $user)
    {
        $this->buildUserPermissions($user['user_id']);

        return $this->authorizeTask($request, $user, self::APPROVE_TASK);
    }

    /**
     * Authorize user approvals related tasks
     *
     * @param \stdClass $approval
     * @param array $user
     * @param string $taskType
     *
     * @return bool
     */
    private function authorizeTask(
        \stdClass $approval,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($approval->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $approval->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($approval->company_id);
        }

        return false;
    }
}
