<?php

namespace App\Approval;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * Class ApprovalRequestService
 *
 * @package App\Approval
 */
class ApprovalRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get the user approvals for given ID and request data
     *
     * @param int   $userId User ID
     * @param array $data   Request data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserApprovals(int $userId, array $data)
    {
        $request = new Request(
            'POST',
            "/user/{$userId}/approvals",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }
}
