<?php

namespace App\Approval;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class ApprovalAuditService
{
    const OBJECT_NAME = 'approval';
    const ACTION_APPROVE_REQUEST = 'approve';
    const ACTION_DENY_REQUEST = 'deny';
    const ACTION_CANCEL_REQUEST = 'cancel';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Approval related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_APPROVE_REQUEST:
                $this->logApprove($cacheItem);
                break;
            case self::ACTION_DENY_REQUEST:
                $this->logDeny($cacheItem);
                break;
            case self::ACTION_CANCEL_REQUEST:
                $this->logCancel($cacheItem);
                break;
        }
    }

    /**
     * Approve Request
     *
     * @param array $cacheItem
     */
    public function logApprove(array $cacheItem)
    {
        $data = json_decode($cacheItem['requests'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_APPROVE_REQUEST,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'requests' => $data
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Deny Request
     *
     * @param array $cacheItem
     *
     */
    public function logDeny(array $cacheItem)
    {
        $data = json_decode($cacheItem['requests']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DENY_REQUEST,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'requests' => $data
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Cancel Request
     *
     * @param array $cacheItem
     */
    public function logCancel(array $cacheItem)
    {
        $data = json_decode($cacheItem['requests'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $user['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CANCEL_REQUEST,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'requests' => $data
            ]
        ]);
        $this->auditService->log($item);
    }
}
