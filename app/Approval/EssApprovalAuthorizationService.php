<?php

namespace App\Approval;

use App\Authorization\EssBaseAuthorizationService;

class EssApprovalAuthorizationService extends EssBaseAuthorizationService
{
    const APPROVE_TASK = 'ess.approval.request';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeApprove(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::APPROVE_TASK);
    }
}
