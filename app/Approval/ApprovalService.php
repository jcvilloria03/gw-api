<?php

namespace App\Approval;

use Illuminate\Support\Arr;
use App\User\UserService;

/**

 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ApprovalService
{
    public static function isEitherRequestorOrApprover(int $employeeId, $approvalRequest, $userData = [])
    {
        // check if the id is the owner of the request
        if ($employeeId === $approvalRequest->employee_id) {
            return true;
        }

        $approverUserId = Arr::get($userData, 'user_id');
        $workflowLevelApprovers = [];
        $workflowId = $approvalRequest->workflow_id ?? null;
        if (empty($workflowId)) {
            return false;
        }

        $workflowRequestService = app()->make(\App\Workflow\WorkflowRequestService::class);
        $responseData = $workflowRequestService->getWorkflowLevels($workflowId);
        $workflowLevels = collect(json_decode($responseData->getData(), true));
        foreach ($workflowLevels as $level) {
            $approvers = Arr::get($level, 'approvers');
            if (!empty($approvers)) {
                $workflowLevelApprovers = array_merge($workflowLevelApprovers, Arr::get($level, 'approvers'));
            }
        }

        $approverWorkflowData = collect($workflowLevelApprovers)
            ->filter(function ($val) use ($employeeId, $approverUserId) {
                if (isset($val['employee_id']) && $val['employee_id'] == $employeeId) {
                    return true;
                }
                if (!empty($approverUserId) && isset($val['user_id']) && $val['user_id'] == $approverUserId) {
                    return true;
                }
            });

        return $approverWorkflowData->isNotEmpty();
    }

    public static function isEitherRequestorOrApproverNew(int $employeeId, $approvalRequest, $userData = [])
    {
        // check if the id is the owner of the request
        if ($employeeId === $approvalRequest->employee_id) {
            return true;
        }

        $approverUserId = Arr::get($userData, 'user_id');
        $workflowLevelApprovers = [];
        $workflowId = $approvalRequest->workflow_id ?? null;
        $accountId = $approvalRequest->account_id;
        $companyId = $approvalRequest->company_id ?? Arr::get($userData, 'employee_company_id');

        if (empty($workflowId)) {
            return false;
        }

        $userService = app()->make(UserService::class);

        $workflowLevels = $approvalRequest->workflow_levels;
        $users = $userService->getUsersFromEssentialData($accountId, $companyId, null);

        foreach ($workflowLevels as $level) {
            $approvers = $level->approvers;

            if (!empty($approvers)) {
                $approverData = collect($approvers)->flatten(1)->map(function ($approver) use ($users) {
                    $userData = collect($users)->where('employee_id', $approver->employee_id)->first();
                    return [
                        'employee_id' => $approver->employee_id,
                        'type' => $approver->type,
                        'user_id' => $userData['id'],
                        'name' => $userData['name'],
                    ];
                })->toArray();

                $workflowLevelApprovers = array_merge($workflowLevelApprovers, $approverData);
            }
        }

        $approverWorkflowData = collect($workflowLevelApprovers)
            ->filter(function ($val) use ($employeeId, $approverUserId) {
                if (isset($val['employee_id']) && $val['employee_id'] == $employeeId) {
                    return true;
                }
                if (!empty($approverUserId) && isset($val['user_id']) && $val['user_id'] == $approverUserId) {
                    return true;
                }
                if ($val['type'] == 'user' && !empty($approverUserId) && $val['employee_id'] == $approverUserId) {
                    return true;
                }
            });

        return $approverWorkflowData->isNotEmpty();
    }
}
