<?php

namespace App\OtherIncome;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use App\Audit\AuditServiceTrait;

class OtherIncomeAuditService
{
    use AuditServiceTrait;

    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Other Income related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_BATCH_CREATE:
                $this->logBatchCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log Other Income create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => array_get($cacheItem, 'type') ?: array_get($data, 'type'),
            'data' => $data
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log Other Income Batch create
     *
     * @param array $cacheItem
     */
    public function logBatchCreate(array $cacheItem)
    {
        $item = new AuditItem([
            'company_id' => $cacheItem['company_id'],
            'user_id' => $cacheItem['user_id'],
            'account_id' => $cacheItem['account_id'],
            'action' => self::ACTION_BATCH_CREATE,
            'data' => $cacheItem['other_income_ids'],
            'object_name' => $cacheItem['type']
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log Other Income update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => $cacheItem['type'],
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Other Income Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $data = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data->id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => $cacheItem['type'],
            'data' => [
                'id' => $data->id
            ]
        ]);
        $this->auditService->log($item);
    }
}
