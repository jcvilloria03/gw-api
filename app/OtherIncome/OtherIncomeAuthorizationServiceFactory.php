<?php

namespace App\OtherIncome;

use App\Allowance\AllowanceAuthorizationService;
use App\Bonus\BonusAuthorizationService;
use App\Commission\CommissionAuthorizationService;
use App\Deduction\DeductionAuthorizationService;
use App\Adjustment\AdjustmentAuthorizationService;
use Illuminate\Support\Arr;

class OtherIncomeAuthorizationServiceFactory
{
    const PHILIPPINE_ALLOWANCE = 'App\Model\PhilippineAllowance';
    const PHILIPPINE_BONUS = 'App\Model\PhilippineBonus';
    const PHILIPPINE_COMMISSION = 'App\Model\PhilippineCommission';
    const PHILIPPINE_DEDUCTION = 'App\Model\PhilippineDeduction';
    const PHILIPPINE_ADJUSTMENT = 'App\Model\PhilippineAdjustment';

    const AUTH_SERVICES = [
        self::PHILIPPINE_BONUS => BonusAuthorizationService::class,
        self::PHILIPPINE_COMMISSION => CommissionAuthorizationService::class,
        self::PHILIPPINE_ALLOWANCE => AllowanceAuthorizationService::class,
        self::PHILIPPINE_DEDUCTION => DeductionAuthorizationService::class,
        self::PHILIPPINE_ADJUSTMENT => AdjustmentAuthorizationService::class,
    ];

    const AUTH_MODULES = [
        self::PHILIPPINE_ADJUSTMENT => ['employees.people.adjustments', 'employees.adjustments'],
        self::PHILIPPINE_ALLOWANCE => ['employees.people.allowances', 'employees.allowances'],
        self::PHILIPPINE_BONUS => ['employees.people.bonuses', 'payroll.bonuses'],
        self::PHILIPPINE_DEDUCTION => ['employees.people.deductions', 'employees.deductions'],
        self::PHILIPPINE_COMMISSION => ['employees.people.commissions', 'payroll.commissions'],
    ];

    public static function get($type)
    {
        if (empty(self::AUTH_SERVICES[$type])) {
            throw new \Exception("Other Income {$type} not found!");
        }

        return app()->make(self::AUTH_SERVICES[$type]);
    }

    public static function getModules($type): array
    {
        if (!Arr::has(self::AUTH_MODULES, $type)) {
            throw new \Exception("Other Income {$type} not found!");
        }

        return Arr::get(self::AUTH_MODULES, $type);
    }
}
