<?php

namespace App\OtherIncome;

use App\Authz\AuthzDataScope;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class OtherIncomeService
{
    const OTHER_INCOME_ALLOWANCE = 'allowance';
    const OTHER_INCOME_BONUS = 'bonus';
    const OTHER_INCOME_COMMISSION = 'commission';
    const OTHER_INCOME_DEDUCTION = 'deduction';
    const OTHER_INCOME_ADJUSTMENT = 'adjustment';

    const OTHER_INCOMES = [
        self::OTHER_INCOME_BONUS,
        self::OTHER_INCOME_COMMISSION,
        self::OTHER_INCOME_ALLOWANCE,
        self::OTHER_INCOME_DEDUCTION,
        self::OTHER_INCOME_ADJUSTMENT,
    ];

    const AUTH_MODULES = [
        self::OTHER_INCOME_ADJUSTMENT => 'employees.adjustments',
        self::OTHER_INCOME_ALLOWANCE => 'employees.allowances',
        self::OTHER_INCOME_BONUS => 'payroll.bonuses',
        self::OTHER_INCOME_COMMISSION => 'payroll.commissions',
        self::OTHER_INCOME_DEDUCTION => 'employees.deductions',
    ];

    const AUTHZ_MODULES = [
        self::OTHER_INCOME_BONUS => ['payroll.bonuses'],
        self::OTHER_INCOME_COMMISSION => ['payroll.commissions'],
        self::OTHER_INCOME_DEDUCTION => ['employees.deductions'],
        self::OTHER_INCOME_ADJUSTMENT => ['employees.adjustments'],
        self::OTHER_INCOME_ALLOWANCE => ['employees.allowances'],
    ];

    public function checkAuthorization(
        array $otherIncomeIds,
        array $otherIncomes,
        AuthzDataScope $authzDataScope
    ): array {
        $otherIncomeCollection = Collection::make($otherIncomes)
            ->map(function ($otherIncome) {
                return array_get($otherIncome, 'id');
            });
        $specifiedAndFoundDiff = Collection::make($otherIncomeIds)->diff($otherIncomeCollection);

        if ($specifiedAndFoundDiff->count() > 0) {
            $idsAsString = $specifiedAndFoundDiff->implode(',');

            return [
                'message' =>  'Specified Other Income(s) not found. Id(s): ' . $idsAsString,
                'code' => Response::HTTP_NOT_FOUND,
            ];
        }
        $unAuthorizedIds = [];
        foreach ($otherIncomes as $otherIncome) {
            $isAuthorized = false;
            foreach (OtherIncomeAuthorizationServiceFactory::getModules($otherIncome['type_name']) as $module) {
                $companyId = $otherIncome['company_id'];
                if ($authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId, $module)) {
                    $isAuthorized = true;
                    break;
                }
            }
            if (!$isAuthorized) {
                $unAuthorizedIds[] = $otherIncome['id'];
            }
        }

        if (!empty($unAuthorizedIds)) {
            return [
                'message' => 'Not authorized. Ids: ' . implode(',', $unAuthorizedIds),
                'code' => Response::HTTP_UNAUTHORIZED,
            ];
        }

        return ['code' => Response::HTTP_OK];
    }
}
