<?php

namespace App\OtherIncome;

use App\Authz\AuthzDataScope;
use App\Request\DownloadRequestService;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Str;

class OtherIncomeRequestService extends DownloadRequestService
{
    /**
     * Get single other income.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id, AuthzDataScope $authzDataScope = null)
    {
        return $this->send(new Request(
            'GET',
            "/other_income/{$id}"
        ), $authzDataScope);
    }

    /**
     * Call endpoint to get multiple Other Incomes.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMultiple(int $companyId, array $otherIncomeIds)
    {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/other_incomes",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeIds])
        ));
    }

    /**
     * Call endpoint to get Other Incomes for Company.
     *
     * @param int $companyId Company ID
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCompanyIncomes(
        int $companyId,
        string $type,
        AuthzDataScope $authzDataScope = null,
        array $queryString = []
    ) {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/other_incomes/{$type}?" . http_build_query($queryString)
        ), $authzDataScope);
    }

    /**
     * Call endpoint to get Other Incomes assigned to Employee
     *
     * @param int $companyId Company ID
     * @param int $employeeId Employee ID
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeOtherIncomes(int $companyId, int $employeeId, string $type)
    {
        return $this->send(new Request(
            'GET',
            "company/{$companyId}/employee/{$employeeId}/other_incomes/{$type}"
        ));
    }

    /**
     * Call endpoint to delete multiple Other Incomes.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMultiple(int $companyId, array $otherIncomeIds, AuthzDataScope $dataScope = null)
    {
        return $this->send(new Request(
            'DELETE',
            "/company/{$companyId}/other_income",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeIds])
        ), $dataScope);
    }

    /**
     * Call endpoint to check if delete is available.
     *
     * @param int $companyId Company ID
     * @param array $otherIncomeIds
     * @return \Illuminate\Http\JsonResponse
     */
    public function isDeleteAvailable(int $companyId, array $otherIncomeIds)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/other_income/is_delete_available",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeIds])
        ));
    }

    /**
     * Call endpoint to fetch Data to be saved in Other Income upload job
     *
     * @param string $id Job ID
     * @return \Illuminate\Http\JsonResponse Company information
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/company/other_income/upload/preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to download CSV for Other Incomes
     *
     * @param int $companyId
     * @param string $type
     * @param array $otherIncomeIds
     * @return \Illuminate\Http\JsonResponse CSV
     */
    public function downloadOtherIncomesCsv(
        int $companyId,
        string $type,
        array $otherIncomeIds,
        AuthzDataScope $dataScope = null
    ) {
        $request = new Request(
            'POST',
            "/company/{$companyId}/other_incomes/{$type}/download",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $otherIncomeIds])
        );

        return $this->download($request, $dataScope);
    }

    /**
     * Trigger upload processing
     */
    public function processFileUpload(
        $companyId,
        $incomeType,
        $fileBucket,
        $fileKey,
        AuthzDataScope $dataScope = null
    ) {
        $payload = [
            'fileKey' => $fileKey,
            'fileBucket' => $fileBucket
        ];
        if (!Str::endsWith($incomeType, "_type")) {
            $incomeType .= '_type';
        }
        $request = new Request(
            'POST',
            "/company/{$companyId}/{$incomeType}/upload",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );
        return $this->send($request, $dataScope);
    }

    /**
     * Get upload processing result
     */
    public function getFileUploadStatus($companyId, $incomeType, $jobId)
    {
        if (!Str::endsWith($incomeType, "_type")) {
            $incomeType .= '_type';
        }
        $request = new Request(
            'GET',
            "/company/{$companyId}/{$incomeType}/upload/status?job_id={$jobId}"
        );
        return $this->send($request);
    }
}
