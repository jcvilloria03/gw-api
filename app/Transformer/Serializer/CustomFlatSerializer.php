<?php
namespace App\Transformer\Serializer;

use League\Fractal\Serializer\DataArraySerializer;

/**
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 *
 */
class CustomFlatSerializer extends DataArraySerializer
{
    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        return $data;
    }
}
