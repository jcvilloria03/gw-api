<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class EssEmployeeShiftTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(array $shift)
    {
        return [
            'id' => $shift['id'],
            'schedule_id' => $shift['schedule_id'],
            'schedule' => [
                'name' => $shift['schedule']['name'],
                'type' => $shift['schedule']['type'],
                'default' => false,
                'day_type' => 'regular',
                'start_time' => $shift['schedule']['start_time'],
                'end_time' => $shift['schedule']['end_time'],
                'total_hours' => $shift['schedule']['total_hours'],
            ]
        ];
    }
}
