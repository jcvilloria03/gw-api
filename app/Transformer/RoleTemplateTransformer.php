<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;
use App\Model\Role;

class RoleTemplateTransformer extends TransformerAbstract
{
    /**
     * Transform a Role model into an array
     *
     * @param Role $role
     * @return array
     */
    public function transform(Role $role)
    {
        return [
            'role_id' => $role->id,
            'name' => $role->name,
        ];
    }
}
