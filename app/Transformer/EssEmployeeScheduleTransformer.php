<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class EssEmployeeScheduleTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(array $schedule)
    {
        return [
            'id' => $schedule['id'],
            'name' => $schedule['name'],
            'type' => $schedule['type'],
            'default' => false,
            'day_type' => 'regular',
            'never_ends' => $schedule['repeat']['end_never'],
            'start_date' => $schedule['start_date'],
            'end_date' => $schedule['end_date'],
            'start_time' => $schedule['start_time'],
            'end_time' => $schedule['end_time'],
            'total_hours' => $schedule['total_hours'],
        ];
    }
}
