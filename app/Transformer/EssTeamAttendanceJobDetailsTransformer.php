<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class EssTeamAttendanceJobDetailsTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(array $job)
    {
        return [
            'job_id' => $this->getJobId($job['job_id']),
            'status' => $job['status'],
            'count' => intval($job['count']),
            'done' => intval($job['done']),
            'error' => intval($job['error'] ?? 0),
            'warning' => intval($job['warnings'])
        ];
    }

    /**
     * Get job ID string
     *
     * @param  string $jobId Get Job ID
     * @return string
     */
    protected function getJobId(string $jobId)
    {
        return explode(':', $jobId)[1] ?? null;
    }
}
