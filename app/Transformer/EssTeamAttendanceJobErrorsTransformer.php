<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class EssTeamAttendanceJobErrorsTransformer extends TransformerAbstract
{
    /** @var string */
    protected $jobId;

    /**
     * Constructs EssTeamAttendanceJobErrorsTransformer
     *
     * @param string $jobId Job ID
     */
    public function __construct(string $jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * @return array
     */
    public function transform(array $entries)
    {
        $errors = [];
        $warnings = [];

        foreach ($entries as $entry) {
            $isWarning = boolval($entry['is_warning'] ?? false);

            if ($isWarning) {
                $warnings[] = $this->transformEntry($entry);

                continue;
            }

            $errors[] = $this->transformEntry($entry);
        }

        return [
            'job_id' => $this->jobId,
            'errors' => $errors,
            'warnings' => $warnings
        ];
    }

    /**
     * Transform single error/warning item
     *
     * @param  array  $error Error/warning data
     * @return array
     */
    protected function transformEntry(array $error)
    {
        return [
            'employee_id' => $error['employee_uid'] ?? null,
            'date' => $error['attendance_date'] ?? null,
            'message' => $error['message']
        ];
    }
}
