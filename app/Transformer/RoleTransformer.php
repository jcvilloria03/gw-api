<?php
namespace App\Transformer;

use App\Role\UserRoleService;
use League\Fractal\TransformerAbstract;
use App\Model\Role;

class RoleTransformer extends TransformerAbstract
{
    /**
     * @var UserRoleService
     */
    protected $userRoleService;

    public function __construct(UserRoleService $userRoleService)
    {
        $this->userRoleService = $userRoleService;
    }

    /**
     * Transform a Role model into an array
     *
     * @param Role $role
     * @return array
     */
    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'account_id' => $role->account_id,
            'company_id' => $role->company_id === 0 ? null : $role->company_id,
            'name' => $role->name,
            'type' => $role->type,
            'custom_role' => $role->custom_role,
            'users_count' => $role->userRoles()->count(),
            'time_and_attendance' => in_array(Role::ACCESS_MODULE_TA, (array) $role->module_access),
            'payroll' => in_array(Role::ACCESS_MODULE_PAYROLL, (array) $role->module_access),
            'users' => $role->users ?? [],
            'number_of_active_users' => $this->userRoleService->getTotalAssignedUsers(
                $role->account_id,
                $role
            ),
        ];
    }
}
