<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;
use App\Permission\TaskScopes;

class TaskScopesTransformer extends TransformerAbstract
{
    /**
     * Transform a TaskScopes model into an array
     *
     * @param TaskScopes $taskScopes
     * @return array
     */
    public function transform(TaskScopes $taskScopes)
    {
        return $taskScopes->toArray();
    }
}
