<?php

namespace App\Transformer;

use League\Fractal\TransformerAbstract;
use App\Model\Role;

class TimeSheetTransformer extends TransformerAbstract
{

    /**
     * Transform a Role model into an array
     *
     * @param Timesheet $timesheet
     * @return array
     */
    public function transform($timesheet)
    {
        return [
            'employee_uid' => $timesheet['employee_uid']['N'],
            'company_uid' => $timesheet['company_uid']['S'],
            'max_clock_out' => isset($timesheet['max_clock_out'])
                ? $timesheet['max_clock_out']['BOOL']
                : false,
            'timestamp' => $timesheet['timestamp']['N'],
            'tags' => isset($timesheet['tags'])
                ? $timesheet['tags']['L']
                : [],
            'state' => $timesheet['state']['BOOL'],
        ];
    }
}
