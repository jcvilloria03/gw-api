<?php

namespace App\AnnualEarning;

use App\Authz\AuthzDataScope;
use App\Request\DownloadRequestService;
use GuzzleHttp\Psr7\Request;

class AnnualEarningRequestService extends DownloadRequestService
{
    /**
     * Call endpoint to set annual earning
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function setAnnualEarning(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/annual_earning/set",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch annual earnings for company grouped by year
     *
     * @param int $id Company ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnnualEarningsForCompanyGroupedByYear(int $id, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/annual_earning/group_by_year"
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to fetch all annual earnings for employee
     *
     * @param int $id Company ID
     * @param int $employeeId Employee ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnnualEarningsByEmployee(int $id, int $employeeId)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/employee/{$employeeId}/annual_earning"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch annual earning by id
     *
     * @param int $id Annual Earning ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/annual_earning/{$id}"
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to download CSV
     *
     * @param int $companyId
     * @param array $annualEarningIds
     * @return \Illuminate\Http\JsonResponse CSV
     */
    public function downloadCsv(int $companyId, array $annualEarningIds)
    {
        $request = new Request(
            'POST',
            "company/{$companyId}/annual_earning/download",
            ['Content-Type' => 'application/json'],
            json_encode(['ids' => $annualEarningIds])
        );

        return $this->download($request);
    }

    /*
     * Call endpoint to fetch Data to be saved in Annual earning upload job
     *
     * @param string $id Job ID
     * @return json Company information
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/annual_earning/upload_preview?job_id={$jobId}"
        );
        return $this->send($request);
    }

   /**
     * Trigger upload processing
     */
    public function processFileUpload($companyId, $fileBucket, $fileKey, AuthzDataScope $authzDataScope = null)
    {
        $payload = [
            'fileKey' => $fileKey,
            'fileBucket' => $fileBucket,
        ];
        
        $request = new Request(
            'POST',
            "/company/{$companyId}/annual_earning/upload",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Get upload processing result
     */
    public function getFileUploadStatus($companyId, $jobId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/annual_earning/upload/status?job_id={$jobId}"
        );
        return $this->send($request);
    }

    /**
     * Delete annual earning
     */
    public function deleteAnnualEarning($companyId, array $annualEarningIds, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/annual_earning/bulk_delete",
            ['Content-type' => 'application/json'],
            json_encode([ "ids" => $annualEarningIds ])
        );
        return $this->send($request, $authzDataScope);
    }
}
