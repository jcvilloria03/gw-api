<?php

namespace App\AnnualEarning;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use App\Audit\AuditServiceTrait;

class AnnualEarningAuditService
{
    use AuditServiceTrait;

    const ANNUAL_EARNING = 'annual_earning';

    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @var AuditService
     */
    private $auditService;

    /**
     * @var AnnualEarningRequestService
     */
    private $requestService;

    public function __construct(
        AuditService $auditService,
        AnnualEarningRequestService $requestService
    ) {
        $this->auditService = $auditService;
        $this->requestService = $requestService;
    }

    /**
     * Log Annual Earning related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
        }
    }

    /**
     * Log Annual Earning create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem, $isBatch = false)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE : self::ACTION_CREATE,
            'object_name' => self::ANNUAL_EARNING,
            'data' => $data
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log batch create annual earnings
     *
     * @param array $ids annual earnings ids
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->requestService->get($id);
                $data = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($data),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit Annual earning batch create with id = ' . $id);
            }
        }
    }
}
