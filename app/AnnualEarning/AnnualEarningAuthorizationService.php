<?php

namespace App\AnnualEarning;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class AnnualEarningAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.annual_earnings';
    const CREATE_TASK = 'create.annual_earnings';
    const UPDATE_TASK = 'edit.annual_earnings';
    const DELETE_TASK = 'delete.annual_earnings';

    /**
     * @param \stdClass $annualEarnings
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $annualEarnings, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($annualEarnings, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $annualEarnings
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $annualEarnings, array $user)
    {
        $this->buildUserPermissions($user['user_id']);

        return $this->authorizeTask($annualEarnings, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $annualEarnings
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $annualEarnings, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($annualEarnings, $user, self::UPDATE_TASK);
    }


    /**
     * @param \stdClass $annualEarnings
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $annualEarnings, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($annualEarnings, $user, self::DELETE_TASK);
    }

    /**
     * Authorize annual earnings related tasks
     *
     * @param \stdClass $annualEarnings
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $annualEarnings,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);

        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($annualEarnings->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $annualEarnings->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($annualEarnings->company_id);
        }

        return false;
    }
}
