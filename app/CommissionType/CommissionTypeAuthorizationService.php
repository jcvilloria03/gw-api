<?php

namespace App\CommissionType;

use App\Common\CommonAuthorizationService;

class CommissionTypeAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.commission_types';
    public $createTask = 'create.commission_types';
    public $updateTask = 'edit.commission_types';
    public $deleteTask = 'delete.commission_types';
}
