<?php

namespace App\CommissionType;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineCommissionTypeRequestService extends RequestService
{
    /**
     * Call endpoint to create company commission types
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Amortizations Info
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/commission_type/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check commission type name is available
     *
     * @param int $companyId Company ID
     * @param array $data commission type information
     * @return \Illuminate\Http\JsonResponse Availability of commission type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$companyId}/other_income_type/commission_type/is_name_available",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update commission type.
     *
     * @param int $id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $data)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/commission_type/{$id}",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }
}
