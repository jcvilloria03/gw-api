<?php

namespace App\Rule;

use Illuminate\Validation\Rule as IlluminateRule;

class ResetPasswordRule extends Rule
{
    public function setRules()
    {
        $this->rules = [
            'token' => [
                'required',
                'string',
                IlluminateRule::exists('reset_password_tokens', 'token')
                    ->where(function ($query) {
                        $query->whereNull('deleted_at', null);
                    })
            ],

            'new_password'              => 'required|string|min:8|confirmed',
            'new_password_confirmation' => 'required|string|min:8'
        ];
    }

    public function setMessages()
    {
        parent::setMessages();

        $this->messages = array_merge($this->messages, [
            'token.exists' => 'The given token is invalid.'
        ]);
    }
}
