<?php

namespace App\Rule;

use App\Rule\Rule as AppRule;
use Illuminate\Validation\Rule;
use App\Model\Role;

class RoleRule extends AppRule
{
    /**
     * array $attributes
     */
    private $attributes;

    public function __construct(array $attributes, bool $isUpdating = false)
    {
        $this->attributes = $attributes;
        $this->isUpdating = $isUpdating;

        parent::__construct();
    }

    protected function setRules()
    {
        $accountId = array_get($this->attributes, 'account_id', '');
        $companyId = array_get($this->attributes, 'company_id', '');

        $this->rules = [
            'name' => [
                !$this->isUpdating ? 'required' : '',
                Rule::unique('roles', 'name')->where(function ($query) use ($accountId, $companyId) {
                    $query = $query->where('account_id', $accountId);

                    if ($companyId) {
                        $query->where('company_id', $companyId);
                    }
                }),
            ],
            'company_id' => ['integer', 'nullable'],
            'selected_modules' => [
                'required',
                'array',
                'in:' . implode(',', Role::MODULE_ACCESSES),
                'array_contains:' . Role::ACCESS_MODULE_HRIS
            ],
            'tasks' => ['required', 'array', ],
            'tasks.*' => [
                'int',
                Rule::exists('tasks', 'id', function ($query) use ($accountId, $companyId) {
                    if ($companyId) {
                        $query->where('company_id', $companyId);
                    } else {
                        $query->where('account_id', $accountId);
                    }
                })
            ]
        ];
    }

    protected function setMessages()
    {
        parent::setMessages();
        $this->messages = array_merge(
            $this->messages,
            [
                'name.unique' => 'That name is already in use by another role, please choose another.',
                'name.required' => 'Please enter a name for this role.',
                'selected_modules.array_contains' => 'Selected modules must contain HRIS module',
            ]
        );

        $this->customAttributes = [
            'tasks.*' => 'task',
        ];
    }
}
