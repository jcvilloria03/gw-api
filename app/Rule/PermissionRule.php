<?php

namespace App\Rule;

class PermissionRule extends Rule
{
    public function setRules()
    {
        $this->rules = [
            'task_id' => [
                'required',
                'int',
                'exists:tasks,id',
            ],
        ];
    }
}
