<?php

namespace App\Rule;

class EssChangePasswordRule extends Rule
{
    public function setRules()
    {
        $this->rules = [
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
            'new_password_confirmation' => ['required', 'string', 'min:8']
        ];
    }
}
