<?php

namespace App\Rule;

use App\Permission\TargetType;

class ScopeRule extends Rule
{
    public function setRules()
    {
        $this->rules = [
            'type' => [
                'required',
                'in:' . implode(',', TargetType::TARGET_TYPES),
            ],
            'targets' => [
                'required',
            ],
        ];
    }

    protected function setMessages()
    {
        parent::setMessages();
        $this->messages = array_merge(
            $this->messages,
            [
                'type.in' => 'Please choose a valid scope for this task',
            ]
        );
    }
}
