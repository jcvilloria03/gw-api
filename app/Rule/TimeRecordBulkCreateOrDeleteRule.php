<?php

namespace App\Rule;

class TimeRecordBulkCreateOrDeleteRule extends Rule
{
    public function setRules()
    {
        $this->rules = [
            'company_id' => 'required|integer',
            'create' => 'required_without:delete|array',
            'delete' => 'required_without:create|array',
            'create.*.employee_id' => 'required|integer',
            'create.*.type' => 'required|in:clock_in,clock_out',
            'create.*.tags' => 'array',
            'create.*.timestamp' => 'required|numeric',
            'delete.*.employee_id' => 'required|integer',
            'delete.*.timestamp' => 'required|numeric'
        ];
    }

    protected function setMessages()
    {
        parent::setMessages();

        $this->customAttributes = [
            'create.*.employee_id' => 'employee ID',
            'create.*.type' => 'time record type',
            'create.*.tags' => 'time record tags',
            'create.*.timestamp' => 'timestamp',
            'delete.*.employee_id' => 'employee ID',
            'delete.*.timestamp' => 'timestamp',
        ];
    }
}
