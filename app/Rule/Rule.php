<?php

namespace App\Rule;

abstract class Rule
{
    protected $rules;

    protected $messages;

    public function __construct()
    {
        $this->setRules();
        $this->setMessages();
    }

    abstract protected function setRules();

    protected function setMessages()
    {
        $this->messages = [
            'required' => ':Attribute is required',
            'email' => 'Invalid email',
            'absolute_number' => ':Attribute accepts numeric value only',
            'mobile_number' => ':Attribute has invalid format. It must be in (xx)xxxxxxxxxx format',
            'alpha' => ':Attribute accepts letters only',
            'name' => ':Attribute accepts letters, hyphens, periods, apostrophes, and spaces only',
            'date_format' => 'Invalid :Attribute format. Date must be in MM/DD/YYYY format',
            'in' => 'Invalid :Attribute',
            'payroll_frequency' => ':frequency payroll group is only allowed to set Basic Pay as Contribution Basis',
            'contribution_schedule' => 'Payroll Group that is schedule to First Pay will only allowed '
                .' to set Basic Pay as Contribution Basis',
            'number_format' => 'Invalid :Attribute format',
            'alphanum_hyphen_underscore' => ':Attribute accepts letters, numbers, hyphen, and underscore only',
            'employee_id_validate' => "Please enter a valid Employee ID. Employee ID field can only accept A-Z, " .
                'a-z, 0-9, hyphen "-", and underscore "_".',
        ];
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}
