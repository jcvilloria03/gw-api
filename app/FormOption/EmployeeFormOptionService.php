<?php

namespace App\FormOption;

use App\BasePay\BasePayRequestService;
use App\Tax\TaxRequestService;
use App\Contribution\ContributionRequestService;

class EmployeeFormOptionService
{
    /**
     * @var \App\BasePay\BasePayRequestService
     */
    private $basePayRequestService;

    /**
     * @var \App\Tax\TaxRequestService
     */
    private $taxRequestService;

    /**
     * @var \App\Contribution\ContributionRequestService
     */
    private $contributionRequestService;

    public function __construct(
        BasePayRequestService $basePayRequestService,
        TaxRequestService $taxRequestService,
        ContributionRequestService $contributionRequestService
    ) {
        $this->basePayRequestService = $basePayRequestService;
        $this->taxRequestService = $taxRequestService;
        $this->contributionRequestService = $contributionRequestService;
    }

    /**
     * Get form options for Philippine Employee creation
     * @return array
     */
    public function getPhilippineEmployeeFormOptions()
    {
        $basePayCollection = collect(json_decode($this->basePayRequestService->getUnits()->getData(), true));
        $taxTypeCollection = collect(json_decode($this->taxRequestService->getTypes()->getData(), true));
        $taxStatusCollection = collect(json_decode($this->taxRequestService->getStatuses()->getData(), true));
        $contributionCollection = collect(json_decode($this->contributionRequestService->getBasis()->getData(), true));
        $consultantTaxRateCollection = collect(
            json_decode($this->taxRequestService->getConsultantTaxRates()->getData(), true)
        );

        $contributions = $contributionCollection->all();

        return [
            'base_pay_unit' => $basePayCollection->all(),
            'tax_type' => $taxTypeCollection->all(),
            'tax_status' => $taxStatusCollection->all(),
            'consultant_tax_rate' => $consultantTaxRateCollection->all(),
            'sss_basis' => $contributions,
            'philhealth_basis' => $contributions,
            'hdmf_basis' => $contributions
        ];
    }
}
