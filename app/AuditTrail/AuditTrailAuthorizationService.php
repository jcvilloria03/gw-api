<?php

namespace App\AuditTrail;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class AuditTrailAuthorizationService extends AuthorizationService
{
    const VIEW_LOGS_TASK = 'view.account_logs';

    /**
     * @param int $targetAccountId
     * @param string $taskType
     * @return bool
     */
    private function authorizeAccountAccess(
        int $targetAccountId,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        // verify account scope
        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        return $accountScope && $accountScope->inScope($targetAccountId);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeGetAccountLogs(int $targetAccountId, int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeAccountAccess($targetAccountId, self::VIEW_LOGS_TASK);
    }
}
