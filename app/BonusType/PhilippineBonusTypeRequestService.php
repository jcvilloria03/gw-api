<?php

namespace App\BonusType;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineBonusTypeRequestService extends RequestService
{
    /**
     * Call endpoint to get company bonus types
     *
     * @param int $id Company ID
     * @return \Illuminate\Http\JsonResponse Bonus Types for provided company
     *
     */
    public function getCompanyBonusTypes(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/company/{$id}/bonus_types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create company bonus types
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/bonus_type/bulk_create",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update bonus type.
     *
     * @param int $id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $data)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/bonus_type/{$id}",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to check bonus type name is available
     *
     * @param int $companyId Company ID
     * @param array $data bonus type information
     * @return \Illuminate\Http\JsonResponse Availability of bonus type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$companyId}/other_income_type/bonus_type/is_name_available",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
