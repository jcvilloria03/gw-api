<?php

namespace App\BonusType;

use App\Common\CommonAuthorizationService;

class BonusTypeAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.bonus_types';
    public $createTask = 'create.bonus_types';
    public $updateTask = 'edit.bonus_types';
    public $deleteTask = 'delete.bonus_types';
}
