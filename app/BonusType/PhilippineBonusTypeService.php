<?php

namespace App\BonusType;

class PhilippineBonusTypeService
{
    const FREQUENCY_ONE_TIME = 'ONE_TIME';
    const FREQUENCY_PERIODIC = 'PERIODIC';

    const FREQUENCIES = [
        self::FREQUENCY_ONE_TIME,
        self::FREQUENCY_PERIODIC,
    ];
}
