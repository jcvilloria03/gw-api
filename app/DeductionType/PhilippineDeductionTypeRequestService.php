<?php

namespace App\DeductionType;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineDeductionTypeRequestService extends RequestService
{
    /**
     * Call endpoint to create company deduction types
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/deduction_type/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check deduction type name is available
     *
     * @param int $companyId Company ID
     * @param array $data deduction type information
     * @return \Illuminate\Http\JsonResponse Availability of deduction type name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$companyId}/other_income_type/deduction_type/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update deduction type.
     *
     * @param int $id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, $data)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/deduction_type/{$id}",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        ));
    }
}
