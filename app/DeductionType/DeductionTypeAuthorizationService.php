<?php

namespace App\DeductionType;

use App\Common\CommonAuthorizationService;

class DeductionTypeAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.deduction_types';
    public $createTask = 'create.deduction_types';
    public $updateTask = 'edit.deduction_types';
    public $deleteTask = 'delete.deduction_types';
}
