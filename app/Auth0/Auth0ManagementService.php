<?php

namespace App\Auth0;

use Auth0\SDK\API\Management;
use Illuminate\Support\Facades\App;
use App\Authentication\AuthenticationRequestService;

class Auth0ManagementService
{
    const INITIAL_PASSWORD_LENGTH = 20;

    const USER_QUERY_PER_PAGE = 100;

    const LUCENE_QUERY_MAX_LENGTH = 4096;

    const LUCENE_QUERY_RESERVED_LENGTH = 24;

    /**
     * Auth0Management API wrapper
     *
     * @var \Auth0\SDK\API\Management
     */
    protected $management;

    /**
     * Constructor
     *
     * @param \Auth0\SDK\API\Management $management
     */
    public function __construct(Management $management)
    {
        $this->management = $management;
    }

    protected function connect()
    {
        $authentication = new \Auth0\SDK\API\Authentication(getenv('AUTH0_DOMAIN'));
        $token = $authentication->client_credentials([
            'client_id' => getenv('MANAGEMENT_CLIENT_ID'),
            'client_secret' => getenv('MANAGEMENT_CLIENT_SECRET'),
            'audience' => getenv('JWT_TRUSTED_ISS') . 'api/v2/'
        ]);

        $this->management = new \Auth0\SDK\API\Management($token['access_token'], getenv('AUTH0_DOMAIN'));
    }

    /**
     * Create user in Auth0
     *
     * @param array $data
     * @param bool $sendVerificationEmail
     * @return mixed
     */
    public function createUser(array $data, $sendVerificationEmail = true)
    {
        $this->connect();
        $initialPassword = str_random(self::INITIAL_PASSWORD_LENGTH);

        $name = $data['first_name'] . $data['middle_name'] . $data['last_name'];

        $data = [
            'connection' => "Username-Password-Authentication",
            'email' => $data['email'],
            'name' => $name,
            'email_verified' => false,
            'password' => $initialPassword,
            'verify_email' => $sendVerificationEmail,
            'user_metadata' => [
                'account_name' => $data['account_name'],
                'fresh_account' => "true",
                'initial_password' => $initialPassword
            ]
        ];
        return $this->management->users->create($data);
    }

    /**
     * Create import users job
     *
     * @param array $users
     * @return array
     */
    public function createImportUsersJob(array $users)
    {
        $this->connect();

        // Get connection ID for Username-Password-Authentication connection
        $connectionId = $this->getConnectionIdByName('Username-Password-Authentication');

        // Prepare data for batch import
        $userData = array_map(function ($user) {
            $initialPassword = str_random(self::INITIAL_PASSWORD_LENGTH);

            // Replace 2y with 2a to meet the format requirement of Auth0
            $hashedPassword = str_replace('$2y$', '$2a$', password_hash($initialPassword, PASSWORD_BCRYPT));

            return [
                'email' => $user['email'],
                'name' => $user['first_name'] . $user['middle_name'] . $user['last_name'],
                'email_verified' => false,
                'password_hash' => $hashedPassword,
                'verify_email' => true,
                'user_metadata' => [
                    'account_name' => $user['account_name'],
                    'fresh_account' => "true",
                    'initial_password' => $initialPassword
                ]
            ];
        }, $users);

        // Store users json file in tmp to upload to auth0 for batch import
        $tempUsersFile = tempnam(sys_get_temp_dir(), 'users');

        file_put_contents($tempUsersFile, json_encode($userData));

        return $this->management->jobs->importUsers($tempUsersFile, $connectionId);
    }

    /**
     * Create verify email address job
     * @param string $auth0UserId
     * @return array
     */
    public function createEmailVerificationJob(string $auth0UserId)
    {
        $this->connect();

        return $this->management->jobs->sendVerificationEmail($auth0UserId);
    }

    /**
     * Get auth0 management job details
     *
     * @param string $jobId
     * @return array
     */
    public function getJobDetails(string $jobId)
    {
        $this->connect();
        return $this->management->jobs->get($jobId);
    }

    /**
     * Delete user from auth0
     *
     * @param string $auth0UserId
     * @return mixed
     */
    public function deleteUser(string $auth0UserId)
    {
        $this->connect();
        return $this->management->users->delete($auth0UserId);
    }

    /**
     * Update user in Auth0
     *
     * @param string $auth0UserId
     * @param array  $data
     * @return mixed
     */
    public function updateUser(string $auth0UserId, array $data)
    {
        $this->connect();
        return $this->management->users->update($auth0UserId, $data);
    }

    /**
     *
     * Get Auth0 user profile from Auth0
     *
     * @param string $auth0UserId
     * @return mixed
     */
    public function getAuth0UserProfile(string $auth0UserId)
    {
        $this->connect();
        $users = $this->management->users->search([
            'q'             => "user_id:\"{$auth0UserId}\"",
            'search_engine' => 'v3'
        ]);
        return !empty($users) ? current($users) : null;
    }

    /**
     *
     * Get Auth0 user profile from Auth0, using email
     *
     * @param string $email
     * @return mixed
     */
    public function getAuth0UserProfileByEmail(string $email)
    {
        $this->connect();
        $users = $this->management->users->search([
            'q'             => rawurlencode("email:\"{$email}\""),
            'search_engine' => 'v3'
        ]);
        return !empty($users) ? current($users) : null;
    }

    /**
     * Get multiple Auth0 user profile from Auth0, using array of emails
     *
     * @param array $emails
     * @return mixed
     */
    public function getMultipleAuth0UserProfileByEmails(array $emails)
    {
        $this->connect();

        $chunks = [];
        $chunkIndex = 0;
        $chunkSize = 0;

        $allUsers = [];

        /**
         * Auth0 allows up to self::LUCENE_QUERY_MAX_LENGTH of Lucene query string length for the "q"
         * parameter, and up to self::USER_QUERY_PER_PAGE count of results per page.
         *
         * This splits the email addresses into chunks to fit within the restrictions of Auth0.
         */
        foreach ($emails as $email) {
            // Wrap each email address in double-quotes
            $email = sprintf('"%s"', $email);

            // Compute expected length of all appended query string for this chunk
            $expectedLength = strlen(rawurlencode($chunks[$chunkIndex] ?? '')) +
                strlen(rawurlencode($email . ',')) + self::LUCENE_QUERY_RESERVED_LENGTH;

            // Move to the next chunk if the query reaches the LUCENE_QUERY_MAX_LENGTH and
            // email address count reaches the USER_QUERY_PER_PAGE
            if ($expectedLength > self::LUCENE_QUERY_MAX_LENGTH || $chunkSize >= self::USER_QUERY_PER_PAGE) {
                $chunkIndex++;
                $chunkSize = 0;
            }

            if (!isset($chunks[$chunkIndex])) {
                $chunks[$chunkIndex] = '';
            }

            $chunks[$chunkIndex] .= $email . ',';
            $chunkSize++;
        }

        // Finally we get user details per email chunk
        foreach ($chunks as $emails) {
            $emails = trim($emails, ',');

            $users = $this->management->users->search([
                'q' => rawurlencode("email:{$emails}"),
                'search_engine' => 'v3',
                'per_page' => 100
            ]);

            $allUsers = array_merge($allUsers, $users);
        }

        return collect($allUsers)->keyBy('email')->all();
    }

    /**
     * Get Auth0 connection_id by name
     *
     * @param string $name
     * @return string
     */
    public function getConnectionIdByName(string $name)
    {
        $this->connect();

        $connections = $this->management->connections->getAll(null, ['id', 'name']);

        foreach ($connections as $connection) {
            if ($connection['name'] == $name) {
                return $connection['id'];
            }
        }
    }

    /**
     * Send verification email to user using UA-API
     *
     * @param string $auth0UserId
     * @return mixed
     */
    public function verifyEmail(string $auth0UserId)
    {
        $authenticationRequestService = App::make(AuthenticationRequestService::class);

        $response =  $authenticationRequestService->verifyResend([
            'data' => [
                'user' => [
                    'auth0_user_id' => $auth0UserId,
                ]
            ]
        ]);

        return json_decode($response->getData(), true);
    }
}
