<?php

namespace App\Auth0;

use Carbon\Carbon;
use App\Model\Auth0User;
use Illuminate\Support\Str;
use App\Model\ResetPasswordToken;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Account\AccountRequestService;
use App\CompanyUser\CompanyUserService;
use App\Authentication\AuthenticationRequestService;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Auth0UserService
{
    const PREACTIVE_ID = 'preactive|';
    /**
     * @var \App\Auth0\Auth0ManagementService
     */
    protected $auth0ManagementService;

    /**
     * @var \App\Account\AccountRequestService
     */
    protected $accountRequestService;

    /**
     * @var \App\CompanyUser\CompanyUserService
     */
    protected $companyUserService;

    public function __construct(
        Auth0ManagementService $auth0ManagementService,
        AccountRequestService $accountRequestService,
        CompanyUserService $companyUserService
    ) {
        $this->auth0ManagementService = $auth0ManagementService;
        $this->accountRequestService  = $accountRequestService;
        $this->companyUserService     = $companyUserService;
    }

    /**
     *
     * Creates Auth0User for Employee
     *
     * @param \App\Model\Auth0User $auth0User
     * @param array                 $attributes
     * @return Auth0User object
     */
    public function createEmployeeUserInManagement(array $attributes, Auth0User $auth0User = null)
    {
        $auth0UserProfile = $this->getAuth0UserProfileByEmail($attributes['email']);

        // get account name
        $accountResponse = $this->accountRequestService->get($attributes['account_id']);
        $account = json_decode($accountResponse->getData());

        // Create a new auth0 user profile
        if (!$auth0UserProfile) {
            $auth0UserDetails = $this->auth0ManagementService->createUser([
                'email'        => $attributes['email'],
                'first_name'   => $attributes['first_name'],
                'middle_name'  => $attributes['middle_name'],
                'last_name'    => $attributes['last_name'],
                'account_name' => $account->name
            ]);
        } else {
            $auth0UserDetails = [
                'user_id' => $auth0UserProfile['user_id']
            ];
        }

        // Allow creation of new auth0user records
        // for backwards compatibility with existing data
        if (!$auth0User) {
            $auth0User = $this->create([
                'auth0_user_id' => $auth0UserDetails['user_id'],
                'user_id'       => $attributes['company_user_id'],
                'account_id'    => $attributes['account_id'],
                'status'        => $attributes['active']
                                    ? Auth0User::STATUS_ACTIVE
                                    : Auth0User::STATUS_INACTIVE
            ]);
        } else {
            // Update auth0user record with auth0 user id
            $auth0User = $this->update(
                $auth0User,
                [
                    'auth0_user_id' => $auth0UserDetails['user_id'],
                    'status'        => $attributes['active']
                                        ? Auth0User::STATUS_ACTIVE
                                        : Auth0User::STATUS_INACTIVE
                ]
            );
        }

        return $auth0User;
    }

    /**
     * Batch Create Auth0User for Employees
     *
     * @param int $accountId
     * @param array $data
     * @return Auth0User[] array
     */
    public function batchCreateEmployeeUserInManagement(int $accountId, array $data)
    {
        Log::info('Starting batchCreateEmployeeUserInManagement', [
            'account_id' => $accountId,
            'data_count' => count($data)
        ]);

        // get account name
        $accountResponse = $this->accountRequestService->get($accountId);
        $account = json_decode($accountResponse->getData());

        $data = array_map(function ($entry) {
            // Transform all emails to lowercase
            $entry['attributes']['email'] = strtolower($entry['attributes']['email']);

            return $entry;
        }, $data);

        $emails = array_map(function ($entry) {
            return $entry['attributes']['email'];
        }, $data);

        $auth0UserProfiles = $this->auth0ManagementService->getMultipleAuth0UserProfileByEmails($emails);
        $pendingAuth0Users = [];

        foreach ($data as $entry) {
            list($attributes, $auth0User) = array_values($entry);

            $pendingUser = [
                'auth0_user' => $auth0User,
                'auth0_user_id' => null,
                'attributes' => $attributes,
                'user_data' => [
                    'email'        => $attributes['email'],
                    'first_name'   => $attributes['first_name'],
                    'middle_name'  => $attributes['middle_name'],
                    'last_name'    => $attributes['last_name'],
                    'account_name' => $account->name
                ]
            ];

            $auth0UserProfile = array_get($auth0UserProfiles, $attributes['email']);

            if ($auth0UserProfile) {
                $pendingUser['auth0_user_id'] = $auth0UserProfile['user_id'];
            }

            $pendingAuth0Users[] = $pendingUser;
        }

        $newAuth0UserProfiles = [];

        // Get pending user entries with create mode
        $pendingAuth0UsersCreate = array_filter($pendingAuth0Users, function ($entry) {
            // Filter pending users without auth0_user_id
            return !$entry['auth0_user_id'];
        });

        if (!empty($pendingAuth0UsersCreate)) {
            // Process auth0 batch user creation
            $pendingCreateUserData = array_column($pendingAuth0UsersCreate, 'user_data');

            $importJob = $this->auth0ManagementService->createImportUsersJob($pendingCreateUserData);
            Log::info('Created Auth0 user import job', $importJob);

            $jobDetails = ['status' => 'pending'];

            do {
                // Wait until the job is completed
                $jobDetails = $this->auth0ManagementService->getJobDetails($importJob['id']);
                sleep(1);
            } while ($jobDetails['status'] == 'pending');

            Log::info('Auth0 user import job status', $jobDetails);

            // Process newly created auth0 users
            $newEmails = array_column($pendingCreateUserData, 'email');
            $newAuth0UserProfiles = $this->auth0ManagementService->getMultipleAuth0UserProfileByEmails($newEmails);
        }

        foreach ($pendingAuth0Users as $pendingUser) {
            // Set auth0_user_id of pending user from the list of newly created auth0 users
            if (!$pendingUser['auth0_user_id']) {
                $newProfile = array_get($newAuth0UserProfiles, $pendingUser['attributes']['email']);

                if (!$newProfile) {
                    Log::info('Cannot get Auth0 user profile for ' . $pendingUser['attributes']['email']);
                    continue;
                }

                $pendingUser['auth0_user_id'] = $newProfile['user_id'];
            }

            if ($pendingUser['auth0_user']) {
                $this->update($pendingUser['auth0_user'], [
                    'auth0_user_id' => $pendingUser['auth0_user_id'],
                    'status'        => $pendingUser['attributes']['active']
                                            ? Auth0User::STATUS_ACTIVE
                                            : Auth0User::STATUS_INACTIVE
                ]);

                continue;
            }

            $this->create([
                'auth0_user_id' => $pendingUser['auth0_user_id'],
                'user_id'       => $pendingUser['attributes']['company_user_id'],
                'account_id'    => $pendingUser['attributes']['account_id'],
                'status'        => $pendingUser['attributes']['active']
                                        ? Auth0User::STATUS_ACTIVE
                                        : Auth0User::STATUS_INACTIVE
            ]);
        }

        $auth0ExistingUserIds = array_column($auth0UserProfiles, 'user_id');
        $auth0NewUserIds = array_column($newAuth0UserProfiles, 'user_id');

        Log::info('Completed batchCreateEmployeeUserInManagement', [
            'account_id' => $accountId,
            'data_count' => count($data)
        ]);

        return array_merge($auth0ExistingUserIds, $auth0NewUserIds);
    }

    /**
     *
     * Update Auth0User for Employee
     *
     * @param Auth0User $auth0User
     * @param array $attributes
     */
    public function updateEmployeeUser(Auth0User $auth0User, array $attributes)
    {
        // update Auth0User in Auth0 database
        $this->auth0ManagementService->updateUser($auth0User->auth0_user_id, [
            'email' => $attributes['email'],
        ]);
    }

    /**
     *
     * Get Auth0 user profile from Auth0
     *
     * @param string $auth0UserId
     * @return mixed
     */
    public function getAuth0UserProfile(string $auth0UserId)
    {
        return $this->auth0ManagementService->getAuth0UserProfile($auth0UserId);
    }

    /**
     *
     * Get Auth0 user profile from Auth0, using email
     *
     * @param string $email
     * @return array
     */
    public function getAuth0UserProfileByEmail(string $email)
    {
        return $this->auth0ManagementService->getAuth0UserProfileByEmail($email);
    }

    /**
     *
     * Create Auth0User link in Gateway DB
     *
     * @param array $attributes
     * @return Auth0User object
     */
    public function create(array $attributes)
    {
        $auth0User = Auth0User::create($attributes);

        return $auth0User;
    }

    /**
     *
     * Fetch Auth0User for a given User ID
     *
     * @param string $userId User ID
     * @return Auth0User object
     */
    public function getUser(string $userId)
    {
        return Auth0User::where('user_id', $userId)->first();
    }

    /**
     *
     * Fetch Auth0Users for a given User IDS
     *
     * @param array $ids User IDS
     * @return Auth0User object
     */
    public function getUsersByIds(array $ids)
    {
        return Auth0User::whereIn('user_id', $ids)->get();
    }

    /**
     *
     * Fetch Auth0User for a given Auth0 User ID
     *
     * @param string $auth0UserId Auth0 User ID
     * @return Auth0User object
     */
    public function getUserByAuth0UserId(string $auth0UserId)
    {
        return Auth0User::where('auth0_user_id', $auth0UserId)->get()->first();
    }

    /**
     *
     * Fetch Auth0User for a given Employee ID
     *
     * @param string $employeeId Employee ID
     * @return Auth0User|null object
     */
    public function getUserByEmployeeId(string $employeeId)
    {
        $employee = $this->companyUserService->getByEmployeeId($employeeId);

        if ($employee) {
            return Auth0User::where('user_id', $employee->user_id)->first();
        }
    }

    /**
     *
     * Delete Auth0 User for a given User ID
     *
     * @param int $userId User ID
     */
    public function deleteUser(int $userId)
    {
        if (Auth0User::where('user_id', $userId)->delete()) {
            $auth0User = new Auth0User;
            $auth0User->user_id = $userId;
        }
    }

    /**
     *
     * Get Auth0 users profiles from Auth0 for given user ids
     *
     * @param array $userIds
     * @return array
     */
    public function getAuth0UsersProfilesForUserIds(array $userIds)
    {
        return $this->getByUserIds($userIds)->map(function ($auth0User) {
            return $this->getAuth0UserProfile($auth0User->auth0_user_id);
        })->all();
    }

    /**
     * Change password of ess (employee) user.
     *
     * @param int $employeeId
     * @param string $newPassword
     * @return void
     */
    public function changeEmployeePassword($employeeId, $newPassword)
    {
        $auth0user = $this->getUserByEmployeeId($employeeId);

        return $this->auth0ManagementService->updateUser($auth0user->auth0_user_id, [
            'password' => $newPassword,
        ]);
    }

    /**
     * Get Auth0 User id.
     *
     * @param int $userId
     * @return string|null
     */
    public function getAuth0UserId(int $userId)
    {
        $authUser = $this->getUser($userId);

        return $authUser ? $authUser->auth0_user_id : null;
    }

    /**
     * Get auth0 users with status for account id.
     *
     * @param int $accountId
     * @return array
     */
    public function getUsersWithStatuses(int $accountId)
    {
        $accountUsers = Auth0User::where('account_id', $accountId)->get();
        $usersWithStatuses = [];

        foreach ($accountUsers as $user) {
            $usersWithStatuses[$user->user_id] = $user->status;
        }

        return $usersWithStatuses;
    }

    /**
     * Get auth0 Active users for account id.
     *
     * @param int $accountId
     * @return array
     */
    public function getActiveUserIds(int $accountId)
    {
        return Auth0User::where('account_id', $accountId)
            ->where('status', 'like', Auth0User::STATUS_ACTIVE)
            ->get()
            ->pluck('user_id')
            ->toArray();
    }

    /**
     * Get auth0 Inactive users for account id.
     *
     * @param int $accountId
     * @return array
     */
    public function getInctiveUserIds(int $accountId)
    {
        return Auth0User::where('account_id', $accountId)
            ->where('status', 'like', Auth0User::STATUS_INACTIVE)
            ->get()
            ->pluck('user_id')
            ->toArray();
    }

    /**
     * Update Auth0User's status
     *
     * @param int $userId
     * @param string $status
     *
     * @return mixed
     */
    public function setStatus(int $userId, string $status)
    {
        $user = $this->getUser($userId);

        if ($user) {
            $user = $this->update($user, ['status' => $status]);
        }

        return $user;
    }

    /**
     * Get Auth0 users by array of IDs
     *
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByUserIds(array $ids)
    {
        return Auth0User::whereIn('id', $ids)->get();
    }

    /**
     *
     * Create Auth0User link in Gateway DB
     *
     * @param \App\Model\Auth0User $model
     * @param array                $attributes
     * @return Auth0User object
     */
    public function update(Auth0User $model, array $attributes)
    {
        if (!$model->update($attributes)) {
            throw new \Exception('Unable to update Auth0 user.', 500);
        }

        return $model;
    }

    /**
     * Generate preactive id for newly created inactive users
     *
     * @param int $accountId
     * @param int $companyUserId
     * @return string
     */
    public static function generatePreactiveId($accountId, $companyUserId)
    {
        return static::PREACTIVE_ID . "{$accountId}-{$companyUserId}";
    }

    public static function isPreactive(Auth0User $auth0User)
    {
        return stripos($auth0User->auth0_user_id, static::PREACTIVE_ID) === 0;
    }

    /**
     * GeneraCreatete a reset password token record for the given user
     *
     * @param int $userId
     * @return \App\Model\ResetPasswordToken
     */
    public function generateResetPasswordToken($userId) : ResetPasswordToken
    {
        return ResetPasswordToken::create([
            'user_id'    => $userId,
            'token'      => Str::random(64),
            'expires_at' => Carbon::now()->addHour()->format('Y-m-d H:i:s')
        ]);
    }

    /**
     * Get reset password token object by token
     *
     * @param string $token
     * @return \App\Model\ResetPasswordToken
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getResetPasswordTokenObjByToken($token) : ResetPasswordToken
    {
        return ResetPasswordToken::where('token', $token)
            ->firstOrFail();
    }

    /**
     * Delete all reset password tokens belonging to given user id
     *
     * @param int $userId
     * @return \Illuminate\Support\Collection
     */
    public function deleteExistingResetPasswordTokens($userId)
    {
        return ResetPasswordToken::where('user_id', $userId)
            ->delete();
    }

    /**
     * Sends change password request to UA-API
     *
     * @param \App\Model\Auth0User $user
     * @param string $newPassword
     * @param array $extra
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function updateUserPassword(Auth0User $user, $newPassword, array $extra = [])
    {
        $service = App::make(AuthenticationRequestService::class);
        return $service->changePassword($user->auth0_user_id, $newPassword, $extra);
    }

    /**
     *
     * Update Auth0User for Employee
     *
     * @param Auth0User $auth0User
     * @param array $attributes
     */
    public function updateEmployeeAuth0UserId(Auth0User $auth0User, array $attributes)
    {
        // update Auth0User in Auth0 database
        $auth0User = $this->update(
            $auth0User,
            [
                'auth0_user_id' => $attributes['auth0_user_id']
            ]
        );
    }
}
