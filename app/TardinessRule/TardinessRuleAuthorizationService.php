<?php

namespace App\TardinessRule;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class TardinessRuleAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.tardiness_rule';
    const CREATE_TASK = 'create.tardiness_rule';
    const UPDATE_TASK = 'edit.tardiness_rule';
    const DELETE_TASK = 'delete.tardiness_rule';

    /**
     * @param \stdClass $tardinessRule
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $tardinessRule
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyTardinessRules(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $tardinessRule
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $tardinessRule
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $tardinessRule
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $leaveEntitlement
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $tardinessRule, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($tardinessRule, $user, self::CREATE_TASK);
    }

    /**
     * Authorize team related tasks
     *
     * @param \stdClass $tardinessRule
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $tardinessRule,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($tardinessRule->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $tardinessRule->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($tardinessRule->company_id);
        }

        return false;
    }
}
