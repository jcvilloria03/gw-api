<?php

namespace App\TardinessRule;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TardinessRuleRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get tardines rule info
     *
     * @param int $id Tardiness Rule ID
     * @return \Illuminate\Http\JsonResponse TardinessRule Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/tardiness_rule/{$id}"
        );

        return $this->send($request);
    }


    /**
     * Call endpoint to get all tardiness rules within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company tardines rules
     */
    public function getCompanyTardinessRules(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/tardiness_rules"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create Tardiness rule
     *
     * @param array $data Tardiness rule information
     * @return \Illuminate\Http\JsonResponse Created TardinessRule
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/tardiness_rule",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple tardiness rules
     *
     * @param array $data information on tardiness rules to delete
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/tardiness_rule/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update tardiness rules
     *
     * @param array $data tardiness rule information
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, array $data)
    {
        $request = new Request(
            'PUT',
            "/tardiness_rule/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

     /**
     * Call endpoint to check are tadiness rules in use
     *
     * @param array $data tardiness rules information
     * @return \Illuminate\Http\JsonResponse Usement of tardiness rules
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            "/tardiness_rule/check_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if tardiness rule name is available
     *
     * @param int $companyId Company ID
     * @param array $data tardiness rule information
     * @return \Illuminate\Http\JsonResponse Availability of tardiness rule name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/tardiness_rule/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }
}
