<?php

namespace App\Payroll;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PayrollFinalPayRequestService extends RequestService
{
    /**
     * Create final payroll
     *
     * @param $data
     * @param AuthzDataScope|null $authzDataScope
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFinalPayroll($data, $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            '/payroll/final',
            ['Content-Type' => 'application/json'],
            $data
        );

        return $this->send($request, $authzDataScope);
    }
}
