<?php

namespace App\Payroll;
//For deprecation
class PayrollAllowanceUploadTask extends PayrollUploadTask
{
    const TYPE = 'allowance';

    const ID_PREFIX = 'payroll_allowance_upload:';
}
