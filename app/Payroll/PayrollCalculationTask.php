<?php

namespace App\Payroll;

use App\Tasks\Task;

class PayrollCalculationTask extends Task
{
    const ID_PREFIX = 'payroll_calculation:';

    const STATUS_CALCULATION_QUEUED = 'calculation_queued';
    const STATUS_CALCULATING = 'calculating';
    const STATUS_CALCULATED = 'calculated';
    const STATUS_CALCULATION_FAILED = 'calculation_failed';

    const CALCULATION_STATUSES = [
        self::STATUS_CALCULATION_QUEUED,
        self::STATUS_CALCULATING,
        self::STATUS_CALCULATED,
        self::STATUS_CALCULATION_FAILED
    ];

    /**
     * Create task in Redis
     *
     * @var int $payrollId Payroll ID
     * @var string $id Job id
     *
     */
    public function create(int $payrollId, string $id = null)
    {
        $this->payrollId = $payrollId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();
            $this->currentValues = [
                'calculation_status' => self::STATUS_CALCULATION_QUEUED
            ];
            $this->createInRedis();
        } else {
            $this->id = $id;
            if (!$this->isPayrollIdSame()) {
                // Job::Payroll ID does not match $this->payrollId
                throw new PayrollTaskException('Job does not belong to given Payroll ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new PayrollTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    public function generateId()
    {
        return static::ID_PREFIX . $this->payrollId . ':' . uniqid();
    }

    /**
     * Check if payrollId and payrollId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     *
     */
    public function isPayrollIdSame()
    {
        // check job.payroll_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->payrollId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Update calculation status
     *
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateCalculationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::CALCULATION_STATUSES)) {
            throw new PayrollTaskException('Invalid Calculation Status');
        }

        $currentStatus = $this->currentValues['calculation_status'] ?? null;

        $changeStatus = $this->canChangeCalculationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal('calculation_status', $newStatus);
        }
    }

    /**
     * Check if Calculation Status can be updated
     *
     * @param string $newStatus
     * @param string $currentStatus
     * @return boolean
     *
     */
    protected function canChangeCalculationStatus(string $currentStatus = null, string $newStatus)
    {
        $changeStatus = false;
        switch ($newStatus) {
            case self::STATUS_CALCULATION_QUEUED:
                $changeStatus = $this->canUpdateStatusToQueued($currentStatus);
                break;
            case self::STATUS_CALCULATING:
                $changeStatus = $this->canUpdateStatusToValidating($currentStatus);
                break;
            case self::STATUS_CALCULATED:
            case self::STATUS_CALCULATION_FAILED:
                $changeStatus = $this->canUpdateStatusToFinished($currentStatus);
                break;
        }
        return $changeStatus;
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return ($currentStatus !== self::STATUS_CALCULATING);
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return ($currentStatus === self::STATUS_CALCULATION_QUEUED);
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return ($currentStatus === self::STATUS_CALCULATING);
    }
}
