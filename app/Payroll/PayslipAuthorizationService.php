<?php

namespace App\Payroll;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class PayslipAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.payslip';
    const GENERATE_TASK = 'generate.payslip';

    /**
     * Authorize view, create, delete Payroll related tasks
     *
     * @param \stdClass $payroll Payroll where payslip is attached
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $payroll,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payroll's account
            return $accountScope->inScope($payroll->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payrolls's account
                return $payroll->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($payroll->company_id);
        }

        // verify payroll group scope
        $payrollGroupScope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        // check if user has payroll group level permissions for payroll group
        return $payrollGroupScope && $payrollGroupScope->inScope($payroll->payroll_group_id);
    }

    /**
     * @param \stdClass $payroll Payslip's payroll
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $payroll Payslip's payroll
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::GENERATE_TASK);
    }
}
