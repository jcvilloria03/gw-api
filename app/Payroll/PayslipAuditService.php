<?php

namespace App\Payroll;

use App\Audit\AuditService;
use App\Audit\AuditItem;

class PayslipAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_VIEW = 'view';

    const OBJECT_NAME = 'payslip';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        AuditService $auditService
    ) {
        $this->auditService = $auditService;
    }

    /**
     *
     * Log Payroll related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_VIEW:
                $this->logView($cacheItem);
                break;
        }
    }

    /**
     *
     * Log Payroll Payslip Generation
     *
     * @param array $cacheItem
     *
     */
    public function logCreate(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log Employee Payslip View
     * @param  array  $cacheItem
     * @return void
     */
    public function logView(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $data = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $data['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_VIEW,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
            ],
        ]);

        $this->auditService->log($item);
    }
}
