<?php

namespace App\Payroll;

use App\Tasks\UploadTask;

class PayrollUploadTask extends UploadTask
{
    const PROCESS_VALIDATION = 'validation';
    const PROCESS_SAVE = 'save';

    //overwrite this in child class
    const TYPE = 'upload';

    //overwrite this in child class
    const ID_PREFIX = 'payroll_upload:';

    const VALID_PROCESSES = [
        self::PROCESS_VALIDATION,
        self::PROCESS_SAVE,
    ];

    /**
     * Create task in Redis
     *
     * @var int $payrollId Payroll ID
     * @var string $id Job id
     *
     */
    public function create(int $payrollId, string $id = null)
    {
        $this->payrollId = $payrollId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();
            $this->currentValues = [
                's3_bucket' => $this->s3Bucket
            ];
            $this->createInRedis();
        } else {
            $this->id = $id;
            if (!$this->isPayrollIdSame()) {
                // Job::Payroll ID does not match $this->payrollId
                throw new PayrollTaskException('Job does not belong to given Payroll ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new PayrollTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    public function generateId()
    {
        return static::ID_PREFIX . $this->payrollId . ':' . uniqid();
    }

    /**
     * Check if payrollId and payrollId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     *
     */
    public function isPayrollIdSame()
    {
        // check job.payroll_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->payrollId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Save Personal Info
     *
     * @param string $path Full path of file to upload
     *
     */
    public function saveFile(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key();
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal(self::PROCESS_VALIDATION . '_s3_key', $s3Key);
        $this->updateValidationStatus(self::STATUS_VALIDATION_QUEUED);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     */
    protected function generateS3Key()
    {
        return self::ID_PREFIX .
               $this->payrollId .
               ':' .
               uniqid();
    }

    /**
     * Update validation status
     *
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateValidationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new PayrollTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[self::PROCESS_VALIDATION . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_VALIDATION . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     *
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new PayrollTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_VALIDATION is already validated
        if (
            !isset($this->currentValues[self::PROCESS_VALIDATION . '_status']) ||
            $this->currentValues[self::PROCESS_VALIDATION . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new PayrollTaskException(
                'Payroll information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Update Error File Location
     *
     * @param string $errorFileS3Key The S3 key of the error file
     *
     */
    public function updateErrorFileLocation(string $errorFileS3Key)
    {
        $this->setVal(self::PROCESS_VALIDATION . '_error_file_s3_key', $errorFileS3Key);
    }
}
