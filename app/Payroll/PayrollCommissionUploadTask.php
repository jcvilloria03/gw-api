<?php

namespace App\Payroll;

class PayrollCommissionUploadTask extends PayrollUploadTask
{
    const TYPE = 'commission';

    const ID_PREFIX = 'payroll_commission_upload:';
}
