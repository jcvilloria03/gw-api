<?php

namespace App\Payroll;

use App\Payroll\PayrollRequestService;
use App\Audit\AuditService;
use App\Audit\AuditItem;

class PayrollAuditService
{
    const ACTION_CALCULATE = 'calculate';
    const ACTION_RECALCULATE = 'recalculate';
    const ACTION_CREATE = 'create';
    const ACTION_CLOSE = 'close';
    const ACTION_OPEN = 'open';
    const ACTION_DELETE = 'delete';
    const ACTION_EDIT = 'edit';
    const ACTION_UPLOAD = 'upload';
    const ACTION_CLEAR_UPLOAD = 'clear_upload';

    const OBJECT_NAME = 'payroll';

    const CHANGEABLE_SUMMARY = [
        'gross',
        'net',
        'total_deductions',
        'total_contributions',
    ];

    const CHANGEABLE_EMPLOYEE = [
        'taxable_income',
        'gross',
        'withholding_tax',
        'total_deduction',
        'net_pay',
        'sss_employee',
        'hdmf_employee',
        'philhealth_employee',
        'total_mandatory_employee',
        'sss_employer',
        'sss_ec_employer',
        'hdmf_employer',
        'philhealth_employer',
        'total_mandatory_employer',
    ];

    /*
     * App\Payroll\PayrollRequestService
     */
    protected $payrollRequestService;

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        PayrollRequestService $payrollRequestService,
        AuditService $auditService
    ) {
        $this->payrollRequestService = $payrollRequestService;
        $this->auditService = $auditService;
    }

    /**
     *
     * Log Payroll related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CALCULATE:
                $this->logCalculate($cacheItem);
                break;
            case self::ACTION_CLOSE:
                $this->logClose($cacheItem);
                break;
            case self::ACTION_OPEN:
                $this->logClose($cacheItem);
                break;
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
            case self::ACTION_EDIT:
                $this->logEdit($cacheItem);
                break;
            case self::ACTION_UPLOAD:
                $this->logUpload($cacheItem);
                break;
        }
    }

    /**
     *
     * Log Payroll Calculation
     *
     * @param array $cacheItem
     *
     */
    public function logCalculate(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CALCULATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Payroll Close
     *
     * @param array $cacheItem
     *
     */
    public function logClose(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CLOSE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date,
                'old_status' => $payrollData->status,
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Payroll Open
     *
     * @param array $cacheItem
     *
     */
    public function logOpen(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_OPEN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date,
                'old_status' => $payrollData->status,
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Payroll Creation
     *
     * @param array $cacheItem
     *
     */
    public function logCreate(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['new']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date,
                'status' => $payrollData->status,
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Payroll Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date,
                'old_status' => $payrollData->status,
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     *
     * Log Payroll Edit
     *
     * @param array $cacheItem
     *
     */
    public function logEdit(array $cacheItem)
    {
        $payrollDataOld = json_decode($cacheItem['old'], true);
        $payrollDataNew = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        // only use payroll summary fields in the edit request
        $summaryNew = array_intersect_key($payrollDataNew, array_flip(self::CHANGEABLE_SUMMARY));

        // disregard columns that are not in summary new
        $summaryOld = array_intersect_key($payrollDataOld, $summaryNew);

        // get old and new values only for changed keys
        $oldChanges = array_diff_assoc($summaryOld, $summaryNew);
        $newChanges = array_diff_assoc($summaryNew, $summaryOld);
        if (!empty($oldChanges)) {
            $item = new AuditItem([
                'company_id' => $payrollDataOld['company_id'],
                'account_id' => $user['account_id'],
                'user_id' => $user['id'],
                'action' => self::ACTION_EDIT,
                'object_name' => self::OBJECT_NAME,
                'data' => [
                    'payroll_id' => $payrollDataOld['id'],
                    'old' => $oldChanges,
                    'new' => $newChanges
                ]
            ]);
            $this->auditService->log($item);
        }

        // check if employee was edited
        if (!empty($payrollDataNew['employees'])) {
            // loop across each changed employee
            foreach ($payrollDataNew['employees'] as $newEmpData) {
                $oldEmpData = $this->fetchOldEmployeePayrollData(
                    $newEmpData['id'],
                    $payrollDataOld['employees']
                );

                // only use payroll employee fields in the edit request
                $empPayrollNew = array_intersect_key($newEmpData, array_flip(self::CHANGEABLE_EMPLOYEE));

                // disregard columns that are not in new employee payroll data
                $empPayrollOld = array_intersect_key($oldEmpData, $empPayrollNew);

                // get old and new values only for changed keys
                $oldChanges = array_diff_assoc($empPayrollOld, $empPayrollNew);
                $newChanges = array_diff_assoc($empPayrollNew, $empPayrollOld);
                if (!empty($oldChanges)) {
                    $item = new AuditItem([
                        'company_id' => $payrollDataOld['company_id'],
                        'account_id' => $user['account_id'],
                        'user_id' => $user['id'],
                        'action' => self::ACTION_EDIT,
                        'object_name' => self::OBJECT_NAME,
                        'data' => [
                            'payroll_employee_id' => $newEmpData['id'],
                            'payroll_id' => $payrollDataOld['id'],
                            'old' => $oldChanges,
                            'new' => $newChanges
                        ]
                    ]);
                    $this->auditService->log($item);
                }
            }
        }
    }

    protected function fetchOldEmployeePayrollData(int $id, array $oldData)
    {
        $result = [];
        foreach ($oldData as $oldEmployee) {
            if ($oldEmployee['id'] === $id) {
                $result = $oldEmployee;
            }
        }
        return $result;
    }

    /**
     *
     * Log Payroll Data Upload (Attendance, Allowance, etc.)
     *
     * @param array $cacheItem
     *
     */
    public function logUpload(array $cacheItem)
    {
        $payrollData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $payrollData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPLOAD,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'payroll_id' => $payrollData->id,
                'payroll_group_id' => $payrollData->payroll_group_id,
                'start_date' => $payrollData->start_date,
                'end_date' => $payrollData->end_date,
                'payroll_date' => $payrollData->payroll_date,
                'job_id' => $payrollData->job_id,
                'type' => $payrollData->type,
            ]
        ]);
        $this->auditService->log($item);
    }
}
