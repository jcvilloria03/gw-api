<?php

namespace App\Payroll;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;
use App\Permission\TaskScopes;

class PayrollAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.payroll';
    const CREATE_TASK = 'create.payroll';
    const EDIT_TASK = 'edit.payroll';
    const CLOSE_TASK = 'close.payroll';
    const OPEN_TASK = 'open.payroll';
    const DELETE_TASK = 'delete.payroll';
    const RUN_TASK = 'run.payroll';
    const SEND_PAYSLIP_TASK = 'send_payslip.payroll';

    /**
     * Authorize view, update, delete Payroll related tasks
     *
     * @param \stdClass $payroll
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $payroll,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);

        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payroll's account
            return $accountScope->inScope($payroll->account_id);
        }

        // verify company scope OR payroll group scope
        return $this->verifyCompanyScope($taskScopes, $payroll, $user) ||
               $this->verifyPayrollGroupScope($taskScopes, $payroll);
    }

    /**
     * Authorize Company Scope
     *
     * @param TaskScopes $taskScopes
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    protected function verifyCompanyScope(
        TaskScopes $taskScopes,
        \stdClass $payroll,
        array $user
    ) {
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payroll's account
                return $payroll->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($payroll->company_id);
        }
        return false;
    }


    /**
     * Authorize PayrolL Group Scope
     *
     * @param TaskScopes $taskScopes
     * @param \stdClass $payroll
     * @return bool
     */
    protected function verifyPayrollGroupScope(
        TaskScopes $taskScopes,
        \stdClass $payroll
    ) {
        $payrollGroupScope = $taskScopes->getScopeBasedOnType(TargetType::PAYROLL_GROUP);
        // check if user has payroll group level permissions for payroll
        return $payrollGroupScope && $payrollGroupScope->inScope($payroll->id);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeEdit(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::EDIT_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeCalculate(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::RUN_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeUpload(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeClose(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::CLOSE_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeOpen(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::OPEN_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $payroll
     * @param array $user
     * @return bool
     */
    public function authorizeSendPayslips(\stdClass $payroll, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($payroll, $user, self::SEND_PAYSLIP_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeViewCompanyPayrolls(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);

        $taskScopes = $this->getTaskScopes(self::VIEW_TASK);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for payroll's account
            return $accountScope->inScope($company->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as payrolls's account
                return $company->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($company->id);
        }
        return false;
    }
}
