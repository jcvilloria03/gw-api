<?php

namespace App\Payroll;

class PayrollDeductionUploadTask extends PayrollUploadTask
{
    const TYPE = 'deduction';

    const ID_PREFIX = 'payroll_deduction_upload:';
}
