<?php

namespace App\Payroll;

use App\Tasks\Task;

class PayslipGenerationTask extends Task
{
    const ID_PREFIX = 'payslip_generation:';

    const STATUS_GENERATION_QUEUED = 'generation_queued';
    const STATUS_GENERATING = 'generating';
    const STATUS_GENERATED = 'generated';
    const STATUS_GENERATION_FAILED = 'generation_failed';

    const GENERATION_STATUSES = [
        self::STATUS_GENERATION_QUEUED,
        self::STATUS_GENERATING,
        self::STATUS_GENERATED,
        self::STATUS_GENERATION_FAILED
    ];

    /**
     * Create task in Redis
     *
     * @var int $payrollId Payroll ID
     * @var string $id Job id
     *
     */
    public function create(int $payrollId, string $id = null)
    {
        $this->payrollId = $payrollId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();

            // check if an existing payroll payslip generation request is still running
            $this->currentValues = $this->fetch();
            if (
                isset($this->currentValues['generation_status']) &&
                in_array($this->currentValues['generation_status'], [
                    self::STATUS_GENERATION_QUEUED,
                    self::STATUS_GENERATING
                ])
            ) {
                throw new PayrollTaskException('Payslips are still being generated for this payroll.');
            }

            $this->currentValues = [
                'generation_status' => self::STATUS_GENERATION_QUEUED
            ];
            $this->createInRedis();
        } else {
            $this->id = $id;
            if (!$this->isPayrollIdSame()) {
                // Job::Payroll ID does not match $this->payrollId
                throw new PayrollTaskException('Job does not belong to given Payroll ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new PayrollTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    public function generateId()
    {
        return static::ID_PREFIX . $this->payrollId;
    }

    /**
     * Check if payrollId and payrollId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     *
     */
    public function isPayrollIdSame()
    {
        // check job.payroll_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->payrollId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Update generation status
     *
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateGenerationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::GENERATION_STATUSES)) {
            throw new PayrollTaskException('Invalid Generation Status');
        }

        $currentStatus = $this->currentValues['generation_status'] ?? null;

        $changeStatus = $this->canChangeGenerationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal('generation_status', $newStatus);
        }
    }

    /**
     * Check if Generation Status can be updated
     *
     * @param string $newStatus
     * @param string $currentStatus
     * @return boolean
     *
     */
    protected function canChangeGenerationStatus(string $currentStatus = null, string $newStatus)
    {
        $changeStatus = false;
        switch ($newStatus) {
            case self::STATUS_GENERATION_QUEUED:
                $changeStatus = $this->canUpdateStatusToQueued($currentStatus);
                break;
            case self::STATUS_GENERATING:
                $changeStatus = $this->canUpdateStatusToValidating($currentStatus);
                break;
            case self::STATUS_GENERATED:
            case self::STATUS_GENERATION_FAILED:
                $changeStatus = $this->canUpdateStatusToFinished($currentStatus);
                break;
        }
        return $changeStatus;
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return ($currentStatus !== self::STATUS_GENERATING);
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return ($currentStatus === self::STATUS_GENERATION_QUEUED);
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return ($currentStatus === self::STATUS_GENERATING);
    }


    /**
     * Check if Payslips can be generated for a given Payroll
     *
     * @param int $payrollId Payroll Id
     *
     */
    public function canGenerate(int $payrollId)
    {
        //check job exists (will throw exception if job doesn't exist)
        try {
            $jobId = static::ID_PREFIX . $payrollId;
            $this->create($payrollId, $jobId);
        } catch (PayrollTaskException $e) {
            // job does not exist
            return true;
        }

        // check if existing job is done generating
        $currentStatus = $this->currentValues['generation_status'];
        if (
            in_array(
                $currentStatus,
                [
                    static::STATUS_GENERATION_FAILED,
                    static::STATUS_GENERATED
                ]
            )
        ) {
            // payslip is waiting to be generated or is still generating
            return true;
        }
        return false;
    }
}
