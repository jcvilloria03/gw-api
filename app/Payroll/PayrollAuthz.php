<?php

namespace App\Payroll;

use App\Authz\AuthzDataScope;

class PayrollAuthz
{
    /**
     * @var PayrollRequestService
     */
    private $payrollRequestService;

    public function __construct(PayrollRequestService $payrollRequestService)
    {
        $this->payrollRequestService = $payrollRequestService;
    }

    public function isAuthorized(int $payrollId, AuthzDataScope $authzDataScope): bool
    {
        $response = $this->payrollRequestService->get($payrollId);
        $payroll = json_decode($response->getData());

        return $authzDataScope->isAllAuthorized([
            AuthzDataScope::SCOPE_PAYROLL_GROUP => $payroll->payroll_group_id,
            AuthzDataScope::SCOPE_COMPANY => $payroll->company_id,
        ]);
    }

    /**
     * @param int[] $payrollIds
     * @param AuthzDataScope $authzDataScope
     * @return bool
     */
    public function isAllAuthorized(array $payrollIds, AuthzDataScope $authzDataScope): bool
    {
        $authorized = true;
        foreach ($payrollIds as $id) {
            if (!$this->isAuthorized((int) $id, $authzDataScope)) {
                $authorized = false;
                break;
            }
        }

        return $authorized;
    }

}
