<?php

namespace App\Payroll;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class PayrollRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Fetch payroll for given id
     *
     * @param int $id Payroll ID
     * @param boolean $withEmployees should include payroll employees
     * @param int $page paginate the result with the current page
     * @return json Payroll information
     *
     */
    public function get(int $id, bool $withEmployees = false, $page = null, $searchTerm = null, $perPage = 10)
    {
        $queryString = '?with_employees=' . ($withEmployees ? 'true' : 'false');

        if ($page) {
            $queryString .= "&page=" . $page;
            $queryString .= "&per_page=" . $perPage;
        }

        if ($searchTerm) {
            $queryString .= "&search_term=" . $searchTerm;
        }

        $request = new Request(
            'GET',
            "/payroll/{$id}" . $queryString
        );
        return $this->send($request);
    }

    /**
     * Fetch payrolls for given ids
     *
     * @param array $id Payroll IDs
     * @return json Payroll information
     *
     */
    public function getMultiple(array $ids, int $page = null, int $limit = null)
    {

        $filters = [
            'filter[ids]' => $ids,
            'filter[page]' => $page,
            'filter[limit]' => $limit
        ];

        $request = new Request(
            'GET',
            "/payrolls",
            ['Content-Type' => 'application-x-www-form-urlencoded'],
            http_build_query(array_filter($filters))
        );
        return $this->send($request);
    }

    /**
     * Check if payroll has gap loan candidates
     *
     * @param int $id Payroll ID
     * @return json Payroll information
     *
     */
    public function hasGapLoanCandidates(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll/{$id}/has_gap_loan_candidates/"
        );

        return $this->send($request);
    }

    /**
     * Fetch payrolls for given company id
     *
     * @param int $id Company ID
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Payroll of given company
     *
     */
    public function getCompanyPayrolls(int $id, array $data = [], AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/payrolls?" . http_build_query($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Fetch payroll groups with payroll periods
     *
     * @param int $id Company ID
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Payroll groups of given company
     *
     */
    public function getCompanyPayrollGroupsWithPeriods(int $id, $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/payroll_group_periods"
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Create Payroll
     *
     * @param array $payroll
     * @return \Illuminate\Http\JsonResponse Created payroll with employee
     */
    public function create(array $payroll)
    {
        $request = new Request(
            'POST',
            "/payroll",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($payroll)
        );
        return $this->send($request);
    }

    /**
     * Start Payroll Creation Job
     *
     * @param array $payroll
     * @return \Illuminate\Http\JsonResponse Job id
     */
    public function regularPayrollJob(array $payroll)
    {
        $request = new Request(
            'POST',
            "/payroll/regular_payroll_job",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($payroll)
        );
        return $this->send($request);
    }

    /**
     * Return Payroll Creation Job Status
     *
     * @param string $jobId
     * @return \Illuminate\Http\JsonResponse job status
     */
    public function regularPayrollJobStatus(string $jobId)
    {
        $request = new Request(
            'GET',
            "/payroll/regular_payroll_job/{$jobId}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ]
        );
        return $this->send($request);
    }


    /**
     * Create Special Payroll
     *
     * @param array $payroll
     * @param AuthzDataScope|null $authzDataScope
     * @return \Illuminate\Http\JsonResponse Created payroll with employee
     */
    public function createSpecial(array $payroll, $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/payroll/special",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($payroll)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Start Special Payroll Creation Job
     *
     * @param array $payroll
     * @param AuthzDataScope|null $authzDataScope
     * @return \Illuminate\Http\JsonResponse Created payroll with employee
     */
    public function createSpecialJob(array $payroll, $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/payroll/special_payroll_job",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($payroll)
        );
        return $this->send($request, $authzDataScope);
    }

     /**
     * Return Special Payroll Creation Job Status
     *
     * @param string $jobId
     * @return \Illuminate\Http\JsonResponse job status
     */
    public function specialPayrollJobStatus(string $jobId)
    {
        $request = new Request(
            'GET',
            "/payroll/special_payroll_job/{$jobId}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ]
        );
        return $this->send($request);
    }
    /**
     * Update Payroll
     *
     * @param int $id Payroll ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Created payroll with employee
     */
    public function update(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/payroll/{$id}",
            [
                'Content-Type' => 'application/json'

            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Close payroll for given id
     *
     * @param int $id Payroll ID
     * @return json Payroll information
     *
     */
    public function close(int $id, array $data = [])
    {
        $request = new Request(
            'POST',
            "/payroll/{$id}/close",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Open payroll for given id
     *
     * @param int $id Payroll ID
     * @return json Payroll information
     *
     */
    public function open(int $id)
    {
        $request = new Request(
            'POST',
            "/payroll/{$id}/open"
        );
        return $this->send($request);
    }

    /**
     * Delete payroll for given id
     *
     * @param int $id Payroll ID
     * @return json Payroll information
     *
     */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/payroll/{$id}"
        );
        return $this->send($request);
    }


    /**
     * Delete multiple payroll for given ids
     *
     * @param array $id Payroll ID
     * @param AuthzDataScope|null $authzDataScope
     * @return json Payroll information
     *
     */
    public function deleteMany(array $id, $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            "/payrolls",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($id)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Start the job to delete multiple payroll for given ids
     *
     * @param array $id Payroll ID
     * @param AuthzDataScope|null $authzDataScope
     * @return json Payroll information
     *
     */
    public function deleteManyJob(array $id, $authzDataScope = null)
    {
        $request = new Request(
            'DELETE',
            "/payrolls/delete_payroll_job",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($id)
        );

        return $this->send($request, $authzDataScope);
    }

    public function deleteManyJobStatus($jobId)
    {
        $request = new Request(
            'GET',
            "/payrolls/delete_payroll_job/{$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Check if payroll has generated payslips
     *
     * @param int $id Payroll ID
     * @return json Payroll has payslips
     *
     */
    public function hasPayslips(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll/{$id}/has_payslips"
        );
        return $this->send($request);
    }

    /**
     * Send payslip for the given payroll id
     *
     * @param int $id Payroll ID
     * @return \Illuminate\Http\JsonResponse Payroll information
     */
    public function sendPayslips(int $id)
    {
        $request = new Request(
            'POST',
            "/payroll/{$id}/send_payslips"
        );
        return $this->send($request);
    }

    /**
     * Get gap loan candidates for payroll
     *
     * @param int $id Payroll ID
     * @return json Employees information
     *
     */
    public function getGapLoanCandidates(int $id)
    {
        $request = new Request(
            'GET',
            "/payroll/{$id}/get_gap_loan_candidates"
        );

        return $this->send($request);
    }

    /**
     * Get company payroll available items
     *
     * @param int $id Company Id
     * @param int $type Payroll Type
     * @return Psr\Http\Message\ResponseInterface
     *
     */
    public function getCompanyPayrollAvailableItems(
        int $id,
        string $type,
        AuthzDataScope $authzDataScope = null,
        array $query = null
    ) {
        $queryString = empty($query) ? '' : '?' . http_build_query($query);

        $request = new Request(
            'GET',
            "/company/{$id}/payroll/{$type}/available_items" . $queryString
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Create a payroll job record for the given payload
     *
     * @param array $payload required payload for request
     * @return \Illuminate\Http\JsonResponse Payroll information
     */
    public function addUploadJobToPayroll(array $payload)
    {
        $request = new Request(
            'POST',
            "/payroll_job",
            [
                'Content-Type' => 'application/vnd.api+json'
            ],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * get a payroll job record
     *
     * @param array $jobId Payroll Job Job Identifier
     * @return \Illuminate\Http\JsonResponse Payroll Job information
     */
    public function getPayrollJob(string $jobId)
    {
        $request = new Request(
            'GET',
            "/payroll_job/{$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Delete a payroll job record for the given payload
     *
     * @param array $payload required payload for request
     * @return \Illuminate\Http\JsonResponse Payroll information
     */
    public function deleteUploadedJobToPayroll(array $payload)
    {
        $request = new Request(
            'DELETE',
            "/payroll_job",
            [
                'Content-Type' => 'application/vnd.api+json'
            ],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * Calculate Payroll
     *
     * @param int $payrollId Payroll ID
     * @return \Illuminate\Http\JsonResponse Payroll Calculation Job response
     */
    public function calculate($payrollId, $params)
    {
        // Joe
        $request = new Request(
            'POST',
            "/payroll/{$payrollId}/calculate",
            [
                'Content-Type' => 'application/vnd.api+json' //TODO this is not compliant
            ],
            json_encode($params)
        );

        return $this->send($request);
    }

    /**
     * Get Employees current active disbursement method
     *
     * @param array $payload required payload for request
     * @return \Illuminate\Http\JsonResponse Employee Disbursement information
     */
    public function getEmployeeActiveDisbursementMethod(int $payrollId, array $request)
    {
        $params = array_only($request, ['page', 'perPage', 'filter']);
        $queryString = http_build_query($params);
        $request = new Request(
            'GET',
            "/payroll/{$payrollId}/employee_disbursement_method" . ($queryString ? "?{$queryString}" : '')
        );

        return $this->send($request);
    }

    /**
     * Get Payroll Disbursement Summary
     *
     * @param array $payload required payload for request
     * @return \Illuminate\Http\JsonResponse Employee Disbursement information
     */
    public function getDisbursementSummary(int $payrollId)
    {
        $request = new Request(
            'GET',
            "/payroll/{$payrollId}/disbursement_summary"
        );

        return $this->send($request);
    }

    /**
     * Get Payroll Job details
     */
    public function getPayrollJobDetail($payrollId, $jobName)
    {
        $request = new Request(
            'GET',
            "/payroll/{$payrollId}/job/{$jobName}"
        );

        return $this->send($request);
    }

    /**
     * get payroll register
     */
    public function getPayrollRegister($payrollId)
    {
        $request = new Request(
            'GET',
            "/payroll_register/{$payrollId}"
        );

        return $this->send($request);
    }

    /**
     * get payroll register by hash
     */
    public function getPayrollRegisterByHash($hashId)
    {
        return $this->getPayrollRegister($hashId);
    }

    /**
     * get zipped payroll register by hash
     */
    public function getZippedPayrollRegisterByHash($hashId)
    {
        $request = new Request(
            'GET',
            "/payroll_registers/{$hashId}"
        );

        return $this->send($request);
    }

    /**
     * create payroll register
     */
    public function createPayrollRegister($payrollId)
    {
        $request = new Request(
            'POST',
            "/payroll_register/{$payrollId}"
        );

        return $this->send($request);
    }

    /**
     * create multiple payroll registers
     */
    public function createMultiplePayrollRegisters($ids, $userData)
    {

        $payload = [
            'user' => $userData,
            'id' => $ids['id']
        ];


        $request = new Request(
            'POST',
            "/payroll_register/multiple",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * Send payslip per employee
     */
    public function sendSinglePayslip($id, $employeeId)
    {
        $request = new Request(
            'POST',
            "/payroll/{$id}/send_single_payslip",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query(compact('employeeId'))
        );

        return $this->send($request);
    }

    /**
     * Get Payroll Bank Advise per bank
     */
    public function getBankAdvise($payrollId, $bankName)
    {
        $payload = [
            'data' => ['bank' => $bankName]
        ];

        $request = new Request(
            'POST',
            "/payroll/{$payrollId}/bank_advise",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * Get Payroll Bank Advise per bank
     */
    public function getBankFile($payrollId, $bankName)
    {
        $payload = [
            'data' => ['bank' => $bankName]
        ];

        $request = new Request(
            'POST',
            "/payroll/{$payrollId}/bank_file",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * Fetch payrolls statistics data for given company id
     *
     * @param int $id Company ID
     * @return \Illuminate\Http\JsonResponse List of Payroll of given company
     *
     */
    public function getCompanyPayrollDataCounts($id)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/payroll_data_counts"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to trigger the command create company payroll statistics data
     *
     * @param array $data payroll statistics
     * @return \Illuminate\Http\JsonResponse payroll statistics data
     */
    public function createCompanyPayrollStatistics(int $companyId)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/payroll_statistics"
        );

        return $this->send($request);
    }


    /**
     * Get the latest generate and regenerate jobs status for multiple jobs
     */
    public function getPayrollsJobsStatus(array $payrollIds)
    {
        $payload = [
            'payroll_ids' => $payrollIds
        ];
        $request = new Request(
            'GET',
            "/payrolls/jobs/status",
            ['Content-type' => 'application/json'],
            json_encode($payload)
        );

        return $this->send($request);
    }

    /**
     * Fetch payroll group periods
     *
     * @param int $payrollGroupId Payroll Group ID
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Payroll group periods of given payroll group id
     *
     */
    public function getPayrollGroupPeriods(int $payrollGroupId, $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/payroll_group/{$payrollGroupId}/periods"
        );

        return $this->send($request, $authzDataScope);
    }
}
