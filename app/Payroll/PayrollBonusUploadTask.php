<?php

namespace App\Payroll;

class PayrollBonusUploadTask extends PayrollUploadTask
{
    const TYPE = 'bonus';

    const ID_PREFIX = 'payroll_bonus_upload:';
}
