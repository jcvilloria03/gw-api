<?php

namespace App\Allowance;

use App\Common\CommonAuthorizationService;

class AllowanceAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.allowances';
    public $createTask = 'create.allowances';
    public $updateTask = 'edit.allowances';
    public $deleteTask = 'delete.allowances';
}
