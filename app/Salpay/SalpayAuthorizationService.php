<?php

namespace App\Salpay;

use App\Common\CommonAuthorizationService;

class SalpayAuthorizationService extends CommonAuthorizationService
{
    protected $viewTask   = 'view.salpay_integration';
    protected $createTask = 'create.salpay_integration';
    protected $updateTask = 'edit.salpay_integration';
    protected $deleteTask = 'delete.salpay_integration';
}
