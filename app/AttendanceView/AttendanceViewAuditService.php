<?php

namespace App\AttendanceView;

use App\Audit\AuditService;
use App\Audit\AuditItem;

class AttendanceViewAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';

    const OBJECT_NAME = 'attendance_view';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        AuditService $auditService
    ) {
        $this->auditService = $auditService;
    }

    /**
     *
     * Log Attendance view related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
        }
    }

    /**
     * Log AttendanceView create
     * @param  array  $cacheItem
     * @return void
     */
    public function logCreate(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $newData = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $newData['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $newData['id'],
                'new' => $newData
            ],
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log AttendanceView update
     * @param  array  $cacheItem
     * @return void
     */
    public function logUpdate(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $oldData = json_decode($cacheItem['old'], true);
        $newData = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $newData['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'old' => $oldData,
                'new' => $newData,
            ],
        ]);

        $this->auditService->log($item);
    }

}
