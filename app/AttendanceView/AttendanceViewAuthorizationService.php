<?php

namespace App\AttendanceView;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class AttendanceViewAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.attendance_view';
    const CREATE_TASK = 'create.attendance_view';
    const UPDATE_TASK = 'edit.attendance_view';

    /**
     * @param \stdClass $attendanceView
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $attendanceView, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($attendanceView, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $attendanceView
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $attendanceView, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($attendanceView, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $attendanceView
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $attendanceView, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($attendanceView, $user, self::UPDATE_TASK);
    }

    /**
     * Authorize attendance view related tasks
     *
     * @param \stdClass $attendanceView
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $attendanceView,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for attendance view account
            return $accountScope->inScope($attendanceView->account_id);
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for attendance view account
            return $accountScope->inScope($attendanceView->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as attendance view's account
                return $attendanceView->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($attendanceView->company_id);
        }

        return false;
    }
}
