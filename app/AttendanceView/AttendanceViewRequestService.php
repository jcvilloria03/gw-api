<?php

namespace App\AttendanceView;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class AttendanceViewRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get user custom view
     *
     * @param int $companyId Company ID
     * @param int $userId User ID
     *
     * @return \Illuminate\Http\JsonResponse User Custom View
     */
    public function getUserCustomView(int $companyId, int $userId)
    {
        $request = new Request(
            'GET',
            "/{$companyId}/attendance/user/{$userId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Attendance Custom View Columns
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAttendanceViewColumns(int $id)
    {
        $request = new Request(
            'GET',
            "/attendance/columns/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Attendance Custom View
     *
     * @param int $attendanceId attendance ID
     *
     * @return \Illuminate\Http\JsonResponse Attendance View
     */
    public function getView(int $attendanceId)
    {
        $request = new Request(
            'GET',
            "/attendance/{$attendanceId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create custom view
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse User Custom View
     */
    public function createCustomView(array $data)
    {
        $request = new Request(
            'POST',
            '/attendance',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update attendance view
     *
     * @param array $data
     * @param int $attendanceId
     *
     * @return \Illuminate\Http\JsonResponse User Custom View
     */
    public function updateCustomView(array $data, int $attendanceId)
    {
        $request = new Request(
            'PATCH',
            "/attendance/{$attendanceId}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }
}
