<?php

namespace App\Authz;

use App\Account\AccountRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;

class AuthzService
{
    /**
     * @var string
     */
    private $accountEssentialCacheKey;

    /**
     * @var array
     */
    private $clearCacheForEndpoints;

    /**
     * @var array
     */
    private $clearCacheForStatusCodes;

    public function __construct()
    {
        $this->accountEssentialCacheKey = Config::get('account_essential.redis_cache_key');
        $this->clearCacheForEndpoints = Collection::make(Config::get('account_essential.cache.clear.endpoints', []));
        $this->clearCacheForStatusCodes = Config::get('account_essential.cache.clear.status_codes', []);
    }

    public function isUriIncludesToClearEssential(string $method, string $uri, int $statusCode): bool
    {
        return
            in_array($statusCode, $this->clearCacheForStatusCodes)
            && $this->clearCacheForEndpoints
                ->where('method', '=', $method)
                ->where('uri', '=', $uri)
                ->isNotEmpty()
        ;
    }

    public function clearAccountCacheEssentialData(int $accountId)
    {
        Redis::del($this->getAccountCacheKey($accountId));
    }

    /**
     * @param int $accountId
     * @return string
     */
    private function getAccountCacheKey(int $accountId)
    {
        return sprintf($this->accountEssentialCacheKey, $accountId);
    }
}
