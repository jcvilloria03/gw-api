<?php

namespace App\Authz;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthzRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Check Salarium Clearance
     *
     * @param  array    $payload
     * @return array|null
     */
    public function checkSalariumClearance(array $payload)
    {
        $request = new Request(
            'POST',
            '/api/v1/salarium_clearance',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($payload)
        );

        try {
            $response = $this->send($request);
            $data = json_decode($response->getData(), true)['data'];

            return $data;
        } catch (HttpException $e) {
            if ($e->getStatusCode() !== Response::HTTP_UNAUTHORIZED) {
                throw $e;
            }

            return null;
        }
    }
}
