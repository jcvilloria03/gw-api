<?php

namespace App\Authz;

use Illuminate\Support\Arr;

class AuthzDataScope
{
    const REQUEST_AUTHZ_ENABLED = 'authz_enabled';

    const REQUEST_AUTHZ_DATA_SCOPE = 'authz_data_scope';

    const SCOPE_COMPANY = 'COMPANY';

    const SCOPE_DEPARTMENT = 'DEPARTMENT';

    const SCOPE_POSITION = 'POSITION';

    const SCOPE_LOCATION = 'LOCATION';

    const SCOPE_TEAM = 'TEAM';

    const SCOPE_PAYROLL_GROUP = 'PAYROLL_GROUP';

    const VALID_SCOPES = [
        self::SCOPE_COMPANY,
        self::SCOPE_DEPARTMENT,
        self::SCOPE_POSITION,
        self::SCOPE_LOCATION,
        self::SCOPE_TEAM,
        self::SCOPE_PAYROLL_GROUP
    ];


    const SCOPE_VALUE_ALL = '__ALL__';

    /**
     * Data Scope
     *
     * @var array
     */
    protected $dataScope;

    /**
     * Clearance
     *
     * @var array
     */
    protected $clearance;

    /**
     * Subject
     *
     * @var array
     */
    protected $subject;

    /**
     * Data Scope Filter
     *
     * @var array
     */
    public $dataScopeFilter;

    /**
     * Constructs AuthzDataScope
     *
     * @param array $dataScope Data Scope
     */
    public function __construct(array $dataScope, array $clearance, array $subject = [])
    {
        $this->dataScope = $dataScope;
        $this->clearance = $clearance;
        $this->subject = $subject;
        $this->dataScopeFilter = $dataScope;
    }

    /**
     * Creates an instance of AuthzDataScope from clearance data
     *
     * @param  array  $clearance Clearance data from Authz API
     * @return self
     */
    public static function makeFromClearance(array $clearance, array $accountEssential = [], array $subject = [])
    {
        $modifiedClearance = [];
        $dataScope = [];
        foreach ($clearance as $module => $data) {
            $scope = $data['data_scope'] ?? [];
            foreach ($scope as $key => &$values) {
                if (in_array(self::SCOPE_VALUE_ALL, $values)) {
                    $values = $accountEssential[$key];
                }
            }
            $modifiedClearance[$module] = $scope;
            $dataScope = array_merge($dataScope, $scope);
        }

        return new self($dataScope, $modifiedClearance, $subject);
    }

    /**
     * Check if the requested scope, and ID is authorized
     *
     * @param  string  $scope   Scope name
     * @param  array|int     $idValue Scope ID value
     * @param  string|null  $module Data Scopes
     * @return boolean          Returns true if authorized
     */
    public function isAuthorized(string $scope, $idValues, $module = null): bool
    {
        if (!in_array($scope, self::VALID_SCOPES)) {
            return false;
        }

        if (!is_array($idValues)) {
            $idValues = [$idValues];
        }

        if (empty($idValues)) {
            return false;
        }

        if ($module === null) {
            $dataScopes = $this->dataScope;
            $dataScopesFilter = $this->dataScopeFilter;
        } else {
            $dataScopes = $this->getDataScopeForModule($module);
            $dataScopesFilter = $this->getDataScopeForModule($module);
        }

        return $this->validateScope($scope, $idValues, $dataScopes);
    }

    private function validateScope(string $scope, array $idValues, array $dataScopes): bool
    {
        if ($scope !== self::SCOPE_COMPANY && empty(Arr::get($dataScopes, $scope, []))) {
            return true;
        } elseif ($scope === self::SCOPE_COMPANY && !array_key_exists($scope, $dataScopes)) {
            return false;
        }

        $scopeValues = $dataScopes[$scope];
        $result = true;
        foreach ($idValues as $idValue) {
            if (!in_array(self::SCOPE_VALUE_ALL, $scopeValues) && !in_array($idValue, $scopeValues)) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    /**
     * Check if all provided scope-value is authorized in the requested array of scopes and IDs
     *
     * @param  array   $scopeValues Key-value pairs of scope name and scope ID value
     * @param  string|null   $module Data Scope
     * @return boolean              Returns true if all provided scope-value is authorized
     */
    public function isAllAuthorized(array $scopeValues, $module = null): bool
    {
        foreach ($scopeValues as $scope => $idValue) {
            if ($idValue === null) {
                continue;
            }

            $isAuthorized = $this->isAuthorized($scope, $idValue, $module);

            if (!$isAuthorized) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the stored data scope
     *
     * @return array
     */
    public function getDataScope()
    {
        return $this->dataScope;
    }

    /**
     * Get the stored clearance
     *
     * @return array
     */
    public function getClearance(): array
    {
        return $this->clearance;
    }

    /**
     * Get the stored data scope filter
     *
     * @return array
     */
    public function getDataScopeFilter()
    {
        return $this->dataScopeFilter;
    }

    public function getDataScopeForModule(string $module): array
    {
        return Arr::get($this->clearance, $module, []);
    }

    public function getModules(): array
    {
        return array_keys($this->clearance);
    }
}
