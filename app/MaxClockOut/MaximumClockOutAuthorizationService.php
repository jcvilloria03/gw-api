<?php

namespace App\MaxClockOut;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class MaximumClockOutAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.max_clock_out';
    const CREATE_TASK = 'create.max_clock_out';
    const UPDATE_TASK = 'edit.max_clock_out';
    const DELETE_TASK = 'delete.max_clock_out';

    /**
     * @param \stdClass $maxClockOut
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $maxClockOut, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($maxClockOut, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $maxClockOut
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyMaxClockOuts(\stdClass $maxClockOut, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($maxClockOut, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $maxClockOut
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $maxClockOut, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($maxClockOut, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $maxClockOut
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $maxClockOut, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($maxClockOut, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $maxClockOut
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $maxClockOut, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($maxClockOut, $user, self::DELETE_TASK);
    }

    /**
     * Authorize project related tasks
     *
     * @param \stdClass $project
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $project,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for project account
            return $accountScope->inScope($project->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as project's account
                return $project->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($project->company_id);
        }

        return false;
    }
}
