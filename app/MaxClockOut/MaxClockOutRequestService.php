<?php

namespace App\MaxClockOut;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class MaxClockOutRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get max clock outs rates for the company.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Maximum Clock-out for the company
     */
    public function getCompanyMaxClockOuts(int $companyId, array $params)
    {
        $url = "/company/{$companyId}/max_clock_outs?" . http_build_query($params);
        $request = new Request(
            'GET',
            $url
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get single max clock out
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Maximum Clock-out for the company
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/max_clock_out/$id"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create max clock out.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Maximum Clock-out for the company
     */
    public function createCompanyMaxClockOut(array $data)
    {
        $request = new Request(
            'POST',
            "/max_clock_out",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update max clock out
     *
     * @param array $data
     * @param int $id Day/Hour Rate ID
     * @return \Illuminate\Http\JsonResponse Updated Day/Hour Rate
     */
    public function updateCompanyMaxClockOut(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/max_clock_out/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple max clock-out
     *
     * @param array $data max clock-out to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted max clock-out id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/max_clock_out/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch max clock-out rule
     *
     * @param int $id Employee ID
     * @return json Max clockout information
     *
     */
    public function getRules(int $id)
    {
        $request = new Request(
            'GET',
            "/max_clock_out/get_rule/{$id}"
        );
        return $this->send($request);
    }
}
