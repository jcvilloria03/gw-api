<?php

namespace App\HoursWorked;

use App\Authorization\EssBaseAuthorizationService;

class EssHoursWorkedAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.hours_worked';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeGetHoursWorked(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
