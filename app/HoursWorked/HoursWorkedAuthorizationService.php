<?php

namespace App\HoursWorked;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class HoursWorkedAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.hours_worked';
    const EDIT_TASK = 'edit.hours_worked';

    /**
     * @param \stdClass $hoursWorked
     * @param array $user
     * @return bool
     */
    public function authorizeGetHoursWorked(\stdClass $hoursWorked, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($hoursWorked, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $hoursWorked
     * @param array $user
     * @return bool
     */
    public function authorizeBulkCreateOrUpdateOrDelete(\stdClass $hoursWorked, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($hoursWorked, $user, self::EDIT_TASK);
    }

    /**
     * Authorize time record related tasks
     *
     * @param \stdClass $timeRecord
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $timeRecord,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);

        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($timeRecord->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $timeRecord->account_id == $user['account_id'];
            }

            // check if user has company level permissions for company
            return $companyScope->inScope($timeRecord->company_id);
        }

        return false;
    }
}
