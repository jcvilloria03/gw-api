<?php

namespace App\HoursWorked;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class HoursWorkedAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    const OBJECT_NAME = 'hours_worked';

    const ACTIONS = [
        'created' => self::ACTION_CREATE,
        'updated' => self::ACTION_UPDATE,
        'deleted' => self::ACTION_DELETE
    ];

    const ACTIONS_CREATE_OR_UPDATE_OR_DELETE = [
        'created',
        'updated',
        'deleted'
    ];

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log TimeRecord related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log Hours Worked create
     *
     * @param array $cacheItem
     * @return void
     */
    public function logCreate(array $cacheItem)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'new' => $new
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Hours Worked update
     *
     * @param array $cacheItem
     * @return void
     */
    public function logUpdate(array $cacheItem)
    {
        $new = json_decode($cacheItem['new'], true);
        $old = json_decode($cacheItem['old'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'old' => $old,
                'new' => $new
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Hours Worked Delete
     *
     * @param array $cacheItem
     * @return void
     */
    public function logDelete(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $old['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'old' => $old
            ]
        ]);

        $this->auditService->log($item);
    }
}
