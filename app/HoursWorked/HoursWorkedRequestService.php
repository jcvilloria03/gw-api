<?php

namespace App\HoursWorked;

use GuzzleHttp\Psr7\Request;
use App\Request\RequestService;

class HoursWorkedRequestService extends RequestService
{
    /**
     * Call endpoint to get employees hours worked withing company
     *
     * @param int $id Company ID
     * @param string $query string
     * @return \Illuminate\Http\JsonResponse HoursWorked
     */
    public function getCompanyHoursWorked(int $id, string $query)
    {
        return $this->send(new Request(
            'GET',
            "/company/{$id}/hours_worked?{$query}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ));
    }

    /**
     * Get employee hours worked
     *
     * @param array $data from request
     * @return \Illuminate\Http\JsonResponse HoursWorked
     */
    public function getEmployeeHoursWorked(array $data)
    {
        $queryString = ($query = http_build_query(array_except($data, ['company_id']))) ? $query : '';

        return $this->getCompanyHoursWorked($data['company_id'], $queryString);
    }

    /**
     * Call endpoint to bulk create or update or delete hours worked
     *
     * @param array $data employees hours worked data
     * @param int $companyId company ID
     * @return \Illuminate\Http\JsonResponse Created or updated or deleted Hours Worked
     */
    public function bulkCreateOrUpdateOrDelete(array $data, int $companyId)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/hours_worked/bulk_create_or_update_or_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to retrieve leaves in hours worked for an employee.
     *
     * @param int $companyId
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAttendanceLeaves($companyId, array $data)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/hours_worked/leaves",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to set the hours work from a given time record
     *
     * @param array $data
     * @param int $companyId
     * @return \Illuminate\Http\JsonResponse
     */
    public function setHoursWorkFromTimeRecord(array $data, int $companyId)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/set/hours_worked",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call endpoint to bulk create or update or delete leaves
     *
     * @param array $data employees leaves data
     * @param int $companyId company ID
     * @return \Illuminate\Http\JsonResponse Created or updated or deleted leaves
     */
    public function leavesBulkCreateOrUpdateOrDelete(array $data, int $companyId)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/hours_worked/leaves/bulk_create_or_update_or_delete",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }
}
