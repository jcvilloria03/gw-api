<?php

namespace App\AdminDashboard;

use App\Common\CommonAuthorizationService;

/**
 * Class AdminDashboardAuthorizationService
 *
 * @package App\AdminDashboard
 */
class AdminDashboardAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.admin_dashboard';
    public $exportTask = 'create.scorecard.export';

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeExportData(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->exportTask);
    }
}
