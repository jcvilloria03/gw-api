<?php

namespace App\AdminDashboard;

use App\Authz\AuthzDataScope;
use GuzzleHttp\Psr7\Request;
use App\Request\DownloadRequestService;

/**
 * Class AdminDashboardRequestService
 *
 * @package App\AdminDashboard
 */
class AdminDashboardRequestService extends DownloadRequestService
{

    /**
     * Call endpoint to fetch all calendar data for selected company
     *
     * @param array $params [company_id, start_date, end_date, group]
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCalendarData(array $params, AuthzDataScope $dataScope = null): \Illuminate\Http\JsonResponse
    {
        $request = new Request(
            'POST',
            '/admin_dashboard/calendar_data',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($params)
        );
        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to fetch all calendar data counts for selected company
     *
     * @param array $params [company_id, start_date, end_date, group]
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCalendarDataCounts(
        array $params,
        AuthzDataScope $dataScope = null
    ): \Illuminate\Http\JsonResponse {
        $request = new Request(
            'POST',
            '/admin_dashboard/calendar_data_counts',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($params)
        );
        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to download csv dashboard statistics for selected company, for specific request type
     *
     * @param int   $id     Company Id
     * @param array $params [type, employees_ids]
     *
     * @return \Illuminate\Http\JsonResponse CSV
     */
    public function downloadCsv(int $id, array $params)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/dashboard_statistics/download",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($params)
        );
        return $this->download($request);
    }

    /**
     * Call endpoint to fetch dashboard statistics for given company
     *
     * @param array $param Company ID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDashboardStatistics(array $param)
    {
        $request = new Request(
            'POST',
            '/admin_dashboard/statistics',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($param)
        );

        return $this->send($request);
    }
}
