<?php

namespace App\Termination;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TerminationInformationRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to create termination information
     *
     * @param array $data termination information
     * @return \Illuminate\Http\JsonResponse Created Termination information
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/termination_informations",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get termination info
     *
     * @param int   $id Termination ID
     * @return \Illuminate\Http\JsonResponse Termination information
     */
    public function get(int $id, array $data = [])
    {
        $request = new Request(
            'GET',
            "/termination_informations/{$id}?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get termination info based on employee
     *
     * @param int   $id Termination ID
     * @return \Illuminate\Http\JsonResponse Termination information
     */
    public function getByEmployeeId(int $id, array $data = [])
    {
        $request = new Request(
            'GET',
            "/employee/{$id}/termination_informations?" .  http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to edit termination info
     *
     * @param int $id Termination ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Edited Termination information
     */
    public function update(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/termination_informations/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete termination info
     *
     * @param int $id Termination ID
     * @return \Illuminate\Http\JsonResponse Termination information
     */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/termination_informations/{$id}"
        );
        return $this->send($request);
    }
}
