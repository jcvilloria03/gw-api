<?php

namespace App\Termination;

use App\Common\CommonAuthorizationService;

class TerminationAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.terminations';
    public $createTask = 'create.terminations';
    public $updateTask = 'edit.terminations';
    public $deleteTask = 'delete.terminations';
}
