<?php

namespace App\Shift;

use App\Tasks\UploadTask;
use Illuminate\Support\Facades\Redis;
use App\Shift\ShiftUploadTaskException;

class AssignShiftsTask extends UploadTask
{
    const PROCESS_VALIDATION = 'save';
    const PROCESS_SAVE = 'save';
    const CACHE_TTL = 259200; # 3 days

    const VALID_PROCESSES = [
        self::PROCESS_VALIDATION,
        self::PROCESS_SAVE,
    ];

    const ID_PREFIX = 'assign_shifts:';

    /**
     * Company Id
     *
     * @var int
     */
    protected $companyId;

    /**
     * Create task in Redis
     *
     * @param int $companyId Company Id
     * @param string $id Job id
     * @return void
     */
    public function create(int $companyId, int $totalAffectedEmployees, string $id = null)
    {
        $this->companyId = $companyId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();
            $this->currentValues = [
                'status' => 'validating',
                'errors' => '',
                'employees' => $totalAffectedEmployees,
                'created' => 0,
                'company_id' => $companyId
            ];
            $this->createInRedis();
            $this->expireInRedis($this->id, self::CACHE_TTL);
        } else {
            $this->id = $id;
            if (!$this->isCompanyIdSame()) {
                // Job::Company Id does not match $this->companyId
                throw new ShiftUploadTaskException('Job does not belong to given Company ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new ShiftUploadTaskException('Invalid Job ID');
        }
    }

    /**
     * Get Redis Key
     * @return string
     */
    public function getRedisKey($companyId, $key)
    {
        $this->companyId = $companyId;
        $this->id = $key;
        if (!$this->isCompanyIdSame()) {
            throw new ShiftUploadTaskException('Job does not belong to given Company ID');
        }
        return Redis::hGetAll($key);
    }

    /**
     * Generate Task Id, to be used as Redis Key
     * @return string
     */
    protected function generateId()
    {
        return static::ID_PREFIX . $this->companyId . ':' . uniqid();
    }

    /**
     * Check if companyId and companyId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     */
    public function isCompanyIdSame()
    {
        // check job.company_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->companyId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Save Leave Credits
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     */
    public function saveFile(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key();
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('s3_key', $s3Key);
        $this->updateValidationStatus(self::STATUS_VALIDATION_QUEUED);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @return string
     */
    protected function generateS3Key()
    {
        return self::ID_PREFIX .
            $this->companyId .
            ':' .
            uniqid();
    }

    /**
     * Update validation status
     *
     * @param string $newStatus The new status of the current process
     */
    public function updateValidationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new ShiftUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[self::PROCESS_VALIDATION . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_VALIDATION . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new ShiftUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_VALIDATION is already validated
        if (
            !isset($this->currentValues[self::PROCESS_VALIDATION . '_status']) ||
            $this->currentValues[self::PROCESS_VALIDATION . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new ShiftUploadTaskException(
                'Assign shifts information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return (!in_array(
                $currentStatus,
                [
                    self::STATUS_VALIDATING,
                    self::STATUS_SAVING,
                ]
            )
        );
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATION_QUEUED || $currentStatus === self::STATUS_SAVE_QUEUED;
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATING || $currentStatus === self::STATUS_SAVING;
    }

    /**
     * Update Error File Location
     *
     * @param string $process The new status of the saving process
     * @param string $errorFileS3Key The S3 key of the error file
     *
     */
    public function updateErrorFileLocation(string $process, string $errorFileS3Key)
    {
        $this->setVal($process . '_error_file_s3_key', $errorFileS3Key);
    }
}
