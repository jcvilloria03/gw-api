<?php

namespace App\Shift;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use App\User\UserRequestService;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ShiftAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_UNASSIGN = 'unassign';
    const OBJECT_NAME = 'shift';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    private $userRequestService;

    public function __construct(
        AuditService $auditService,
        UserRequestService $userRequestService
    ) {
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Log Shift related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_UNASSIGN:
                $this->logUnassign($cacheItem);
                break;
        }
    }

    /**
     * Log Shift create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'user_name' => $userData->name,
                'employee_name' => $data['employee']['first_name'] . ' ' . $data['employee']['last_name'],
                'schedule' => $data['schedule'],
                'shift_dates' => $data['dates']
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Shift update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'user_name' => $userData->name,
                'employee_name' => $new['employee']['first_name'] . ' ' . $new['employee']['first_name'],
                'old_shift_data' => $old,
                'new_shift_data' => $new,
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     *
     * Log Shift Unassign
     *
     * @param array $cacheItem
     *
     */
    public function logUnassign(array $cacheItem)
    {
        $shift = json_decode($cacheItem['new']);
        $user = json_decode($cacheItem['user'], true);
        $userResponse = $this->userRequestService->get($user['id']);
        $userData = json_decode($userResponse->getData());

        $item = new AuditItem([
            'company_id' => $shift->company_id,
            'account_id' => $userData->account_id,
            'user_id' => $userData->id,
            'action' => self::ACTION_UNASSIGN,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'user_name' => $userData->name,
                'employee_name' => $shift->employee->first_name . ' ' . $shift->employee->last_name,
                'schedule' => $shift->schedule,
                'date_of_unassign' => Carbon::now()->format('Y-m-d')
            ]
        ]);

        $this->auditService->log($item);
    }
}
