<?php

namespace App\Shift;

use App\Tasks\UploadTask;
use App\Shift\ShiftUploadTaskException;

class ShiftUploadTask extends UploadTask
{
    const PROCESS_SHIFT = 'validate_shifts';
    const PROCESS_SAVE = 'save';
    const VALIDATED_STATUS_KEY = "validate_shifts_status";

    const VALID_PROCESSES = [
        self::PROCESS_SHIFT,
        self::PROCESS_SAVE
    ];

    const VALIDATION_PROCESSES = [
        self::PROCESS_SHIFT
    ];

    const ID_PREFIX = 'employee_assign_shifts:';
    const REDIS_KEY = 'upload_shift';

    /**
     * Company Id
     *
     * @var int
     */
    protected $companyId;

    /**
     * Create task in Redis
     *
     * @param int $companyId Company Id
     */
    public function create(int $companyId)
    {
        $this->companyId = $companyId;
        // create a new task and create key in Redis
        $this->id = $this->generateId();
        $this->currentValues = [
            's3_bucket' => $this->s3Bucket
        ];
        $this->createInRedis();
    }

    /**
     * Setter: Sets the current task values in this object
     *
     * @param string $id The Redis key that contains the tasks.
     *
     */
    public function setTasksPool(string $id)
    {
        $this->id = $id;
        $tasks = $this->fetch();
        $this->currentValues = $tasks;
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    protected function generateId()
    {
        return static::ID_PREFIX . $this->companyId . ':' . uniqid();
    }

    /**
     * Save shift information in Redis.
     *
     * @param string $path Full path of file to upload
     *
     */
    public function saveShiftInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key(self::PROCESS_SHIFT);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal(self::REDIS_KEY, $s3Key);
        $this->updateValidationStatus(
            self::PROCESS_SHIFT,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @param string $process
     *
     */
    protected function generateS3Key(string $process)
    {
        return 'shift_upload_' .
               $process .
               ':' .
               $this->companyId .
               ':' .
               uniqid();
    }

    /**
     * Update validation status
     *
     * @param string $process The process to update status
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateValidationStatus(string $process, string $newStatus)
    {
        if (!in_array($process, self::VALIDATION_PROCESSES)) {
            throw new ShiftUploadTaskException('Invalid Process');
        }
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new ShiftUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[$process . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal($process . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     *
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new ShiftUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_PAYROLL is already validated
        if (
            !isset($this->currentValues[self::VALIDATED_STATUS_KEY]) ||
            $this->currentValues[self::VALIDATED_STATUS_KEY] !== self::STATUS_VALIDATED
        ) {
            throw new ShiftUploadTaskException(
                'Shift information must be validated first.'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;
        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }
}
