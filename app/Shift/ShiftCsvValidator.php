<?php

namespace App\Shift;

use App\Authz\AuthzDataScope;
use App\CSV\CsvValidatorException;
use App\Employee\EmployeeRequestService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use League\Csv\Reader;

class ShiftCsvValidator
{
    /**
     * @var EmployeeRequestService
     */
    private $employeeRequestService;

    public function __construct(EmployeeRequestService $employeeRequestService)
    {
        $this->employeeRequestService = $employeeRequestService;
    }

    public function validate(UploadedFile $file, AuthzDataScope $authzDataScope, int $companyId)
    {
        $fileName = $file->getPathName();
        $reader = Reader::createFromPath($fileName);
        $records = $reader->fetchAll();
        $employeeIds = [];
        $totalRecords = count($records);
        for ($i = 1; $i < $totalRecords; $i++) {
            $employeeIds[] = $records[$i][0];
        }
        $response = $this->employeeRequestService->getCompanyEmployeesByAttribute(
            $companyId,
            'employee_id',
            ['values' => implode(',', $employeeIds)]
        );
        $data = json_decode($response->getData(), true);
        $unauthorizedIds = [];
        foreach ($data as $employee) {
            if (!$authzDataScope->isAllAuthorized([
                AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
                AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
                AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
                AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
                AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'team_id'),
                AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll_group_id'),
            ])) {
                $unauthorizedIds[] = $employee['employee_id'];
            }
        }

        if (!empty($unauthorizedIds)) {
            throw new CsvValidatorException(sprintf(
                'Permission denied for employee %s.',
                implode(', ', $unauthorizedIds)
            ));
        }
    }
}
