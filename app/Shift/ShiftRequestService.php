<?php

namespace App\Shift;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Authz\AuthzDataScope;

class ShiftRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get shift
     *
     * @param int $id Shift ID
     * @return \Illuminate\Http\JsonResponse Shift Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/shift/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all shifts within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company shifts
     */
    public function getCompanyShifts(int $companyId, AuthzDataScope $authzDataScope = null, array $queryParams = [])
    {
        $url = "/company/{$companyId}/shifts";
        if (!empty($queryParams)) {
            $url .= "?" . http_build_query($queryParams);
        }

        $request = new Request(
            'GET',
            $url
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to get shifts data within company
     *
     * @param int $companyId Company Id
     * @param AuthzDataScope $authzDataScope Authz Data Scope object
     * @return \Illuminate\Http\JsonResponse List of shifts, employees and rest days for calendar
     */
    public function getShiftsData(int $companyId, AuthzDataScope $authzDataScope = null, array $queryParams = [])
    {

        $url = "/company/{$companyId}/shifts_data";
        if (!empty($queryParams)) {
            $url .= "?" . http_build_query($queryParams);
        }

        $request = new Request(
            'GET',
            $url
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to create shift
     *
     * @param array $data shift information
     * @return \Illuminate\Http\JsonResponse Created Shift
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            '/shift',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create multiple shifts.
     *
     * @param array $data Shifts information
     * @return \Illuminate\Http\JsonResponse Created Shifts Ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            '/shift/bulk_create',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update shift for given id
     *
     * @param array $data shift informations
     * @param int $id shift Id
     * @return \Illuminate\Http\JsonResponse Updated shift
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            '/shift/' . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to unassign shift for given id and data
     *
     * @param array $data unassign informations
     * @param int $id shift Id
     * @return \Illuminate\Http\JsonResponse Shift
     */
    public function unassign(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            '/shift/unassign/' . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to generate csv with shifts
     *
     * @param int $companyId Company Id
     * @param array $data data
     * @return \Illuminate\Http\JsonResponse Name of generated csv file
     */
    public function generateCsv(int $companyId, array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/shifts/generate_csv",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to download generated csv
     *
     * @param int $companyId Company Id
     * @param string $fileName csv file name
     * @return \Illuminate\Http\Response
     */
    public function downloadCsv(int $companyId, string $fileName)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/shifts/download/{$fileName}",
            [
                'Content-Type' => 'application/json'
            ]
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get employee's shifts
     *
     * @param int $id Employee Id
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function getEmployeeShifts(int $id, array $data)
    {
        return $this->send(new Request(
            'GET',
            "/employee/{$id}/shifts?" . http_build_query($data)
        ));
    }

    /**
     * Call endpoint to get multiple employee shifts
     *
     * @param  int    $companyId   Company ID
     * @param  array  $employeeIds List of employee IDs
     * @param  array  $data        Request data
     * @return array
     */
    public function getMultipleEmployeeShifts(int $companyId, array $employeeIds, array $data)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/employee_shifts",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'data' => array_merge([
                    'employee_ids' => $employeeIds
                ], $data)
            ])
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'];
    }

    /**
     * Call endpoint to get filtered shifts for given params
     *
     * @param array $params
     * @return \Illuminate\Http\Response
     */
    public function getFilteredShifts(array $params)
    {
        $employeeIds = $params['employee_ids'] ?? [];

        $request = new Request(
            'GET',
            '/shift?' . http_build_query(['employee_ids' => $employeeIds])
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get requests that are overlaping with shifts
     *
     * @param int $id shift Id
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Shift
     */
    public function getShiftsOverlappingRequests(array $data)
    {
        $request = new Request(
            'POST',
            '/shift/overlapping_requests',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to assign and overwrite employee shift
     *
     * @param  array  $data Shift data
     * @return array
     */
    public function assignOverwrite(array $data)
    {
        $request = new Request(
            'POST',
            '/shift/assign_overwrite',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }

    /**
     * Call endpoint to unassign and overwrite employee shift
     *
     * @param  array  $data Shift data
     * @return array
     */
    public function unassignOverwrite(array $data)
    {
        $request = new Request(
            'POST',
            '/shift/unassign_overwrite',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        $response = $this->send($request);

        return json_decode($response->getData(), true);
    }
}
