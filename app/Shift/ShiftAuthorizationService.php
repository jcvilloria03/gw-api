<?php

namespace App\Shift;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class ShiftAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.shift';
    const CREATE_TASK = 'create.shift';
    const UPDATE_TASK = 'edit.shift';
    const DELETE_TASK = 'delete.shift';

    /**
     * @param \stdClass $shift
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $shift
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyShifts(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $shift
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $shift
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $shift
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $shift
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $shift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($shift, $user, self::CREATE_TASK);
    }

    /**
     * Authorize shift related tasks
     *
     * @param \stdClass $shift
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $shift,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($shift->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $shift->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($shift->company_id);
        }

        return false;
    }
}
