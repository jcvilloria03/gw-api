<?php

namespace App\Shift;

use App\Authorization\EssBaseAuthorizationService;

class EssShiftsAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.shift';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeGet(int $userId)
    {
        $this->buildUserPermissions($userId);

        return $this->authorizeTask(self::VIEW_TASK);
    }
}
