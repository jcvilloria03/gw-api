<?php

namespace App\Earning;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class EarningRequestService extends RequestService
{
    /**
     * Call endpoint to fetch earnings for given company id
     *
     * @param int $id Company ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(int $id)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/earnings"
        );

        return $this->send($request);
    }

    /*
     * Call endpoint to fetch Data to be saved in Earning upload job
     *
     * @param string $id Job ID
     * @return json Company information
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/earning/upload_preview?job_id={$jobId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch earning for given earning ID
     *
     * @param int $id Earning ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/earning/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update earning info
     *
     * @param int $earningId earning ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Edited Termination information
     */
    public function update($earningId, array $data)
    {
        $request = new Request(
            'PATCH',
            "/earning/{$earningId}/update",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch earning for given employee ID
     *
     * @param int $employeeId Earning ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByEmployeeId(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/earning"
        );

        return $this->send($request);
    }
}
