<?php

namespace App\Earning;

use App\Tasks\UploadTask;

class EarningUploadTask extends UploadTask
{
    const PROCESS_VALIDATION = 'validation';
    const PROCESS_SAVE = 'save';

    const VALID_PROCESSES = [
        self::PROCESS_VALIDATION,
        self::PROCESS_SAVE,
    ];

    const ID_PREFIX = 'earning_upload:';

    /**
     * Company Id
     * @var int
     */
    protected $companyId;

    /**
     * Create task in Redis
     * @param int $companyId Company id
     * @param string $id Job id
     * @throws EarningUploadTaskException
     */
    public function create(int $companyId, string $id = null)
    {
        $this->companyId = $companyId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();
            $this->currentValues = [
                's3_bucket' => $this->s3Bucket
            ];
            $this->createinRedis();
        } else {
            $this->id = $id;
            if (!$this->isCompanyIdSame()) {
                // Job::Company Id does not match $this->companyId
                throw new EarningUploadTaskException('Job does not belong to given Company ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new EarningUploadTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     */
    protected function generateId()
    {
        return static::ID_PREFIX . $this->companyId . ':' . uniqid();
    }

    /**
     * Check if companyId and companyId in jobId match.
     * May be different when jobId is not auto-generated.
     * @return boolean
     */
    public function isCompanyIdSame()
    {
        // check job.company_id metches request
        $needle = '/^' . static::ID_PREFIX . $this->companyId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Save  earnings
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     */
    public function saveFile(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key();
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('s3_key', $s3Key);
        $this->updateValidationStatus(self::STATUS_VALIDATION_QUEUED);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded.
     */
    protected function generateS3key()
    {
        return self::ID_PREFIX .
            $this->companyId .
            ':' .
            uniqid();
    }

    /**
     * Update validation status.
     * @param string $newStatus The new status of the current process.
     * @throws EarningUploadTaskException
     */
    public function updateValidationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new EarningUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[self::PROCESS_VALIDATION . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_VALIDATION . '_status', $newStatus);
        }
    }

    /**
     * Update write status.
     * @param string $newStatus The new status of the saving process
     * @throws EarningUploadTaskException
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new EarningUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_VALIDATION is already validated
        if (
            !isset($this->currentValues[self::PROCESS_VALIDATION . '_status']) ||
            $this->currentValues[self::PROCESS_VALIDATION . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new EarningUploadTaskException(
                ' earnings information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Check if Status can be updated to Queued.
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return (
        !in_array(
            $currentStatus,
            [
                self::STATUS_VALIDATING,
                self::STATUS_SAVING,
            ]
        )
        );
    }

    /**
     * Check if Status can be updated to Validating.
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATION_QUEUED || $currentStatus === self::STATUS_SAVE_QUEUED;
    }

    /**
     * Check if Status can be updated to a finished status.
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATING || $currentStatus === self::STATUS_SAVING;
    }

    /**
     * Update Error File Location.
     * @param string $process The new status of the saving process
     * @param string $errorFileS3Key The S3 key of the error file
     */
    public function updateErrorFileLocation(string $process, string $errorFileS3Key)
    {
        $this->setVal($process . '_error_file_s3_key', $errorFileS3Key);
    }
}
