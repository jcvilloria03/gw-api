<?php

namespace App\Earning;

use App\Audit\AuditItem;
use App\Audit\AuditService;
use App\Audit\AuditServiceTrait;

class EarningAuditService
{
    use AuditServiceTrait;

    const EARNING = 'earning';

    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @var AuditService
     */
    private $auditService;

    /**
     * @var EarningRequestService
     */
    private $requestService;

    public function __construct(
        AuditService $auditService,
        EarningRequestService $requestService
    ) {
        $this->auditService = $auditService;
        $this->requestService = $requestService;
    }

    /**
     * Log Earning related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
        }
    }

    /**
     * Log Earning create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem, $isBatch = false)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE : self::ACTION_CREATE,
            'object_name' => self::EARNING,
            'data' => $data
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log batch create earnings
     *
     * @param array $ids earnings ids
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->requestService->get($id);
                $data = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($data),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit earning batch create with id = ' . $id);
            }
        }
    }

    /**
     * Log Earning update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::EARNING,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);
        $this->auditService->log($item);
    }
}
