<?php

namespace App\Earning;

use App\Common\CommonAuthorizationService;

class EarningAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.earnings';
    public $createTask = 'create.earnings';
    public $updateTask = 'edit.earnings';
    public $deleteTask = 'delete.earnings';
}
