<?php

namespace App\Subscriptions;

use App\Common\CommonAuthorizationService;

class SubscriptionInvoicesAuthorizationService extends CommonAuthorizationService
{
    public $viewTask   = 'view.subscription_invoices';
    public $createTask = 'create.subscriptions';
    public $updateTask = 'edit.subscriptions';
    public $deleteTask = 'delete.subscriptions';
}
