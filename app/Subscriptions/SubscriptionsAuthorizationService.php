<?php

namespace App\Subscriptions;

use App\Common\CommonAuthorizationService;

class SubscriptionsAuthorizationService extends CommonAuthorizationService
{
    public $viewTask   = 'view.subscriptions';
    public $createTask = 'create.subscriptions';
    public $updateTask = 'edit.subscriptions';
    public $deleteTask = 'delete.subscriptions';
}
