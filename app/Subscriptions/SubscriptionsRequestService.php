<?php

namespace App\Subscriptions;

use App\Authz\AuthzDataScope;
use App\Model\Role;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Request\RequestService;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class SubscriptionsRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    protected function send(Request $request, AuthzDataScope $authzDataScope = null)
    {
        try {
            $response = $this->client->send($request);
            return response()->json($response->getBody()->getContents(), $response->getStatusCode());
        } catch (BadResponseException $e) {
            // we catch 400 and 500 errors from the micro-service, then return as HttpExceptions
            $contents = json_decode($e->getResponse()->getBody()->getContents());
            $statusCode = $e->getResponse()->getStatusCode();
            $message = $e->getResponse()->getReasonPhrase();

            if ($contents && isset($contents->errors) && is_array($contents->errors)) {
                $message = $contents->errors[0]->detail[0];
            }

            throw new HttpException($statusCode, $message, $e);
        }
    }

    /**
     * This endpoint gets the module access based from the passed product code/id
     *
     * @param string $code
     */
    public function getModuleAccess(int $productId = null)
    {
        $idMapping = [
            "PR_1" => Role::TA_MODULE_ACCESS,
            "PR_2" => Role::PAYROLL_MODULE_ACCESS,
            "PR_3" => ROLE::MODULE_ACCESSES
        ];

        if ($productId) {
            return $idMapping["PR_" . (string) $productId];
        }
    }

    /**
     * This endpoint gets the module access based from the passed plan id
     *
     * @param string $code
     */
    public function getModuleAccessByPlanId(int $planId)
    {
        $idMapping = [
            "PL_1" => Role::TA_MODULE_ACCESS,
            "PL_2" => Role::PAYROLL_MODULE_ACCESS,
            "PL_3" => ROLE::MODULE_ACCESSES
        ];

        if (array_key_exists("PL_{$planId}", $idMapping)) {
            return $idMapping["PL_{$planId}"];
        }
    }

    /**
     * This endpoint creates a new customer and subscribes it to the
     * choosen product.
     *
     * @param array $data Query data
     *
     */
    public function createCustomer(array $data)
    {
        $request = new Request(
            'POST',
            "/subscriptions/customer",
            ['Content-Type' => 'application/json'],
            json_encode([
                "data" => $data
            ])
        );
        return $this->send($request);
    }

    /**
     * Get subscriptions by single account
     *
     * @param array $userIds
     * @return array
     */
    public function getSubscriptionsBySingleUserId(int $userId)
    {
        $subscriptionsRequest = $this->getSubscriptionsByUserIds([$userId]);
        $subscriptionsData = json_decode($subscriptionsRequest->getData(), true)['data'];

        return $subscriptionsData[$userId] ?? null;
    }

    /**
     * Get subscriptions by multiple account ids
     *
     * @param array $userIds
     * @return array
     */
    public function getSubscriptionsByUserIds(array $userIds)
    {
        $request = new Request(
            'GET',
            "/subscriptions/users?filter[user_ids]=" . implode(',', $userIds),
            ['Content-Type' => 'application/json']
        );

        return $this->send($request);
    }

    /**
     * Get all licenses by user ids
     *
     * @param array $userIds
     * @return array
     */
    public function getAllLicensesByUsers(array $userIds)
    {
        $request = new Request(
            'GET',
            "/subscriptions/licenses/users?filter[user_ids]=" . implode(',', $userIds),
            ['Content-Type' => 'application/json']
        );

        return $this->send($request);
    }

    /**
     * Get all licenses by user ids
     *
     * @param int $userId
     * @return array
     */
    public function getLicenseByUser(int $userId)
    {
        $licenses = $this->getAllLicensesByUsers([$userId]);
        $licensesData = json_decode($licenses->getData(), true)['data'];

        return $licensesData[$userId] ?? null;
    }

    /**
     * Get assigned license ID for user
     *
     * @param int $userId
     * @return int|null
     */
    public function getAssignedLicenseIdForUser(int $userId)
    {
        $license = $this->getLicenseByUser($userId);

        if ($license) {
            return $license['id'];
        }
    }

    public function applyLicenses(array $userIds, string $productName, string $subscriptionLicenseId = "")
    {
        $paramData = [
            'user_ids'  => $userIds,
            'product_code'  => $productName,
        ];
        if (!empty($subscriptionLicenseId)) {
            $paramData['subscription_license_id'] = $subscriptionLicenseId;
        }
        $query = [
            'data' => $paramData
        ];
        $request = new Request(
            'POST',
            "/subscriptions/licenses",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Updates plan and/or license units for a subscription
     *
     * @param array $params
     * @return array
     */
    public function updateSubscriptionPlan($inputs, $subscriptionId)
    {
        $query = [
            'data' => $inputs
        ];
        $request = new Request(
            'PATCH',
            "/subscriptions/{$subscriptionId}/update",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Updates license product at the owner level
     *
     * @param int $ownerUserId
     * @param string $productCode
     * @param int $subscriptionLicenseId
     * @param int $newUnits
     * @return array
     */
    public function updateOwnerLicenseProduct(
        int $ownerUserId,
        string $productCode,
        int $subscriptionLicenseId,
        int $newUnits = 0
    ) {
        $paramData = [
            'product_code'  => $productCode,
            'subscription_license_id' => $subscriptionLicenseId,
            'new_units' => $newUnits
        ];

        $query = [
            'data' => $paramData
        ];
        $request = new Request(
            'PATCH',
            "/subscriptions/license/product/$ownerUserId",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Assign licenses to specified user ids and product id
     *
     * @param int $ownerUserId The owner user id
     * @param array $assignees The list of users to assign
     * @param string $jobId The job id of a previous job
     * @return array
     */
    public function assign(
        int $ownerUserId,
        array $assignees,
        string $jobId = null
    ) {
        $paramData = [
            'owner_user_id'  => $ownerUserId,
            'assignees' => $assignees
        ];

        if (!empty($jobId)) {
            $paramData["job_id"] = $jobId;
        }

        $query = [
            'data' => $paramData
        ];

        $request = new Request(
            'POST',
            "/subscriptions/transpose/license_units",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Unassign licenses to specified user ids and product id
     *
     * @param int $ownerUserId The owner user id
     * @param array $candidates The list of users to assign
     * @param string $jobId The job id of a previous job
     * @return array
     */
    public function unassign(
        int $ownerUserId,
        array $candidates,
        string $jobId = null
    ) {
        $paramData = [
            'owner_user_id'  => $ownerUserId,
            'candidates' => $candidates
        ];

        if (!empty($jobId)) {
            $paramData["job_id"] = $jobId;
        }

        $query = [
            'data' => $paramData
        ];

        $request = new Request(
            'POST',
            "/subscriptions/transpose/license_units",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Get assign license status
     *
     * @param string $jobId The job id of a previous job
     * @return array
     */
    public function getTransposeStatus(string $jobId)
    {
        $request = new Request(
            "GET",
            "/subscriptions/transpose/status/" . $jobId,
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }
    /**
     * Get assign license status
     *
     * @param string $jobId The job id of a previous job
     * @return array
     */
    public function getSubscriptionProductId(string $productCode)
    {
        $request = new Request(
            "GET",
            "/subscriptions/products?filter[code]=" . $productCode,
            [
                'Content-Type' => 'application/json'
            ]
        );
        return $this->send($request);
    }

    /**
     * Get list of license units owned by a user id
     *
     * @param array $user_ids A list of owner user ids or user ids.
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getLicenseUnits(array $userIds)
    {
        $filter = join(",", $userIds);
        $request = new Request(
            "GET",
            "/subscriptions/license_units?filter[owner_user_id]=$filter",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get list of available subscription license products
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getAvailableProducts()
    {
        $request = new Request(
            "GET",
            "/subscriptions/products",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get customer details including current subscription
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function searchCustomerDetailsByUserId($userId)
    {
        $request = new Request(
            'GET',
            '/subscriptions/customers?'
            . http_build_query(['filter' => ['user_id' => $userId]]),
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get customer details including current subscription
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getCustomerSubscriptionDiscounts($subscriptionId)
    {
        $request = new Request(
            "GET",
            "/subscriptions/{$subscriptionId}/discounts",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get customer details including current subscription
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getCustomerSubscriptionStats($subscriptionId)
    {
        $request = new Request(
            "GET",
            "/subscriptions/{$subscriptionId}/stats",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get customer details by account id
     *
     * @param int $accountId
     * @param array $includes
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getCustomerByAccountId($accountId, array $includes = [])
    {
        $request = new Request(
            "GET",
            "/subscriptions/customers?filter[account_id]=" . $accountId . "&includes=" . implode(',', $includes),
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get product details by ID
     *
     * @param  $productId
     * @return \Illuminate\Http\Response
     * @throws HttpException
     */
    public function getProductById(int $productId)
    {
        $request = new Request(
            'GET',
            "/subscriptions/products/{$productId}"
        );

        return $this->send($request);
    }

    /**
     * Updates an invoice status
     *
     * @param int $invoiceId
     * @param string $status
     * @return array
     */
    public function updateInvoice(
        int $invoiceId,
        string $status
    ) {
        $request = new Request(
            'PATCH',
            "/subscriptions/invoices/$invoiceId?status=$status",
            [
                'Content-Type' => 'application/json'
            ]
        );

        return $this->send($request);
    }

    /**
     * Get subscription update draft
     *
     * @param int $subscriptionId
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getSubscriptionDraft($subscriptionId)
    {
        $request = new Request(
            'GET',
            "/subscriptions/{$subscriptionId}/draft"
        );

        return $this->send($request);
    }

    /**
     * Delete subscription update draft
     *
     * @param int $subscriptionId
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function deleteSubscriptionDraft($subscriptionId)
    {
        $request = new Request(
            'DELETE',
            "/subscriptions/{$subscriptionId}/draft"
        );

        return $this->send($request);
    }

    /**
     * Get plan products given an accountId
     *
     * @param  $productId null Optional parameter
     * @return \Illuminate\Http\Response
     * @throws HttpException
     */
    public function getAccountProducts(int $accountId = null)
    {
        $url = "/subscriptions/account/{$accountId}/plans";
        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request);
    }

    /**
     * Get list of receipts by account id
     *
     * @param array $accountIds    List of account ids to match receipt owners
     * @param array $invoiceIds    List of invoice ids to match receipts
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function searchReceipts(array $accountIds, $invoiceIds = []) : JsonResponse
    {
        $query = (!empty($invoiceIds) )
                ? 'filter[invoice_id]='.implode(',', $invoiceIds)
                : 'filter[account_id]='.implode(',', $accountIds);

        return $this->send(new Request(
            'GET',
            "/subscriptions/receipts?{$query}"
        ));
    }

    /**
     * Get list of invoices by account id
     *
     * @param array $accountIds List of account ids to match receipt owners
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function searchInvoices(array $accountIds) : JsonResponse
    {
        $query = 'filter[account_id]=' . implode(',', $accountIds);

        return $this->send(new Request(
            'GET',
            "/subscriptions/invoices?{$query}"
        ));
    }

    /**
     * Get single invoice by ID
     *
     * @param int $invoiceId Invoice ID
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getInvoiceById(int $invoiceId) : JsonResponse
    {
        return $this->send(new Request(
            'GET',
            "/subscriptions/invoices/{$invoiceId}"
        ));
    }

    /**
     * Get customer billing information
     *
     * @param int $accountId Account ID
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getBillingInformation($accountId) : JsonResponse
    {
        return $this->send(new Request(
            'GET',
            "/subscriptions/customers/account/{$accountId}/billing_information"
        ));
    }

    /**
     * Update or create customer billing information
     *
     * @param int $accountId Account ID
     * @param array $customerBillingInfo Account billing information data
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function setBillingInformation($accountId, array $customerBillingInfo) : JsonResponse
    {
        $data = [
            'data' => $customerBillingInfo
        ];

        return $this->send(new Request(
            'PATCH',
            "/subscriptions/customers/account/{$accountId}/billing_information",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Get Invoice Paynamics Payment Details
     *
     * @param int $invoiceId
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function getInvoicePaynamicsPaymentDetails($invoiceId)
    {
        return $this->send(new Request(
            'GET',
            "/subscriptions/invoices/{$invoiceId}/payments/generate_paynamics"
        ));
    }

    /**
     * Create invoice PayPal order
     *
     * @param int $invoiceId Invoice ID
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function createPayPalOrder($invoiceId) : JsonResponse
    {
        return $this->send(new Request(
            'POST',
            "/subscriptions/invoices/{$invoiceId}/payments/paypal/orders",
            [
                'Content-Type' => 'application/json'
            ]
        ));
    }

    /**
     * Capture invoice PayPal order
     *
     * @param int $invoiceId Invoice ID
     * @param string $orderId Order ID
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function capturePayPalOrder($invoiceId, $orderId) : JsonResponse
    {
        return $this->send(new Request(
            'POST',
            "/subscriptions/invoices/{$invoiceId}/payments/paypal/capture/{$orderId}",
            [
                'Content-Type' => 'application/json'
            ]
        ));
    }

    /**
     * Get list of billed users under an invoice
     *
     * @param int $invoiceId
     * @return JsonResponse
     */
    public function getBilledUsers($invoiceId) : JsonResponse
    {
        return $this->send(new Request(
            'GET',
            "/subscriptions/invoices/{$invoiceId}/billed_users"
        ));
    }

    /**
     * This endpoint update the customer email
     *
     * @param int $id Customer ID
     * @param array $data Query data
     *
     */
    public function updateCustomer(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/subscriptions/customer/{$id}",
            ['Content-Type' => 'application/json'],
            json_encode([
                "data" => $data
            ])
        );
        return $this->send($request);
    }
}
