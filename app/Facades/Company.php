<?php

namespace App\Facades;

use App\Company\CompanyRequestService;
use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Company\CompanyRequestService
 */
class Company extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CompanyRequestService::class;
    }
}
