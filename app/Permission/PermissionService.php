<?php

namespace App\Permission;

use App\Model\Permission;

class PermissionService
{
    /**
     * Create permissions.
     *
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return Permission::create($attributes);
    }

    /**
     * Return role permissions.
     *
     * @param int $roleId
     * @return array|\Illuminate\Support\Collection
     */
    public function getRolePermissions(int $roleId)
    {
        $permissions = Permission::where('role_id', $roleId);
        $results = [];
        $permissions->each(function ($permission) use (&$results) {
            $results[] = [
                'id' => $permission->task->id,
                'display_name' => $permission->task->display_name,
                'ess' => $permission->task->ess,
                'module' => $permission->task->module,
                'submodule' => $permission->task->submodule,
                'scope' => $permission->scope,
                'name' => $permission->task->name
            ];
        });
        $results = collect($results)->groupBy('module');
        $results->transform(function ($item) {
            $result = [];
            $firstItem = $item->first();
            $submodules = collect($item)->groupBy('submodule');
            $submodules->transform(function ($submoduleItem) {
                $submoduleFirstItem = $submoduleItem->first();
                $submodulePermissions = collect($submoduleItem);
                $submodulePermissions->transform(function ($permission) {
                    return [
                        'id' => $permission['id'],
                        'display_name' => $permission['display_name'],
                        'scopes' => $permission['scope'],
                        'name' => $permission['name'],
                    ];
                });
                return [
                    'name' => $submoduleFirstItem['submodule'],
                    'tasks' => $submodulePermissions->toArray()
                ];
            });
            $result['module'] = [
                'name' => $firstItem['module'],
                'ess' => (boolean)$firstItem['ess'],
                'submodules' => array_values($submodules->toArray()),
            ];
            return $result;
        });

        $results = array_values($results->toArray());
        return $results;
    }
}
