<?php

namespace App\Permission;

class AuthorizationPermissions
{
    /**
    * array
    */
    protected $tasks = [];

    public function addTaskScope(TaskScopes $taskScopes)
    {
        if (!isset($this->tasks[$taskScopes->getTaskName()])) {
            $this->tasks[$taskScopes->getTaskName()] = $taskScopes;
        }
        $this->mergeTaskScopesOfSameTasks($taskScopes->getTaskName(), $taskScopes);
    }

    public function getTaskScopesForTask(string $targetTask)
    {
        foreach ($this->tasks as $task => $taskScopes) {
            if ($task === $targetTask) {
                return $taskScopes;
            }
        }
        return null;
    }

    public function toArray()
    {
        $tasks = [];

        foreach ($this->tasks as $task) {
            $tasks += $task->toArray();
        }

        return $tasks;
    }

    protected function mergeTaskScopesOfSameTasks(
        string $task,
        TaskScopes $newTaskScope
    ) {
        $currentTaskScope = $this->tasks[$task];
        foreach ($newTaskScope->getScopes() as $newScope) {
            $currentTaskScope->addScope($newScope);
        }
        $this->tasks[$task] = $currentTaskScope;
    }
}
