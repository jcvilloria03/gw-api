<?php

namespace App\Permission;

class Scope
{
    const TARGET_ALL = 'all';

    protected $type;

    protected $target;

    /**
     * Scope constructor.
     * @param string $type
     * @param mixed $target
     */
    public function __construct(string $type, $target)
    {
        $this->type = $type;
        $this->setTarget($target);
    }

    public function targetAll()
    {
        return $this->target === self::TARGET_ALL;
    }

    public function inScope($id)
    {
        return $this->targetAll() || in_array($id, $this->target);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget($target)
    {
        if ($target != self::TARGET_ALL && !is_array($target)) {
            throw new \InvalidArgumentException('Invalid Target');
        }
        $this->target = $target;
    }

    public function toArray()
    {
        return [
            'type' => $this->type,
            'target' => $this->target,
        ];
    }
}
