<?php

namespace App\Permission;

class TaskScopes
{
    /**
    * string
    */
    protected $taskName;

    /**
    * array
    */
    protected $scopes = [];

    /**
     * @var array
     */
    protected $moduleAccess = [];

    public function __construct(string $taskName)
    {
        $this->taskName = $taskName;
    }

    public function getTaskName()
    {
        return $this->taskName;
    }


    public function addScope(Scope $scope)
    {
        if (!isset($this->scopes[$scope->getType()])) {
            $this->scopes[$scope->getType()] = $scope;
        }
        $this->mergeTargetsOfSameScopeType($scope->getType(), $scope);
    }

    public function getScopes()
    {
        return $this->scopes;
    }

    public function getScopeBasedOnType(string $type)
    {
        foreach ($this->scopes as $scope) {
            if ($scope->getType() === $type) {
                return $scope;
            }
        }
        return null;
    }

    /**
     * Add module access to task scope
     *
     * @param array $moduleAccess
     * @return void
     */
    public function addModuleScope(array $moduleAccess)
    {
        $this->moduleAccess = !empty($this->moduleAccess)
            ? array_intersect($this->moduleAccess, $moduleAccess)
            : $moduleAccess;
    }

    public function getModuleScopes()
    {
        return $this->moduleAccess;
    }

    /**
     * Transform object to array
     *
     * @return array
     */
    public function toArray()
    {
        $result = [
            $this->taskName => [
                'scopes'  => [],
                'modules' => $this->moduleAccess
            ]
        ];

        foreach ($this->scopes as $scope) {
            $result[$this->taskName]['scopes'][] = $scope->toArray();
        }

        return $result;
    }

    protected function mergeTargetsOfSameScopeType(string $type, Scope $newScope)
    {
        $currentScope = $this->scopes[$type];

        if (
            $currentScope->targetAll() ||
            $newScope->targetAll()
        ) {
            $currentScope->setTarget(SCOPE::TARGET_ALL);
        } else {
            $mergedTargets = array_merge($currentScope->getTarget(), $newScope->getTarget());
            $currentScope->setTarget(array_values(array_unique($mergedTargets)));
        }

        // saved the merged targets
        $this->scopes[$type] = $currentScope;
    }
}
