<?php

namespace App\Permission;

class TargetType
{
    const ACCOUNT = 'Account';
    const COMPANY = 'Company';
    const PAYROLL_GROUP = 'Payroll Group';
    const TEAM = 'Team';

    const TARGET_TYPES = [
        self::ACCOUNT,
        self::COMPANY,
        self::PAYROLL_GROUP,
        self::TEAM,
    ];
}
