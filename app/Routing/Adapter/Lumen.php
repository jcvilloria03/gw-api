<?php

namespace App\Routing\Adapter;

use Dingo\Api\Routing\Adapter\Lumen as LumenAdapter;
use ReflectionClass;

class Lumen extends LumenAdapter
{
    protected function removeMiddlewareFromApp()
    {
        if ($this->middlewareRemoved) {
            return;
        }
        $this->middlewareRemoved = true;
        $reflection = new ReflectionClass($this->app);
        $property = $reflection->getProperty('middleware');
        $property->setAccessible(true);
        $oldMiddlewares = $property->getValue($this->app);
        $newMiddlewares = [];
        foreach ($oldMiddlewares as $middle) {
            if ((new ReflectionClass($middle))->hasMethod('terminate')
                && $middle != 'Dingo\Api\Http\Middleware\Request'
            ) {
                $newMiddlewares = array_merge($newMiddlewares, [$middle]);
            }
        }
        $property->setValue($this->app, $newMiddlewares);
        $property->setAccessible(false);
    }
}
