<?php

namespace App\Employee;

use App\Authorization\EssBaseAuthorizationService;

class EssEmployeeAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.profile';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeGet(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
