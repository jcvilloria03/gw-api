<?php

namespace App\Employee;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EmployeeRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Data to be saved in Employee upload job
     *
     * @param string $id Job ID
     * @return json Company information
     *
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/employee/upload_preview?job_id={$jobId}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Data to be saved in Employee upload job
     *
     * @param string $id Job ID
     * @return json Company information
     *
     */
    public function createPerson(array $attributes, $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/people",
            ['Content-Type' => 'application/json'],
            json_encode($attributes)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Fetch employees for given company id
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @return json List of Employees of given company
     *
     */
    public function getCompanyEmployees(int $id, array $data, array $authzDataScope = [])
    {
        $url = "/company/{$id}/employees?" . http_build_query($data);
        if (!empty($authzDataScope)) {
            $url .= (empty($data) ? "" : "&") . "authz_data_scope=" . json_encode($authzDataScope);
        }

        $request = new Request(
            'GET',
            "/company/{$id}/employees?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Fetch employees for given company id
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Employees of given company
     *
     */
    public function getCompanyInactiveEmployees(int $id, array $data, $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/inactive_employees",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Fetch employees for given company ID, filtered by name or ID
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @return json List of Employees of given company filtered by name or ID
     *
     */
    public function getCompanyEmployeesFilteredByNameOrId(int $id, array $data)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/employees/search?" . http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Fetch employees for given company ID, filtered by ID
     *
     * @param int $id Company ID
     * @param array $employeeIds Query data
     * @return json List of Employees of given company filtered by ID
     *
     */
    public function getCompanyEmployeesById(int $companyId, array $employeeIds)
    {
        $ids = ['values' => implode(',', $employeeIds)];

        $request = new Request(
            'POST',
            "/company/{$companyId}/employees/id",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($ids)
        );

        return $this->send($request);
    }

    /**
     * Fetch employees for given company ID, filtered by ID
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @return json List of Employees of given company filtered by IDs
     *
     */
    public function getCompanyTaEmployeesByIds(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/ta_employees/id",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Fetch employees with time and attendance for given company id
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @return json List of Employees of given company
     *
     */
    public function getCompanyTaEmployees(int $id, array $data, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/ta_employees?" . http_build_query($data)
        );
        return $this->send($request, $dataScope);
    }

    /**
     * Fetch employees with time and attendance for given company id
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Employees of given company
     */
    public function getCompanyEmployeesByIds(
        int $id,
        array $ids,
        string $mode = 'DEFAULT',
        array $include = ['payroll', 'time_attendance'],
        $authzDataScope = null
    ) {
        $request = new Request(
            'POST',
            "/company/{$id}/employees_by_ids",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'ids' => $ids,
                'mode' => $mode,
                'include' => $include
            ])
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Fetch employee with time & attendance or payroll or both for given employee id
     *
     * @param int $id Employee ID
     * @return json Employee information
     *
     */
    public function getEmployeeInfo(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Fetch employees with time and attendance for given company id and by first, middle and last name
     *
     * @param int $id Company ID
     * @param array $data request data
     * @return json List of Employees of given company for given first, middle and last name
     *
     */
    public function searchCompanyTaEmployees(int $id, array $data)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/ta_employees/search",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Fetch employee for given internal employee id
     *
     * @param int $id Employee ID
     * @return json Employee information
     *
     */
    public function getEmployee(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Update Employee details
     *
     * @param int $id Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/philippine/employee/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Update Payroll Employee details
     *
     * @param int $id Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTAPayrollEmployee(int $id, array $data, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'PUT',
            "/employee/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Create Philippine Employee
     *
     * @param int $id Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPhilippineEmployee(array $data, $dataScope = null)
    {
        $request = new Request(
            'POST',
            '/philippine/employee',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        if ($dataScope instanceof AuthzDataScope) {
            return $this->send($request, $dataScope);
        }

        return $this->send($request);
    }

    /**
     * Create Payroll Employee
     *
     * @param int $id Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTAPayrollEmployee(array $data, $dataScope = null)
    {
        $request = new Request(
            'POST',
            '/employee',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );

        if ($dataScope instanceof AuthzDataScope) {
            return $this->send($request, $dataScope);
        }

        return $this->send($request);
    }

    public function generateEmployeeMasterfile($companyId, array $payload, $dataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/employees/generate_masterfile",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($payload)
        );

        if ($dataScope instanceof AuthzDataScope) {
            return $this->send($request, $dataScope);
        }

        return $this->send($request);
    }

    /**
     * Get company time and attendance employees by attribute values
     *
     * @param int|string $companyId
     * @param string $attribute
     * @param array $values
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompanyTaEmployeesByAttribute($companyId, string $attribute, array $values)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/ta_employees/{$attribute}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($values)
        );

        return $this->send($request);
    }

    /**
     * Get company employees by attribute values
     *
     * @param int|string $companyId
     * @param string $attribute
     * @param array $values
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCompanyEmployeesByAttribute($companyId, string $attribute, array $values)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/employees/{$attribute}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($values)
        );

        return $this->send($request);
    }

    /**
     * Update Employee details
     *
     * @param int $id Employee ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateEmployee(int $id, array $data)
    {
        $request = new Request(
            'PATCH',
            "/employee/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Get Sorted Employee Ids collection
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSortedCompanyEmployees(int $id, array $data)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/sorted_employees",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch employee events for a given month
     *
     * @param array $params Params (Month, Group)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEventsForEmployees(array $params, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            '/employee/events',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($params)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call company service to generate employee 201s
     *
     * @param int $companyId
     * @param array $params
     * @return \Illuminate\Http\JsonResponse
     */
    public function exportEmployee201s($companyId, array $params, AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/employees/export",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($params)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Get the number of Active Employees, per Company
     *
     * @param int $companyId Company ID
     * @return json
     *
     */
    public function getActiveEmployeesCount(int $companyId, AuthzDataScope $authzDataScope)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/active_employees_count"
        );
        return $this->send($request, $authzDataScope);
    }

        /**
     * Fetch employees with time and attendance for given company id
     *
     * @param int $id Company ID
     * @param array $data Query data
     * @param AuthzDataScope|null $authzDataScope
     * @return json List of Employees of given company
     */
    public function getCompanyEmployeesByEmployeeIds(
        int $id,
        array $ids,
        string $mode = 'DEFAULT',
        array $include = ['payroll', 'time_attendance'],
        $authzDataScope = null
    ) {
        $request = new Request(
            'POST',
            "/company/{$id}/employees_by_ids",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'uids' => $ids,
                'mode' => $mode,
                'include' => $include
            ])
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to upload Employee picture
     *
     * @param array $employee Employee information
     * @param int $id Employee ID
     * @return json Employee information
     *
     */
    public function uploadEmployeePic(array $employee, $id)
    {
        return $this->sendMultipart(
            'POST',
            "/employee/upload/picture/$id",
            $employee
        );
    }

    /**
     * Validate employee payroll details
     *
     * @param array $params Employee payroll data
     * @return \Illuminate\Http\JsonResponse Job id
     *
     */
    public function getEmployeesForPayrollValidation(array $params)
    {
        $request = new Request(
            'POST',
            "employee/employees_for_payroll_validation",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($params)
        );
        return $this->send($request);
    }

    /**
     * Validate employee payroll details
     *
     * @param string $jobId
     * @return \Illuminate\Http\JsonResponse job status
     *
     */
    public function getEmployeesForPayrollValidationStatus($jobId)
    {
        $request = new Request(
            'GET',
            "employee/employees_for_payroll_validation/{$jobId}",
            [
                'Content-Type' => 'application/json'
            ]
        );
        return $this->send($request);
    }

    /**
     * Fetch employees for given company id
     *
     * @param int $id Company ID
     * @return json List of Employees of given company
     *
     */
    public function getCompanyEmployeesSimple(int $id)
    {
        $request = new Request(
            'GET',
            "/company/{$id}/employees?mode=simple&status=active"
        );
        return $this->send($request);
    }
    
}
