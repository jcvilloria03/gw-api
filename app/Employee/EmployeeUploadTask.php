<?php

namespace App\Employee;

use App\Model\Auth0User;
use App\Tasks\UploadTask;
use App\Auth0\Auth0UserService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use App\CompanyUser\CompanyUserService;

class EmployeeUploadTask extends UploadTask
{
    const PROCESS_PERSONAL = 'personal';
    const PROCESS_PEOPLE = 'people';

    const PROCESS_PAYROLL = 'payroll';
    const PROCESS_SAVE = 'save';

    const PROCESS_TIME_ATTENDANCE = 'time_attendance';
    const PROCESS_TA_SAVE = 'ta_save';

    const VALID_PROCESSES = [
        self::PROCESS_PERSONAL,
        self::PROCESS_PAYROLL,
        self::PROCESS_TIME_ATTENDANCE,
        self::PROCESS_SAVE,
        self::PROCESS_TA_SAVE,
        self::PROCESS_PEOPLE,
    ];

    const VALIDATION_PROCESSES = [
        self::PROCESS_PERSONAL,
        self::PROCESS_PAYROLL,
        self::PROCESS_TIME_ATTENDANCE,
        self::PROCESS_PEOPLE
    ];

    const ID_PREFIX = 'employee_upload:';

    const REDIS_EXPIRY = 1800;

    /**
     * Company Id
     *
     * @var int
     */
    protected $companyId;

    /**
     * Create task in Redis
     *
     * @param int $companyId Company Id
     * @param string $id Job id
     *
     */
    public function create(int $companyId, string $id = null)
    {
        $this->companyId = $companyId;
        if (empty($id)) {
            // create a new task and create key in Redis
            $this->id = $this->generateId();
            $this->currentValues = [
                's3_bucket' => $this->s3Bucket
            ];
            $this->createInRedis();
        } else {
            $this->id = $id;
            if (!$this->isCompanyIdSame()) {
                // Job::Company Id does not match $this->companyId
                throw new EmployeeUploadTaskException('Job does not belong to given Company ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            // invalid job
            throw new EmployeeUploadTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    protected function generateId()
    {
        return static::ID_PREFIX . $this->companyId . ':' . uniqid();
    }

    /**
     * Check if companyId and companyId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     *
     */
    public function isCompanyIdSame()
    {
        // check job.company_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->companyId . '/';
        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Save Personal Info
     *
     * @param string $path Full path of file to upload
     *
     */
    public function savePersonalInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key(self::PROCESS_PERSONAL);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('personal_s3_key', $s3Key);
        $this->updateValidationStatus(
            self::PROCESS_PERSONAL,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Save People Info
     *
     * @param string $path Full path of file to upload
     *
     */
    public function savePeopleInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key(self::PROCESS_PEOPLE);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('people_s3_key', $s3Key);
        $this->updateValidationStatus(
            self::PROCESS_PEOPLE,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Save Payroll Info
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     *
     */
    public function savePayrollInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key(self::PROCESS_PAYROLL);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('payroll_s3_key', $s3Key);
        $this->updateValidationStatus(
            self::PROCESS_PAYROLL,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Save Time and Attendance Info
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     *
     */
    public function saveTimeAttendanceInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key(self::PROCESS_TIME_ATTENDANCE);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('time_attendance_s3_key', $s3Key);
        $this->updateValidationStatus(
            self::PROCESS_TIME_ATTENDANCE,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @param string $process
     *
     */
    protected function generateS3Key(string $process)
    {
        return 'employee_upload_' .
               $process .
               ':' .
               $this->companyId .
               ':' .
               uniqid();
    }

    /**
     * Update validation status
     *
     * @param string $process The process to update status
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateValidationStatus(string $process, string $newStatus)
    {
        if (!in_array($process, self::VALIDATION_PROCESSES)) {
            throw new EmployeeUploadTaskException('Invalid Process');
        }
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new EmployeeUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[$process . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal($process . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     *
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new EmployeeUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        if (isset($this->currentValues[self::PROCESS_PAYROLL . '_status'])) {
            // check if PROCESS_PAYROLL is already validated
            if (
                !isset($this->currentValues[self::PROCESS_PAYROLL . '_status']) ||
                $this->currentValues[self::PROCESS_PAYROLL . '_status'] !== self::STATUS_VALIDATED
            ) {
                throw new EmployeeUploadTaskException(
                    'Payroll information needs to be validated successfully first'
                );
            }
        } else if (
            !isset($this->currentValues[self::PROCESS_PEOPLE . '_status']) ||
            $this->currentValues[self::PROCESS_PEOPLE . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new EmployeeUploadTaskException(
                'Employee information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     *
     */
    public function updateTaSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new EmployeeUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_TIME_ATTENDANCE is already validated
        if (
            !isset($this->currentValues[self::PROCESS_TIME_ATTENDANCE . '_status']) ||
            $this->currentValues[self::PROCESS_TIME_ATTENDANCE . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new EmployeeUploadTaskException(
                'Time and attendance information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_TA_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_TA_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Create Preactive auth0 users and companies_users records
     * for created employees
     *
     * @param array $employees
     * @return void
     */
    public function createUserRecords(array $employees)
    {
        $companyUserService = App::make(CompanyUserService::class);

        foreach ($employees as $employee) {
            $companyUser = $companyUserService->getByUserId($employee['user_id']);
            if (empty($companyUser)) {
                $companyUser = $companyUserService->create([
                    'user_id'     => $employee['user_id'],
                    'company_id'  => $employee['company_id'],
                    'employee_id' => $employee['id']
                ]);
            }
        }
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return (
            !in_array(
                $currentStatus,
                [
                    self::STATUS_VALIDATING,
                    self::STATUS_SAVING,
                ]
            )
        );
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATION_QUEUED || $currentStatus === self::STATUS_SAVE_QUEUED;
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     *
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATING || $currentStatus === self::STATUS_SAVING;
    }

    /**
     * Update Error File Location
     *
     * @param string $process The new status of the saving process
     * @param string $errorFileS3Key The S3 key of the error file
     *
     */
    public function updateErrorFileLocation(string $process, string $errorFileS3Key)
    {
        if (
            !in_array($process, self::VALIDATION_PROCESSES) &&
            $process !== self::PROCESS_SAVE &&
            $process !== self::PROCESS_TA_SAVE
        ) {
            throw new EmployeeUploadTaskException('Invalid Process');
        }
        $this->setVal($process . '_error_file_s3_key', $errorFileS3Key);
    }

    /**
     * @Override \App\Tasks\Task::createInRedis
     *
     * Create task in Redis
     *
     */
    protected function createInRedis()
    {
        $res = Redis::hMSet($this->id, $this->currentValues);
        Redis::expire($this->id, self::REDIS_EXPIRY);

        return $res;
    }
}
