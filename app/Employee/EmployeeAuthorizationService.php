<?php

namespace App\Employee;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class EmployeeAuthorizationService extends AuthorizationService
{
    const CREATE_TASK = 'create.employee';
    const VIEW_TASK = 'view.employee';
    const UPDATE_TASK = 'edit.employee';

    /**
     * Authorize employee related tasks
     *
     * @param \stdClass $company
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $company,
        array $user,
        string $taskType
    ) {
        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for company's account
            return $accountScope->inScope($company->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as company's account
                return $company->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($company->id);
        }

        return false;
    }

    /**
     * @param \stdClass $company Company of the employee
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $company Company of the employees
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company Company of the employees
     * @param array $user
     * @return bool
     */
    public function authorizeViewCompanyEmployees(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $company Company of the employee
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::UPDATE_TASK);
    }
}
