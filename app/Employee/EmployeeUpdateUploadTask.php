<?php

namespace App\Employee;

use App\Model\Role;
use App\Model\Auth0User;
use App\Role\UserRoleService;
use App\Auth0\Auth0UserService;
use App\Role\DefaultRoleService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\CompanyUser\CompanyUserService;
use App\ProductSeat\ProductSeatService;
use App\Exceptions\Auth0BatchCreateJobException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use App\Authn\AuthnService;
use App\Model\AuthnUser;

class EmployeeUpdateUploadTask extends EmployeeUploadTask
{
    const STATUS_UPDATING_ACCOUNTS_STATUS = 'updating_accounts_status';
    const STATUS_DEACTIVATING_ACCOUNTS    = 'deactivating_accounts';
    const PROCESS_ACCOUNT_PROCESSING      = 'account_processing';

    const VALID_PROCESSES = [
        self::PROCESS_PERSONAL,
        self::PROCESS_PAYROLL,
        self::PROCESS_TIME_ATTENDANCE,
        self::PROCESS_ACCOUNT_PROCESSING,
        self::PROCESS_PEOPLE
    ];

    const VALIDATION_PROCESSES = [
        self::PROCESS_PERSONAL,
        self::PROCESS_PAYROLL,
        self::PROCESS_TIME_ATTENDANCE,
        self::PROCESS_PEOPLE
    ];

    const SAVE_STATUSES = [
        self::STATUS_SAVE_QUEUED,
        self::STATUS_SAVING,
        self::STATUS_SAVED,
        self::STATUS_SAVE_FAILED,
        self::STATUS_UPDATING_ACCOUNTS_STATUS
    ];

    const ID_PREFIX = 'employee_update_upload:';

    /**
     * Personal OR Payroll info is being updated
     */
    protected $process;

    /**
     * Get process
     *
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Generate Task Id, to be used as Redis Key
     *
     */
    protected function generateId()
    {
        return static::ID_PREFIX .
            $this->companyId . ':' . uniqid();
    }

    /**
     * Create task in Redis
     *
     * @param int $companyId Company Id
     * @param string $id Job id
     * @param process $process Updating personal or payroll info
     *
     *
     */
    public function create(int $companyId, string $id = null, string $process = null)
    {
        if (
            empty($id) &&
            !in_array($process, self::VALIDATION_PROCESSES)
        ) {
            throw new EmployeeUploadTaskException('Invalid Process');
        }
        parent::create($companyId, $id);
        if (empty($id)) {
            $this->process = $process;
            $this->setVal('process', $process);
        } else {
            $this->process = $this->currentValues['process'];
        }
    }

    /**
     * Save Update Info
     *
     * @param string $path Full path of file to upload
     *
     */
    public function saveInfo(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key($this->process);
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('s3_key', $s3Key);
        $this->updateValidationStatus(
            $this->process,
            self::STATUS_VALIDATION_QUEUED
        );

        return $s3Key;
    }

    /**
     * Save Personal Info
     *
     * Override parent function so that saveInfo() is always used instead
     *
     * @param string $path Full path of file to upload
     *
     */
    public function savePersonalInfo(string $path)
    {
        return $this->saveInfo($path);
    }

    /**
     * Save Payroll Info
     *
     * Override parent function so that saveInfo() is always used instead
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     *
     */
    public function savePayrollInfo(string $path)
    {
        return $this->saveInfo($path);
    }

    /**
     * Save Time & Attendance Info
     *
     * Override parent function so that saveInfo() is always used instead
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     *
     */
    public function saveTimeAttendanceInfo(string $path)
    {
        return $this->saveInfo($path);
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @param string $process
     *
     */
    protected function generateS3Key(string $process)
    {
        return 'employee_update_upload_' .
               $process .
               ':' .
               $this->companyId .
               ':' .
               uniqid();
    }

    /**
     * Update status
     *
     * @param string $process The process to update status
     * @param string $newStatus The new status of the current process
     *
     */
    public function updateValidationStatus(string $process, string $newStatus)
    {
        if (!in_array($process, self::VALIDATION_PROCESSES)) {
            throw new EmployeeUploadTaskException('Invalid Process');
        }
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new EmployeeUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[$process . '_status'] ?? null;

        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal($process . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     *
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new EmployeeUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if upload data is already validated
        if (
            !isset($this->currentValues[$this->process . '_status']) ||
            $this->currentValues[$this->process . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new EmployeeUploadTaskException(
                'Employee information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;

        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);
        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Update Error File Location
     *
     * @param string $process The new status of the saving process
     * @param string $errorFileS3Key The S3 key of the error file
     *
     */
    public function updateErrorFileLocation(string $process, string $errorFileS3Key)
    {
        $this->setVal($process . '_error_file_s3_key', $errorFileS3Key);
    }

    /**
     * Activate employees
     *
     * @param  int    $accountId
     * @param  int    $companyId
     * @param  array  $forActivationEmployeeIds
     * @return void
     */
    public function activateEmployees(
        int $accountId,
        array $forActivationEmployees
    ) {
        $auth0UserService   = App::make(Auth0UserService::class);
        $companyUserService = App::make(CompanyUserService::class);

        $preactiveUsers = [];

        foreach ($forActivationEmployees as $employee) {
            $auth0User   = $auth0UserService->getUserByEmployeeId($employee['id']);
            $companyUser = $companyUserService->getByEmployeeId($employee['id']);

            // #491 backwards compatibility
            if (empty($companyUser)) {
                $companyUser = $companyUserService->create([
                    'user_id'     => $employee['user_id'],
                    'company_id'  => $employee['company_id'],
                    'employee_id' => $employee['id']
                ]);
            }

            // #491 backwards compatibility
            if (empty($auth0UserService->getUser($employee['user_id']))) {
                $auth0User = $auth0UserService->create([
                    'auth0_user_id' => Auth0UserService::generatePreactiveId(
                        $employee['account_id'],
                        $companyUser->user_id
                    ),
                    'user_id'       => $companyUser->user_id,
                    'account_id'    => $employee['account_id'],
                    'status'        => Auth0User::STATUS_INACTIVE
                ]);
            }

            // #491 Backwards compatibility
            $this->assignDefaultEmployeeRolesToUser(
                $auth0User,
                $employee['account_id'],
                $this->buildAssignedCompanies($employee)
            );

            if (Auth0UserService::isPreactive($auth0User)) {
                $preactiveUsers[] = [
                    'attributes' => [
                        'email'           => $employee['email'],
                        'first_name'      => $employee['first_name'],
                        'last_name'       => $employee['last_name'],
                        'middle_name'     => $employee['middle_name'],
                        'company_user_id' => $companyUser->user_id,
                        'active'          => true
                    ],

                    'auth0_user' => $auth0User
                ];
            }
        }

        if (!empty($preactiveUsers)) {
            try {
                return $auth0UserService->batchCreateEmployeeUserInManagement($accountId, $preactiveUsers);
            } catch (ClientException $e) {
                Log::error('ClientException in batchCreateEmployeeUserInManagement: ' . $e->getMessage(), [
                    'exception' => $e
                ]);

                if ($e->getResponse() && $e->getResponse()->getStatusCode() == Response::HTTP_TOO_MANY_REQUESTS) {
                    throw new Auth0BatchCreateJobException($e->getMessage());
                }
            }
        }
    }

    /**
     * Activate Authn employees
     *
     * @param  array  $forActivationEmployeeIds
     * @return void
     */
    public function activateAuthnEmployees(
        array $forActivationEmployees
    ) {
        $authnService   = App::make(AuthnService::class);
        $companyUserService = App::make(CompanyUserService::class);

        foreach ($forActivationEmployees as $employee) {
            $companyUser = $companyUserService->getByEmployeeId($employee['id']);
            $authnUser   = $authnService->getUser($employee['user_id']);

            // #491 backwards compatibility
            if (empty($companyUser)) {
                $companyUser = $companyUserService->create([
                    'user_id'     => $employee['user_id'],
                    'company_id'  => $employee['company_id'],
                    'employee_id' => $employee['id']
                ]);
            }

            if (empty($authnUser)) {
                $authnUser = $authnService->createAuthnUser([
                    'user_id' => $companyUser->user_id,
                    'account_id' => $employee['account_id'],
                    'company_id' => $employee['company_id'],
                    'employee_id' => $employee['id'],
                    'authn_user_id' => $employee['authn_user_id'],
                    'status' => AuthnUser::STATUS_ACTIVE
                ]);
            } else {
                $authnUser = $authnService->activateUserByuserId($companyUser->user_id);
            }
        }
    }

    /**
     * Build list of assigned companies for the user
     *
     * @param array  $employeeData
     * @return array
     */
    private function buildAssignedCompanies(array $employeeData) : array
    {
        $assignedCompanies[] = [
            'company_id'  => $employeeData['company_id'],
            'employee_id' => $employeeData['employee_id']
        ];

        return $assignedCompanies;
    }

    /**
     * Assigns default employee roles to given auth0User
     *
     * @param  \App\Model\Auth0User $auth0User
     * @param  int                  $accountId
     * @param  array                $assignedCompanies
     * @return void
     */
    private function assignDefaultEmployeeRolesToUser(
        Auth0User $auth0User,
        $accountId,
        array $assignedCompanies
    ) {
        $defaultRoleService = App::make(DefaultRoleService::class);

        // Assign defualt roles per assigned company
        foreach ($assignedCompanies as $assignedCompany) {
            // get default system role for employee
            $defaultRole = $defaultRoleService->getEmployeeSystemRole(
                $accountId,
                $assignedCompany['company_id']
            );

            $this->assignRoleToUser($auth0User, $defaultRole);
        }
    }

    /**
     * Assigns given role to user if not yet assigned
     *
     * @param  \App\Model\Auth0User $auth0User
     * @param  \App\Model\Role      $role
     * @return void
     */
    private function assignRoleToUser(Auth0User $auth0User, Role $role)
    {
        $userRoleService = App::make(UserRoleService::class);

        // Check if employee default role is not yet set for user
        $userRoles = $userRoleService->getWithConditions([
            ['user_id', $auth0User->user_id],
            ['role_id', $role->id]
        ]);

        if ($userRoles->isEmpty()) {
            $userRoleService->create([
                'role_id' => $role->id,
                'user_id' => $auth0User->user_id,
            ]);
        }
    }
}
