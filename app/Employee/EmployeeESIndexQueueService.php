<?php

namespace App\Employee;

use App\ES\ESIndexQueueService;

class EmployeeESIndexQueueService
{
    const TYPE = 'employee';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created employees for indexing
     * @param array $employeeIds Employee Ids to be indexed
     * @return void
     */
    public function queue(array $employeeIds)
    {
        $details = [
            'id' => $employeeIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
