<?php

namespace App\Employee;

use App\Audit\AuditService;
use App\Audit\AuditItem;
use App\Audit\AuditCacheItem;
use App\Audit\AuditUser;
use Illuminate\Support\Facades\Log;
use App\Facades\ArrayHelper;
use Illuminate\Support\Facades\Redis;

class EmployeeAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_EDIT = 'update';
    const OBJECT_NAME = 'employee';

    /*
     * App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        EmployeeRequestService $employeeRequestService,
        AuditService $auditService
    ) {
        $this->employeeRequestService = $employeeRequestService;
        $this->auditService = $auditService;
    }

    /**
     * Log Employee related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_EDIT:
                $this->logEdit($cacheItem);
                break;
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
        }
    }

    /**
     * Log Employee Edit
     *
     * @param array $cacheItem
     *
     */
    public function logEdit(array $cacheItem)
    {
        $employeeDataOld = json_decode($cacheItem['old'], true);
        $employeeDataNew = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        // get old and new values only for changed keys
        $oldData = ArrayHelper::arrayDiffAssocRecursive($employeeDataOld, $employeeDataNew);
        $newData = ArrayHelper::arrayDiffAssocRecursive($employeeDataNew, $employeeDataOld);

        if (!empty($oldData)) {
            $item = new AuditItem([
                'company_id' => $employeeDataOld['company_id'],
                'account_id' => $user['account_id'],
                'user_id' => $user['id'],
                'action' => self::ACTION_EDIT,
                'object_name' => self::OBJECT_NAME,
                'data' => [
                    'employee_id' => $employeeDataOld['id'],
                    'old' => $oldData,
                    'new' => $newData
                ]
            ]);
            $this->auditService->log($item);
        }
    }

    /**
     * Log Employee Creation
     *
     * @param string $jobId
     * @param array $employeeIds
     * @param int $userId
     * @param int $companyId
     *
     */
    public function cacheEmployeesBeforeUpdate(
        string $jobId,
        array $employeeIds,
        int $userId,
        int $companyId
    ) {
        if (empty($employeeIds)) {
            // Skip request if there are no updated employees
            return;
        }

        $employeesResponse = $this->employeeRequestService->getCompanyEmployeesByIds($companyId, $employeeIds);
        $employees = json_decode($employeesResponse->getData(), true)['data'];

        foreach ($employees as $employeeData) {
            try {
                $details = [
                    'old' => $employeeData,
                    'company_id' => $companyId,
                    'employee_id' => $employeeData['id'],
                ];
                $item = new AuditCacheItem(
                    EmployeeAuditService::class,
                    EmployeeAuditService::ACTION_EDIT,
                    new AuditUser($userId, $employeeData['account_id']),
                    $details
                );
                $item->cache();

                // add cache item to Redis set
                Redis::sAdd("$jobId:for_logging", $item->getKey());
            } catch (\Exception $e) {
                Log::error($e->getMessage() . ' : Could not audit Employee update with id = ' . $employeeData['id']);
            }
        }
    }

    /**
     * Log Employee Update
     *
     * @param string $jobId
     * @param array $employeeIds
     * @param int $userId
     *
     */
    public function logUpdatedEmployees(
        string $jobId,
        array $employeeIds,
        int $userId
    ) {
        // fetch audit items
        $updateSet = Redis::sMembers("$jobId:for_logging");
        $originalEmployeeData = [];
        foreach ($updateSet as $updateEmployeeKey) {
            $cache = Redis::hGetAll($updateEmployeeKey);
            $originalEmployee = json_decode($cache['old'], true);
            $originalEmployeeData[$originalEmployee['id']] = $originalEmployee;
        }

        foreach ($employeeIds as $employeeId) {
            try {
                $employeeData = $originalEmployeeData[$employeeId];
                $response = $this->employeeRequestService->getEmployee($employeeId);
                $responseData = json_decode($response->getData(), true);

                $details = [
                    'old' => $employeeData,
                    'new' => $responseData,
                    'company_id' => $employeeData['company_id'],
                    'employee_id' => $employeeId,
                ];
                $item = new AuditCacheItem(
                    EmployeeAuditService::class,
                    EmployeeAuditService::ACTION_EDIT,
                    new AuditUser($userId, $employeeData['account_id']),
                    $details
                );
                $this->auditService->queue($item);
            } catch (\Exception $e) {
                Log::error($e->getMessage() . ' : Could not audit Employee update with id = ' . $employeeId);
            }
        }
    }

    /**
     * Log Employee Creation
     *
     * @param array $employeeIds
     * @param int $userId
     *
     */
    public function logCreatedEmployees(array $employeeIds, int $userId)
    {
        foreach ($employeeIds as $employeeId) {
            try {
                $response = $this->employeeRequestService->getEmployee($employeeId);
                $employeeData = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($employeeData),
                    'user' => json_encode([
                        'account_id' => $employeeData['account_id'],
                        'id' => $userId,
                    ]),
                ]);
            } catch (\Exception $e) {
                Log::error($e->getMessage() . ' : Could not audit Employee creation with id = ' . $employeeId);
            }
        }
    }

    /**
     * Log T&A Employee Creation
     *
     * @param array $employees
     * @param int $userId
     *
     */
    public function logCreatedTaEmployees(array $employees, int $userId)
    {
        foreach ($employees as $employee) {
            $this->logCreate([
                'new' => json_encode($employee),
                'user' => json_encode([
                    'account_id' => $employee['account_id'],
                    'id' => $userId,
                ]),
            ]);
        }
    }

    /**
     * Log Employee creation
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);

        $this->auditService->log($item);
    }
}
