<?php

namespace App\Employee;

use App\Authz\AuthzDataScope;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EmployeeAuthz
{
    /**
     * @var EmployeeRequestService
     */
    private $employeeRequestService;

    public function __construct(EmployeeRequestService $employeeRequestService)
    {
        $this->employeeRequestService = $employeeRequestService;
    }

    public function isAuthorizedById(int $id, AuthzDataScope $authzDataScope): bool
    {
        try {
            $response = $this->employeeRequestService->getEmployee($id);
            $employee = json_decode($response->getData(), true);
        } catch (HttpException $exception) {
            return false;
        }

        return $this->isAuthorized($employee, $authzDataScope);
    }

    public function isAuthorized(array $employee, AuthzDataScope $authzDataScope): bool
    {
        return $authzDataScope->isAllAuthorized([
            AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
            AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
            AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
            AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
            AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'team_id'),
            AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll_group.id'),
        ]);
    }

    /**
     * Check all ids are authorized
     *
     * @param array|int[] $ids
     * @param AuthzDataScope $authzDataScope
     * @return bool
     */
    public function isAllAuthorizedByIds(array $ids, AuthzDataScope $authzDataScope): bool
    {
        $authorized = true;
        foreach ($ids as $id) {
            if (!$this->isAuthorizedById((int) $id, $authzDataScope)) {
                $authorized = false;
                break;
            }
        }

        return $authorized;
    }

    /**
     * Check all company employee are authorized
     *
     * @param int $companyId
     * @param array|int[] $ids
     * @param AuthzDataScope $authzDataScope
     * @return bool
     */
    public function isAllAuthorizedByIdAndCompanyId(int $companyId, array $ids, AuthzDataScope $authzDataScope): bool
    {
        if (!$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)) {
            return false;
        }

        $response = $this->employeeRequestService->getCompanyEmployeesByIds(
            $companyId,
            $ids,
            'DEFAULT',
            [],
            $authzDataScope
        );
        $employees = json_decode($response->getData(), true)['data'];
        $authorized = true;

        if (count($ids) !== count($employees)) {
            return false;
        }

        foreach ($employees as $employee) {
            if (!$this->isAuthorized($employee, $authzDataScope)) {
                $authorized = false;
                break;
            }
        }

        return $authorized;
    }

    /**
     * Check all company employee are authorized
     *
     * @param int $companyId
     * @param array|int[] $ids
     * @param AuthzDataScope $authzDataScope
     * @return bool
     */
    public function isAllAuthorizedByIdsAndCompanyId(
        int $companyId,
        array $data,
        AuthzDataScope $authzDataScope
    ): bool {
        if (!$authzDataScope->isAuthorized(AuthzDataScope::SCOPE_COMPANY, $companyId)) {
            return false;
        }

        $authorized = true;

        foreach ($data as $employee) {
            if (!$this->isAuthorizedEmployees($employee, $authzDataScope)) {
                $authorized = false;
                break;
            }
        }

        return $authorized;
    }

    public function isAuthorizedEmployees(array $employee, AuthzDataScope $authzDataScope): bool
    {
        return $authzDataScope->isAllAuthorized([
            AuthzDataScope::SCOPE_COMPANY => Arr::get($employee, 'company_id'),
            AuthzDataScope::SCOPE_DEPARTMENT => Arr::get($employee, 'department_id'),
            AuthzDataScope::SCOPE_POSITION => Arr::get($employee, 'position_id'),
            AuthzDataScope::SCOPE_LOCATION => Arr::get($employee, 'location_id'),
            AuthzDataScope::SCOPE_TEAM => Arr::get($employee, 'team_id'),
            AuthzDataScope::SCOPE_PAYROLL_GROUP => Arr::get($employee, 'payroll_group_id'),
        ]);
    }
}
