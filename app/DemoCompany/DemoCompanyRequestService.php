<?php

namespace App\DemoCompany;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class DemoCompanyRequestService extends RequestService
{
    /**
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Get company employees.
     *
     * @param int $companyId
     * @param array $statusFilter
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setProgress($companyId, $progressType)
    {
        $data['id'] = $companyId;
        $data['type'] = $progressType;

        return $this->send(new Request(
            'PATCH',
            '/philippine/company_demo/progress',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));

    }

    /**
     * Get company employees.
     *
     * @param int $companyId
     * @param array $statusFilter
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setError($companyId, $progressType, $error)
    {
        $data['id'] = $companyId;
        $data['type'] = $progressType;
        $data['error'] = $error;

        return $this->send(new Request(
            'PATCH',
            '/philippine/company_demo/error',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));

    }

    /**
     * Get company employees.
     *
     * @param int $companyId
     * @param array $statusFilter
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProgress($companyId, $progressType)
    {
        $data['id'] = $companyId;
        $data['type'] = $progressType;

        return $this->send(new Request(
            'GET',
            '/philippine/company_demo/progress?' . http_build_query($data)
        ));
    }

    /**
     * Send error message to mattermost.
     *
     * @param string $msg
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function mattermostNotif($msg)
    {
        $data['msg'] = $msg;

        return $this->send(new Request(
            'POST',
            '/philippine/company_demo/mattermost_notif',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }
}
