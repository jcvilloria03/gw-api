<?php

namespace App\DemoCompany;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Attendance\AttendanceRequestService;
use App\Attendance\AttendanceUploadTask;
use App\DemoCompany\DemoCompanyException;
use App\DemoCompany\DemoCompanyRequestService;
use App\Employee\EmployeeRequestService;
use App\Jobs\JobsRequestService;
use App\Payroll\PayrollRequestService;
use App\Schedule\ScheduleRequestService;
use App\Shift\ShiftRequestService;
use Bschmitt\Amqp\Facades\Amqp;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */

class DemoCompanyService
{
    //const SCHEDULE_CSV = 'CompanyDemo/schedules.csv';

    /**
     * @var int Company id
     */
    protected $companyId;

    /**
     * @var array attributes
     */
    protected $attributes;

    /**
     * @var string fileName
     */
    protected $fileName;

    /**
     * @var \App\DemoCompany\DemoCompanyRequestService
     */
    protected $demoCompanyRequestService;

    /**
     * @var \App\Employee\EmployeeRequestService
     */
    protected $employeeRequestService;

    /**
     * @var \App\Shift\ShiftRequestService
     */
    protected $shiftRequestService;

    /**
     * @var \App\Attendance\AttendanceRequestService
     */
    protected $attendanceRequestService;

    /**
     * @var \App\Payroll\PayrollRequestService
     */
    protected $payrollRequestService;

    /**
     * @var \App\Schedule\ScheduleRequestService
     */
    protected $scheduleRequestService;

    /**
     * Constructor
     *
     * @param \App\DemoCompany\DemoCompanyRequestService $demoCompanyRequestService
     * @param \App\Employee\EmployeeRequestService $employeeRequestService
     * @param \App\Shift\ShiftRequestService $shiftRequestService
     * @param \App\Attendance\AttendanceRequestService $attendanceRequestService
     * @param \App\Payroll\PayrollRequestService $payrollRequestService
     * @param \App\Schedule\ScheduleRequestService $scheduleRequestService
     *
     */
    public function __construct(
        DemoCompanyRequestService $demoCompanyRequestService,
        EmployeeRequestService $employeeRequestService,
        ShiftRequestService $shiftRequestService,
        AttendanceRequestService $attendanceRequestService,
        PayrollRequestService $payrollRequestService,
        ScheduleRequestService $scheduleRequestService
    ) {
        $this->demoCompanyRequestService = $demoCompanyRequestService;
        $this->employeeRequestService = $employeeRequestService;
        $this->shiftRequestService = $shiftRequestService;
        $this->attendanceRequestService = $attendanceRequestService;
        $this->payrollRequestService = $payrollRequestService;
        $this->scheduleRequestService = $scheduleRequestService;
    }

    /**
     * Process ta-api demo company populate request
     *
     * @param array $attributes
     *
     * @throws DemoCompanyException
     */
    public function process(array $attributes)
    {
        Log::info('[DemoCompany] Start process gw-api');
        if (
            empty($attributes['company_id'])
        ) {
            throw new DemoCompanyException('Incomplete details for processing');
        }

        $this->companyId = $attributes['company_id'];
        $this->fileName = 'attendance_'. uniqid() .'.csv';

        if ($attributes['type'] === 'attendance') {
            $progressStatus = $this->demoCompanyRequestService->getProgress($this->companyId, 'attendance');
            $progressStatus = json_decode($progressStatus->getData(), true);
            $progressStatus = $progressStatus['status'];
            if ($progressStatus == true) {
                return;
            }
            $this->processAttendance($attributes['data']);
        }

        if ($attributes['type'] === 'payroll') {
            $progressStatus = $this->demoCompanyRequestService->getProgress($this->companyId, 'payroll');
            $progressStatus = json_decode($progressStatus->getData(), true);
            $progressStatus = $progressStatus['status'];
            if ($progressStatus == true) {
                return;
            }
            $this->processPayroll();
        }
    }

    /**
     * Process as-api demo company attendance populate request
     *
     * @param array $attributes
     */
    public function processAttendance($spanData)
    {
        Log::info('[DemoCompany] Start processAttendance Company id: ' . $this->companyId);
        $employeeIds = [];
        $dates = [];

        $employeesData = $this->employeeRequestService->getCompanyEmployeesSimple($this->companyId);
        $employees = json_decode($employeesData->getData(), true);
        $employees = $employees['data'];
        $clnEmployees = collect($employees);
        $employeeIds = $clnEmployees->pluck('id')->unique()->all();
        $shiftsData = $this->shiftRequestService->getCompanyShifts($this->companyId);
        $shifts = json_decode($shiftsData->getData(), true);
        $shifts = $shifts['data'];
        $clnShifts = collect($shifts);
        $scheduleIds = $clnShifts->pluck('schedule_id')->unique()->all();
        $schedules = $this->scheduleRequestService->getMultiple($this->companyId, $scheduleIds);

        if (!empty($shifts) && !empty($employeeIds)) {
            $dates = $shifts[0]['dates'];

            $progressAttRecStat = $this->demoCompanyRequestService->getProgress($this->companyId, 'attendance_records');
            $progressAttRecStat = json_decode($progressAttRecStat->getData(), true);
            $progressAttRecStat = $progressAttRecStat['status'];

            if ($progressAttRecStat == false) {
                Log::info('[DemoCompany] Before createCsv Company id: ' . $this->companyId);
                $this->createCsv($employees, $dates, $shifts, $schedules);
                Log::info('[DemoCompany] After createCsv Company id: ' . $this->companyId);
                Log::info('[DemoCompany] Before uploadCsv Company id: ' . $this->companyId);
                $uploadStatus = $this->uploadCsv($this->attendanceRequestService);
                if ($uploadStatus == 'FAIL') {
                    Log::info('[DemoCompany] uploadCsv FAILED Company id: ' . $this->companyId);
                    return;
                }
                Log::info('[DemoCompany] After uploadCsv Company id: ' . $this->companyId);
                Log::info('[DemoCompany] Before calculateAttendance Company id: ' . $this->companyId);
                $calculateStatus = $this->calculateAttendance($spanData, $employeeIds, $this->attendanceRequestService);
                Log::info('[DemoCompany] After calculateAttendance Company id: ' . $this->companyId);
            } else {
                Log::info('[DemoCompany] Before calculateAttendance only Company id: ' . $this->companyId);
                $calculateStatus = $this->calculateAttendance($spanData, $employeeIds, $this->attendanceRequestService);
                Log::info('[DemoCompany] After calculateAttendance only Company id: ' . $this->companyId);
            }

            if ($calculateStatus == 'FINISHED') {
                if (file_exists($this->fileName)) {
                    unlink($this->fileName);
                }

                $this->demoCompanyRequestService->setProgress($this->companyId, 'attendance');
                Log::info('[DemoCompany] Done processAttendance Company id: ' . $this->companyId);
                // send message
                $details = [
                    'company_id' => $this->companyId,
                    'type' => 'payroll'
                ];
                $this->demoCompanyQueue($details);
                return;
            } else {
                return;
            }
        }
    }

    public function calculateAttendance(array $spanData, array $employeeIds, $attendanceRequestService)
    {
        Log::info('[DemoCompany] Start calculateAttendance Company id: ' . $this->companyId);
        $retries = 240;
        $attempts = 0;
        $calcData = [];

        $calcData['company_id'] = $this->companyId;
        $calcData['filter']['employee_status'] = ['active'];
        $calcData['employee_ids'] = $employeeIds;
        $calcData['search_terms'] = null;
        $calcData['dates'] = $spanData;

        $respBulkCalculate = $attendanceRequestService->bulkAttendanceCalculateByEmployees($calcData);
        $respBulkCalculate = json_decode($respBulkCalculate->getData(), true);
        $respBulkCalculate = array_get($respBulkCalculate, 'data', null);
        $calculateJobId = array_get($respBulkCalculate, 'job_id', null);

        $calculateStatus = 'PENDING';
        while ($calculateStatus != 'FINISHED') {
            $calculateStatus = $attendanceRequestService->getJobDetails($calculateJobId);
            $calculateStatus = json_decode($calculateStatus->getData(), true);
            $calculateStatus = array_get($calculateStatus, 'data', null);
            $calculateStatus = array_get($calculateStatus, 'status', null);
            $attempts++;
            if ($attempts == $retries) {
                $type = 'attendance';
                $msg = 'calculateAttendance max retries reached - Job ID: ' . $calculateJobId;
                $env = strtoupper(getenv('APP_ENV'));
                $this->demoCompanyRequestService->setError(
                    $this->companyId,
                    $type,
                    $msg
                );
                $msg = ':no_entry: **[' . $env . ']' . '[GW-API]' . '[DemoCompany] ' . ucwords($type) . '**' .
                "\n" . "```". $msg ."```";
                $this->demoCompanyRequestService->mattermostNotif($msg);
                return 'FAIL';
            }
            sleep(15);
        }
        Log::info('[DemoCompany] Done calculateAttendance Company id: ' . $this->companyId);
        return $calculateStatus;
    }

    public function createCsv(array $employeeInput, array $dateInput, array $shiftInput, array $scheduleInput)
    {
        Log::info('[DemoCompany] Start createCsv Company id: ' . $this->companyId);
        $attendanceCsv = [];
        $csvHeader = array('Employee Id', 'Employee Name', 'Timesheet Date', 'Tags', 'Clock In', 'Clock Out');
        $cnt = 0;
        foreach ($employeeInput as $data) {
            foreach ($dateInput as $dateData) {
                foreach ($shiftInput as $shiftData) {
                    foreach ($scheduleInput as $schedData) {
                        if ($shiftData['employee_id'] == $data['id']) {
                            if ($shiftData['schedule_id'] == $schedData['id']) {
                                switch ($schedData['name']) {
                                    case '9AM to 6PM':
                                        $clockIn = '09:00';
                                        $clockOut = '18:00';
                                        break;
                                    case 'Full Flexi':
                                        $clockIn = '09:00';
                                        $clockOut = '17:00';
                                        break;
                                    case 'Semi-Flexi':
                                        $clockIn = '09:00';
                                        $clockOut = '17:00';
                                        break;
                                    case '12AM to 9PM':
                                        $clockIn = '09:00';
                                        $clockOut = '18:00';
                                        break;
                                    case '7AM to 4PM':
                                        $clockIn = '07:00';
                                        $clockOut = '16:00';
                                        break;
                                    case '8AM to 5PM':
                                        $clockIn = '08:00';
                                        $clockOut = '17:00';
                                        break;
                                    case '9PM to 6AM':
                                        $clockIn = '21:00';
                                        $clockOut = '06:00';
                                        break;
                                    case '1PM to 10PM':
                                        $clockIn = '13:00';
                                        $clockOut = '22:00';
                                        break;
                                    case '8:30AM to 5:30PM':
                                        $clockIn = '08:30';
                                        $clockOut = '17:30';
                                        break;
                                    case 'Fixed with Scheduled OT':
                                        $clockIn = '07:00';
                                        $clockOut = '16:00';
                                        break;
                                    default:
                                        $clockIn = '08:00';
                                        $clockOut = '17:00';
                                        break;
                                }

                                $attendanceCsv[$cnt] =
                                    array(
                                        trim($data['employee_id']),
                                        $data['first_name'] . ' ' . $data['last_name'],
                                        $dateData,
                                        '',
                                        $clockIn,
                                        $clockOut
                                    );
                                $cnt++;
                            }
                        }
                    }
                }
            }
        }

        $fp = fopen($this->fileName, 'w');
        fwrite($fp, implode(',', $csvHeader) . "\n");

        foreach ($attendanceCsv as $line) {
            fputcsv($fp, $line, ',');
        }
        fclose($fp);
        Log::info('[DemoCompany] End createCsv Company id: ' . $this->companyId);
    }

    public function uploadCsv($attendanceRequestService)
    {
        Log::info('[DemoCompany] Start uploadCsv Company id: ' . $this->companyId);
        $retries = 240;
        $attempts = 0;
        $filepath = $this->fileName;
        $uploadTask = App::make(AttendanceUploadTask::class);
        $uploadTask->create($this->companyId);
        Log::info('[DemoCompany] uploadCsv s3_bucket: ' . $uploadTask->getS3Bucket());
        $s3Key = $uploadTask->saveAttendanceInfo($filepath);
        Log::info('[DemoCompany] uploadCsv s3key: ' . $s3Key);
        $scope = [];
        $scope['COMPANY'] = [$this->companyId];
        $response = $attendanceRequestService->uploadAttendanceInfo([
            'company_id' => $this->companyId,
            's3_bucket' => $uploadTask->getS3Bucket(),
            's3_key' => $s3Key,
            'authz_data_scope' => $scope
        ]);

        $responseData = json_decode($response->getData(), true);
        $responseData = array_get($responseData, 'data', null);
        $uploadJobId = array_get($responseData, 'job_id', null);

        $uploadStatus = 'PENDING';
        while ($uploadStatus != 'FINISHED') {
            $uploadStatusResponse = $attendanceRequestService->getJobDetails($uploadJobId);
            $uploadStatusData = Arr::get(json_decode($uploadStatusResponse->getData(), true), 'data');
            $uploadStatus = Arr::get($uploadStatusData, 'status');
            Log::info('[DemoCompany] uploadCsv', [
                "company_id" => $this->companyId,
                "upload_status" => $uploadStatus,
                "retries" => $attempts
            ]);
            if (strtolower($uploadStatus) == 'finished') {
                break;
            }
            $attempts++;
            if ($attempts >= $retries) {
                $type = 'attendance';
                $msg = 'uploadCsv max retries reached - Job ID: ' . $uploadJobId .
                    'uploadStatus: ' . $uploadStatus;
                $env = strtoupper(getenv('APP_ENV'));
                $this->demoCompanyRequestService->setError(
                    $this->companyId,
                    $type,
                    $msg
                );
                $msg = ':no_entry: **[' . $env . ']' . '[GW-API]' . '[DemoCompany] ' . ucwords($type) . '**' .
                "\n" . "```". $msg ."```";
                $this->demoCompanyRequestService->mattermostNotif($msg);
                return 'FAIL';
            }
            sleep(2);
        }
        Log::info('[DemoCompany] uploaded Csv Company id: ' . $this->companyId);
        $this->demoCompanyRequestService->setProgress($this->companyId, 'attendance_records');
        Log::info('[DemoCompany] End uploadCsv Company id: ' . $this->companyId);
        return $uploadStatus;
    }

    /**
     * Trigger pr-api demo company payroll
     *
     * @param array $attributes
     */
    public function processPayroll()
    {
        Log::info('[DemoCompany] Start processPayroll Company id: ' . $this->companyId);
        $shiftsData = $this->shiftRequestService->getCompanyShifts($this->companyId);
        $shifts = json_decode($shiftsData->getData(), true);
        $shifts = $shifts['data'];
        $startDate = $shifts[0]['start'];
        $endDate = $shifts[0]['end'];

        $cbnStartDate = Carbon::createFromFormat('Y-m-d', $startDate);
        $cbnEndDate = Carbon::createFromFormat('Y-m-d', $endDate);

        //check for ongoing payrolls
        $this->checkUnprocessedPayroll();

        // get payroll groups
        $payrollGroupsResponse = $this->payrollRequestService->getCompanyPayrollGroupsWithPeriods($this->companyId);
        $payrollGroupsData = json_decode($payrollGroupsResponse->getData(), true);
        $payrollGroups = $payrollGroupsData['data'];

        foreach ($payrollGroups as $groups) {
            $payrollGroupPeriodsResponse = $this->payrollRequestService->getPayrollGroupPeriods($groups['id']);
            $payrollGroupPeriodsData = json_decode($payrollGroupPeriodsResponse->getData(), true);
            $payrollGroupPeriods = array_get($payrollGroupPeriodsData, 'data', null);
            $payDatePeriods = array_get($payrollGroupPeriods, 'pay_date_periods', null);

            foreach ($payDatePeriods as $payDatePeriodBatch) {
                $payrollPeriod = array_get($payDatePeriodBatch, 'payroll_period', null);
                $attendancePeriod = array_get($payDatePeriodBatch, 'attendance_period', null);

                $payrollStart = array_get($payrollPeriod, 'start', null);
                $payrollCutoff = array_get($payrollPeriod, 'cutoff', null);

                $cbnPeriodStart = Carbon::createFromFormat('Y-m-d', $payrollStart);
                $cbnPeriodCutoff = Carbon::createFromFormat('Y-m-d', $payrollCutoff);

                if (
                    $cbnPeriodStart->between( $cbnStartDate, $cbnEndDate ) ||
                    $cbnPeriodCutoff->between( $cbnStartDate, $cbnEndDate )
                ) {
                    $inputs = [];
                    if ($attendancePeriod != null) {
                        $attStart = array_get($attendancePeriod, 'start', null);
                        $attCutoff = array_get($attendancePeriod, 'cutoff', null);
                        $inputs['payroll_group_id'] = $groups['id'];
                        $inputs['start_date'] = $payrollStart;
                        $inputs['end_date'] = $payrollCutoff;
                        $inputs['attendance_start_date'] = $attStart;
                        $inputs['attendance_end_date'] = $attCutoff;
                        $inputs['search_term'] = '';
                        $inputs['annualize'] = false;
                    } else {
                        $inputs['payroll_group_id'] = $groups['id'];
                        $inputs['start_date'] = $payrollStart;
                        $inputs['end_date'] = $payrollCutoff;
                        $inputs['attendance_start_date'] = $payrollStart;
                        $inputs['attendance_end_date'] = $payrollCutoff;
                        $inputs['search_term'] = '';
                        $inputs['annualize'] = false;
                    }

                    $regularPayrollJobResponse = $this->payrollRequestService->regularPayrollJob($inputs);
                    $regularPayrollJobResponseData = json_decode($regularPayrollJobResponse->getData(), true);
                    $regPayrollJobId = array_get($regularPayrollJobResponseData, 'jobId', null);

                    $retries = 240;
                    $attempts = 0;
                    //call/payroll/regular_payroll_job/{jobId}
                    $regularPayrollStatus = 'CREATING';
                    Log::info('[DemoCompany] Creating payroll');
                    while ($regularPayrollStatus != 'CREATED') {
                        //call GET payroll/regular_payroll_job/{jobId}
                        $regPayrollJobStatResp = $this->payrollRequestService->regularPayrollJobStatus(
                            $regPayrollJobId);
                        $regularPayrollStatusData = json_decode($regPayrollJobStatResp->getData(), true);
                        $regularPayrollStatusResult = array_get($regularPayrollStatusData, 'result', null);
                        $regularPayrollId = array_get($regularPayrollStatusResult, 'payrollId', null);
                        $regularPayrollStatus = array_get($regularPayrollStatusResult, 'status', null);
                        $attendanceJobId = array_get($regularPayrollStatusResult, 'attendanceJobId', null);

                        if ($regularPayrollStatus == 'FAILED') {
                            break;
                        } else {
                            $attempts++;
                            if ($attempts == $retries) {
                                $type = 'payroll';
                                $msg = 'regularPayrollJobStatus max retries reached - Payroll ID: ' . $regularPayrollId;
                                $env = strtoupper(getenv('APP_ENV'));
                                $this->demoCompanyRequestService->setError(
                                    $this->companyId,
                                    $type,
                                    $msg
                                );
                                $msg = ':no_entry: **['.$env.']'.'[GW-API]'.'[DemoCompany] '. ucwords($type) . '**' .
                                "\n" . "```". $msg ."```";
                                $this->demoCompanyRequestService->mattermostNotif($msg);
                                return 'FAIL';
                            }
                            sleep(15);
                        }
                    }
                    Log::info('[DemoCompany] Created payroll Company id: ' . $this->companyId);
                    Log::info('[DemoCompany] Created payroll Payroll id: ' . $regularPayrollId);
                    if ($regularPayrollStatus == 'CREATED') {
                        $retries = 240;
                        $attempts = 0;
                        $attendanceStatus = 'PENDING';
                        while ($attendanceStatus != 'FINISHED') {
                            $jobRequestService = app()->make(JobsRequestService::class);
                            //call GET payroll/upload/attendance/status
                            Log::info('[DemoCompany] Fetching attendance status Company id: ' . $this->companyId);
                            Log::info('[DemoCompany] Fetching attendance status Payroll id: ' . $regularPayrollId);
                            $attendanceStatusResponse = $jobRequestService->getJobStatus($attendanceJobId);
                            $attendanceStatus = json_decode($attendanceStatusResponse->getData(), true);
                            $attendanceStatus = array_get($attendanceStatus, 'data', null);
                            $attendanceStatus = array_get($attendanceStatus, 'attributes', null);
                            $attendanceStatus = array_get($attendanceStatus, 'status', null);
                            $attempts++;
                            if ($attempts == $retries) {
                                $type = 'payroll';
                                $msg = 'getJobStatus max retries reached - Job ID: ' . $attendanceJobId;
                                $env = strtoupper(getenv('APP_ENV'));
                                $this->demoCompanyRequestService->setError(
                                    $this->companyId,
                                    $type,
                                    $msg
                                );
                                $msg = ':no_entry: **['.$env.']'.'[GW-API]'.'[DemoCompany] '. ucwords($type) . '**' .
                                "\n" . "```". $msg ."```";
                                $this->demoCompanyRequestService->mattermostNotif($msg);
                                return 'FAIL';
                            }
                            sleep(15);
                        }

                        //call POST /payroll/{payroll_id}/calculate
                        Log::info('[DemoCompany] Before calculatePayroll Company id: ' . $this->companyId);
                        Log::info('[DemoCompany] Before calculatePayroll Payroll id: ' . $regularPayrollId);
                        $this->calculatePayroll($regularPayrollId);
                        Log::info('[DemoCompany] After calculatePayroll Company id: ' . $this->companyId);
                        Log::info('[DemoCompany] After calculatePayroll Payroll id: ' . $regularPayrollId);
                    }
                }
            }
        }

        $this->checkUnprocessedPayroll();

        $this->demoCompanyRequestService->setProgress($this->companyId, 'payroll');
        Log::info('[DemoCompany] Done processPayroll Company id: ' . $this->companyId);
        return;
    }

    /**
     * Publish a message to a queue.
     *
     * @param int $company_id Company Id
     */
    protected function demoCompanyQueue($details)
    {
        $message = new \Bschmitt\Amqp\Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(config('queues.demo_company_gw_queue'), $message);
    }

    public function checkUnprocessedPayroll()
    {
        Log::info('[DemoCompany] Start checkUnprocessedPayroll Company id: ' . $this->companyId);
        $companyPayrollsResp = $this->payrollRequestService->getCompanyPayrolls($this->companyId);
        $companyPayrollsData = json_decode($companyPayrollsResp->getData(), true);
        $companyPayrollsData = array_get($companyPayrollsData, 'data', null);

        foreach ($companyPayrollsData as $payroll) {
            if ($payroll['status'] == 'DRAFT') {
                $this->calculatePayroll($payroll['id']);
            } else if ($payroll['status'] == 'OPEN') {
                $this->closePayroll($payroll['id']);
            }
        }
        Log::info('[DemoCompany] Done checkUnprocessedPayroll Company id: ' . $this->companyId);
    }

    public function calculatePayroll(int $payrollId)
    {
        Log::info('[DemoCompany] Start calculatePayroll Company id: ' . $this->companyId);
        //call POST /payroll/{payroll_id}/calculate
        $params = [];
        $this->payrollRequestService->calculate($payrollId, $params);
        $retries = 240;
        $attempts = 0;
        $computePayrollStatus = 'PENDING';
        while ($computePayrollStatus != 'FINISHED') {
            //call GET /payrolls/jobs/status?payroll_ids[]={payroll_id}
            $arrPayId = [];
            array_push($arrPayId, $payrollId);
            $payCalcStatResp = $this->payrollRequestService->getPayrollsJobsStatus($arrPayId);
            $payrollCalcStatus = json_decode($payCalcStatResp->getData(), true);
            $payrollCalcStatus = array_get($payrollCalcStatus, 'data', null);
            foreach ($payrollCalcStatus as $calcStatusItem) {
                if ($calcStatusItem['data']['attributes']['payload']['name'] == 'compute-payroll') {
                    $computePayrollStatus = $calcStatusItem['data']['attributes']['status'];
                    break;
                }
            }
            $attempts++;
            if ($attempts == $retries) {
                $type = 'payroll';
                $msg = 'getPayrollsJobsStatus max retries reached - Payroll ID: ' . $arrPayId;
                $env = strtoupper(getenv('APP_ENV'));
                $this->demoCompanyRequestService->setError(
                    $this->companyId,
                    $type,
                    $msg
                );
                $msg = ':no_entry: **[' . $env . ']' . '[GW-API]' . '[DemoCompany] ' . ucwords($type) . '**' .
                "\n" . "```". $msg ."```";
                $this->demoCompanyRequestService->mattermostNotif($msg);
                return 'FAIL';
            }
            sleep(15);
        }
        $this->closePayroll($payrollId);
        Log::info('[DemoCompany] Done calculatePayroll Company id: ' . $this->companyId);
    }

    public function closePayroll($payrollId)
    {
        Log::info('[DemoCompany] Start Closing Payroll Company id: ' . $this->companyId . ' Payroll id: ' . $payrollId);
        $inputs = [];
        $inputs['skipEnforceGapLoans'] = false;
        $this->payrollRequestService->close($payrollId, $inputs);
        Log::info('[DemoCompany] Done Closing Payroll Company id: ' . $this->companyId . ' Payroll id: ' . $payrollId);
    }
}
