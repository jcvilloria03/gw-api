<?php

namespace App\AffectedEntity;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Authz\AuthzDataScope;

class AffectedEntityRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to search affected entities
     *
     * @param int $id Company ID
     * @param string $term Search term
     * @param int $limit Results limit
     * @param bool $includeAdminUsers
     * @param AuthzDataScope $authzDataScope Authz Data Scope object
     * @return \Illuminate\Http\JsonResponse Search results
     */
    public function search(
        int $id,
        string $term,
        $limit,
        $includeAdminUsers = false,
        AuthzDataScope $authzDataScope = null
    ) {
        $url = "/company/{$id}/affected_entities/search?term={$term}";
        $url .= empty($limit) ? '' : "&limit={$limit}";
        $url .= $includeAdminUsers ? '&include_admin_users=true' : '';

        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to search affected entities
     *
     * @param int $id Company ID
     * @param array $entities items for checking
     * @return \Illuminate\Http\JsonResponse Search results
     */
    public function getAllAffecteEntities(int $id, array $entities)
    {
        $entities = [
            'affected_entities' => collect($entities)->transform(function ($item) {
                return array_only($item, ['id', 'type']);
            })->all()
        ];
        $request = new Request(
            'POST',
            "/company/{$id}/affected_users",
            ['Content-Type' => 'application/json'],
            json_encode($entities)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to search affected entities
     *
     * @param int $id Company ID
     * @param array $entities items for checking
     * @return \Illuminate\Http\JsonResponse Search results
     */
    public function getAllActiveAffectedEmployees(int $id, array $entities)
    {
        $entities = [
            'affected_entities' => collect($entities)->transform(function ($item) {
                return array_only($item, ['id', 'type']);
            })->all()
        ];
        $request = new Request(
            'GET',
            "/company/{$id}/active_affected_employees",
            ['Content-Type' => 'application/json'],
            json_encode($entities)
        );

        return $this->send($request);
    }
}
