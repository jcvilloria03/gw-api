<?php

namespace App\AffectedEntity;

use App\Department\DepartmentAuthorizationService;
use App\Position\PositionAuthorizationService;
use App\Employee\EmployeeAuthorizationService;
use App\Team\TeamAuthorizationService;
use App\User\UserAuthorizationService;

class AffectedEntityAuthorizationService
{

     /**
     * @var \App\Department\DepartmentAuthorizationService
     */
    private $departmentAuthorizationService;

    /**
     * @var \App\Position\PositionAuthorizationService
     */
    private $positionAuthorizationService;

    /**
     * @var \App\Team\TeamAuthorizationService
     */
    private $teamAuthorizationService;

    /**
     * @var \App\Employee\EmployeeAuthorizationService
     */
    private $employeeAuthorizationService;

    /**
     * @var \App\User\UserAuthorizationService
     */
    private $userAuthorizationService;


    public function __construct(
        DepartmentAuthorizationService $departmentAuthorizationService,
        PositionAuthorizationService $positionAuthorizationService,
        EmployeeAuthorizationService $employeeAuthorizationService,
        TeamAuthorizationService $teamAuthorizationService,
        UserAuthorizationService $userAuthorizationService
    ) {
        $this->departmentAuthorizationService = $departmentAuthorizationService;
        $this->positionAuthorizationService = $positionAuthorizationService;
        $this->teamAuthorizationService = $teamAuthorizationService;
        $this->userAuthorizationService = $userAuthorizationService;
        $this->employeeAuthorizationService = $employeeAuthorizationService;
    }
    /**
     * @param \stdClass $data
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $data, array $user)
    {
        return $this->departmentAuthorizationService->authorizeGet($data, $user)
            && $this->positionAuthorizationService->authorizeGet($data, $user)
            && $this->teamAuthorizationService->authorizeGet($data, $user)
            && $this->userAuthorizationService->authorizeGet($data, $user)
            && $this->employeeAuthorizationService->authorizeGet($data, $user);
    }
}
