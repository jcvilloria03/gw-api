<?php

namespace App\Country;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class CountryRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get list of countries
     *
     * @return json List of supported countries
     *
     */
    public function getAll()
    {
        $request = new Request(
            'GET',
            "/countries/"
        );
        return $this->send($request);
    }
}
