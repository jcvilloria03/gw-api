<?php

namespace App\TimeRecord;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class TimeRecordAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';

    const OBJECT_NAME = 'time_record';

    const ACTIONS = [
        'created' => self::ACTION_CREATE,
        'deleted' => self::ACTION_DELETE
    ];

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log TimeRecord related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log Time Record create
     *
     * @param array $cacheItem
     * @return void
     */
    public function logCreate(array $cacheItem)
    {
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'new' => $new
            ]
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log Time Record Delete
     *
     * @param array $cacheItem
     * @return void
     */
    public function logDelete(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $old['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'old' => $old
            ]
        ]);

        $this->auditService->log($item);
    }
}
