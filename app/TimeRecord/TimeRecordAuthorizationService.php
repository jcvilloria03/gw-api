<?php

namespace App\TimeRecord;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class TimeRecordAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.time_records';
    const EDIT_TASK = 'edit.time_records';

    /**
     * @param \stdClass $timeRecord
     * @param array $user
     * @return bool
     */
    public function authorizeViewTimeRecords(\stdClass $timeRecord, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($timeRecord, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $timeRecord
     * @param array $user
     * @return bool
     */
    public function authorizeBulkCreateOrDelete(\stdClass $timeRecord, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($timeRecord, $user, self::EDIT_TASK);
    }

    /**
     * Authorize time record related tasks
     *
     * @param \stdClass $timeRecord
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $timeRecord,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);

        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($timeRecord->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as team's account
                return $timeRecord->account_id == $user['account_id'];
            }

            // check if user has company level permissions for company
            return $companyScope->inScope($timeRecord->company_id);
        }

        return false;
    }
}
