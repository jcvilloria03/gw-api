<?php

namespace App\TimeRecord;

use GuzzleHttp\Psr7\Request;
use App\Request\RequestService;

class TimeRecordRequestService extends RequestService
{
    /**
     * Call endpoint to create employee's time clock record
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'PUT',
            'log',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));

    }

    /**
     * Call endpoint to delete employee's time clock record
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(array $data)
    {
        return $this->send(new Request(
            'POST',
            'delete',
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        ));
    }

    /**
     * Call TimeRecord service endpoint to get timesheet for employee identified by ID
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTimesheet(int $id)
    {
        return $this->send(new Request(
            'GET',
            'timesheet/' . $id
        ));
    }
}
