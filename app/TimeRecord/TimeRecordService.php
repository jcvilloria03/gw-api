<?php

namespace App\TimeRecord;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\MaxClockOut\MaxClockOutRequestService;
use Illuminate\Support\Facades\App;

class TimeRecordService
{
    /**
     * @var \App\TimeRecord\TimeRecordRequestService
     */
    protected $requestService;

    /**
     * @var \App\MaxClockOut\MaxClockOutRequestService
     */
    protected $maxClockoutRequestService;

    public function __construct(
        TimeRecordRequestService $requestService
    ) {
        $this->requestService = $requestService;
    }

    /**
     * Create time clock record
     *
     * @param array $data
     *
     * @return array
     */
    public function create(array $data)
    {

        try {
            $timeClockData = $this->requestService->create($data);
            $timeClockItem = json_decode($timeClockData->getData(), true);
            $timeClock = $timeClockItem['item'];
        } catch (HttpException $e) {
            \Log::error($e->getMessage());
            $timeClock = [];
        }

        return $timeClock;
    }

    /**
     * Delete time clock record
     *
     * @param array $data
     *
     * @return array
     */
    public function delete(array $data)
    {

        try {
            $timeClockData = $this->requestService->delete($data);
            $timeClock = json_decode($timeClockData->getData(), true);
        } catch (HttpException $e) {
            \Log::error($e->getMessage());
            $timeClock = [];
        }

        return $timeClock;
    }

    /**
     * Bulk create time clock records for employee in company
     *
     * @param integer $companyId
     * @param array $timeclocks
     *
     * @return array
     */
    public function bulkCreate(int $companyId, array $timeclocks)
    {
        $responses = [];
        $hours = null;

        if (array_key_exists("rule", $timeclocks)) {
            $rule = $timeclocks['rule'];
            unset($timeclocks['rule']);
        }

        foreach ($timeclocks as $timeclock) {
            foreach ($rule as $ruleItem) {
                if ($ruleItem['id'] == $timeclock['employee_id']) {
                    $hours = $ruleItem['hours'];
                }
            }
            $responses[] = $this->create([
                'employee_uid' => strval($timeclock['employee_id']),
                'company_uid' => strval($companyId),
                'state' => ($timeclock['type'] === 'clock_in') ? strval(1) : strval(0),
                'tags' => $timeclock['tags'] ?? [],
                'timestamp' => strval($timeclock['timestamp']),
                'hour_rule' => strval($hours)
            ]);
        }
        return $responses;
    }
    
    /**
     * Bulk delete time clock records
     *
     * @param array $timeclocks
     *
     * @return array
     */
    public function bulkDelete(array $timeclocks)
    {
        $responses = [];

        foreach ($timeclocks as $timeclock) {
            $responses[] = $this->delete([
                'employee_uid' => strval($timeclock['employee_id']),
                'timestamp' => strval($timeclock['timestamp'])
            ]);
        }

        return $responses;
    }

    /**
     * Get timesheet for employee identified by an ID
     *
     * @param int $employeeId
     *
     * @return array
     */
    public function getTimesheetForEmployee(int $employeeId)
    {
        try {
            $employeeTimeSheetRequest = $this->requestService->getTimesheet($employeeId);
            $employeeTimeSheet = json_decode($employeeTimeSheetRequest->getData(), true);
        } catch (HttpException $e) {
            \Log::error($e->getMessage());
            $employeeTimeSheet = [];
        }

        return $employeeTimeSheet;
    }

    /**
     * Perform timeclocks changes for employee in company
     *
     * @param array $data time records for create or delete
     * @param integer $companyId
     * @return array
     */
    public function bulkCreateOrDelete(array $data, int $companyId)
    {
        $timeRecords = [];
        $newData = [];

        if (!empty($data['delete'])) {
            $timeRecords['deleted'] = $this->bulkDelete($data['delete']);
        }

        if (!empty($data['create'])) {
            $ids = collect($data['create'])->pluck('employee_id')->unique()->all();

            if (!empty($ids)) {
                $maxClockOutRequestService = App::make(MaxClockOutRequestService::class);
                $x = 0;

                foreach ($ids as $id) {
                    $maxClockoutRule = $maxClockOutRequestService->getRules($id);
                    $maxClockoutRuleRes = $maxClockoutRule->getData();
                    $result = json_decode($maxClockoutRuleRes, true);
                    if (empty($result) || empty($result['data']['hours'])) {
                        $hours = null;
                    } else {
                        $hours = $result['data']['hours'];
                    }
                    $newData[$x]['id'] = $id;
                    $newData[$x]['hours'] = $hours;
                    $x++;
                }
            }

            $data['create']['rule'] = $newData;
            $timeRecords['created'] = $this->bulkCreate($companyId, $data['create']);
        }
                    
        return $timeRecords;
    }
}
