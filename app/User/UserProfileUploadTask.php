<?php

namespace App\User;

use App\Tasks\UploadTask;
use Illuminate\Support\Facades\Redis;

class UserProfileUploadTask extends UploadTask
{
    const PROCESS_VALIDATION = 'validation';
    const PROCESS_SAVE = 'save';
    const CACHED_DATA_SET_PREFIX = 'data_set:';

    const VALID_PROCESSES = [
        self::PROCESS_VALIDATION,
        self::PROCESS_SAVE,
    ];

    const ID_PREFIX = 'user_profile_upload:';

    /**
     * Account Id
     *
     * @var int
     */
    protected $accountId;

    /**
     * Company Id
     *
     * @var mixed
     */
    protected $companyId;

    /**
     * Create task in Redis
     *
     * @param int $accountId Account Id
     * @param $companyId Company Id
     * @param string $id Job id
     * @return void
     */
    public function create(int $accountId, $companyId, string $id = null)
    {
        $this->accountId = $accountId;
        $this->companyId = $companyId;

        if (empty($id)) {
            $this->id = $this->generateId();
            $this->currentValues = [
                's3_bucket' => $this->s3Bucket
            ];
            $this->createInRedis();
        } else {
            $this->id = $id;
            if (!$this->isIdSame()) {
                throw new UserProfileUploadTaskException('Job does not belong to given Account ID and Company ID');
            }
            $this->currentValues = $this->fetch();
        }
        if (empty($this->currentValues)) {
            throw new UserProfileUploadTaskException('Invalid Job ID');
        }
    }

    /**
     * Generate Task Id, to be used as Redis Key
     * @return string
     */
    protected function generateId()
    {
        return static::ID_PREFIX . $this->accountId . ':' . ($this->companyId ?? 0) . ':' . uniqid();
    }

    /**
     * Check if accountId and accountId in jobId match.
     * May be different when jobId is not auto-generated
     *
     * @return boolean
     */
    public function isIdSame()
    {
        // check job.account_id matches request
        $needle = '/^' . static::ID_PREFIX . $this->accountId . ':' . ($this->companyId ?? 0) . '/';

        return (preg_match($needle, $this->id) === 1);
    }

    /**
     * Persist data set in redis
     *
     * @param array $dataSet
     */
    public function cacheDataSetInRedis(array $dataSet)
    {
        return Redis::hMSet(self::CACHED_DATA_SET_PREFIX . $this->id, $dataSet);
    }

    /**
     * Save User Profiles
     *
     * @param string $path Full path of file to upload
     * @return string $path Full path of file to upload
     */
    public function saveFile(string $path)
    {
        // save file to s3
        $s3Key = $this->generateS3Key();
        $this->saveFileToS3($s3Key, $path);

        // update Redis key
        $this->setVal('s3_key', $s3Key);
        $this->updateValidationStatus(self::STATUS_VALIDATION_QUEUED);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @return string
     */
    protected function generateS3Key()
    {
        return self::ID_PREFIX .
            $this->accountId .
            ':' .
            ($this->companyId ?? 0) .
            ':' .
            uniqid();
    }

    /**
     * Update validation status
     *
     * @param string $newStatus The new status of the current process
     */
    public function updateValidationStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::VALIDATION_STATUSES)) {
            throw new UserProfileUploadTaskException('Invalid Validation Status');
        }

        $currentStatus = $this->currentValues[self::PROCESS_VALIDATION . '_status'] ?? null;
        $changeStatus = $this->canChangeValidationStatus($currentStatus, $newStatus);

        if ($changeStatus) {
            $this->setVal(self::PROCESS_VALIDATION . '_status', $newStatus);
        }
    }

    /**
     * Update write status
     *
     * @param string $newStatus The new status of the saving process
     */
    public function updateSaveStatus(string $newStatus)
    {
        if (!in_array($newStatus, self::SAVE_STATUSES)) {
            throw new UserProfileUploadTaskException('Invalid Save Status : ' . $newStatus);
        }

        // check if PROCESS_VALIDATION is already validated
        if (
            !isset($this->currentValues[self::PROCESS_VALIDATION . '_status']) ||
            $this->currentValues[self::PROCESS_VALIDATION . '_status'] !== self::STATUS_VALIDATED
        ) {
            throw new UserProfileUploadTaskException(
                'User profiles information needs to be validated successfully first'
            );
        }

        $currentStatus = $this->currentValues[self::PROCESS_SAVE . '_status'] ?? null;
        $changeStatus = $this->canChangeSaveStatus($currentStatus, $newStatus);

        if ($changeStatus) {
            $this->setVal(self::PROCESS_SAVE . '_status', $newStatus);
        }
    }

    /**
     * Check if Status can be updated to Queued
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToQueued(string $currentStatus = null)
    {
        return (
            !in_array(
                $currentStatus,
                [
                    self::STATUS_VALIDATING,
                    self::STATUS_SAVING,
                ]
            )
        );
    }

    /**
     * Check if Status can be updated to Validating
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToValidating(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATION_QUEUED || $currentStatus === self::STATUS_SAVE_QUEUED;
    }

    /**
     * Check if Status can be updated to a finished status
     *
     * @param string $currentStatus
     * @return boolean
     */
    protected function canUpdateStatusToFinished(string $currentStatus)
    {
        return $currentStatus === self::STATUS_VALIDATING || $currentStatus === self::STATUS_SAVING;
    }

    /**
     * Update Error File Location
     *
     * @param string $process The new status of the saving process
     * @param string $errorFileS3Key The S3 key of the error file
     *
     */
    public function updateErrorFileLocation(string $process, string $errorFileS3Key)
    {
        $this->setVal($process . '_error_file_s3_key', $errorFileS3Key);
    }
}
