<?php

namespace App\User;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class UserRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch user details
     *
     * @param string $id User ID
     * @return json User information
     *
     */
    public function get(string $id)
    {
        $url = "/user/{$id}?includes=account.owner.product_seats,account.companies,product_seats,companies";

        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch user status
     *
     * @param string $id User ID
     * @return json User information
     *
     */
    public function getUserStatus(string $id, array $toInclude = [])
    {
        $url = "/user/status/{$id}";

        if (!empty($toInclude)) {
            $url += implode(",", $toInclude);
        }

        $request = new Request('GET', $url);

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch user with companies
     *
     * @param int $id User ID
     * @return json User information
     */
    public function getUserCompaniesRelations(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/user/{$id}/companies"
        ));
    }

    /**
     * Call endpoint to fetch list of users for account
     *
     * @param string $accountId Account ID
     * @return json User list
     *
     */
    public function getAccountOrCompanyUsers(
        string $accountId,
        array $companyIds = null,
        array $filters = null
    ) {
        $query = [
            'account_id' => $accountId
        ];

        if ($companyIds) {
            $query['company_ids'] = $companyIds;
        }

        if (!empty($filters)) {
            $query = array_merge($query, $filters);
        }

        $request = new Request(
            'POST',
            '/account/users',
            [
               'Content-Type' => 'application/json'
            ],
            json_encode($query)
        );

        return $this->send($request);
    }

    /**
     * Get essential user data
     *
     * @param  int      $userId
     * @param  int|null $companyId
     * @return array
     */
    public function getEssentialData(int $userId, int $companyId = null)
    {
        $url = sprintf('/user/%d/essential_data', $userId);

        if ($companyId) {
            $url = sprintf('/user/%d/essential_data?company_id=%d', $userId, $companyId);
        }

        $request = new Request('GET', $url);
        $response = $this->send($request);

        return json_decode($response->getData(), true)['data'];
    }

    public function create(array $user)
    {
        $request = new Request(
            'POST',
            '/user/',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($user)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to subscribe existing User to Product Seat
     *
     * @param array $data user_id and product_seat_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(array $data)
    {
        $request = new Request(
            'POST',
            '/user/subscribe',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete user
     *
     * @param int $id User ID
     *
     */
    public function delete(string $id)
    {
        $request = new Request(
            'DELETE',
            "/user/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if email is available
     *
     * @param int $id User ID
     *
     */
    public function isEmailAvailable(array $details)
    {
        $request = new Request(
            'POST',
            '/user/is_email_available',
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($details)
        );

        return $this->send($request);
    }

    /**
     * Fetch account users filtering by selected attribute
     *
     * @param int $id Account ID
     * @param string $attribute
     * @param array $attributeValues
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccountUsersByAttribute(int $id, string $attribute, array $attributeValues)
    {
        $request = new Request(
            'POST',
            "/account/{$id}/users/{$attribute}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributeValues)
        );

        return $this->send($request);
    }

     /**
     * Call endpoint to fetch user informations with account data
     *
     * @param string $id User ID
     * @return json User information
     *
     */
    public function getUserInformations(string $id)
    {
        $request = new Request(
            'GET',
            "/user/{$id}?includes=account,product_seats,companies,salpay_settings"
        );

        return $this->send($request);
    }

    public function update($id, array $attributes)
    {
        $request = new Request(
            'PATCH',
            "/user/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributes)
        );

        return $this->send($request);
    }

    public function validateStatus($id, array $attributes)
    {
        $request = new Request(
            'POST',
            "/user/{$id}/validate_status",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Data to be saved in User Profile upload job
     *
     * @param string $id Job ID
     * @return \Illuminate\Http\JsonResponse User Profile upload preview
     */
    public function getUploadPreview(string $jobId)
    {
        $request = new Request(
            'GET',
            "/user/upload_preview?job_id={$jobId}"
        );

        return $this->send($request);
    }

    /**
     * Send request to get account owners
     *
     * @param int $accountId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccountOwners($accountId)
    {
        return $this->send(new Request('POST', '/account/users', [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ], http_build_query([
            'account_id' => $accountId,
            'types' => ['owner']
        ])));
    }

    /**
     * Send request to get account owner and super admins.
     *
     * @param int $accountId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccountOwnerAndSuperAdmins($accountId)
    {
        return $this->send(new Request('POST', '/account/users', [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ], http_build_query([
            'account_id' => $accountId,
            'types' => ['owner', 'super admin']
        ])));
    }

    /**
     * Send request to get super admins.
     *
     * @param int $accountId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccountSuperAdmins($accountId)
    {
        return $this->send(new Request('POST', '/account/users', [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ], http_build_query([
            'account_id' => $accountId,
            'types' => ['super admin']
        ])));
    }


    /**
     * Call endpoint to set last active company ID
     *
     * @param int $id User ID
     * @param int $companyId Company ID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setLastActiveCompany(int $id, int $companyId)
    {
        return $this->send(new Request(
            'PATCH',
            '/user/last_active_company',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query([
                'id' => $id,
                'company_id' => $companyId,
            ])
        ));
    }

    /**
     * Call endpoint to delete multiple users
     *
     * @param array $data information on users to delete
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/user/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    public function getUserDetails(int $userId)
    {
        $request = new Request(
            'GET',
            '/user/details/'.$userId
        );

        return $this->send($request);
    }

    public function getUserByEmail(string $emailAddress, array $includes = [])
    {
        $query = [
            'email' => $emailAddress
        ];

        if (count($includes) > 0) {
            $query['includes'] = $includes;
        }

        $request = new Request(
            'GET',
            "/user?" . http_build_query($query)
        );

        return $this->send($request);
    }

    public function getUserBydId(int $userId)
    {
        $request = new Request(
            'GET',
            '/user/basic/'.$userId
        );

        return $this->send($request);
    }

    /**
     * Send set user status request
     *
     * @param int  $id
     * @param mixed $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStatus($id, $status)
    {
        $request = new Request(
            'PATCH',
            "/user/{$id}/status",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'status' => $status
            ])
        );

        return $this->send($request);
    }

    /**
     * Fetch account users filtering by selected attribute
     *
     * @param string $attribute
     * @param string $attributeValue
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUsersFilter(string $attributeValue, int $companyId)
    {
        $param = '';
        if (!empty($companyId) && !empty($attributeValue)) {
            $param .= '?company_id='.$companyId.'&keyword='.$attributeValue;
        }

        $request = new Request(
            'GET',
            "/account/allusers{$param}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        );

        return $this->send($request);
    }

    public function getUserInitialData(int $userId)
    {
        $request = new Request(
            'GET',
            "/user/{$userId}/initial-data"
        );

        return $this->send($request);
    }
}
