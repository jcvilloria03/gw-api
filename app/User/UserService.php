<?php

namespace App\User;

use App\Model\UserEssentialData;
use App\User\UserRequestService;
use Salarium\Cache\FragmentedRedisCache;
use Symfony\Component\HttpFoundation\Response;

class UserService
{
    const CACHE_BUCKET_PREFIX = 'user-status';

    const CACHE_FIELD_PREFIX = 'id';

    /**
     * @var App\User\UserRequestService
     */
    protected $userRequestService;

    /**
     * @var Salarium\Cache\FragmentedRedisCache
     */
    protected $cacheService;

    public function __construct(
        UserRequestService $userRequestService,
        FragmentedRedisCache $fragmentedRedisCache
    ) {
        $this->userRequestService = $userRequestService;
        $this->cacheService = $fragmentedRedisCache;

        $this->cacheService->setPrefix(static::CACHE_BUCKET_PREFIX);
        $this->cacheService->setHashFieldPrefix(static::CACHE_FIELD_PREFIX);
    }

    /**
     * Call endpoint to fetch user's status
     *
     * @param string $id User ID
     * @return json User information
     *
     */
    public function getStatus(string $id)
    {
        if ($this->cacheService->exists($id)) {
            return $this->cacheService->get($id)['status'];
        } else {
            $status = null;
            $user = $this->getUserFromDataStore($id);

            if (!empty($user)) {
                $status = $user['status'];
            }

            return $status;
        }
    }

    private function getUserFromDataStore($id)
    {
        $userResponse = $this->userRequestService->getUserStatus($id);

        if ($userResponse->getStatusCode() === Response::HTTP_OK) {
            $userResponseData = json_decode($userResponse->getData(), true);
        } else {
            $userResponseData = [];
        }

        return $userResponseData;
    }

    /**
     * Get account users from user_essential_data table
     *
     * @param int $accountId Account ID where the user belongs to
     * @param int|null $companyId Company ID where the user belongs to
     * @param string|null $keyword User name keyword
     * @return array array of user data
     *
     */
    public function getUsersFromEssentialData(int $accountId, int $companyId = null, string $keyword = null)
    {
        $data = UserEssentialData::select([
                'user_id as id',
                'employee_id',
                'company_id',
                'account_id',
                'data'
            ])
            ->where('account_id', $accountId)
            ->where('data', '<>', null)
            ->when($companyId, function ($q) use ($companyId) {
                $q->where('company_id', $companyId);
            })
            ->when($keyword, function ($q) use ($keyword) {
                $q->whereRaw('LOWER(JSON_EXTRACT(data, "$.user_full_name")) like ?', "%" . strtolower($keyword) . "%");
            })
            ->groupBy('user_id')
            ->get();

        return $data->map(function ($item) {
            $name = isset($item['data']['user_full_name'])
                            ? $item['data']['user_full_name']
                            : null;
            $item['name'] = ucwords(strtolower($name));
            unset($item['data']);
            return $item;
        })->toArray();
    }

    /**
     * Get account users from user_essential_data table
     *
     * @param int $accountId Account ID where the user belongs to
     * @param int|null $companyId Company ID where the user belongs to
     * @param string|null $keyword User name keyword
     * @return array array of user data
     *
     */
    public function getCompaniesFromEssentialData(int $accountId, int $userId)
    {
        $data = UserEssentialData::select([
                'company_id'
            ])
            ->distinct()
            ->where('user_id', $userId)
            ->where('account_id', $accountId)
            ->where('data', '<>', null)
            ->get();
        return $data;
    }

    /**
     * Get account users from user_essential_data table
     *
     * @param int $accountId Account ID where the user belongs to
     * @param int|null $companyId Company ID where the user belongs to
     * @param string|null $keyword User name keyword
     * @return array array of user data
     *
     */
    public function getEmployeeDataFromEssentialData(int $accountId, int $userId)
    {
        $data = UserEssentialData::select([
                'department_id',
                'position_id',
                'location_id',
                'team_id',
                'employee_id',
                'payroll_group_id'
            ])
            ->where('user_id', $userId)
            ->where('account_id', $accountId)
            ->where('data', '<>', null)
            ->where('employee_id', '<>', null)
            ->get();
        return $data;
    }

    /**
     * Get employee relations from user_essential_data table
     *
     * @param int $accountId Account ID where the user belongs to
     * @param int $userId User ID where the user belongs to
     * @param int $companyId Company ID where the user belongs to
     * @param int $employeeId Employee ID where the user belongs to
     *
     */
    public function getEmployeeRelationsData(int $accountId, int $userId, int $employeeId = null)
    {
        $companyData = $this->getCompaniesFromEssentialData(
            $accountId,
            $userId
        );

        if (!empty($companyData)) {
            $companyIds = collect($companyData)->pluck('company_id')->unique()->all();
            $attributes['company_ids'] = $companyIds;
        }

        $employeeData = $this->getEmployeeDataFromEssentialData(
            $accountId,
            $userId
        );

        if (!empty($employeeData)) {
            $departmentIds = collect($employeeData)->pluck('department_id')->unique()->all();
            $positionIds = collect($employeeData)->pluck('position_id')->unique()->all();
            $locationIds = collect($employeeData)->pluck('location_id')->unique()->all();
            $teamIds = collect($employeeData)->pluck('team_id')->unique()->all();
            $payrollGroupIds = collect($employeeData)->pluck('payroll_group_id')->unique()->all();
            if ($employeeId == null) {
                $employeeId = collect($employeeData)->pluck('employee_id')->unique()->all();
            }

            $attributes['id'] = $employeeId;
            if (!empty($departmentIds)) {
                $attributes['department'] = $departmentIds[0];
            }
            if (!empty($positionIds)) {
                $attributes['position'] = $positionIds[0];
            }
            if (!empty($teamIds)) {
                $attributes['teams'] = $teamIds;
            }
            if (!empty($locationIds)) {
                $attributes['locations'] = $locationIds;
            }
            if (!empty($payrollGroupIds)) {
                $attributes['payroll_group'] = $payrollGroupIds;
            }
            $attributes['user'] = $userId;
        }

        return $attributes;
    }
}
