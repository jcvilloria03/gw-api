<?php

namespace App\User;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class UserAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_BATCH_CREATE = 'batch_create';
    const ACTION_DELETE = 'delete';
    const ACTION_UPDATE = 'update';

    const OBJECT_NAME = 'user';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    /**
     * @var \App\User\UserRequestService
     */
    protected $userRequestService;

    public function __construct(AuditService $auditService, UserRequestService $userRequestService)
    {
        $this->auditService = $auditService;
        $this->userRequestService = $userRequestService;
    }

    /**
     * Log User related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
        }
    }

    /**
     * Log user create
     *
     * @param array $cacheItem
     * @param boolean $isBatch is batch create
     */
    public function logCreate(array $cacheItem, bool $isBatch = false)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => $isBatch ? self::ACTION_BATCH_CREATE : self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => $data
        ]);

        $this->auditService->log($item);
    }

    /**
     * Log role update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new
            ]
        ]);
        $this->auditService->log($item);
    }

     /**
     *
     * Log User Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $userData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => 0,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $userData->id
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log batch create users
     *
     * @param array $ids users ids
     * @param array $user
     * @return void
     */
    public function logBatchCreate(array $ids, array $user)
    {
        foreach ($ids as $id) {
            try {
                $response = $this->userRequestService->get($id);
                $userData = json_decode($response->getData(), true);

                $this->logCreate([
                    'new' => json_encode($userData),
                    'user' => json_encode($user),
                ], true);
            } catch (\Exception $e) {
                \Log::error($e->getMessage() . ' : Could not audit Leave Credit batch create with id = ' . $id);
            }
        }
    }
}
