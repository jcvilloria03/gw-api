<?php

namespace App\User;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;
use App\Model\Role;

class UserAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.user';
    const CREATE_TASK = 'create.user';
    const UPDATE_TASK = 'edit.user';
    const DELETE_TASK = 'delete.user';

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, self::VIEW_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeCreate(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, self::CREATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeUpdate(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, self::UPDATE_TASK);
    }

    /**
     * Authorize by User Type
     *
     * @param \stdClass $user
     * @param int $companyId
     *
     * @return boolean
     */
    public function authorizeUserType(\stdClass $user, $companyId)
    {
        $role = Role::where('company_id', $companyId)
            ->whereHas('userRoles', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            })->first();

        $isAdmin = false;
        if ($role) {
            $isAdmin = $role->type === 'Admin';
        }

        return $user->user_type === 'owner' || $user->user_type === 'super admin' || $isAdmin;
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, self::DELETE_TASK);
    }

    /**
     * Authorize related tasks
     *
     * @param \stdClass $model
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    protected function authorizeTask(
        \stdClass $model,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for account
            return $accountScope->inScope($model->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as account
                return $model->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($model->company_id);
        }

        return false;
    }
}
