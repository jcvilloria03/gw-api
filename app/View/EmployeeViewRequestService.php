<?php

namespace App\View;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EmployeeViewRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get the settings of an Employee View
     *
     * @param int $id Employee View Id
     * @return \Illuminate\Http\JsonResponse Employee View settings
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/employee/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get the items of an Employee View
     *
     * @param int $id Employee View Id
     * @param array $attributes Employee View Pagination / Sorting options
     * @return \Illuminate\Http\JsonResponse Employee View items
     */
    public function getItems(int $id, array $attributes)
    {
        $request = new Request(
            'POST',
            "/employee/{$id}/list",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create an Employee View
     *
     * @param array $attributes Employee View fields
     * @return \Illuminate\Http\JsonResponse Created Employee View
     */
    public function create(array $attributes)
    {
        $request = new Request(
            'POST',
            '/employee',
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create an Employee View
     *
     * @param int $id Employee View Id
     * @param array $attributes Employee View fields
     * @return \Illuminate\Http\JsonResponse Updated Employee View
     */
    public function update(int $id, array $attributes)
    {
        $request = new Request(
            'PATCH',
            "/employee/{$id}",
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($attributes)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get the list of Employee Views of User
     *
     * @param int $userId User Id
     * @return \Illuminate\Http\JsonResponse List of Employee Views
     */
    public function getUserEmployeeViews(int $userId)
    {
        $request = new Request(
            'GET',
            "/employee/user/{$userId}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get list of fields in Employee View
     *
     * @return \Illuminate\Http\JsonResponse Employee View results
     */
    public function getFields()
    {
        $request = new Request(
            'GET',
            "/employee/fields"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get list of available and required columns in Employee View
     *
     * @return \Illuminate\Http\JsonResponse Employee View available and required columns list
     */
    public function getColumns()
    {
        $request = new Request(
            'GET',
            "/employee/columns"
        );

        return $this->send($request);
    }
}
