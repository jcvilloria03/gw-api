<?php

namespace App\View;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class FilterViewRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get list of filter types and allowed conditions per type
     *
     * @return \Illuminate\Http\JsonResponse Filter types and conditions results
     */
    public function getTypes()
    {
        $request = new Request(
            'GET',
            "/filter/types"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get list of available filter modes
     *
     * @return \Illuminate\Http\JsonResponse list of Filter modes results
     */
    public function getModes()
    {
        $request = new Request(
            'GET',
            "/filter/modes"
        );

        return $this->send($request);
    }
}
