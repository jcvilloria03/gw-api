<?php

namespace App\AffectedEmployee;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class AffectedEmployeeRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to search affected employees
     *
     * @param int $id Company ID
     * @param string $term Search term
     * @param int $limit Results limit
     * @param boolean $withoutUsers exclude users from search result
     * @param AuthzDataScope|null $authzDataScope Authorization Data Scope
     * @return \Illuminate\Http\JsonResponse Search results
     */
    public function search(
        int $id,
        string $term,
        $limit,
        $withoutUsers = false,
        $adminUsers = false,
        $authzDataScope = null
    ) {
        $query = http_build_query([
            'term' => $term,
            'without_users' => $withoutUsers,
            'admin_users' => $adminUsers,
        ]).(empty($limit) ? '' : "&limit={$limit}");

        return $this->send(new Request(
            'GET',
            "/company/{$id}/affected_employees/search?{$query}"
        ), $authzDataScope);
    }
}
