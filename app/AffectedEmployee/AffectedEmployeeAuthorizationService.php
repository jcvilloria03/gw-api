<?php

namespace App\AffectedEmployee;

use App\Department\DepartmentAuthorizationService;
use App\Position\PositionAuthorizationService;
use App\Employee\EmployeeAuthorizationService;
use App\Location\LocationAuthorizationService;

class AffectedEmployeeAuthorizationService
{

     /**
     * @var \App\Department\DepartmentAuthorizationService
     */
    private $departmentAuthorizationService;

    /**
     * @var \App\Position\PositionAuthorizationService
     */
    private $positionAuthorizationService;

    /**
     * @var \App\Location\LocationAuthorizationService
     */
    private $locationAuthorizationService;

    /**
     * @var \App\Employee\EmployeeAuthorizationService
     */
    private $employeeAuthorizationService;


    public function __construct(
        DepartmentAuthorizationService $departmentAuthorizationService,
        PositionAuthorizationService $positionAuthorizationService,
        EmployeeAuthorizationService $employeeAuthorizationService,
        LocationAuthorizationService $locationAuthorizationService
    ) {
        $this->departmentAuthorizationService = $departmentAuthorizationService;
        $this->positionAuthorizationService = $positionAuthorizationService;
        $this->locationAuthorizationService = $locationAuthorizationService;
        $this->employeeAuthorizationService = $employeeAuthorizationService;
    }
    /**
     * @param \stdClass $data
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $data, array $user)
    {
        return $this->departmentAuthorizationService->authorizeGet($data, $user)
            && $this->positionAuthorizationService->authorizeGet($data, $user)
            && $this->locationAuthorizationService->authorizeGet($data, $user)
            && $this->employeeAuthorizationService->authorizeGet($data, $user);
    }
}
