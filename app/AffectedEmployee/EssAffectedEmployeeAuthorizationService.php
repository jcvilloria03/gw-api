<?php

namespace App\AffectedEmployee;

use App\Authorization\EssBaseAuthorizationService;

class EssAffectedEmployeeAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.profile';

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeSearch(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
