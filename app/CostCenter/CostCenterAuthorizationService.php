<?php

namespace App\CostCenter;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;
use Log;

class CostCenterAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.cost_center';
    const CREATE_TASK = 'create.cost_center';
    const DELETE_TASK = 'delete.cost_center';
    const UPDATE_TASK = 'edit.cost_center';

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $costCenter, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyCostCenters(\stdClass $costCenter, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $costCenter, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $costCenter, array $user)
    {
        #Log::debug("User: ". $user);
        #Log::debug("Cost Center: ".  $costCenter);
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::DELETE_TASK);
    }

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $costCenter, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $costCenter
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $costCenter, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($costCenter, $user, self::CREATE_TASK);
    }

    /**
     * Authorize cost center related tasks
     *
     * @param \stdClass $costCenter
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $costCenter,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for cost center's account
            return $accountScope->inScope($costCenter->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as cost centers's account
                return $costCenter->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($costCenter->company_id);
        }

        return false;
    }
}
