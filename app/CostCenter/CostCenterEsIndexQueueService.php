<?php

namespace App\CostCenter;

use App\ES\ESIndexQueueService;

class CostCenterEsIndexQueueService
{
    const TYPE = 'cost_center';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created Cost Centers for indexing
     * @param array $costCenterIds Cost Center Ids to be indexed
     */
    public function queue(array $costCenterIds)
    {
        $details = [
            'id' => $costCenterIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
