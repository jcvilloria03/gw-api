<?php

namespace App\CostCenter;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class CostCenterRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get cost center info
     *
     * @param int $id Cost Center ID
     * @return \Illuminate\Http\JsonResponse Cost Center Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/cost_center/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create cost center
     *
     * @param array $data cost center information
     * @return \Illuminate\Http\JsonResponse Created Cost Center
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/cost_center",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple cost center
    *
    * @param array $data cost center to delete informations
    * @return \Illuminate\Http\JsonResponse Deleted Cost Center
    */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/cost_center/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update cost center
     *
     * @param int $costCenterId cost center ID
     * @param array $data cost center information
     * @return \Illuminate\Http\JsonResponse Updated Cost Center
     */
    public function update(int $costCenterId, array $data)
    {
        $request = new Request(
            'PUT',
            "/cost_center/{$costCenterId}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if cost center name is available
     *
     * @param int $companyId Company ID
     * @param array $data cost center information
     * @return \Illuminate\Http\JsonResponse Availability of cost center name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/cost_center/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all cost center within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company cost center
     */
    public function getCompanyCostCenters(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/cost_centers"
        );

        return $this->send($request);
    }
}
