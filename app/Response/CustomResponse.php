<?php

namespace App\Response;

use Symfony\Component\HttpFoundation\Response;

class CustomResponse extends Response
{
    const HTTP_ACCESS_REVOKE = 309;

    const ACTION_CODE_REDIRECT = 1;
    const ACTION_CODE_LOGOUT = 2;
    const ACTION_CODE_RESTRICT = 3;

    const CONTACT_ADMIN = 'Please contact your administrator for more information.';

    const LOGOUT_SEMIACTIVE_MSG = 'Your employee status has been updated to semi-active. ' . self::CONTACT_ADMIN;
    const LOGOUT_INACTIVE_MSG = 'Your employee status has been updated to inactive. '. self::CONTACT_ADMIN;
    const REDIRECT_ESS_MSG = 'Your employee status has been updated to semi-active. '. self::CONTACT_ADMIN;
    const SEMI_ACTIVE_RESTRICT_MSG = 'You are no longer authorized to access the ESS. '. self::CONTACT_ADMIN;

    const ADMIN_LOGOUT_SEMIACTIVE_MSG = 'Your user status has been updated to semi-active.';
    const ADMIN_LOGOUT_INACTIVE_MSG = 'Your user status has been updated to inactive.';
    const ADMIN_SEMI_ACTIVE_RESTRICT_MSG = "You are no longer authorized to access the Admin Dashboard.";

    const ACCOUNT_DELETION_RESTRICT_MSG = 'Your account is being deleted. ' . self::CONTACT_ADMIN;
}
