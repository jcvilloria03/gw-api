<?php

namespace App\Adjustment;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class PhilippineAdjustmentRequestService extends RequestService
{
    /**
     * Call endpoint to assign allowances
     *
     * @param int $id Company ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkCreate(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/company/{$id}/adjustment/bulk_create",
            ['Content-Type' => 'application/x-www-form-urlencoded'],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to update adjustment.
     *
     * @param int $id Adjustment ID
     * @param array $data Request data
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, array $data, AuthzDataScope $dataScope = null)
    {
        return $this->send(new Request(
            'PATCH',
            "/philippine/adjustment/{$id}",
            ['Content-Type' => 'application/json'],
            json_encode($data)
        ), $dataScope);
    }
}
