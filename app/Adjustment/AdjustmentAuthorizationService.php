<?php

namespace App\Adjustment;

use App\Common\CommonAuthorizationService;

class AdjustmentAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.adjustments';
    public $createTask = 'create.adjustments';
    public $updateTask = 'edit.adjustments';
    public $deleteTask = 'delete.adjustments';
}
