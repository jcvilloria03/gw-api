<?php

namespace App\Exceptions;

use Exception;

/**
 * Exception for handling Auth0 batch management jobs errors.
 *
 * Used for re-queuing messages to be sent later when Auth0 rate limits are lifted.
 */
class Auth0BatchCreateJobException extends Exception
{

}
