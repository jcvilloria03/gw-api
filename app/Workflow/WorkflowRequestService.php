<?php

namespace App\Workflow;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class WorkflowRequestService extends RequestService
{
    /**
     * Call endpoint to get workflow
     *
     * @param int $id Workflow ID
     * @param int $accountId Account ID
     *
     * @return \Illuminate\Http\JsonResponse Workflow Info
     */
    public function get(int $id, int $accountId)
    {
        return $this->send(new Request('GET', "/workflow/{$id}?account_id={$accountId}"));
    }

    /**
     * Call endpoint to get all workflows within company
     *
     * @param int    $companyId Company Id
     * @param string $query     Search term
     *
     * @return \Illuminate\Http\JsonResponse List of company workflows
     */
    public function getCompanyWorkflows(int $companyId, string $query)
    {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/workflows?{$query}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ));
    }

    /**
     * Call endpoint to create workflow
     *
     * @param array $data workflow information
     *
     * @return \Illuminate\Http\JsonResponse Created Workflow
     */
    public function create(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/workflow",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to update workflow for given id
     *
     * @param array $data workflow informations
     * @param int   $id   workflow Id
     *
     * @return \Illuminate\Http\JsonResponse Updated workflow
     */
    public function update(array $data, int $id)
    {
        return $this->send(new Request(
            'PUT',
            "/workflow/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to check if workflow name is available
     *
     * @param int   $companyId Company ID
     * @param array $data      workflow information
     *
     * @return \Illuminate\Http\JsonResponse Availability of workflow name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        return $this->send(new Request(
            'POST',
            "/company/{$companyId}/workflow/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to delete multiple workflows
     *
     * @param array $data information on workflows to delete
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkDelete(array $data)
    {
        return $this->send(new Request(
            'DELETE',
            "/workflow/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to check if workflows are in use.
     *
     * @param array $data information on workflows to check in use
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInUse(array $data)
    {
        return $this->send(new Request(
            'POST',
            "/workflow/check_in_use",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }

    /**
     * Call endpoint to get workflows for given employee
     *
     * @param int $employeeId
     *
     * @return array|null
     */
    public function getEmployeeWorkflows(int $employeeId)
    {
        return $this->send(new Request('GET', "/employee/{$employeeId}/workflows"));
    }

    /**
     * Call endpoint to get workflows for a given user
     *
     * @param int $userId User ID
     * @param bool $includeAsEmployee
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserWorkflows(int $userId, bool $includeAsEmployee = false)
    {
        $query = $includeAsEmployee ? '?include_as_employee=true' : '';

        return $this->send(new Request('GET', "/user/{$userId}/workflows{$query}"));
    }

    /**
     * Call endpoint to get workflow levels and approvers
     *
     * @param int    $id workflow Id
     *
     * @return \Illuminate\Http\JsonResponse List of workflow user ids
     */
    public function getWorkflowLevels(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/workflow/{$id}/levels",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ));
    }
}
