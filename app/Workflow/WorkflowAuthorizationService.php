<?php

namespace App\Workflow;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class WorkflowAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.workflow';
    const CREATE_TASK = 'create.workflow';
    const UPDATE_TASK = 'edit.workflow';
    const DELETE_TASK = 'delete.workflow';

    /**
     * @param \stdClass $workflow
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $workflow
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyWorkflows(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $workflow
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $workflow
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $workflow
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $workflow
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $workflow, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($workflow, $user, self::DELETE_TASK);
    }

    /**
     * Authorize workflow related tasks
     *
     * @param \stdClass $workflow
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $workflow,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for workflow account
            return $accountScope->inScope($workflow->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as workflow's account
                return $workflow->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($workflow->company_id);
        }

        return false;
    }
}
