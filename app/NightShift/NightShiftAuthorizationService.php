<?php

namespace App\NightShift;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class NightShiftAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.night_shift';
    const UPDATE_TASK = 'edit.night_shift';

    /**
     * @param \stdClass $nightShift
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $nightShift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($nightShift, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $nightShift
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyNightShift(\stdClass $nightShift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($nightShift, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $nightShift
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $nightShift, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($nightShift, $user, self::UPDATE_TASK);
    }

    /**
     * Authorize project related tasks
     *
     * @param \stdClass $nightShift
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $nightShift,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for project account
            return $accountScope->inScope($nightShift->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as project's account
                return $nightShift->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($nightShift->company_id);
        }

        return false;
    }
}
