<?php

namespace App\NightShift;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class NightShiftRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get night shift.
     *
     * @param int $id Night Shift ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function get(int $id)
    {
        return $this->send(new Request(
            'GET',
            "/night_shift/{$id}"
        ));
    }

    /**
     * Call endpoint to get night shift setings for the company.
     *
     * @param int $companyId Company ID
     * @return \Illuminate\Http\JsonResponse Day/Hour rates for the company
     */
    public function getCompanyNightShift($companyId)
    {
        return $this->send(new Request(
            'GET',
            "/company/{$companyId}/night_shift"
        ));
    }

    /**
     * Call endpoint to update night shift.
     *
     * @param int $id Night Shift ID
     * @param array $data
     * @return \Illuminate\Http\JsonResponse Updated night shift
     */
    public function update(int $id, array $data)
    {
        return $this->send(new Request(
            'PUT',
            "/night_shift/{$id}",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        ));
    }
}
