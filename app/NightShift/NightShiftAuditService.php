<?php

namespace App\NightShift;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class NightShiftAuditService
{
    const ACTION_UPDATE = 'update';
    const OBJECT_NAME = 'night_shift';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Project related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
        }
    }

    /**
     * Log night shift update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);

        $this->auditService->log($item);
    }
}
