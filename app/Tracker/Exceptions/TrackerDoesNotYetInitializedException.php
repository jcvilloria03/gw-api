<?php

namespace App\Tracker\Exceptions;

use Throwable;

class TrackerDoesNotYetInitializedException extends \Exception
{
    public function __construct($message = "Tracker not yet initialized", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
