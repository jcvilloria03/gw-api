<?php

namespace App\Tracker;

use Ramsey\Uuid\Uuid;

class TrackingId
{
    /**
     * @var string
     */
    private $id;

    public static function generate(int $length): self
    {
        return new static(substr(bin2hex(random_bytes((int) ceil($length / 2))), 0, $length));
    }

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->id;
    }
}
