<?php

namespace App\Tracker;

use App\Tracker\Exceptions\TrackerDoesNotYetInitializedException;
use Illuminate\Http\Request;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

class Tracker
{
    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    private $configs;

    /**
     * @var \App\Tracker\TrackingId|null
     */
    private $trackingId;

    /**
     * @var string|null
     */
    private $trackingSource;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $clientIp;

    /**
     * @var string[]
     */
    private $clientIps;

    /**
     * @var bool
     */
    private $initialized;

    public function __construct(Logger $logger, array $configs)
    {
        $this->configs = new ParameterBag($configs);
        $this->logger = $logger;
        $this->initialized = false;
    }

    public function initFromRequest(Request $request): self
    {
        if ($this->initialized) {
            return $this;
        }

        $this->request = clone $request;
        $this->clientIp = $request->getClientIp();
        $this->clientIps = $request->getClientIps();
        $trackingHeaderName = $this->configs->get('header_name', 'X-Tracking-Id');
        $sourceTrackingHeaderName = $trackingHeaderName . '-Src';
        if (!$request->hasHeader($trackingHeaderName)) {
            $this->trackingId = TrackingId::generate($this->configs->get('tracking_id_length', 20));
            $this->trackingSource = $this->configs->get('app_name');
        } else {
            $this->trackingId = new TrackingId($this->request->headers->get($trackingHeaderName));
            $this->trackingSource = $this->request->headers->get($sourceTrackingHeaderName, '');
        }

        $this->logger->pushProcessor([$this, 'processLogRecord']);

        return $this;
    }

    public function processLogRecord(array $record): array
    {
        $record['extra'] += [
            'tracking_id' => $this->getTrackingId()->toString(),
            'tracking_source' => $this->getTrackingSource(),
            'client_ip' => $this->request->getClientIp(),
            'client_ips' => $this->request->getClientIps(),
        ];

        return $record;
    }

    public function init(string $trackingId, string $trackingSource, string $clientIp, array $clientIps): self
    {
        if ($this->initialized) {
            return $this;
        }

        $this->trackingId = new TrackingId($trackingId);
        $this->trackingSource = $trackingSource;
        $this->clientIps = $clientIps;
        $this->clientIp = $clientIp;
        $this->initialized = true;

        return $this;
    }

    public function getTrackingId(): TrackingId
    {
        if ($this->trackingId === null) {
            $this->trackingId = TrackingId::generate($this->configs->get('tracking_id_length', 20));
        }

        return $this->trackingId;
    }

    public function getTrackingSource()
    {
        if ($this->trackingSource === null) {
            $this->trackingSource = strtolower(str_replace(' ', '_', $this->configs->get('app_name')));
        }

        return $this->trackingSource;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getClientIp()
    {
        if ($this->clientIp === null) {
            return '127.0.0.1';
        }

        return $this->clientIp;
    }

    public function getClientIps()
    {
        if ($this->clientIps === null) {
            $this->clientIps = [];
        }

        return $this->clientIps;
    }

    public function getServerIp()
    {
        if ($this->request === null) {
            return '127.0.0.1';
        }

        return $this->request->server('SERVER_ADDR');
    }

    public function getRequestIps()
    {
        return array_merge($this->getClientIps(), [$this->getServerIp()]);
    }

    public function getHeaderTrackingName()
    {
        return $this->configs->get('header_name', 'X-Tracking-Id');
    }

    public function getHeaderTrackingSourceName()
    {
        return $this->getHeaderTrackingName() . '-Src';
    }
}
