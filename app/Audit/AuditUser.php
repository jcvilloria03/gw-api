<?php

namespace App\Audit;

class AuditUser {

    protected $id;

    protected $accountId;

    public function __construct(int $id, int $accountId)
    {
        $this->id = $id;
        $this->accountId = $accountId;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->accountId,
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAccountId()
    {
        return $this->accountId;
    }
}
