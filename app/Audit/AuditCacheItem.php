<?php

namespace App\Audit;

use Illuminate\Support\Facades\Redis;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class AuditCacheItem {

    const KEY_PREFIX = 'audit:';

    const REDIS_KEY_EXPIRY = 3600; // 2 hours

    protected $key;
    protected $class;
    protected $action;
    protected $user;
    protected $old;
    protected $new;

    public function __construct(
        string $class,
        string $action,
        AuditUser $user,
        array $details
    ) {
        // depricated old audit trail
        return;
        $this->setClass($class);
        $this->setAction($action);
        $this->setUser($user);
    }

    /**
     *
     * Set class to handle logging
     *
     * @param string $class
     *
     */
    public function setClass(string $class)
    {
        if (
            empty($class) ||
            !class_exists($class)
        ) {
            throw new AuditException('Specified class does not exist');
        }
        $this->class = $class;
    }

    /**
     *
     * Set action to log
     *
     * @param string $action
     *
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     *
     * Set user doing the audited action
     *
     * @param array $user User details
     *
     */
    public function setUser(AuditUser $user)
    {
        $this->user = $user;
    }

    /**
     *
     * Set old data
     *
     * @param array $old Old object details
     *
     */
    public function setOld(array $old)
    {
        $this->old = $old;
    }

    /**
     *
     * Set new data
     *
     * @param array $new New object details
     *
     */
    public function setNew(array $new)
    {
        $this->new = $new;
    }

    /**
     *
     * Get Cache Key
     *
     * @return string $key
     *
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     *
     * Set type
     *
     * @param array $type
     *
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     *
     * Generate Cache Key
     *
     */
    protected function generateKey()
    {
        $this->key = self::KEY_PREFIX . $this->user->getId() . ':' . uniqid();
    }

    /**
     *
     * Cache data for logging later
     *
     */
    public function cache()
    {
        return;
    }
}
