<?php

namespace App\Audit;

class AuditItem {

    const JSON_EMPTY_STRING = '""';

    protected $companyId;
    protected $accountId;
    protected $userId;
    protected $action;
    protected $objectName;
    protected $data;

    public function __construct(array $vars)
    {
        $this->setAccountId($vars['account_id']);
        $this->setCompanyId($vars['company_id']);
        $this->setUserId($vars['user_id']);
        $this->setAction($vars['action']);
        $this->setObjectName($vars['object_name']);
        $this->setData($vars['data']);
    }

    public function setAccountId(int $id)
    {
        $this->accountId = $id;
    }

    public function setCompanyId(int $id)
    {
        $this->companyId = $id;
    }

    public function setUserId(string $id)
    {
        $this->userId = $id;
    }

    public function setAction(string $action)
    {
        $this->action = $action;
    }

    public function setObjectName(string $name)
    {
        $this->objectName = $name;
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function toArray()
    {
        // make data array ready for json
        array_walk_recursive($this->data, function (&$item) {
            if (empty($item)) {
                // empty strings need to be '""' to be a valid json
                $item = self::JSON_EMPTY_STRING;
            }
        });

        // return array expected by audit endpoint
        return [
             "timestamp" => date('Y-m-d H:i:s'),
             "companyid" => $this->companyId,
             "accountid" => $this->accountId,
             "userid" => $this->userId,
             "action" => $this->action,
             "object_name" => $this->objectName,
             "data" => $this->data,
        ];
    }
}
