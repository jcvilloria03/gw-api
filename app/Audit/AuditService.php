<?php

namespace App\Audit;

use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use Bschmitt\Amqp\Message;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class AuditService
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var string
     */
    private $queue;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->queue = config('queues.audit_queue');
        $this->apiKey = env('AUDIT_LOG_API_KEY');
    }

    /**
     *
     * Call service to log action
     *
     * @param AuditItem $item
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function log(AuditItem $item)
    {
        // depricated old audit trail
        return;
    }


    /**
     *
     * View logs by account id, in descending chronological order
     *
     * @param int $accountId Account to view logs for
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function view(int $accountId)
    {
        $response = $this->client->request(
            'GET',
            "view/{$accountId}",
            [
                'headers' => [
                    'x-api-key' => $this->apiKey
                ]
            ]
        );
        return response()->json($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     *
     * Queue for audit log in consumer
     *
     * @param AuditCacheItem $cacheItem to queue for logging
     *
     */
    public function queue(AuditCacheItem $cacheItem = null)
    {
        // depricated old audit trail
        return;
    }

    /**
     *
     * Retrieve cached audit info
     *
     * @param string $key Redis key of cached info
     *
     * @return array
     *
     */
    public function retrieve(string $key)
    {
        // depricated old audit trail
        return null;
    }
}
