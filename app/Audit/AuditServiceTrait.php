<?php

namespace App\Audit;

use App\Audit\AuditUser;
use App\Audit\AuditCacheItem;

trait AuditServiceTrait
{
    /**
     *
     * Map And Queue Audit Logs
     *
     * @param array $items
     * @param string $action
     * @param array $user
     *
     */
    public function mapAndQueueAuditItems(
        array $items,
        string $action,
        array $user,
        array $oldItems = []
    ) {
        collect($items)
            ->each(function ($item, $key) use ($action, $user, $oldItems) {
                $auditCacheItem = new AuditCacheItem(
                    self::class,
                    $action,
                    new AuditUser(
                        array_get($user, 'user_id'),
                        array_get($user, 'account_id')
                    ),
                    [
                        'old' => $this->getOldData(
                            $action,
                            $item,
                            array_get($oldItems, $key, [])
                        ),
                        'new' => $action !== self::ACTION_DELETE ?
                            $item : [],
                        'type' => snake_case(
                            str_replace(
                                'App\Model\\',
                                '',
                                array_get($item, 'type_name')
                            )
                        )
                    ]
                );

                $this->auditService->queue(
                    $auditCacheItem
                );
            });
    }

    /**
     *
     * Get Audit Logs Old Data
     *
     * @param string $action
     * @param array $item
     * @param array $oldItem
     *
     * @return array
     *
     */
    private function getOldData(string $action, array $item, array $oldItem)
    {
        if ($action === self::ACTION_CREATE) {
            return [];
        }

        if (self::ACTION_UPDATE === $action) {
            return $oldItem;
        }

        return [
            'id' => array_get($item, 'id'),
            'company_id' => array_get($item, 'company_id')
        ];
    }
}
