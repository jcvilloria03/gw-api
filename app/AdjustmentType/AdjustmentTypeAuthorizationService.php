<?php

namespace App\AdjustmentType;

use App\Common\CommonAuthorizationService;

class AdjustmentTypeAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.adjustment_types';
    public $createTask = 'create.adjustment_types';
    public $updateTask = 'edit.adjustment_types';
    public $deleteTask = 'delete.adjustment_types';
}
