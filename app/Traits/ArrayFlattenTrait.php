<?php

namespace App\Traits;

/**
 * Trait ArrayFlatten
 *
 * @package App\Traits
 */
trait ArrayFlattenTrait
{
    /**
     * Flatten multidimensional array using SPL iterator_to_array
     *
     * @param array $array   Multidimensional array
     * @param bool  $useKeys Whether to use the iterator element keys as index.
     *
     * @return array
     */
    public function arrayFlatten(array $array, $useKeys = false)
    {
        return iterator_to_array($this->arrayFlattenIterator($array), $useKeys);
    }

    /**
     * Array iterator generator (recursive call included)
     *
     * @param array $array Multidimensional array
     *
     * @return \Generator
     */
    private function arrayFlattenIterator(array $array)
    {
        foreach ($array as $value) {
            if (\is_array($value)) {
                yield from $this->arrayFlattenIterator($value);
            } else {
                yield $value;
            }
        }
    }
}
