<?php

namespace App\Traits;

use Carbon\Carbon;
use Bschmitt\Amqp\Message;
use Illuminate\Http\Request;
use Bschmitt\Amqp\Facades\Amqp;
use App\Model\UserEssentialData;
use Illuminate\Support\Facades\Log;
use App\User\UserRequestService;
use Exception;

/**
 * Trait AuditTrailTrait
 *
 * @package App\Traits
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
trait AuditTrailTrait
{
    /**
     * This function prepares the data for audit trail queue
     *
     * @param Request $request          The request class, required
     * @param integer|null $companyId   The user's company ID, optional
     * @param array $new                The updated data, optional, for POST, PUT AND PATCH request methods
     * @param array $old                The data before the changes, optional, for DELETE, PUT, PATCH
     * @param bool $logChangesOnly      Indicator for update actions to log only the updated details
     *
     * @return void
     */
    public function audit(
        Request $request,
        $companyId = null,
        array $new = [],
        array $old = [],
        bool $minimalLogs = false
    ) {
        try {
            if (!config('audit_trail.is_enable')) {
                return;
            }

            $uri = $request->route()[1]['uri'];

            if (!$uri) {
                Log::error("[AuditTrailTrait][audit][ERROR]|[" . $request->path() . "]| No URI Found.");
                return;
            }

            $isInAuditEndpoints = $this->isInAuditEndpoints(
                $uri,
                $request->headers->get('X-Authz-Entities')
            );

            if (!$isInAuditEndpoints) {
                return;
            }

            $input = $request->all();
            if (stripos($request->headers->get('content-type'), 'multipart/form-data') !== false) {
                $input = [];
            }

            $ignoreInputEndpoints = config('audit_trail.ignore_input_endpoints');
            if (in_array($uri, $ignoreInputEndpoints)) {
                $input = [];
            }

            if (!$companyId) {
                $data = array_merge($old, $new);
                $companyId = $this->determineCompanyId($request, $data);
            }

            $requestDetails = [
                'method'            => $request->method(),
                'route_uri'         => $request->route()[1]['uri'],
                'authz_entities'    => $request->headers->get('X-Authz-Entities'),
                'authz_company_id'  => $request->headers->get('X-Authz-Company-Id'),
                'headers'           => json_encode($this->formatRequestDetails($request)),
                'inputs'            => $input,
                'user'              => $request->attributes->get('user'),
                'company_id'        => $companyId,
                'new_data'          => $new,
                'old_data'          => $old,
                'action_date'       => date('Y-m-d H:i:s')
            ];

            $this->triggerAuditTrail($requestDetails);
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailTrait][audit][ERROR]|[" . $request->route()[1]['uri'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * Publish log to audit trail microservice queue
     *
     * @param  array    $log
     * @return bool
     */
    protected function publishAuditLog(array $log)
    {
        try {
            if (!$log || !config('audit_trail.is_enable')) {
                return;
            }

            $logData = $this->formatLogDetails($log);

            if (!empty($logData)) {
                $this->queueAuditTrail($logData);
            }
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailTrait][publishAuditLog][ERROR]|[" . $log['route_uri'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }
    }

    /**
     * Check if URI is in auditable endpoints
     *
     * @param  string    $uri
     * @param  string|null $authzEntities   Authz Entities from request
     * @return bool
     */
    protected function isInAuditEndpoints(string $uri, $authzEntities = null)
    {
        $moduleEndpoints = config('audit_trail_endpoints');
        $enabledModules = config('audit_trail.enabled_modules');
        $enabledNonAuthzEndpoints = config('audit_trail.enabled_non_authz_endpoints');

        if (!$authzEntities) {
            if (in_array($uri, $enabledNonAuthzEndpoints)) {
                return true;
            }

            return false;
        }

        // check on specific authz module
        $entities = explode(',', $authzEntities);
        foreach ($entities as $entity) {
            $entity = str_replace(' ', '', $entity);
            if (in_array($entity, $enabledModules)) {
                $modules = array_get($moduleEndpoints, $entity, []);
                if (isset($modules['endpoints']) && in_array($uri, $modules['endpoints'])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * Queue for audit trail trigger in consumer
     *
     * @param array $details to trigger for logging
     *
     * @return void
     */
    public function triggerAuditTrail($details)
    {
        $message = new Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(
            config('queues.audit_queue'),
            $message
        );
    }

    /**
     *
     * Queue for audit trail log in consumer
     *
     * @param array $details to queue for logging
     *
     * @return void
     */
    public function queueAuditTrail($details)
    {
        $message = new Message(
            base64_encode(json_encode($details)),
            [
                'content_type' => 'application/json',
                'delivery_mode' => 1
            ]
        );

        Amqp::publish(
            config('queues.audit_trail_queue'),
            $message,
            [
                'exchange_type' => 'direct',
                'exchange' => config('queues.audit_trail_exchange'),
            ]
        );
    }

    private function validateFields(array $dataFields)
    {
        try {
            $dataKeys = array_keys($dataFields);

            $requiredFields = [
                'method',
                'route_uri',
                'authz_entities',
                'authz_company_id',
                'headers',
                'inputs',
                'user',
                'company_id',
                'new_data',
                'old_data',
                'action_date'
            ];

            $difference = array_diff($dataKeys, $requiredFields);

            if (count($difference) > 0) {
                throw new Exception(implode(',', $difference) ." are missing fields.");
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * Format log details
     *
     * @param  array $request   Request
     * @return array
     */
    public function formatLogDetails(array $request)
    {
        try {
            $this->validateFields($request);

            $data           = null;
            $uri            = $request['route_uri'];
            $method         = $request['method'];
            $authzEntities  = $request['authz_entities'];
            $headers        = $request['headers'];
            $inputs         = $request['inputs'];
            $newValue       = $request['new_data'];
            $oldValue       = $request['old_data'];
            $actionDate     = $request['action_date'];

            $targetCompanyId    = $request['company_id'] ?: $request['authz_company_id'];
            $user               = $this->getUserDetail($request['user'], $targetCompanyId);

            if (!$user) {
                throw new Exception('Unable to audit log record. No user record found.');
            }

            $moduleName     = $this->getModuleName($uri, $authzEntities);
            $actionType     = $this->getModuleActionType($method, $uri, $authzEntities);
            $requestDetails = $headers;
            $companyId      = $user['company']['id'];

            // special handling for bonus/commission/allowance/adjustment
            // to identify the module name from old data
            $isOtherIncomeUri = $uri == '/company/{id}/other_income';
            if ($isOtherIncomeUri && $actionType == 'DELETE' && !empty($oldValue)) {
                $otherIncomeModuleName = $this->getOtherIncomeModuleNameFromData($oldValue);
                $moduleName = $otherIncomeModuleName ?: $moduleName;
            }

            $oldValue = $oldValue ? $this->stripData($oldValue) : [];
            $newValue = $newValue ? $this->stripData($newValue) : [];
            $inputs   = $inputs ? $this->stripData($inputs) : [];

            $oldData = [];
            $newData = [];

            if ($actionType === 'VIEW' && !empty($newValue) && empty($oldValue)) {
                $oldValue = $newValue;
                $newValue = [];
            }

            if ($actionType === 'ADD' && !empty($inputs)) {
                $newData = $inputs;
            }

            if ($actionType === 'UPDATE') {
                if (!empty($inputs)) {
                    $newInputValue = array_intersect_key($inputs, $newValue);
                    $oldInputValue = array_intersect_key($inputs, $oldValue);
                }

                foreach ($inputs as $key => $value) {
                    if (array_key_exists($key, $oldInputValue) && array_key_exists($key, $newInputValue)) {
                        if ($newValue[$key] != $oldValue[$key]) {
                            $oldData[$key] = $oldValue[$key];
                            $newData[$key] = $newValue[$key];
                        }
                    }
                }
            }

            $data = [
                'account_id' => $user['account']['id'],
                'company_id' => $companyId,
                'user_id' => $user['user']['id'],
                'module_name' => $moduleName,
                'action_type' => $actionType,
                'new_value' => !empty($newData) ? $newData : $newValue,
                'old_value' => !empty($oldData) ? $oldData : $oldValue,
                'details' => $user,
                'request_details' => $requestDetails,
                'action_date' => $actionDate
            ];
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailTrait][formatLogDetails][ERROR]|[" . $request['user']['user_id'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }

        return $data;
    }

    private function stripData(array $data)
    {
        try {
            $keys = array_keys($data);
            $hasLink = in_array('links', $keys);
            $hasPage = in_array('page', $keys);
            $hasPerPage = in_array('per_page', $keys);
            $hasData = in_array('data', $keys);

            if (($hasData && ($hasPage || $hasPerPage)) || ($hasData && $hasLink) || count($keys) == 1) {
                $data = $data['data'] ?? $data;
            }

            $this->stripUnnecessaryData($data);
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailTrait][stripData][ERROR] Failed to strip data: " . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }

        return $data;
    }

    private function stripUnnecessaryData(array &$array)
    {
        $keysToRemove = ['deleted_at', 'created_at', 'updated_at', 'jobs', 'previousRouteName'];

        foreach ($array as $key => &$value) {
            if (is_array($value)) {
                $this->stripUnnecessaryData($value);
            } else {
                if (in_array($key, $keysToRemove)) {
                    unset($array[$key]);
                }
            }
        }
    }

    /**
     * Get user, account and company names
     *
     * @param  array $user   user
     * @param  int|null $companyId   Company ID
     * @return array
     */
    public function getUserDetail($user, int $companyId = null)
    {
        $userDetails = null;

        try {
            $companyId = $companyId ?: array_get($user, 'employee_company_id');
            $userData = $this->getUserEssentialData($user['user_id'], $companyId);

            if (empty($userData)) {
                return null;
            }

            if ($userData['data']) {
                $details = $userData['data'];
                $userDetails = [
                    'user' => [
                        'id' => $userData['user_id'],
                        'name' => $details['user_full_name'],
                    ],
                    'account' => [
                        'id' => $userData['account_id'],
                        'name' => $details['account_name'],
                    ],
                    'company' => [
                        'id' => $userData['company_id'],
                        'name' => $details['company_name'],
                    ]
                ];
            }
        } catch (\Throwable $e) {
            $companyId = null;

            Log::error(
                "[AuditTrailTrait][getUserDetails][ERROR]|[" . $user['user_id'] . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());
        }

        return $userDetails;
    }

    private function getUserEssentialData(int $userId, $companyId = null)
    {
        try {
            $user = UserEssentialData::select([
                    'user_id',
                    'account_id',
                    'company_id',
                    'data'
                ])
                ->where('data', '<>', null)
                ->where('user_id', '=', $userId)
                ->where('updated_at', '>=', Carbon::now()->subDay())
                ->when($companyId, function ($q) use ($companyId) {
                    $q->where('company_id', $companyId);
                })
                ->orderBy('company_id', 'asc')
                ->first();

            if ($user === null) {
                $userRequestService = app()->make(UserRequestService::class);
                $essentialData = $userRequestService->getEssentialData($userId, $companyId);

                try {
                    return $essentialData['subject'];
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }
            }

            return $user;
        } catch (\Throwable $e) {
            return null;
        }
    }

    /**
     * Get module name for the given request
     *
     * @param  string $uri   Request uri
     * @param  string|null $authzEntities   Authz Entities from request
     *
     * @return string
     */
    public function getModuleName(string $uri, string $authzEntities = null)
    {
        $moduleName = null;

        try {
            $moduleNames = config('audit_trail.module_names');
            $authzEntities = $this->getAuthzEntities($authzEntities);

            $key = !empty($authzEntities) ? $authzEntities[0] : $uri;

            $isInAuditTrailModuleNames = in_array($key, array_keys($moduleNames));

            if ($isInAuditTrailModuleNames) {
                $moduleName = $moduleNames[$key];
            }

            if (!$moduleName) {
                throw new Exception('No module name found for URI.');
            }
        } catch (\Throwable $e) {
            $moduleName = null;

            Log::error(
                "[AuditTrailTrait][getModuleName][ERROR]|[" . $uri . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());

            throw $e;
        }

        return $moduleName;
    }

    /**
     * Get module name for the given request
     *
     * @param  string $uri   Request uri
     * @param  array|null $data   data to extract the module name from
     *
     * @return string
     */
    public function getOtherIncomeModuleNameFromData(array $data = null)
    {
        $moduleName = null;

        try {
            if ($data && isset($data['type_name']) && $data['type_name']) {
                $types = config('audit_trail.other_incomes_type_entity');
                $entity = isset($types[$data['type_name']]) ? $types[$data['type_name']] : null;

                $moduleNames = config('audit_trail.module_names');
                $isInAuditTrailModuleNames = in_array($entity, array_keys($moduleNames));

                if ($isInAuditTrailModuleNames) {
                    return $moduleNames[$entity];
                }
            }
        } catch (\Throwable $e) {
            $moduleName = null;

            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return $moduleName;
    }

    /**
     * Get module action type for the given request
     *
     * @param  string $method   Request method
     * @param  string $uri   Request uri
     * @param  string|null $authzEntities   Authz Entities from request
     * @return string
     */
    public function getModuleActionType(string $method, string $uri, string $authzEntities = null)
    {
        $methodType         = $method;
        $transactionType    = null;
        $transactionTypes   = config('audit_trail.transaction_types');

        try {
            $uriActions = config('audit_trail.uri_actions');

            // check first if uri is in config uri_actions lists
            $uriExists = in_array($uri, array_keys($uriActions));
            $methodType = $uriExists ? $uriActions[$uri] : null;

            // if not in uri_actions, fetch using authz entity
            if (!$methodType) {
                $authzEntities = $this->getAuthzEntities($authzEntities);

                if (!empty($authzEntities)) {
                    $modules = $this->getModules($method, $uri);
                    $resource = $this->getResource($modules, $authzEntities);

                    if (count($resource) > 0) {
                        $methodType = array_values($resource)[0]['actions'][0];
                    }
                }
            }

            if (!$methodType) {
                throw new Exception('No action type found for URI.');
            }
        } catch (\Throwable $e) {
            Log::error(
                "[AuditTrailTrait][getModuleActionType][ERROR]|[" . $uri . "]|" . $e->getMessage()
            );
            Log::error($e->getTraceAsString());

            throw $e;
        }

        if ($methodType) {
            $transactionType = $transactionTypes[$methodType];
        }

        return $transactionType;
    }

    /**
     * Get and format request details
     *
     * @param  Request $request   Request
     * @return array
     */
    public function formatRequestDetails(Request $request)
    {
        return [
            'uri' => $request->method() . ' ' . $request->path(),
            'headers' => collect($request->headers->all())->only([
                'host',
                'x-authz-entities',
                'x-authz-company-id',
                'user-agent',
                'referer',
                'origin',
                'content-type'
            ])->all(),
            'query' => $request->query->all()
        ];
    }

    /**
     * Get modules for the given route URI and HTTP method
     *
     * @param  string $method   Request method
     * @param  string $uri   Request uri
     * @return array
     */
    protected function getModules(string $method, string $uri)
    {
        $moduleNamesMap = config('modules_map');

        foreach ($moduleNamesMap as $map) {
            if ($map['method'] == $method && $map['url'] === $uri) {
                return $map['modules'];
            }
        }

        return [];
    }

    /**
     * Get resource data for the given module list and authz entities
     *
     * @param  array  $modules          List of modules
     * @param  array  $authzEntities    List of Authz Entities
     * @return array
     */
    protected function getResource(array $modules, array $authzEntities)
    {
        return array_filter($modules, function ($moduleKey) use ($authzEntities) {
            return in_array($moduleKey, $authzEntities);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Get Authz entities
     *
     * @param  string|null $authzEntities   Authz Entities from request
     * @return array
     */
    public function getAuthzEntities(string $authzEntities = null)
    {
        if ($authzEntities) {
            return array_map(
                'trim',
                explode(',', $authzEntities)
            );
        } else {
            return [];
        }
    }

    public function convertCsvToArray($csvFiles)
    {
        try {
            if (($handle = fopen($csvFiles, "r")) !== false) {
                $csvs = [];
                while (!feof($handle)) {
                    $csvs[] = fgetcsv($handle);
                }

                $datas = [];
                $columnNames = [];
                foreach ($csvs[0] as $singleCsv) {
                    $columnNames[] = $singleCsv;
                }

                foreach ($csvs as $key => $csv) {
                    if ($key === 0) {
                        continue;
                    }
                    foreach ($columnNames as $columnKey => $columnName) {
                        $datas[$key-1][$columnName] = $csv[$columnKey];
                    }
                }

                fclose($handle);

                $result = [];
                foreach ($datas as $data) {
                    $item = [];
                    foreach ($data as $key => $value) {
                        if ($value) {
                            $item[$key] = $value;
                        }
                    }

                    if (!empty($item)) {
                        array_push($result, $item);
                    }
                }

                return $result;
            }
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }

        return [];
    }

    /**
     * Determine company ID from request params/url/body/response
     *
     * @param  Request $request Request object
     * @param  array|null $data   request data
     * @return int|null
     */
    private function determineCompanyId(Request $request, array $data = [])
    {
        $route = $request->route();

        $inputParams = $request->only(['company_id', 'companyId']);

        $pathParams = is_array($route)
            ? array_last($route)
            : [];

        $params = array_merge($inputParams, $pathParams);

        $requestedCompanyId = current(array_filter([
            $params['companyId'] ?? null,
            $params['company_id'] ?? null,
        ]));

        // get from path where param name is id after the keyword company
        if (!$requestedCompanyId) {
            $path = $request->path();
            $pathParts = explode('/', $path);

            foreach ($pathParts as $key => $part) {
                if ($part === 'company') {
                    $companyIdkey = $key + 1;
                    if (isset($pathParts[$companyIdkey])) {
                        $requestedCompanyId = $pathParts[$companyIdkey];
                    }
                }
            }
        }

        // get from response
        if (!$requestedCompanyId) {
            $requestedCompanyId = current(array_filter([
                array_get($data, 'companyId'),
                array_get($data, 'company_id'),
                array_get($data, 'data.company_id'),
                array_get($data, 'data.attributes.company_id')
            ]));
        }

        return is_numeric($requestedCompanyId)
            ? (int) $requestedCompanyId
            : null;
    }
}
