<?php

namespace App\Traits;

use Carbon\Carbon;
use InvalidArgumentException;

/**
 * Trait DateTrait
 *
 * @package App\Traits
 */
trait DateTrait
{
    protected $dateFormat = 'Y-m-d';

    /**
     * Generate date range
     *
     * @param string $startDate start date
     * @param string $endDate   end date
     *
     * @return array $dates     array of dates
     */
    public function generateDateRange($startDate, $endDate)
    {
        if (empty($startDate)) {
            throw new InvalidArgumentException('Start date can not be null.');
        }

        $startDate = Carbon::parse($startDate);
        if (empty($endDate)) {
            return [$startDate->format($this->dateFormat)];
        }
        $endDate = Carbon::parse($endDate);

        if ($endDate->lt($startDate)) {
            $temporaryHolderForEndDate = $endDate;
            $endDate = $startDate;
            $startDate = $temporaryHolderForEndDate;
        }

        $dates = [];

        for ($date = $startDate; $date->lte($endDate); $date->addDay()) {
            $dates[] = $date->format($this->dateFormat);
        }
        return $dates;
    }
}
