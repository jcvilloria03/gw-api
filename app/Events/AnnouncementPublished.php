<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AnnouncementPublished extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Announcement model.
     *
     * @var array;
     */
    public $announcement;

    /**
     * ID of user that needs to be notified of new announcement.
     *
     * @var string;
     */
    protected $userId;

    /**
     * @param array $announcement
     * @param int|string $userId
     */
    public function __construct($announcement, $userId)
    {
        $this->announcement = $announcement;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.$this->userId");
    }
}
