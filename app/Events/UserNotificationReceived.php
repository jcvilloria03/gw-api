<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserNotificationReceived extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Notification model.
     *
     * @var array;
     */
    public $notification;

    /**
     * ID of user that needs to be notified of new notification.
     *
     * @var string;
     */
    protected $userId;

    public function __construct($notification, $userId)
    {
        $this->notification = $notification;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.$this->userId");
    }
}
