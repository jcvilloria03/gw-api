<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EssRequestUpdated extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Request model data
     *
     * @var array
     */
    public $request;

    /**
     * Constructor
     *
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('ess-request.' . $this->request['id']);
    }
}
