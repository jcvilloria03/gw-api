<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ClockStateChanged extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    protected $employeeId;

    /**
     * Request model data
     *
     * @var array
     */
    public $response;

    /**
     * Constructor
     *
     * @param array $response
     * @param int $employeeId
     */
    public function __construct(array $response, $employeeId)
    {
        $this->response = $response;
        $this->employeeId = $employeeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('employee.' . $this->employeeId);
    }
}
