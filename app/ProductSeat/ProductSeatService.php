<?php

namespace App\ProductSeat;

use App\ProductSeat\ProductSeatRequestService;

class ProductSeatService
{
    /**
     * @var \App\ProductSeat\ProductSeatService
     */
    private $requestService;

    public function __construct(ProductSeatRequestService $requestService)
    {
        $this->requestService = $requestService;
    }

     /**
     * Get Product Seat Name
     *
     * @param int $id
     * @return string
     */

    public function getProductSeatName(int $id)
    {
        $response = $this->requestService->getProductSeatName($id);
        return $response->getData();
    }

    /**
     * Call endpoint to fetch all product seats.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllAvailable()
    {
        $response = $this->requestService->getAllAvailable();
        return json_decode($response->getData(), true);
    }

    public function getTimeAndAttendanceId()
    {
        $products = $this->getAllAvailable();
        foreach ($products['data'] as $product) {
            if ($product['name'] == 'time and attendance') {
                return $product['id'];
            }
        }
        return;
    }
}
