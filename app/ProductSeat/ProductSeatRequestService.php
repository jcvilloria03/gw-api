<?php

namespace App\ProductSeat;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class ProductSeatRequestService extends RequestService
{
    /**
     * Call endpoint to fetch all product seats.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllAvailable()
    {
        $request = new Request(
            'GET',
            "/available_product_seats"
        );

        return $this->send($request);
    }
    
    /**
    * Get Product Seat Name
    *
    * @param int $id
    * @return string
    */
    public function getProductSeatName(int $id)
    {
        $request = new Request(
            'GET',
            "/product_seat/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch all product seats details.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductSeatsDetails(
        int $accountId,
        array $activeUserIds = [],
        int $companyId = null
    ) {
        $companyIdParam = $companyId ?: '';
        //$data['active_user_ids'] = $activeUserIds;
        //$query = http_build_query($data);

        $request = new Request(
            'POST',
            "/account/{$accountId}/product_seat_details/{$companyIdParam}",
            ['Content-Type' => 'application/json'],
            json_encode([
                "active_user_ids" => $activeUserIds
            ])
        );

        return $this->send($request);
    }
}
