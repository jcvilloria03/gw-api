<?php

namespace App\Cache;

use lastguest\Murmur;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use App\Cache\Contracts\CacheServiceContract;

class FragmentedRedisCacheService implements CacheServiceContract
{
    protected $prefix = '';

    protected $hashFieldPrefix = '';

    protected $bucketCount = 25;

    public function get($key)
    {
        $data = Redis::hGet(
            $this->generateBucketKey($key),
            $this->generateFieldKey($key)
        );

        return !empty($data)
            ? json_decode($data, true)
            : null;
    }

    public function set($key, array $data)
    {
        Redis::hSet(
            $this->generateBucketKey($key),
            $this->generateFieldKey($key),
            json_encode($data)
        );
    }

    public function delete($key)
    {
        Redis::hDel(
            $this->generateBucketKey($key),
            $this->generateFieldKey($key)
        );
    }

    public function exists($key)
    {
        return !empty(Redis::hExists(
            $this->generateBucketKey($key),
            $this->generateFieldKey($key)
        ));
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function setHashFieldPrefix($prefix)
    {
        $this->hashFieldPrefix = $prefix;
    }

    /**
     * Generate bucket key for given identifier
     *
     * @param mix $identifier
     * @return string
     */
    public function generateBucketKey($identifier)
    {

        $bucketKey = Murmur::hash3_int((string) $identifier) % intval($this->bucketCount);

        $key = rtrim($this->prefix, ':')
            . ':bucket:'
            . $bucketKey;

        Log::debug("Bucket key: {$key}");

        return $key;
    }

    /**
     * Generate field key for given field identifier
     *
     * @param string $identifier
     * @return string
     */
    public function generateFieldKey($identifier)
    {
        $key = rtrim($this->hashFieldPrefix, ':')
            . ":{$identifier}";

        Log::debug("Field key: {$key}");

        return $key;
    }
}
