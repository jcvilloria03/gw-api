<?php

namespace App\Cache\Contracts;

interface CacheServiceContract
{
    public function get($key);

    public function set($key, array $data);

    public function exists($key);
}
