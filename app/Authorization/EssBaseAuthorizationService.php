<?php

namespace App\Authorization;

use App\Authorization\AuthorizationService;

abstract class EssBaseAuthorizationService extends AuthorizationService
{
    /**
     * Authorize ESS related tasks
     *
     * @param string $taskType
     * @return bool
     */
    protected function authorizeTask(string $taskType)
    {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        return !empty($taskScopes);
    }
}
