<?php

namespace App\Authorization;

use App\Permission\Scope;
use App\Role\UserRoleService;
use App\Permission\TaskScopes;
use Salarium\Cache\FragmentedRedisCache;
use App\Permission\AuthorizationPermissions;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class AuthorizationService
{
    const CACHE_BUCKET_PREFIX = 'rbac';

    const CACHE_FIELD_PREFIX = 'user';

    /**
     * App\Role\UserRoleService
     */
    private $userRoleService;

    /**
     * App\Permission\AuthorizationPermissions
     */
    private $permissions;

    public function __construct(
        UserRoleService $userRoleService,
        AuthorizationPermissions $permissions,
        FragmentedRedisCache $cacheService
    ) {
        $this->userRoleService = $userRoleService;
        $this->permissions     = $permissions;
        $this->cacheService    = $cacheService;

        $this->cacheService->setPrefix(static::CACHE_BUCKET_PREFIX);
        $this->cacheService->setHashFieldPrefix(static::CACHE_FIELD_PREFIX);
    }

    /**
     * Build user permissions cache
     *
     * @param int $userId
     * @return void
    */

    public function buildUserPermissions(int $userId)
    {
        // $this->buildFromDataStore($userId);
        if ($this->cacheService->exists($userId)) {
            $this->buildFromCache($userId);
        } else {
            $this->buildFromDataStore($userId);
        }
    }

    /**
     * Get task scopes
     *
     * @param array $tasks
     * @return array $taskScopes
    */

    public function getTaskScopes(string $task)
    {
        return $this->permissions->getTaskScopesForTask($task);
    }

    /**
     * Get multiple task scopes
     *
     * @param array $tasks
     * @return array $results
    */

    public function getMultipleTaskScopes(array $tasks)
    {
        $results = [];
        foreach ($tasks as $task) {
            if ($scopes = $this->getTaskScopes($task)) {
                $results[] = $scopes;
            }
        }
        return $results;
    }

    /**
     * Delete cached key (userId) for RBAC
     *
     * @param int $userId
     * @return void
    */

    public function invalidateCachedPermissions(int $userId)
    {
        $this->cacheService->delete($userId);
    }

    /**
     * Delete cached keys (userId) for RBAC
     *
     * @param array $userIds
     * @return void
    */

    public function invalidateUsersCachedPermissions(array $userIds)
    {
        if (!empty($userIds)) {
            foreach ($userIds as $userId) {
                $this->cacheService->delete($userId);
            }
        }
    }

    /**
     * Check if user has module access for task's module access scope
     *
     * @param string $taskName
     * @return boolean
     */
    public function checkTaskModuleAccess(string $taskName)
    {
        // validate module access
        $taskScopes = $this->getTaskScopes($taskName);

        return !empty($taskScopes) && !empty($taskScopes->getModuleScopes());
    }

    /**
     * Build permissions list from data store
     *
     * @param integer $userId
     * @return void
     */
    protected function buildFromDataStore(int $userId)
    {
        $roles = $this->userRoleService->getUserRoles(
            $userId,
            [ 'role.permissions.task' ]
        );

        foreach ($roles as $userRole) {
            $role         = $userRole->role;
            $moduleAccess = $role->module_access ?? [];

            if (!empty($userRole->module_access)) {
                $moduleAccess = array_intersect($moduleAccess, $userRole->module_access);
            }

            foreach ($role->permissions as $permission) {
                // create Task Scope
                $taskScope = new TaskScopes($permission->task->name);

                // create Scope, and add to task scopes
                $key = key($permission->scope);
                if (!empty($permission->scope)) {
                    $scope = new Scope($key, $permission->scope[$key]);
                    $taskScope->addScope($scope);
                }

                // Add module scope
                if (in_array($permission->task->module_access, $moduleAccess)) {
                    $taskScope->addModuleScope([$permission->task->module_access]);
                }

                // add Task Scope to the list of Authorization Permissions
                $this->permissions->addTaskScope($taskScope);
            }
        }

        $this->cacheService->set($userId, $this->permissions->toArray());
    }

    protected function buildFromCache(int $userId)
    {
        $data = $this->cacheService->get($userId);
        foreach ($data as $taskName => $scopes) {
            $taskScope = new TaskScopes($taskName);

            foreach ($scopes['scopes'] as $scope) {
                $scopeObj = new Scope($scope['type'], $scope['target']);
                $taskScope->addScope($scopeObj);
            }

            $taskScope->addModuleScope($scopes['modules']);

            $this->permissions->addTaskScope($taskScope);
        }
    }
}
