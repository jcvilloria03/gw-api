<?php

namespace App\Department;

use App\ES\ESIndexQueueService;

class DepartmentEsIndexQueueService
{
    const TYPE = 'department';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created departments for indexing
     * @param array $departmentIds Department Ids to be indexed
     */
    public function queue(array $departmentIds)
    {
        $details = [
            'id' => $departmentIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
