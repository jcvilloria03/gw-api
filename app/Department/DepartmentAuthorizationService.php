<?php

namespace App\Department;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class DepartmentAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.department';
    const CREATE_TASK = 'create.department';
    const UPDATE_TASK = 'edit.department';
    const DELETE_TASK = 'delete.department';

    /**
     * @param \stdClass $department
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $department
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyDepartments(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $department
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $department
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $department
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::CREATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $department, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($department, $user, self::DELETE_TASK);
    }

    /**
     * Authorize department related tasks
     *
     * @param \stdClass $department
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $department,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for department's account
            return $accountScope->inScope($department->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as departments's account
                return $department->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($department->company_id);
        }

        return false;
    }
}
