<?php

namespace App\Department;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class DepartmentRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get department info
     *
     * @param int $id Department ID
     * @return \Illuminate\Http\JsonResponse Department Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/department/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create department
     *
     * @param array $data department information
     * @return \Illuminate\Http\JsonResponse Created Department
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/department",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if department name is available
     *
     * @param int $companyId Company ID
     * @param array $data department information
     * @return \Illuminate\Http\JsonResponse Availability of department name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/department/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all department within company
     *
     * @param int $companyId Company Id
     * @param array|null $params Query string params
     * @param \App\Authz\AuthzDataScope|null $authzDataScope Authz Data Scope instance
     * @return \Illuminate\Http\JsonResponse List of company department
     */
    public function getCompanyDepartments(int $companyId, array $params = [], AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/departments?" . http_build_query($params)
        );

        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to create multiple departments
     *
     * @param array $data departments information
     * @return \Illuminate\Http\JsonResponse Created Departments
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/department/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update multiple departments
     *
     * @param array $data departments information
     * @return \Illuminate\Http\JsonResponse Created Departments
     */
    public function bulkUpdate(array $data)
    {
        $request = new Request(
            'PUT',
            "/department/bulk_update",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update department for given id
     *
     * @param array $data department informations
     * @param int $id Department Id
     * @return \Illuminate\Http\JsonResponse Updated department
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/department/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

      /**
       * Call endpoint to delete department
       *
       * @param int $id department to delete id
       * @return \Illuminate\Http\JsonResponse Deleted department
       */
    public function delete(int $id)
    {
        $request = new Request(
            'DELETE',
            "/department/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        );
        return $this->send($request);
    }
}
