<?php

namespace App\Payslip;

use App\Authorization\EssBaseAuthorizationService;

class EssPayslipAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.payslip';

    /**
     * @param array $user User details
     * @param array $payslip Payslip details
     * @return bool
     */
    public function authorizeView(array $user, array $payslip)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask(self::VIEW_TASK) && $payslip['employee_id'] == $user['employee_id'];
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function authorizeList(int $userId)
    {
        $this->buildUserPermissions($userId);
        return $this->authorizeTask(self::VIEW_TASK);
    }
}
