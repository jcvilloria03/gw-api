<?php

namespace App\Payslip;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class PayslipRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Payslip information
     *
     * @param int $id Payslip ID
     * @return \Illuminate\Http\JsonResponse Payslip details
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/payslip/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch multiple Payslip information
     *
     * @param  array $data Payslip Request data
     * @return json Payslips information
     *
     */
    public function getMultiple(array $data)
    {
        $request = new Request(
            'POST',
            "/payslip/get_multiple",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to search Payslips
     *
     * @param array $data Payslip search filters
     * @return json PayrollGroup information
     *
     */
    public function search(array $data)
    {
        $request = new Request(
            'GET',
            "/payslips/search?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to download Payslips
     *
     * @param int $id Payslip ID
     * @param array $data Payslip Options
     * @return \Illuminate\Http\JsonResponse Payslip
     */
    public function download(int $id, array $data)
    {
        $request = new Request(
            'GET',
            "/payslip/{$id}/download?" . http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to downloadMultiplePayslips
     *
     * @param  array $data Payslip Request data
     * @return json ZippedPayslip information
     *
     */
    public function downloadMultiple(array $data)
    {
        $request = new Request(
            'POST',
            "/payslip/download_multiple",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Payslips Zip Information
     *
     * @param  int $id ZippedPayslip ID
     * @return json ZippedPayslip information
     *
     */
    public function getZippedPayslips(int $id)
    {
        $request = new Request(
            'GET',
            "/payslip/zipped/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get Payslips Zip URL
     *
     * @param  int $id ZippedPayslip ID
     * @return json ZippedPayslip information
     *
     */
    public function getZippedPayslipsUrl($uuid)
    {
        $request = new Request(
            'GET',
            "/payslip/zipped/{$uuid}/url"
        );
        return $this->send($request);
    }
}
