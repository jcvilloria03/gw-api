<?php

namespace App\Tag;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TagRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get all tags within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company tags
     */
    public function getCompanyTags(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/tags"
        );

        return $this->send($request);
    }
}
