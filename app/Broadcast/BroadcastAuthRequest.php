<?php

namespace App\Broadcast;

use Illuminate\Http\Request;

class BroadcastAuthRequest
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function __get($property)
    {
        return $this->request->input($property);
    }

    public function user()
    {
        return $this->request->attributes->get('user');
    }
}
