<?php

namespace App\Broadcast;

use App\EmployeeRequest\EmployeeRequestAuthorizationService;
use App\EmployeeRequest\EssEmployeeRequestAuthorizationService;
use App\ESS\EssEmployeeRequestRequestService;
use App\Facades\Company;
use App\Workflow\WorkflowRequestService;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EssRequestChannel
{
    /**
     * @var \App\EmployeeRequest\EmployeeRequestAuthorizationService
     */
    private $authorizationService;

    /**
     * @var \App\EmployeeRequest\EssEmployeeRequestAuthorizationService
     */
    private $essAuthorizationService;

    /**
     * @var \App\ESS\EssEmployeeRequestRequestService
     */
    private $employeeRequestService;

    /**
     * @var \App\Workflow\WorkflowRequestService
     */
    private $workflowRequestService;

    /**
     * @param \App\EmployeeRequest\EmployeeRequestAuthorizationService $authorizationService
     * @param \App\EmployeeRequest\EssEmployeeRequestAuthorizationService $essAuthorizationService
     * @param \App\ESS\EssEmployeeRequestRequestService $employeeRequestService
     * @param \App\Workflow\WorkflowRequestService $workflowRequestService
     */
    public function __construct(
        EmployeeRequestAuthorizationService $authorizationService,
        EssEmployeeRequestAuthorizationService $essAuthorizationService,
        EssEmployeeRequestRequestService $employeeRequestService,
        WorkflowRequestService $workflowRequestService
    ) {
        $this->authorizationService = $authorizationService;
        $this->essAuthorizationService = $essAuthorizationService;
        $this->employeeRequestService = $employeeRequestService;
        $this->workflowRequestService = $workflowRequestService;
    }

    /**
     * Authorize access to broadcasting channel.
     *
     * @param array $user
     * @param int $requestId
     * @return bool
     */
    public function __invoke($user, $requestId)
    {
        try {
            $workflowsResponse = $this->workflowRequestService->getUserWorkflows($user['user_id'], true);

            if (!$workflowsResponse->isSuccessful()) {
                return false;
            }

            $employeeRequestResponse = $this->employeeRequestService->get($requestId);

            if (!$employeeRequestResponse->isSuccessful()) {
                return false;
            }
        } catch (HttpException $e) {
            Log::error($e->getMessage());

            return false;
        }

        $workflows = json_decode($workflowsResponse->getData(), true);
        $request = json_decode($employeeRequestResponse->getData());
        $request->account_id = Company::getAccountId($request->company_id);

        return $this->authorizationService->authorizeViewSingleRequest($user, $workflows, $request) ||
            $this->essAuthorizationService->authorizeViewSingleRequest($user, $workflows, $request);
    }
}
