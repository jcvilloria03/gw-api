<?php

namespace App\Commission;

use App\Common\CommonAuthorizationService;

class CommissionAuthorizationService extends CommonAuthorizationService
{
    public $viewTask = 'view.commissions';
    public $createTask = 'create.commissions';
    public $updateTask = 'edit.commissions';
    public $deleteTask = 'delete.commissions';
}
