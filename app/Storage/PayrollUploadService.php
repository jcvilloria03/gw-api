<?php

namespace App\Storage;

class PayrollUploadService extends UploadService
{
    const ATTENDANCE_UPLOAD = 'validate-attendance-file';
    const ALLOWANCE_UPLOAD = 'validate-allowance-file';
    const BONUS_UPLOAD = 'validate-bonus-file';
    const COMMISSION_UPLOAD = 'validate-commission-file';
    const DEDUCTION_UPLOAD = 'validate-deduction-file';
}
