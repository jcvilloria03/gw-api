<?php

namespace App\Storage;

use Aws\S3\S3Client;
use App\Model\CompanyUser;
use Illuminate\Support\Facades\Redis;

class PictureService
{
    /**
     * S3 client
     *
     * @var Aws\S3\S3Client
     */
    protected $s3Client;

    /**
     * Constructor
     *
     * @param Aws\S3\S3Client $s3Client
     */
    public function __construct(S3Client $s3Client)
    {
        $this->s3Client = $s3Client;
        $this->s3Bucket = env('UPLOADS_BUCKET');
    }

    /**
     * Create signed URL to an S3 object
     *
     * @param  string $s3Key S3 object key
     * @param  string $expiry Expiry string
     * @return string
     */
    public function createSignedUrl(string $s3Key, string $expiry = '+10 minutes')
    {
        $data = [
            'Bucket' => $this->s3Bucket,
            'Key' => $s3Key
        ];

        $command = $this->s3Client->getCommand('GetObject', $data);

        return (string) $this->s3Client->createPresignedRequest($command, $expiry)->getUri();
    }

    public function getUserPicture(int $userId)
    {
        $pictureUrl = null;
        try {
            $user = CompanyUser::where([
                    ['user_id', '=', $userId],
                    ['employee_id', '<>', null]
                ])->first();

            if (!empty($user)) {
                $employeeId = $user['employee_id'];

                $redisKey = env('REDIS_EMPLOYEE_PICTURE_KEY', 'employee:picture:') . $employeeId;
                $s3EmployeePictureKey = Redis::get($redisKey);

                if ($s3EmployeePictureKey) {
                    $pictureUrl = $this->createSignedUrl($s3EmployeePictureKey);
                }
            }
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            $pictureUrl = null;
        }

        return $pictureUrl;
    }
}
