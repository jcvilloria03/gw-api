<?php

namespace App\Storage;

use Aws\S3\S3Client;
use Illuminate\Http\UploadedFile;

class UploadService
{
    /**
     * S3 client
     *
     * @var Aws\S3\S3Client
     */
    protected $s3Client;

    /**
     * Constructor
     *
     * @param Aws\S3\S3Client $s3Client
     */
    public function __construct(S3Client $s3Client)
    {
        $this->s3Client = $s3Client;
        $this->s3Bucket = env('UPLOADS_BUCKET');
    }

    /**
     * Get S3 Bucket name
     * @return string
     */
    public function getS3Bucket()
    {
        return $this->s3Bucket;
    }

    /**
     * Save file to S3 bucket
     *
     * @param Illuminate\Http\UploadedFile $file File to be uploaded
     * @param array $user User details of Uploader
     * @param string $acl ACL of uploaded object
     * @return string Generated S3 Key of the uploaded file
     */
    public function saveFile(UploadedFile $file, array $user, string $acl = '')
    {
        $suffix = $user['account_id'] . '-' . $user['user_id'];
        //dd($suffix);
        $s3Key = $this->generateS3Key($file->getClientOriginalName(), $suffix);
        //var_dump($s3Key);
        //var_dump($file->getPathName());
        //dd($acl);
        $this->saveFileToS3($s3Key, $file->getPathName(), $acl);

        return $s3Key;
    }

    /**
     * Generate S3 key depending on what is being uploaded
     *
     * @param string $fileName Original filename of the uploaded file
     * @param string $suffix Suffix string, for unique labeling, organization
     * @return string
     */
    private function generateS3Key(string $fileName, $suffix = '')
    {
        $suffix .= '-' . $this->generateUniqueId();
        $fileNameArr = explode('.', $fileName);
        $fileExtension = array_pop($fileNameArr);
        $fileBasename = implode('.', $fileNameArr);

        return $fileBasename . '-' . $suffix . '.' . $fileExtension;
    }

    /**
     * Generate Unique ID for naming every uploaded file/object
     * @return string
     */
    private function generateUniqueId()
    {
        return str_replace('.', '', uniqid(microtime(true), true));
    }

    /**
     * Save File to S3
     *
     * @param string $s3Key S3 Key of file after upload
     * @param string $path Full path of file to upload
     * @param string $acl ACL of uploaded object
     *
     */
    public function saveFileToS3(string $s3Key, string $path, string $acl = '')
    {
        $data = [
            'Bucket' => $this->s3Bucket,
            'Key' => $s3Key,
            'SourceFile' => $path
        ];

        if (!empty($acl)) {
            $data['ACL'] = $acl;
        }
        //dd($data);
        //bonuses__1___1_-1278-40760
        $this->s3Client->putObject($data);
    }

    /**
     * Create signed URL to an S3 object
     *
     * @param  string $s3Key S3 object key
     * @param  string $expiry Expiry string
     * @return string
     */
    public function createSignedlUrl(string $s3Key, string $expiry = '+10 minutes')
    {
        $data = [
            'Bucket' => $this->s3Bucket,
            'Key' => $s3Key
        ];

        $command = $this->s3Client->getCommand('GetObject', $data);

        return (string) $this->s3Client->createPresignedRequest($command, $expiry)->getUri();
    }
}
