<?php

namespace App\EmployeeRequest;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class EmployeeRequestAuthorizationService extends AuthorizationService
{
    const UPDATE_TASK = 'edit.request';
    const VIEW_TASK = 'view.request';

    /**
     * Authorize admin view task scope.
     *
     * @param \stdClass $request
     * @param array     $user
     *
     * @return bool
     */
    public function authorizeView(\stdClass $request, array $user)
    {
        $this->buildUserPermissions($user['user_id']);

        return $this->authorizeTask($request, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $request
     * @param array $user
     *
     * @return bool
     */
    public function authorizeUpdate($request, $user)
    {
        $this->buildUserPermissions($user['user_id']);

        return $this->authorizeTask($request, $user, self::UPDATE_TASK);
    }

    /**
     * @param array     $user
     * @param array     $workflows
     * @param \stdClass $employeeRequest
     *
     * @return bool
     */
    public function authorizeSendMessage(array $user, array $workflows, \stdClass $employeeRequest)
    {
        return $this->authorizeUpdate($employeeRequest, $user) &&
            $this->authorizeRequestParticipant($user, $workflows, $employeeRequest);
    }

    /**
     * @param array     $user
     * @param array     $workflows
     * @param \stdClass $request
     *
     * @return bool
     */
    private function authorizeRequestParticipant(array $user, array $workflows, \stdClass $request)
    {
        return collect($workflows)->where('workflow_id', $request->workflow_id)->isNotEmpty() ||
            $request->employee_id === $user['employee_id'];
    }

    /**
     * @param array     $user
     * @param array     $workflows
     * @param \stdClass $employeeRequest
     *
     * @return bool
     */
    public function authorizeViewSingleRequest(array $user, array $workflows, \stdClass $employeeRequest)
    {
        $this->buildUserPermissions($user['user_id']);
        $authorized = $this->authorizeTask($employeeRequest, $user, self::VIEW_TASK);

        return $authorized && $this->authorizeRequestParticipant($user, $workflows, $employeeRequest);
    }

    /**
     * Authorize user approvals related tasks
     *
     * @param \stdClass $authorizationRequest
     * @param array     $user
     * @param string    $taskType
     *
     * @return bool
     */
    private function authorizeTask(
        \stdClass $authorizationRequest,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for teams's account
            return $accountScope->inScope($authorizationRequest->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll() || $taskType == self::VIEW_TASK) {
                // check if user's account is same as team's account
                return $authorizationRequest->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($authorizationRequest->company_id);
        }

        return false;
    }
}
