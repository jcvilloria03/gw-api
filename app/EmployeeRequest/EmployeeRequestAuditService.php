<?php

namespace App\EmployeeRequest;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class EmployeeRequestAuditService
{
    const ACTION_SEND_MESSAGE = 'send_message';
    const ACTION_CANCEL_REQUEST = 'cancel_request';
    const ACTION_UPDATE_STATUS = 'update_status';
    const OBJECT_NAME = 'employee_request';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log Employee Request related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_SEND_MESSAGE:
                $this->logSendMessage($cacheItem);
                break;
            case self::ACTION_UPDATE_STATUS:
                $this->logUpdateStatus($cacheItem);
                break;
            case self::ACTION_CANCEL_REQUEST:
                $this->logCancelRequest($cacheItem);
                break;
        }
    }

    /**
     * Log send employee request message
     *
     * @param array $cacheItem
     */
    public function logSendMessage(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'employee' => $user['employee_id'],
            'user_id' => $user['id'],
            'account_id' => $user['account_id'],
            'company_id' => $user['employee_company_id'],
            'action' => self::ACTION_SEND_MESSAGE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'content' => $data['content'],
                'request_id' => $data['request_id']
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log update employee request status
     *
     * @param array $cacheItem
     */
    public function logUpdateStatus(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $newStatus = json_decode($cacheItem['new_status'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'employee' => $user['employee_id'],
            'user_id' => $user['id'],
            'account_id' => $user['account_id'],
            'company_id' => $user['employee_company_id'],
            'action' => self::ACTION_UPDATE_STATUS,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'new_employee_request' => $data,
                'new_status' => $newStatus
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log cancel request
     *
     * @param array $cacheItem
     */
    public function logCancelRequest(array $cacheItem)
    {
        $data = json_decode($cacheItem['request'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'employee' => $user['employee_id'],
            'user_id' => $user['id'],
            'account_id' => $user['account_id'],
            'company_id' => $user['employee_company_id'],
            'action' => self::ACTION_CANCEL_REQUEST,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'request' => $data
            ]
        ]);
        $this->auditService->log($item);
    }
}
