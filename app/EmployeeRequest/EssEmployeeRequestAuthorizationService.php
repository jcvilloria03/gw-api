<?php

namespace App\EmployeeRequest;

use App\Authorization\EssBaseAuthorizationService;

class EssEmployeeRequestAuthorizationService extends EssBaseAuthorizationService
{
    const VIEW_TASK = 'ess.view.request';
    const CREATE_TASK = 'ess.create.request';
    const UPDATE_TASK = 'ess.update.request';
    const APPROVE_TASK = 'ess.approval.request';
    const CANCEL_TASK = 'ess.cancel.request';

    /**
     * @param array $user
     * @param array|null $employeeRequest
     * @return bool
     */
    public function authorizeView($user, $employeeRequest = null)
    {
        $this->buildUserPermissions($user['user_id']);
        $authorized = $this->authorizeTask(self::VIEW_TASK);

        if ($employeeRequest) {
            $authorized = $authorized && $user['employee_id'] === $employeeRequest->employee_id;
        }

        return $authorized;
    }

    /**
     * @param array $user
     * @return bool
     */
    public function authorizeCreate($user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask(self::CREATE_TASK);
    }

    /**
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate($user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask(self::UPDATE_TASK);
    }

    /**
     * Authorize approve.
     *
     * @param int $userId
     * @return bool
     */
    public function authorizeApprove($userId)
    {
        $this->buildUserPermissions($userId);

        return $this->authorizeTask(self::APPROVE_TASK);
    }

    /**
     * @param array $user
     * @param \stdClass|null $employeeRequest
     * @return bool
     */
    public function authorizeAttachmentUpload(array $user, \stdClass $employeeRequest = null)
    {
        $this->buildUserPermissions($user['user_id']);
        $authorized = $this->authorizeTask(self::CREATE_TASK);

        if ($employeeRequest) {
            $authorized = $authorized && $user['employee_id'] === $employeeRequest->employee_id;
        }

        return $authorized;
    }

    /**
     * @param array $user
     * @param array $workflows
     * @param \stdClass $employeeRequest
     * @return bool
     */
    public function authorizeSendMessage(array $user, array $workflows, \stdClass $employeeRequest)
    {
        return $this->authorizeUpdate($user) && $this->authorizeRequestParticipant($user, $workflows, $employeeRequest);
    }

     /**
     * @param array $user
     * @param array $workflows
     * @param \stdClass $employeeRequest
     * @return bool
     */
    public function authorizeViewSingleRequest(array $user, array $workflows, \stdClass $employeeRequest)
    {
        $this->buildUserPermissions($user['user_id']);
        $authorized = $this->authorizeTask(self::VIEW_TASK);

        return $authorized && $this->authorizeRequestParticipant($user, $workflows, $employeeRequest);
    }

     /**
     * @param array $user
     * @param array $workflows
     * @param \stdClass $request
     * @return bool
     */
    private function authorizeRequestParticipant(array $user, array $workflows, \stdClass $request)
    {
        return collect($workflows)->where('workflow_id', $request->workflow_id)->isNotEmpty() ||
            $request->employee_id === $user['employee_id'];
    }

    /**
     * @param array $user
     * @param \stdClass $employeeRequest
     * @return bool
     */
    public function authorizeCancel(array $user, \stdClass $employeeRequest)
    {
        $this->buildUserPermissions($user['user_id']);

        return $this->authorizeTask(self::CANCEL_TASK) && $user['employee_id'] === $employeeRequest->employee_id;
    }
}
