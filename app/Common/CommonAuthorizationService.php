<?php

namespace App\Common;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class CommonAuthorizationService extends AuthorizationService
{
    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->viewTask);
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeGetAll(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->viewTask);
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->createTask);
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->updateTask);
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->deleteTask);
    }

    /**
     * @param \stdClass $model
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $model, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($model, $user, $this->createTask);
    }

    /**
     * Authorize related tasks
     *
     * @param \stdClass $model
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    protected function authorizeTask(
        \stdClass $model,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for account
            return $accountScope->inScope($model->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as account
                return $model->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($model->company_id);
        }

        return false;
    }
}
