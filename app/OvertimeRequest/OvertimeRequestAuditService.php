<?php

namespace App\OvertimeRequest;

use App\Audit\AuditService;
use App\Audit\AuditItem;

class OvertimeRequestAuditService
{
    const ACTION_CREATE = 'create';

    const OBJECT_NAME = 'overtime_request';

    /*
     * App\Audit\AuditService
     */
    protected $auditService;

    public function __construct(
        AuditService $auditService
    ) {
        $this->auditService = $auditService;
    }

    /**
     *
     * Log Payroll related action
     *
     * @param array $cacheItem
     *
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
        }
    }

    /**
     * Log OvertimeRequest create
     * @param  array  $cacheItem
     * @return void
     */
    public function logCreate(array $cacheItem)
    {
        $user = json_decode($cacheItem['user'], true);
        $data = json_decode($cacheItem['new'], true);

        $item = new AuditItem([
            'company_id' => $data['employee_company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
            ],
        ]);

        $this->auditService->log($item);
    }
}
