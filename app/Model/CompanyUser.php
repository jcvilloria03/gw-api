<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
    protected $table = 'companies_users';

    protected $fillable = [
        'user_id',
        'company_id',
        'employee_id'
    ];
}
