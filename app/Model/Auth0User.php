<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Auth0User extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_SEMI_ACTIVE = 'semi-active';
    const STATUS_INACTIVE = 'inactive';

    const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_SEMI_ACTIVE,
        self::STATUS_INACTIVE,
    ];

    const MANUAL_STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
    ];

    protected $fillable = [
        'user_id',
        'account_id',
        'auth0_user_id',
        'status'
    ];

    public function userRoles()
    {
        return $this->hasMany(UserRole::class, 'user_id', 'user_id');
    }

    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }
}
