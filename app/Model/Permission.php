<?php

namespace App\Model;

use App\Permission\Scope;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $fillable = [
        'task_id',
        'role_id',
        'scope',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'scope' => 'json',
    ];

    public function task()
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}
