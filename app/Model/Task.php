<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const ACCOUNT_MODULE = 'Account';

    const ACCOUNT_ONLY_SCOPE = '["Account"]';

    const MODULE_ACCESS_HRIS = 'HRIS';
    const MODULE_ACCESS_TA = 'T&A';
    const MODULE_ACCESS_PAYROLL = 'Payroll';

    const MODULE_ACCESSES = [
        self::MODULE_ACCESS_HRIS,
        self::MODULE_ACCESS_TA,
        self::MODULE_ACCESS_PAYROLL
    ];

    public $fillable = [
        'name',
        'display_name',
        'description',
        'module_access',
        'module',
        'submodule',
        'allowed_scopes',
        'ess',
    ];

    protected $casts = [
        'allowed_scopes' => 'json'
    ];
}
