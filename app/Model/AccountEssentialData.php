<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccountEssentialData extends Model
{
    protected $table = 'account_essential_data';

    protected $fillable = [
        'account_id',
        'data'
    ];

    /**
     * The attributes that should be casted
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];
}
