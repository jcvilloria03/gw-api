<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $fillable = [
        'role_id',
        'user_id',
        'module_access'
    ];

    /**
     * The attributes that should be casted
     *
     * @var array
     */
    protected $casts = [
        'module_access' => 'json',
    ];

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function getModuleAccessArrayAttribute()
    {
        return json_decode($this->module_access, true);
    }
}
