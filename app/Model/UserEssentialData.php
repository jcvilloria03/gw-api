<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserEssentialData extends Model
{
    protected $table = 'user_essential_data';

    protected $fillable = [
        'user_id',
        'account_id',
        'company_id',
        'role_id',
        'employee_id',
        'department_id',
        'cost_center_id',
        'position_id',
        'location_id',
        'payroll_group_id',
        'team_id',
        'data'
    ];

    /**
     * The attributes that should be casted
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];
}
