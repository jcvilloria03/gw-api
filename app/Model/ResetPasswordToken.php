<?php

namespace App\Model;

use Carbon\Carbon;
use App\Model\Auth0User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResetPasswordToken extends Model
{
    use SoftDeletes;

    const SUCCESS_MESSAGE = 'A password reset link has been sent to your email address. '
                          . 'Please check your email address.';

    const INACTIVE_USER_REQUESTED = 'Unable to process request. '
                                  . 'Please Contact your Administrator.';

    public $fillable = [
        'user_id',
        'token',
        'expires_at'
    ];

    public function auth0User()
    {
        return $this->belongsTo(Auth0User::class, 'user_id', 'user_id');
    }

    public function isExpired()
    {
        return Carbon::now()->gt(Carbon::parse($this->expires_at));
    }
}
