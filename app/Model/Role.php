<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use SoftDeletes;

    const ADMIN = 'Admin';
    const EMPLOYEE = 'Employee';
    const SUPERADMIN_NAME = 'Super Admin';
    const OWNER_NAME = 'Owner';

    const ROLE_TYPES = [
        self::ADMIN,
        self::EMPLOYEE,
    ];

    const ACCESS_MODULE_HRIS = 'HRIS';
    const ACCESS_MODULE_TA = 'T&A';
    const ACCESS_MODULE_PAYROLL = 'Payroll';

    const MODULE_ACCESSES = [
        self::ACCESS_MODULE_HRIS,
        self::ACCESS_MODULE_TA,
        self::ACCESS_MODULE_PAYROLL,
    ];

    const DEFAULT_MODULE_ACCESSES = [
        self::ACCESS_MODULE_HRIS,
        //self::ACCESS_MODULE_TA,
    ];

    const TA_MODULE_ACCESS = [
        self::ACCESS_MODULE_HRIS,
        self::ACCESS_MODULE_TA,
    ];

    const PAYROLL_MODULE_ACCESS = [
        self::ACCESS_MODULE_HRIS,
        self::ACCESS_MODULE_PAYROLL
    ];

    public $fillable = [
        'account_id',
        'company_id',
        'name',
        'type',
        'custom_role',
        'module_access',
    ];

    /**
     * The attributes that should be casted
     *
     * @var array
     */
    protected $casts = [
        'custom_role' => 'bool',
        'module_access' => 'json',
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }

    public function userRoles()
    {
        return $this->hasMany(UserRole::class);
    }

    public function isDefault()
    {
        return $this->custom_role == false;
    }
}
