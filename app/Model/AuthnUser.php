<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuthnUser extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_SEMI_ACTIVE = 'semi-active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_UNVERIFIED = 'unverified';

    const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_SEMI_ACTIVE,
        self::STATUS_INACTIVE,
        self::STATUS_UNVERIFIED,
    ];

    const MANUAL_STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
    ];

    protected $fillable = [
        'user_id',
        'account_id',
        'company_id',
        'employee_id',
        'authn_user_id',
        'status'
    ];

    public function userRoles()
    {
        return $this->hasMany(UserRole::class, 'user_id', 'user_id');
    }

    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }
}
