<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AuthnAccessToken extends Model
{
    protected $fillable = [
        'user_id',
        'account_id',
        'company_id',
        'employee_id',
        'authn_user_id',
        'access_token',
        'client_id',
        'client_name',
        'scopes',
        'revoked',
        'expires_at',
    ];

    public function isValid()
    {
        $expiresAt = Carbon::parse($this->expires_at);
        return !$this->revoked && Carbon::now()->lt($expiresAt);
    }
}
