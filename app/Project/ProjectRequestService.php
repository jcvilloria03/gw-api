<?php

namespace App\Project;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ProjectRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get project info
     *
     * @param int $id Project ID
     * @return \Illuminate\Http\JsonResponse Project Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/project/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create project
     *
     * @param array $data project information
     * @return \Illuminate\Http\JsonResponse Created Project
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/project",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to create multiple projects
     *
     * @param array $data projects information
     * @return \Illuminate\Http\JsonResponse Created Project ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/project/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if projects name is available
     *
     * @param int $companyId Company ID
     * @param array $data projects information
     * @return \Illuminate\Http\JsonResponse Availability of projects name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/project/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all projects within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company projects
     */
    public function getCompanyProjects(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/projects"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update project for given id
     *
     * @param array $data project informations
     * @param int $id Project Id
     * @return \Illuminate\Http\JsonResponse Updated project
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/project/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple projects
     *
     * @param array $data projects to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted projects id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/project/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
