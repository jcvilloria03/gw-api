<?php

namespace App\Project;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class ProjectAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.project';
    const CREATE_TASK = 'create.project';
    const UPDATE_TASK = 'edit.project';
    const DELETE_TASK = 'delete.project';

    /**
     * @param \stdClass $project
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $project, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($project, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $project
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyProjects(\stdClass $project, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($project, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $project
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $project, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($project, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $project
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $project, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($project, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::DELETE_TASK);
    }

    /**
     * Authorize project related tasks
     *
     * @param \stdClass $project
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $project,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for project account
            return $accountScope->inScope($project->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as project's account
                return $project->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($project->company_id);
        }

        return false;
    }
}
