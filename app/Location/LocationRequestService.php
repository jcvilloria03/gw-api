<?php

namespace App\Location;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class LocationRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to check if location name is available
     *
     * @param int $id Company ID
     * @param array $data location information
     * @return json Name is available for company
     *
     */
    public function isNameAvailable(int $id, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$id}/location/is_name_available/",
            [
               'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Philippine Location details
     *
     * @param int $id Location ID
     * @return json Location information
     *
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/location/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Philippine Locations for a company
     *
     * @param int $companyId Company ID
     * @return json Locations information
     *
     */
    public function getCompanyLocations(int $companyId, AuthzDataScope $dataScope = null)
    {
        $request = new Request(
            'GET',
            "/philippine/company/{$companyId}/locations"
        );
        return $this->send($request, $dataScope);
    }

    /**
     * Call endpoint to create location
     *
     * @param array $data location information
     * @return \Illuminate\Http\JsonResponse Created Location
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/philippine/location",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
