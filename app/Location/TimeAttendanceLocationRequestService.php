<?php

namespace App\Location;

use App\Authz\AuthzDataScope;
use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TimeAttendanceLocationRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Philippine Location details
     *
     * @param int $id Location ID
     * @return json Location information
     *
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/time_attendance_locations/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to fetch Philippine Locations for a company
     *
     * @param int $companyId Company ID
     * @param array $params Parameters
     * @param \App\Authz\AuthzDataScope|null $authzDataScope Authz Data Scope instance
     * @return json Locations information
     *
     */
    public function getCompanyLocations(int $companyId, array $params = [], AuthzDataScope $authzDataScope = null)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/time_attendance_locations?" . http_build_query($params)
        );
        return $this->send($request, $authzDataScope);
    }

    /**
     * Call endpoint to create multiple locations
     *
     * @param array $data locations information
     * @return \Illuminate\Http\JsonResponse Created Location ids
     */
    public function bulkCreate(array $data)
    {
        $request = new Request(
            'POST',
            "/time_attendance_locations/bulk_create",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update location for given id
     *
     * @param array $data location informations
     * @param int $id location Id
     * @return \Illuminate\Http\JsonResponse Updated location
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/time_attendance_locations/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
      * Call endpoint to delete multiple locations
      *
      * @param array $data locations to delete informations
      * @return \Illuminate\Http\JsonResponse Deleted locations id's
      */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/time_attendance_locations/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create location
     *
     * @param array $data location information
     * @return \Illuminate\Http\JsonResponse Created Location
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/time_attendance_locations",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }
}
