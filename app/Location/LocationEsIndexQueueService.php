<?php

namespace App\Location;

use App\ES\ESIndexQueueService;

class LocationEsIndexQueueService
{
    const TYPE = 'location';

    /**
     * @var \App\ES\ESIndexQueueService
     */
    protected $indexQueueService;

    public function __construct(
        ESIndexQueueService $indexQueueService
    ) {
        $this->indexQueueService = $indexQueueService;
    }

    /**
     * Enqueue updated/created locations for indexing
     * @param array $locationIds Location Ids to be indexed
     */
    public function queue(array $locationIds)
    {
        $details = [
            'id' => $locationIds,
            'type' => self::TYPE,
        ];

        $this->indexQueueService->queue($details);
    }
}
