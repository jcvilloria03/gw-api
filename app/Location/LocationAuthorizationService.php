<?php

namespace App\Location;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class LocationAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.location';
    const CREATE_TASK = 'create.location';
    const UPDATE_TASK = 'edit.location';
    const DELETE_TASK = 'delete.location';

    /**
     * Authorize location related tasks
     *
     * @param \stdClass $location
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $location,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for location's account
            return $accountScope->inScope($location->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as locations's account
                return $location->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($location->company_id);
        }

        return false;
    }

    /**
     * @param \stdClass $location
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $location, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($location, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $location
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyLocations(\stdClass $location, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($location, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $location
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $location, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($location, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $location
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $location, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($location, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::UPDATE_TASK);
    }

     /**
      * @param int $targetAccountId
      * @param int $userId
      * @return bool
      */
    public function authorizeDelete(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($company, $user, self::DELETE_TASK);
    }
}
