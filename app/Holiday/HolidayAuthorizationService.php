<?php

namespace App\Holiday;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class HolidayAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.holiday';
    const CREATE_TASK = 'create.holiday';
    const UPDATE_TASK = 'edit.holiday';
    const DELETE_TASK = 'delete.holiday';

    /**
     * @param \stdClass $holiday
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $holiday
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyHolidays(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $holiday
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $holiday
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $holiday
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::UPDATE_TASK);
    }

    /**
     * @param \stdClass $holiday
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $holiday, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($holiday, $user, self::DELETE_TASK);
    }

    /**
     * Authorize holiday related tasks
     *
     * @param \stdClass $holiday
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $holiday,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for holiday account
            return $accountScope->inScope($holiday->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as holiday's account
                return $holiday->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($holiday->company_id);
        }

        return false;
    }
}
