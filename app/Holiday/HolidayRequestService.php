<?php

namespace App\Holiday;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class HolidayRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get holiday info
     *
     * @param int $id Holiday ID
     * @return \Illuminate\Http\JsonResponse Holiday Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/holiday/{$id}"
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create holiday
     *
     * @param array $data holiday information
     * @return \Illuminate\Http\JsonResponse Created Holiday
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            '/holiday',
            [
                'Content-Type' => 'application/json'
            ],
            \GuzzleHttp\json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check if holiday's name is available
     *
     * @param int $companyId Company ID
     * @param array $data holiday's information
     * @return \Illuminate\Http\JsonResponse Availability of holiday's name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/holiday/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to get all holidays within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company holidays
     */
    public function getCompanyHolidays(int $companyId, array $params = [])
    {
        $url = "/company/{$companyId}/holidays";
        if (!empty($params)) {
            $url .= "?" . http_build_query($params);
        }
        $request = new Request(
            'GET',
            $url
        );

        return $this->send($request);
    }

     /**
     * Call endpoint to update holiday for given id
     *
     * @param array $data holiday informations
     * @param int $id Holiday Id
     * @return \Illuminate\Http\JsonResponse Updated holiday
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/holiday/{$id}",
            [
                'Content-Type' => 'application/json'
            ],
            \GuzzleHttp\json_encode($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple holidays
     *
     * @param array $data holidays to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted holiday id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            '/holiday/bulk_delete',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to check are holidays in use
     *
     * @param array $data holidays information
     * @return \Illuminate\Http\JsonResponse Usement of holidays
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            '/holiday/check_in_use/',
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to create company default holidays
     *
     * @param array $data holidays information
     * @return \Illuminate\Http\JsonResponse Usement of holidays
     */
    public function createCompanyDefaultHolidays(int $companyId)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/default_holidays"
        );

        return $this->send($request);
    }
}
