<?php

namespace App\LeaveType;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class LeaveTypeRequestService extends RequestService
{
    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to get leave type info
     *
     * @param int $id Leave Type ID
     * @return \Illuminate\Http\JsonResponse Leave Type Info
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/leave_type/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create leave type
     *
     * @param array $data leave type information
     * @return \Illuminate\Http\JsonResponse Created Leave Type
     */
    public function create(array $data)
    {
        $request = new Request(
            'POST',
            "/leave_type",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check if leave types name is available
     *
     * @param int $companyId Company ID
     * @param array $data leave types information
     * @return \Illuminate\Http\JsonResponse Availability of leave types name
     */
    public function isNameAvailable(int $companyId, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$companyId}/leave_type/is_name_available/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all leave types within company
     *
     * @param int $companyId Company Id
     * @return \Illuminate\Http\JsonResponse List of company leave types
     */
    public function getCompanyLeaveTypes(int $companyId)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/leave_types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all entitled leave types for employee
     *
     * @param int $employeeId Employee Id
     * @return \Illuminate\Http\JsonResponse List of employee leave types
     */
    public function getEntitledLeaveTypes(int $employeeId)
    {
        $request = new Request(
            'GET',
            "/employee/{$employeeId}/entitled_leave_types"
        );
        return $this->send($request);
    }

     /**
     * Call endpoint to update leave type for given id
     *
     * @param array $data leave type informations
     * @param int $id Leave Type Id
     * @return \Illuminate\Http\JsonResponse Updated leave type
     */
    public function update(array $data, int $id)
    {
        $request = new Request(
            'PUT',
            "/leave_type/" . $id,
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to delete multiple leave types
     *
     * @param array $data leave types to delete informations
     * @return \Illuminate\Http\JsonResponse Deleted leave types id's
     */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/leave_type/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to check are leave types in use
     *
     * @param array $data leave types information
     * @return \Illuminate\Http\JsonResponse Count of leave types in use
     */
    public function checkInUse(array $data)
    {
        $request = new Request(
            'POST',
            "/leave_type/check_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all leave types within company
     *
     * @param int $companyId Company Id
     * @param string $query
     * @return \Illuminate\Http\JsonResponse List of company leave types
     */
    public function searchLeaveTypes(int $companyId, string $query)
    {
        $request = new Request(
            'GET',
            "/company/{$companyId}/leave_types/search?{$query}"
        );

        return $this->send($request);
    }
}
