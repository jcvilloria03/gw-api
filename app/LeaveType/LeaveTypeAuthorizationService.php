<?php

namespace App\LeaveType;

use App\Authorization\AuthorizationService;
use App\Permission\TargetType;

class LeaveTypeAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.leave_type';
    const CREATE_TASK = 'create.leave_type';
    const UPDATE_TASK = 'edit.leave_type';
    const DELETE_TASK = 'delete.leave_type';

    /**
     * @param \stdClass $leaveType
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveType
     * @param array $user
     * @return bool
     */
    public function authorizeGetCompanyLeaveTypes(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::VIEW_TASK);
    }

    /**
     * @param \stdClass $leaveType
     * @param array $user
     * @return bool
     */
    public function authorizeCreate(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $leaveType
     * @param array $user
     * @return bool
     */
    public function authorizeIsNameAvailable(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::CREATE_TASK);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::UPDATE_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeDelete(\stdClass $leaveType, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeTask($leaveType, $user, self::DELETE_TASK);
    }

    /**
     * Authorize leaveType related tasks
     *
     * @param \stdClass $leaveType
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeTask(
        \stdClass $leaveType,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for leaveType account
            return $accountScope->inScope($leaveType->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as leaveType's account
                return $leaveType->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($leaveType->company_id);
        }

        return false;
    }
}
