<?php

namespace App\LeaveType;

use App\Audit\AuditItem;
use App\Audit\AuditService;

class LeaveTypeAuditService
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const OBJECT_NAME = 'leave_type';

    /**
     * @var \App\Audit\AuditService
     */
    private $auditService;

    public function __construct(AuditService $auditService)
    {
        $this->auditService = $auditService;
    }

    /**
     * Log LeaveType related action
     *
     * @param array $cacheItem
     */
    public function log(array $cacheItem)
    {
        switch ($cacheItem['action']) {
            case self::ACTION_CREATE:
                $this->logCreate($cacheItem);
                break;
            case self::ACTION_UPDATE:
                $this->logUpdate($cacheItem);
                break;
            case self::ACTION_DELETE:
                $this->logDelete($cacheItem);
                break;
        }
    }

    /**
     * Log LeaveType create
     *
     * @param array $cacheItem
     */
    public function logCreate(array $cacheItem)
    {
        $data = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $data['company_id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_CREATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $data['id'],
                'name' => $data['name']
            ]
        ]);
        $this->auditService->log($item);
    }

    /**
     * Log LeaveType update
     *
     * @param array $cacheItem
     */
    public function logUpdate(array $cacheItem)
    {
        $old = json_decode($cacheItem['old'], true);
        $new = json_decode($cacheItem['new'], true);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $new['id'],
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_UPDATE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $new['id'],
                'old' => $old,
                'new' => $new,
            ]
        ]);
        $this->auditService->log($item);
    }

     /**
     *
     * Log LeaveType Delete
     *
     * @param array $cacheItem
     *
     */
    public function logDelete(array $cacheItem)
    {
        $leaveTypeData = json_decode($cacheItem['old']);
        $user = json_decode($cacheItem['user'], true);

        $item = new AuditItem([
            'company_id' => $leaveTypeData->company_id,
            'account_id' => $user['account_id'],
            'user_id' => $user['id'],
            'action' => self::ACTION_DELETE,
            'object_name' => self::OBJECT_NAME,
            'data' => [
                'id' => $leaveTypeData->id
            ]
        ]);
        $this->auditService->log($item);
    }
}
