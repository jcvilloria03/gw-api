<?php

namespace App\Authn;

use App\Request\RequestService;
use GuzzleHttp\Psr7\Request;

class AuthnRequestService extends RequestService
{
    /**
     * Call endpoint to fetch access token's user
     *
     * @param string $bearerToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByAccessToken(string $bearerToken)
    {
        $request = new Request(
            'GET',
            '/api/v1/auth/user?' . http_build_query($this->getClientCredentials()),
            [
                'Accept' => 'application/json',
                'Authorization' => $bearerToken
            ]
        );

        return $this->send($request);
    }

    /**
     * Call endpoint to resend email verification
     *
     * @param int $id Authn user ID
     * @param string $bearerToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendEmailVerification(int $id, string $bearerToken)
    {
        $request = new Request(
            'POST',
            "/api/v1/auth/email/verify/{$id}/resend?" . http_build_query($this->getClientCredentials()),
            [
                'Accept' => 'application/json',
                'Authorization' => $bearerToken
            ]
        );

        return $this->send($request);
    }

    /**
     * Get client credentials
     *
     * @return array
     */
    private function getClientCredentials()
    {
        return [
            'client_id' => env('AUTHN_CLIENT_ID'),
            'client_secret' => env('AUTHN_CLIENT_SECRET')
        ];
    }

    /**
     * Call endpoint to fetch user via email
     *
     * @param string $bearerToken
     * @param string $email
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthnUserProfileByEmail(string $bearerToken, string $email)
    {
        $clientCredentials = [
            'client_id' => env('AUTHN_CLIENT_ID'),
            'client_secret' => env('AUTHN_CLIENT_SECRET')
        ];

        $request = new Request(
            'GET',
            '/api/v1/users/via-email/' . $email.  '?' . http_build_query($clientCredentials),
            [
                'Accept' => 'application/json',
                'Authorization' => $bearerToken
            ]
        );

        return $this->send($request);
    }
}
