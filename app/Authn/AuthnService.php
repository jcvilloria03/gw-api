<?php

namespace App\Authn;

use Carbon\Carbon;
use App\Model\AuthnUser;
use Illuminate\Support\Arr;
use App\Model\AuthnAccessToken;
use App\Authn\AuthnRequestService;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class AuthnService
{
    /**
     * @var \App\Authn\AuthnRequestService
     */
    protected $requestService;

    public function __construct(AuthnRequestService $requestService)
    {
        $this->requestService = $requestService;
    }

    /**
     *
     * Create Authn user record
     *
     * @param array $data
     *
     * @return AuthnUser The created authn user object
     */
    public function createAuthnUser(array $data)
    {
        return AuthnUser::create([
            'user_id' => $data['user_id'],
            'account_id' => $data['account_id'],
            'company_id' => !empty($data['company_id']) ? $data['company_id'] : null,
            'employee_id' => !empty($data['employee_id']) ? $data['employee_id'] : null,
            'authn_user_id' => $data['authn_user_id'],
            'status' => $data['status']
        ]);
    }

    /**
     *
     * Update Authn user record
     *
     * @param int $userId
     * @param array $data
     *
     * @return AuthnUser The updated authn user object
     */
    public function updateAuthnUser(int $userId, array $data)
    {
        return AuthnUser::where('user_id', $userId)->update($data);
    }

    /**
     *
     * Get AuthnUser record by authn_user_id
     *
     * @param int $authnUserId
     *
     * @return AuthnUser The authn user object
     */
    public function getAuthnUserByAuthnId(string $authnUserId)
    {
        return AuthnUser::whereAuthnUserId($authnUserId)->first();
    }

    /**
     *
     * Get access token
     *
     * @param int $authnId
     *
     * @return AuthnAccessToken The authn access token object
     */
    public function getAccessTokenByAuthnId(int $authnId)
    {
        return AuthnAccessToken::where([
            ['authn_user_id', $authnId],
            ['revoked', 0]
        ])->first();
    }

    /**
     *
     * Insert access token record
     *
     * @param array $data The access token details
     *
     * @return AuthnAccessToken The created authn access token object
     */
    public function insertAccessToken(array $data)
    {
        return AuthnAccessToken::create([
            'user_id' => $data['user_id'],
            'account_id' => $data['account_id'],
            'company_id' => $data['company_id'],
            'authn_user_id' => $data['authn_user_id'],
            'client_id' => $data['client_id'],
            'access_token' => $data['access_token'],
            'scopes' => $data['scopes'],
            'revoked' => $data['revoked'],
            'expires_at' => $data['expires_at']
        ]);
    }

    /**
     *
     * Revoke user's existing tokens
     *
     * @param int $userId
     *
     * @return void
     */
    public function revokeUserTokens(int $userId)
    {
        AuthnAccessToken::where([
            ['user_id', $userId],
            ['revoked', 0]
        ])->update(['revoked' => 1]);
    }

    /**
     *
     * Validate token in authn and store
     *
     * @param string $bearerToken
     *
     * @return AuthnAccessToken The created authn access token object
     */
    public function validateToken(string $bearerToken)
    {
        // Validate token's client id and expiration
        $payload = $this->getPayloadFromToken($bearerToken);
        $clientId = Arr::get($payload, 'aud');
        $expiresAtUnix = Arr::get($payload, 'exp');
        $authnId = Arr::get($payload, 'sub');
        $scopes = Arr::get($payload, 'scopes');

        // return error if token payload has no aud and exp attributes
        if (!$clientId || !$expiresAtUnix) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Unauthenticated.');
        }

        // return error if token is already expired
        $expiresAt = Carbon::createFromTimestamp(Arr::get($payload, 'exp'));
        if (Carbon::now()->startOfDay()->gte($expiresAt)) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Unauthenticated. Access token has already expired.');
        }

        // get token from authn_access_tokens table
        $token = $this->getAccessTokenByAuthnId($authnId);


        // if token doesn't exists validate from authn-api
        if (!$token || ($token && $token->access_token !== $bearerToken)) {
            try {
                $response = $this->requestService->getUserByAccessToken($bearerToken);
                $data = json_decode($response->getData(), true);
                $user = Arr::get($data, 'data.user');
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error($e->getTraceAsString());

                throw new HttpException(
                    Response::HTTP_UNAUTHORIZED,
                    'Unauthenticated. Invalid access token.'
                );
            }

            // revoke old user tokens
            $this->revokeUserTokens($user['user_id']);

            // store validated token
            $token = $this->insertAccessToken([
                'user_id' => $user['user_id'],
                'account_id' => $user['account_id'],
                'company_id' => $user['company_id'],
                'authn_user_id' => $user['id'],
                'client_id' => $clientId,
                'access_token' => $bearerToken,
                'scopes' => json_encode($scopes),
                'revoked' => 0,
                'expires_at' => $expiresAt
            ]);
        }

        if ($token && !$token->isValid()) {
            throw new HttpException(
                Response::HTTP_UNAUTHORIZED,
                'Unauthenticated. Access Token is not valid or has expired.'
            );
        }

        return $token;
    }

    /**
     *
     * Insert access token record
     *
     * @param string $bearerToken The access token details
     *
     * @return array $The payload extracted from token
     */
    public function getPayloadFromToken(string $bearerToken)
    {
        try {
            // decode jwt
            $parts = explode('.', $bearerToken);
            $payload = base64_decode($parts[1]);

            return json_decode($payload, true);
        } catch (\Exception $e) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Invalid token.');
        }
    }

    /**
     *
     * Fetch AuthnUser for a given User ID
     *
     * @param int $userId User ID
     * @return AuthnUser object
     */
    public function getUser(int $userId)
    {
        return AuthnUser::where('user_id', $userId)->first();
    }

    /**
     *
     * Resend email verification
     *
     * @param int $userId User ID
     * @param string $bearerToken The access token
     * @return AuthnUser object
     */
    public function resendVerification(int $authnId, string $bearerToken)
    {
        return $this->requestService->resendEmailVerification($authnId, $bearerToken);
    }

    /**
     * Get authn Active users for account id.
     *
     * @param int $accountId
     * @param array $userIds
     * @return array
     */
    public function getActiveUserIds(int $accountId, array $userIds = [])
    {
        return AuthnUser::where('account_id', $accountId)
            ->where('status', AuthnUser::STATUS_ACTIVE)
            ->when(!empty($userIds), function ($q) use ($userIds) {
                return $q->whereIn('user_id', $userIds);
            })
            ->get()
            ->toArray();
    }

    /**
     *
     * Fetch AuthnUsers for a given User IDS
     *
     * @param array $ids User IDS
     * @return AuthnUser object
     */
    public function getUsersByIds(array $ids)
    {
        return AuthnUser::whereIn('user_id', $ids)->get();
    }

    /**
     *
     * Fetch AuthnUser for a given Employee ID
     *
     * @param string $employeeId Employee ID
     * @return AuthnUser|null object
     */
    public function getUserByEmployeeId(string $employeeId)
    {
        return AuthnUser::where('employee_id', $employeeId)->first();
    }

    /**
     *
     * Activate AuthnUser for a given User ID
     *
     * @param int $userId User ID
     * @return AuthnUser|null object
     */
    public function activateUserByuserId($userId)
    {
        return AuthnUser::where('user_id', $userId)
            ->update(['status' => AuthnUser::STATUS_ACTIVE]);
    }
}
