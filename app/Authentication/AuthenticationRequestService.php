<?php

namespace App\Authentication;

use GuzzleHttp\Psr7\Request;
use App\Request\RequestService;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthenticationRequestService extends RequestService
{
    const INVALID_CREDENTIAL_MSG = "Wrong Email or Password entered: Please try again!";
    /**
     * Call endpoint to create a new Auth user
     *
     * @param array $data Data to pass to endpoint
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function signUp(array $data)
    {
        try {
            $response = $this->client->request(
                'POST',
                'auth/user',
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $data
                ]
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $response = $e->getResponse();

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Call endpoint to login a registered Auth user, returns id_token for Authorization header
     *
     * @param array $data Data to pass to endpoint
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function login(array $data)
    {
        try {
            $response = $this->client->request(
                'POST',
                'auth/user/login',
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $data
                ]
            );

            $responseContents = json_decode($response->getBody()->getContents());

            if (isset($data['data'])) {
                $responseContents->data->user->intercom_user_hash = $this->getIntercomUserHash(
                    $data['data']['user']['email']
                );
            }

            return response()->json(
                $responseContents,
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $response = $e->getResponse();

            $errorMsg = $response->getReasonPhrase();

            if (in_array($response->getStatusCode(), [Response::HTTP_NOT_FOUND, Response::HTTP_FORBIDDEN])) {
                $errorMsg = self::INVALID_CREDENTIAL_MSG;
            }

            throw new HttpException($response->getStatusCode(), $errorMsg, $e);
        }
    }

    /**
     * Call endpoint to send a 'verification email' email to the user.
     *
     * @param array $data Data to pass to endpoint
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function verifyResend(array $data)
    {
        try {
            $response = $this->client->request(
                'POST',
                'auth/user/verify_resend',
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'json' => $data
                ]
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $response = $e->getResponse();

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    /**
     * Change Password request to UA-API
     *
     * @param string $auth0UserId
     * @param string $newPassword
     * @param array  $extra         extra user data to be set
     * @return \Illuminate\Http\JsonResponse
     * @throws HttpException
     */
    public function changePassword(
        string $auth0UserId,
        string $newPassword,
        array $extra = []
    ) {
        try {
            $response = $this->client->request(
                'PATCH',
                'auth/user/change_password',
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'json' => [
                        'data' => [
                            'user' => array_merge(
                                $extra,
                                [
                                    'auth0_user_id' => $auth0UserId,
                                    'password'      => $newPassword
                                ]
                            )
                        ]
                    ]
                ]
            );

            return response()->json(
                $response->getBody()->getContents(),
                $response->getStatusCode()
            );
        } catch (ConnectException $e) {
            Log::error($e->getMessage());

            throw new HttpException(Response::HTTP_SERVICE_UNAVAILABLE, 'Service unavailable.', $e);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $response = $e->getResponse();

            throw new HttpException($response->getStatusCode(), $response->getReasonPhrase(), $e);
        }
    }

    private function getIntercomUserHash(string $email)
    {
        return hash_hmac(
            'sha256',
            $email,
            config('intercom.secret')
        );
    }
}
