<?php

namespace App\Company;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;

class CompanyRequestService extends RequestService
{

    const ID_BUCKET_PREFIX = 'company_account_id:';
    const ID_BUCKET_SIZE = 1000;

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Account ID for given Company ID
     *
     * @param int $id Company ID
     * @return json Company information
     *
     */
    public function getAccountId(int $id)
    {
        if ($accountId = $this->getAccountIdFromRedis($id)) {
            return $accountId;
        }

        // nothing found in Redis, fetch from company-api then store in Redis
        $request = new Request(
            'GET',
            "/company/{$id}"
        );
        $response = $this->send($request);

        $company = json_decode($response->getData());

        $accountId = $company->account_id;

        $this->storeAccountId($id, $accountId);

        return $accountId;
    }

    public function searchByIds(array $ids)
    {
        return $this->send(new Request(
            'GET',
            '/companies?' . http_build_query(['ids' => $ids])
        ));
    }

    /**
     * Get Company's Account ID in Redis bucket
     *
     * @param int $companyId
     * @return int
     *
     */
    protected function getAccountIdFromRedis(int $companyId)
    {
        $data = Redis::hGet($this->getKey($companyId), $companyId);

        return $data;
    }

    /**
     * Store Company's Account ID in Redis bucket
     *
     * @param int $companyId
     * @return int
     *
     */
    protected function storeAccountId(int $companyId, int $accountId)
    {
        Redis::hSet($this->getKey($companyId), $companyId, $accountId);
    }

    /**
     * Get Redis Key which stores accountId of given Company ID
     *
     * @param int $companyId
     * @return int
     *
     */
    protected function getKey(int $companyId)
    {
        // Company ID : Account ID pairs are stored in buckets of 1000
        $bucketId = floor($companyId / self::ID_BUCKET_SIZE);
        return self::ID_BUCKET_PREFIX . $bucketId;
    }

    /**
     * Create CRBAC System-Defined Roles
     *
     * @param int $accountId
     * @return int
     *
     */
    public function createSystemDefinedRoles(int $accountId, bool $autoAssign = false)
    {
        return $this->send(new Request(
            'POST',
            "/accounts/{$accountId}/roles/system-defined",
            [
                'Content-Type' => 'application/json'
            ],
            json_encode([
                'auto_assign' => $autoAssign
            ])
        ));
    }
}
