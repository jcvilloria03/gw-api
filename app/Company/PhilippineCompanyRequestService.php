<?php

namespace App\Company;

use App\Request\RequestService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\MultipartStream;
use Illuminate\Support\Facades\Input;

class PhilippineCompanyRequestService extends RequestService
{

    /**
     * Constructor
     *
     * @param \GuzzleHttp\Client $client Guzzle client
     *
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * Call endpoint to fetch Philippine Company details
     *
     * @param int $id Company ID
     * @return json Company information
     *
     */
    public function get(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/company/{$id}"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create Philippine Company
     *
     * @param array $company Company information
     * @return json Company information
     *
     */
    public function create(array $data)
    {
        return $this->sendMultipart(
            'POST',
            "/philippine/company/",
            $data
        );
    }


    /**
     * Call endpoint to update Philippine Company details
     *
     * @param array $company Company information
     * @return json Company information
     *
     */
    public function updateDetails(array $company)
    {
        $request = new Request(
            'PATCH',
            "/philippine/company/details",
            [
               'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($company)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to get all company types
     *
     * @return json Company types
     *
     */
    public function getTypes()
    {
        $request = new Request(
            'GET',
            "/philippine/company_types"
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to update Company details
     *
     * @param array $company Company information
     * @param int $id Company ID
     * @return json Company information
     *
     */
    public function update(array $company, $id)
    {

        return $this->sendMultipart(
            'POST',
            "/company/$id",
            $company
        );
    }

    /**
     * Check is something in use
     *
     * @param string $type type of model
     * @param array $data array of details: id's of model
     * @return int $inUse number of records in use
     */
    public function isInUse(string $type, array $data)
    {
        $request = new Request(
            'POST',
            "/company/{$type}/is_in_use/",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
    * Call endpoint to delete multiple companies
    *
    * @param array $data companies to delete informations
    * @return \Illuminate\Http\JsonResponse Deleted companies id's
    */
    public function bulkDelete(array $data)
    {
        $request = new Request(
            'DELETE',
            "/company/bulk_delete",
            [
                'Content-Type' => 'application/x-www-form-urlencoded'

            ],
            http_build_query($data)
        );
        return $this->send($request);
    }

    /**
     * Call endpoint to create Philippine Company Demo
     *
     * @param array $company Company information
     * @return json Company information
     *
     */
    public function createCompanyDemo(array $data)
    {
        return $this->sendMultipart(
            'POST',
            "/philippine/company_demo",
            $data
        );
    }

    /**
     * Call endpoint to trigger the job to process company demo
     *
     * @param int $companyId
     * @return int
     *
     */
    public function triggerCompanyDemo(int $companyId)
    {
        return $this->send(new Request(
            'POST',
            "/philippine/company_demo_process/{$companyId}",
            [
                'Content-Type' => 'application/json'
            ]
        ));
    }

    /**
     * Call endpoint to fetch Philippine Demo Company details
     *
     * @param int $id Company ID
     * @return json Demo Company Details
     *
     */
    public function getCompanyDemo(int $id)
    {
        $request = new Request(
            'GET',
            "/philippine/company_demo/details/{$id}"
        );
        return $this->send($request);
    }

}
