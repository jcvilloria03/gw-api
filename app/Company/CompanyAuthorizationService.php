<?php

namespace App\Company;

use App\Authorization\AuthorizationService;
use App\Model\Permission;
use App\Permission\TargetType;

class CompanyAuthorizationService extends AuthorizationService
{
    const VIEW_TASK = 'view.company';
    const VIEW_LOGS_TASK = 'view.company_logs';
    const CREATE_TASK = 'create.company';
    const UPDATE_TASK = 'edit.company';
    const DELETE_TASK = 'delete.company';

    /**
     * Authorize view, update, delete related tasks
     *
     * @param \stdClass $company
     * @param array $user
     * @param string $taskType
     * @return bool
     */
    private function authorizeNonCreateTask(
        \stdClass $company,
        array $user,
        string $taskType
    ) {
        // Check module access
        if (!$this->checkTaskModuleAccess($taskType)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes($taskType);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        if ($accountScope) {
            // check if user has account level permissions for company's account
            return $accountScope->inScope($company->account_id);
        }

        // verify company scope
        $companyScope = $taskScopes->getScopeBasedOnType(TargetType::COMPANY);
        if ($companyScope) {
            if ($companyScope->targetAll()) {
                // check if user's account is same as company's account
                return $company->account_id == $user['account_id'];
            }
            // check if user has company level permissions for company
            return $companyScope->inScope($company->id);
        }

        return false;
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeGet(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($company, $user, self::VIEW_TASK);
    }

    /**
     * @param int $targetAccountId
     * @param int $userId
     * @return bool
     */
    public function authorizeCreate(int $targetAccountId, int $userId)
    {
        $this->buildUserPermissions($userId);

        // Check module access
        if (!$this->checkTaskModuleAccess(self::CREATE_TASK)) {
            return false;
        }

        $taskScopes = $this->getTaskScopes(self::CREATE_TASK);
        if (!$taskScopes) {
            return false;
        }

        // verify account scope
        $accountScope = $taskScopes->getScopeBasedOnType(TargetType::ACCOUNT);
        return $accountScope &&$accountScope->inScope($targetAccountId);
    }

    /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeUpdate(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($company, $user, self::UPDATE_TASK);
    }

     /**
     * @param \stdClass $company
     * @param array $user
     * @return bool
     */
    public function authorizeDelete(\stdClass $company, array $user)
    {
        $this->buildUserPermissions($user['user_id']);
        return $this->authorizeNonCreateTask($company, $user, self::DELETE_TASK);
    }

}
