# API Gateway [![CircleCI](https://circleci.com/gh/SalariumPH/salarium-api-lumen-boilerplate.svg?style=shield&circle-token=3b577cc540d72ef3200213bc48747fcbc1ba2db0)](https://circleci.com/gh/SalariumPH/salarium-api-lumen-boilerplate/tree/master)

# Documentations

- [Boilerplate Wiki](https://github.com/SalariumPH/salarium-api-lumen-boilerplate/wiki) - Contains Local setup guide, Git workflow, and tools used in the project. Also contains guide to creating new project from boilerplate.

- [Auth0 Middleware](https://github.com/SalariumPH/api-gateway/wiki/Auth0Middleware) - Middleware that can be used to authorize JWT tokens passed along with requests.


# Audit Trail Instructions

1. Go to the file `gw-api/config/audit_trail.php` and add the endpoints to the `endpoint` list. Group them by module if the module doesn't exist.
2. All the Fetch endpoint requests included in the audit trail will be processed by the AuditTrailMiddleware even if it's a `POST` method. AuditTrailMiddleware will determine the correct method via Authz Action.
If the endpoint is a `Fetch Request` then the setup is finished here. Otherwise proceed to steps below.
3. Go to each endpoint controller and paste the uses of the `Audit Trail` Trait.
    - at the top part `use App\Traits\AuditTrailTrait`;
    - inside the controller class `use AuditTrailTrait`;
4. To use the function inside the trait we only need to call:
	```
    $this->audit($request, $companyId, $newData, $oldData);
    ```
    ### Parameters:
        - $request: the request instance (required)
        - $companyId: the company id (optional but better if passed if it's accessible, the audit trail fetches the company id from authz if the value is not set and is not 100% reliable)
        - $newData: the updated or newly created data after processing the request. This is needed in `Create` and `Update` requests but not required in `Delete` request
        - $oldData: the current data before processing the request. This is needed in `Update` and `Delete` requests but not required in `Create` request. You may also need to manually fetched the current data here
5. Don't forget to configure and up the Audit Trail API to be able to receive the logs.